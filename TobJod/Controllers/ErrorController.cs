﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TobJod.Web.Controllers {
    public class ErrorController : BaseController {

        protected override void OnActionExecuting(ActionExecutingContext filterContext) {
            if (Request.QueryString["aspxerrorpath"] != null && Request.QueryString["aspxerrorpath"].Contains("/en/")) {
                Language = TobJod.Models.Language.EN;
            } else {
                Language = TobJod.Models.Language.TH;
            }
            filterContext.RouteData.Values["language"] = Language.ToString().ToLower();

            base.OnActionExecuting(filterContext);
        }

        // GET: Error
        public ActionResult Index() {
            return View(Language.ToString() + "/Index");
        }

        // GET: Browser
        public ActionResult Browser() {
            return View(Language.ToString() + "/Browser");
        }
    }
}