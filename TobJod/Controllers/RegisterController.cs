﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;
using TobJod.Web.Models;

namespace TobJod.Web.Controllers
{
    public class RegisterController : BaseController
    {

        // GET: Contact
        public ActionResult Index()
        {
            RegisterViewModel model = BindForm(new RegisterViewModel());
            DayOffRepository dayOffRepo = new DayOffRepository();
            model.DayOff = dayOffRepo.ListDayOff_Active();
            model.EnableCaptcha = base.EnableChaptcha;

            return View(Language.ToString() + "/Index", model);
        }


        private RegisterViewModel BindForm(RegisterViewModel model)
        {
            ConfigPageRepository repo = new ConfigPageRepository();
            var config = repo.GetConfigPage(TobJod.Models.ConfigPageName.ContactUs);
            ContactSubCategoryRepository subRepo = new ContactSubCategoryRepository();

            var sub = subRepo.ListContactSubCategory_SelectList_Active(Language);
            model.ConfigPage = config;
            Dictionary<String, List<ContactSubCategory>> subC = new Dictionary<string, List<ContactSubCategory>>();
            foreach (var item in sub)
            {
                if (!subC.ContainsKey(item.Category)) subC.Add(item.Category, new List<ContactSubCategory>());
                subC[item.Category].Add(item);
            }

            List<SelectListItem> listItems = new List<SelectListItem>();
            foreach (var item in subC.Keys)
            {
                if (subC[item].Count == 1)
                {
                    var it = subC[item][0];
                    listItems.Add(new SelectListItem() { Text = (Language == Language.EN ? it.TitleEN : it.TitleTH), Value = it.Id.ToString(), Group = null });
                }
                else
                {
                    var group = new SelectListGroup() { Name = item };
                    foreach (var it in subC[item])
                    {
                        listItems.Add(new SelectListItem() { Text = (Language == Language.EN ? it.TitleEN : it.TitleTH), Value = it.Id.ToString(), Group = group });



                    }
                }
            }
            model.SubCategoryList = listItems;

            PageMeta.FromConfigPage(config, Language);
            return model;
        }

        public ActionResult Login(RegisterViewModel model)
        {

            return View(Language.ToString() + "/LoginSuccess", model);
        }

        public ActionResult LoginFacebook(RegisterViewModel model)
        {



            return View(Language.ToString() + "/FBregister", model);
        }
    }
}