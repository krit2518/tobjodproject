﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TobJod.Web.Models;
using TobJod.Repositories;
using TobJod.Utils;
using TobJod.Models;
using System.Net.Mail;
using System.Text;

namespace TobJod.Web.Controllers {
    [OutputCache(Duration = 0)]
    public class ProductController : BaseController {

        string PathCompany;
        int ProductListSize = 6;

        public ProductController() {
            PathCompany = HttpUtils.GetPathRelative(System.Configuration.ConfigurationManager.AppSettings["Path_Company"]);
            ConfigRepository configRepo = new ConfigRepository();
            ViewBag.LayoutGridView = string.Equals(configRepo.GetConfig(ConfigName.ProductLayout).Body, "2");
            ViewBag.ListPage = 0;
        }

        // GET: Product
        public ActionResult Index() {
            return View();
        }

        #region Motor

        // GET: Product/Motor-Overview
        [ActionName("motor-overview")]
        public ActionResult MotorOverview() {
            return View(Language.ToString() + "/MotorOverview");
        }

        // GET: Product/Motor
        public ActionResult Motor(ProductMotorViewModel item) {
            ViewBag.ListPage = NullUtils.cvInt(Request.QueryString["listpage"]);
            ViewBag.ShowLeadForm = ShowLeadForm();
            item.PathCompany = PathCompany;
            var prices = ValueUtils.GetIntRange(item.Price, MainLayoutViewModel.ProductMotorPriceMin, MainLayoutViewModel.ProductMotorPriceMax, ';');
            item.PriceFrom = prices.Item1;
            item.PriceTo = prices.Item2;
            item.Compulsory = (NullUtils.cvInt(Request.Params["compulsory"]));
            item.CCTV = GetAllYesNoFlag("cctv");
            item.Garage = GetAllYesNoFlag("garage");
            item.DriverSpecify = GetAllYesNoFlag("driver");
            item.DriverAge = NullUtils.cvInt(Request.Params["driverage"]);

            item.CarBrands = MainLayoutViewModel.CarBrands;
            CarRepository carRepo = new CarRepository();
            item.CarFamilies = carRepo.ListCarFamilyByBrand_NameValue(Language, item.Brand);
            item.CarModels = carRepo.ListCarModelByFamily_NameValue(Language, item.Family);
            MasterOptionRepository masterRepo = new MasterOptionRepository();
            item.CarProvinces = masterRepo.ListMasterOption_SelectList_Active(Language, MasterOptionCategory.MotorProvince);
            item.DriverAges = masterRepo.ListMasterOption_SelectList_Active(Language, MasterOptionCategory.DriverAge);
            return View(Language.ToString() + "/Motor", item);
        }

        // GET: Product/MotorList
        public ActionResult MotorList() {
            ProductMotorViewModel model = new ProductMotorViewModel();
            model.PathCompany = PathCompany;
            model.LayoutGridView = NullUtils.cvBoolean(Request.Params["LayoutGridView"]);
            model.Category = NullUtils.cvInt(Request.Params["category"]);
            model.Year = NullUtils.cvInt(Request.Params["year"]);
            model.Brand = NullUtils.cvInt(Request.Params["brand"]);
            model.Family = NullUtils.cvInt(Request.Params["family"]);
            model.Model = NullUtils.cvInt(Request.Params["model"]);
            model.Province = NullUtils.cvInt(Request.Params["province"]);
            model.Usage = NullUtils.cvInt(Request.Params["usage"]);
            model.Compulsory = NullUtils.cvInt(Request.Params["compulsory"]);
            var prices = ValueUtils.GetIntRange(Request.Params["price"], MainLayoutViewModel.ProductMotorPriceMin, MainLayoutViewModel.ProductMotorPriceMax, ';');
            model.PriceFrom = prices.Item1;
            model.PriceTo = prices.Item2;
            model.CCTV = GetAllYesNoFlag("cctv");
            model.Garage = GetAllYesNoFlag("garage");
            model.DriverSpecify = GetAllYesNoFlag("driver");
            model.DriverAge = NullUtils.cvInt(Request.Params["driverage"]);
            
            int pageNo = GetPageNo();
            int sortId = NullUtils.cvInt(Request.Params["sort"]);
            var premiumColumn = (model.Compulsory == 1 ? "DisplayCombinePremium" : "DisplayPremium");
            string sort = premiumColumn+ " ASC";
            switch (sortId) {
                //case 1: sort = "Id DESC"; break;
                case 2: sort = premiumColumn + " DESC"; break;
                case 3: sort = "Company ASC, Premium ASC"; break;
                case 4: sort = "Company DESC, Premium ASC"; break;
            }

            int driverAgeMin = 0;
            int driverAgeMax = 0;
            if (model.DriverSpecify == AllYesNoFlag.Yes && model.DriverAge > 0) {
                MasterOptionRepository masterRepo = new MasterOptionRepository();
                var ages = masterRepo.GetMasterOption_Active(model.DriverAge);
                if (ages != null) {
                    driverAgeMin = ages.Min;
                    driverAgeMax = ages.Max;
                }
            }

            int listPage = NullUtils.cvInt(Request.QueryString["listpage"]);
            if (listPage > 0) {
                ProductListSize = ProductListSize * (listPage + 1);
                pageNo = 1;
            }

            ProductRepository repo = new ProductRepository();
            var product = repo.ListProductMotor_Active(Language, pageNo, ProductListSize, model.Category, model.Year, model.Model, model.Province, model.CCTV, model.Garage, model.DriverSpecify, driverAgeMin, driverAgeMax, (model.Compulsory == 1), model.PriceFrom, model.PriceTo, sort);

            model.Products = product;
            ViewBag.Usage = model.Usage;

            if (listPage > 0) { pageNo = listPage; }

            ViewBag.Page = pageNo;
            ViewBag.Count = product.Count;

            return PartialView(Language.ToString() + "/MotorList", model);
        }

        // GET: Product/MotorDetail/1/2
        public ActionResult MotorDetail(int ProductId, int PremiumId, int Usage, int Compulsory = 0) {
            ProductRepository repo = new ProductRepository();

            ProductItem item = repo.GetProductMotor_Active(Language, ProductId, PremiumId);
            if (item == null) return RedirectToAction("Motor");
            item.Usage = Usage;
            item.Compulsory = Compulsory;
            item.Id = ProductId;
            item.CompanyImage = PathCompany + item.CompanyImage;
            item.CategoryId = ProductCategoryKey.Motor;
            ViewBag.LeadForm = GetLeadForm();
            return View(Language.ToString() + "/MotorDetail", item);
        }

        // GET: Product/MotorCompare
        public ActionResult MotorCompare() {
            string[] idList = Request.Params["id"].Split(',');
            //if (idList.Length < 2) return RedirectToAction("Motor");
            ProductMotorViewModel model = new ProductMotorViewModel();
            model.Brand = NullUtils.cvInt(Request.QueryString["brand"]);
            model.Family = NullUtils.cvInt(Request.QueryString["family"]);
            model.Model = NullUtils.cvInt(Request.QueryString["model"]);
            model.Year = NullUtils.cvInt(Request.QueryString["year"]);
            model.Province = NullUtils.cvInt(Request.QueryString["Province"]);
            model.Usage = NullUtils.cvInt(Request.QueryString["usage"]);
            model.Compulsory = NullUtils.cvInt(Request.QueryString["compulsory"]);
            model.PathCompany = PathCompany;

            CarRepository carRepo = new CarRepository();
            var cBrand = carRepo.GetCarBrand_Active(Language, model.Brand);
            var cModel = carRepo.GetCarModel_Active(Language, model.Model);
            if (cBrand == null || cModel == null || cModel.BrandId != cBrand.Id) { return RedirectToAction("Motor"); }
            model.BrandTitle = (Language == Language.TH ? cBrand.TitleTH : cBrand.TitleEN);
            model.ModelTitle = (Language == Language.TH ? cModel.TitleTH : cBrand.TitleEN);

            ProductRepository repo = new ProductRepository();
            model.Products = repo.ListProductMotorCompare_Active(Language, idList);

            Session["compare"] = DateTime.Now; // Create session id
            ProductCompareLog log = new ProductCompareLog();
            log.SessionId = Session.SessionID;
            log.CreateDate = DateTime.Now;
            if (model.Products.Count > 0) { log.ProductId1 = model.Products[0].Id; log.PremiumId1 = model.Products[0].PremiumId; }
            if (model.Products.Count > 1) { log.ProductId2 = model.Products[1].Id; log.PremiumId2 = model.Products[1].PremiumId; }
            if (model.Products.Count > 2) { log.ProductId3 = model.Products[2].Id; log.PremiumId3 = model.Products[2].PremiumId; }
            ProductCompareLogRepository logRepo = new ProductCompareLogRepository();
            logRepo.AddProductCompare(log);

            return View(Language.ToString() + "/MotorCompare", model);
        }

        #endregion


        #region Compulsory

        // GET: Product/Compulsory-Overview
        [ActionName("compulsory-overview")]
        public ActionResult CompulsoryOverview() {
            return View(Language.ToString() + "/CompulsoryOverview");
        }

        // GET: Product/Compulsory
        public ActionResult Compulsory(ProductCompulsoryViewModel item) {
            item.PathCompany = PathCompany;
            if (item.SubCategory == 0) item.SubCategory = 210;
            //ProductRepository repo = new ProductRepository();
            //item.Companies = repo.ListProductPRBCompany_Active(Language, (int)ProductCategoryKey.PRB);
            return View(Language.ToString() + "/Compulsory", item);
        }

        // GET: Product/CompulsoryDetail/1/2
        public ActionResult CompulsoryDetail(ProductCompulsoryViewModel item) {
            ViewBag.LeadForm = GetLeadForm();
            ViewBag.ShowLeadForm = ShowLeadForm();
            ProductRepository repo = new ProductRepository();

            ProductItem model = repo.ListProductPRB_Active(Language, 1, 1, (int)item.SubCategory, item.Company, "Id DESC").FirstOrDefault();
            if (model == null) return RedirectToAction("Compulsory", new { company = item.Company, subcategory = item.SubCategory });
            model.CompanyImage = PathCompany + model.CompanyImage;
            return View(Language.ToString() + "/CompulsoryDetail", model);
        }

        //private decimal GetPRBPrice(int Usage) {
        //    ConfigRepository configRepo = new ConfigRepository();
        //    switch (Usage) {
        //        case 110: return NullUtils.cvDec(configRepo.GetConfig(ConfigName.ProductPRB110).Body);
        //        case 120: return NullUtils.cvDec(configRepo.GetConfig(ConfigName.ProductPRB120).Body);
        //        case 210: return NullUtils.cvDec(configRepo.GetConfig(ConfigName.ProductPRB210).Body);
        //        case 320: return NullUtils.cvDec(configRepo.GetConfig(ConfigName.ProductPRB320).Body);
        //    }
        //    return 0;
        //}

        #endregion


        #region Home

        // GET: Product/Home-Overview
        [ActionName("home-overview")]
        public ActionResult HomeOverview() {
            return View(Language.ToString() + "/HomeOverview");
        }

        // GET: Product/Home
        public ActionResult Home(ProductHomeViewModel item) {
            ViewBag.ListPage = NullUtils.cvInt(Request.QueryString["listpage"]);
            ViewBag.ShowLeadForm = ShowLeadForm();
            item.PathCompany = PathCompany;

            return View(Language.ToString() + "/Home", item);
        }

        // GET: Product/Home
        public ActionResult HomeList() {
            ProductHomeViewModel model = new ProductHomeViewModel();
            model.PathCompany = PathCompany;
            model.LayoutGridView = NullUtils.cvBoolean(Request.Params["LayoutGridView"]);
            model.PropertyType = NullUtils.cvInt(Request.Params["PropertyType"]);
            model.ConstructionType = NullUtils.cvInt(Request.Params["ConstructionType"]);
            model.Year = NullUtils.cvInt(Request.Params["Year"]);
            model.PropertyValue = NullUtils.cvInt(Request.Params["PropertyValue"]);
            model.ConstructionValue = NullUtils.cvInt(Request.Params["ConstructionValue"]);

            int pageNo = GetPageNo();
            int sortId = NullUtils.cvInt(Request.Params["sort"]);
            string sort = "Premium ASC";
            switch (sortId) {
                //case 1: sort = "Id DESC"; break;
                case 2: sort = "Premium DESC"; break;
                case 3: sort = "Company ASC, Premium ASC"; break;
                case 4: sort = "Company DESC, Premium ASC"; break;
            }

            int listPage = NullUtils.cvInt(Request.QueryString["listpage"]);
            if (listPage > 0) {
                ProductListSize = ProductListSize * (listPage + 1);
                pageNo = 1;
            }

            ProductRepository repo = new ProductRepository();
            var product = repo.ListProductHome_Active(Language, pageNo, ProductListSize, model.ConstructionType, model.PropertyType, model.Year, model.ConstructionValue, model.PropertyValue, sort);

            model.Products = product;

            if (listPage > 0) { pageNo = listPage; }

            ViewBag.Page = pageNo;
            ViewBag.Count = product.Count;

            return PartialView(Language.ToString() + "/HomeList", model);
        }

        // GET: Product/HomeDetail/1/2
        public ActionResult HomeDetail(int ProductId, int PremiumId) {
            ProductRepository repo = new ProductRepository();

            ProductItem item = repo.GetProductHome_Active(Language, ProductId, PremiumId);
            if (item == null) return RedirectToAction("Home");
            item.Id = ProductId;
            item.CompanyImage = PathCompany + item.CompanyImage;
            item.CategoryId = ProductCategoryKey.Home;
            ViewBag.LeadForm = GetLeadForm();
            return View(Language.ToString() + "/HomeDetail", item);
        }

        // GET: Product/HomeCompare
        public ActionResult HomeCompare() {
            string[] idList = Request.Params["id"].Split(',');
            ProductHomeViewModel model = new ProductHomeViewModel();
            model.PathCompany = PathCompany;
            model.PropertyType = NullUtils.cvInt(Request.QueryString["PropertyType"]);
            model.ConstructionType = NullUtils.cvInt(Request.QueryString["ConstructionType"]);

            MasterOptionRepository masterRepo = new MasterOptionRepository();
            var pType = masterRepo.GetMasterOption_Active(model.PropertyType);
            if (pType == null) { return RedirectToAction("Home"); }
            model.PropertyTypeTitle = (Language == Language.TH ? pType.TitleTH : pType.TitleEN);
            var cType = masterRepo.GetMasterOption_Active(model.ConstructionType);
            if (cType == null) { return RedirectToAction("Home"); }
            model.ConstructionTypeTitle = (Language == Language.TH ? cType.TitleTH : cType.TitleEN);

            ProductRepository repo = new ProductRepository();
            model.Products = repo.ListProductHomeCompare_Active(Language, idList);

            Session["compare"] = DateTime.Now; // Create session id
            ProductCompareLog log = new ProductCompareLog();
            log.SessionId = Session.SessionID;
            log.CreateDate = DateTime.Now;
            if (model.Products.Count > 0) { log.ProductId1 = model.Products[0].Id; log.PremiumId1 = model.Products[0].PremiumId; }
            if (model.Products.Count > 1) { log.ProductId2 = model.Products[1].Id; log.PremiumId2 = model.Products[1].PremiumId; }
            if (model.Products.Count > 2) { log.ProductId3 = model.Products[2].Id; log.PremiumId3 = model.Products[2].PremiumId; }
            ProductCompareLogRepository logRepo = new ProductCompareLogRepository();
            logRepo.AddProductCompare(log);

            return View(Language.ToString() + "/HomeCompare", model);
        }

        #endregion


        #region PA

        // GET: Product/PA-Overview
        [ActionName("pa-overview")]
        public ActionResult PAOverview() {
            return View(Language.ToString() + "/PAOverview");
        }

        // GET: Product/PA
        public ActionResult PA(ProductPAViewModel item) {
            ViewBag.ListPage = NullUtils.cvInt(Request.QueryString["listpage"]);
            ViewBag.ShowLeadForm = ShowLeadForm();
            item.PathCompany = PathCompany;
            var prices = ValueUtils.GetIntRange(item.Price, MainLayoutViewModel.ProductPAPriceMin, MainLayoutViewModel.ProductPAPriceMax, ';');
            item.PriceFrom = prices.Item1;
            item.PriceTo = prices.Item2;

            return View(Language.ToString() + "/PA", item);
        }

        // GET: Product/PA
        public ActionResult PAList() {
            ProductPAViewModel model = new ProductPAViewModel();
            model.PathCompany = PathCompany;
            model.LayoutGridView = NullUtils.cvBoolean(Request.Params["LayoutGridView"]);
            string birthdate = "01/" + Request.Params["birthday"];
            DateTime bd;
            if (DateTime.TryParseExact(birthdate, "dd/MM/yyyy", cultureInfoEN, System.Globalization.DateTimeStyles.None, out bd)) {
                model.BirthDay = bd;
            }
            model.Career = NullUtils.cvInt(Request.Params["career"]);
            var prices = ValueUtils.GetIntRange(Request.Params["price"], MainLayoutViewModel.ProductPAPriceMin, MainLayoutViewModel.ProductPAPriceMax, ';');
            model.PriceFrom = prices.Item1;
            model.PriceTo = prices.Item2;

            int pageNo = GetPageNo();
            int sortId = NullUtils.cvInt(Request.Params["sort"]);
            string sort = "Premium ASC";
            switch (sortId) {
                //case 1: sort = "Id DESC"; break;
                case 2: sort = "Premium DESC"; break;
                case 3: sort = "Company ASC, Premium ASC"; break;
                case 4: sort = "Company DESC, Premium ASC"; break;
            }

            int listPage = NullUtils.cvInt(Request.QueryString["listpage"]);
            if (listPage > 0) {
                ProductListSize = ProductListSize * (listPage + 1);
                pageNo = 1;
            }

            ProductRepository repo = new ProductRepository();
            var product = repo.ListProductPA_Active(Language, pageNo, ProductListSize, model.BirthDay, model.Career, model.PriceFrom, model.PriceTo, sort);

            model.Products = product;

            if (listPage > 0) { pageNo = listPage; }

            ViewBag.Page = pageNo;
            ViewBag.Count = product.Count;

            return PartialView(Language.ToString() + "/PAList", model);
        }

        // GET: Product/PADetail/1/2
        public ActionResult PADetail(int ProductId, int PremiumId) {
            ProductRepository repo = new ProductRepository();

            ProductItem item = repo.GetProductPA_Active(Language, ProductId, PremiumId);
            if (item == null) return RedirectToAction("PA");
            item.Id = ProductId;
            item.CompanyImage = PathCompany + item.CompanyImage;
            item.CategoryId = ProductCategoryKey.PA;
            ViewBag.LeadForm = GetLeadForm();
            return View(Language.ToString() + "/PADetail", item);
        }

        // GET: Product/PACompare
        public ActionResult PACompare() {
            string[] idList = Request.Params["id"].Split(',');
            ProductPAViewModel model = new ProductPAViewModel();
            model.PathCompany = PathCompany;
            model.Career = NullUtils.cvInt(Request.QueryString["career"]);

            MasterOptionRepository masterRepo = new MasterOptionRepository();
            var career = masterRepo.GetMasterOption_Active(model.Career);
            if (career == null) { return RedirectToAction("PA"); }
            model.CareerTitle = (Language == Language.TH ? career.TitleTH : career.TitleEN);

            ProductRepository repo = new ProductRepository();
            model.Products = repo.ListProductPACompare_Active(Language, idList);

            Session["compare"] = DateTime.Now; // Create session id
            ProductCompareLog log = new ProductCompareLog();
            log.SessionId = Session.SessionID;
            log.CreateDate = DateTime.Now;
            if (model.Products.Count > 0) { log.ProductId1 = model.Products[0].Id; log.PremiumId1 = model.Products[0].PremiumId; }
            if (model.Products.Count > 1) { log.ProductId2 = model.Products[1].Id; log.PremiumId2 = model.Products[1].PremiumId; }
            if (model.Products.Count > 2) { log.ProductId3 = model.Products[2].Id; log.PremiumId3 = model.Products[2].PremiumId; }
            ProductCompareLogRepository logRepo = new ProductCompareLogRepository();
            logRepo.AddProductCompare(log);

            return View(Language.ToString() + "/PACompare", model);
        }

        #endregion


        #region PH

        // GET: Product/Health-Overview
        [ActionName("health-overview")]
        public ActionResult HealthOverview() {
            return View(Language.ToString() + "/PHOverview");
        }

        // GET: Product/Health
        public ActionResult Health(ProductPHViewModel item) {
            ViewBag.ListPage = NullUtils.cvInt(Request.QueryString["listpage"]);
            ViewBag.ShowLeadForm = ShowLeadForm();
            item.PathCompany = PathCompany;
            var prices = ValueUtils.GetIntRange(item.Price, MainLayoutViewModel.ProductPHPriceMin, MainLayoutViewModel.ProductPHPriceMax, ';');
            item.PriceFrom = prices.Item1;
            item.PriceTo = prices.Item2;

            return View(Language.ToString() + "/PH", item);
        }

        // GET: Product/HealthList
        public ActionResult HealthList() {
            ProductPHViewModel model = new ProductPHViewModel();
            model.PathCompany = PathCompany;
            model.LayoutGridView = NullUtils.cvBoolean(Request.Params["LayoutGridView"]);
            string birthdate = "01/" + Request.Params["birthday"];
            DateTime bd;
            if (DateTime.TryParseExact(birthdate, "dd/MM/yyyy", cultureInfoEN, System.Globalization.DateTimeStyles.None, out bd)) {
                model.BirthDay = bd;
            }
            model.Career = NullUtils.cvInt(Request.Params["career"]);
            model.Sex = (NullUtils.cvInt(Request.Params["sex"]) == (int)Sex.Male ? Sex.Male : Sex.Female);
            model.Weight = NullUtils.cvInt(Request.Params["weight"]);
            model.Height = NullUtils.cvInt(Request.Params["height"]);
            var prices = ValueUtils.GetIntRange(Request.Params["price"], MainLayoutViewModel.ProductPHPriceMin, MainLayoutViewModel.ProductPHPriceMax, ';');
            model.PriceFrom = prices.Item1;
            model.PriceTo = prices.Item2;

            int pageNo = GetPageNo();
            int sortId = NullUtils.cvInt(Request.Params["sort"]);
            string sort = "Premium ASC";
            switch (sortId) {
                //case 1: sort = "Id DESC"; break;
                case 2: sort = "Premium DESC"; break;
                case 3: sort = "Company ASC, Premium ASC"; break;
                case 4: sort = "Company DESC, Premium ASC"; break;
            }

            int listPage = NullUtils.cvInt(Request.QueryString["listpage"]);
            if (listPage > 0) {
                ProductListSize = ProductListSize * (listPage + 1);
                pageNo = 1;
            }

            ProductRepository repo = new ProductRepository();
            var product = repo.ListProductPH_Active(Language, pageNo, ProductListSize, model.BirthDay, model.Career, (int)model.Sex, model.Weight, model.Height, model.PriceFrom, model.PriceTo, sort);

            model.Products = product;

            if (listPage > 0) { pageNo = listPage; }

            ViewBag.Page = pageNo;
            ViewBag.Count = product.Count;

            return PartialView(Language.ToString() + "/PHList", model);
        }

        // GET: Product/HealthDetail/1/2
        public ActionResult HealthDetail(int ProductId, int PremiumId) {
            ProductRepository repo = new ProductRepository();

            ProductItem item = repo.GetProductPH_Active(Language, ProductId, PremiumId);
            if (item == null) return RedirectToAction("Health");
            item.Id = ProductId;
            item.CompanyImage = PathCompany + item.CompanyImage;
            item.CategoryId = ProductCategoryKey.PH;
            ViewBag.LeadForm = GetLeadForm();
            return View(Language.ToString() + "/PHDetail", item);
        }

        // GET: Product/HealthCompare
        public ActionResult HealthCompare() {
            string[] idList = Request.Params["id"].Split(',');
            ProductPHViewModel model = new ProductPHViewModel();
            model.PathCompany = PathCompany;
            model.Career = NullUtils.cvInt(Request.QueryString["career"]);
            model.Sex = (NullUtils.cvInt(Request.Params["sex"]) == (int)Sex.Male ? Sex.Male : Sex.Female);
            model.Weight = NullUtils.cvInt(Request.Params["weight"]);
            model.Height = NullUtils.cvInt(Request.Params["height"]);

            MasterOptionRepository masterRepo = new MasterOptionRepository();
            var career = masterRepo.GetMasterOption_Active(model.Career);
            if (career == null) { return RedirectToAction("PH"); }
            model.CareerTitle = (Language == Language.TH ? career.TitleTH : career.TitleEN);

            ProductRepository repo = new ProductRepository();
            model.Products = repo.ListProductPHCompare_Active(Language, idList);

            Session["compare"] = DateTime.Now; // Create session id
            ProductCompareLog log = new ProductCompareLog();
            log.SessionId = Session.SessionID;
            log.CreateDate = DateTime.Now;
            if (model.Products.Count > 0) { log.ProductId1 = model.Products[0].Id; log.PremiumId1 = model.Products[0].PremiumId; }
            if (model.Products.Count > 1) { log.ProductId2 = model.Products[1].Id; log.PremiumId2 = model.Products[1].PremiumId; }
            if (model.Products.Count > 2) { log.ProductId3 = model.Products[2].Id; log.PremiumId3 = model.Products[2].PremiumId; }
            ProductCompareLogRepository logRepo = new ProductCompareLogRepository();
            logRepo.AddProductCompare(log);

            return View(Language.ToString() + "/PHCompare", model);
        }

        #endregion


        #region TA

        // GET: Product/TA-Overview
        [ActionName("ta-overview")]
        public ActionResult TAOverview() {
            return View(Language.ToString() + "/TAOverview");
        }

        // GET: Product/TA
        public ActionResult TA(ProductTAViewModel item) {
            ViewBag.ListPage = NullUtils.cvInt(Request.QueryString["listpage"]);
            ViewBag.ShowLeadForm = ShowLeadForm();
            item.PathCompany = PathCompany;
            var prices = ValueUtils.GetIntRange(item.Price, MainLayoutViewModel.ProductTAPriceMin, MainLayoutViewModel.ProductTAPriceMax, ';');
            item.PriceFrom = prices.Item1;
            item.PriceTo = prices.Item2;
            if (item.TripType == (int)ProductTATripType.Domestic) item.Country = MainLayoutViewModel.CountryThailand;
            if (item.CoverageType == (int)ProductTACoverageType.Family) item.Person = 4;

            var dateFrom = ValueUtils.GetDate(item.DateFrom);
            var dateTo = ValueUtils.GetDate(item.DateTo);
            if (dateFrom.Year > 1753 && dateTo.Year > 1753) item.Day = (dateTo - dateFrom).Days + 1;

            return View(Language.ToString() + "/TA", item);
        }

        // GET: Product/TA
        public ActionResult TAList() {
            ProductTAViewModel model = new ProductTAViewModel();
            model.PathCompany = PathCompany;
            model.LayoutGridView = NullUtils.cvBoolean(Request.Params["LayoutGridView"]);

            model.TripType = NullUtils.cvInt(Request.Params["TripType"]);
            model.CoverageType = NullUtils.cvInt(Request.Params["CoverageType"]);
            model.CoverageOption = NullUtils.cvInt(Request.Params["CoverageOption"]);
            model.Person = NullUtils.cvInt(Request.Params["Person"]);
            model.Day = NullUtils.cvInt(Request.Params["Day"]);
            model.DateFrom = NullUtils.cvString(Request.Params["DateFrom"]);
            model.DateTo = NullUtils.cvString(Request.Params["DateTo"]);
            model.Country = NullUtils.cvInt(Request.Params["Country"]);
            model.Price = Request.Params["price"];
            var prices = ValueUtils.GetIntRange(Request.Params["price"], MainLayoutViewModel.ProductTAPriceMin, MainLayoutViewModel.ProductTAPriceMax, ';');
            model.PriceFrom = prices.Item1;
            model.PriceTo = prices.Item2;
            model.Zone = (model.IsSchengen ? (int)CountryZone.Schengen : 0);
            if (model.TripType == (int)ProductTATripType.Domestic) model.Country = MainLayoutViewModel.CountryThailand;
            if (model.CoverageType == (int)ProductTACoverageType.Family) model.Person = 4;
            bool checkPerson = (model.CoverageType != (int)ProductTACoverageType.Family);

            int pageNo = GetPageNo();
            int sortId = NullUtils.cvInt(Request.Params["sort"]);
            string sort = "Premium ASC";
            switch (sortId) {
                //case 1: sort = "Id DESC"; break;
                case 2: sort = "Premium DESC"; break;
                case 3: sort = "Company ASC, Premium ASC"; break;
                case 4: sort = "Company DESC, Premium ASC"; break;
            }

            int listPage = NullUtils.cvInt(Request.QueryString["listpage"]);
            if (listPage > 0) {
                ProductListSize = ProductListSize * (listPage + 1);
                pageNo = 1;
            }

            ProductRepository repo = new ProductRepository();
            var product = repo.ListProductTA_Active(Language, pageNo, ProductListSize, model.TripType, model.Country, model.Zone, model.CoverageOption, model.CoverageType, model.Day, model.Person, model.PriceFrom, model.PriceTo, checkPerson, sort);

            model.Products = product;

            if (listPage > 0) { pageNo = listPage; }

            ViewBag.Page = pageNo;
            ViewBag.Count = product.Count;

            return PartialView(Language.ToString() + "/TAList", model);
        }

        // GET: Product/TADetail/1/2
        public ActionResult TADetail(int ProductId, int PremiumId) {
            ProductRepository repo = new ProductRepository();

            ProductItem item = repo.GetProductTA_Active(Language, ProductId, PremiumId);
            if (item == null) return RedirectToAction("TA");
            item.Id = ProductId;
            item.CompanyImage = PathCompany + item.CompanyImage;
            item.CategoryId = ProductCategoryKey.TA;
            ViewBag.LeadForm = GetLeadForm();
            return View(Language.ToString() + "/TADetail", item);
        }

        // GET: Product/TACompare
        public ActionResult TACompare() {
            string[] idList = Request.Params["id"].Split(',');
            ProductTAViewModel model = new ProductTAViewModel();
            model.PathCompany = PathCompany;

            model.TripType = NullUtils.cvInt(Request.Params["TripType"]);
            model.CoverageType = NullUtils.cvInt(Request.Params["CoverageType"]);
            model.CoverageOption = NullUtils.cvInt(Request.Params["CoverageOption"]);
            model.Person = NullUtils.cvInt(Request.Params["Person"]);
            model.Day = NullUtils.cvInt(Request.Params["Day"]);
            model.DateFrom = NullUtils.cvString(Request.Params["DateFrom"]);
            model.DateTo = NullUtils.cvString(Request.Params["DateTo"]);
            model.Country = NullUtils.cvInt(Request.Params["Country"]);
            model.Price = Request.Params["price"];
            var prices = ValueUtils.GetIntRange(Request.Params["price"], MainLayoutViewModel.ProductTAPriceMin, MainLayoutViewModel.ProductTAPriceMax, ';');
            model.PriceFrom = prices.Item1;
            model.PriceTo = prices.Item2;

            ProductRepository repo = new ProductRepository();
            model.Products = repo.ListProductTACompare_Active(Language, idList);

            Session["compare"] = DateTime.Now; // Create session id
            ProductCompareLog log = new ProductCompareLog();
            log.SessionId = Session.SessionID;
            log.CreateDate = DateTime.Now;
            if (model.Products.Count > 0) { log.ProductId1 = model.Products[0].Id; log.PremiumId1 = model.Products[0].PremiumId; }
            if (model.Products.Count > 1) { log.ProductId2 = model.Products[1].Id; log.PremiumId2 = model.Products[1].PremiumId; }
            if (model.Products.Count > 2) { log.ProductId3 = model.Products[2].Id; log.PremiumId3 = model.Products[2].PremiumId; }
            ProductCompareLogRepository logRepo = new ProductCompareLogRepository();
            logRepo.AddProductCompare(log);

            return View(Language.ToString() + "/TACompare", model);
        }

        #endregion

        #region Lead

        // POST: Product/LeadForm
        public ActionResult LeadForm() {
            bool success = false;
            string message = string.Empty;
            if (Session != null && NullUtils.cvInt(Session["LeadFormId"]) > 0) {
                success = true;
            } else {
                LeadFormRepository leadRepo = new LeadFormRepository();
                LeadForm model = new TobJod.Models.LeadForm();
                model.Name = Request.Params["Name"];
                model.Surname = Request.Params["Surname"];
                model.Tel = Request.Params["Tel"];
                model.Email = Request.Params["Email"];
                model.Status = Status.Active;
                model.CreateDate = DateTime.Now;
                if (Session == null) Session["LeadFormId"] = "";
                model.SessionId = Session.SessionID;
                model.IPPublic = HttpUtils.GetPublicIP();
                model.IPPrivate = HttpUtils.GetPrivateIP();
                model.CreateDate = DateTime.Now;

                var check = leadRepo.GetLeadFormExists(model);

                if (TryValidateModel(model)) {
                    if (check == null) {
                        var result = leadRepo.AddLeadForm(model);
                        if (result > 0) { success = true; Session["LeadFormId"] = result; }
                    } else {
                        Session["LeadFormId"] = check.Id;
                        success = true;
                    }
                } else {
                    success = false;
                }
            }

            return Json(new { success = success, message = message });
        }

        private bool ShowLeadForm() {
            var check = GetLeadForm();
            return (check == null);
        }

        private LeadForm GetLeadForm() {
            LeadFormRepository leadRepo = new LeadFormRepository();
            if (Session != null && NullUtils.cvInt(Session["LeadFormId"]) > 0) return leadRepo.GetLeadForm_Active(NullUtils.cvInt(Session["LeadFormId"]));
            LeadForm model = new TobJod.Models.LeadForm();
            model.SessionId = Session.SessionID;
            model.IPPublic = HttpUtils.GetPublicIP();
            model.IPPrivate = HttpUtils.GetPrivateIP();
            model.CreateDate = DateTime.Now;
            var check = leadRepo.GetLeadFormExists(model);
            if (check != null) Session["LeadFormId"] = model.Id;
            return check;
        }
        #endregion

        // POST: Product/Save
        //[PreserveQueryString]
        public ActionResult Save() {
            //ReCaptcha
            bool status = ValidateReCaptcha();

            int productId = NullUtils.cvInt(Request.Form["ProductId"]);
            int premiumId = NullUtils.cvInt(Request.Form["PremiumId"]);

            ProductCategoryKey category = ProductCategoryKey.Motor;
            try { category = (ProductCategoryKey)Enum.Parse(typeof(ProductCategoryKey), Request.Form["Category"]); } catch { }

            ProductRepository repo = new ProductRepository();
            ProductItem item = repo.GetProduct_Active(Language, productId, premiumId);
            Lead model = new Lead();
            status = (status && item != null);
            if (status) {
                LeadRepository leadRepo = new LeadRepository();
                DateTime d;
                model.Name = Request.Params["Name"];
                model.Surname = Request.Params["Surname"];
                model.Tel = Request.Params["Tel"];
                model.Email = Request.Params["Email"];
                var dr = DateTime.TryParseExact(Request.Params["CallBackDay"] + " " + Request.Params["CallBackTime"], "dd/MM/yyyy HH:mm", cultureInfoEN, System.Globalization.DateTimeStyles.None, out d);
                model.CallBackDate = d;
                model.Message = Request.Params["Message"];
                if (model.Message == null) model.Message = "";
                model.Source = LeadSource.Product;
                model.ProductId = productId;
                model.PremiumId = premiumId;
                model.Remark = "";
                model.LeadStatus = LeadStatus.Pending;
                model.OwnerAdminId = model.AssignAdminId = "";
                model.CreateDate = model.UpdateDate = DateTime.Now;
                model.CreateAdminId = model.UpdateAdminId = "";

                model.LeadCriteria = new LeadCriteria();
                model.LeadCriteria.Birthday = DateUtils.SqlMinDate();
                model.LeadCriteria.SubCategoryId = item.SubCategoryId;

                category = ProductCategory.GetCategoryBySubCategory(item.SubCategoryId);

                var prices = ValueUtils.GetIntRange(Request.QueryString["Price"], MainLayoutViewModel.ProductMotorPriceMin, MainLayoutViewModel.ProductMotorPriceMax, ';');
                model.LeadCriteria.PriceFrom = prices.Item1;
                model.LeadCriteria.PriceTo = prices.Item2;
                switch (category) {
                    case ProductCategoryKey.Motor:
                        model.LeadCriteria.MotorBrandId = NullUtils.cvInt(Request.QueryString["Brand"]);
                        model.LeadCriteria.MotorFamilyId = NullUtils.cvInt(Request.QueryString["Family"]);
                        model.LeadCriteria.MotorModelId = NullUtils.cvInt(Request.QueryString["Model"]);
                        model.LeadCriteria.MotorRegisterProvinceId = NullUtils.cvInt(Request.QueryString["Province"]);
                        model.LeadCriteria.MotorYear = NullUtils.cvInt(Request.QueryString["Year"]);
                        model.LeadCriteria.MotorUsage = NullUtils.cvInt(Request.QueryString["Usage"]);
                        model.LeadCriteria.MotorCCTV = NullUtils.cvInt(Request.QueryString["CCTV"]);
                        model.LeadCriteria.MotorAccessory = NullUtils.cvInt(Request.QueryString["Accessory"]) == 1;
                        model.LeadCriteria.MotorGarage = NullUtils.cvInt(Request.QueryString["Garage"]);
                        model.LeadCriteria.MotorDriverAge = NullUtils.cvInt(Request.QueryString["Driver"]);
                        model.LeadCriteria.MotorDriverAgeId = NullUtils.cvInt(Request.QueryString["DriverAge"]);
                        model.LeadCriteria.MotorPRB = NullUtils.cvInt(Request.QueryString["Compulsory"]) == 1;
                        break;
                    case ProductCategoryKey.Home:
                        model.LeadCriteria.HousePropertyType = NullUtils.cvInt(Request.QueryString["PropertyType"]);
                        model.LeadCriteria.HouseConstructionType = NullUtils.cvInt(Request.QueryString["ConstructionType"]);
                        model.LeadCriteria.HousePropertyValue = NullUtils.cvInt(Request.QueryString["PropertyValue"]);
                        model.LeadCriteria.HouseConstructionValue = NullUtils.cvInt(Request.QueryString["ConstructionValue"]);
                        model.LeadCriteria.HouseYear = NullUtils.cvInt(Request.QueryString["Year"]);
                        break;
                    case ProductCategoryKey.PA:
                        model.LeadCriteria.CareerId = NullUtils.cvInt(Request.QueryString["Career"]);
                        model.LeadCriteria.Birthday = ValueUtils.GetDate("01/" + Request.QueryString["Birthday"]);
                        break;
                    case ProductCategoryKey.PH:
                        model.LeadCriteria.CareerId = NullUtils.cvInt(Request.QueryString["Career"]);
                        model.LeadCriteria.Birthday = ValueUtils.GetDate("01/" + Request.QueryString["Birthday"]);
                        model.LeadCriteria.Sex = NullUtils.cvInt(Request.QueryString["Sex"]);
                        model.LeadCriteria.Weight = NullUtils.cvInt(Request.QueryString["Weight"]);
                        model.LeadCriteria.Height = NullUtils.cvInt(Request.QueryString["Height"]);
                        break;
                    case ProductCategoryKey.TA:
                        model.LeadCriteria.TACountryId = NullUtils.cvInt(Request.QueryString["Country"]);
                        if (model.LeadCriteria.TACountryId == new ProductTAViewModel().SchengenId) {
                            model.LeadCriteria.TACountryId = 0;
                            model.LeadCriteria.TAZone = (int)CountryZone.Schengen;
                        }
                        model.LeadCriteria.TACoverageOption = NullUtils.cvInt(Request.QueryString["CoverageOption"]);
                        model.LeadCriteria.TACoverageType = NullUtils.cvInt(Request.QueryString["CoverageType"]);
                        model.LeadCriteria.TATripType = NullUtils.cvInt(Request.QueryString["TripType"]);
                        model.LeadCriteria.TADays = NullUtils.cvInt(Request.QueryString["Day"]);
                        model.LeadCriteria.TAPerson = NullUtils.cvInt(Request.QueryString["Person"]);
                        break;
                    default: break;
                }

                if (TryValidateModel(model)) {
                    var result = leadRepo.AddLead(model);
                    if (result > 0) {
                        SendMail(model, item);
                        Notify("Success");
                        //ViewBag.Message = "Success";
                        //model.Name = "";
                        //model.Surname = "";
                        //model.Tel = "";
                        //model.Email = "";
                        //model.SubjectId = 0;
                        //model.Message = "";
                        //RedirectToAction("Index");
                    } else {
                        Notify("Error");
                    }
                } else {
                    Notify("Error");
                }

            }
            //string url = "MotorDetail";
            //switch (category) {
            //    case ProductCategoryKey.Home: url = "HomeDetail"; break;
            //    case ProductCategoryKey.PA: url = "PADetail"; break;
            //    case ProductCategoryKey.PH: url = "HealthDetail"; break;
            //    case ProductCategoryKey.TA: url = "TADetail"; break;
            //    case ProductCategoryKey.PRB: url = "CompulsoryDetail"; break;
            //}
            //return RedirectToAction(url);

            //RouteData.Values.Clear();
            //foreach (var q in Request.QueryString.AllKeys) {
            //    RouteData.Values.Remove(q);
            //}
            return RedirectToAction("Index", "Home");
        }

        private void SendMail(Lead model, ProductItem item) {
            string row = "<tr><th>{0}</th><td>{1}<td></tr>";
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<table style='margin=auto;'>");
            if (Language == TobJod.Models.Language.TH) {
                sb.AppendLine(string.Format(row, "ชื่อ-นามสกุล", model.Name + " " + model.Surname));
                sb.AppendLine(string.Format(row, "อีเมล", model.Email));
                sb.AppendLine(string.Format(row, "เบอร์โทรศัพท์", model.Tel));
                sb.AppendLine(string.Format(row, "วันเวลาสำหรับติดต่อกลับ", model.CallBackDate.ToString("dd/MM/yyyy HH:mm", cultureInfoEN) + " - " + model.CallBackDate.AddHours(2).ToString("HH:mm", cultureInfoEN)));
                sb.AppendLine(string.Format(row, "เรื่องที่ต้องการติดต่อ", "สนใจผลิตภัณฑ์"));
                sb.AppendLine(string.Format(row, "ผลิตภัณฑ์", item.TitleTH));
                sb.AppendLine(string.Format(row, "ข้อความ", model.Message));
            } else {
                sb.AppendLine(string.Format(row, "Name", model.Name + " " + model.Surname));
                sb.AppendLine(string.Format(row, "Email", model.Email));
                sb.AppendLine(string.Format(row, "Tel", model.Tel));
                sb.AppendLine(string.Format(row, "Call Back Date", model.CallBackDate.ToString("dd/MM/yyyy HH:mm", cultureInfoEN) + " - " + model.CallBackDate.AddHours(2).ToString("HH:mm", cultureInfoEN)));
                sb.AppendLine(string.Format(row, "Subject", "Interest in Product"));
                sb.AppendLine(string.Format(row, "Product", item.TitleEN));
                sb.AppendLine(string.Format(row, "Message", model.Message));
            }
            sb.AppendLine("</table>");

            ConfigRepository configRepo = new ConfigRepository();

            string template = Server.MapPath("~/assets/mail/" + Language.ToString() + "/product.html");
            string body = FileUtils.ReadText(template);
            string template_blank = Server.MapPath("~/assets/mail/template.html");
            string body_blank = FileUtils.ReadText(template_blank);
            MailMessage message = new MailMessage();
            string configPath = HttpUtils.GetPathUrlAbsolute(System.Configuration.ConfigurationManager.AppSettings["Path_Config"]);
            var configs = configRepo.ListConfigs(new List<ConfigName>() { ConfigName.ProductEmailTo, ConfigName.EmailImageHeader, ConfigName.EmailImageFooter });
            try {
                message.Subject = "Tobjod - Contact Form";
                message.Body = body.Replace("{{base_url}}", HttpUtils.GetBaseUrl())
                                        .Replace("{{email_image_header}}", configPath + configs[ConfigName.EmailImageHeader])
                                        .Replace("{{email_image_footer}}", configPath + configs[ConfigName.EmailImageFooter])
                                    .Replace("{{message}}", sb.ToString());
                message.IsBodyHtml = true;

                message.From = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["Mail_From_Email"], System.Configuration.ConfigurationManager.AppSettings["Mail_From_Name"]);
                
                SmtpClient smtp = new SmtpClient();

                message.To.Add(model.Email);
                try { smtp.Send(message); } catch { }

                message.Body = body_blank.Replace("{{base_url}}", HttpUtils.GetBaseUrl())
                                        .Replace("{{email_image_header}}", configPath + configs[ConfigName.EmailImageHeader])
                                        .Replace("{{email_image_footer}}", configPath + configs[ConfigName.EmailImageFooter])
                                    .Replace("{{message}}", sb.ToString());
                message.To.Clear();
                var emails = configs[ConfigName.ProductEmailTo].Split(';');
                foreach (var mailTo in emails) {
                    if (!string.IsNullOrWhiteSpace(mailTo)) message.To.Add(mailTo.Trim());
                }
                smtp.Send(message);
            } catch { }
        }

        public string ListDayOff() {
            DayOffRepository dayOffRepo = new DayOffRepository();
            var items = dayOffRepo.ListDayOff_Active();
            return string.Join(",", items.Select(x => ("'" + x.Year.ToString() + "-" + x.ToString("MM-dd") + "'")));
        }

        protected AllYesNoFlag GetAllYesNoFlag(string paramName) {
            if (!string.IsNullOrEmpty(Request.Params[paramName])) return NullUtils.cvEnum<AllYesNoFlag>(NullUtils.cvInt(Request.Params[paramName]), AllYesNoFlag.None);

            if (!Request.Params.AllKeys.Contains(paramName + "1") && !Request.Params.AllKeys.Contains(paramName + "2")) {
                return AllYesNoFlag.None;
            }

            AllYesNoFlag flag = AllYesNoFlag.None;
            if (string.Equals(Request.Params.AllKeys.Contains(paramName + "1"), "1")) {
                flag = flag | AllYesNoFlag.Yes;
            }
            if (string.Equals(Request.Params.AllKeys.Contains(paramName + "2"), "1")) {
                flag = flag | AllYesNoFlag.No;
            }
            return flag;
        }
    }
}