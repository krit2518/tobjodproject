﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TobJod.Web.Models;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;
using System.Globalization;
using System.Net;
using Newtonsoft.Json.Linq;

namespace TobJod.Web.Controllers {
    public class BaseController : Controller {

        protected Language Language = Language.TH;
        protected CultureInfo cultureInfo;
        protected CultureInfo cultureInfoEN;
        protected MainLayoutViewModel MainLayoutViewModel { get; set; }
        protected PageMeta PageMeta { get; set; }
        protected bool EnableChaptcha { get; set; }

        public BaseController() {
            cultureInfoEN = new CultureInfo("en-GB");
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext) {
            var language = filterContext.RouteData.Values["language"] as string;

            if (string.Equals(language, "en")) {
                Language = Language.EN;
                cultureInfo = new CultureInfo("en-GB");
            } else {
                cultureInfo = new CultureInfo("th-TH");
            }

            //var userLanguage = HttpContext.Request.UserLanguages;

            //CultureInfo cultureInfo = new CultureInfo(culture ?? (userLanguage != null ? userLanguage[0].Substring(0, 2) : "sv"));

            //Thread.CurrentThread.CurrentUICulture = cultureInfo;
            //Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(cultureInfo.Name);

            BindData();
            base.OnActionExecuting(filterContext);
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext) {
            base.OnActionExecuted(filterContext);
            if (ViewBag.Notify == null && TempData.ContainsKey("Notify")) {
                ViewBag.Notify = TempData["Notify"];
            }
        }


        protected override void OnException(ExceptionContext filterContext) {
            //base.OnException(filterContext);
            string body = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " " + HttpContext.Request.RequestContext.RouteData.Values["controller"].ToString() + " : " + HttpContext.Request.RequestContext.RouteData.Values["action"].ToString() + " : " + filterContext.Exception.ToString();
            FileUtils.SaveText(body,
                string.Format(System.Configuration.ConfigurationManager.AppSettings["Error_Log_Path"], DateTime.Today.ToString("yyyyMMdd", cultureInfoEN)));
        }

        protected void Notify(string message) {
            TempData["Notify"] = message;
            ViewBag.NotifyMessage = message;
        }

        private void BindData() {
            MainLayoutViewModel = new MainLayoutViewModel();
            MainLayoutViewModel.Lanugage = Language;

            ProductCategoryRepository categoryRepo = new ProductCategoryRepository();
            MainLayoutViewModel.ProductCategory = categoryRepo.ListProductCategory_Active(Language);

            CarRepository carRepo = new CarRepository();
            MainLayoutViewModel.CarBrands = carRepo.ListCarBrand_SelectList(Language);

            ProductRepository productRepo = new ProductRepository();
            MainLayoutViewModel.PRBCompanies = productRepo.ListProductPRBCompany_SelectList_Active(Language, (int)ProductCategoryKey.PRB);

            CareerRepository careerRepo = new CareerRepository();
            MainLayoutViewModel.Careers = careerRepo.ListCareer_SelectList_Active(Language);

            CountryRepository countryRepo = new CountryRepository();
            MainLayoutViewModel.Countries = countryRepo.ListCountry_SelectList_ExceptZone_Active(Language, CountryZone.Thailand);
            var countryThai = countryRepo.ListCountry_ByZone_Active(Language, CountryZone.Thailand);
            if (countryThai.Count > 0) MainLayoutViewModel.CountryThailand = countryThai[0].Id;

            MasterOptionRepository masterRepo = new MasterOptionRepository();
            MainLayoutViewModel.CarProvinces = masterRepo.ListMasterOption_SelectList_Active(Language, MasterOptionCategory.MotorProvince);
            MainLayoutViewModel.ConstructionTypes = masterRepo.ListMasterOption_SelectList_Active(Language, MasterOptionCategory.ConstructionType);
            MainLayoutViewModel.PropertyTypes = masterRepo.ListMasterOption_SelectList_Active(Language, MasterOptionCategory.PropertyType);

            PageMeta = new PageMeta();
            ConfigRepository configRepo = new ConfigRepository();
            Dictionary<ConfigName, string> configs;
            string configPath = HttpUtils.GetPathUrlAbsolute(System.Configuration.ConfigurationManager.AppSettings["Path_Config"]);
            if (Language == Language.TH) {
                configs = configRepo.ListConfigs(new List<ConfigName>() { ConfigName.MetaTitleTH, ConfigName.MetaKeywordTH, ConfigName.MetaDescriptionTH, ConfigName.OGTitleTH, ConfigName.OGDescriptionTH, ConfigName.OGImage, ConfigName.ProductMotorPriceMin, ConfigName.ProductMotorPriceMax, ConfigName.ProductPAPriceMin, ConfigName.ProductPAPriceMax, ConfigName.ProductPHPriceMin, ConfigName.ProductPHPriceMax, ConfigName.ProductTAPriceMin, ConfigName.ProductTAPriceMax, ConfigName.ProductTAPersonMax, ConfigName.ProductHomeYearMax, ConfigName.ContactAddressTH, ConfigName.ContactEmailTH, ConfigName.ContactFaxTH, ConfigName.ContactTelephoneTH });
                PageMeta.Title = configs[ConfigName.MetaTitleTH];
                PageMeta.MetaDescription = configs[ConfigName.MetaKeywordTH];
                PageMeta.MetaDescription = configs[ConfigName.MetaDescriptionTH];
                PageMeta.OGTitle = configs[ConfigName.OGTitleTH];
                PageMeta.OGDescription = configs[ConfigName.OGDescriptionTH];
                PageMeta.OGImage = configPath + configs[ConfigName.OGImage];
                MainLayoutViewModel.ContactAddressTH = configs[ConfigName.ContactAddressTH];
                MainLayoutViewModel.ContactTelephoneTH = configs[ConfigName.ContactTelephoneTH];
                MainLayoutViewModel.ContactFaxTH = configs[ConfigName.ContactFaxTH];
                MainLayoutViewModel.ContactEmailTH = configs[ConfigName.ContactEmailTH];
            } else {
                configs = configRepo.ListConfigs(new List<ConfigName>() { ConfigName.MetaTitleEN, ConfigName.MetaKeywordEN, ConfigName.MetaDescriptionEN, ConfigName.OGTitleEN, ConfigName.OGDescriptionEN, ConfigName.OGImage, ConfigName.ProductMotorPriceMin, ConfigName.ProductMotorPriceMax, ConfigName.ProductPAPriceMin, ConfigName.ProductPAPriceMax, ConfigName.ProductPHPriceMin, ConfigName.ProductPHPriceMax, ConfigName.ProductTAPriceMin, ConfigName.ProductTAPriceMax, ConfigName.ProductTAPersonMax, ConfigName.ProductHomeYearMax, ConfigName.ContactAddressEN, ConfigName.ContactEmailEN, ConfigName.ContactFaxEN, ConfigName.ContactTelephoneEN });
                PageMeta.Title = configs[ConfigName.MetaTitleEN];
                PageMeta.MetaDescription = configs[ConfigName.MetaKeywordEN];
                PageMeta.MetaDescription = configs[ConfigName.MetaDescriptionEN];
                PageMeta.OGTitle = configs[ConfigName.OGTitleEN];
                PageMeta.OGDescription = configs[ConfigName.OGDescriptionEN];
                PageMeta.OGImage = configPath + configs[ConfigName.OGImage];
                MainLayoutViewModel.ContactAddressEN = configs[ConfigName.ContactAddressEN];
                MainLayoutViewModel.ContactTelephoneEN = configs[ConfigName.ContactTelephoneEN];
                MainLayoutViewModel.ContactFaxEN = configs[ConfigName.ContactFaxEN];
                MainLayoutViewModel.ContactEmailEN = configs[ConfigName.ContactEmailEN];
            }

            EnableChaptcha = NullUtils.cvBoolean(System.Configuration.ConfigurationManager.AppSettings["Captcha_Enable"]); // NullUtils.cvBoolean(configs[ConfigName.EnableCaptcha]);

            ViewBag.PageMeta = PageMeta;

            MainLayoutViewModel.ProductMotorPriceMin = NullUtils.cvInt(configs[ConfigName.ProductMotorPriceMin]);
            MainLayoutViewModel.ProductMotorPriceMax = NullUtils.cvInt(configs[ConfigName.ProductMotorPriceMax]);
            MainLayoutViewModel.ProductPAPriceMin = NullUtils.cvInt(configs[ConfigName.ProductPAPriceMin]);
            MainLayoutViewModel.ProductPAPriceMax = NullUtils.cvInt(configs[ConfigName.ProductPAPriceMax]);
            MainLayoutViewModel.ProductPHPriceMin = NullUtils.cvInt(configs[ConfigName.ProductPHPriceMin]);
            MainLayoutViewModel.ProductPHPriceMax = NullUtils.cvInt(configs[ConfigName.ProductPHPriceMax]);
            MainLayoutViewModel.ProductTAPriceMin = NullUtils.cvInt(configs[ConfigName.ProductTAPriceMin]);
            MainLayoutViewModel.ProductTAPriceMax = NullUtils.cvInt(configs[ConfigName.ProductTAPriceMax]);
            MainLayoutViewModel.ProductTAPersonMax = NullUtils.cvInt(configs[ConfigName.ProductTAPersonMax]);
            MainLayoutViewModel.ProductHomeYearMax = NullUtils.cvInt(configs[ConfigName.ProductHomeYearMax]);

            MainLayoutViewModel.EnableCaptcha = EnableChaptcha; //NullUtils.cvBoolean(configs[ConfigName.EnableCaptcha]);
            if (Session["UserAccount"] != null)
            {
                MainLayoutViewModel.EnableMember = true;
            }

            ViewBag.MainLayoutViewModel = MainLayoutViewModel;
        }

        protected int GetPageNo(string key = "p") {
            int page = NullUtils.cvInt(Request.Params[key]);
            return Math.Max(1, page);
        }

        protected string ReplaceTextEditorPathDisplay(string val) {
            return string.Format(val, HttpUtils.GetPathRelative("~")).Replace("//", "/");
        }

        protected bool ValidateReCaptcha() {
            return true;
            //if (EnableChaptcha)
            //{
            //    bool status = false;
            //    try
            //    {
            //        var response = Request["g-recaptcha-response"];
            //        string secretKey = System.Configuration.ConfigurationManager.AppSettings["Captcha_Key"];// "6LfcwisUAAAAAK46BLY26QSnK5mh4eR5brenoeuR";
            //        var client = new WebClient();
            //        var cresult = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secretKey, response));
            //        var obj = JObject.Parse(cresult);
            //        status = (bool)obj.SelectToken("success");
            //    }
            //    catch { }
            //    return status;
            //}
            //else
            //{
            //    return !EnableChaptcha;
            //}
        }
    }
}