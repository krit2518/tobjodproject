﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Web.Controllers {
    public class POIController : BaseController {

        // GET: POI
        public ActionResult Index() {
            //ConfigPageRepository repo = new ConfigPageRepository();
            //var model = repo.GetConfigPage(TobJod.Models.ConfigPageName.POI);
            //PageMeta.FromConfigPage(model, Language);
            PoiRepository repo = new PoiRepository();
            ViewBag.POIGarage = repo.ListPoi_Active(Language, (int)PoiCategory.Garage);
            ViewBag.POIHospital = repo.ListPoi_Active(Language, (int)PoiCategory.Hospital);
            ViewBag.POIPolice = repo.ListPoi_Active(Language, (int)PoiCategory.Police);
            return View(Language.ToString() + "/Index");//, model);
        }


        // GET: POI/List
        public ActionResult List() {
            int category = NullUtils.cvInt(Request.Form["c"]);
            PoiRepository repo = new PoiRepository();
            var model = repo.ListPoi_Active(Language, category);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

    }
}