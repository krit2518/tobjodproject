﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Service;
using TobJod.Utils;
using TobJod.Web.Models;


namespace TobJod.Web.Controllers
{
    public class MemberController : BaseController
    {
        private string GoogleClientId = ConfigurationManager.AppSettings["Google.ClientID"];
        private string GoogleSecretKey = ConfigurationManager.AppSettings["Google.SecretKey"];
        private string GoogleRedirectUrl = ConfigurationManager.AppSettings["Google.RedirectUrl"];
        private string FacebookClientId = ConfigurationManager.AppSettings["Facebook.ClientID"];
        private string FacebookSecretKey = ConfigurationManager.AppSettings["Facebook.SecretKey"];
        private string FacebookRedirectUrl = ConfigurationManager.AppSettings["Facebook.RedirectUrl"];
        private string VerifyEmai = ConfigurationManager.AppSettings["VerifyEmai"];
        // GET: Member
        public ActionResult Index()
        {
            LoginViewModel model = BindForm(new LoginViewModel());
            return View(Language.ToString() + "/Index", model);
        }
        public ActionResult Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                MemberRepository LoginRepo = new MemberRepository();
                Login obj = new Login();
                obj.Email = model.Email;
                obj.Password = model.Password;
                obj.Rememberme = model.Rememberme;
                var result = LoginRepo.LogIn(obj);
                if (result != null)
                {

                    ViewBag.Notify = "เข้าสู่ระบบสำเร็จ";
                    Notify("เข้าสู่ระบบสำเร็จ");
                    Session["UserAccount"] = "";

                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ViewBag.Notify = "ไม่สามารถเข้าสู่ระบบได้ กรุณาลองใหม่อีกครั้ง";
                    Notify("ไม่สามารถเข้าสู่ระบบได้ กรุณาลองใหม่อีกครั้ง");
                }
            }
            else
            {
                ViewBag.Notify = "ไม่สามารถเข้าสู่ระบบได้ กรุณาลองใหม่อีกครั้ง";
                Notify("ไม่สามารถเข้าสู่ระบบได้ กรุณาลองใหม่อีกครั้ง");
            }
            model = BindForm(model);
            return View(Language.ToString() + "/Index", model);
        }
        public ActionResult Register()
        {
            //  LoginViewModel model = BindForm(new LoginViewModel());
            LoginViewModel model = BindForm(new LoginViewModel());

            return View(Language.ToString() + "/Register", model);

        }
        [ActionName("registered")]
        public ActionResult Registered()
        {
            RegisterViewModel model = BindForm(new RegisterViewModel());
            return View(Language.ToString() + "/Registered", model);
        }
        public ActionResult SaveRegister(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (string.IsNullOrEmpty(model.Password) || string.IsNullOrEmpty(model.ConfirmPassword))
                {
                    ViewBag.Notify = "รหัสผ่านไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง";
                    return View(Language.ToString() + "/Registered", model);
                }
                if (model.Password.Trim() != model.ConfirmPassword.Trim())
                {
                    ViewBag.Notify = "รหัสผ่านไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง";
                    return View(Language.ToString() + "/Registered", model);
                }
                MemberRepository MemberRepo = new MemberRepository();
                Register obj = new Register();

                obj.Email = model.Email;
                obj.Member_Password = SecurityUtils.GenerateSHA256String(model.Password);
                obj.LoginRefId = "";
                obj.LoginChannel = 1;
                obj.Name = model.Name;
                obj.Surname = model.Surname;
                obj.Telephone = "";
                obj.Sex = 1;
                obj.Birthday = DateTime.Now;
                obj.MaritalStatus = 1;
                obj.CareerId = 1;
                obj.NationalId = 1;
                obj.Weight = 0;
                obj.Height = 0;
                obj.Image = "";
                obj.AddressNo = "";
                obj.AddressMoo = "";
                obj.AddressVillage = "";
                obj.AddressFloor = "";
                obj.AddressSoi = "";
                obj.AddressRoad = "";
                obj.AddressDistrictId = 1;
                obj.AddressSubDistrictId = 1;
                obj.AddressProvinceId = 1;
                obj.AddressPostalCode = "";
                obj.LoginAttempt = 0;
                obj.Status = 0;
                obj.CreateDate = DateTime.Now;
                obj.UpdateDate = DateTime.Now;

                var result = MemberRepo.InsertRegister(obj);
                if (result > 0)
                {
                    string url = VerifyEmai + model.Email;
                    //string url = $"http://localhost/tobjod/th/authen/VerifyEmail?email=" + model.Email ;
                    ActionResultStatus Mail = MailService.SendMail("guicontroltest@gmail.com", "TOBJOD", model.Email, "TOBJOD : Verify Your Email Address", "Verify Your Email Address <br> We now need to Verify your email address. We've sent an email to <br>  " + obj.Email + " to verify you address <br> Please click the link in that email to continue <br> <a target='_blank' href='" + url + "' >Click Verify Email</a>", "", "");

                    if (Mail.Success)
                    {
                        Notify("ดำเนินการเรียบร้อย");

                        //return RedirectToAction("Index", "Home");
                        return RedirectToAction("VerifyEmail", "member");
                    }
                    else
                    {

                        ViewBag.Notify = "การส่ง Email มีปีญหา กรุณาลองใหม่อีกครั้ง";
                    }
                }
                else
                {
                    ViewBag.Notify = "ไม่สามารถทำรายการได้  กรุณาลองใหม่อีกครั้ง";
                }
            }
            else
            {
                ViewBag.Notify = "ไม่สามารถทำรายการได้  กรุณาลองใหม่อีกครั้ง";
            }
            model = BindForm(model);

            return View(Language.ToString() + "/Registered", model);
        }

        public ActionResult VerifyEmail()
        {
            return View(Language.ToString() + "/VerifyEmail");

        }
        public ActionResult Verify()
        {
            return View(Language.ToString() + "/Verify");

        }
        public void LoginUsingGoogle()
        {

            string url = "https://accounts.google.com/o/oauth2/v2/auth?scope=profile&include_granted_scopes=true&redirect_uri=" + GoogleRedirectUrl + "&response_type=code&client_id=" + GoogleClientId + "";
            Response.Redirect(url);

        }
        public void LoginUsingFacebook()
        {
            string url = "https://graph.facebook.com/oauth/authorize?client_id=" + FacebookClientId + "&redirect_uri=" + FacebookRedirectUrl;
            Response.Redirect(url);

        }

        public ActionResult LogOut()
        {
            Session.Abandon();
            return RedirectToAction("Index", "Home");
        }
        private LoginViewModel BindForm(LoginViewModel model)
        {
            ConfigPageRepository repo = new ConfigPageRepository();
            var config = repo.GetConfigPage(TobJod.Models.ConfigPageName.ContactUs);
            ContactSubCategoryRepository subRepo = new ContactSubCategoryRepository();
            var sub = subRepo.ListContactSubCategory_SelectList_Active(Language);
            model.ConfigPage = config;
            Dictionary<String, List<ContactSubCategory>> subC = new Dictionary<string, List<ContactSubCategory>>();
            foreach (var item in sub)
            {
                if (!subC.ContainsKey(item.Category)) subC.Add(item.Category, new List<ContactSubCategory>());
                subC[item.Category].Add(item);
            }

            List<SelectListItem> listItems = new List<SelectListItem>();
            foreach (var item in subC.Keys)
            {
                if (subC[item].Count == 1)
                {
                    var it = subC[item][0];
                    listItems.Add(new SelectListItem() { Text = (Language == Language.EN ? it.TitleEN : it.TitleTH), Value = it.Id.ToString(), Group = null });
                }
                else
                {
                    var group = new SelectListGroup() { Name = item };
                    foreach (var it in subC[item])
                    {
                        listItems.Add(new SelectListItem() { Text = (Language == Language.EN ? it.TitleEN : it.TitleTH), Value = it.Id.ToString(), Group = group });
                    }
                }
            }
            //model.SubCategoryList = listItems;

            PageMeta.FromConfigPage(config, Language);
            return model;
        }
        private RegisterViewModel BindForm(RegisterViewModel model)
        {
            ConfigPageRepository repo = new ConfigPageRepository();
            var config = repo.GetConfigPage(TobJod.Models.ConfigPageName.ContactUs);
            ContactSubCategoryRepository subRepo = new ContactSubCategoryRepository();
            var sub = subRepo.ListContactSubCategory_SelectList_Active(Language);
            model.ConfigPage = config;
            Dictionary<String, List<ContactSubCategory>> subC = new Dictionary<string, List<ContactSubCategory>>();
            foreach (var item in sub)
            {
                if (!subC.ContainsKey(item.Category)) subC.Add(item.Category, new List<ContactSubCategory>());
                subC[item.Category].Add(item);
            }

            List<SelectListItem> listItems = new List<SelectListItem>();
            foreach (var item in subC.Keys)
            {
                if (subC[item].Count == 1)
                {
                    var it = subC[item][0];
                    listItems.Add(new SelectListItem() { Text = (Language == Language.EN ? it.TitleEN : it.TitleTH), Value = it.Id.ToString(), Group = null });
                }
                else
                {
                    var group = new SelectListGroup() { Name = item };
                    foreach (var it in subC[item])
                    {
                        listItems.Add(new SelectListItem() { Text = (Language == Language.EN ? it.TitleEN : it.TitleTH), Value = it.Id.ToString(), Group = group });
                    }
                }
            }
            //model.SubCategoryList = listItems;

            PageMeta.FromConfigPage(config, Language);
            return model;
        }
    }
}