﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TobJod.Repositories;
using TobJod.Utils;
using TobJod.Web.Models;
using TobJod.Models;

namespace TobJod.Web.Controllers {
    public class PrivilegeController : BaseController {
        // GET: Privilege
        public ActionResult Index() {
            ConfigPageRepository repo = new ConfigPageRepository();
            var model = repo.GetConfigPage(TobJod.Models.ConfigPageName.Privilege);
            PageMeta.FromConfigPage(model, Language);
            return View(Language.ToString() + "/Index", model);
            //PrivilegeListViewModel model = new PrivilegeListViewModel();
            //model.UrlUpload = HttpUtils.GetPathRelative(System.Configuration.ConfigurationManager.AppSettings["Path_Privilege"]);
            
            //return View(Language.ToString() + "/Index", model);
        }

        public ActionResult List() {
            int category = NullUtils.cvInt(Request.Params["c"]);
            int subCategory = NullUtils.cvInt(Request.Params["s"]);
            int pageNo = GetPageNo();

            PrivilegeRepository repo = new PrivilegeRepository();
            var model = repo.ListPrivilege_Active(Language, pageNo, 6);


            var data = new { page = pageNo, data = model.Select((x) => new PrivilegeListItemViewModel() { Title = (Language == Language.TH ? x.TitleTH : x.TitleEN), Brief = (Language == Language.TH ? x.BriefTH.ReplacePath() : x.BriefEN.ReplacePath()), Image = (Language == Language.TH ? x.ImageTH : x.ImageEN), ImageAlt = (Language == Language.TH ? x.ImageAltTH : x.ImageAltEN), ImageTitle = (Language == Language.TH ? x.ImageTitleTH : x.ImageTitleEN), Url = "privilege/detail/" + (Language == Language.TH ? x.UrlTH : x.UrlEN), Date = x.CreateDate.ToString("dd MMMM yyyy", cultureInfo) }).ToList() };

            return Json(data, JsonRequestBehavior.AllowGet);

            //return PartialView(Language.ToString() + "/List", model);
        }

        // GET: Privilege/slug
        public ActionResult Detail(string slug = "") {
            if (string.IsNullOrEmpty(slug)) return RedirectToAction("Index");
            PrivilegeRepository repo = new PrivilegeRepository();
            var item = repo.GetPrivilege_Active(slug, Language);
            PrivilegeListViewModel model = new PrivilegeListViewModel();
            if (item == null) return RedirectToAction("Index");
            model.Item = item;
            model.UrlUpload = HttpUtils.GetPathRelative(System.Configuration.ConfigurationManager.AppSettings["Path_Privilege"]);

            if (Language == Language.TH) {
                PageMeta.Title = model.Item.MetaTitleTH;
                PageMeta.MetaDescription = model.Item.MetaDescriptionTH;
                PageMeta.MetaKeyword = model.Item.MetaKeywordTH;
                PageMeta.OGTitle = model.Item.OGTitleTH;
                PageMeta.OGImage = (!string.IsNullOrEmpty(model.Item.OGImageTH) ? HttpUtils.GetPathUrlAbsolute(System.Configuration.ConfigurationManager.AppSettings["Path_Privilege"]) + model.Item.OGImageTH : PageMeta.OGImage);
                PageMeta.OGDescription = model.Item.OGDescriptionTH;
                ViewBag.UrlLang = "/../detail/" + model.Item.UrlEN;
            } else {
                PageMeta.Title = model.Item.MetaTitleTH;
                PageMeta.MetaDescription = model.Item.MetaDescriptionEN;
                PageMeta.MetaKeyword = model.Item.MetaKeywordEN;
                PageMeta.OGTitle = model.Item.OGTitleEN;
                PageMeta.OGImage = (!string.IsNullOrEmpty(model.Item.OGImageEN) ? HttpUtils.GetPathUrlAbsolute(System.Configuration.ConfigurationManager.AppSettings["Path_Privilege"]) + model.Item.OGImageEN : PageMeta.OGImage);
                PageMeta.OGDescription = model.Item.OGDescriptionEN;
                ViewBag.UrlLang = "/../detail/" + model.Item.UrlTH;
            }

            return View(Language.ToString() + "/detail", model);
        }

        // POST: About/Preview
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Preview(String body) {
            if (NullUtils.cvBoolean(System.Configuration.ConfigurationManager.AppSettings["Backend_Preview"])) {
                ConfigPageRepository repo = new ConfigPageRepository();
                ConfigPage model = repo.GetConfigPage_Admin(ConfigPageName.Privilege);
                model.BodyTH = ReplaceTextEditorPathDisplay(body);
                model.BodyEN = ReplaceTextEditorPathDisplay(body);
                return View(Language.ToString() + "/Index", model);
            } else {
                return Redirect("Index");
            }
        }

    }
}