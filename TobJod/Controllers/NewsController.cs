﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TobJod.Repositories;
using TobJod.Utils;
using TobJod.Web.Models;
using TobJod.Models;

namespace TobJod.Web.Controllers {
    public class NewsController : BaseController {
        // GET: News
        public ActionResult Index() {
            NewsSubCategoryRepository catRepo = new NewsSubCategoryRepository();
            var sub = catRepo.ListNewsSubCategory_SelectList_Active(Language);
            NewsListViewModel model = new NewsListViewModel();
            model.SubCategoryList = new SelectList(sub, "Id", "Title" + Language.ToString());
            model.UrlUpload = HttpUtils.GetPathRelative(System.Configuration.ConfigurationManager.AppSettings["Path_News"]);

            ViewBag.SubCategoryJson = sub.GroupBy((x) => x.NewsCategory).Select(g => new {
                GroupId = g.Key,
                Items = g.Select(i => new {
                    Text = (Language == TobJod.Models.Language.TH ? i.TitleTH : i.TitleEN),
                    Value = i.Id
                }).ToList()
            });

            return View(Language.ToString() + "/Index", model);
        }

        public ActionResult List() {
            int category = NullUtils.cvInt(Request.Params["c"]);
            int subCategory = NullUtils.cvInt(Request.Params["s"]);
            int pageNo = GetPageNo();

            NewsRepository repo = new NewsRepository();
            var model = repo.ListNews_Active(Language, pageNo, 6, category, subCategory);
            

            var data = new { page = pageNo, data = model.Select((x) => new NewsListItemViewModel() { Title = (Language == Language.TH ? x.TitleTH : x.TitleEN), SubCategory = x.NewsSubCategory, Brief = (Language == Language.TH ? x.BriefTH.ReplacePath() : x.BriefEN.ReplacePath()), Image = (Language == Language.TH ? x.ImageTH : x.ImageEN), ImageAlt = (Language == Language.TH ? x.ImageAltTH : x.ImageAltEN), ImageTitle = (Language == Language.TH ? x.ImageTitleTH : x.ImageTitleEN), Url = "News/" + x.UrlPrefix + "/" + (Language == Language.TH ? x.UrlTH : x.UrlEN), Date = x.CreateDate.ToString("dd MMMM yyyy", cultureInfo) }).ToList() };

            return Json(data, JsonRequestBehavior.AllowGet);

            //return PartialView(Language.ToString() + "/List", model);
        }

        // GET: News/slug
        public ActionResult Detail(int year = 0, string slug = "") {
            if (year == 0 && string.IsNullOrEmpty(slug)) return RedirectToAction("Index");
            NewsRepository repo = new NewsRepository();
            var item = repo.GetNews_Active(year.ToString(), slug, Language, 3);
            NewsListViewModel model = new NewsListViewModel();
            if (item.Item1 == null) return RedirectToAction("Index");
            model.News = item.Item1;
            model.NewsList = item.Item2;
            model.UrlUpload = HttpUtils.GetPathRelative(System.Configuration.ConfigurationManager.AppSettings["Path_News"]);


            if (Language == Language.TH) {
                PageMeta.Title = model.News.MetaTitleTH;
                PageMeta.MetaDescription = model.News.MetaDescriptionTH;
                PageMeta.MetaKeyword = model.News.MetaKeywordTH;
                PageMeta.OGTitle = model.News.OGTitleTH;
                PageMeta.OGImage = (!string.IsNullOrEmpty(model.News.OGImageTH) ? HttpUtils.GetPathUrlAbsolute(System.Configuration.ConfigurationManager.AppSettings["Path_News"]) + model.News.OGImageTH : PageMeta.OGImage);
                PageMeta.OGDescription = model.News.OGDescriptionTH;
                ViewBag.UrlLang = "/../" + model.News.UrlPrefix + "/" + model.News.UrlEN;
            } else {
                PageMeta.Title = model.News.MetaTitleTH;
                PageMeta.MetaDescription = model.News.MetaDescriptionEN;
                PageMeta.MetaKeyword = model.News.MetaKeywordEN;
                PageMeta.OGTitle = model.News.OGTitleEN;
                PageMeta.OGImage = (!string.IsNullOrEmpty(model.News.OGImageEN) ? HttpUtils.GetPathUrlAbsolute(System.Configuration.ConfigurationManager.AppSettings["Path_News"]) + model.News.OGImageEN : PageMeta.OGImage);
                PageMeta.OGDescription = model.News.OGDescriptionEN;
                ViewBag.UrlLang = "/../" + model.News.UrlPrefix + "/" + model.News.UrlTH;
            }

            return View(Language.ToString() + "/Detail", model);
        }
    }
}