﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TobJod.Repositories;

namespace TobJod.Web.Controllers {
    public class FAQController : BaseController {
        // GET: FAQ
        public ActionResult Index() {
            FaqRepository repo = new FaqRepository();
            var model = repo.ListFaq_Active(Language);
            ViewBag.Pin = repo.ListFaqPin_Active(Language, 5);
            return View(Language.ToString() + "/Index", model);
        }
    }
}