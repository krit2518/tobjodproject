﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TobJod.Web.Models;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Web.Controllers {
    public class PaymentMethodController : BaseController {
        // GET: PaymentMethod
        public ActionResult Index() {
            ConfigPageRepository repo = new ConfigPageRepository();
            var model = repo.GetConfigPage(TobJod.Models.ConfigPageName.PaymentMethod);
            PageMeta.FromConfigPage(model, Language);
            return View(Language.ToString() + "/Index", model);
            //PaymentMethodListViewModel model = new PaymentMethodListViewModel();
            //model.UrlUpload = HttpUtils.GetPathRelative(System.Configuration.ConfigurationManager.AppSettings["Path_PaymentMethod"]);

            //return View(Language.ToString() + "/Index", model);
        }

        public ActionResult List() {
            int category = NullUtils.cvInt(Request.Params["c"]);
            int subCategory = NullUtils.cvInt(Request.Params["s"]);
            int pageNo = GetPageNo();

            PaymentMethodRepository repo = new PaymentMethodRepository();
            var model = repo.ListPaymentMethod_Active(Language, pageNo, 6);


            var data = new { page = pageNo, data = model.Select((x) => new PaymentMethodListItemViewModel() { Title = (Language == Language.TH ? x.TitleTH : x.TitleEN), Brief = (Language == Language.TH ? x.BriefTH.ReplacePath() : x.BriefEN.ReplacePath()), Image = (Language == Language.TH ? x.ImageTH : x.ImageEN), ImageAlt = (Language == Language.TH ? x.ImageAltTH : x.ImageAltEN), ImageTitle = (Language == Language.TH ? x.ImageTitleTH : x.ImageTitleEN), Url = "PaymentMethod/detail/" + (Language == Language.TH ? x.UrlTH : x.UrlEN), Date = x.CreateDate.ToString("dd MMMM yyyy", cultureInfo) }).ToList() };

            return Json(data, JsonRequestBehavior.AllowGet);

            //return PartialView(Language.ToString() + "/List", model);
        }

        // GET: PaymentMethod/slug
        public ActionResult Detail(string slug = "") {
            if (string.IsNullOrEmpty(slug)) return RedirectToAction("Index");
            PaymentMethodRepository repo = new PaymentMethodRepository();
            var item = repo.GetPaymentMethod_Active(slug, Language);
            PaymentMethodListViewModel model = new PaymentMethodListViewModel();
            if (item == null) return RedirectToAction("Index");
            model.Item = item;
            model.UrlUpload = HttpUtils.GetPathRelative(System.Configuration.ConfigurationManager.AppSettings["Path_PaymentMethod"]);

            if (Language == Language.TH) {
                PageMeta.Title = model.Item.MetaTitleTH;
                PageMeta.MetaDescription = model.Item.MetaDescriptionTH;
                PageMeta.MetaKeyword = model.Item.MetaKeywordTH;
                PageMeta.OGTitle = model.Item.OGTitleTH;
                PageMeta.OGImage = (!string.IsNullOrEmpty(model.Item.OGImageTH) ? HttpUtils.GetPathUrlAbsolute(System.Configuration.ConfigurationManager.AppSettings["Path_PaymentMethod"]) + model.Item.OGImageTH : PageMeta.OGImage);
                PageMeta.OGDescription = model.Item.OGDescriptionTH;
                ViewBag.UrlLang = "/../detail/" + model.Item.UrlEN;
            } else {
                PageMeta.Title = model.Item.MetaTitleTH;
                PageMeta.MetaDescription = model.Item.MetaDescriptionEN;
                PageMeta.MetaKeyword = model.Item.MetaKeywordEN;
                PageMeta.OGTitle = model.Item.OGTitleEN;
                PageMeta.OGImage = (!string.IsNullOrEmpty(model.Item.OGImageEN) ? HttpUtils.GetPathUrlAbsolute(System.Configuration.ConfigurationManager.AppSettings["Path_PaymentMethod"]) + model.Item.OGImageEN : PageMeta.OGImage);
                PageMeta.OGDescription = model.Item.OGDescriptionEN;
                ViewBag.UrlLang = "/../detail/" + model.Item.UrlTH;
            }

            return View(Language.ToString() + "/detail", model);
        }

        // POST: About/Preview
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Preview(String body) {
            if (NullUtils.cvBoolean(System.Configuration.ConfigurationManager.AppSettings["Backend_Preview"])) {
                ConfigPageRepository repo = new ConfigPageRepository();
                ConfigPage model = repo.GetConfigPage_Admin(ConfigPageName.PaymentMethod);
                model.BodyTH = ReplaceTextEditorPathDisplay(body);
                model.BodyEN = ReplaceTextEditorPathDisplay(body);
                return View(Language.ToString() + "/Index", model);
            } else {
                return Redirect("Index");
            }
        }

    }
}