using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Service;
using TobJod.Utils;
using TobJod.Web.Models;
using Newtonsoft.Json;
using MigraDoc.DocumentObjectModel;

namespace TobJod.Web.Controllers {

    public class OrderController : BaseController {
        string PathCompany;
        const int MIN_AGE = 10; const int MAX_AGE = 100;
        const int POLICY_HOLDER_SIZE = 4; const int DRIVER_SIZE = 2;
        readonly string Channel;
        readonly string ORDER_MOTOR = "MT";
        readonly string ORDER_NON_MOTOR = "MI";
        readonly string SUCCESS_PAID = "000";
        readonly string SUCCESS_PENDING = "001";
        readonly string TOKEN_SALT = "9v[F0mpNwebw:9N";
        //readonly string PAYMENT_IDLE_TIMEOUT = "9055";

        public OrderController() {
            PathCompany = HttpUtils.GetPathRelative(System.Configuration.ConfigurationManager.AppSettings["Path_Company"]);
            Channel = NullUtils.cvString(System.Configuration.ConfigurationManager.AppSettings["Channel"], "01");
        }

        public ActionResult Index(OrderProductViewModel item) {
            ProductApplicationRepository appRepo = new ProductApplicationRepository();
            ProductApplicationFieldRepository productAppFieldRepo = new ProductApplicationFieldRepository();
            ProductRepository productRepo = new ProductRepository();

            string page = ""; string[] prices = (item.Price + string.Empty).Split(';');

            if (string.IsNullOrWhiteSpace(Request.Params["ProductId"] + "") || string.IsNullOrWhiteSpace(Request.Params["PremiumId"] + "")) { return Redirect(Url.Content("~")); }
            OrderViewModel model = new OrderViewModel();

            model.ProductId = NullUtils.cvInt(Request.Params["ProductId"]);
            model.ProductCategoryKey = GetProductCategory(model.ProductId);
            model.PremiumId = NullUtils.cvInt(Request.Params["PremiumId"], 0);
            model.PriceMin = prices.Length == 2 ? NullUtils.cvInt(prices[0]) : 0;
            model.PriceMax = prices.Length == 2 ? NullUtils.cvInt(prices[1]) : 0;
            model.Price = model.PriceMax;

            var productApplicationFields = productAppFieldRepo.ListProductApplicationFieldByProductCategoryKey(model.ProductCategoryKey, Language);
            if (!productApplicationFields.Any()) { return Redirect(Url.Content("~")); }

            model.ProductApplicationFields = productApplicationFields;
            page = model.ProductCategoryKey.ToString();

            ProductApplication application = new ProductApplication();
            application.ProductId = model.ProductId;
            application.PremiumId = model.PremiumId;
            application.Language = Language;

            application.Steps = CreateSteps(model.ProductApplicationFields);
            application.Step = ProductApplicationStage.Base.Unknown;
            application.PreviousUrl = Request.UrlReferrer != null ? Request.UrlReferrer.PathAndQuery : Url.Content("~");

            var product = productRepo.GetProduct_Active(Language, model.ProductId);
            if (product != null) {
                application.ProductCode = product.ProductCode;
                application.ProductName = Language == Language.TH ? product.TitleTH : product.TitleEN;
                application.InsuranceCode = NullUtils.cvString(product.InsuranceProductCode);
            }


            int applicationId = 0;
            try {
                applicationId = appRepo.AddProductApplication(application);
            } catch (Exception ex) {
                Response.Write(JsonConvert.SerializeObject(application));
                Response.Write(ex.ToString());
                Response.End();
            }
            var leadForm = GetLeadForm();
            if (leadForm != null) { appRepo.UpdateProductApplicationLeadFormId(applicationId, leadForm.Id); }
            string url = Url.Action(page, "Order") + Request.Url.Query + "&ApplicationId=" + applicationId + "&Token=" + GenerateOrderToken(applicationId, application.ProductId);

            return Redirect(url);
        }

        public ActionResult DeleteFile(string fileId, string applicationId, string productId, string appForm) {
            int result = 0;
            if ((NullUtils.cvInt(fileId) > 0 && NullUtils.cvInt(applicationId) > 0 && NullUtils.cvInt(productId) > 0) && appForm + "" != string.Empty) {
                ProductApplicationRepository repo = new ProductApplicationRepository();
                var application = repo.GetProductApplicationForm(NullUtils.cvInt(applicationId));
                if (application != null && application.ProductApplicationAttachment != null) {
                    var o = application.ProductApplicationAttachment.Where(x => x.Id == NullUtils.cvInt(fileId)).FirstOrDefault();
                    if (o != null) {
                        result = repo.DeleteProductApplicationAttachment(NullUtils.cvInt(fileId));
                        if (result > 0) {
                            string url = string.Format(Url.Action(appForm, "Order") + "?ApplicationId={0}&ProductId={1}", applicationId, productId);
                            return Redirect(url);
                        }
                    }
                }
            }
            return Redirect(Url.Content("~"));
        }

        #region PRB

        [ActionName("prb")]
        public ActionResult PRB(OrderProductViewModel item) {
            if (!ValidateOrderToken()) { return Redirect(Url.Content("~")); }
            int applicationId = NullUtils.cvInt(Request.QueryString.Get("ApplicationId"), -1);
            ProductApplicationRepository appRepo = new ProductApplicationRepository();
            CountryRepository countryRepo = new CountryRepository();
            ProvinceRepository provinceRepo = new ProvinceRepository();
            MasterOptionRepository masterRepo = new MasterOptionRepository();
            ProductRepository productRepo = new ProductRepository();
            LeadFormRepository leadRepo = new LeadFormRepository();
            ProductApplicationFieldRepository productAppFieldRepo = new ProductApplicationFieldRepository();
            CarRepository carRepo = new CarRepository();
            ProductApplicationMotor motor = new ProductApplicationMotor();
            BankRepository bankRepo = new BankRepository();
            CareerRepository careerRepo = new CareerRepository();
            DistrictRepository districtRepo = new DistrictRepository();
            SubDistrictRepository subDistrictRepo = new SubDistrictRepository();

            ProductApplication application = applicationId != -1 ? appRepo.GetProductApplicationForm(NullUtils.cvInt(applicationId)) : null;
            if (application == null || item.ProductId == 0) { return Redirect(Url.Content("~")); }

            OrderViewModel model = new OrderViewModel();

            Stack<int> steps = ConvertToApplicationSteps(application.Steps);
            if ((steps == null || steps.Count == 0)) { return Redirect(Url.Content("~")); }

            model.PremiumId = application.PremiumId;
            model.ProductId = application.ProductId;
            model.ProductCategoryKey = GetProductCategory(model.ProductId);
            model.State = steps.Peek();
            model.Page = model.ProductCategoryKey.ToString();
            model.ApplicationId = applicationId;
            model.Token = GenerateOrderToken(model.ApplicationId, model.ProductId);

            #region Lead Form
            var leadForm = GetLeadForm();
            if (leadForm != null) {
                var policyHolder = model.ProductApplicationPolicyHolder;
                policyHolder.Name = leadForm.Name;
                policyHolder.Surname = leadForm.Surname;
                policyHolder.MobilePhone = leadForm.Tel;
                policyHolder.Email = leadForm.Email;
            }
            #endregion

            ProductSubCategory productSubCatetory = appRepo.GetProductSubCategory(application.ProductId, Language);
            ProductItem productItem = productRepo.GetProductPRB_Active(Language, application.ProductId);
            if (productItem == null || CheckOrderStatus(application.Id) || !productItem.SaleChannel.HasFlag(TobJod.Models.ProductSaleChannel.Online)) { return Redirect(Url.Content("~")); }
            var productPremium = productItem.ProductPRB;
            var productName = Language == Language.TH ? productItem.TitleTH : productItem.TitleEN;
            model.ProductItem = productItem;
            model.ProductItem.CompanyImage = PathCompany + productItem.CompanyImage;

            application.ProductCode = NullUtils.cvString(productItem.ProductCode);
            application.ProductName = NullUtils.cvString(productName);
            application.SumInsured = 0;
            application.Premium = productPremium.Premium;
            application.NetPremium = productPremium.NetPremium;
            application.DisplayPremium = productPremium.Premium;
            application.Duty = productPremium.Duty;
            application.EPolicy = 0;

            var date = DateTime.Now.AddDays(1);
            application.ActiveDate = date;
            application.ExpireDate = date.AddYears(1);

            model.PolicyHolder_Size = POLICY_HOLDER_SIZE;
            model.Driver_Size = DRIVER_SIZE;
            model.MinAge = MIN_AGE;
            model.MaxAge = MAX_AGE;

            if (application.ProductApplicationMotor == null) {
                motor.CarRegisterYear = NullUtils.cvInt(item.Year, 0);
                motor.CarBrand = NullUtils.cvString(item.Brand);
                motor.CarFamily = NullUtils.cvString(item.Family);
                motor.CarModelId = NullUtils.cvInt(item.Model, 0);
                motor.CarProvinceId = NullUtils.cvInt(item.Province, 0);
                motor.CarUsageCode = NullUtils.cvInt(item.Usage);
                motor.CarUsageTypeCode = productSubCatetory.Id.ToString();
                //model.Accessory = item.Accessory;
                model.Garage = item.Garage;
                model.ProductApplicationMotor = motor;
            } else {
                model.ProductApplicationMotor = application.ProductApplicationMotor;
            }

            var productApplicationFields = productAppFieldRepo.ListProductApplicationFieldByProductCategoryKey(model.ProductCategoryKey, Language);
            model.ProductApplicationFields = productApplicationFields;

            #region DropDownList
            model.Careers = careerRepo.ListCareer_SelectList_Active(Language);
            model.Countries = countryRepo.ListCountry_SelectList_Active(Language);
            model.Salutations = masterRepo.ListMasterOption_SelectList_Active(Language, MasterOptionCategory.Salutation);
            model.Relationships = masterRepo.ListMasterOption_SelectList_Active(Language, MasterOptionCategory.Relationship);
            model.Provinces = provinceRepo.ListProvince_SelectList_Active(Language);
            model.CarBrands = carRepo.ListCarBrand_SelectList(Language); ;
            model.CarProvinces = masterRepo.ListMasterOption_SelectList_Active(Language, MasterOptionCategory.MotorProvince);
            model.Banks = bankRepo.ListBank_SelectList_Active(Language);
            #endregion

            #region DropDownList: District, SubDistrict
            var districts = districtRepo.ListDistrict_SelectList_Active(NullUtils.cvInt(application.AddressProvince), Language);
            var subDistricts = subDistrictRepo.ListSubDistrict_Active(NullUtils.cvInt(application.AddressDistrict), Language);
            var billing_districts = districtRepo.ListDistrict_SelectList_Active(NullUtils.cvInt(application.BillingAddressProvince), Language);
            var billing_subDistricts = subDistrictRepo.ListSubDistrict_Active(NullUtils.cvInt(application.BillingAddressDistrict), Language);
            var shipping_districts = districtRepo.ListDistrict_SelectList_Active(NullUtils.cvInt(application.ShippingAddressProvince), Language);
            var shipping_subDistricts = subDistrictRepo.ListSubDistrict_Active(NullUtils.cvInt(application.ShippingAddressDistrict), Language);
            model.Districts = districts != null ? districts : new List<SelectListItem>();
            model.SubDistricts = subDistricts != null ? subDistricts.Select<SubDistrict, SelectListItem>(s => new SelectListItem { Value = s.Id.ToString(), Text = Language == Language.TH ? s.TitleTH : s.TitleEN }).ToList<SelectListItem>() : new List<SelectListItem>();
            model.BillingDistricts = billing_districts != null ? billing_districts : new List<SelectListItem>();
            model.BillingSubDistricts = billing_subDistricts != null ? billing_subDistricts.Select<SubDistrict, SelectListItem>(s => new SelectListItem { Value = s.Id.ToString(), Text = Language == Language.TH ? s.TitleTH : s.TitleEN }).ToList<SelectListItem>() : new List<SelectListItem>();
            model.ShippingDistricts = shipping_districts != null ? shipping_districts : new List<SelectListItem>();
            model.ShippingSubDistricts = shipping_subDistricts != null ? shipping_subDistricts.Select<SubDistrict, SelectListItem>(s => new SelectListItem { Value = s.Id.ToString(), Text = Language == Language.TH ? s.TitleTH : s.TitleEN }).ToList<SelectListItem>() : new List<SelectListItem>();
            #endregion

            #region Bind Motor Details
            var _brand = application.ProductApplicationMotor != null ? NullUtils.cvInt(application.ProductApplicationMotor.CarBrand) : NullUtils.cvInt(item.Brand);
            var _family = application.ProductApplicationMotor != null ? NullUtils.cvInt(application.ProductApplicationMotor.CarFamily) : NullUtils.cvInt(item.Family);
            var _model = application.ProductApplicationMotor != null ? NullUtils.cvInt(application.ProductApplicationMotor.CarModelId) : NullUtils.cvInt(item.Model);
            var _province = application.ProductApplicationMotor != null ? NullUtils.cvInt(application.ProductApplicationMotor.CarProvinceId) : NullUtils.cvInt(item.Province);
            var _year = application.ProductApplicationMotor != null ? NullUtils.cvInt(application.ProductApplicationMotor.CarRegisterYear) : NullUtils.cvInt(item.Year);

            model.CarBrands = carRepo.ListCarBrand_SelectList(Language);
            model.CarFamilies = carRepo.ListCarFamilyByBrand_NameValue(Language, _brand);
            model.CarModels = carRepo.ListCarModelByFamily_NameValue(Language, _family);
            model.CarProvinces = masterRepo.ListMasterOption_SelectList_Active(Language, MasterOptionCategory.MotorProvince);

            #endregion


            model.ProductApplicationPolicyHolders = application.ProductApplicationPolicyHolders != null && application.ProductApplicationPolicyHolders.Count > 0
                ? application.ProductApplicationPolicyHolders.ToArray() : new ProductApplicationPolicyHolder[1] { model.ProductApplicationPolicyHolder };
            model.ProductApplicationDrivers = application.ProductApplicationDrivers != null && application.ProductApplicationDrivers.Count > 0
                ? application.ProductApplicationDrivers.ToArray() : new ProductApplicationDriver[1] { model.ProductApplicationDriver };
            model.ProductApplicationAttachments = application.ProductApplicationAttachment != null && application.ProductApplicationAttachment.Count > 0
            ? application.ProductApplicationAttachment : new List<ProductApplicationAttachment>();

            application.Step = steps.Peek();
            if (application.Step != ProductApplicationStage.Base.Success) {
                appRepo.UpdateProductApplication(application);
            }
            model.ProductApplication = application;

            return View(Language.ToString() + "/PRB", model);
        }

        [ActionName("prb")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PRB_Save(OrderViewModel model) {
            if (!ValidateOrderToken()) { return Redirect(Url.Content("~")); }
            bool MoveOn = true;
            ProductApplicationRepository appRepo = new ProductApplicationRepository();
            CountryRepository countryRepo = new CountryRepository();
            ProvinceRepository provinceRepo = new ProvinceRepository();
            MasterOptionRepository masterRepo = new MasterOptionRepository();
            ProductRepository productRepo = new ProductRepository();
            LeadFormRepository leadRepo = new LeadFormRepository();
            CarRepository carRepo = new CarRepository();
            ProductApplicationMotor motor = new ProductApplicationMotor();
            BankRepository bankRepo = new BankRepository();
            CareerRepository careerRepo = new CareerRepository();
            DistrictRepository districtRepo = new DistrictRepository();
            SubDistrictRepository subDistrictRepo = new SubDistrictRepository();

            ProductApplication application = model != null ? appRepo.GetProductApplicationForm(NullUtils.cvInt(model.ApplicationId)) : null;
            if (application == null) { return Redirect(Url.Content("~")); }

            int lastStep = application.Step; 
            int State = ProductApplicationStage.Base.Unknown;
            Stack<int> steps = ConvertToApplicationSteps(application.Steps);
            Stack<int> overSteps = ConvertToApplicationSteps(application.OverSteps);
            if (steps == null || steps.Count == 0) { return Redirect(Url.Content("~")); }
            if (model.UserAction == UserAction.Next) { State = steps.Peek(); };
            if (model.UserAction == UserAction.Back) { State = overSteps.Any() ? overSteps.Peek() : State; }

            ProductSubCategory productSubCatetory = appRepo.GetProductSubCategory(application.ProductId, Language);
            ProductItem productItem = productRepo.GetProductPRB_Active(Language, application.ProductId);
            if (productItem == null || CheckOrderStatus(application.Id) || !productItem.SaleChannel.HasFlag(TobJod.Models.ProductSaleChannel.Online)) { return Redirect(Url.Content("~")); }
            var productPremium = productItem.ProductPRB;
            var productName = Language == Language.TH ? productItem.TitleTH : productItem.TitleEN;
            model.ProductItem = productItem;
            model.ProductItem.CompanyImage = PathCompany + productItem.CompanyImage;

            ProductApplicationFieldRepository productAppFieldRepo = new ProductApplicationFieldRepository();
            var productApplicationFields = productAppFieldRepo.ListProductApplicationFieldByProductCategoryKey(model.ProductCategoryKey, Language);
            model.ProductApplicationFields = productApplicationFields;

            model.PolicyHolder_Size = POLICY_HOLDER_SIZE;
            model.Driver_Size = DRIVER_SIZE;
            model.MinAge = MIN_AGE;
            model.MaxAge = MAX_AGE;

            model.Page = model.ProductCategoryKey.ToString();
            model.ApplicationId = model.ApplicationId;
            model.Token = GenerateOrderToken(model.ApplicationId, model.ProductId);

            #region DropDownList
            model.Careers = careerRepo.ListCareer_SelectList_Active(Language);
            model.Countries = countryRepo.ListCountry_SelectList_Active(Language);
            model.Salutations = masterRepo.ListMasterOption_SelectList_Active(Language, MasterOptionCategory.Salutation);
            model.Relationships = masterRepo.ListMasterOption_SelectList_Active(Language, MasterOptionCategory.Relationship);
            model.Provinces = provinceRepo.ListProvince_SelectList_Active(Language);
            model.CarBrands = carRepo.ListCarBrand_SelectList(Language); ;
            model.CarProvinces = masterRepo.ListMasterOption_SelectList_Active(Language, MasterOptionCategory.MotorProvince);
            model.Banks = bankRepo.ListBank_SelectList_Active(Language);
            #endregion

            #region DropDownList: District, SubDistrict
            var districts = districtRepo.ListDistrict_SelectList_Active(NullUtils.cvInt(application.AddressProvince), Language);
            var subDistricts = subDistrictRepo.ListSubDistrict_Active(NullUtils.cvInt(application.AddressDistrict), Language);
            var billing_districts = districtRepo.ListDistrict_SelectList_Active(NullUtils.cvInt(application.BillingAddressProvince), Language);
            var billing_subDistricts = subDistrictRepo.ListSubDistrict_Active(NullUtils.cvInt(application.BillingAddressDistrict), Language);
            var shipping_districts = districtRepo.ListDistrict_SelectList_Active(NullUtils.cvInt(application.ShippingAddressProvince), Language);
            var shipping_subDistricts = subDistrictRepo.ListSubDistrict_Active(NullUtils.cvInt(application.ShippingAddressDistrict), Language);
            model.Districts = districts != null ? districts : new List<SelectListItem>();
            model.SubDistricts = subDistricts != null ? subDistricts.Select<SubDistrict, SelectListItem>(s => new SelectListItem { Value = s.Id.ToString(), Text = Language == Language.TH ? s.TitleTH : s.TitleEN }).ToList<SelectListItem>() : new List<SelectListItem>();
            model.BillingDistricts = billing_districts != null ? billing_districts : new List<SelectListItem>();
            model.BillingSubDistricts = billing_subDistricts != null ? billing_subDistricts.Select<SubDistrict, SelectListItem>(s => new SelectListItem { Value = s.Id.ToString(), Text = Language == Language.TH ? s.TitleTH : s.TitleEN }).ToList<SelectListItem>() : new List<SelectListItem>();
            model.ShippingDistricts = shipping_districts != null ? shipping_districts : new List<SelectListItem>();
            model.ShippingSubDistricts = shipping_subDistricts != null ? shipping_subDistricts.Select<SubDistrict, SelectListItem>(s => new SelectListItem { Value = s.Id.ToString(), Text = Language == Language.TH ? s.TitleTH : s.TitleEN }).ToList<SelectListItem>() : new List<SelectListItem>();
            #endregion

            #region Lead Form
            var leadForm = GetLeadForm();
            if (leadForm != null) {
                var policyHolder = model.ProductApplicationPolicyHolder;
                policyHolder.Name = leadForm.Name;
                policyHolder.Surname = leadForm.Surname;
                policyHolder.MobilePhone = leadForm.Tel;
                policyHolder.Email = leadForm.Email;
            }
            #endregion

            #region Bind Motor Details
            var _brand = application.ProductApplicationMotor != null ? NullUtils.cvInt(application.ProductApplicationMotor.CarBrand) : NullUtils.cvInt(model.ProductApplicationMotor.CarBrand);
            var _family = application.ProductApplicationMotor != null ? NullUtils.cvInt(application.ProductApplicationMotor.CarFamily) : NullUtils.cvInt(model.ProductApplicationMotor.CarFamily);
            var _model = application.ProductApplicationMotor != null ? NullUtils.cvInt(application.ProductApplicationMotor.CarModelId) : NullUtils.cvInt(model.ProductApplicationMotor.CarModelId);
            var _province = application.ProductApplicationMotor != null ? NullUtils.cvInt(application.ProductApplicationMotor.CarProvinceId) : NullUtils.cvInt(model.ProductApplicationMotor.CarProvinceId);
            var _year = application.ProductApplicationMotor != null ? NullUtils.cvInt(application.ProductApplicationMotor.CarRegisterYear) : NullUtils.cvInt(model.ProductApplicationMotor.CarRegisterYear);

            var carBrands = carRepo.ListCarBrand_SelectList(Language);
            var carFamily = carRepo.ListCarFamilyByBrand_NameValue(Language, _family);
            var carModel = carRepo.ListCarModelByBrand_NameValue(Language, _brand);
            var carProvinces = masterRepo.ListMasterOption_SelectList_Active(Language, MasterOptionCategory.MotorProvince);

            var carBrandObj = carBrands.FirstOrDefault(brand => brand.Value == _brand.ToString());
            var carFamilyObj = carFamily.FirstOrDefault(family => family.Value == _family.ToString());
            var carModelObj = carModel.FirstOrDefault(m => m.Value == _model.ToString());
            var carProvinceObj = carProvinces.FirstOrDefault(p => p.Value == _province.ToString());

            model.CarBrand = carBrandObj != null ? carBrandObj.Text : string.Empty;
            model.CarFamily = carFamilyObj != null ? carFamilyObj.Name : string.Empty;
            model.CarModel = carModelObj != null ? carModelObj.Name : string.Empty;
            model.CarProvince = carProvinceObj != null ? carProvinceObj.Text : string.Empty;

            model.CarBrands = carRepo.ListCarBrand_SelectList(Language);
            model.CarFamilies = carRepo.ListCarFamilyByBrand_NameValue(Language, _brand);
            model.CarModels = carRepo.ListCarModelByFamily_NameValue(Language, _family);
            model.CarProvinces = masterRepo.ListMasterOption_SelectList_Active(Language, MasterOptionCategory.MotorProvince);

            #endregion

            if (NullUtils.cvInt(Request.Form["State"]) != application.Step) {
                return RedirectToAction("prb", new { ApplicationId = application.Id, ProductId = application.ProductId, Token = model.Token });
            }

            if (State == ProductApplicationStage.PRB.PRBForm && model.UserAction == UserAction.Next) {
                motor = CopyModel<ProductApplicationMotor>(model.ProductApplicationMotor);

                var carPlateNo = GetCarPlateNo(model.CarPlateNo);

                carModel = carRepo.ListCarModelByBrand_NameValue(Language, NullUtils.cvInt(motor.CarBrand));
                carModelObj = carModel.FirstOrDefault(m => m.Value == NullUtils.cvString(motor.CarModelId));

                model.CarModel = carModelObj != null ? carModelObj.Name : string.Empty;

                motor.ApplicationId = model.ApplicationId;
                motor.CarPlateNo1 = NullUtils.cvString(carPlateNo.Item1);
                motor.CarPlateNo2 = NullUtils.cvString(carPlateNo.Item2);
                motor.CarAccessoryValue = motor.CarAccessoryValue;
                motor.CarModel = NullUtils.cvString(model.CarModel);
                motor.CarModelId = NullUtils.cvInt(model.ProductApplicationMotor.CarModelId);

                if (application.ProductApplicationMotor == null) {
                    appRepo.AddProductApplicationMotor(motor);
                    application.ProductApplicationMotor = motor;
                } else {
                    appRepo.UpdateProductApplicationMotor(motor);
                }

            }

            if (State == ProductApplicationStage.PRB.DriverForm && model.UserAction == UserAction.Next) {
                List<ProductApplicationDriver> drivers = new List<ProductApplicationDriver>();
                for (var i = 0; model.ProductApplicationDrivers != null && i < model.ProductApplicationDrivers.Length; i++) {
                    var driverItem = model.ProductApplicationDrivers[i];
                    if (NullUtils.cvInt(driverItem.TitleId) > 0 && i == 0) {
                        ProductApplicationDriver driver = new ProductApplicationDriver();
                        driver = CopyModel<ProductApplicationDriver>(driverItem);
                        driver.ApplicationId = model.ApplicationId;
                        driver.Birthday = ValueUtils.GetDate(Request.Form["ProductApplicationDrivers[" + i + "].Birthday"]);

                        drivers.Add(driver);
                    }

                    if (i > 0) {
                        if (driverItem.TitleId > 0 || NullUtils.cvString(driverItem.Name).Length > 0 || NullUtils.cvString(driverItem.Surname).Length > 0 || NullUtils.cvInt(driverItem.IDType) > 0 ||
                            NullUtils.cvString(driverItem.IDCard).Length > 0 || NullUtils.cvInt(driverItem.NationalId) > 0 || NullUtils.cvString(driverItem.RefIDCard).Length > 0 || driverItem.Birthday.ToString("dd/mm/yyyy") != DateUtils.SqlMinDate().ToString("dd/mm/yyyy")) {
                            ProductApplicationDriver driver = new ProductApplicationDriver();
                            driver = CopyModel<ProductApplicationDriver>(driverItem);
                            driver.ApplicationId = model.ApplicationId;
                            driver.Birthday = ValueUtils.GetDate(Request.Form["ProductApplicationDrivers[" + i + "].Birthday"]);

                            drivers.Add(driver);
                        }
                    }
                }
                if (drivers != null && drivers.Count > 0) {
                    appRepo.AddProductApplicationDrivers(drivers);
                    application.ProductApplicationDrivers = drivers;
                }
            }

            if (State == ProductApplicationStage.PRB.PolicyHolderForm && model.UserAction == UserAction.Next) {
                List<ProductApplicationPolicyHolder> policyHolders = new List<ProductApplicationPolicyHolder>();
                for (var i = 0; model.ProductApplicationPolicyHolders != null && i < model.ProductApplicationPolicyHolders.Length; i++) {
                    var policyHolderItem = model.ProductApplicationPolicyHolders[i];
                    if (policyHolderItem != null && NullUtils.cvInt(policyHolderItem.TitleId) > 0) {
                        ProductApplicationPolicyHolder policyHolderModel = new ProductApplicationPolicyHolder();
                        policyHolderModel = CopyModel<ProductApplicationPolicyHolder>(policyHolderItem);
                        policyHolderModel.ApplicationId = model.ApplicationId;
                        policyHolderModel.Sequence = (i + 1);
                        policyHolderModel.Birthday = ValueUtils.GetDate(Request.Form["ProductApplicationPolicyHolders[" + i + "].Birthday"]);
                        policyHolders.Add(policyHolderModel);
                    }
                }

                if (policyHolders != null && policyHolders.Count > 0) {
                    appRepo.AddProductApplicationPolicyHolders(policyHolders);
                    application.ProductApplicationPolicyHolders = policyHolders;
                }

                application.ActiveDate = ValueUtils.GetDate(Request.Form["ProductApplication[ActiveDate]"]);
                application.ExpireDate = ValueUtils.GetDate(Request.Form["ProductApplication[ExpireDate]"]);
                appRepo.UpdateProductApplicationCoverageDate(application);

                if (!SendTBrokerAPIMotorCheck(application.ProductApplicationMotor.CarSerialNo, application.ProductApplicationMotor.CarEngineNo, application.ProductApplicationMotor.CarPlateNo1, application.ProductApplicationMotor.CarPlateNo2, application.ProductApplicationMotor.CarProvinceId, application.ActiveDate, true)) {
                    model.ShowMotorContactAgent = true;
                    steps.Push(State);
                    leadRepo.UpdateAPIStatus(leadForm.Id, OrderStatus.Fail);
                    appRepo.UpdateProductApplicationAPIStatusCategory(application.Id, APIStatusCategory.Motor_C_Fail);
                }
            }

            if (State == ProductApplicationStage.PRB.AddressForm && model.UserAction == UserAction.Next) {
                application = CopyModel<ProductApplication>(model.ProductApplication);
                application.Id = model.ApplicationId;
                #region Setup Address
                if (model.ProductApplication.BillingAddressIsChecked == YesNo.Yes) {
                    application.BillingAddressBranchType = NullUtils.cvInt(model.ProductApplication.AddressBranchType);
                    application.BillingAddressBranchNo = NullUtils.cvString(model.ProductApplication.AddressBranchNo);
                    application.BillingAddressNo = NullUtils.cvString(model.ProductApplication.AddressNo);
                    application.BillingAddressMoo = NullUtils.cvString(model.ProductApplication.AddressMoo);
                    application.BillingAddressVillage = NullUtils.cvString(model.ProductApplication.AddressVillage);
                    application.BillingAddressFloor = NullUtils.cvString(model.ProductApplication.AddressFloor);
                    application.BillingAddressSoi = NullUtils.cvString(model.ProductApplication.AddressSoi);
                    application.BillingAddressRoad = NullUtils.cvString(model.ProductApplication.AddressRoad);
                    application.BillingAddressDistrict = NullUtils.cvString(model.ProductApplication.AddressDistrict);
                    application.BillingAddressSubDistrict = NullUtils.cvString(model.ProductApplication.AddressSubDistrict);
                    application.BillingAddressProvince = NullUtils.cvString(model.ProductApplication.AddressProvince);
                    application.BillingAddressPostalCode = NullUtils.cvString(model.ProductApplication.AddressPostalCode);
                }

                if (model.ProductApplication.ShippingAddressIsChecked == YesNo.Yes) {
                    application.ShippingAddressBranchType = NullUtils.cvInt(model.ProductApplication.AddressBranchType);
                    application.ShippingAddressBranchNo = NullUtils.cvString(model.ProductApplication.AddressBranchNo);
                    application.ShippingAddressNo = NullUtils.cvString(model.ProductApplication.AddressNo);
                    application.ShippingAddressMoo = NullUtils.cvString(model.ProductApplication.AddressMoo);
                    application.ShippingAddressVillage = NullUtils.cvString(model.ProductApplication.AddressVillage);
                    application.ShippingAddressFloor = NullUtils.cvString(model.ProductApplication.AddressFloor);
                    application.ShippingAddressSoi = NullUtils.cvString(model.ProductApplication.AddressSoi);
                    application.ShippingAddressRoad = NullUtils.cvString(model.ProductApplication.AddressRoad);
                    application.ShippingAddressDistrict = NullUtils.cvString(model.ProductApplication.AddressDistrict);
                    application.ShippingAddressSubDistrict = NullUtils.cvString(model.ProductApplication.AddressSubDistrict);
                    application.ShippingAddressProvince = NullUtils.cvString(model.ProductApplication.AddressProvince);
                    application.ShippingAddressPostalCode = NullUtils.cvString(model.ProductApplication.AddressPostalCode);
                }
                #endregion
                appRepo.UpdateProductApplicationAddressForm(application);
                application = appRepo.GetProductApplicationForm(application.Id);
            }

            var path = Server.MapPath(System.Web.Configuration.WebConfigurationManager.AppSettings.Get("Path_Upload"));
            if (State == ProductApplicationStage.PRB.AttachmentForm && model.UserAction == UserAction.Next) {
                #region Upload Files
                List<ProductApplicationAttachment> attachments = new List<ProductApplicationAttachment>();
                
                MoveOn &= UploadAttachmentFile(model.ApplicationId, model.ProductId, model.IDCardFile, FileCategory.IDCard, application, attachments);
                MoveOn &= UploadAttachmentFile(model.ApplicationId, model.ProductId, model.CCTVFile, FileCategory.CCTV, application, attachments);
                MoveOn &= UploadAttachmentFile(model.ApplicationId, model.ProductId, model.CarRegistrationCopyFile, FileCategory.RegisteredCarCopy, application, attachments);
                MoveOn &= UploadAttachmentFile(model.ApplicationId, model.ProductId, model.File4, FileCategory.File4, application, attachments);
                MoveOn &= UploadAttachmentFile(model.ApplicationId, model.ProductId, model.File5, FileCategory.File5, application, attachments);
                MoveOn &= UploadAttachmentFile(model.ApplicationId, model.ProductId, model.File6, FileCategory.File6, application, attachments);
                
                if (attachments.Count > 0) { appRepo.AddProductApplicationAttachments(attachments); }
                #endregion
            }

            if (State == ProductApplicationStage.Base.Final && model.UserAction == UserAction.Next) {
                #region Promotion And Coupon
                var promotionId = NullUtils.cvInt(model.ProductApplication.PromotionId);
                PromotionItem promotion = null; bool hasGift = false;
                ClearPromotionAndCoupon(application);
                if (promotionId > 0 && productItem.Promotion != null) {
                    promotion = productItem.Promotion.Where(prm => prm.Id == promotionId).FirstOrDefault();
                    if (promotion != null) {
                        application.PromotionId = promotionId;
                        var discount = promotion.DiscountAmount;
                        if (promotion.DiscountType == PromotionDiscountType.Amount) {
                            model.TotalDiscount = (discount > application.DisplayPremium ? 0 : discount);
                            UpdatePromotionAndCoupon(application, model.TotalDiscount, promotionId, promotion.CampaignId, string.Empty);
                            hasGift = true;
                        } else {
                            model.TotalDiscount = ((application.DisplayPremium * discount) / 100);
                            UpdatePromotionAndCoupon(application, model.TotalDiscount, promotionId, promotion.CampaignId, string.Empty);
                            hasGift = true;
                        }
                    }
                }

                var systemMessageRepository = new SystemMessageRepository();
                var messages = systemMessageRepository.ListSystemMessage();
                var message = new SystemMessage();
                var couponCode = NullUtils.cvString(model.ProductApplication.CouponCode);
                Coupon coupon = null;

                if (couponCode.Length > 0) {
                    coupon = appRepo.GetCoupon_Active(couponCode, application.ProductId);
                    if (coupon != null) {
                        var checkedDiscount = (promotion != null && promotion.PromotionType == PromotionType.Discount);
                        if (coupon.CouponType == CouponType.OneTime) {
                            if (coupon.TotalUsed == 0) {
                                if (!checkedDiscount) {
                                    if (coupon.DiscountType == CouponDiscountType.Amount) {
                                        model.TotalDiscount += coupon.DiscountAmount;
                                        UpdatePromotionAndCoupon(application, model.TotalDiscount, hasGift ? promotionId : 0, promotion.CampaignId, couponCode);
                                        model.ProductApplication.CouponCode = couponCode;
                                    } else {
                                        model.TotalDiscount += (application.DisplayPremium * (coupon.DiscountAmount) / 100);
                                        UpdatePromotionAndCoupon(application, model.TotalDiscount, hasGift ? promotionId : 0, promotion.CampaignId, couponCode);
                                        model.ProductApplication.CouponCode = couponCode;
                                    }
                                }
                            } else {
                                ClearPromotionAndCoupon(application);
                                model.ProductApplication = application;
                                model.TotalDiscount = 0;
                                message = messages.Where(o => o.Category == SystemMessageCategory.Coupon && o.Code == ((int)SystemMessageStatus.CouponUsed).ToString()).FirstOrDefault();
                                ViewBag.CouponError = Language == Language.TH ? message.BodyTH : message.BodyEN;
                                return View(Language.ToString() + "/PRB", model);
                            }
                        }
                        if (coupon.CouponType == CouponType.Mass) {
                            if (!checkedDiscount) {
                                if (coupon.TotalUsed < coupon.Total) {
                                    if (coupon.DiscountType == CouponDiscountType.Amount) {
                                        model.TotalDiscount += coupon.DiscountAmount;
                                        UpdatePromotionAndCoupon(application, model.TotalDiscount, hasGift ? promotionId : 0, promotion.CampaignId, couponCode);
                                        model.ProductApplication.CouponCode = couponCode;
                                    } else {
                                        model.TotalDiscount += (application.DisplayPremium * (coupon.DiscountAmount) / 100);
                                        UpdatePromotionAndCoupon(application, model.TotalDiscount, hasGift ? promotionId : 0, promotion.CampaignId, couponCode);
                                        model.ProductApplication.CouponCode = couponCode;
                                    }
                                } else {
                                    ClearPromotionAndCoupon(application);
                                    model.ProductApplication = application;
                                    model.TotalDiscount = 0;
                                    message = messages.Where(o => o.Category == SystemMessageCategory.Coupon && o.Code == ((int)SystemMessageStatus.CouponUsed).ToString()).FirstOrDefault();
                                    ViewBag.CouponError = Language == Language.TH ? message.BodyTH : message.BodyEN;
                                    return View(Language.ToString() + "/PRB", model);
                                }
                            } else {
                                ClearPromotionAndCoupon(application);
                                model.ProductApplication = application;
                                model.TotalDiscount = 0;
                                message = messages.Where(o => o.Category == SystemMessageCategory.Coupon && o.Code == ((int)SystemMessageStatus.CouponTopUp).ToString()).FirstOrDefault();
                                ViewBag.CouponError = Language == Language.TH ? message.BodyTH : message.BodyEN;
                                return View(Language.ToString() + "/PRB", model);
                            }
                        }
                    } else {
                        ClearPromotionAndCoupon(application);
                        model.ProductApplication = application;
                        model.TotalDiscount = 0;
                        model.ProductApplication.CouponCode = string.Empty;
                        message = messages.Where(o => o.Category == SystemMessageCategory.Coupon && o.Code == ((int)SystemMessageStatus.CouponUnKnown).ToString()).FirstOrDefault();
                        ViewBag.CouponError = Language == Language.TH ? message.BodyTH : message.BodyEN;
                        return View(Language.ToString() + "/PRB", model);
                    }
                }

                var currentAppForm = appRepo.GetProductApplicationForm(application.Id);
                application.CouponCode = NullUtils.cvString(currentAppForm.CouponCode);
                application.TotalDiscount = currentAppForm.TotalDiscount;
                RecheckPromotion_Active(promotionId, productItem, application);
                RecheckCoupon_Active(couponCode, application);
                #endregion
            }

            model.ProductApplicationPolicyHolders = application.ProductApplicationPolicyHolders != null && application.ProductApplicationPolicyHolders.Count > 0
                ? application.ProductApplicationPolicyHolders.ToArray() : new ProductApplicationPolicyHolder[1] { model.ProductApplicationPolicyHolder };
            model.ProductApplicationDrivers = application.ProductApplicationDrivers != null && application.ProductApplicationDrivers.Count > 0
                ? application.ProductApplicationDrivers.ToArray() : new ProductApplicationDriver[1] { model.ProductApplicationDriver };
            model.ProductApplicationAttachments = application.ProductApplicationAttachment != null && application.ProductApplicationAttachment.Count > 0
                ? application.ProductApplicationAttachment : new List<ProductApplicationAttachment>();
            model.ProductApplicationMotor = application.ProductApplicationMotor != null ? application.ProductApplicationMotor : model.ProductApplicationMotor;

            #region Validate

            MoveOn &= ValidateOtp(application.Id, model.ProductApplicationPolicyHolders);
            MoveOn &= ValidateStringAlphanum(application.ProductApplicationMotor.CarEngineNo);
            MoveOn &= ValidateStringAlphanum(application.ProductApplicationMotor.CarSerialNo);
            MoveOn &= ValidateStringAlphanum(application.ProductApplicationMotor.CarPlateNo1);
            MoveOn &= ValidateStringNumeric(application.ProductApplicationMotor.CarPlateNo2);
            MoveOn &= ValidateStringAlphanum(application.ProductApplicationMotor.CarAccessories);

            MoveOn &= ValidateStringAlphanum(application.ContactName);
            MoveOn &= ValidateStringAlphanum(application.ContactSurname);
            MoveOn &= ValidateStringAlphanumTel(application.ContactTelephone);
            MoveOn &= ValidateStringAlphanumTel(application.ContactMobilePhone);
            MoveOn &= ValidateStringAlphanumEmail(application.ContactEmail);
            MoveOn &= ValidateStringAlphanum(application.ContactLineId);

            MoveOn &= ValidateStringAlphanum(application.BuyerName);
            MoveOn &= ValidateStringAlphanum(application.BuyerSurname);
            MoveOn &= ValidateStringAlphanumTel(application.BuyerTelephone);
            MoveOn &= ValidateStringAlphanumTel(application.BuyerMobilePhone);
            MoveOn &= ValidateStringAlphanumEmail(application.BuyerEmail);
            MoveOn &= ValidateStringAlphanum(application.BuyerLineId);

            MoveOn &= ValidateStringAlphanum(application.AddressBranchNo);
            MoveOn &= ValidateStringAlphanumAddress(application.AddressNo);
            MoveOn &= ValidateStringAlphanum(application.AddressMoo);
            MoveOn &= ValidateStringAlphanum(application.AddressVillage);
            MoveOn &= ValidateStringNumeric(application.AddressFloor);
            MoveOn &= ValidateStringAlphanum(application.AddressSoi);
            MoveOn &= ValidateStringAlphanum(application.AddressRoad);
            MoveOn &= ValidateStringAlphanum(application.AddressPostalCode);

            MoveOn &= ValidateStringAlphanum(application.BillingAddressBranchNo);
            MoveOn &= ValidateStringAlphanumAddress(application.BillingAddressNo);
            MoveOn &= ValidateStringAlphanum(application.BillingAddressMoo);
            MoveOn &= ValidateStringAlphanum(application.BillingAddressVillage);
            MoveOn &= ValidateStringNumeric(application.BillingAddressFloor);
            MoveOn &= ValidateStringAlphanum(application.BillingAddressSoi);
            MoveOn &= ValidateStringAlphanum(application.BillingAddressRoad);
            MoveOn &= ValidateStringAlphanum(application.BillingAddressPostalCode);

            MoveOn &= ValidateStringAlphanum(application.ShippingAddressBranchNo);
            MoveOn &= ValidateStringAlphanumAddress(application.ShippingAddressNo);
            MoveOn &= ValidateStringAlphanum(application.ShippingAddressMoo);
            MoveOn &= ValidateStringAlphanum(application.ShippingAddressVillage);
            MoveOn &= ValidateStringNumeric(application.ShippingAddressFloor);
            MoveOn &= ValidateStringAlphanum(application.ShippingAddressSoi);
            MoveOn &= ValidateStringAlphanum(application.ShippingAddressRoad);
            MoveOn &= ValidateStringAlphanum(application.ShippingAddressPostalCode);

            foreach (var it in model.ProductApplicationDrivers) {
                MoveOn &= ValidateStringAlphanum(it.Name);
                MoveOn &= ValidateStringAlphanum(it.Surname);
                MoveOn &= ValidateStringAlphanum(it.RefIDCard);
                MoveOn &= ValidateStringAlphanum(it.IDCard);
            }
            foreach (var it in model.ProductApplicationPolicyHolders) {
                MoveOn &= ValidateStringAlphanum(it.Name);
                MoveOn &= ValidateStringAlphanum(it.Surname);
                MoveOn &= ValidateStringAlphanum(it.RefIDCard);
                MoveOn &= ValidateStringAlphanum(it.IDCard);
                MoveOn &= ValidateStringAlphanumTel(it.Telephone);
                MoveOn &= ValidateStringAlphanumTel(it.MobilePhone);
                MoveOn &= ValidateStringAlphanumEmail(it.Email);
                MoveOn &= ValidateStringAlphanum(it.BeneficiaryName);
                MoveOn &= ValidateStringAlphanum(it.BeneficiarySurname);
                MoveOn &= ValidateStringAlphanum(it.BeneficiaryIDCard);
                MoveOn &= ValidateStringAlphanumTel(it.BeneficiaryMobilePhone);
                MoveOn &= ValidateStringAlphanum(it.LineId);
                MoveOn &= ValidateStringAlphanum(it.Instagram);
                MoveOn &= ValidateStringAlphanum(it.Facebook);
            }

            #endregion

            if (State != ProductApplicationStage.Base.Success || (State == ProductApplicationStage.Base.Success && model.UserAction == UserAction.Back)) {
                if (MoveOn) {
                    if (model.UserAction == UserAction.Next) { State = steps.Pop(); overSteps.Push(State); } else { State = overSteps.Pop(); steps.Push(State); };
                    model.State = model.UserAction == UserAction.Next ? steps.Peek() : State;
                    application.Steps = steps != null ? string.Join("-", steps.ToList().Select(x => (int)x).Reverse().ToArray()) : string.Empty;
                    application.OverSteps = overSteps != null ? string.Join("-", overSteps.ToList().Select(x => (int)x).Reverse().ToArray()) : string.Empty;
                    application.Step = (int)model.State;

                    appRepo.UpdateProductApplicationState(application);
                }
                model.ProductApplication = application;
            }

            if (State == ProductApplicationStage.Base.Success && model.UserAction == UserAction.Next) {
                var couponRepo = new CouponRepository();
                var order = GenerateOrder(application, ORDER_MOTOR, leadForm != null ? leadForm.Id : 0);
                if (order != null) {
                    couponRepo.UpdateUsedCouponCode(NullUtils.cvString(application.CouponCode), order.Id);
                }

                State = ProductApplicationStage.Base.Success;
                model.State = State;
                application.Step = (int)model.State;
                application.Language = Language;
                appRepo.UpdateProductApplicationState(application);
                return Redirect(Url.Action("PaymentRequest") + "/?ApplicationId=" + application.Id);
            }

            return View(Language.ToString() + "/PRB", model);
        }

        #endregion

        #region PA

        [ActionName("pa")]
        public ActionResult PA(OrderProductViewModel item) {
            if (!ValidateOrderToken()) { return Redirect(Url.Content("~")); }
            int applicationId = NullUtils.cvInt(Request.QueryString.Get("ApplicationId"), -1);
            ProductApplicationRepository appRepo = new ProductApplicationRepository();
            CountryRepository countryRepo = new CountryRepository();
            ProvinceRepository provinceRepo = new ProvinceRepository();
            MasterOptionRepository masterRepo = new MasterOptionRepository();
            ProductRepository productRepo = new ProductRepository();
            LeadFormRepository leadRepo = new LeadFormRepository();
            ProductApplicationFieldRepository productAppFieldRepo = new ProductApplicationFieldRepository();
            CarRepository carRepo = new CarRepository();
            ProductApplicationMotor motor = new ProductApplicationMotor();
            DistrictRepository districtRepo = new DistrictRepository();
            SubDistrictRepository subDistrictRepo = new SubDistrictRepository();
            CareerRepository careerRepo = new CareerRepository();

            ProductApplication application = applicationId != -1 ? appRepo.GetProductApplicationForm(NullUtils.cvInt(applicationId)) : null;
            if (application == null || item.ProductId == 0 /*|| application.Step == ProductApplicationStage.Base.Success*/) { return Redirect(Url.Content("~")); }

            OrderViewModel model = new OrderViewModel();

            Stack<int> steps = ConvertToApplicationSteps(application.Steps);
            if ((steps == null || steps.Count == 0)) { return Redirect(Url.Content("~")); }

            model.PremiumId = application.PremiumId;
            model.ProductId = application.ProductId;
            model.ProductCategoryKey = GetProductCategory(model.ProductId);
            model.State = steps.Peek();
            model.Page = model.ProductCategoryKey.ToString();
            model.ApplicationId = applicationId;
            model.Token = GenerateOrderToken(model.ApplicationId, model.ProductId);

            if (application.ProductApplicationPolicyHolders == null || application.ProductApplicationPolicyHolders.Count == 0) {
                ProductApplicationPolicyHolder policyHolder = new ProductApplicationPolicyHolder();
                policyHolder.ApplicationId = application.Id;
                policyHolder.CareerId = NullUtils.cvInt(Request.Params["Career"]);
                policyHolder.Birthday = ValueUtils.GetDate(NullUtils.cvInt(Request.Params["BirthDay"]));

                #region Lead Form

                var leadForm = GetLeadForm();
                if (leadForm != null) {
                    policyHolder.Name = leadForm.Name;
                    policyHolder.Surname = leadForm.Surname;
                    policyHolder.MobilePhone = leadForm.Tel;
                    policyHolder.Email = leadForm.Email;
                }

                #endregion

                appRepo.AddProductApplicationPolicyHolder(policyHolder);
                application.ProductApplicationPolicyHolders = new List<ProductApplicationPolicyHolder> { policyHolder };
            }

            ProductItem productItem = productRepo.GetProductPA_Active(Language, application.ProductId, application.PremiumId);
            if (productItem == null || CheckOrderStatus(application.Id) || !productItem.SaleChannel.HasFlag(TobJod.Models.ProductSaleChannel.Online)) { return Redirect(Url.Content("~")); }
            var productPremium = productItem.ProductPAPremium[0];
            var productName = Language == Language.TH ? productItem.TitleTH : productItem.TitleEN;
            model.ProductItem = productItem;
            model.ProductItem.CompanyImage = PathCompany + productItem.CompanyImage;

            application.ProductCode = NullUtils.cvString(productItem.ProductCode);
            application.ProductName = NullUtils.cvString(productName);
            application.SumInsured = productPremium.SumInsured;
            application.NetPremium = productPremium.NetPremium;
            application.DisplayPremium = productPremium.DisplayPremium;
            application.EPolicy = productPremium.EPolicy ? (int)TobJod.Models.AllYesNo.Yes : (int)TobJod.Models.AllYesNo.No;

            var productApplicationFields = productAppFieldRepo.ListProductApplicationFieldByProductCategoryKey(model.ProductCategoryKey, Language);
            model.ProductApplicationFields = productApplicationFields;

            #region DropDownList
            var districts = districtRepo.ListDistrict_SelectList_Active(NullUtils.cvInt(application.AddressProvince), Language);
            var subDistricts = subDistrictRepo.ListSubDistrict_Active(NullUtils.cvInt(application.AddressDistrict), Language);
            var billing_districts = districtRepo.ListDistrict_SelectList_Active(NullUtils.cvInt(application.BillingAddressProvince), Language);
            var billing_subDistricts = subDistrictRepo.ListSubDistrict_Active(NullUtils.cvInt(application.BillingAddressDistrict), Language);
            var shipping_districts = districtRepo.ListDistrict_SelectList_Active(NullUtils.cvInt(application.ShippingAddressProvince), Language);
            var shipping_subDistricts = subDistrictRepo.ListSubDistrict_Active(NullUtils.cvInt(application.ShippingAddressDistrict), Language);

            model.Careers = careerRepo.ListCareer_SelectList_Active(Language);
            model.Countries = countryRepo.ListCountry_SelectList_Active(Language);
            model.Relationships = masterRepo.ListMasterOption_SelectList_Active(Language, MasterOptionCategory.Relationship);
            model.Salutations = masterRepo.ListMasterOption_SelectList_Active(Language, MasterOptionCategory.Salutation);
            model.Provinces = provinceRepo.ListProvince_SelectList_Active(Language);
            model.Districts = districts != null ? districts : new List<SelectListItem>();
            model.SubDistricts = subDistricts != null ? subDistricts.Select<SubDistrict, SelectListItem>(s => new SelectListItem { Value = s.Id.ToString(), Text = Language == Language.TH ? s.TitleTH : s.TitleEN }).ToList<SelectListItem>() : new List<SelectListItem>();
            model.BillingDistricts = billing_districts != null ? billing_districts : new List<SelectListItem>();
            model.BillingSubDistricts = billing_subDistricts != null ? billing_subDistricts.Select<SubDistrict, SelectListItem>(s => new SelectListItem { Value = s.Id.ToString(), Text = Language == Language.TH ? s.TitleTH : s.TitleEN }).ToList<SelectListItem>() : new List<SelectListItem>();
            model.ShippingDistricts = shipping_districts != null ? shipping_districts : new List<SelectListItem>();
            model.ShippingSubDistricts = shipping_subDistricts != null ? shipping_subDistricts.Select<SubDistrict, SelectListItem>(s => new SelectListItem { Value = s.Id.ToString(), Text = Language == Language.TH ? s.TitleTH : s.TitleEN }).ToList<SelectListItem>() : new List<SelectListItem>();
            #endregion

            var date = DateTime.Now.AddDays(1);
            application.ActiveDate = date;
            application.ExpireDate = date.AddYears(1);
            model.PolicyHolder_Size = POLICY_HOLDER_SIZE;
            model.Driver_Size = DRIVER_SIZE;

            model.ProductApplicationPolicyHolders = application.ProductApplicationPolicyHolders != null && application.ProductApplicationPolicyHolders.Count > 0
                ? application.ProductApplicationPolicyHolders.ToArray() : new ProductApplicationPolicyHolder[1] { model.ProductApplicationPolicyHolder };
            model.ProductApplicationAttachments = application.ProductApplicationAttachment != null && application.ProductApplicationAttachment.Count > 0
                ? application.ProductApplicationAttachment : new List<ProductApplicationAttachment>();

            model.MinAge = productPremium.MinAge != 0 ? productPremium.MinAge : MIN_AGE;
            model.MaxAge = productPremium.MaxAge != 0 ? productPremium.MaxAge : MAX_AGE;

            application.Step = steps.Peek();
            if (application.Step != ProductApplicationStage.Base.Success) {
                appRepo.UpdateProductApplication(application);
            }
            model.ProductApplication = application;

            return View(Language.ToString() + "/PA", model);
        }

        [ActionName("pa")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PA_Save(OrderViewModel model) {
            if (!ValidateOrderToken()) { return Redirect(Url.Content("~")); }
            bool MoveOn = true;
            ProductApplicationRepository appRepo = new ProductApplicationRepository();
            CountryRepository countryRepo = new CountryRepository();
            ProvinceRepository provinceRepo = new ProvinceRepository();
            MasterOptionRepository masterRepo = new MasterOptionRepository();
            ProductRepository productRepo = new ProductRepository();
            LeadFormRepository leadRepo = new LeadFormRepository();
            CarRepository carRepo = new CarRepository();
            ProductApplicationMotor motor = new ProductApplicationMotor();
            DistrictRepository districtRepo = new DistrictRepository();
            SubDistrictRepository subDistrictRepo = new SubDistrictRepository();
            CareerRepository careerRepo = new CareerRepository();

            ProductApplication application = model != null ? appRepo.GetProductApplicationForm(NullUtils.cvInt(model.ApplicationId)) : null;
            if (application == null) { return Redirect(Url.Content("~")); }

            int lastStep = application.Step;
            int State = ProductApplicationStage.Base.Unknown;
            Stack<int> steps = ConvertToApplicationSteps(application.Steps);
            Stack<int> overSteps = ConvertToApplicationSteps(application.OverSteps);
            if (steps == null || steps.Count == 0) { return Redirect(Url.Content("~")); }
            if (model.UserAction == UserAction.Next) { State = steps.Peek(); };
            if (model.UserAction == UserAction.Back) { State = overSteps.Any() ? overSteps.Peek() : State; }

            ProductItem productItem = productRepo.GetProductPA_Active(Language, application.ProductId, application.PremiumId);
            if (productItem == null || CheckOrderStatus(application.Id) || !productItem.SaleChannel.HasFlag(TobJod.Models.ProductSaleChannel.Online)) { return Redirect(Url.Content("~")); }
            var productPremium = productItem.ProductPAPremium[0];
            model.ProductItem = productItem;
            model.ProductItem.CompanyImage = PathCompany + productItem.CompanyImage;

            ProductApplicationFieldRepository productAppFieldRepo = new ProductApplicationFieldRepository();
            var productApplicationFields = productAppFieldRepo.ListProductApplicationFieldByProductCategoryKey(model.ProductCategoryKey, Language);
            model.ProductApplicationFields = productApplicationFields;

            model.PolicyHolder_Size = POLICY_HOLDER_SIZE;
            model.Driver_Size = DRIVER_SIZE;
            model.MinAge = productPremium.MinAge != 0 ? productPremium.MinAge : MIN_AGE;
            model.MaxAge = productPremium.MaxAge != 0 ? productPremium.MaxAge : MAX_AGE;
            model.Page = model.ProductCategoryKey.ToString();
            model.ApplicationId = model.ApplicationId;

            #region DropDownList
            model.Careers = careerRepo.ListCareer_SelectList_Active(Language);
            model.Countries = countryRepo.ListCountry_SelectList_Active(Language);
            model.Relationships = masterRepo.ListMasterOption_SelectList_Active(Language, MasterOptionCategory.Relationship);
            model.Salutations = masterRepo.ListMasterOption_SelectList_Active(Language, MasterOptionCategory.Salutation);
            model.Provinces = provinceRepo.ListProvince_SelectList_Active(Language);
            #endregion

            #region DropDownList: Province, District, SubDistrict
            var districts = districtRepo.ListDistrict_SelectList_Active(NullUtils.cvInt(application.AddressProvince), Language);
            var subDistricts = subDistrictRepo.ListSubDistrict_Active(NullUtils.cvInt(application.AddressDistrict), Language);
            var billing_districts = districtRepo.ListDistrict_SelectList_Active(NullUtils.cvInt(application.BillingAddressProvince), Language);
            var billing_subDistricts = subDistrictRepo.ListSubDistrict_Active(NullUtils.cvInt(application.BillingAddressDistrict), Language);
            var shipping_districts = districtRepo.ListDistrict_SelectList_Active(NullUtils.cvInt(application.ShippingAddressProvince), Language);
            var shipping_subDistricts = subDistrictRepo.ListSubDistrict_Active(NullUtils.cvInt(application.ShippingAddressDistrict), Language);
            model.Districts = districts != null ? districts : new List<SelectListItem>();
            model.SubDistricts = subDistricts != null ? subDistricts.Select<SubDistrict, SelectListItem>(s => new SelectListItem { Value = s.Id.ToString(), Text = Language == Language.TH ? s.TitleTH : s.TitleEN }).ToList<SelectListItem>() : new List<SelectListItem>();
            model.BillingDistricts = billing_districts != null ? billing_districts : new List<SelectListItem>();
            model.BillingSubDistricts = billing_subDistricts != null ? billing_subDistricts.Select<SubDistrict, SelectListItem>(s => new SelectListItem { Value = s.Id.ToString(), Text = Language == Language.TH ? s.TitleTH : s.TitleEN }).ToList<SelectListItem>() : new List<SelectListItem>();
            model.ShippingDistricts = shipping_districts != null ? shipping_districts : new List<SelectListItem>();
            model.ShippingSubDistricts = shipping_subDistricts != null ? shipping_subDistricts.Select<SubDistrict, SelectListItem>(s => new SelectListItem { Value = s.Id.ToString(), Text = Language == Language.TH ? s.TitleTH : s.TitleEN }).ToList<SelectListItem>() : new List<SelectListItem>();
            #endregion

            #region Lead Form
            var leadForm = GetLeadForm();
            if (leadForm != null) {
                var policyHolder = model.ProductApplicationPolicyHolder;
                policyHolder.Name = leadForm.Name;
                policyHolder.Surname = leadForm.Surname;
                policyHolder.MobilePhone = leadForm.Tel;
                policyHolder.Email = leadForm.Email;
            }
            #endregion

            if (NullUtils.cvInt(Request.Form["State"]) != application.Step) {
                return RedirectToAction("pa", new { ApplicationId = application.Id, ProductId = application.ProductId, Token = model.Token });
            }

            if (State == ProductApplicationStage.PA.PolicyHolderForm) {
                if (model.UserAction == UserAction.Next) {
                    List<ProductApplicationPolicyHolder> policyHolders = new List<ProductApplicationPolicyHolder>();
                    for (var i = 0; model.ProductApplicationPolicyHolders != null && i < model.ProductApplicationPolicyHolders.Length; i++) {
                        var policyHolderItem = model.ProductApplicationPolicyHolders[i];
                        if (policyHolderItem != null && NullUtils.cvInt(policyHolderItem.TitleId) > 0) {
                            ProductApplicationPolicyHolder policyHolderModel = new ProductApplicationPolicyHolder();
                            policyHolderModel = CopyModel<ProductApplicationPolicyHolder>(policyHolderItem);
                            policyHolderModel.ApplicationId = model.ApplicationId;
                            policyHolderModel.Sequence = (i + 1);
                            policyHolderModel.Birthday = ValueUtils.GetDate(Request.Form["ProductApplicationPolicyHolders[" + i + "].Birthday"]);
                            policyHolders.Add(policyHolderModel);
                        }
                    }

                    if (policyHolders.Count > 0) {
                        appRepo.AddProductApplicationPolicyHolders(policyHolders);
                        model.ProductApplicationPolicyHolders = policyHolders.ToArray();
                    }

                    application.ContactName = NullUtils.cvString(model.ProductApplication.ContactName);
                    application.ContactSurname = NullUtils.cvString(model.ProductApplication.ContactSurname);
                    application.ContactTelephone = NullUtils.cvString(model.ProductApplication.ContactTelephone);
                    application.ContactMobilePhone = NullUtils.cvString(model.ProductApplication.ContactMobilePhone);
                    application.ContactEmail = NullUtils.cvString(model.ProductApplication.ContactEmail);

                    application.BuyerName = NullUtils.cvString(model.ProductApplication.BuyerName);
                    application.BuyerSurname = NullUtils.cvString(model.ProductApplication.BuyerSurname);
                    application.BuyerTelephone = NullUtils.cvString(model.ProductApplication.BuyerTelephone);
                    application.BuyerMobilePhone = NullUtils.cvString(model.ProductApplication.BuyerMobilePhone);
                    application.BuyerEmail = NullUtils.cvString(model.ProductApplication.BuyerEmail);

                    application.ActiveDate = ValueUtils.GetDate(Request.Form["ProductApplication[ActiveDate]"]);
                    application.ExpireDate = ValueUtils.GetDate(Request.Form["ProductApplication[ExpireDate]"]);

                    appRepo.UpdateProductApplication(application);
                }

            }

            if (State == ProductApplicationStage.PA.AddressForm && model.UserAction == UserAction.Next) {
                application = CopyModel<ProductApplication>(model.ProductApplication);
                application.Id = model.ApplicationId;
                application.AddressBranchNo = NullUtils.cvString(application.AddressBranchNo);
                application.BillingAddressBranchNo = NullUtils.cvString(application.BillingAddressBranchNo);
                application.ShippingAddressBranchNo = NullUtils.cvString(application.ShippingAddressBranchNo);
                #region Setup Address
                if (model.ProductApplication.BillingAddressIsChecked == YesNo.Yes) {
                    application.BillingAddressBranchType = NullUtils.cvInt(model.ProductApplication.AddressBranchType);
                    application.BillingAddressBranchNo = NullUtils.cvString(model.ProductApplication.AddressBranchNo);
                    application.BillingAddressNo = NullUtils.cvString(model.ProductApplication.AddressNo);
                    application.BillingAddressMoo = NullUtils.cvString(model.ProductApplication.AddressMoo);
                    application.BillingAddressVillage = NullUtils.cvString(model.ProductApplication.AddressVillage);
                    application.BillingAddressFloor = NullUtils.cvString(model.ProductApplication.AddressFloor);
                    application.BillingAddressSoi = NullUtils.cvString(model.ProductApplication.AddressSoi);
                    application.BillingAddressRoad = NullUtils.cvString(model.ProductApplication.AddressRoad);
                    application.BillingAddressDistrict = NullUtils.cvString(model.ProductApplication.AddressDistrict);
                    application.BillingAddressSubDistrict = NullUtils.cvString(model.ProductApplication.AddressSubDistrict);
                    application.BillingAddressProvince = NullUtils.cvString(model.ProductApplication.AddressProvince);
                    application.BillingAddressPostalCode = NullUtils.cvString(model.ProductApplication.AddressPostalCode);
                }

                if (model.ProductApplication.ShippingAddressIsChecked == YesNo.Yes) {
                    application.ShippingAddressBranchType = NullUtils.cvInt(model.ProductApplication.AddressBranchType);
                    application.ShippingAddressBranchNo = NullUtils.cvString(model.ProductApplication.AddressBranchNo);
                    application.ShippingAddressNo = NullUtils.cvString(model.ProductApplication.AddressNo);
                    application.ShippingAddressMoo = NullUtils.cvString(model.ProductApplication.AddressMoo);
                    application.ShippingAddressVillage = NullUtils.cvString(model.ProductApplication.AddressVillage);
                    application.ShippingAddressFloor = NullUtils.cvString(model.ProductApplication.AddressFloor);
                    application.ShippingAddressSoi = NullUtils.cvString(model.ProductApplication.AddressSoi);
                    application.ShippingAddressRoad = NullUtils.cvString(model.ProductApplication.AddressRoad);
                    application.ShippingAddressDistrict = NullUtils.cvString(model.ProductApplication.AddressDistrict);
                    application.ShippingAddressSubDistrict = NullUtils.cvString(model.ProductApplication.AddressSubDistrict);
                    application.ShippingAddressProvince = NullUtils.cvString(model.ProductApplication.AddressProvince);
                    application.ShippingAddressPostalCode = NullUtils.cvString(model.ProductApplication.AddressPostalCode);
                }
                #endregion
                appRepo.UpdateProductApplicationAddressForm(application);
                application = appRepo.GetProductApplicationForm(application.Id);
            }

            var path = Server.MapPath(System.Web.Configuration.WebConfigurationManager.AppSettings.Get("Path_Upload"));
            if (State == ProductApplicationStage.PA.AttachmentForm && model.UserAction == UserAction.Next) {
                #region Upload Files
                List<ProductApplicationAttachment> attachments = new List<ProductApplicationAttachment>();
                
                MoveOn &= UploadAttachmentFile(model.ApplicationId, model.ProductId, model.File1, FileCategory.File1, application, attachments);
                MoveOn &= UploadAttachmentFile(model.ApplicationId, model.ProductId, model.File2, FileCategory.File2, application, attachments);
                MoveOn &= UploadAttachmentFile(model.ApplicationId, model.ProductId, model.File3, FileCategory.File3, application, attachments);
                MoveOn &= UploadAttachmentFile(model.ApplicationId, model.ProductId, model.File4, FileCategory.File4, application, attachments);
                MoveOn &= UploadAttachmentFile(model.ApplicationId, model.ProductId, model.File5, FileCategory.File5, application, attachments);
                MoveOn &= UploadAttachmentFile(model.ApplicationId, model.ProductId, model.File6, FileCategory.File6, application, attachments);

                if (attachments.Count > 0) { appRepo.AddProductApplicationAttachments(attachments); }
                #endregion
            }

            if (State == ProductApplicationStage.Base.Final && model.UserAction == UserAction.Next) {
                #region Promotion And Coupon
                var promotionId = NullUtils.cvInt(model.ProductApplication.PromotionId);
                PromotionItem promotion = null; bool hasGift = false;
                ClearPromotionAndCoupon(application);
                if (promotionId > 0 && productItem.Promotion != null) {
                    promotion = productItem.Promotion.Where(prm => prm.Id == promotionId).FirstOrDefault();
                    if (promotion != null) {
                        application.PromotionId = promotionId;
                        var discount = promotion.DiscountAmount;
                        if (promotion.DiscountType == PromotionDiscountType.Amount) {
                            model.TotalDiscount = (discount > application.DisplayPremium ? 0 : discount);
                            UpdatePromotionAndCoupon(application, model.TotalDiscount, promotionId, promotion.CampaignId, string.Empty);
                            hasGift = true;
                        } else {
                            model.TotalDiscount = ((application.DisplayPremium * discount) / 100);
                            UpdatePromotionAndCoupon(application, model.TotalDiscount, promotionId, promotion.CampaignId, string.Empty);
                            hasGift = true;
                        }
                    }
                }

                var systemMessageRepository = new SystemMessageRepository();
                var messages = systemMessageRepository.ListSystemMessage();
                var message = new SystemMessage();
                var couponCode = NullUtils.cvString(model.ProductApplication.CouponCode);
                Coupon coupon = null;

                if (couponCode.Length > 0) {
                    coupon = appRepo.GetCoupon_Active(couponCode, application.ProductId);
                    if (coupon != null) {
                        var checkedDiscount = (promotion != null && promotion.PromotionType == PromotionType.Discount);
                        if (coupon.CouponType == CouponType.OneTime) {
                            if (coupon.TotalUsed == 0) {
                                if (!checkedDiscount) {
                                    if (coupon.DiscountType == CouponDiscountType.Amount) {
                                        model.TotalDiscount += coupon.DiscountAmount;
                                        UpdatePromotionAndCoupon(application, model.TotalDiscount, hasGift ? promotionId : 0, promotion.CampaignId, couponCode);
                                        model.ProductApplication.CouponCode = couponCode;
                                    } else {
                                        model.TotalDiscount += (application.DisplayPremium * (coupon.DiscountAmount) / 100);
                                        UpdatePromotionAndCoupon(application, model.TotalDiscount, hasGift ? promotionId : 0, promotion.CampaignId, couponCode);
                                        model.ProductApplication.CouponCode = couponCode;
                                    }
                                }
                            } else {
                                ClearPromotionAndCoupon(application);
                                model.ProductApplication = application;
                                model.TotalDiscount = 0;
                                message = messages.Where(o => o.Category == SystemMessageCategory.Coupon && o.Code == ((int)SystemMessageStatus.CouponUsed).ToString()).FirstOrDefault();
                                ViewBag.CouponError = Language == Language.TH ? message.BodyTH : message.BodyEN;
                                return View(Language.ToString() + "/PA", model);
                            }
                        }
                        if (coupon.CouponType == CouponType.Mass) {
                            if (!checkedDiscount) {
                                if (coupon.TotalUsed < coupon.Total) {
                                    if (coupon.DiscountType == CouponDiscountType.Amount) {
                                        model.TotalDiscount += coupon.DiscountAmount;
                                        UpdatePromotionAndCoupon(application, model.TotalDiscount, hasGift ? promotionId : 0, promotion.CampaignId, couponCode);
                                        model.ProductApplication.CouponCode = couponCode;
                                    } else {
                                        model.TotalDiscount += (application.DisplayPremium * (coupon.DiscountAmount) / 100);
                                        UpdatePromotionAndCoupon(application, model.TotalDiscount, hasGift ? promotionId : 0, promotion.CampaignId, couponCode);
                                        model.ProductApplication.CouponCode = couponCode;
                                    }
                                } else {
                                    ClearPromotionAndCoupon(application);
                                    model.ProductApplication = application;
                                    model.TotalDiscount = 0;
                                    message = messages.Where(o => o.Category == SystemMessageCategory.Coupon && o.Code == ((int)SystemMessageStatus.CouponUsed).ToString()).FirstOrDefault();
                                    ViewBag.CouponError = Language == Language.TH ? message.BodyTH : message.BodyEN;
                                    return View(Language.ToString() + "/PA", model);
                                }
                            } else {
                                ClearPromotionAndCoupon(application);
                                model.ProductApplication = application;
                                model.TotalDiscount = 0;
                                message = messages.Where(o => o.Category == SystemMessageCategory.Coupon && o.Code == ((int)SystemMessageStatus.CouponTopUp).ToString()).FirstOrDefault();
                                ViewBag.CouponError = Language == Language.TH ? message.BodyTH : message.BodyEN;
                                return View(Language.ToString() + "/PA", model);
                            }
                        }
                    } else {
                        ClearPromotionAndCoupon(application);
                        model.ProductApplication = application;
                        model.TotalDiscount = 0;
                        model.ProductApplication.CouponCode = string.Empty;
                        message = messages.Where(o => o.Category == SystemMessageCategory.Coupon && o.Code == ((int)SystemMessageStatus.CouponUnKnown).ToString()).FirstOrDefault();
                        ViewBag.CouponError = Language == Language.TH ? message.BodyTH : message.BodyEN;
                        return View(Language.ToString() + "/PA", model);
                    }
                }

                var currentAppForm = appRepo.GetProductApplicationForm(application.Id);
                application.CouponCode = NullUtils.cvString(currentAppForm.CouponCode);
                application.TotalDiscount = currentAppForm.TotalDiscount;
                RecheckPromotion_Active(promotionId, productItem, application);
                RecheckCoupon_Active(couponCode, application);
                #endregion
            }

            model.ProductApplicationPolicyHolders = application.ProductApplicationPolicyHolders != null && application.ProductApplicationPolicyHolders.Count > 0
                ? application.ProductApplicationPolicyHolders.ToArray() : new ProductApplicationPolicyHolder[1] { model.ProductApplicationPolicyHolder };
            model.ProductApplicationAttachments = application.ProductApplicationAttachment != null && application.ProductApplicationAttachment.Count > 0
                ? application.ProductApplicationAttachment : new List<ProductApplicationAttachment>();

            #region Validate

            MoveOn &= ValidateOtp(application.Id, model.ProductApplicationPolicyHolders);
            MoveOn &= ValidateStringAlphanum(application.ContactName);
            MoveOn &= ValidateStringAlphanum(application.ContactSurname);
            MoveOn &= ValidateStringAlphanumTel(application.ContactTelephone);
            MoveOn &= ValidateStringAlphanumTel(application.ContactMobilePhone);
            MoveOn &= ValidateStringAlphanumEmail(application.ContactEmail);
            MoveOn &= ValidateStringAlphanum(application.ContactLineId);

            MoveOn &= ValidateStringAlphanum(application.BuyerName);
            MoveOn &= ValidateStringAlphanum(application.BuyerSurname);
            MoveOn &= ValidateStringAlphanumTel(application.BuyerTelephone);
            MoveOn &= ValidateStringAlphanumTel(application.BuyerMobilePhone);
            MoveOn &= ValidateStringAlphanumEmail(application.BuyerEmail);
            MoveOn &= ValidateStringAlphanum(application.BuyerLineId);

            MoveOn &= ValidateStringAlphanum(application.AddressBranchNo);
            MoveOn &= ValidateStringAlphanumAddress(application.AddressNo);
            MoveOn &= ValidateStringAlphanum(application.AddressMoo);
            MoveOn &= ValidateStringAlphanum(application.AddressVillage);
            MoveOn &= ValidateStringNumeric(application.AddressFloor);
            MoveOn &= ValidateStringAlphanum(application.AddressSoi);
            MoveOn &= ValidateStringAlphanum(application.AddressRoad);
            MoveOn &= ValidateStringAlphanum(application.AddressPostalCode);

            MoveOn &= ValidateStringAlphanum(application.BillingAddressBranchNo);
            MoveOn &= ValidateStringAlphanumAddress(application.BillingAddressNo);
            MoveOn &= ValidateStringAlphanum(application.BillingAddressMoo);
            MoveOn &= ValidateStringAlphanum(application.BillingAddressVillage);
            MoveOn &= ValidateStringNumeric(application.BillingAddressFloor);
            MoveOn &= ValidateStringAlphanum(application.BillingAddressSoi);
            MoveOn &= ValidateStringAlphanum(application.BillingAddressRoad);
            MoveOn &= ValidateStringAlphanum(application.BillingAddressPostalCode);

            MoveOn &= ValidateStringAlphanum(application.ShippingAddressBranchNo);
            MoveOn &= ValidateStringAlphanumAddress(application.ShippingAddressNo);
            MoveOn &= ValidateStringAlphanum(application.ShippingAddressMoo);
            MoveOn &= ValidateStringAlphanum(application.ShippingAddressVillage);
            MoveOn &= ValidateStringNumeric(application.ShippingAddressFloor);
            MoveOn &= ValidateStringAlphanum(application.ShippingAddressSoi);
            MoveOn &= ValidateStringAlphanum(application.ShippingAddressRoad);
            MoveOn &= ValidateStringAlphanum(application.ShippingAddressPostalCode);

            foreach (var it in model.ProductApplicationPolicyHolders) {
                MoveOn &= ValidateStringAlphanum(it.Name);
                MoveOn &= ValidateStringAlphanum(it.Surname);
                MoveOn &= ValidateStringAlphanum(it.RefIDCard);
                MoveOn &= ValidateStringAlphanum(it.IDCard);
                MoveOn &= ValidateStringAlphanumTel(it.Telephone);
                MoveOn &= ValidateStringAlphanumTel(it.MobilePhone);
                MoveOn &= ValidateStringAlphanumEmail(it.Email);
                MoveOn &= ValidateStringAlphanum(it.BeneficiaryName);
                MoveOn &= ValidateStringAlphanum(it.BeneficiarySurname);
                MoveOn &= ValidateStringAlphanum(it.BeneficiaryIDCard);
                MoveOn &= ValidateStringAlphanumTel(it.BeneficiaryMobilePhone);
                MoveOn &= ValidateStringAlphanum(it.LineId);
                MoveOn &= ValidateStringAlphanum(it.Instagram);
                MoveOn &= ValidateStringAlphanum(it.Facebook);
            }

            #endregion

            if (State != ProductApplicationStage.Base.Success || (State == ProductApplicationStage.Base.Success && model.UserAction == UserAction.Back)) {
                if (MoveOn) {
                    if (model.UserAction == UserAction.Next) { State = steps.Pop(); overSteps.Push(State); } else { State = overSteps.Pop(); steps.Push(State); };
                    model.State = model.UserAction == UserAction.Next ? steps.Peek() : State;
                    application.Steps = steps != null ? string.Join("-", steps.ToList().Select(x => (int)x).Reverse().ToArray()) : string.Empty;
                    application.OverSteps = overSteps != null ? string.Join("-", overSteps.ToList().Select(x => (int)x).Reverse().ToArray()) : string.Empty;
                    application.Step = (int)model.State;

                    appRepo.UpdateProductApplicationState(application);
                }
                model.ProductApplication = application;
            }

            if (State == ProductApplicationStage.Base.Success && model.UserAction == UserAction.Next) {
                var couponRepo = new CouponRepository();
                var order = GenerateOrder(application, ORDER_NON_MOTOR, leadForm != null ? leadForm.Id : 0);
                if (order != null) {
                    couponRepo.UpdateUsedCouponCode(NullUtils.cvString(application.CouponCode), order.Id);
                }

                State = ProductApplicationStage.Base.Success;
                model.State = State;
                application.Step = (int)model.State;
                application.Language = Language;
                appRepo.UpdateProductApplicationState(application);
                return Redirect(Url.Action("PaymentRequest") + "/?ApplicationId=" + application.Id);
            }
            return View(Language.ToString() + "/PA", model);
        }

        #endregion

        #region PH

        [ActionName("ph")]
        public ActionResult PH(OrderProductViewModel item) {
            if (!ValidateOrderToken()) { return Redirect(Url.Content("~")); }
            int applicationId = NullUtils.cvInt(Request.QueryString.Get("ApplicationId"), -1);
            ProductApplicationRepository appRepo = new ProductApplicationRepository();
            CountryRepository countryRepo = new CountryRepository();
            ProvinceRepository provinceRepo = new ProvinceRepository();
            MasterOptionRepository masterRepo = new MasterOptionRepository();
            ProductRepository productRepo = new ProductRepository();
            LeadFormRepository leadRepo = new LeadFormRepository();
            ProductApplicationFieldRepository productAppFieldRepo = new ProductApplicationFieldRepository();
            CareerRepository careerRepo = new CareerRepository();

            ProductApplication application = applicationId != -1 ? appRepo.GetProductApplicationForm(NullUtils.cvInt(applicationId)) : null;
            if (application == null || item.ProductId == 0) { return Redirect(Url.Content("~")); }

            OrderViewModel model = new OrderViewModel();

            Stack<int> steps = ConvertToApplicationSteps(application.Steps);
            if ((steps == null || steps.Count == 0)) { return Redirect(Url.Content("~")); }

            model.PremiumId = application.PremiumId;
            model.ProductId = application.ProductId;
            model.ProductCategoryKey = GetProductCategory(model.ProductId);
            model.State = steps.Peek();
            model.Page = model.ProductCategoryKey.ToString();
            model.ApplicationId = applicationId;
            model.Token = GenerateOrderToken(model.ApplicationId, model.ProductId);


            #region Lead Form
            var leadForm = GetLeadForm();
            if (leadForm != null) {
                var policyHolder = model.ProductApplicationPolicyHolder;
                policyHolder.Name = leadForm.Name;
                policyHolder.Surname = leadForm.Surname;
                policyHolder.MobilePhone = leadForm.Tel;
                policyHolder.Email = leadForm.Email;
            }
            #endregion

            ProductItem productItem = productRepo.GetProductPH_Active(Language, application.ProductId, application.PremiumId);
            if (productItem == null || CheckOrderStatus(application.Id) || !productItem.SaleChannel.HasFlag(TobJod.Models.ProductSaleChannel.Online)) { return Redirect(Url.Content("~")); }
            var productPremium = productItem.ProductPHPremium[0];
            var productName = Language == Language.TH ? productItem.TitleTH : productItem.TitleEN;
            model.ProductItem = productItem;
            model.ProductItem.CompanyImage = PathCompany + productItem.CompanyImage;

            application.ProductCode = NullUtils.cvString(productItem.ProductCode);
            application.ProductName = NullUtils.cvString(productName);
            application.SumInsured = productPremium.SumInsured;
            application.NetPremium = productPremium.NetPremium;
            application.DisplayPremium = productPremium.DisplayPremium;
            application.EPolicy = productPremium.EPolicy ? (int)TobJod.Models.AllYesNo.Yes : (int)TobJod.Models.AllYesNo.No;

            var date = DateTime.Now.AddDays(1);
            application.ActiveDate = date;
            application.ExpireDate = date.AddYears(1);

            var productApplicationFields = productAppFieldRepo.ListProductApplicationFieldByProductCategoryKey(model.ProductCategoryKey, Language);
            model.ProductApplicationFields = productApplicationFields;

            #region DropDownList
            model.Careers = careerRepo.ListCareer_SelectList_Active(Language);
            model.Countries = countryRepo.ListCountry_SelectList_Active(Language);
            model.Salutations = masterRepo.ListMasterOption_SelectList_Active(Language, MasterOptionCategory.Salutation);
            model.Relationships = masterRepo.ListMasterOption_SelectList_Active(Language, MasterOptionCategory.Relationship);
            model.Provinces = provinceRepo.ListProvince_SelectList_Active(Language);
            #endregion

            model.MinAge = productPremium.MinAge;
            model.MaxAge = productPremium.MaxAge;
            model.MinWeight = productPremium.MinWeight;
            model.MaxWeight = productPremium.MaxWeight;
            model.MinHeight = productPremium.MinHeight;
            model.MaxHeight = productPremium.MaxHeight;

            if (application.ProductApplicationPolicyHolders == null || application.ProductApplicationPolicyHolders.Count == 0) {
                model.ProductApplicationPolicyHolder.CareerId = NullUtils.cvInt(Request.QueryString.Get("Career"));
                model.ProductApplicationPolicyHolder.Sex = NullUtils.cvInt(Request.QueryString.Get("Sex"));
                model.ProductApplicationPolicyHolder.Height = NullUtils.cvInt(Request.QueryString.Get("Height"));
                model.ProductApplicationPolicyHolder.Weight = NullUtils.cvInt(Request.QueryString.Get("Weight"));
            }

            model.ProductApplicationPolicyHolders = application.ProductApplicationPolicyHolders != null && application.ProductApplicationPolicyHolders.Count > 0
                ? application.ProductApplicationPolicyHolders.ToArray() : new ProductApplicationPolicyHolder[1] { model.ProductApplicationPolicyHolder };

            application.Step = steps.Peek();
            if (application.Step != ProductApplicationStage.Base.Success) {
                appRepo.UpdateProductApplication(application);
            }
            model.ProductApplication = application;

            return View(Language.ToString() + "/PH", model);
        }

        [ActionName("ph")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PH_Save(OrderViewModel model) {
            if (!ValidateOrderToken()) { return Redirect(Url.Content("~")); }
            bool MoveOn = true;
            ProductApplicationRepository appRepo = new ProductApplicationRepository();
            CountryRepository countryRepo = new CountryRepository();
            ProvinceRepository provinceRepo = new ProvinceRepository();
            MasterOptionRepository masterRepo = new MasterOptionRepository();
            ProductRepository productRepo = new ProductRepository();
            LeadFormRepository leadRepo = new LeadFormRepository();
            DistrictRepository districtRepo = new DistrictRepository();
            SubDistrictRepository subDistrictRepo = new SubDistrictRepository();
            CareerRepository careerRepo = new CareerRepository();

            ProductApplication application = model != null ? appRepo.GetProductApplicationForm(NullUtils.cvInt(model.ApplicationId)) : null;
            if (application == null) { return Redirect(Url.Content("~")); }

            int lastStep = application.Step;
            int State = ProductApplicationStage.Base.Unknown;
            Stack<int> steps = ConvertToApplicationSteps(application.Steps);
            Stack<int> overSteps = ConvertToApplicationSteps(application.OverSteps);
            if (steps == null || steps.Count == 0) { return Redirect(Url.Content("~")); }
            if (model.UserAction == UserAction.Next) { State = steps.Peek(); };
            if (model.UserAction == UserAction.Back) { State = overSteps.Any() ? overSteps.Peek() : State; }

            ProductItem productItem = productRepo.GetProductPH_Active(Language, application.ProductId, application.PremiumId);
            if (productItem == null || CheckOrderStatus(application.Id) || !productItem.SaleChannel.HasFlag(TobJod.Models.ProductSaleChannel.Online)) { return Redirect(Url.Content("~")); }
            var productPremium = productItem.ProductPHPremium[0];
            model.ProductItem = productItem;
            model.ProductItem.CompanyImage = PathCompany + productItem.CompanyImage;

            ProductApplicationFieldRepository productAppFieldRepo = new ProductApplicationFieldRepository();
            var productApplicationFields = productAppFieldRepo.ListProductApplicationFieldByProductCategoryKey(model.ProductCategoryKey, Language);
            model.ProductApplicationFields = productApplicationFields;

            model.Page = model.ProductCategoryKey.ToString();
            model.State = State;
            model.ApplicationId = model.ApplicationId;

            #region DropDownList
            model.Careers = careerRepo.ListCareer_SelectList_Active(Language);
            model.Countries = countryRepo.ListCountry_SelectList_Active(Language);
            model.Relationships = masterRepo.ListMasterOption_SelectList_Active(Language, MasterOptionCategory.Relationship);
            model.Salutations = masterRepo.ListMasterOption_SelectList_Active(Language, MasterOptionCategory.Salutation);
            model.Provinces = provinceRepo.ListProvince_SelectList_Active(Language);
            #endregion

            #region DropDownList: District, SubDistrict
            var districts = districtRepo.ListDistrict_SelectList_Active(NullUtils.cvInt(application.AddressProvince), Language);
            var subDistricts = subDistrictRepo.ListSubDistrict_Active(NullUtils.cvInt(application.AddressDistrict), Language);
            var billing_districts = districtRepo.ListDistrict_SelectList_Active(NullUtils.cvInt(application.BillingAddressProvince), Language);
            var billing_subDistricts = subDistrictRepo.ListSubDistrict_Active(NullUtils.cvInt(application.BillingAddressDistrict), Language);
            var shipping_districts = districtRepo.ListDistrict_SelectList_Active(NullUtils.cvInt(application.ShippingAddressProvince), Language);
            var shipping_subDistricts = subDistrictRepo.ListSubDistrict_Active(NullUtils.cvInt(application.ShippingAddressDistrict), Language);
            model.Districts = districts != null ? districts : new List<SelectListItem>();
            model.SubDistricts = subDistricts != null ? subDistricts.Select<SubDistrict, SelectListItem>(s => new SelectListItem { Value = s.Id.ToString(), Text = Language == Language.TH ? s.TitleTH : s.TitleEN }).ToList<SelectListItem>() : new List<SelectListItem>();
            model.BillingDistricts = billing_districts != null ? billing_districts : new List<SelectListItem>();
            model.BillingSubDistricts = billing_subDistricts != null ? billing_subDistricts.Select<SubDistrict, SelectListItem>(s => new SelectListItem { Value = s.Id.ToString(), Text = Language == Language.TH ? s.TitleTH : s.TitleEN }).ToList<SelectListItem>() : new List<SelectListItem>();
            model.ShippingDistricts = shipping_districts != null ? shipping_districts : new List<SelectListItem>();
            model.ShippingSubDistricts = shipping_subDistricts != null ? shipping_subDistricts.Select<SubDistrict, SelectListItem>(s => new SelectListItem { Value = s.Id.ToString(), Text = Language == Language.TH ? s.TitleTH : s.TitleEN }).ToList<SelectListItem>() : new List<SelectListItem>();
            #endregion

            model.MinAge = productPremium.MinAge;
            model.MaxAge = productPremium.MaxAge;
            model.MinWeight = productPremium.MinWeight;
            model.MaxWeight = productPremium.MaxWeight;
            model.MinHeight = productPremium.MinHeight;
            model.MaxHeight = productPremium.MaxHeight;

            #region Lead Form
            var leadForm = GetLeadForm();
            if (leadForm != null) {
                var policyHolder = model.ProductApplicationPolicyHolder;
                policyHolder.Name = leadForm.Name;
                policyHolder.Surname = leadForm.Surname;
                policyHolder.MobilePhone = leadForm.Tel;
                policyHolder.Email = leadForm.Email;
            }
            #endregion

            if (NullUtils.cvInt(Request.Form["State"]) != application.Step) {
                return RedirectToAction("ph", new { ApplicationId = application.Id, ProductId = application.ProductId, Token = model.Token });
            }

            if (State == ProductApplicationStage.PH.PolicyHolderForm && model.UserAction == UserAction.Next) {
                List<ProductApplicationPolicyHolder> policyHolders = new List<ProductApplicationPolicyHolder>();
                for (var i = 0; model.ProductApplicationPolicyHolders != null && i < model.ProductApplicationPolicyHolders.Length; i++) {
                    var policyHolderItem = model.ProductApplicationPolicyHolders[i];
                    if (policyHolderItem.TitleId > 0) {
                        ProductApplicationPolicyHolder policyHolderModel = new ProductApplicationPolicyHolder();
                        policyHolderModel = CopyModel<ProductApplicationPolicyHolder>(policyHolderItem);
                        policyHolderModel.ApplicationId = model.ApplicationId;
                        policyHolderModel.Sequence = (i + 1);
                        policyHolderModel.Birthday = ValueUtils.GetDate(Request.Form["ProductApplicationPolicyHolders[" + i + "].Birthday"]);
                        policyHolders.Add(policyHolderModel);
                    }
                }

                if (policyHolders.Count > 0) {
                    appRepo.AddProductApplicationPolicyHolders(policyHolders);
                    model.ProductApplicationPolicyHolders = policyHolders.ToArray();
                }

                application.ContactName = NullUtils.cvString(model.ProductApplication.ContactName);
                application.ContactSurname = NullUtils.cvString(model.ProductApplication.ContactSurname);
                application.ContactTelephone = NullUtils.cvString(model.ProductApplication.ContactTelephone);
                application.ContactMobilePhone = NullUtils.cvString(model.ProductApplication.ContactMobilePhone);
                application.ContactEmail = NullUtils.cvString(model.ProductApplication.ContactEmail);

                application.BuyerName = NullUtils.cvString(model.ProductApplication.BuyerName);
                application.BuyerSurname = NullUtils.cvString(model.ProductApplication.BuyerSurname);
                application.BuyerTelephone = NullUtils.cvString(model.ProductApplication.BuyerTelephone);
                application.BuyerMobilePhone = NullUtils.cvString(model.ProductApplication.BuyerMobilePhone);
                application.BuyerEmail = NullUtils.cvString(model.ProductApplication.BuyerEmail);

                application.ActiveDate = ValueUtils.GetDate(Request.Form["ProductApplication[ActiveDate]"]);
                application.ExpireDate = ValueUtils.GetDate(Request.Form["ProductApplication[ExpireDate]"]);

                appRepo.UpdateProductApplication(application);
            }

            if (State == ProductApplicationStage.PH.AddressForm && model.UserAction == UserAction.Next) {
                application = CopyModel<ProductApplication>(model.ProductApplication);
                application.Id = model.ApplicationId;
                #region Setup Address
                if (model.ProductApplication.BillingAddressIsChecked == YesNo.Yes) {
                    application.BillingAddressBranchType = NullUtils.cvInt(model.ProductApplication.AddressBranchType);
                    application.BillingAddressBranchNo = NullUtils.cvString(model.ProductApplication.AddressBranchNo);
                    application.BillingAddressNo = NullUtils.cvString(model.ProductApplication.AddressNo);
                    application.BillingAddressMoo = NullUtils.cvString(model.ProductApplication.AddressMoo);
                    application.BillingAddressVillage = NullUtils.cvString(model.ProductApplication.AddressVillage);
                    application.BillingAddressFloor = NullUtils.cvString(model.ProductApplication.AddressFloor);
                    application.BillingAddressSoi = NullUtils.cvString(model.ProductApplication.AddressSoi);
                    application.BillingAddressRoad = NullUtils.cvString(model.ProductApplication.AddressRoad);
                    application.BillingAddressDistrict = NullUtils.cvString(model.ProductApplication.AddressDistrict);
                    application.BillingAddressSubDistrict = NullUtils.cvString(model.ProductApplication.AddressSubDistrict);
                    application.BillingAddressProvince = NullUtils.cvString(model.ProductApplication.AddressProvince);
                    application.BillingAddressPostalCode = NullUtils.cvString(model.ProductApplication.AddressPostalCode);
                }

                if (model.ProductApplication.ShippingAddressIsChecked == YesNo.Yes) {
                    application.ShippingAddressBranchType = NullUtils.cvInt(model.ProductApplication.AddressBranchType);
                    application.ShippingAddressBranchNo = NullUtils.cvString(model.ProductApplication.AddressBranchNo);
                    application.ShippingAddressNo = NullUtils.cvString(model.ProductApplication.AddressNo);
                    application.ShippingAddressMoo = NullUtils.cvString(model.ProductApplication.AddressMoo);
                    application.ShippingAddressVillage = NullUtils.cvString(model.ProductApplication.AddressVillage);
                    application.ShippingAddressFloor = NullUtils.cvString(model.ProductApplication.AddressFloor);
                    application.ShippingAddressSoi = NullUtils.cvString(model.ProductApplication.AddressSoi);
                    application.ShippingAddressRoad = NullUtils.cvString(model.ProductApplication.AddressRoad);
                    application.ShippingAddressDistrict = NullUtils.cvString(model.ProductApplication.AddressDistrict);
                    application.ShippingAddressSubDistrict = NullUtils.cvString(model.ProductApplication.AddressSubDistrict);
                    application.ShippingAddressProvince = NullUtils.cvString(model.ProductApplication.AddressProvince);
                    application.ShippingAddressPostalCode = NullUtils.cvString(model.ProductApplication.AddressPostalCode);
                }
                #endregion
                appRepo.UpdateProductApplicationAddressForm(application);
                application = appRepo.GetProductApplicationForm(application.Id);
            }

            var path = Server.MapPath(System.Web.Configuration.WebConfigurationManager.AppSettings.Get("Path_Upload"));
            if (State == ProductApplicationStage.PH.AttachmentForm && model.UserAction == UserAction.Next) {
                #region Upload Files
                List<ProductApplicationAttachment> attachments = new List<ProductApplicationAttachment>();

                MoveOn &= UploadAttachmentFile(model.ApplicationId, model.ProductId, model.File1, FileCategory.File1, application, attachments);
                MoveOn &= UploadAttachmentFile(model.ApplicationId, model.ProductId, model.File2, FileCategory.File2, application, attachments);
                MoveOn &= UploadAttachmentFile(model.ApplicationId, model.ProductId, model.File3, FileCategory.File3, application, attachments);
                MoveOn &= UploadAttachmentFile(model.ApplicationId, model.ProductId, model.File4, FileCategory.File4, application, attachments);
                MoveOn &= UploadAttachmentFile(model.ApplicationId, model.ProductId, model.File5, FileCategory.File5, application, attachments);
                MoveOn &= UploadAttachmentFile(model.ApplicationId, model.ProductId, model.File6, FileCategory.File6, application, attachments);
                
                if (attachments.Count > 0) { appRepo.AddProductApplicationAttachments(attachments); }
                #endregion
            }

            if (State == ProductApplicationStage.Base.Final && model.UserAction == UserAction.Next) {
                #region Promotion And Coupon
                var promotionId = NullUtils.cvInt(model.ProductApplication.PromotionId);
                PromotionItem promotion = null; bool hasGift = false;
                ClearPromotionAndCoupon(application);
                if (promotionId > 0 && productItem.Promotion != null) {
                    promotion = productItem.Promotion.Where(prm => prm.Id == promotionId).FirstOrDefault();
                    if (promotion != null) {
                        application.PromotionId = promotionId;
                        var discount = promotion.DiscountAmount;
                        if (promotion.DiscountType == PromotionDiscountType.Amount) {
                            model.TotalDiscount = (discount > application.DisplayPremium ? 0 : discount);
                            UpdatePromotionAndCoupon(application, model.TotalDiscount, promotionId, promotion.CampaignId, string.Empty);
                            hasGift = true;
                        } else {
                            model.TotalDiscount = ((application.DisplayPremium * discount) / 100);
                            UpdatePromotionAndCoupon(application, model.TotalDiscount, promotionId, promotion.CampaignId, string.Empty);
                            hasGift = true;
                        }
                    }
                }

                var systemMessageRepository = new SystemMessageRepository();
                var messages = systemMessageRepository.ListSystemMessage();
                var message = new SystemMessage();
                var couponCode = NullUtils.cvString(model.ProductApplication.CouponCode);
                Coupon coupon = null;

                if (couponCode.Length > 0) {
                    coupon = appRepo.GetCoupon_Active(couponCode, application.ProductId);
                    if (coupon != null) {
                        var checkedDiscount = (promotion != null && promotion.PromotionType == PromotionType.Discount);
                        if (coupon.CouponType == CouponType.OneTime) {
                            if (coupon.TotalUsed == 0) {
                                if (!checkedDiscount) {
                                    if (coupon.DiscountType == CouponDiscountType.Amount) {
                                        model.TotalDiscount += coupon.DiscountAmount;
                                        UpdatePromotionAndCoupon(application, model.TotalDiscount, hasGift ? promotionId : 0, promotion.CampaignId, couponCode);
                                        model.ProductApplication.CouponCode = couponCode;
                                    } else {
                                        model.TotalDiscount += (application.DisplayPremium * (coupon.DiscountAmount) / 100);
                                        UpdatePromotionAndCoupon(application, model.TotalDiscount, hasGift ? promotionId : 0, promotion.CampaignId, couponCode);
                                        model.ProductApplication.CouponCode = couponCode;
                                    }
                                }
                            } else {
                                ClearPromotionAndCoupon(application);
                                model.ProductApplication = application;
                                model.TotalDiscount = 0;
                                message = messages.Where(o => o.Category == SystemMessageCategory.Coupon && o.Code == ((int)SystemMessageStatus.CouponUsed).ToString()).FirstOrDefault();
                                ViewBag.CouponError = Language == Language.TH ? message.BodyTH : message.BodyEN;
                                return View(Language.ToString() + "/PH", model);
                            }
                        }
                        if (coupon.CouponType == CouponType.Mass) {
                            if (!checkedDiscount) {
                                if (coupon.TotalUsed < coupon.Total) {
                                    if (coupon.DiscountType == CouponDiscountType.Amount) {
                                        model.TotalDiscount += coupon.DiscountAmount;
                                        UpdatePromotionAndCoupon(application, model.TotalDiscount, hasGift ? promotionId : 0, promotion.CampaignId, couponCode);
                                        model.ProductApplication.CouponCode = couponCode;
                                    } else {
                                        model.TotalDiscount += (application.DisplayPremium * (coupon.DiscountAmount) / 100);
                                        UpdatePromotionAndCoupon(application, model.TotalDiscount, hasGift ? promotionId : 0, promotion.CampaignId, couponCode);
                                        model.ProductApplication.CouponCode = couponCode;
                                    }
                                } else {
                                    ClearPromotionAndCoupon(application);
                                    model.ProductApplication = application;
                                    model.TotalDiscount = 0;
                                    message = messages.Where(o => o.Category == SystemMessageCategory.Coupon && o.Code == ((int)SystemMessageStatus.CouponUsed).ToString()).FirstOrDefault();
                                    ViewBag.CouponError = Language == Language.TH ? message.BodyTH : message.BodyEN;
                                    return View(Language.ToString() + "/PH", model);
                                }
                            } else {
                                ClearPromotionAndCoupon(application);
                                model.ProductApplication = application;
                                model.TotalDiscount = 0;
                                message = messages.Where(o => o.Category == SystemMessageCategory.Coupon && o.Code == ((int)SystemMessageStatus.CouponTopUp).ToString()).FirstOrDefault();
                                ViewBag.CouponError = Language == Language.TH ? message.BodyTH : message.BodyEN;
                                return View(Language.ToString() + "/PH", model);
                            }
                        }
                    } else {
                        ClearPromotionAndCoupon(application);
                        model.ProductApplication = application;
                        model.TotalDiscount = 0;
                        model.ProductApplication.CouponCode = string.Empty;
                        message = messages.Where(o => o.Category == SystemMessageCategory.Coupon && o.Code == ((int)SystemMessageStatus.CouponUnKnown).ToString()).FirstOrDefault();
                        ViewBag.CouponError = Language == Language.TH ? message.BodyTH : message.BodyEN;
                        return View(Language.ToString() + "/PH", model);
                    }
                }

                var currentAppForm = appRepo.GetProductApplicationForm(application.Id);
                application.CouponCode = NullUtils.cvString(currentAppForm.CouponCode);
                application.TotalDiscount = currentAppForm.TotalDiscount;
                RecheckPromotion_Active(promotionId, productItem, application);
                RecheckCoupon_Active(couponCode, application);
                #endregion
            }

            model.ProductApplicationPolicyHolders = application.ProductApplicationPolicyHolders != null && application.ProductApplicationPolicyHolders.Count > 0
                ? application.ProductApplicationPolicyHolders.ToArray() : new ProductApplicationPolicyHolder[1] { model.ProductApplicationPolicyHolder };
            model.ProductApplicationAttachments = application.ProductApplicationAttachment != null && application.ProductApplicationAttachment.Count > 0
                ? application.ProductApplicationAttachment : new List<ProductApplicationAttachment>();

            #region Validate

            MoveOn &= ValidateOtp(application.Id, model.ProductApplicationPolicyHolders);
            MoveOn &= ValidateStringAlphanum(application.ContactName);
            MoveOn &= ValidateStringAlphanum(application.ContactSurname);
            MoveOn &= ValidateStringAlphanumTel(application.ContactTelephone);
            MoveOn &= ValidateStringAlphanumTel(application.ContactMobilePhone);
            MoveOn &= ValidateStringAlphanumEmail(application.ContactEmail);
            MoveOn &= ValidateStringAlphanum(application.ContactLineId);

            MoveOn &= ValidateStringAlphanum(application.BuyerName);
            MoveOn &= ValidateStringAlphanum(application.BuyerSurname);
            MoveOn &= ValidateStringAlphanumTel(application.BuyerTelephone);
            MoveOn &= ValidateStringAlphanumTel(application.BuyerMobilePhone);
            MoveOn &= ValidateStringAlphanumEmail(application.BuyerEmail);
            MoveOn &= ValidateStringAlphanum(application.BuyerLineId);

            MoveOn &= ValidateStringAlphanum(application.AddressBranchNo);
            MoveOn &= ValidateStringAlphanumAddress(application.AddressNo);
            MoveOn &= ValidateStringAlphanum(application.AddressMoo);
            MoveOn &= ValidateStringAlphanum(application.AddressVillage);
            MoveOn &= ValidateStringNumeric(application.AddressFloor);
            MoveOn &= ValidateStringAlphanum(application.AddressSoi);
            MoveOn &= ValidateStringAlphanum(application.AddressRoad);
            MoveOn &= ValidateStringAlphanum(application.AddressPostalCode);

            MoveOn &= ValidateStringAlphanum(application.BillingAddressBranchNo);
            MoveOn &= ValidateStringAlphanumAddress(application.BillingAddressNo);
            MoveOn &= ValidateStringAlphanum(application.BillingAddressMoo);
            MoveOn &= ValidateStringAlphanum(application.BillingAddressVillage);
            MoveOn &= ValidateStringNumeric(application.BillingAddressFloor);
            MoveOn &= ValidateStringAlphanum(application.BillingAddressSoi);
            MoveOn &= ValidateStringAlphanum(application.BillingAddressRoad);
            MoveOn &= ValidateStringAlphanum(application.BillingAddressPostalCode);

            MoveOn &= ValidateStringAlphanum(application.ShippingAddressBranchNo);
            MoveOn &= ValidateStringAlphanumAddress(application.ShippingAddressNo);
            MoveOn &= ValidateStringAlphanum(application.ShippingAddressMoo);
            MoveOn &= ValidateStringAlphanum(application.ShippingAddressVillage);
            MoveOn &= ValidateStringNumeric(application.ShippingAddressFloor);
            MoveOn &= ValidateStringAlphanum(application.ShippingAddressSoi);
            MoveOn &= ValidateStringAlphanum(application.ShippingAddressRoad);
            MoveOn &= ValidateStringAlphanum(application.ShippingAddressPostalCode);

            foreach (var it in model.ProductApplicationPolicyHolders) {
                MoveOn &= ValidateStringAlphanum(it.Name);
                MoveOn &= ValidateStringAlphanum(it.Surname);
                MoveOn &= ValidateStringAlphanum(it.RefIDCard);
                MoveOn &= ValidateStringAlphanum(it.IDCard);
                MoveOn &= ValidateStringAlphanumTel(it.Telephone);
                MoveOn &= ValidateStringAlphanumTel(it.MobilePhone);
                MoveOn &= ValidateStringAlphanumEmail(it.Email);
                MoveOn &= ValidateStringAlphanum(it.BeneficiaryName);
                MoveOn &= ValidateStringAlphanum(it.BeneficiarySurname);
                MoveOn &= ValidateStringAlphanum(it.BeneficiaryIDCard);
                MoveOn &= ValidateStringAlphanumTel(it.BeneficiaryMobilePhone);
                MoveOn &= ValidateStringAlphanum(it.LineId);
                MoveOn &= ValidateStringAlphanum(it.Instagram);
                MoveOn &= ValidateStringAlphanum(it.Facebook);
            }

            #endregion

            if (State != ProductApplicationStage.Base.Success || (State == ProductApplicationStage.Base.Success && model.UserAction == UserAction.Back)) {
                if (MoveOn) {
                    if (model.UserAction == UserAction.Next) { State = steps.Pop(); overSteps.Push(State); } else { State = overSteps.Pop(); steps.Push(State); };
                    model.State = model.UserAction == UserAction.Next ? steps.Peek() : State;
                    application.Steps = steps != null ? string.Join("-", steps.ToList().Select(x => (int)x).Reverse().ToArray()) : string.Empty;
                    application.OverSteps = overSteps != null ? string.Join("-", overSteps.ToList().Select(x => (int)x).Reverse().ToArray()) : string.Empty;
                    application.Step = (int)model.State;

                    appRepo.UpdateProductApplicationState(application);
                }
                model.ProductApplication = application;

            }

            if (State == ProductApplicationStage.Base.Success && model.UserAction == UserAction.Next) {
                var couponRepo = new CouponRepository();
                var order = GenerateOrder(application, ORDER_NON_MOTOR, leadForm != null ? leadForm.Id : 0);
                if (order != null) {
                    couponRepo.UpdateUsedCouponCode(NullUtils.cvString(application.CouponCode), order.Id);
                }

                State = ProductApplicationStage.Base.Success;
                model.State = State;
                application.Step = (int)model.State;
                application.Language = Language;
                appRepo.UpdateProductApplicationState(application);
                return Redirect(Url.Action("PaymentRequest") + "/?ApplicationId=" + application.Id);
            }
            return View(Language.ToString() + "/PH", model);
        }
        #endregion

        #region TA

        [ActionName("ta")]
        public ActionResult TA(OrderProductViewModel item) {
            if (!ValidateOrderToken()) { return Redirect(Url.Content("~")); }
            int applicationId = NullUtils.cvInt(Request.QueryString.Get("ApplicationId"), -1);
            ProductApplicationRepository appRepo = new ProductApplicationRepository();
            CountryRepository countryRepo = new CountryRepository();
            ProvinceRepository provinceRepo = new ProvinceRepository();
            MasterOptionRepository masterRepo = new MasterOptionRepository();
            ProductRepository productRepo = new ProductRepository();
            LeadFormRepository leadRepo = new LeadFormRepository();
            CareerRepository careerRepo = new CareerRepository();
            ProductApplicationFieldRepository productAppFieldRepo = new ProductApplicationFieldRepository();


            ProductApplication application = applicationId != -1 ? appRepo.GetProductApplicationForm(NullUtils.cvInt(applicationId)) : null;
            if (application == null || item.ProductId == 0) { return Redirect(Url.Content("~")); }

            OrderViewModel model = new OrderViewModel();

            Stack<int> steps = ConvertToApplicationSteps(application.Steps);
            if ((steps == null || steps.Count == 0)) { return Redirect(Url.Content("~")); }

            model.PremiumId = application.PremiumId;
            model.ProductId = application.ProductId;
            model.ProductCategoryKey = GetProductCategory(model.ProductId);
            model.State = steps.Peek();
            model.Page = model.ProductCategoryKey.ToString();
            model.ApplicationId = applicationId;
            model.Token = GenerateOrderToken(model.ApplicationId, model.ProductId);

            if (application.ProductApplicationTA == null) {
                model.ProductApplicationTA.CoverageType = NullUtils.cvInt(Request.QueryString.Get("CoverageType"));
                model.ProductApplicationTA.CoverageOption = NullUtils.cvInt(Request.QueryString.Get("CoverageOption"));
                model.ProductApplicationTA.Country = NullUtils.cvInt(Request.QueryString.Get("Country"));
                model.ProductApplicationTA.TravelType = NullUtils.cvInt(Request.QueryString.Get("TripType"));
                model.ProductApplicationTA.Day = NullUtils.cvInt(Request.QueryString.Get("Day"));
                model.ProductApplicationTA.Persons = NullUtils.cvInt(Request.QueryString.Get("Person"));
                model.ProductApplicationTA.DepartureDate = ValueUtils.GetDate(Request.QueryString.Get("DateFrom"));
                model.ProductApplicationTA.ReturnDate = ValueUtils.GetDate(Request.QueryString.Get("DateTo"));
            } else {
                model.ProductApplicationTA = application.ProductApplicationTA;
            }
            
            if (model.ProductApplicationTA.Day <= 0) {
                model.ProductApplicationTA.Day = (model.ProductApplicationTA.ReturnDate - model.ProductApplicationTA.DepartureDate).Days + 1;
            }

            #region Lead Form
            var leadForm = GetLeadForm();
            if (leadForm != null) {
                var policyHolder = model.ProductApplicationPolicyHolder;
                policyHolder.Name = leadForm.Name;
                policyHolder.Surname = leadForm.Surname;
                policyHolder.MobilePhone = leadForm.Tel;
                policyHolder.Email = leadForm.Email;
            }
            #endregion

            ProductItem productItem = productRepo.GetProductTA_Active(Language, application.ProductId, application.PremiumId);
            if (productItem == null || CheckOrderStatus(application.Id) || !productItem.SaleChannel.HasFlag(TobJod.Models.ProductSaleChannel.Online)) { return Redirect(Url.Content("~")); }
            var productPremium = productItem.ProductTAPremium[0];
            var productName = Language == Language.TH ? productItem.TitleTH : productItem.TitleEN;
            model.ProductItem = productItem;
            model.ProductItem.CompanyImage = PathCompany + productItem.CompanyImage;

            application.ProductCode = NullUtils.cvString(productItem.ProductCode);
            application.ProductName = NullUtils.cvString(productName);
            application.SumInsured = productPremium.SumInsured;
            application.NetPremium = productPremium.NetPremium;
            application.DisplayPremium = productPremium.DisplayPremium;
            application.EPolicy = productPremium.EPolicy ? (int)TobJod.Models.AllYesNo.Yes : (int)TobJod.Models.AllYesNo.No;

            var productApplicationFields = productAppFieldRepo.ListProductApplicationFieldByProductCategoryKey(model.ProductCategoryKey, Language);
            model.ProductApplicationFields = productApplicationFields;

            #region DropDownList
            model.CountriesTA = appRepo.GetProductTACountries(application.ProductId);
            model.Countries = countryRepo.ListCountry_SelectList_Active(Language);
            model.Salutations = masterRepo.ListMasterOption_SelectList_Active(Language, MasterOptionCategory.Salutation);
            model.Relationships = masterRepo.ListMasterOption_SelectList_Active(Language, MasterOptionCategory.Relationship);
            model.TravelMethods = masterRepo.ListMasterOption_SelectList_Active(Language, MasterOptionCategory.TravelMethod);
            model.Careers = careerRepo.ListCareer_SelectList_Active(Language);
            model.Provinces = provinceRepo.ListProvince_SelectList_Active(Language);
            #endregion

            var date = application.ProductApplicationTA == null ? model.ProductApplicationTA.DepartureDate : application.ProductApplicationTA.DepartureDate;
            var dateExpire = application.ProductApplicationTA == null ? model.ProductApplicationTA.ReturnDate : application.ProductApplicationTA.ReturnDate;// date.AddYears(1);
            application.ActiveDate = date;
            application.ExpireDate = dateExpire;
            model.PolicyHolder_Size = POLICY_HOLDER_SIZE;
            model.Driver_Size = DRIVER_SIZE;

            model.ProductApplicationPolicyHolders = application.ProductApplicationPolicyHolders != null && application.ProductApplicationPolicyHolders.Count > 0
                ? application.ProductApplicationPolicyHolders.ToArray() : new ProductApplicationPolicyHolder[1] { model.ProductApplicationPolicyHolder };
            model.ProductApplicationAttachments = application.ProductApplicationAttachment != null && application.ProductApplicationAttachment.Count > 0
                ? application.ProductApplicationAttachment : new List<ProductApplicationAttachment>();

            model.MinAge = MIN_AGE;
            model.MaxAge = MAX_AGE;

            application.Step = steps.Peek();
            if (application.Step != ProductApplicationStage.Base.Success) {
                appRepo.UpdateProductApplication(application);
            }
            model.ProductApplication = application;

            return View(Language.ToString() + "/TA", model);
        }

        [ActionName("ta")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult TA_Save(OrderViewModel model) {
            if (!ValidateOrderToken()) { return Redirect(Url.Content("~")); }
            bool MoveOn = true;
            ProductApplicationRepository appRepo = new ProductApplicationRepository();
            CountryRepository countryRepo = new CountryRepository();
            ProvinceRepository provinceRepo = new ProvinceRepository();
            MasterOptionRepository masterRepo = new MasterOptionRepository();
            ProductRepository productRepo = new ProductRepository();
            LeadFormRepository leadRepo = new LeadFormRepository();
            CareerRepository careerRepo = new CareerRepository();
            DistrictRepository districtRepo = new DistrictRepository();
            SubDistrictRepository subDistrictRepo = new SubDistrictRepository();

            ProductApplication application = model != null ? appRepo.GetProductApplicationForm(NullUtils.cvInt(model.ApplicationId)) : null;
            if (application == null) { return Redirect(Url.Content("~")); }

            int lastStep = application.Step;
            int State = ProductApplicationStage.Base.Unknown;
            Stack<int> steps = ConvertToApplicationSteps(application.Steps);
            Stack<int> overSteps = ConvertToApplicationSteps(application.OverSteps);
            if (steps == null || steps.Count == 0) { return Redirect(Url.Content("~")); }
            if (model.UserAction == UserAction.Next) { State = steps.Peek(); };
            if (model.UserAction == UserAction.Back) { State = overSteps.Any() ? overSteps.Peek() : State; }

            ProductItem productItem = productRepo.GetProductTA_Active(Language, application.ProductId, application.PremiumId);
            if (productItem == null || CheckOrderStatus(application.Id) || !productItem.SaleChannel.HasFlag(TobJod.Models.ProductSaleChannel.Online)) { return Redirect(Url.Content("~")); }
            var productPremium = productItem.ProductTAPremium[0];
            model.ProductItem = productItem;
            model.ProductItem.CompanyImage = PathCompany + productItem.CompanyImage;

            ProductApplicationFieldRepository productAppFieldRepo = new ProductApplicationFieldRepository();
            var productApplicationFields = productAppFieldRepo.ListProductApplicationFieldByProductCategoryKey(model.ProductCategoryKey, Language);
            model.ProductApplicationFields = productApplicationFields;

            model.PolicyHolder_Size = POLICY_HOLDER_SIZE;
            model.Driver_Size = DRIVER_SIZE;
            model.MinAge = MIN_AGE;
            model.MaxAge = MAX_AGE;
            model.Page = model.ProductCategoryKey.ToString();
            model.State = State;
            model.ApplicationId = model.ApplicationId;

            #region DropDownList
            model.CountriesTA = appRepo.GetProductTACountries(application.ProductId);
            model.Countries = countryRepo.ListCountry_SelectList_Active(Language);
            model.Relationships = masterRepo.ListMasterOption_SelectList_Active(Language, MasterOptionCategory.Relationship);
            model.Salutations = masterRepo.ListMasterOption_SelectList_Active(Language, MasterOptionCategory.Salutation);
            model.TravelMethods = masterRepo.ListMasterOption_SelectList_Active(Language, MasterOptionCategory.TravelMethod);
            model.Careers = careerRepo.ListCareer_SelectList_Active(Language);
            model.Provinces = provinceRepo.ListProvince_SelectList_Active(Language);
            #endregion

            #region DropDownList: District, SubDistrict
            var districts = districtRepo.ListDistrict_SelectList_Active(NullUtils.cvInt(application.AddressProvince), Language);
            var subDistricts = subDistrictRepo.ListSubDistrict_Active(NullUtils.cvInt(application.AddressDistrict), Language);
            var billing_districts = districtRepo.ListDistrict_SelectList_Active(NullUtils.cvInt(application.BillingAddressProvince), Language);
            var billing_subDistricts = subDistrictRepo.ListSubDistrict_Active(NullUtils.cvInt(application.BillingAddressDistrict), Language);
            var shipping_districts = districtRepo.ListDistrict_SelectList_Active(NullUtils.cvInt(application.ShippingAddressProvince), Language);
            var shipping_subDistricts = subDistrictRepo.ListSubDistrict_Active(NullUtils.cvInt(application.ShippingAddressDistrict), Language);
            model.Districts = districts != null ? districts : new List<SelectListItem>();
            model.SubDistricts = subDistricts != null ? subDistricts.Select<SubDistrict, SelectListItem>(s => new SelectListItem { Value = s.Id.ToString(), Text = Language == Language.TH ? s.TitleTH : s.TitleEN }).ToList<SelectListItem>() : new List<SelectListItem>();
            model.BillingDistricts = billing_districts != null ? billing_districts : new List<SelectListItem>();
            model.BillingSubDistricts = billing_subDistricts != null ? billing_subDistricts.Select<SubDistrict, SelectListItem>(s => new SelectListItem { Value = s.Id.ToString(), Text = Language == Language.TH ? s.TitleTH : s.TitleEN }).ToList<SelectListItem>() : new List<SelectListItem>();
            model.ShippingDistricts = shipping_districts != null ? shipping_districts : new List<SelectListItem>();
            model.ShippingSubDistricts = shipping_subDistricts != null ? shipping_subDistricts.Select<SubDistrict, SelectListItem>(s => new SelectListItem { Value = s.Id.ToString(), Text = Language == Language.TH ? s.TitleTH : s.TitleEN }).ToList<SelectListItem>() : new List<SelectListItem>();
            #endregion

            #region Lead Form

            var leadForm = GetLeadForm();
            if (leadForm != null) {
                var policyHolder = model.ProductApplicationPolicyHolder;
                policyHolder.Name = leadForm.Name;
                policyHolder.Surname = leadForm.Surname;
                policyHolder.MobilePhone = leadForm.Tel;
                policyHolder.Email = leadForm.Email;
            }

            #endregion

            if (NullUtils.cvInt(Request.Form["State"]) != application.Step) {
                return RedirectToAction("ta", new { ApplicationId = application.Id, ProductId = application.ProductId, Token = model.Token });
            }

            if (State == ProductApplicationStage.TA.TAForm && model.UserAction == UserAction.Next) {
                ProductApplicationTA taModel = new ProductApplicationTA();
                taModel = CopyModel<ProductApplicationTA>(model.ProductApplicationTA);
                taModel.ApplicationId = application.Id;
                taModel.DepartureDate = ValueUtils.GetDate(HttpUtils.GetFormSingle("ProductApplicationTA[DepartureDate]"));
                taModel.ReturnDate = ValueUtils.GetDate(HttpUtils.GetFormSingle("ProductApplicationTA[ReturnDate]"));

                if (taModel.Day <= 0) {
                    taModel.Day = (taModel.ReturnDate - taModel.DepartureDate).Days + 1;
                }

                if (application.ProductApplicationTA == null) {
                    appRepo.AddProductApplicationTA(taModel);
                } else {
                    appRepo.UpdateProductApplicationTA(taModel);
                }
            }

            if (State == ProductApplicationStage.TA.PolicyHolderForm && model.UserAction == UserAction.Next) {
                List<ProductApplicationPolicyHolder> policyHolders = new List<ProductApplicationPolicyHolder>();
                for (var i = 0; model.ProductApplicationPolicyHolders != null && i < model.ProductApplicationPolicyHolders.Length; i++) {
                    var policyHolderItem = model.ProductApplicationPolicyHolders[i];
                    if (NullUtils.cvInt(policyHolderItem.TitleId) > 0) {
                        ProductApplicationPolicyHolder policyHolderModel = new ProductApplicationPolicyHolder();
                        policyHolderModel = CopyModel<ProductApplicationPolicyHolder>(policyHolderItem);
                        policyHolderModel.ApplicationId = model.ApplicationId;
                        policyHolderModel.Sequence = (i + 1);
                        policyHolderModel.Birthday = ValueUtils.GetDate(Request.Form["ProductApplicationPolicyHolders[" + i + "].Birthday"]);
                        policyHolders.Add(policyHolderModel);
                    }
                }

                if (policyHolders.Count > 0) {
                    appRepo.AddProductApplicationPolicyHolders(policyHolders);
                    model.ProductApplicationPolicyHolders = policyHolders.ToArray();
                }

                application.ContactName = NullUtils.cvString(model.ProductApplication.ContactName);
                application.ContactSurname = NullUtils.cvString(model.ProductApplication.ContactSurname);
                application.ContactTelephone = NullUtils.cvString(model.ProductApplication.ContactTelephone);
                application.ContactMobilePhone = NullUtils.cvString(model.ProductApplication.ContactMobilePhone);
                application.ContactEmail = NullUtils.cvString(model.ProductApplication.ContactEmail);

                application.BuyerName = NullUtils.cvString(model.ProductApplication.BuyerName);
                application.BuyerSurname = NullUtils.cvString(model.ProductApplication.BuyerSurname);
                application.BuyerTelephone = NullUtils.cvString(model.ProductApplication.BuyerTelephone);
                application.BuyerMobilePhone = NullUtils.cvString(model.ProductApplication.BuyerMobilePhone);
                application.BuyerEmail = NullUtils.cvString(model.ProductApplication.BuyerEmail);

                application.ActiveDate = ValueUtils.GetDate(Request.Form["ProductApplication[ActiveDate]"]);
                application.ExpireDate = ValueUtils.GetDate(Request.Form["ProductApplication[ExpireDate]"]);

                appRepo.UpdateProductApplication(application);
            } else {
                model.ProductApplicationPolicyHolders = application.ProductApplicationPolicyHolders != null && application.ProductApplicationPolicyHolders.Count > 0 ?
                    application.ProductApplicationPolicyHolders.ToArray() : new ProductApplicationPolicyHolder[1] { model.ProductApplicationPolicyHolder };
            }

            if (State == ProductApplicationStage.TA.AddressForm && model.UserAction == UserAction.Next) {
                application = CopyModel<ProductApplication>(model.ProductApplication);
                application.Id = model.ApplicationId;
                #region Setup Address
                if (model.ProductApplication.BillingAddressIsChecked == YesNo.Yes) {
                    application.BillingAddressBranchType = NullUtils.cvInt(model.ProductApplication.AddressBranchType);
                    application.BillingAddressBranchNo = NullUtils.cvString(model.ProductApplication.AddressBranchNo);
                    application.BillingAddressNo = NullUtils.cvString(model.ProductApplication.AddressNo);
                    application.BillingAddressMoo = NullUtils.cvString(model.ProductApplication.AddressMoo);
                    application.BillingAddressVillage = NullUtils.cvString(model.ProductApplication.AddressVillage);
                    application.BillingAddressFloor = NullUtils.cvString(model.ProductApplication.AddressFloor);
                    application.BillingAddressSoi = NullUtils.cvString(model.ProductApplication.AddressSoi);
                    application.BillingAddressRoad = NullUtils.cvString(model.ProductApplication.AddressRoad);
                    application.BillingAddressDistrict = NullUtils.cvString(model.ProductApplication.AddressDistrict);
                    application.BillingAddressSubDistrict = NullUtils.cvString(model.ProductApplication.AddressSubDistrict);
                    application.BillingAddressProvince = NullUtils.cvString(model.ProductApplication.AddressProvince);
                    application.BillingAddressPostalCode = NullUtils.cvString(model.ProductApplication.AddressPostalCode);
                }

                if (model.ProductApplication.ShippingAddressIsChecked == YesNo.Yes) {
                    application.ShippingAddressBranchType = NullUtils.cvInt(model.ProductApplication.AddressBranchType);
                    application.ShippingAddressBranchNo = NullUtils.cvString(model.ProductApplication.AddressBranchNo);
                    application.ShippingAddressNo = NullUtils.cvString(model.ProductApplication.AddressNo);
                    application.ShippingAddressMoo = NullUtils.cvString(model.ProductApplication.AddressMoo);
                    application.ShippingAddressVillage = NullUtils.cvString(model.ProductApplication.AddressVillage);
                    application.ShippingAddressFloor = NullUtils.cvString(model.ProductApplication.AddressFloor);
                    application.ShippingAddressSoi = NullUtils.cvString(model.ProductApplication.AddressSoi);
                    application.ShippingAddressRoad = NullUtils.cvString(model.ProductApplication.AddressRoad);
                    application.ShippingAddressDistrict = NullUtils.cvString(model.ProductApplication.AddressDistrict);
                    application.ShippingAddressSubDistrict = NullUtils.cvString(model.ProductApplication.AddressSubDistrict);
                    application.ShippingAddressProvince = NullUtils.cvString(model.ProductApplication.AddressProvince);
                    application.ShippingAddressPostalCode = NullUtils.cvString(model.ProductApplication.AddressPostalCode);
                }
                #endregion
                appRepo.UpdateProductApplicationAddressForm(application);
                application = appRepo.GetProductApplicationForm(application.Id);
            }

            var path = Server.MapPath(System.Web.Configuration.WebConfigurationManager.AppSettings.Get("Path_Upload"));
            if (State == ProductApplicationStage.TA.AttachmentForm && model.UserAction == UserAction.Next) {
                #region Upload Files
                List<ProductApplicationAttachment> attachments = new List<ProductApplicationAttachment>();
                
                MoveOn &= UploadAttachmentFile(model.ApplicationId, model.ProductId, model.File1, FileCategory.File1, application, attachments);
                MoveOn &= UploadAttachmentFile(model.ApplicationId, model.ProductId, model.File2, FileCategory.File2, application, attachments);
                MoveOn &= UploadAttachmentFile(model.ApplicationId, model.ProductId, model.File3, FileCategory.File3, application, attachments);
                MoveOn &= UploadAttachmentFile(model.ApplicationId, model.ProductId, model.File4, FileCategory.File4, application, attachments);
                MoveOn &= UploadAttachmentFile(model.ApplicationId, model.ProductId, model.File5, FileCategory.File5, application, attachments);
                MoveOn &= UploadAttachmentFile(model.ApplicationId, model.ProductId, model.File6, FileCategory.File6, application, attachments);
                
                if (attachments.Count > 0) { appRepo.AddProductApplicationAttachments(attachments); }
                #endregion
            }

            if (State == ProductApplicationStage.Base.Final && model.UserAction == UserAction.Next) {
                #region Promotion And Coupon
                var promotionId = NullUtils.cvInt(model.ProductApplication.PromotionId);
                PromotionItem promotion = null; bool hasGift = false;
                ClearPromotionAndCoupon(application);
                if (promotionId > 0 && productItem.Promotion != null) {
                    promotion = productItem.Promotion.Where(prm => prm.Id == promotionId).FirstOrDefault();
                    if (promotion != null) {
                        application.PromotionId = promotionId;
                        var discount = promotion.DiscountAmount;
                        if (promotion.DiscountType == PromotionDiscountType.Amount) {
                            model.TotalDiscount = (discount > application.DisplayPremium ? 0 : discount);
                            UpdatePromotionAndCoupon(application, model.TotalDiscount, promotionId, promotion.CampaignId, string.Empty);
                            hasGift = true;
                        } else {
                            model.TotalDiscount = ((application.DisplayPremium * discount) / 100);
                            UpdatePromotionAndCoupon(application, model.TotalDiscount, promotionId, promotion.CampaignId, string.Empty);
                            hasGift = true;
                        }
                    }
                }

                var systemMessageRepository = new SystemMessageRepository();
                var messages = systemMessageRepository.ListSystemMessage();
                var message = new SystemMessage();
                var couponCode = NullUtils.cvString(model.ProductApplication.CouponCode);
                Coupon coupon = null;

                if (couponCode.Length > 0) {
                    coupon = appRepo.GetCoupon_Active(couponCode, application.ProductId);
                    if (coupon != null) {
                        var checkedDiscount = (promotion != null && promotion.PromotionType == PromotionType.Discount);
                        if (coupon.CouponType == CouponType.OneTime) {
                            if (coupon.TotalUsed == 0) {
                                if (!checkedDiscount) {
                                    if (coupon.DiscountType == CouponDiscountType.Amount) {
                                        model.TotalDiscount += coupon.DiscountAmount;
                                        UpdatePromotionAndCoupon(application, model.TotalDiscount, hasGift ? promotionId : 0, promotion.CampaignId, couponCode);
                                        model.ProductApplication.CouponCode = couponCode;
                                    } else {
                                        model.TotalDiscount += (application.DisplayPremium * (coupon.DiscountAmount) / 100);
                                        UpdatePromotionAndCoupon(application, model.TotalDiscount, hasGift ? promotionId : 0, promotion.CampaignId, couponCode);
                                        model.ProductApplication.CouponCode = couponCode;
                                    }
                                }
                            } else {
                                ClearPromotionAndCoupon(application);
                                model.ProductApplication = application;
                                model.TotalDiscount = 0;
                                message = messages.Where(o => o.Category == SystemMessageCategory.Coupon && o.Code == ((int)SystemMessageStatus.CouponUsed).ToString()).FirstOrDefault();
                                ViewBag.CouponError = Language == Language.TH ? message.BodyTH : message.BodyEN;
                                return View(Language.ToString() + "/TA", model);
                            }
                        }
                        if (coupon.CouponType == CouponType.Mass) {
                            if (!checkedDiscount) {
                                if (coupon.TotalUsed < coupon.Total) {
                                    if (coupon.DiscountType == CouponDiscountType.Amount) {
                                        model.TotalDiscount += coupon.DiscountAmount;
                                        UpdatePromotionAndCoupon(application, model.TotalDiscount, hasGift ? promotionId : 0, promotion.CampaignId, couponCode);
                                        model.ProductApplication.CouponCode = couponCode;
                                    } else {
                                        model.TotalDiscount += (application.DisplayPremium * (coupon.DiscountAmount) / 100);
                                        UpdatePromotionAndCoupon(application, model.TotalDiscount, hasGift ? promotionId : 0, promotion.CampaignId, couponCode);
                                        model.ProductApplication.CouponCode = couponCode;
                                    }
                                } else {
                                    ClearPromotionAndCoupon(application);
                                    model.ProductApplication = application;
                                    model.TotalDiscount = 0;
                                    message = messages.Where(o => o.Category == SystemMessageCategory.Coupon && o.Code == ((int)SystemMessageStatus.CouponUsed).ToString()).FirstOrDefault();
                                    ViewBag.CouponError = Language == Language.TH ? message.BodyTH : message.BodyEN;
                                    return View(Language.ToString() + "/TA", model);
                                }
                            } else {
                                ClearPromotionAndCoupon(application);
                                model.ProductApplication = application;
                                model.TotalDiscount = 0;
                                message = messages.Where(o => o.Category == SystemMessageCategory.Coupon && o.Code == ((int)SystemMessageStatus.CouponTopUp).ToString()).FirstOrDefault();
                                ViewBag.CouponError = Language == Language.TH ? message.BodyTH : message.BodyEN;
                                return View(Language.ToString() + "/TA", model);
                            }
                        }
                    } else {
                        ClearPromotionAndCoupon(application);
                        model.ProductApplication = application;
                        model.TotalDiscount = 0;
                        model.ProductApplication.CouponCode = string.Empty;
                        message = messages.Where(o => o.Category == SystemMessageCategory.Coupon && o.Code == ((int)SystemMessageStatus.CouponUnKnown).ToString()).FirstOrDefault();
                        ViewBag.CouponError = Language == Language.TH ? message.BodyTH : message.BodyEN;
                        return View(Language.ToString() + "/TA", model);
                    }
                }

                var currentAppForm = appRepo.GetProductApplicationForm(application.Id);
                application.CouponCode = NullUtils.cvString(currentAppForm.CouponCode);
                application.TotalDiscount = currentAppForm.TotalDiscount;
                RecheckPromotion_Active(promotionId, productItem, application);
                RecheckCoupon_Active(couponCode, application);
                #endregion
            }

            model.ProductApplicationPolicyHolders = application.ProductApplicationPolicyHolders != null && application.ProductApplicationPolicyHolders.Count > 0
                ? application.ProductApplicationPolicyHolders.ToArray() : new ProductApplicationPolicyHolder[1] { model.ProductApplicationPolicyHolder };
            model.ProductApplicationAttachments = application.ProductApplicationAttachment != null && application.ProductApplicationAttachment.Count > 0
                ? application.ProductApplicationAttachment : new List<ProductApplicationAttachment>();
            model.ProductApplicationTA = application.ProductApplicationTA != null
                ? application.ProductApplicationTA : new ProductApplicationTA();

            #region Validate

            MoveOn &= ValidateOtp(application.Id, model.ProductApplicationPolicyHolders);
            MoveOn &= ValidateStringAlphanum(application.ContactName);
            MoveOn &= ValidateStringAlphanum(application.ContactSurname);
            MoveOn &= ValidateStringAlphanumTel(application.ContactTelephone);
            MoveOn &= ValidateStringAlphanumTel(application.ContactMobilePhone);
            MoveOn &= ValidateStringAlphanumEmail(application.ContactEmail);
            MoveOn &= ValidateStringAlphanum(application.ContactLineId);

            MoveOn &= ValidateStringAlphanum(application.BuyerName);
            MoveOn &= ValidateStringAlphanum(application.BuyerSurname);
            MoveOn &= ValidateStringAlphanumTel(application.BuyerTelephone);
            MoveOn &= ValidateStringAlphanumTel(application.BuyerMobilePhone);
            MoveOn &= ValidateStringAlphanumEmail(application.BuyerEmail);
            MoveOn &= ValidateStringAlphanum(application.BuyerLineId);

            MoveOn &= ValidateStringAlphanum(application.AddressBranchNo);
            MoveOn &= ValidateStringAlphanumAddress(application.AddressNo);
            MoveOn &= ValidateStringAlphanum(application.AddressMoo);
            MoveOn &= ValidateStringAlphanum(application.AddressVillage);
            MoveOn &= ValidateStringNumeric(application.AddressFloor);
            MoveOn &= ValidateStringAlphanum(application.AddressSoi);
            MoveOn &= ValidateStringAlphanum(application.AddressRoad);
            MoveOn &= ValidateStringAlphanum(application.AddressPostalCode);

            MoveOn &= ValidateStringAlphanum(application.BillingAddressBranchNo);
            MoveOn &= ValidateStringAlphanumAddress(application.BillingAddressNo);
            MoveOn &= ValidateStringAlphanum(application.BillingAddressMoo);
            MoveOn &= ValidateStringAlphanum(application.BillingAddressVillage);
            MoveOn &= ValidateStringNumeric(application.BillingAddressFloor);
            MoveOn &= ValidateStringAlphanum(application.BillingAddressSoi);
            MoveOn &= ValidateStringAlphanum(application.BillingAddressRoad);
            MoveOn &= ValidateStringAlphanum(application.BillingAddressPostalCode);

            MoveOn &= ValidateStringAlphanum(application.ShippingAddressBranchNo);
            MoveOn &= ValidateStringAlphanumAddress(application.ShippingAddressNo);
            MoveOn &= ValidateStringAlphanum(application.ShippingAddressMoo);
            MoveOn &= ValidateStringAlphanum(application.ShippingAddressVillage);
            MoveOn &= ValidateStringNumeric(application.ShippingAddressFloor);
            MoveOn &= ValidateStringAlphanum(application.ShippingAddressSoi);
            MoveOn &= ValidateStringAlphanum(application.ShippingAddressRoad);
            MoveOn &= ValidateStringAlphanum(application.ShippingAddressPostalCode);

            foreach (var it in model.ProductApplicationPolicyHolders) {
                MoveOn &= ValidateStringAlphanum(it.Name);
                MoveOn &= ValidateStringAlphanum(it.Surname);
                MoveOn &= ValidateStringAlphanum(it.RefIDCard);
                MoveOn &= ValidateStringAlphanum(it.IDCard);
                MoveOn &= ValidateStringAlphanumTel(it.Telephone);
                MoveOn &= ValidateStringAlphanumTel(it.MobilePhone);
                MoveOn &= ValidateStringAlphanumEmail(it.Email);
                MoveOn &= ValidateStringAlphanum(it.BeneficiaryName);
                MoveOn &= ValidateStringAlphanum(it.BeneficiarySurname);
                MoveOn &= ValidateStringAlphanum(it.BeneficiaryIDCard);
                MoveOn &= ValidateStringAlphanumTel(it.BeneficiaryMobilePhone);
                MoveOn &= ValidateStringAlphanum(it.LineId);
                MoveOn &= ValidateStringAlphanum(it.Instagram);
                MoveOn &= ValidateStringAlphanum(it.Facebook);
            }

            #endregion


            if (State != ProductApplicationStage.Base.Success || (State == ProductApplicationStage.Base.Success && model.UserAction == UserAction.Back)) {
                if (MoveOn) {
                    if (model.UserAction == UserAction.Next) { State = steps.Pop(); overSteps.Push(State); } else { State = overSteps.Pop(); steps.Push(State); };
                    model.State = model.UserAction == UserAction.Next ? steps.Peek() : State;
                    application.Steps = steps != null ? string.Join("-", steps.ToList().Select(x => (int)x).Reverse().ToArray()) : string.Empty;
                    application.OverSteps = overSteps != null ? string.Join("-", overSteps.ToList().Select(x => (int)x).Reverse().ToArray()) : string.Empty;
                    application.Step = (int)model.State;

                    appRepo.UpdateProductApplicationState(application);
                }
                model.ProductApplication = application;

            }

            if (State == ProductApplicationStage.Base.Success && model.UserAction == UserAction.Next) {
                var couponRepo = new CouponRepository();
                var order = GenerateOrder(application, ORDER_NON_MOTOR, leadForm != null ? leadForm.Id : 0);
                if (order != null) {
                    couponRepo.UpdateUsedCouponCode(NullUtils.cvString(application.CouponCode), order.Id);
                }

                State = ProductApplicationStage.Base.Success;
                model.State = State;
                application.Step = (int)model.State;
                application.Language = Language;
                appRepo.UpdateProductApplicationState(application);
                return Redirect(Url.Action("PaymentRequest") + "/?ApplicationId=" + application.Id);
            }

            return View(Language.ToString() + "/TA", model);
        }

        #endregion

        #region Home
        [ActionName("home")]
        public ActionResult Home(OrderProductViewModel item) {
            if (!ValidateOrderToken()) { return Redirect(Url.Content("~")); }
            int applicationId = NullUtils.cvInt(Request.QueryString.Get("ApplicationId"), -1);
            ProductApplicationRepository appRepo = new ProductApplicationRepository();
            CountryRepository countryRepo = new CountryRepository();
            ProvinceRepository provinceRepo = new ProvinceRepository();
            MasterOptionRepository masterRepo = new MasterOptionRepository();
            ProductRepository productRepo = new ProductRepository();
            LeadFormRepository leadRepo = new LeadFormRepository();
            ProductApplicationFieldRepository productAppFieldRepo = new ProductApplicationFieldRepository();
            CareerRepository careerRepo = new CareerRepository();
            DistrictRepository districtRepo = new DistrictRepository();
            SubDistrictRepository subDistrictRepo = new SubDistrictRepository();

            ProductApplication application = applicationId != -1 ? appRepo.GetProductApplicationForm(NullUtils.cvInt(applicationId)) : null;
            if (application == null || item.ProductId == 0) { return Redirect(Url.Content("~")); }

            OrderViewModel model = new OrderViewModel();

            Stack<int> steps = ConvertToApplicationSteps(application.Steps);
            if ((steps == null || steps.Count == 0)) { return Redirect(Url.Content("~")); }

            model.PremiumId = application.PremiumId;
            model.ProductId = application.ProductId;
            model.ProductCategoryKey = GetProductCategory(model.ProductId);
            model.State = steps.Peek();
            model.Page = model.ProductCategoryKey.ToString();
            model.ApplicationId = applicationId;
            model.Token = GenerateOrderToken(model.ApplicationId, model.ProductId);

            #region Lead Form
            var leadForm = GetLeadForm();
            if (leadForm != null) {
                var policyHolder = model.ProductApplicationPolicyHolder;
                policyHolder.Name = leadForm.Name;
                policyHolder.Surname = leadForm.Surname;
                policyHolder.MobilePhone = leadForm.Tel;
                policyHolder.Email = leadForm.Email;
            }
            #endregion

            ProductItem productItem = productRepo.GetProductHome_Active(Language, application.ProductId, application.PremiumId);
            if (productItem == null || !productItem.SaleChannel.HasFlag(TobJod.Models.ProductSaleChannel.Online)) { return Redirect(Url.Content("~")); }
            var productPremium = productItem.ProductHomePremium[0];
            var productName = Language == Language.TH ? productItem.TitleTH : productItem.TitleEN;
            model.ProductItem = productItem;
            model.ProductItem.CompanyImage = PathCompany + productItem.CompanyImage;

            application.ProductCode = NullUtils.cvString(productItem.ProductCode);
            application.ProductName = NullUtils.cvString(productName);
            application.SumInsured = productPremium.SumInsured;
            application.NetPremium = productPremium.NetPremium;
            application.DisplayPremium = productPremium.DisplayPremium;
            application.EPolicy = productPremium.EPolicy ? (int)TobJod.Models.AllYesNo.Yes : (int)TobJod.Models.AllYesNo.No;

            var date = DateTime.Now.AddDays(1);
            application.ActiveDate = date;
            application.ExpireDate = date.AddYears(1);

            var constructionTypeId = application.ProductApplicationHome == null ? NullUtils.cvString(Request.Params["constructiontype"]) : application.ProductApplicationHome.ConstructionType.ToString();
            var propertyTypeId = application.ProductApplicationHome == null ? NullUtils.cvString(Request.Params["propertytype"]) : application.ProductApplicationHome.PropertyType.ToString();

            var productApplicationFields = productAppFieldRepo.ListProductApplicationFieldByProductCategoryKey(model.ProductCategoryKey, Language);
            model.ProductApplicationFields = productApplicationFields;
            var constructionType = MainLayoutViewModel.ConstructionTypes.FirstOrDefault(con => con.Value == constructionTypeId);
            var propertyType = MainLayoutViewModel.PropertyTypes.FirstOrDefault(con => con.Value == propertyTypeId);

            model.ConstructionType = constructionType != null ? constructionType.Text : string.Empty;
            model.PropertyType = propertyType != null ? propertyType.Text : string.Empty;

            if (application.ProductApplicationHome == null) {
                model.ProductApplicationHome.ConstructionType = NullUtils.cvInt(Request.Params["constructiontype"]);
                model.ProductApplicationHome.PropertyType = NullUtils.cvInt(Request.Params["propertytype"]);
                model.ProductApplicationHome.ConstructionValue = NullUtils.cvDec(Request.Params["constructionValue"]);
            } else {
                model.ProductApplicationHome = application.ProductApplicationHome;
            }

            #region DrowDownList
            model.Careers = careerRepo.ListCareer_SelectList_Active(Language);
            model.Countries = countryRepo.ListCountry_SelectList_Active(Language);
            model.Salutations = masterRepo.ListMasterOption_SelectList_Active(Language, MasterOptionCategory.Salutation);
            model.Relationships = masterRepo.ListMasterOption_SelectList_Active(Language, MasterOptionCategory.Relationship);
            model.LocationUsages = masterRepo.ListMasterOption_SelectList_Active(Language, MasterOptionCategory.LocationUsage);
            model.Provinces = provinceRepo.ListProvince_SelectList_Active(Language);
            #endregion

            #region DropDownList: District, SubDistrict
            var homeDistricts = application.ProductApplicationHome != null ? districtRepo.ListDistrict_SelectList_Active(NullUtils.cvInt(application.ProductApplicationHome.AddressProvince), Language) : new List<SelectListItem>();
            var homeSubDistricts = application.ProductApplicationHome != null ? subDistrictRepo.ListSubDistrict_Active(NullUtils.cvInt(application.ProductApplicationHome.AddressDistrict), Language) : new List<SubDistrict>();
            var districts = districtRepo.ListDistrict_SelectList_Active(NullUtils.cvInt(application.AddressProvince), Language);
            var subDistricts = subDistrictRepo.ListSubDistrict_Active(NullUtils.cvInt(application.AddressDistrict), Language);
            var billing_districts = districtRepo.ListDistrict_SelectList_Active(NullUtils.cvInt(application.BillingAddressProvince), Language);
            var billing_subDistricts = subDistrictRepo.ListSubDistrict_Active(NullUtils.cvInt(application.BillingAddressDistrict), Language);
            var shipping_districts = districtRepo.ListDistrict_SelectList_Active(NullUtils.cvInt(application.ShippingAddressProvince), Language);
            var shipping_subDistricts = subDistrictRepo.ListSubDistrict_Active(NullUtils.cvInt(application.ShippingAddressDistrict), Language);
            model.HomeDistricts = homeDistricts != null ? homeDistricts : new List<SelectListItem>();
            model.HomeSubDistricts = homeSubDistricts != null ? homeSubDistricts.Select<SubDistrict, SelectListItem>(s => new SelectListItem { Value = s.Id.ToString(), Text = Language == Language.TH ? s.TitleTH : s.TitleEN }).ToList<SelectListItem>() : new List<SelectListItem>();
            model.Districts = districts != null ? districts : new List<SelectListItem>();
            model.SubDistricts = subDistricts != null ? subDistricts.Select<SubDistrict, SelectListItem>(s => new SelectListItem { Value = s.Id.ToString(), Text = Language == Language.TH ? s.TitleTH : s.TitleEN }).ToList<SelectListItem>() : new List<SelectListItem>();
            model.BillingDistricts = billing_districts != null ? billing_districts : new List<SelectListItem>();
            model.BillingSubDistricts = billing_subDistricts != null ? billing_subDistricts.Select<SubDistrict, SelectListItem>(s => new SelectListItem { Value = s.Id.ToString(), Text = Language == Language.TH ? s.TitleTH : s.TitleEN }).ToList<SelectListItem>() : new List<SelectListItem>();
            model.ShippingDistricts = shipping_districts != null ? shipping_districts : new List<SelectListItem>();
            model.ShippingSubDistricts = shipping_subDistricts != null ? shipping_subDistricts.Select<SubDistrict, SelectListItem>(s => new SelectListItem { Value = s.Id.ToString(), Text = Language == Language.TH ? s.TitleTH : s.TitleEN }).ToList<SelectListItem>() : new List<SelectListItem>();
            #endregion

            model.ProductApplicationPolicyHolders = application.ProductApplicationPolicyHolders != null && application.ProductApplicationPolicyHolders.Count > 0 ?
                application.ProductApplicationPolicyHolders.ToArray() : new ProductApplicationPolicyHolder[1] { model.ProductApplicationPolicyHolder };

            model.MinAge = MIN_AGE;
            model.MaxAge = MAX_AGE;

            application.Step = steps.Peek();
            if (application.Step != ProductApplicationStage.Base.Success) {
                appRepo.UpdateProductApplication(application);
            }
            model.ProductApplication = application;

            return View(Language.ToString() + "/Home", model);
        }

        [ActionName("home")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Home_Save(OrderViewModel model) {
            if (!ValidateOrderToken()) { return Redirect(Url.Content("~")); }
            bool MoveOn = true;
            ProductApplicationRepository appRepo = new ProductApplicationRepository();
            CountryRepository countryRepo = new CountryRepository();
            ProvinceRepository provinceRepo = new ProvinceRepository();
            MasterOptionRepository masterRepo = new MasterOptionRepository();
            ProductRepository productRepo = new ProductRepository();
            LeadFormRepository leadRepo = new LeadFormRepository();
            CareerRepository careerRepo = new CareerRepository();
            ProductApplicationMotor motor = new ProductApplicationMotor();
            DistrictRepository districtRepo = new DistrictRepository();
            SubDistrictRepository subDistrictRepo = new SubDistrictRepository();

            ProductApplication application = model != null ? appRepo.GetProductApplicationForm(NullUtils.cvInt(model.ApplicationId)) : null;
            if (application == null) { return Redirect(Url.Content("~")); }

            int lastStep = application.Step;
            int State = ProductApplicationStage.Base.Unknown;
            Stack<int> steps = ConvertToApplicationSteps(application.Steps);
            Stack<int> overSteps = ConvertToApplicationSteps(application.OverSteps);
            if (steps == null || steps.Count == 0) { return Redirect(Url.Content("~")); }
            if (model.UserAction == UserAction.Next) { State = steps.Peek(); };
            if (model.UserAction == UserAction.Back) { State = overSteps.Any() ? overSteps.Peek() : State; }

            ProductItem productItem = productRepo.GetProductHome_Active(Language, application.ProductId, application.PremiumId);
            if (productItem == null || CheckOrderStatus(application.Id) || !productItem.SaleChannel.HasFlag(TobJod.Models.ProductSaleChannel.Online)) { return Redirect(Url.Content("~")); }
            var productPremium = productItem.ProductHomePremium[0];
            model.ProductItem = productItem;
            model.ProductItem.CompanyImage = PathCompany + productItem.CompanyImage;

            ProductApplicationFieldRepository productAppFieldRepo = new ProductApplicationFieldRepository();
            var productApplicationFields = productAppFieldRepo.ListProductApplicationFieldByProductCategoryKey(model.ProductCategoryKey, Language);
            model.ProductApplicationFields = productApplicationFields;
            model.Page = model.ProductCategoryKey.ToString();
            model.ApplicationId = model.ApplicationId;

            var constructionTypeId = application.ProductApplicationHome != null ? application.ProductApplicationHome.ConstructionType.ToString() : model.ProductApplicationHome.ConstructionType.ToString();
            var propertypTypeId = application.ProductApplicationHome != null ? application.ProductApplicationHome.PropertyType.ToString() : model.ProductApplicationHome.PropertyType.ToString();
            var constructionType = MainLayoutViewModel.ConstructionTypes.FirstOrDefault(con => con.Value == constructionTypeId);
            var propertyType = MainLayoutViewModel.PropertyTypes.FirstOrDefault(con => con.Value == propertypTypeId);

            model.ConstructionType = constructionType != null ? constructionType.Text : string.Empty;
            model.PropertyType = propertyType != null ? propertyType.Text : string.Empty;

            model.PolicyHolder_Size = POLICY_HOLDER_SIZE;
            model.MinAge = MIN_AGE;
            model.MaxAge = MAX_AGE;

            #region DropDownList
            model.Careers = careerRepo.ListCareer_SelectList_Active(Language);
            model.Countries = countryRepo.ListCountry_SelectList_Active(Language);
            model.Relationships = masterRepo.ListMasterOption_SelectList_Active(Language, MasterOptionCategory.Relationship);
            model.Salutations = masterRepo.ListMasterOption_SelectList_Active(Language, MasterOptionCategory.Salutation);
            model.LocationUsages = masterRepo.ListMasterOption_SelectList_Active(Language, MasterOptionCategory.LocationUsage);
            model.Provinces = provinceRepo.ListProvince_SelectList_Active(Language);
            #endregion

            #region DropDownList: District, SubDistrict
            var homeDistricts = application.ProductApplicationHome != null ? districtRepo.ListDistrict_SelectList_Active(NullUtils.cvInt(application.ProductApplicationHome.AddressProvince), Language) : new List<SelectListItem>();
            var homeSubDistricts = application.ProductApplicationHome != null ? subDistrictRepo.ListSubDistrict_Active(NullUtils.cvInt(application.ProductApplicationHome.AddressDistrict), Language) : new List<SubDistrict>();
            var districts = districtRepo.ListDistrict_SelectList_Active(NullUtils.cvInt(application.AddressProvince), Language);
            var subDistricts = subDistrictRepo.ListSubDistrict_Active(NullUtils.cvInt(application.AddressDistrict), Language);
            var billing_districts = districtRepo.ListDistrict_SelectList_Active(NullUtils.cvInt(application.BillingAddressProvince), Language);
            var billing_subDistricts = subDistrictRepo.ListSubDistrict_Active(NullUtils.cvInt(application.BillingAddressDistrict), Language);
            var shipping_districts = districtRepo.ListDistrict_SelectList_Active(NullUtils.cvInt(application.ShippingAddressProvince), Language);
            var shipping_subDistricts = subDistrictRepo.ListSubDistrict_Active(NullUtils.cvInt(application.ShippingAddressDistrict), Language);
            model.HomeDistricts = homeDistricts != null ? homeDistricts : new List<SelectListItem>();
            model.HomeSubDistricts = homeSubDistricts != null ? homeSubDistricts.Select<SubDistrict, SelectListItem>(s => new SelectListItem { Value = s.Id.ToString(), Text = Language == Language.TH ? s.TitleTH : s.TitleEN }).ToList<SelectListItem>() : new List<SelectListItem>();
            model.Districts = districts != null ? districts : new List<SelectListItem>();
            model.SubDistricts = subDistricts != null ? subDistricts.Select<SubDistrict, SelectListItem>(s => new SelectListItem { Value = s.Id.ToString(), Text = Language == Language.TH ? s.TitleTH : s.TitleEN }).ToList<SelectListItem>() : new List<SelectListItem>();
            model.BillingDistricts = billing_districts != null ? billing_districts : new List<SelectListItem>();
            model.BillingSubDistricts = billing_subDistricts != null ? billing_subDistricts.Select<SubDistrict, SelectListItem>(s => new SelectListItem { Value = s.Id.ToString(), Text = Language == Language.TH ? s.TitleTH : s.TitleEN }).ToList<SelectListItem>() : new List<SelectListItem>();
            model.ShippingDistricts = shipping_districts != null ? shipping_districts : new List<SelectListItem>();
            model.ShippingSubDistricts = shipping_subDistricts != null ? shipping_subDistricts.Select<SubDistrict, SelectListItem>(s => new SelectListItem { Value = s.Id.ToString(), Text = Language == Language.TH ? s.TitleTH : s.TitleEN }).ToList<SelectListItem>() : new List<SelectListItem>();
            #endregion

            #region Lead Form

            var leadForm = GetLeadForm();
            if (leadForm != null) {
                var policyHolder = model.ProductApplicationPolicyHolder;
                policyHolder.Name = leadForm.Name;
                policyHolder.Surname = leadForm.Surname;
                policyHolder.MobilePhone = leadForm.Tel;
                policyHolder.Email = leadForm.Email;
            }

            #endregion

            if (NullUtils.cvInt(Request.Form["State"]) != application.Step) {
                return RedirectToAction("home", new { ApplicationId = application.Id, ProductId = application.ProductId, Token = model.Token });
            }

            if (State == ProductApplicationStage.Home.HomeForm && model.UserAction == UserAction.Next) {
                ProductApplicationHome homeModel = new ProductApplicationHome();
                homeModel = CopyModel<ProductApplicationHome>(model.ProductApplicationHome);
                homeModel.ApplicationId = application.Id;

                if (application.ProductApplicationHome == null) {
                    appRepo.AddProductApplicationHome(homeModel);
                } else {
                    appRepo.UpdateProductApplicationHome(homeModel);
                }
            }

            if (State == ProductApplicationStage.Home.PolicyHolderForm && model.UserAction == UserAction.Next) {
                List<ProductApplicationPolicyHolder> policyHolders = new List<ProductApplicationPolicyHolder>();
                for (var i = 0; model.ProductApplicationPolicyHolders != null && i < model.ProductApplicationPolicyHolders.Length; i++) {
                    var policyHolderItem = model.ProductApplicationPolicyHolders[i];
                    if (policyHolderItem.TitleId > 0) {
                        ProductApplicationPolicyHolder policyHolderModel = new ProductApplicationPolicyHolder();
                        policyHolderModel = CopyModel<ProductApplicationPolicyHolder>(policyHolderItem);
                        policyHolderModel.ApplicationId = model.ApplicationId;
                        policyHolderModel.Sequence = (i + 1);
                        policyHolderModel.Birthday = ValueUtils.GetDate(Request.Form["ProductApplicationPolicyHolders[" + i + "].Birthday"]);
                        policyHolders.Add(policyHolderModel);
                    }
                }

                if (policyHolders.Count() > 0) {
                    appRepo.AddProductApplicationPolicyHolders(policyHolders);
                    application.ProductApplicationPolicyHolders = policyHolders;
                }

                application.ContactName = NullUtils.cvString(model.ProductApplication.ContactName);
                application.ContactSurname = NullUtils.cvString(model.ProductApplication.ContactSurname);
                application.ContactTelephone = NullUtils.cvString(model.ProductApplication.ContactTelephone);
                application.ContactMobilePhone = NullUtils.cvString(model.ProductApplication.ContactMobilePhone);
                application.ContactEmail = NullUtils.cvString(model.ProductApplication.ContactEmail);

                application.BuyerName = NullUtils.cvString(model.ProductApplication.BuyerName);
                application.BuyerSurname = NullUtils.cvString(model.ProductApplication.BuyerSurname);
                application.BuyerTelephone = NullUtils.cvString(model.ProductApplication.BuyerTelephone);
                application.BuyerMobilePhone = NullUtils.cvString(model.ProductApplication.BuyerMobilePhone);
                application.BuyerEmail = NullUtils.cvString(model.ProductApplication.BuyerEmail);

                application.ActiveDate = ValueUtils.GetDate(Request.Form["ProductApplication[ActiveDate]"]);
                application.ExpireDate = ValueUtils.GetDate(Request.Form["ProductApplication[ExpireDate]"]);

                appRepo.UpdateProductApplication(application);
            }

            if (State == ProductApplicationStage.Home.AddressForm && model.UserAction == UserAction.Next) {
                application = CopyModel<ProductApplication>(model.ProductApplication);
                application.Id = model.ApplicationId;
                #region Setup Address
                if (model.ProductApplication.BillingAddressIsChecked == YesNo.Yes) {
                    application.BillingAddressBranchType = NullUtils.cvInt(model.ProductApplication.AddressBranchType);
                    application.BillingAddressBranchNo = NullUtils.cvString(model.ProductApplication.AddressBranchNo);
                    application.BillingAddressNo = NullUtils.cvString(model.ProductApplication.AddressNo);
                    application.BillingAddressMoo = NullUtils.cvString(model.ProductApplication.AddressMoo);
                    application.BillingAddressVillage = NullUtils.cvString(model.ProductApplication.AddressVillage);
                    application.BillingAddressFloor = NullUtils.cvString(model.ProductApplication.AddressFloor);
                    application.BillingAddressSoi = NullUtils.cvString(model.ProductApplication.AddressSoi);
                    application.BillingAddressRoad = NullUtils.cvString(model.ProductApplication.AddressRoad);
                    application.BillingAddressDistrict = NullUtils.cvString(model.ProductApplication.AddressDistrict);
                    application.BillingAddressSubDistrict = NullUtils.cvString(model.ProductApplication.AddressSubDistrict);
                    application.BillingAddressProvince = NullUtils.cvString(model.ProductApplication.AddressProvince);
                    application.BillingAddressPostalCode = NullUtils.cvString(model.ProductApplication.AddressPostalCode);
                }

                if (model.ProductApplication.ShippingAddressIsChecked == YesNo.Yes) {
                    application.ShippingAddressBranchType = NullUtils.cvInt(model.ProductApplication.AddressBranchType);
                    application.ShippingAddressBranchNo = NullUtils.cvString(model.ProductApplication.AddressBranchNo);
                    application.ShippingAddressNo = NullUtils.cvString(model.ProductApplication.AddressNo);
                    application.ShippingAddressMoo = NullUtils.cvString(model.ProductApplication.AddressMoo);
                    application.ShippingAddressVillage = NullUtils.cvString(model.ProductApplication.AddressVillage);
                    application.ShippingAddressFloor = NullUtils.cvString(model.ProductApplication.AddressFloor);
                    application.ShippingAddressSoi = NullUtils.cvString(model.ProductApplication.AddressSoi);
                    application.ShippingAddressRoad = NullUtils.cvString(model.ProductApplication.AddressRoad);
                    application.ShippingAddressDistrict = NullUtils.cvString(model.ProductApplication.AddressDistrict);
                    application.ShippingAddressSubDistrict = NullUtils.cvString(model.ProductApplication.AddressSubDistrict);
                    application.ShippingAddressProvince = NullUtils.cvString(model.ProductApplication.AddressProvince);
                    application.ShippingAddressPostalCode = NullUtils.cvString(model.ProductApplication.AddressPostalCode);
                }
                #endregion
                appRepo.UpdateProductApplicationAddressForm(application);
                application = appRepo.GetProductApplicationForm(application.Id);
            }

            var path = Server.MapPath(System.Web.Configuration.WebConfigurationManager.AppSettings.Get("Path_Upload"));
            if (State == ProductApplicationStage.Home.AttachmentForm && model.UserAction == UserAction.Next) {
                #region Upload Files
                List<ProductApplicationAttachment> attachments = new List<ProductApplicationAttachment>();
                
                MoveOn &= UploadAttachmentFile(model.ApplicationId, model.ProductId, model.File1, FileCategory.File1, application, attachments);
                MoveOn &= UploadAttachmentFile(model.ApplicationId, model.ProductId, model.File2, FileCategory.File2, application, attachments);
                MoveOn &= UploadAttachmentFile(model.ApplicationId, model.ProductId, model.File3, FileCategory.File3, application, attachments);
                MoveOn &= UploadAttachmentFile(model.ApplicationId, model.ProductId, model.File4, FileCategory.File4, application, attachments);
                MoveOn &= UploadAttachmentFile(model.ApplicationId, model.ProductId, model.File5, FileCategory.File5, application, attachments);
                MoveOn &= UploadAttachmentFile(model.ApplicationId, model.ProductId, model.File6, FileCategory.File6, application, attachments);
               
                if (attachments.Count > 0) { appRepo.AddProductApplicationAttachments(attachments); }
                #endregion
            }

            if (State == ProductApplicationStage.Base.Final && model.UserAction == UserAction.Next) {
                #region Promotion And Coupon
                var promotionId = NullUtils.cvInt(model.ProductApplication.PromotionId);
                PromotionItem promotion = null; bool hasGift = false;
                ClearPromotionAndCoupon(application);
                if (promotionId > 0 && productItem.Promotion != null) {
                    promotion = productItem.Promotion.Where(prm => prm.Id == promotionId).FirstOrDefault();
                    if (promotion != null) {
                        application.PromotionId = promotionId;
                        var discount = promotion.DiscountAmount;
                        if (promotion.DiscountType == PromotionDiscountType.Amount) {
                            model.TotalDiscount = (discount > application.DisplayPremium ? 0 : discount);
                            UpdatePromotionAndCoupon(application, model.TotalDiscount, promotionId, promotion.CampaignId, string.Empty);
                            hasGift = true;
                        } else {
                            model.TotalDiscount = ((application.DisplayPremium * discount) / 100);
                            UpdatePromotionAndCoupon(application, model.TotalDiscount, promotionId, promotion.CampaignId, string.Empty);
                            hasGift = true;
                        }
                    }
                }

                var systemMessageRepository = new SystemMessageRepository();
                var messages = systemMessageRepository.ListSystemMessage();
                var message = new SystemMessage();
                var couponCode = NullUtils.cvString(model.ProductApplication.CouponCode);
                Coupon coupon = null;

                if (couponCode.Length > 0) {
                    coupon = appRepo.GetCoupon_Active(couponCode, application.ProductId);
                    if (coupon != null) {
                        var checkedDiscount = (promotion != null && promotion.PromotionType == PromotionType.Discount);
                        if (coupon.CouponType == CouponType.OneTime) {
                            if (coupon.TotalUsed == 0) {
                                if (!checkedDiscount) {
                                    if (coupon.DiscountType == CouponDiscountType.Amount) {
                                        model.TotalDiscount += coupon.DiscountAmount;
                                        UpdatePromotionAndCoupon(application, model.TotalDiscount, hasGift ? promotionId : 0, promotion.CampaignId, couponCode);
                                        model.ProductApplication.CouponCode = couponCode;
                                    } else {
                                        model.TotalDiscount += (application.DisplayPremium * (coupon.DiscountAmount) / 100);
                                        UpdatePromotionAndCoupon(application, model.TotalDiscount, hasGift ? promotionId : 0, promotion.CampaignId, couponCode);
                                        model.ProductApplication.CouponCode = couponCode;
                                    }
                                }
                            } else {
                                ClearPromotionAndCoupon(application);
                                model.ProductApplication = application;
                                model.TotalDiscount = 0;
                                message = messages.Where(o => o.Category == SystemMessageCategory.Coupon && o.Code == ((int)SystemMessageStatus.CouponUsed).ToString()).FirstOrDefault();
                                ViewBag.CouponError = Language == Language.TH ? message.BodyTH : message.BodyEN;
                                return View(Language.ToString() + "/Home", model);
                            }
                        }
                        if (coupon.CouponType == CouponType.Mass) {
                            if (!checkedDiscount) {
                                if (coupon.TotalUsed < coupon.Total) {
                                    if (coupon.DiscountType == CouponDiscountType.Amount) {
                                        model.TotalDiscount += coupon.DiscountAmount;
                                        UpdatePromotionAndCoupon(application, model.TotalDiscount, hasGift ? promotionId : 0, promotion.CampaignId, couponCode);
                                        model.ProductApplication.CouponCode = couponCode;
                                    } else {
                                        model.TotalDiscount += (application.DisplayPremium * (coupon.DiscountAmount) / 100);
                                        UpdatePromotionAndCoupon(application, model.TotalDiscount, hasGift ? promotionId : 0, promotion.CampaignId, couponCode);
                                        model.ProductApplication.CouponCode = couponCode;
                                    }
                                } else {
                                    ClearPromotionAndCoupon(application);
                                    model.ProductApplication = application;
                                    model.TotalDiscount = 0;
                                    message = messages.Where(o => o.Category == SystemMessageCategory.Coupon && o.Code == ((int)SystemMessageStatus.CouponUsed).ToString()).FirstOrDefault();
                                    ViewBag.CouponError = Language == Language.TH ? message.BodyTH : message.BodyEN;
                                    return View(Language.ToString() + "/Home", model);
                                }
                            } else {
                                ClearPromotionAndCoupon(application);
                                model.ProductApplication = application;
                                model.TotalDiscount = 0;
                                message = messages.Where(o => o.Category == SystemMessageCategory.Coupon && o.Code == ((int)SystemMessageStatus.CouponTopUp).ToString()).FirstOrDefault();
                                ViewBag.CouponError = Language == Language.TH ? message.BodyTH : message.BodyEN;
                                return View(Language.ToString() + "/Home", model);
                            }
                        }
                    } else {
                        ClearPromotionAndCoupon(application);
                        model.ProductApplication = application;
                        model.TotalDiscount = 0;
                        model.ProductApplication.CouponCode = string.Empty;
                        message = messages.Where(o => o.Category == SystemMessageCategory.Coupon && o.Code == ((int)SystemMessageStatus.CouponUnKnown).ToString()).FirstOrDefault();
                        ViewBag.CouponError = Language == Language.TH ? message.BodyTH : message.BodyEN;
                        return View(Language.ToString() + "/Home", model);
                    }
                }

                var currentAppForm = appRepo.GetProductApplicationForm(application.Id);
                application.CouponCode = NullUtils.cvString(currentAppForm.CouponCode);
                application.TotalDiscount = currentAppForm.TotalDiscount;
                RecheckPromotion_Active(promotionId, productItem, application);
                RecheckCoupon_Active(couponCode, application);
                #endregion
            }

            model.ProductApplicationPolicyHolders = application.ProductApplicationPolicyHolders != null && application.ProductApplicationPolicyHolders.Count > 0
                ? application.ProductApplicationPolicyHolders.ToArray() : new ProductApplicationPolicyHolder[1] { model.ProductApplicationPolicyHolder };
            model.ProductApplicationAttachments = application.ProductApplicationAttachment != null && application.ProductApplicationAttachment.Count > 0
                ? application.ProductApplicationAttachment : new List<ProductApplicationAttachment>();
            model.ProductApplicationHome = application.ProductApplicationHome != null
                ? application.ProductApplicationHome : model.ProductApplicationHome;

            #region Validate

            MoveOn &= ValidateOtp(application.Id, model.ProductApplicationPolicyHolders);
            MoveOn &= ValidateStringAlphanum(application.ContactName);
            MoveOn &= ValidateStringAlphanum(application.ContactSurname);
            MoveOn &= ValidateStringAlphanumTel(application.ContactTelephone);
            MoveOn &= ValidateStringAlphanumTel(application.ContactMobilePhone);
            MoveOn &= ValidateStringAlphanumEmail(application.ContactEmail);
            MoveOn &= ValidateStringAlphanum(application.ContactLineId);

            MoveOn &= ValidateStringAlphanum(application.BuyerName);
            MoveOn &= ValidateStringAlphanum(application.BuyerSurname);
            MoveOn &= ValidateStringAlphanumTel(application.BuyerTelephone);
            MoveOn &= ValidateStringAlphanumTel(application.BuyerMobilePhone);
            MoveOn &= ValidateStringAlphanumEmail(application.BuyerEmail);
            MoveOn &= ValidateStringAlphanum(application.BuyerLineId);

            MoveOn &= ValidateStringAlphanum(application.AddressBranchNo);
            MoveOn &= ValidateStringAlphanumAddress(application.AddressNo);
            MoveOn &= ValidateStringAlphanum(application.AddressMoo);
            MoveOn &= ValidateStringAlphanum(application.AddressVillage);
            MoveOn &= ValidateStringNumeric(application.AddressFloor);
            MoveOn &= ValidateStringAlphanum(application.AddressSoi);
            MoveOn &= ValidateStringAlphanum(application.AddressRoad);
            MoveOn &= ValidateStringAlphanum(application.AddressPostalCode);

            MoveOn &= ValidateStringAlphanum(application.BillingAddressBranchNo);
            MoveOn &= ValidateStringAlphanumAddress(application.BillingAddressNo);
            MoveOn &= ValidateStringAlphanum(application.BillingAddressMoo);
            MoveOn &= ValidateStringAlphanum(application.BillingAddressVillage);
            MoveOn &= ValidateStringNumeric(application.BillingAddressFloor);
            MoveOn &= ValidateStringAlphanum(application.BillingAddressSoi);
            MoveOn &= ValidateStringAlphanum(application.BillingAddressRoad);
            MoveOn &= ValidateStringAlphanum(application.BillingAddressPostalCode);

            MoveOn &= ValidateStringAlphanum(application.ShippingAddressBranchNo);
            MoveOn &= ValidateStringAlphanumAddress(application.ShippingAddressNo);
            MoveOn &= ValidateStringAlphanum(application.ShippingAddressMoo);
            MoveOn &= ValidateStringAlphanum(application.ShippingAddressVillage);
            MoveOn &= ValidateStringNumeric(application.ShippingAddressFloor);
            MoveOn &= ValidateStringAlphanum(application.ShippingAddressSoi);
            MoveOn &= ValidateStringAlphanum(application.ShippingAddressRoad);
            MoveOn &= ValidateStringAlphanum(application.ShippingAddressPostalCode);

            foreach (var it in model.ProductApplicationPolicyHolders) {
                MoveOn &= ValidateStringAlphanum(it.Name);
                MoveOn &= ValidateStringAlphanum(it.Surname);
                MoveOn &= ValidateStringAlphanum(it.RefIDCard);
                MoveOn &= ValidateStringAlphanum(it.IDCard);
                MoveOn &= ValidateStringAlphanumTel(it.Telephone);
                MoveOn &= ValidateStringAlphanumTel(it.MobilePhone);
                MoveOn &= ValidateStringAlphanumEmail(it.Email);
                MoveOn &= ValidateStringAlphanum(it.BeneficiaryName);
                MoveOn &= ValidateStringAlphanum(it.BeneficiarySurname);
                MoveOn &= ValidateStringAlphanum(it.BeneficiaryIDCard);
                MoveOn &= ValidateStringAlphanumTel(it.BeneficiaryMobilePhone);
                MoveOn &= ValidateStringAlphanum(it.LineId);
                MoveOn &= ValidateStringAlphanum(it.Instagram);
                MoveOn &= ValidateStringAlphanum(it.Facebook);
            }

            #endregion

            if (State != ProductApplicationStage.Base.Success || (State == ProductApplicationStage.Base.Success && model.UserAction == UserAction.Back)) {
                if (MoveOn) {
                    if (model.UserAction == UserAction.Next) { State = steps.Pop(); overSteps.Push(State); } else { State = overSteps.Pop(); steps.Push(State); };
                    model.State = model.UserAction == UserAction.Next ? steps.Peek() : State;
                    application.Steps = steps != null ? string.Join("-", steps.ToList().Select(x => (int)x).Reverse().ToArray()) : string.Empty;
                    application.OverSteps = overSteps != null ? string.Join("-", overSteps.ToList().Select(x => (int)x).Reverse().ToArray()) : string.Empty;
                    application.Step = (int)model.State;

                    appRepo.UpdateProductApplicationState(application);
                }
                model.ProductApplication = application;

            }

            if (State == ProductApplicationStage.Base.Success && model.UserAction == UserAction.Next) {
                var couponRepo = new CouponRepository();
                var order = GenerateOrder(application, ORDER_NON_MOTOR, leadForm != null ? leadForm.Id : 0);
                if (order != null) {
                    couponRepo.UpdateUsedCouponCode(NullUtils.cvString(application.CouponCode), order.Id);
                }

                State = ProductApplicationStage.Base.Success;
                model.State = State;
                application.Step = (int)model.State;
                application.Language = Language;
                appRepo.UpdateProductApplicationState(application);
                return Redirect(Url.Action("PaymentRequest") + "/?ApplicationId=" + application.Id);
            }

            return View(Language.ToString() + "/Home", model);

        }

        #endregion

        #region Motor
        [ActionName("motor")]
        public ActionResult Motor(OrderProductViewModel item) {
            if (!ValidateOrderToken()) { return Redirect(Url.Content("~")); }
            int applicationId = NullUtils.cvInt(Request.QueryString.Get("ApplicationId"), -1);
            ProductApplicationRepository appRepo = new ProductApplicationRepository();
            CountryRepository countryRepo = new CountryRepository();
            ProvinceRepository provinceRepo = new ProvinceRepository();
            MasterOptionRepository masterRepo = new MasterOptionRepository();
            ProductRepository productRepo = new ProductRepository();
            LeadFormRepository leadRepo = new LeadFormRepository();
            ProductApplicationFieldRepository productAppFieldRepo = new ProductApplicationFieldRepository();
            CarRepository carRepo = new CarRepository();
            ProductApplicationMotor motor = new ProductApplicationMotor();
            DistrictRepository districtRepo = new DistrictRepository();
            SubDistrictRepository subDistrictRepo = new SubDistrictRepository();

            ProductApplication application = applicationId != -1 ? appRepo.GetProductApplicationForm(NullUtils.cvInt(applicationId)) : null;
            if (application == null || item.ProductId == 0) { return Redirect(Url.Content("~")); }

            OrderViewModel model = new OrderViewModel();

            Stack<int> steps = ConvertToApplicationSteps(application.Steps);
            if ((steps == null || steps.Count == 0)) { return Redirect(Url.Content("~")); }

            model.PremiumId = application.PremiumId;
            model.ProductId = application.ProductId;
            model.ProductCategoryKey = GetProductCategory(model.ProductId);
            model.State = steps.Peek();
            model.Page = model.ProductCategoryKey.ToString();
            model.ApplicationId = applicationId;
            model.Token = GenerateOrderToken(model.ApplicationId, model.ProductId);

            #region Lead Form

            var leadForm = GetLeadForm();
            if (leadForm != null) {
                var policyHolder = model.ProductApplicationPolicyHolder;
                policyHolder.Name = leadForm.Name;
                policyHolder.Surname = leadForm.Surname;
                policyHolder.MobilePhone = leadForm.Tel;
                policyHolder.Email = leadForm.Email;
            }

            #endregion

            ProductItem productItem = productRepo.GetProductMotor_Active(Language, application.ProductId, application.PremiumId);
            if (productItem == null || CheckOrderStatus(application.Id) || !productItem.SaleChannel.HasFlag(TobJod.Models.ProductSaleChannel.Online)) { return Redirect(Url.Content("~")); }
            var productPremium = productItem.ProductMotorPremium[0];
            var productName = Language == Language.TH ? productItem.TitleTH : productItem.TitleEN;
            model.ProductItem = productItem;
            model.ProductItem.CompanyImage = PathCompany + productItem.CompanyImage;
            var Compulsory = !Request.Params.AllKeys.Contains("Compulsory") && application.ProductApplicationMotor != null ?
                application.ProductApplicationMotor.Compulsory : NullUtils.cvInt(Request.Params["Compulsory"]);

            application.ProductCode = NullUtils.cvString(productItem.ProductCode);
            application.ProductName = NullUtils.cvString(productName);
            application.SumInsured = productPremium.SumInsured;
            application.NetPremium = productPremium.NetPremium;
            application.DisplayPremium = NullUtils.cvInt(Compulsory) == 1 ? productPremium.DisplayCombinePremium : productPremium.DisplayPremium;
            application.EPolicy = productPremium.EPolicy ? (int)TobJod.Models.AllYesNo.Yes : (int)TobJod.Models.AllYesNo.No;

            var date = DateTime.Now.AddDays(1);
            application.ActiveDate = date;
            application.ExpireDate = date.AddYears(1);

            model.PolicyHolder_Size = POLICY_HOLDER_SIZE;
            model.Driver_Size = DRIVER_SIZE;

            var _brand = application.ProductApplicationMotor != null ? NullUtils.cvInt(application.ProductApplicationMotor.CarBrand) : NullUtils.cvInt(item.Brand);
            var _family = application.ProductApplicationMotor != null ? NullUtils.cvInt(application.ProductApplicationMotor.CarFamily) : NullUtils.cvInt(item.Family);
            var _model = application.ProductApplicationMotor != null ? NullUtils.cvInt(application.ProductApplicationMotor.CarModelId) : NullUtils.cvInt(item.Model);
            var _province = application.ProductApplicationMotor != null ? NullUtils.cvInt(application.ProductApplicationMotor.CarProvinceId) : NullUtils.cvInt(item.Province);
            var _year = application.ProductApplicationMotor != null ? NullUtils.cvInt(application.ProductApplicationMotor.CarRegisterYear) : NullUtils.cvInt(item.Year);

            var carBrands = carRepo.ListCarBrand_SelectList(Language);
            var carFamily = carRepo.ListCarFamilyByBrand_NameValue(Language, _brand);
            var carModel = carRepo.ListCarModelByBrand_NameValue(Language, _brand);
            var carProvinces = masterRepo.ListMasterOption_SelectList_Active(Language, MasterOptionCategory.MotorProvince);

            var districts = districtRepo.ListDistrict_SelectList_Active(NullUtils.cvInt(application.AddressProvince), Language);
            var subDistricts = subDistrictRepo.ListSubDistrict_Active(NullUtils.cvInt(application.AddressDistrict), Language);
            var billing_districts = districtRepo.ListDistrict_SelectList_Active(NullUtils.cvInt(application.BillingAddressProvince), Language);
            var billing_subDistricts = subDistrictRepo.ListSubDistrict_Active(NullUtils.cvInt(application.BillingAddressDistrict), Language);
            var shipping_districts = districtRepo.ListDistrict_SelectList_Active(NullUtils.cvInt(application.ShippingAddressProvince), Language);
            var shipping_subDistricts = subDistrictRepo.ListSubDistrict_Active(NullUtils.cvInt(application.ShippingAddressDistrict), Language);

            var carBrandObj = carBrands.FirstOrDefault(brand => brand.Value == _brand.ToString());
            var carFamilyObj = carFamily.FirstOrDefault(family => family.Value == _family.ToString());
            var carModelObj = carModel.FirstOrDefault(m => m.Value == _model.ToString());
            var carProvinceObj = carProvinces.FirstOrDefault(p => p.Value == _province.ToString());

            model.CarBrand = carBrandObj != null ? carBrandObj.Text : string.Empty;
            model.CarFamily = carFamilyObj != null ? carFamilyObj.Name : string.Empty;
            model.CarModel = carModelObj != null ? carModelObj.Name : string.Empty;
            model.CarProvince = carProvinceObj != null ? carProvinceObj.Text : string.Empty;

            if (application.ProductApplicationMotor == null) {
                motor.CarRegisterYear = NullUtils.cvInt(item.Year, 0);
                motor.CarBrand = NullUtils.cvString(item.Brand);
                motor.CarFamily = NullUtils.cvString(item.Family);
                motor.CarModelId = NullUtils.cvInt(item.Model, 0);
                motor.CarModel = NullUtils.cvString(model.CarModel);
                motor.CarProvinceId = NullUtils.cvInt(item.Province, 0);
                motor.CarCCTV = (int)productPremium.CCTV;
                motor.Compulsory = NullUtils.cvInt(Request.Params["Compulsory"]);
                //model.Accessory = item.Accessory;
                model.Garage = item.Garage;
                model.ProductApplicationMotor = motor;
                application.ProductApplicationMotor = motor;
            } else {
                model.ProductApplicationMotor = application.ProductApplicationMotor;
                model.CarPlateNo = model.ProductApplicationMotor.CarPlateNo1 + model.ProductApplicationMotor.CarPlateNo2;
            }


            var productApplicationFields = productAppFieldRepo.ListProductApplicationFieldByProductCategoryKey(model.ProductCategoryKey, Language);
            model.ProductApplicationFields = productApplicationFields;

            #region DrownDownList
            model.Countries = countryRepo.ListCountry_SelectList_Active(Language);
            model.Salutations = masterRepo.ListMasterOption_SelectList_Active(Language, MasterOptionCategory.Salutation);
            model.Relationships = masterRepo.ListMasterOption_SelectList_Active(Language, MasterOptionCategory.Relationship);
            model.Provinces = provinceRepo.ListProvince_SelectList_Active(Language);
            model.Districts = districts != null ? districts : new List<SelectListItem>();
            model.SubDistricts = subDistricts != null ? subDistricts.Select<SubDistrict, SelectListItem>(s => new SelectListItem { Value = s.Id.ToString(), Text = Language == Language.TH ? s.TitleTH : s.TitleEN }).ToList<SelectListItem>() : new List<SelectListItem>();
            model.BillingDistricts = billing_districts != null ? billing_districts : new List<SelectListItem>();
            model.BillingSubDistricts = billing_subDistricts != null ? billing_subDistricts.Select<SubDistrict, SelectListItem>(s => new SelectListItem { Value = s.Id.ToString(), Text = Language == Language.TH ? s.TitleTH : s.TitleEN }).ToList<SelectListItem>() : new List<SelectListItem>();
            model.ShippingDistricts = shipping_districts != null ? shipping_districts : new List<SelectListItem>();
            model.ShippingSubDistricts = shipping_subDistricts != null ? shipping_subDistricts.Select<SubDistrict, SelectListItem>(s => new SelectListItem { Value = s.Id.ToString(), Text = Language == Language.TH ? s.TitleTH : s.TitleEN }).ToList<SelectListItem>() : new List<SelectListItem>();
            #endregion

            model.ProductApplicationPolicyHolders = application.ProductApplicationPolicyHolders != null && application.ProductApplicationPolicyHolders.Count > 0 ? application.ProductApplicationPolicyHolders.ToArray() : new ProductApplicationPolicyHolder[1] { model.ProductApplicationPolicyHolder };
            model.ProductApplicationDrivers = application.ProductApplicationDrivers != null && application.ProductApplicationDrivers.Count > 0 ? application.ProductApplicationDrivers.ToArray() : new ProductApplicationDriver[1] { model.ProductApplicationDriver };
            model.ProductApplicationAttachments = application.ProductApplicationAttachment != null && application.ProductApplicationAttachment.Count > 0
                ? application.ProductApplicationAttachment : new List<ProductApplicationAttachment>();

            model.MinAge = productPremium.DriverAgeMin != 0 ? productPremium.DriverAgeMin : MIN_AGE;
            model.MaxAge = productPremium.DriverAgeMax != 0 ? productPremium.DriverAgeMax : MAX_AGE;



            application.Step = steps.Peek();
            if (application.Step != ProductApplicationStage.Base.Success) {
                appRepo.UpdateProductApplication(application);
            }
            model.ProductApplication = application;

            return View(Language.ToString() + "/Motor", model);
        }

        [ActionName("motor")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Motor_Save(OrderViewModel model) {
            if (!ValidateOrderToken()) { return Redirect(Url.Content("~")); }
            bool MoveOn = true;
            ProductApplicationRepository appRepo = new ProductApplicationRepository();
            CountryRepository countryRepo = new CountryRepository();
            ProvinceRepository provinceRepo = new ProvinceRepository();
            MasterOptionRepository masterRepo = new MasterOptionRepository();
            ProductRepository productRepo = new ProductRepository();
            LeadFormRepository leadRepo = new LeadFormRepository();
            CarRepository carRepo = new CarRepository();
            ProductApplicationMotor motor = new ProductApplicationMotor();
            DistrictRepository districtRepo = new DistrictRepository();
            SubDistrictRepository subDistrictRepo = new SubDistrictRepository();

            ProductApplication application = model != null ? appRepo.GetProductApplicationForm(NullUtils.cvInt(model.ApplicationId)) : null;
            if (application == null) { return Redirect(Url.Content("~")); }

            int lastStep = application.Step;
            int State = ProductApplicationStage.Base.Unknown;
            Stack<int> steps = ConvertToApplicationSteps(application.Steps);
            Stack<int> overSteps = ConvertToApplicationSteps(application.OverSteps);
            if (steps == null || steps.Count == 0) { return Redirect(Url.Content("~")); }
            if (model.UserAction == UserAction.Next) { State = steps.Peek(); };
            if (model.UserAction == UserAction.Back) { State = overSteps.Any() ? overSteps.Peek() : State; }

            ProductItem productItem = productRepo.GetProductMotor_Active(Language, application.ProductId, application.PremiumId);
            if (productItem == null || CheckOrderStatus(application.Id) || !productItem.SaleChannel.HasFlag(TobJod.Models.ProductSaleChannel.Online)) { return Redirect(Url.Content("~")); }
            var productPremium = productItem.ProductMotorPremium[0];
            model.ProductItem = productItem;
            model.ProductItem.CompanyImage = PathCompany + productItem.CompanyImage;

            ProductApplicationFieldRepository productAppFieldRepo = new ProductApplicationFieldRepository();
            var productApplicationFields = productAppFieldRepo.ListProductApplicationFieldByProductCategoryKey(model.ProductCategoryKey, Language);
            model.ProductApplicationFields = productApplicationFields;

            model.PolicyHolder_Size = POLICY_HOLDER_SIZE;
            model.Driver_Size = DRIVER_SIZE;
            model.MinAge = productPremium.DriverAgeMin != 0 ? productPremium.DriverAgeMin : MIN_AGE;
            model.MaxAge = productPremium.DriverAgeMax != 0 ? productPremium.DriverAgeMax : MAX_AGE;
            model.Page = model.ProductCategoryKey.ToString();
            model.ApplicationId = model.ApplicationId;

            #region DropDownList

            model.Countries = countryRepo.ListCountry_SelectList_Active(Language);
            model.Relationships = masterRepo.ListMasterOption_SelectList_Active(Language, MasterOptionCategory.Relationship);
            model.Salutations = masterRepo.ListMasterOption_SelectList_Active(Language, MasterOptionCategory.Salutation);
            model.Provinces = provinceRepo.ListProvince_SelectList_Active(Language);

            #endregion

            #region DropDownList:  District, SubDistrict
            var districts = districtRepo.ListDistrict_SelectList_Active(NullUtils.cvInt(application.AddressProvince), Language);
            var subDistricts = subDistrictRepo.ListSubDistrict_Active(NullUtils.cvInt(application.AddressDistrict), Language);
            var billing_districts = districtRepo.ListDistrict_SelectList_Active(NullUtils.cvInt(application.BillingAddressProvince), Language);
            var billing_subDistricts = subDistrictRepo.ListSubDistrict_Active(NullUtils.cvInt(application.BillingAddressDistrict), Language);
            var shipping_districts = districtRepo.ListDistrict_SelectList_Active(NullUtils.cvInt(application.ShippingAddressProvince), Language);
            var shipping_subDistricts = subDistrictRepo.ListSubDistrict_Active(NullUtils.cvInt(application.ShippingAddressDistrict), Language);
            model.Districts = districts != null ? districts : new List<SelectListItem>();
            model.SubDistricts = subDistricts != null ? subDistricts.Select<SubDistrict, SelectListItem>(s => new SelectListItem { Value = s.Id.ToString(), Text = Language == Language.TH ? s.TitleTH : s.TitleEN }).ToList<SelectListItem>() : new List<SelectListItem>();
            model.BillingDistricts = billing_districts != null ? billing_districts : new List<SelectListItem>();
            model.BillingSubDistricts = billing_subDistricts != null ? billing_subDistricts.Select<SubDistrict, SelectListItem>(s => new SelectListItem { Value = s.Id.ToString(), Text = Language == Language.TH ? s.TitleTH : s.TitleEN }).ToList<SelectListItem>() : new List<SelectListItem>();
            model.ShippingDistricts = shipping_districts != null ? shipping_districts : new List<SelectListItem>();
            model.ShippingSubDistricts = shipping_subDistricts != null ? shipping_subDistricts.Select<SubDistrict, SelectListItem>(s => new SelectListItem { Value = s.Id.ToString(), Text = Language == Language.TH ? s.TitleTH : s.TitleEN }).ToList<SelectListItem>() : new List<SelectListItem>();
            #endregion

            #region Lead Form
            var leadForm = GetLeadForm();
            if (leadForm != null) {
                var policyHolder = model.ProductApplicationPolicyHolder;
                policyHolder.Name = leadForm.Name;
                policyHolder.Surname = leadForm.Surname;
                policyHolder.MobilePhone = leadForm.Tel;
                policyHolder.Email = leadForm.Email;
            }
            #endregion

            if (NullUtils.cvInt(Request.Form["State"]) != application.Step) {
                return RedirectToAction("motor", new { ApplicationId = application.Id, ProductId = application.ProductId, Token = model.Token });
            }

            if (State == ProductApplicationStage.Motor.MotorForm) {
                if (model.UserAction == UserAction.Next) {
                    motor = CopyModel<ProductApplicationMotor>(model.ProductApplicationMotor);

                    var carPlateNo = GetCarPlateNo(model.CarPlateNo);
                    motor.ApplicationId = model.ApplicationId;
                    motor.CarPlateNo1 = NullUtils.cvString(carPlateNo.Item1);
                    motor.CarPlateNo2 = NullUtils.cvString(carPlateNo.Item2);
                    motor.CombinePremium = productPremium.CombinePremium;
                    motor.CompulsoryPremium = productPremium.CompulsoryPremium;
                    motor.Compulsory = NullUtils.cvInt(model.ProductApplicationMotor.Compulsory);
                    motor.CarModel = NullUtils.cvString(model.ProductApplicationMotor.CarModel);

                    if (application.ProductApplicationMotor == null) {
                        appRepo.AddProductApplicationMotor(motor);
                        application.ProductApplicationMotor = motor;
                    } else {
                        appRepo.UpdateProductApplicationMotor(motor);
                    }

                } else {
                    model.ProductApplicationMotor = application.ProductApplicationMotor;
                    motor = model.ProductApplicationMotor;

                    var carBrands = carRepo.ListCarBrand_SelectList(Language);
                    var carFamily = carRepo.ListCarFamilyByBrand_NameValue(Language, NullUtils.cvInt(motor.CarBrand));
                    var carModel = carRepo.ListCarModelByBrand_NameValue(Language, NullUtils.cvInt(motor.CarBrand));
                    var carProvinces = masterRepo.ListMasterOption_SelectList_Active(Language, MasterOptionCategory.MotorProvince);

                    var carBrandObj = carBrands.FirstOrDefault(brand => brand.Value == motor.CarBrand);
                    var carFamilyObj = carFamily.FirstOrDefault(family => family.Value == motor.CarFamily);
                    var carModelObj = carModel.FirstOrDefault(m => m.Value == NullUtils.cvString(motor.CarModelId));
                    var carProvinceObj = carProvinces.FirstOrDefault(p => p.Value == NullUtils.cvString(motor.CarProvinceId));

                    model.CarBrand = carBrandObj != null ? carBrandObj.Text : string.Empty;
                    model.CarFamily = carFamilyObj != null ? carFamilyObj.Name : string.Empty;
                    model.CarModel = carModelObj != null ? carModelObj.Name : string.Empty;
                    model.CarProvince = carProvinceObj != null ? carProvinceObj.Text : string.Empty;
                    model.CarPlateNo = motor.CarPlateNo1 + motor.CarPlateNo2;

                    if (model.ShowMotorContactAgent) return View(Language.ToString() + "/Motor", model);
                }

            }


            if (State == ProductApplicationStage.Motor.DriverForm && model.UserAction == UserAction.Next) {
                List<ProductApplicationDriver> drivers = new List<ProductApplicationDriver>();
                for (var i = 0; model.ProductApplicationDrivers != null && i < model.ProductApplicationDrivers.Length; i++) {
                    var driverItem = model.ProductApplicationDrivers[i];
                    if (NullUtils.cvInt(driverItem.TitleId) > 0 && i == 0) {
                        ProductApplicationDriver driver = new ProductApplicationDriver();
                        driver = CopyModel<ProductApplicationDriver>(driverItem);
                        driver.ApplicationId = model.ApplicationId;
                        driver.Birthday = ValueUtils.GetDate(Request.Form["ProductApplicationDrivers[" + i + "].Birthday"]);

                        drivers.Add(driver);
                    }

                    if (i > 0) {
                        if (driverItem.TitleId > 0 || NullUtils.cvString(driverItem.Name).Length > 0 || NullUtils.cvString(driverItem.Surname).Length > 0 || NullUtils.cvInt(driverItem.IDType) > 0 ||
                            NullUtils.cvString(driverItem.IDCard).Length > 0 || NullUtils.cvInt(driverItem.NationalId) > 0 || NullUtils.cvString(driverItem.RefIDCard).Length > 0 || driverItem.Birthday.ToString("dd/mm/yyyy") != DateUtils.SqlMinDate().ToString("dd/mm/yyyy")) {
                            ProductApplicationDriver driver = new ProductApplicationDriver();
                            driver = CopyModel<ProductApplicationDriver>(driverItem);
                            driver.ApplicationId = model.ApplicationId;
                            driver.Birthday = ValueUtils.GetDate(Request.Form["ProductApplicationDrivers[" + i + "].Birthday"]);

                            drivers.Add(driver);
                        }
                    }
                }
                if (drivers != null && drivers.Count > 0) {
                    appRepo.AddProductApplicationDrivers(drivers);
                    application.ProductApplicationDrivers = drivers;
                }
            }

            if (State == ProductApplicationStage.Motor.PolicyHolderForm && model.UserAction == UserAction.Next) {
                List<ProductApplicationPolicyHolder> policyHolders = new List<ProductApplicationPolicyHolder>();
                for (var i = 0; model.ProductApplicationPolicyHolders != null && i < model.ProductApplicationPolicyHolders.Length; i++) {
                    var policyHolderItem = model.ProductApplicationPolicyHolders[i];
                    if (policyHolderItem != null && NullUtils.cvInt(policyHolderItem.TitleId) > 0) {
                        ProductApplicationPolicyHolder policyHolderModel = new ProductApplicationPolicyHolder();
                        policyHolderModel = CopyModel<ProductApplicationPolicyHolder>(policyHolderItem);
                        policyHolderModel.ApplicationId = model.ApplicationId;
                        policyHolderModel.Sequence = (i + 1);
                        policyHolderModel.Birthday = ValueUtils.GetDate(Request.Form["ProductApplicationPolicyHolders[" + i + "].Birthday"]);
                        policyHolders.Add(policyHolderModel);
                    }
                }

                if (policyHolders != null && policyHolders.Count > 0) {
                    appRepo.AddProductApplicationPolicyHolders(policyHolders);
                    application.ProductApplicationPolicyHolders = policyHolders;
                }

                application.ActiveDate = ValueUtils.GetDate(Request.Form["ProductApplication[ActiveDate]"]);
                application.ExpireDate = ValueUtils.GetDate(Request.Form["ProductApplication[ExpireDate]"]);
                appRepo.UpdateProductApplicationCoverageDate(application);

                if (!SendTBrokerAPIMotorCheck(application.ProductApplicationMotor.CarSerialNo, application.ProductApplicationMotor.CarEngineNo, application.ProductApplicationMotor.CarPlateNo1, application.ProductApplicationMotor.CarPlateNo2, application.ProductApplicationMotor.CarProvinceId, application.ActiveDate, false)) {
                    model.ShowMotorContactAgent = true;
                    steps.Push(State);
                    leadRepo.UpdateAPIStatus(leadForm.Id, OrderStatus.Fail);
                    appRepo.UpdateProductApplicationAPIStatusCategory(application.Id, APIStatusCategory.Motor_V_Fail);
                } else {
                    if (application.ProductApplicationMotor.Compulsory > 0 && !SendTBrokerAPIMotorCheck(application.ProductApplicationMotor.CarSerialNo, application.ProductApplicationMotor.CarEngineNo, application.ProductApplicationMotor.CarPlateNo1, application.ProductApplicationMotor.CarPlateNo2, application.ProductApplicationMotor.CarProvinceId, application.ActiveDate, true)) {
                        model.ShowMotorContactAgent = true;
                        steps.Push(State);
                        leadRepo.UpdateAPIStatus(leadForm.Id, OrderStatus.Fail);
                        appRepo.UpdateProductApplicationAPIStatusCategory(application.Id, APIStatusCategory.Motor_C_Fail);
                    }
                }
            }

            if (State == ProductApplicationStage.Motor.AddressForm && model.UserAction == UserAction.Next) {
                application = CopyModel<ProductApplication>(model.ProductApplication);
                application.Id = model.ApplicationId;
                application.AddressBranchNo = NullUtils.cvString(application.AddressBranchNo);
                application.BillingAddressBranchNo = NullUtils.cvString(application.BillingAddressBranchNo);
                application.ShippingAddressBranchNo = NullUtils.cvString(application.ShippingAddressBranchNo);
                #region Setup Address
                if (model.ProductApplication.BillingAddressIsChecked == YesNo.Yes) {
                    application.BillingAddressBranchType = NullUtils.cvInt(model.ProductApplication.AddressBranchType);
                    application.BillingAddressBranchNo = NullUtils.cvString(model.ProductApplication.AddressBranchNo);
                    application.BillingAddressNo = NullUtils.cvString(model.ProductApplication.AddressNo);
                    application.BillingAddressMoo = NullUtils.cvString(model.ProductApplication.AddressMoo);
                    application.BillingAddressVillage = NullUtils.cvString(model.ProductApplication.AddressVillage);
                    application.BillingAddressFloor = NullUtils.cvString(model.ProductApplication.AddressFloor);
                    application.BillingAddressSoi = NullUtils.cvString(model.ProductApplication.AddressSoi);
                    application.BillingAddressRoad = NullUtils.cvString(model.ProductApplication.AddressRoad);
                    application.BillingAddressDistrict = NullUtils.cvString(model.ProductApplication.AddressDistrict);
                    application.BillingAddressSubDistrict = NullUtils.cvString(model.ProductApplication.AddressSubDistrict);
                    application.BillingAddressProvince = NullUtils.cvString(model.ProductApplication.AddressProvince);
                    application.BillingAddressPostalCode = NullUtils.cvString(model.ProductApplication.AddressPostalCode);
                }

                if (model.ProductApplication.ShippingAddressIsChecked == YesNo.Yes) {
                    application.ShippingAddressBranchType = NullUtils.cvInt(model.ProductApplication.AddressBranchType);
                    application.ShippingAddressBranchNo = NullUtils.cvString(model.ProductApplication.AddressBranchNo);
                    application.ShippingAddressNo = NullUtils.cvString(model.ProductApplication.AddressNo);
                    application.ShippingAddressMoo = NullUtils.cvString(model.ProductApplication.AddressMoo);
                    application.ShippingAddressVillage = NullUtils.cvString(model.ProductApplication.AddressVillage);
                    application.ShippingAddressFloor = NullUtils.cvString(model.ProductApplication.AddressFloor);
                    application.ShippingAddressSoi = NullUtils.cvString(model.ProductApplication.AddressSoi);
                    application.ShippingAddressRoad = NullUtils.cvString(model.ProductApplication.AddressRoad);
                    application.ShippingAddressDistrict = NullUtils.cvString(model.ProductApplication.AddressDistrict);
                    application.ShippingAddressSubDistrict = NullUtils.cvString(model.ProductApplication.AddressSubDistrict);
                    application.ShippingAddressProvince = NullUtils.cvString(model.ProductApplication.AddressProvince);
                    application.ShippingAddressPostalCode = NullUtils.cvString(model.ProductApplication.AddressPostalCode);
                }
                #endregion
                appRepo.UpdateProductApplicationAddressForm(application);
                application = appRepo.GetProductApplicationForm(application.Id);
            }

            var path = Server.MapPath(System.Web.Configuration.WebConfigurationManager.AppSettings.Get("Path_Upload"));
            if (State == ProductApplicationStage.Motor.AttachmentForm && model.UserAction == UserAction.Next) {
                #region Upload Files
                List<ProductApplicationAttachment> attachments = new List<ProductApplicationAttachment>();
                
                MoveOn &= UploadAttachmentFile(model.ApplicationId, model.ProductId, model.IDCardFile, FileCategory.IDCard, application, attachments);
                MoveOn &= UploadAttachmentFile(model.ApplicationId, model.ProductId, model.CCTVFile, FileCategory.CCTV, application, attachments);
                MoveOn &= UploadAttachmentFile(model.ApplicationId, model.ProductId, model.CarRegistrationCopyFile, FileCategory.RegisteredCarCopy, application, attachments);
                MoveOn &= UploadAttachmentFile(model.ApplicationId, model.ProductId, model.File4, FileCategory.File4, application, attachments);
                MoveOn &= UploadAttachmentFile(model.ApplicationId, model.ProductId, model.File5, FileCategory.File5, application, attachments);
                MoveOn &= UploadAttachmentFile(model.ApplicationId, model.ProductId, model.File6, FileCategory.File6, application, attachments);
               
                if (attachments.Count > 0) { appRepo.AddProductApplicationAttachments(attachments); }
                #endregion
            }

            if (State == ProductApplicationStage.Base.Final && model.UserAction == UserAction.Next) {
                #region Promotion And Coupon
                var promotionId = NullUtils.cvInt(model.ProductApplication.PromotionId);
                PromotionItem promotion = null; bool hasGift = false;
                ClearPromotionAndCoupon(application);
                if (promotionId > 0 && productItem.Promotion != null) {
                    promotion = productItem.Promotion.Where(prm => prm.Id == promotionId).FirstOrDefault();
                    if (promotion != null) {
                        application.PromotionId = promotionId;
                        var discount = promotion.DiscountAmount;
                        if (promotion.DiscountType == PromotionDiscountType.Amount) {
                            model.TotalDiscount = (discount > application.DisplayPremium ? 0 : discount);
                            UpdatePromotionAndCoupon(application, model.TotalDiscount, promotionId, promotion.CampaignId, string.Empty);
                            hasGift = true;
                        } else {
                            model.TotalDiscount = ((application.DisplayPremium * discount) / 100);
                            UpdatePromotionAndCoupon(application, model.TotalDiscount, promotionId, promotion.CampaignId, string.Empty);
                            hasGift = true;
                        }
                    }
                }

                var systemMessageRepository = new SystemMessageRepository();
                var messages = systemMessageRepository.ListSystemMessage();
                var message = new SystemMessage();
                var couponCode = NullUtils.cvString(model.ProductApplication.CouponCode);
                Coupon coupon = null;

                if (couponCode.Length > 0) {
                    coupon = appRepo.GetCoupon_Active(couponCode, application.ProductId);
                    if (coupon != null) {
                        var checkedDiscount = (promotion != null && promotion.PromotionType == PromotionType.Discount);
                        if (coupon.CouponType == CouponType.OneTime) {
                            if (coupon.TotalUsed == 0) {
                                if (!checkedDiscount) {
                                    if (coupon.DiscountType == CouponDiscountType.Amount) {
                                        model.TotalDiscount += coupon.DiscountAmount;
                                        UpdatePromotionAndCoupon(application, model.TotalDiscount, hasGift ? promotionId : 0, promotion.CampaignId, couponCode);
                                        model.ProductApplication.CouponCode = couponCode;
                                    } else {
                                        model.TotalDiscount += (application.DisplayPremium * (coupon.DiscountAmount) / 100);
                                        UpdatePromotionAndCoupon(application, model.TotalDiscount, hasGift ? promotionId : 0, promotion.CampaignId, couponCode);
                                        model.ProductApplication.CouponCode = couponCode;
                                    }
                                }
                            } else {
                                ClearPromotionAndCoupon(application);
                                model.ProductApplication = application;
                                model.TotalDiscount = 0;
                                message = messages.Where(o => o.Category == SystemMessageCategory.Coupon && o.Code == ((int)SystemMessageStatus.CouponUsed).ToString()).FirstOrDefault();
                                ViewBag.CouponError = Language == Language.TH ? message.BodyTH : message.BodyEN;
                                return View(Language.ToString() + "/Motor", model);
                            }
                        }
                        if (coupon.CouponType == CouponType.Mass) {
                            if (!checkedDiscount) {
                                if (coupon.TotalUsed < coupon.Total) {
                                    if (coupon.DiscountType == CouponDiscountType.Amount) {
                                        model.TotalDiscount += coupon.DiscountAmount;
                                        UpdatePromotionAndCoupon(application, model.TotalDiscount, hasGift ? promotionId : 0, promotion.CampaignId, couponCode);
                                        model.ProductApplication.CouponCode = couponCode;
                                    } else {
                                        model.TotalDiscount += (application.DisplayPremium * (coupon.DiscountAmount) / 100);
                                        UpdatePromotionAndCoupon(application, model.TotalDiscount, hasGift ? promotionId : 0, promotion.CampaignId, couponCode);
                                        model.ProductApplication.CouponCode = couponCode;
                                    }
                                } else {
                                    ClearPromotionAndCoupon(application);
                                    model.ProductApplication = application;
                                    model.TotalDiscount = 0;
                                    message = messages.Where(o => o.Category == SystemMessageCategory.Coupon && o.Code == ((int)SystemMessageStatus.CouponUsed).ToString()).FirstOrDefault();
                                    ViewBag.CouponError = Language == Language.TH ? message.BodyTH : message.BodyEN;
                                    return View(Language.ToString() + "/Motor", model);
                                }
                            } else {
                                ClearPromotionAndCoupon(application);
                                model.ProductApplication = application;
                                model.TotalDiscount = 0;
                                message = messages.Where(o => o.Category == SystemMessageCategory.Coupon && o.Code == ((int)SystemMessageStatus.CouponTopUp).ToString()).FirstOrDefault();
                                ViewBag.CouponError = Language == Language.TH ? message.BodyTH : message.BodyEN;
                                return View(Language.ToString() + "/Motor", model);
                            }
                        }
                    } else {
                        ClearPromotionAndCoupon(application);
                        model.ProductApplication = application;
                        model.TotalDiscount = 0;
                        model.ProductApplication.CouponCode = string.Empty;
                        message = messages.Where(o => o.Category == SystemMessageCategory.Coupon && o.Code == ((int)SystemMessageStatus.CouponUnKnown).ToString()).FirstOrDefault();
                        ViewBag.CouponError = Language == Language.TH ? message.BodyTH : message.BodyEN;
                        return View(Language.ToString() + "/Motor", model);
                    }
                }

                var currentAppForm = appRepo.GetProductApplicationForm(application.Id);
                application.CouponCode = NullUtils.cvString(currentAppForm.CouponCode);
                application.TotalDiscount = currentAppForm.TotalDiscount;
                RecheckPromotion_Active(promotionId, productItem, application);
                RecheckCoupon_Active(couponCode, application);

                #endregion
            }

            model.ProductApplicationPolicyHolders = application.ProductApplicationPolicyHolders != null && application.ProductApplicationPolicyHolders.Count > 0
                ? application.ProductApplicationPolicyHolders.ToArray() : new ProductApplicationPolicyHolder[1] { model.ProductApplicationPolicyHolder };
            model.ProductApplicationDrivers = application.ProductApplicationDrivers != null && application.ProductApplicationDrivers.Count > 0
                ? application.ProductApplicationDrivers.ToArray() : new ProductApplicationDriver[1] { model.ProductApplicationDriver };
            model.ProductApplicationAttachments = application.ProductApplicationAttachment != null && application.ProductApplicationAttachment.Count > 0
                ? application.ProductApplicationAttachment : new List<ProductApplicationAttachment>();

            #region Validate

            MoveOn &= ValidateOtp(application.Id, model.ProductApplicationPolicyHolders);
            MoveOn &= ValidateStringAlphanum(application.ProductApplicationMotor.CarEngineNo);
            MoveOn &= ValidateStringAlphanum(application.ProductApplicationMotor.CarSerialNo);
            MoveOn &= ValidateStringAlphanum(application.ProductApplicationMotor.CarPlateNo1);
            MoveOn &= ValidateStringNumeric(application.ProductApplicationMotor.CarPlateNo2);
            MoveOn &= ValidateStringAlphanum(application.ProductApplicationMotor.CarAccessories);

            MoveOn &= ValidateStringAlphanum(application.ContactName);
            MoveOn &= ValidateStringAlphanum(application.ContactSurname);
            MoveOn &= ValidateStringAlphanumTel(application.ContactTelephone);
            MoveOn &= ValidateStringAlphanumTel(application.ContactMobilePhone);
            MoveOn &= ValidateStringAlphanumEmail(application.ContactEmail);
            MoveOn &= ValidateStringAlphanum(application.ContactLineId);

            MoveOn &= ValidateStringAlphanum(application.BuyerName);
            MoveOn &= ValidateStringAlphanum(application.BuyerSurname);
            MoveOn &= ValidateStringAlphanumTel(application.BuyerTelephone);
            MoveOn &= ValidateStringAlphanumTel(application.BuyerMobilePhone);
            MoveOn &= ValidateStringAlphanumEmail(application.BuyerEmail);
            MoveOn &= ValidateStringAlphanum(application.BuyerLineId);

            MoveOn &= ValidateStringAlphanum(application.AddressBranchNo);
            MoveOn &= ValidateStringAlphanumAddress(application.AddressNo);
            MoveOn &= ValidateStringAlphanum(application.AddressMoo);
            MoveOn &= ValidateStringAlphanum(application.AddressVillage);
            MoveOn &= ValidateStringNumeric(application.AddressFloor);
            MoveOn &= ValidateStringAlphanum(application.AddressSoi);
            MoveOn &= ValidateStringAlphanum(application.AddressRoad);
            MoveOn &= ValidateStringAlphanum(application.AddressPostalCode);

            MoveOn &= ValidateStringAlphanum(application.BillingAddressBranchNo);
            MoveOn &= ValidateStringAlphanumAddress(application.BillingAddressNo);
            MoveOn &= ValidateStringAlphanum(application.BillingAddressMoo);
            MoveOn &= ValidateStringAlphanum(application.BillingAddressVillage);
            MoveOn &= ValidateStringNumeric(application.BillingAddressFloor);
            MoveOn &= ValidateStringAlphanum(application.BillingAddressSoi);
            MoveOn &= ValidateStringAlphanum(application.BillingAddressRoad);
            MoveOn &= ValidateStringAlphanum(application.BillingAddressPostalCode);

            MoveOn &= ValidateStringAlphanum(application.ShippingAddressBranchNo);
            MoveOn &= ValidateStringAlphanumAddress(application.ShippingAddressNo);
            MoveOn &= ValidateStringAlphanum(application.ShippingAddressMoo);
            MoveOn &= ValidateStringAlphanum(application.ShippingAddressVillage);
            MoveOn &= ValidateStringNumeric(application.ShippingAddressFloor);
            MoveOn &= ValidateStringAlphanum(application.ShippingAddressSoi);
            MoveOn &= ValidateStringAlphanum(application.ShippingAddressRoad);
            MoveOn &= ValidateStringAlphanum(application.ShippingAddressPostalCode);

            foreach (var it in model.ProductApplicationDrivers) {
                MoveOn &= ValidateStringAlphanum(it.Name);
                MoveOn &= ValidateStringAlphanum(it.Surname);
                MoveOn &= ValidateStringAlphanum(it.RefIDCard);
                MoveOn &= ValidateStringAlphanum(it.IDCard);
            }
            foreach (var it in model.ProductApplicationPolicyHolders) {
                MoveOn &= ValidateStringAlphanum(it.Name);
                MoveOn &= ValidateStringAlphanum(it.Surname);
                MoveOn &= ValidateStringAlphanum(it.RefIDCard);
                MoveOn &= ValidateStringAlphanum(it.IDCard);
                MoveOn &= ValidateStringAlphanumTel(it.Telephone);
                MoveOn &= ValidateStringAlphanumTel(it.MobilePhone);
                MoveOn &= ValidateStringAlphanumEmail(it.Email);
                MoveOn &= ValidateStringAlphanum(it.BeneficiaryName);
                MoveOn &= ValidateStringAlphanum(it.BeneficiarySurname);
                MoveOn &= ValidateStringAlphanum(it.BeneficiaryIDCard);
                MoveOn &= ValidateStringAlphanumTel(it.BeneficiaryMobilePhone);
                MoveOn &= ValidateStringAlphanum(it.LineId);
                MoveOn &= ValidateStringAlphanum(it.Instagram);
                MoveOn &= ValidateStringAlphanum(it.Facebook);
            }

            #endregion

            if (State != ProductApplicationStage.Base.Success || (State == ProductApplicationStage.Base.Success && model.UserAction == UserAction.Back)) {
                if (MoveOn) {
                    if (model.UserAction == UserAction.Next) { State = steps.Pop(); overSteps.Push(State); } else { State = overSteps.Pop(); steps.Push(State); };
                    model.State = model.UserAction == UserAction.Next ? steps.Peek() : State;
                    application.Steps = steps != null ? string.Join("-", steps.ToList().Select(x => (int)x).Reverse().ToArray()) : string.Empty;
                    application.OverSteps = overSteps != null ? string.Join("-", overSteps.ToList().Select(x => (int)x).Reverse().ToArray()) : string.Empty;
                    application.Step = (int)model.State;

                    appRepo.UpdateProductApplicationState(application);
                }
                model.ProductApplication = application;
            }

            if (State == ProductApplicationStage.Base.Success && model.UserAction == UserAction.Next) {
                var couponRepo = new CouponRepository();
                var order = GenerateOrder(application, ORDER_MOTOR, leadForm != null ? leadForm.Id : 0);
                if (order != null) {
                    couponRepo.UpdateUsedCouponCode(NullUtils.cvString(application.CouponCode), order.Id);
                }

                State = ProductApplicationStage.Base.Success;
                model.State = State;
                application.Step = (int)model.State;
                application.Language = Language;
                appRepo.UpdateProductApplicationState(application);
                return Redirect(Url.Action("PaymentRequest") + "/?ApplicationId=" + application.Id);
            }

            if (model.ProductApplicationMotor.ApplicationId == 0) model.ProductApplicationMotor = application.ProductApplicationMotor;

            return View(Language.ToString() + "/Motor", model);
        }

        #endregion

        private void RecheckCoupon_Active(string couponCode, ProductApplication application) {
            if (application != null) {
                ProductApplicationRepository applicationRepository = new ProductApplicationRepository();
                var coupon = applicationRepository.GetCoupon_Active(couponCode, application.ProductId);
                bool inactive = false;
                if (!string.IsNullOrEmpty(couponCode) && coupon == null) // The promotion has been deleted;
                {
                    inactive = true;
                }

                if (inactive) {
                    applicationRepository.UpdateProductApplicationPromotion(application.Id, 0, 0, "");
                }
            }
        }

        private void RecheckPromotion_Active(int promotionId, ProductItem productItem, ProductApplication application) {
            if (application != null) {
                ProductApplicationRepository applicationRepository = new ProductApplicationRepository();
                bool inactive = false;
                if (promotionId > 0 && productItem.Promotion == null) // The promotion has been deleted;
                {
                    inactive = true;
                }

                if (promotionId > 0 && productItem.Promotion != null) {
                    if (!productItem.Promotion.Exists(o => o.Id == promotionId)) {
                        inactive = true;
                    }
                }

                if (inactive) {
                    applicationRepository.UpdateProductApplicationPromotion(application.Id, 0, 0, "");
                }
            }
        }

        #region Support Methods

        private Order GenerateOrder(ProductApplication app, string productApp, int leadFormId) {
            OrderRepository orderRepo = new OrderRepository();

            if (app != null) {
                var order = orderRepo.GetOrder_ByProductApplicationId(app.Id);

                if (order == null) {
                    Order newOrder = new Order();

                    newOrder.LeadFormId = NullUtils.cvInt(leadFormId);
                    newOrder.ProductApplicationId = app.Id;
                    newOrder.CANo = string.Empty;
                    newOrder.PolicyNo = string.Empty;
                    newOrder.PolicyNoAdd = string.Empty;
                    newOrder.OrderCode = string.Empty;
                    newOrder.PaymentId = string.Empty;
                    newOrder.PaymentChannel = PaymentChannel.CreditCard;
                    newOrder.PaymentStatus = PaymentStatus.Pending;
                    newOrder.PaymentDate = DateUtils.SqlMinDate();
                    newOrder.PaymentInstallment = 0;
                    newOrder.PaymentTotal = (app.DisplayPremium - app.TotalDiscount);
                    newOrder.PartnerAPIStatus = PartnerAPIStatus.Pending;
                    newOrder.BackendAPIStatus = BackendAPIStatus.Pending;
                    newOrder.Status = OrderStatus.Pending;
                    newOrder.CreateDate = DateTime.Now;
                    newOrder.UpdateDate = DateUtils.SqlMinDate();

                    var result = orderRepo.AddOrder(newOrder, productApp, Channel);
                    if (result > 0) {
                        return orderRepo.GetOrder(result);
                    }
                } else {
                    order.UpdateDate = DateTime.Now;
                    return order;
                }

            }

            return null;
        }

        private bool CheckOrderStatus(int id) {
            OrderRepository repository = new OrderRepository();
            var order = repository.GetOrder_ByProductApplicationId(id);
            if (order != null) {
                if (order.PaymentStatus == PaymentStatus.Retry) {
                    return false;
                }
                return true;
            }

            return false;
        }

        private bool UploadAttachmentFile(int applicationId, int productId, HttpPostedFileBase file, FileCategory fileCategory, ProductApplication app, List<ProductApplicationAttachment> attachments) {
            if (file != null && file.ContentLength > 0) {
                string ext = Path.GetExtension(file.FileName).ToLower().Trim('.');
                string[] allows = new OrderViewModel().ALLOWED_UPLOAD_FILE_TYPES.ToLower().Split(',');
                if (!allows.Contains(ext)) return false;

                var fileName = string.Format("{0}_{1}_{2}", applicationId, (int)fileCategory, file.FileName);
                var fileSize = file.InputStream.Length;
                byte[] bytes;

                using (System.IO.BinaryReader br = new System.IO.BinaryReader(file.InputStream)) {
                    bytes = br.ReadBytes(file.ContentLength);
                }

                ProductApplicationAttachment attachment = new ProductApplicationAttachment();
                attachment.ApplicationId = applicationId;
                attachment.FileSize = fileSize;
                attachment.FileContent = bytes;
                attachment.FileCategory = (int)fileCategory;
                attachment.FileName = fileName;
                attachment.CreateDate = DateTime.Now;
                attachments.Add(attachment);
            } else {
                if (app != null && app.ProductApplicationAttachment != null && app.ProductApplicationAttachment.Count > 0) {
                    var item = app.ProductApplicationAttachment.Where(o => o.ApplicationId == applicationId && o.FileCategory == (int)fileCategory).FirstOrDefault();
                    if (item != null) {
                        item.UpdateDate = DateTime.Now;
                        attachments.Add(item);
                    }
                }
            }
            return true;
        }


        private void UpdatePromotionAndCoupon(ProductApplication application, decimal TotalDiscount, int promotionId, int campaignId, string couponCode) {
            if (application != null) {
                ProductApplicationRepository appRepo = new ProductApplicationRepository();
                application.TotalDiscount = TotalDiscount;
                appRepo.UpdateProductApplicationPromotion(application.Id, promotionId, campaignId, couponCode);
                appRepo.UpdateProductApplicationTotalDiscount(application.Id, application.TotalDiscount);
            }
        }

        private void ClearPromotionAndCoupon(ProductApplication application) {
            if (application != null) {
                ProductApplicationRepository appRepo = new ProductApplicationRepository();
                application.TotalDiscount = 0;
                appRepo.UpdateProductApplicationPromotion(application.Id, 0, 0, string.Empty);
                appRepo.UpdateProductApplicationTotalDiscount(application.Id, 0);
            }
        }

        private LeadForm GetLeadForm() {
            LeadFormRepository leadRepo = new LeadFormRepository();
            if (Session != null && NullUtils.cvInt(Session["LeadFormId"]) > 0) return leadRepo.GetLeadForm_Active(NullUtils.cvInt(Session["LeadFormId"]));
            LeadForm model = new TobJod.Models.LeadForm();
            model.SessionId = Session.SessionID;
            model.IPPublic = HttpUtils.GetPublicIP();
            model.IPPrivate = HttpUtils.GetPrivateIP();
            model.CreateDate = DateTime.Now;
            var check = leadRepo.GetLeadFormExists(model);
            if (check != null) Session["LeadFormId"] = model.Id;
            return check;
        }

        private T CopyModel<T>(T item) where T : new() {
            T obj = new T();

            var properties = TypeDescriptor.GetProperties(item);

            foreach (PropertyDescriptor prop in properties) {
                string name = prop.Name;
                if (prop.GetValue(item) != null) {
                    PropertyDescriptor p = TypeDescriptor.GetProperties(obj)[name];
                    p.SetValue(obj, prop.GetValue(item));
                }
            }

            return obj;
        }

        private string CreateSteps(Dictionary<int, ProductApplicationField> fields) {
            List<int> builder = new List<int>();
            builder.Add(ProductApplicationStage.Base.Success);
            builder.Add(ProductApplicationStage.Base.Final);

            foreach (var item in fields.Select(x => new { x.Value.Step }).OrderByDescending(o => o.Step).GroupBy(g => g.Step)) {
                var state = item.Key;
                builder.Add(state);
            }

            return string.Join("-", builder.ToArray());
        }

        private Stack<int> ConvertToApplicationSteps(string steps) {
            // 0-5-4-3-2-1
            string[] arr = steps.Split('-');
            Stack<int> stack = new Stack<int>();


            if (arr != null && arr != null) {
                foreach (string str in arr) {
                    stack.Push(NullUtils.cvInt(str));
                }
            }

            return stack;
        }

        private ProductCategoryKey GetProductCategory(int productId) {
            ProductApplicationRepository appRepo = new ProductApplicationRepository();
            ProductSubCategory productSubCatetory = appRepo.GetProductSubCategory(productId, Language);
            ProductCategoryKey categoryKey = ProductCategoryKey.Home;
            try { categoryKey = (ProductCategoryKey)Enum.Parse(typeof(ProductCategoryKey), NullUtils.cvString(productSubCatetory.Code)); } catch { }

            return categoryKey;
        }

        private string GenerateOrderToken(int applicationId, int productId) {
            return GenerateOrderToken(applicationId.ToString(), productId.ToString());
        }

        private string GenerateOrderToken(string applicationId, string productId) {
            return SecurityUtils.GenerateSHA256String(TOKEN_SALT + "|" + applicationId + "|" + productId);
        }

        private bool ValidateOrderToken() {
            string token = Request.QueryString["Token"];
            string applicationId = Request.QueryString["ApplicationId"];
            string productId = Request.QueryString["ProductId"];
            return string.Equals(token, GenerateOrderToken(applicationId, productId));
        }

        public Tuple<string, string> GetCarPlateNo(string val) {
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(@"(?<p1>\d?(\D+))(?<p2>\d+)", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            var match = regex.Match(val);
            if (match.Groups.Count > 2) {
                return Tuple.Create(match.Groups["p1"].Value, match.Groups["p2"].Value);
            }
            return Tuple.Create(val, "");
        }

        public bool ValidateOtp(int applicationId, ProductApplicationPolicyHolder[] policyHolder) {
            if (policyHolder == null || policyHolder.Length < 1 || string.IsNullOrEmpty(policyHolder[0].IDCard) || string.IsNullOrEmpty(policyHolder[0].MobilePhone)) return true;
            SmsOtpRepository repo = new SmsOtpRepository();
            var item = repo.GetSmsOtpSuccess(applicationId, policyHolder[0].IDCard, policyHolder[0].MobilePhone);
            return item != null;
        }

        public bool ValidateStringAlphanum(string val) {
            return ValidateStringPattern(val, "^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$"); //[a-zA-Z�-��-�0-9]
        }

        public bool ValidateStringNumeric(string val) {
            return ValidateStringPattern(val, "^[0-9]+$");
        }

        public bool ValidateStringAlphanumTel(string val) {
            return ValidateStringPattern(val, "^[0-9]+$");
        }

        public bool ValidateStringAlphanumAddress(string val) {
            return ValidateStringPattern(val, "^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$");
        }

        public bool ValidateStringAlphanumEmail(string val) {
            return ValidateStringPattern(val, "^[^<>~\\u005E:;%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$");
        }

        public bool ValidateStringAlphanumSocial(string val) {
            return ValidateStringPattern(val, "^[^<>~\\u005E:;%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$");
        }

        public bool ValidateStringPattern(string val, string pattern) {
            if (string.IsNullOrEmpty(val)) return true;
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(pattern, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            var match = regex.Match(val);
            return match.Success;
        }

        #endregion

        #region "Payment Gateway"
        [HttpGet]
        public ActionResult Error(SystemMessageCategory Category = SystemMessageCategory.Payment2C2P, string PaymentStatus = "", string ChannelResponseCode = "", string redirectUrl = "", string Language = "th") {
            SystemMessageRepository repository = new SystemMessageRepository();
            var message = string.Empty;
            redirectUrl = Url.Content("~");

            if (!string.IsNullOrEmpty(PaymentStatus) && !string.IsNullOrEmpty(ChannelResponseCode)) {
                var result = repository.ListSystemMessage();
                var channel_response_code = ChannelResponseCode.Trim();
                if (result != null && result.Count > 0) {
                    var item = result.Where(o => o.Code == PaymentStatus.Trim() && o.SubCode == channel_response_code && o.Category == Category).FirstOrDefault();
                    var temp = result.Where(o => o.Code == "999" && o.Category == SystemMessageCategory.Payment2C2P).FirstOrDefault();
                    if (item != null) {
                        message = Language.ToLower() == "th" ? item.BodyTH : item.BodyEN;
                    } else {
                        message = Language.ToLower() == "th" ? temp.BodyTH : temp.BodyEN;
                    }

                    if (PaymentStatus == "002" || PaymentStatus == "999") {
                        if (channel_response_code == "9022" || channel_response_code == "9055" ||
                            channel_response_code == "9043" || channel_response_code == "009" || channel_response_code == "002") {
                            //ViewBag.BackUrl = redirectUrl + Language.ToLower();
                        }
                    } else {
                        //ViewBag.PreviousUrl = Url.Content("~") + Language.ToString();
                    }
                }
            }

            ViewBag.Message = message;
            ViewBag.PreviousUrl = redirectUrl + Language.ToLower();
            ViewBag.BackUrl = redirectUrl + Language.ToLower();

            return View(Language.ToString() + "/Error");
        }

        [HttpGet]
        public ActionResult Thankyou(string PaymentStatus, string ChannelResponseCode, string Language = "th") {
            SystemMessageRepository repository = new SystemMessageRepository();
            var message = string.Empty;
            if (!string.IsNullOrEmpty(PaymentStatus) && !string.IsNullOrEmpty(ChannelResponseCode)) {
                var result = repository.ListSystemMessage();
                if (result != null && result.Count > 0) {
                    var item = result.Where(o => o.Code == PaymentStatus.Trim() && o.SubCode == ChannelResponseCode.Trim() && o.Category == SystemMessageCategory.Payment2C2P).FirstOrDefault();
                    var temp = result.Where(o => o.Code == "999" && o.Category == SystemMessageCategory.Payment2C2P).FirstOrDefault();
                    if (item != null) {
                        message = Language.ToLower() == "th" ? item.BodyTH : item.BodyEN;
                    } else {
                        message = Language.ToLower() == "th" ? temp.BodyTH : temp.BodyEN;
                    }
                }
            }

            ViewBag.Message = message;
            ViewBag.PreviousUrl = Url.Content("~") + Language.ToLower();
            ViewBag.BackUrl = Url.Content("~") + Language.ToLower();

            return View(Language.ToString() + "/Thankyou");
        }

        [HttpGet]
        public ActionResult PaymentRequest() {
            ProductApplicationRepository appRepo = new ProductApplicationRepository();
            ProductRepository productRepo = new ProductRepository();
            CompanyRepository companyRepo = new CompanyRepository();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            PaymentRequest payment = new PaymentRequest();

            int applicationId = NullUtils.cvInt(Request.QueryString.Get("ApplicationId"));
            bool onlinePayment = NullUtils.cvBoolean(System.Configuration.ConfigurationManager.AppSettings["Online_Payment_Enable"]);
            ProductApplication app = applicationId != 0 ? appRepo.GetProductApplicationForm(NullUtils.cvInt(applicationId)) : null;

            if (!onlinePayment) { return RedirectToAction("/Error", new { Category = (int)SystemMessageCategory.OfflinePayment, PaymentStatus = "3001", ChannelResponseCode = (int)SystemMessageStatus.OfflinePayment }); }
            if ((app == null || app.Order == null)) { return RedirectToAction("/Error"); }

            ProductItem productItem = productRepo.GetProduct_Active(Language, app.ProductId, app.PremiumId);
            Company company = null;
            if (productItem != null) {
                company = companyRepo.GetCompany_Active(productItem.CompanyId);
                var checkMerchantConfig = (string.IsNullOrWhiteSpace(company.PaymentMerchantId) || string.IsNullOrWhiteSpace(company.PaymentSecretKey));
                if (company == null || checkMerchantConfig) {
                    return RedirectToAction("/Error");
                }
            } else { return RedirectToAction("/Error"); }


            Order order = app.Order;

            var date = GetExpireDate(app);
            var expireDate = date.ToString("yyyy-MM-dd HH:mm:ss", cultureInfoEN);
            var email = app.ProductApplicationPolicyHolders != null && app.ProductApplicationPolicyHolders.Count > 0 ?
                NullUtils.cvString(app.ProductApplicationPolicyHolders[0].Email) : string.Empty;

            payment.payment_url = System.Configuration.ConfigurationManager.AppSettings["PaymentUrl"];
            payment.merchant_id = company.PaymentMerchantId;
            payment.secret_key = company.PaymentSecretKey;
            payment.amount = (app.DisplayPremium - app.TotalDiscount);
            payment.order_id = order.OrderCode;
            payment.default_lang = Language.ToString();
            payment.payment_description = NullUtils.cvString(app.ProductName) + "";
            payment.default_lang = NullUtils.cvString(app.Language.ToString(), Language.TH.ToString()).ToString();
            payment.payment_expiry = expireDate;
            payment.customer_email = email;
            payment.hash_value = payment.EncryptSignature();

            ViewBag.Url = payment.payment_url;
            ViewBag.DefaultLang = payment.default_lang;
            ViewBag.Version = payment.version;
            ViewBag.MerchantId = payment.merchant_id;
            ViewBag.Currency = payment.currency2;
            ViewBag.ResultUrl1 = payment.result_url_1;
            ViewBag.ResultUrl2 = payment.result_url_2;
            ViewBag.HashValue = payment.hash_value;
            ViewBag.PaymentDescription = payment.payment_description;
            ViewBag.OrderId = payment.order_id;
            ViewBag.Amount = payment.amount2;
            ViewBag.ExpireDate = expireDate;
            ViewBag.Email = payment.customer_email;

            string message = "[REQUEST_PARAMETERS]" + Environment.NewLine + serializer.Serialize(payment);
            string lang = Language.TH.ToString().ToLower();
            try { LogUtils.LogMessage(message); } catch { }

            return View(Language.ToString() + "/Landing");
        }

        private DateTime GetExpireDate(ProductApplication app) {
            string str = string.Empty;
            ConfigRepository configRepo = new ConfigRepository();
            var config = configRepo.GetConfig(ConfigName.PaymentExpireDay);
            var productCategory = GetProductCategory(app.ProductId);
            var activeDate = app.ActiveDate;
            var payDate = CreatePayDate(DateTime.Now);
            var payDays = NullUtils.cvInt(config.Body);

            if (config != null && payDays > 0 && activeDate > DateTime.Now) {
                payDate = payDate.AddDays(payDays);
                activeDate = activeDate.AddDays(-1);
                return payDate < activeDate ? CreatePayDate(payDate) : CreatePayDate(activeDate);
            }

            return payDate;
        }

        private DateTime CreatePayDate(DateTime date) {
            return new DateTime(date.Year, date.Month, date.Day, 23, 59, 59);
        }

        [HttpPost]
        public ActionResult FrontendPaymentResponse() {
            OrderRepository orderRepo = new OrderRepository();
            ProductApplicationRepository repo = new ProductApplicationRepository();
            PaymentResponse payment = new PaymentResponse();
            payment = MapNameValueToObject<PaymentResponse>(payment, Request.Form);
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string message = "[FRONT_END_RESPONSE]" + Environment.NewLine + serializer.Serialize(payment);
            string lang = Language.TH.ToString().ToLower();
            ProductApplication app = null;
            try { LogUtils.LogMessage(message); } catch { }
            string url = string.Empty;
            //payment_status":"999","channel_response_code":"9055"
            if (payment != null) {
                PaymentStatus paymentStatus = GetPaymentStatus(payment.payment_status);
                Order order = new Order();

                order = orderRepo.GetOrderByOrderCode(payment.order_id);
                if (order != null) {
                    app = repo.GetProductApplication(order.ProductApplicationId);
                    if (app != null) lang = app.Language.ToString().ToLower();
                    var paymentChannel = GetPaymentChannel(payment.payment_channel);
                    var productCategory = GetProductCategory(app.ProductId);
                    order.PaymentChannel = paymentChannel;

                    if ((paymentStatus == PaymentStatus.Success && payment.channel_response_code == "00") || (paymentStatus == PaymentStatus.Success && payment.channel_response_code == "000")) {
                        return Redirect(Url.Content("~") + "/" + lang + "/order/" + "ThankYou?PaymentStatus=" + payment.payment_status + "&ChannelResponseCode=" + payment.channel_response_code + "&Language=" + lang);
                    }

                    if (paymentStatus == PaymentStatus.Pending) {
                        if (paymentChannel == PaymentChannel.Cash && (payment.channel_response_code == SUCCESS_PAID || payment.channel_response_code == SUCCESS_PENDING)) {
                            return Redirect(Url.Content("~") + "/" + lang + "/order/" + "ThankYou?PaymentStatus=" + payment.payment_status + "&ChannelResponseCode=" + payment.channel_response_code + "&Language=" + lang);
                        }
                    }

                    if (order.PaymentStatus == PaymentStatus.Retry) {
                        var redirectUrl = (Url.Content("~") + "/" + lang + "/order/" + productCategory.ToString() + "?ApplicationId=" + app.Id + "&ProductId=" + app.ProductId);
                        return Redirect(Url.Content("~") + "/" + lang + "/order/" + "Error/?PaymentStatus=" + payment.payment_status + " &ChannelResponseCode=" + payment.channel_response_code + "&redirectUrl=" + redirectUrl + "&Language=" + lang);
                    }

                }
            }
            return Redirect(Url.Content("~") + "/" + lang + "/order/" + "Error/?PaymentStatus=" + payment.payment_status + " &ChannelResponseCode=" + payment.channel_response_code + "&redirectUrl=" + "&Language=" + lang);

        }

        [HttpPost]
        public void BackendPaymentResponse() {
            OrderRepository orderRepo = new OrderRepository();
            PaymentResponse payment = new PaymentResponse();
            ProductApplicationRepository repo = new ProductApplicationRepository();
            payment = MapNameValueToObject<PaymentResponse>(payment, Request.Form);
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string message = "[BACK_END_RESPONSE]" + Environment.NewLine + serializer.Serialize(payment);
            ProductApplication app = null;
            LogUtils.LogMessage(message);
            //bool sendSubmit = false;
            bool success = false;
            bool sendMail = false;

            PaymentStatus paymentStatus = GetPaymentStatus(payment.payment_status);
            Order order = new Order();

            if (payment != null) {
                order = orderRepo.GetOrderByOrderCode(payment.order_id);
                if (order != null) {
                    var paymentChannel = GetPaymentChannel(payment.payment_channel);
                    var channelResponseCode = payment.channel_response_code;
                    order.PaymentChannel = paymentChannel;
                    app = repo.GetProductApplication(order.ProductApplicationId);
                    var productCategory = GetProductCategory(app.ProductId);

                    /* PAYMENT CHANNEL RESPONSE CODE
                    001 Credit and debit cards
                    002 Cash payment channel
                    003 Direct debit
                    004 Others
                    005 IPP transaction
                     */
                    if (paymentStatus == PaymentStatus.Reject) {
                        order.PaymentStatus = PaymentStatus.Fail;
                        order.Status = OrderStatus.Fail;
                    } else if (paymentStatus == PaymentStatus.CanceledByUser) {
                        order.PaymentStatus = PaymentStatus.CanceledByUser;
                        order.Status = OrderStatus.Cancel;
                    } else if (paymentChannel == PaymentChannel.CreditCard) {
                        if (paymentStatus == PaymentStatus.Success) {
                            order.PaymentStatus = PaymentStatus.Success;
                            order.Status = OrderStatus.Success;
                            order.PaymentDate = DateTime.Now;
                            //SendPartnerAPI(order.Id);
                            //SendTBrokerAPI(order.Id);
                            //sendSubmit = true;
                            sendMail = true;
                            success = true;
                        }
                    } else if (paymentChannel == PaymentChannel.DirectDebit) {
                        if (paymentStatus == PaymentStatus.Success) {
                            order.PaymentStatus = PaymentStatus.Success;
                            order.Status = OrderStatus.Success;
                            order.PaymentDate = DateTime.Now;
                            //SendPartnerAPI(order.Id);
                            //SendTBrokerAPI(order.Id);
                            //sendSubmit = true;
                            sendMail = true;
                            success = true;
                        }
                    } else if (paymentChannel == PaymentChannel.Cash) // Bill Payment
                      {
                        if (paymentStatus == PaymentStatus.Pending) {
                            /* CHANNEL RESPONSE CODE
                            000 Success (PAID) - only for WEB PAY channel
                            001 Success (PENDING) - for all other channels
                             */
                            if (channelResponseCode == "000" || channelResponseCode == "001") {
                                order.PaymentStatus = PaymentStatus.Pending;
                                order.Status = OrderStatus.Pending;
                                success = true;
                                if (app != null && app.ProductApplicationMotor != null) {
                                    if (productCategory == ProductCategoryKey.Motor || productCategory == ProductCategoryKey.PRB) {
                                        var motor = app.ProductApplicationMotor;
                                        var expireDate = GetExpireDate(app);
                                        SendTBrokerAPIMotorReserve(motor.CarSerialNo, motor.CarEngineNo, motor.CarPlateNo1, motor.CarPlateNo2, motor.CarProvinceId, expireDate);
                                    }
                                }
                            }
                        } else if (paymentStatus == PaymentStatus.Success) {
                            order.PaymentStatus = PaymentStatus.Success;
                            order.Status = OrderStatus.Success;
                            order.PaymentDate = DateTime.Now;
                            //SendPartnerAPI(order.Id);
                            //SendTBrokerAPI(order.Id);
                            //sendSubmit = true;
                            sendMail = true;
                            success = true;
                        }

                    } else if (paymentChannel == PaymentChannel.IPP) {
                        if (paymentStatus == PaymentStatus.Success) {
                            order.PaymentStatus = PaymentStatus.Success;
                            order.Status = OrderStatus.Success;
                            order.PaymentDate = DateTime.Now;
                            success = true;
                            //SendPartnerAPI(order.Id);
                            //SendTBrokerAPI(order.Id);
                            //sendSubmit = true;
                            sendMail = true;
                        }
                    } else {
                        order.PaymentStatus = PaymentStatus.Fail;
                        order.Status = OrderStatus.Fail;
                        success = true;
                    }

                    if (!success) {
                        order.PaymentStatus = PaymentStatus.Fail;
                        order.Status = OrderStatus.Fail;
                    }

                    //if (sendMail) {
                    //    SendMail(order.Id);
                    //}

                    order.UpdateDate = DateTime.Now;
                    order.PaymentChannel = paymentChannel;
                    order.PaymentInstallment = NullUtils.cvInt(payment.ippPeriod);
                    order.PaidAgent = NullUtils.cvString(payment.paid_agent);
                    order.PaidChannel = NullUtils.cvString(payment.paid_channel);
                    order.PaidRef = NullUtils.cvString(payment.transaction_ref);

                    BankRepository bankRepo = new BankRepository();
                    var bank = bankRepo.GetBankByPaymentGatewayCode(order.PaidAgent);
                    if (bank != null) {
                        order.Bank = bank.Code;
                    } else {
                        order.Bank = "";
                    }

                    orderRepo.UpdateOrderPaymentStatus(order, payment.channel_response_code);

                    if (sendMail) {
                        SendMail(order.Id);
                        SendPartnerAPI(order.Id);
                        SendTBrokerAPI(order.Id);
                    }
                }

            }
        }

        //[HttpPost]
        //public void BackendPaymentResponse()
        //{
        //    OrderRepository orderRepo = new OrderRepository();
        //    PaymentResponse payment = new PaymentResponse();
        //    ProductApplicationRepository repo = new ProductApplicationRepository();
        //    payment = MapNameValueToObject<PaymentResponse>(payment, Request.Form);
        //    JavaScriptSerializer serializer = new JavaScriptSerializer();
        //    string message = "[BACK_END_RESPONSE]" + Environment.NewLine + serializer.Serialize(payment);
        //    ProductApplication app = null;
        //    LogUtils.LogMessage(message);

        //    PaymentStatus paymentStatus = GetPaymentStatus(payment.payment_status);
        //    Order order = new Order();

        //    if (payment != null)
        //    {
        //        order = orderRepo.GetOrderByOrderCode(payment.order_id);
        //        if (order != null)
        //        {
        //            order.PaymentChannel = GetPaymentChannel(payment.payment_channel);
        //            app = repo.GetProductApplication(order.ProductApplicationId);
        //            var productCategory = GetProductCategory(app.ProductId);

        //            if (paymentStatus == PaymentStatus.Success)
        //            {
        //                if (payment.channel_response_code == SUCCESS_PAID || payment.channel_response_code == SUCCESS_PENDING)
        //                {
        //                    order.PaymentStatus = PaymentStatus.Pending;
        //                    order.Status = OrderStatus.Pending;

        //                    if (app != null && app.ProductApplicationMotor != null)
        //                    {
        //                        if (productCategory == ProductCategoryKey.Motor || productCategory == ProductCategoryKey.PRB)
        //                        {
        //                            var motor = app.ProductApplicationMotor;
        //                            var expireDate = GetExpireDate(app);
        //                            SendTBrokerAPIMotorReserve(motor.CarSerialNo, motor.CarEngineNo, motor.CarPlateNo1, motor.CarPlateNo2, NullUtils.cvString(motor.CarProvinceId), expireDate);
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    order.PaymentStatus = PaymentStatus.Success;
        //                    order.Status = OrderStatus.Success;
        //                    order.PaymentDate = DateTime.Now;
        //                    SendPartnerAPI(order.Id);
        //                    SendTBrokerAPI(order.Id);
        //                }
        //            }
        //            else if (paymentStatus == PaymentStatus.Pending)
        //            {
        //                if (payment.channel_response_code == SUCCESS_PAID || payment.channel_response_code == SUCCESS_PENDING)
        //                {
        //                    order.PaymentStatus = PaymentStatus.Pending;
        //                    order.Status = OrderStatus.Pending;

        //                    if (app != null && app.ProductApplicationMotor != null)
        //                    {
        //                        if (productCategory == ProductCategoryKey.Motor || productCategory == ProductCategoryKey.PRB)
        //                        {
        //                            var motor = app.ProductApplicationMotor;
        //                            var expireDate = GetExpireDate(app);
        //                            SendTBrokerAPIMotorReserve(motor.CarSerialNo, motor.CarEngineNo, motor.CarPlateNo1, motor.CarPlateNo2, NullUtils.cvString(motor.CarProvinceId), expireDate);
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    order.PaymentStatus = PaymentStatus.Fail;
        //                    order.Status = OrderStatus.Fail;
        //                }
        //            }
        //            else if (paymentStatus == PaymentStatus.CanceledByUser)
        //            {
        //                order.PaymentStatus = PaymentStatus.CanceledByUser;
        //                order.Status = OrderStatus.Cancel;
        //            }
        //            else if (paymentStatus == PaymentStatus.Reject)
        //            {
        //                if (payment.channel_response_code == "9022" || payment.channel_response_code == "9055" ||
        //                    payment.channel_response_code == "9043" || payment.channel_response_code == "009" || payment.channel_response_code == "002")
        //                {
        //                    // This order has already been retired with payment
        //                    if (order.UpdateDate != TobJod.Utils.DateUtils.SqlMinDate())
        //                    {
        //                        order.PaymentStatus = PaymentStatus.Fail;
        //                        order.Status = OrderStatus.Fail;
        //                    }
        //                    else
        //                    {
        //                        order.PaymentStatus = PaymentStatus.Retry;
        //                        order.Status = OrderStatus.Pending;
        //                    }
        //                }
        //                else
        //                {
        //                    order.PaymentStatus = PaymentStatus.Fail;
        //                    order.Status = OrderStatus.Fail;
        //                }
        //            }
        //            else
        //            {
        //                order.PaymentStatus = PaymentStatus.Fail;
        //                order.Status = OrderStatus.Fail;
        //            }

        //            order.UpdateDate = DateTime.Now;
        //            order.PaymentChannel = GetPaymentChannel(payment.payment_channel);
        //            order.PaymentInstallment = NullUtils.cvInt(payment.ippPeriod);
        //            order.PaidAgent = NullUtils.cvString(payment.paid_agent);
        //            order.PaidChannel = NullUtils.cvString(payment.paid_channel);
        //            order.PaidRef = NullUtils.cvString(payment.transaction_ref);

        //            BankRepository bankRepo = new BankRepository();
        //            var bank = bankRepo.GetBankByPaymentGatewayCode(order.PaidAgent);
        //            if (bank != null) {
        //                order.Bank = bank.Code;
        //            } else {
        //                order.Bank = "";
        //            }

        //            orderRepo.UpdateOrderPaymentStatus(order, payment.channel_response_code);
        //        }

        //    }
        //}


        private PaymentStatus GetPaymentStatus(string payment_status) {
            switch (payment_status) {
                case "000": return PaymentStatus.Success; //Payment Successful
                case "001": return PaymentStatus.Pending; //Payment Pending
                case "002": return PaymentStatus.Reject;  //Payment Rejected
                case "003": return PaymentStatus.CanceledByUser; //Payment was canceled by user
                case "999": return PaymentStatus.Fail; // Payment Failed, Timeout
                default: return PaymentStatus.Fail;
            }
        }

        private PaymentChannel GetPaymentChannel(string payment_channel) {
            switch (payment_channel) {
                case "001":
                    return PaymentChannel.CreditCard;
                case "002":
                    return PaymentChannel.Cash;
                case "003":
                    return PaymentChannel.DirectDebit;
                case "005":
                    return PaymentChannel.IPP;
                default: // "004"
                    return PaymentChannel.Others;
            }
            /*
            001 Credit and debit cards
            002 Cash payment channel
            003 Direct debit
            004 Others
            005 IPP transaction
             */
        }


        private void WriteLogFile(string filePath, string content) {
            System.IO.File.AppendAllText(filePath, content + Environment.NewLine, Encoding.UTF8);
        }

        private T MapNameValueToObject<T>(T o, NameValueCollection data) where T : new() {
            T item = new T();

            var properties = TypeDescriptor.GetProperties(typeof(T));

            foreach (PropertyDescriptor property in properties) {
                var name = property.Name;
                var hasKey = data.AllKeys.Where(k => k == name) != null;

                if (hasKey) {
                    property.SetValue(item, data[name]);
                }
            }

            return item;
        }

        #endregion

        #region API

        public void SendTBrokerAPI(int orderId) {
            ActionResultStatus status = new ActionResultStatus();
            var result = new TBrokerAPIService().SubmitPolicy(new List<int>() { orderId });

            OrderRepository repo = new OrderRepository();

            foreach (var item in result) {
                if (item.Value.ErrorMessage == null) item.Value.ErrorMessage = "";
                repo.UpdateOrderStatus_BackendAPI(item.Key, (int)(item.Value.Success ? BackendAPIStatus.Success : BackendAPIStatus.Fail), item.Value.ErrorMessage);
                if (item.Value.Success) status.Success = true;

                Order order = new Order();
                order.Id = orderId;
                order.BackendAPIStatus = (status.Success ? BackendAPIStatus.Success : BackendAPIStatus.Fail);
                order.UpdateDate = DateTime.Now;
                if (!string.IsNullOrEmpty(item.Value.Data)) order.CANo = item.Value.Data;
                repo.UpdateOrderStatus_BackendAPI(order, NullUtils.cvString(status.ErrorMessage));
            }
        }

        public bool SendTBrokerAPIMotorCheck(string serialNo, string engineNo, string plateNo1, string plateNo2, int plateProvince, DateTime activeDate, bool isCompulsory) {
            MasterOptionRepository masterRepo = new MasterOptionRepository();
            var master = masterRepo.GetMasterOption_Active(plateProvince);
            if (master == null) return false;
            var result = new TBrokerAPIService().Motor_CheckDuplicatePolicy(serialNo, engineNo, plateNo1, plateNo2, master.Code, activeDate, isCompulsory);
            return result.Success;
        }

        public bool SendTBrokerAPIMotorReserve(string serialNo, string engineNo, string plateNo1, string plateNo2, int plateProvince, DateTime expireDate) {
            MasterOptionRepository masterRepo = new MasterOptionRepository();
            var master = masterRepo.GetMasterOption_Active(plateProvince);
            if (master == null) return false;
            var result = new TBrokerAPIService().Motor_ReservePolicy(serialNo, engineNo, plateNo1, plateNo2, master.Code, expireDate);
            return result.Success;
        }
       
        public ActionResult SendPartnerAPI(int orderId) {
            ActionResultStatus status = new ActionResultStatus();
            try {
                OrderRepository orderRepo = new OrderRepository();
                var order = orderRepo.GetOrder(orderId);
                ProductApplicationRepository appRepo = new ProductApplicationRepository();
                var app = appRepo.GetProductApplication(order.ProductApplicationId);
                ProductRepository productRepo = new ProductRepository();
                var product = productRepo.GetProduct_Active(Language.TH, app.ProductId, app.PremiumId);
                CompanyRepository companyRepo = new CompanyRepository();
                var company = companyRepo.GetCompany_Active(product.CompanyId);

                if (company.APIChannel == CompanyAPIChannel.Email) {
                    //var result = Service.PartnerAPIService.GenerateExcel(new List<int>() { orderId });
                    //status = result.Item1;

                    //var excel = result.Item2.FirstOrDefault();
                    //if (excel.Value != null) {
                    //    var email = company.MailTo;

                    //    string body = "New Product Application";

                    //    System.IO.Stream stream = new System.IO.MemoryStream(excel.Value);
                    //    System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(stream, "application_" + excel.Key.ToString() + ".xlsx");
                    //    status = MailService.SendMailAttachment(System.Configuration.ConfigurationManager.AppSettings["Mail_From_Email"], System.Configuration.ConfigurationManager.AppSettings["Mail_From_Name"], email, "Product Application", body, "", "", new List<System.Net.Mail.Attachment>() { attachment });

                    //    if (status.Success) {
                    //        //TODO: Update Order PartnerAPIStatus
                    //    }
                    //}
                } else if (company.APIChannel == CompanyAPIChannel.WebService) {
                    var obj = new PartnerAPIService().GenerateObjectJson(orderId);
                    if (obj.Item1.Success) {
                        var ci = new System.Globalization.CultureInfo("en-GB");
                        string logFile = System.Configuration.ConfigurationManager.AppSettings["WebService_Log_Path"] + DateTime.Now.ToString("yyyyMMdd", ci) + "_PartnerAPI.log";
                        StringBuilder sb = new StringBuilder();
                        sb.AppendLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss", ci));
                        sb.AppendLine("[REQUEST]");
                        sb.AppendLine(obj.Item2);
                        sb.AppendLine("[RESPONSE]");
                        string response = string.Empty;
                        try {
                            response = HttpUtils.HttpPostJson(company.APIUrl, obj.Item2);
                            PartnerAPIResponse resp = new JavaScriptSerializer().Deserialize<PartnerAPIResponse>(response);
                            sb.AppendLine(response);
                            if (resp.IsSuccess) {
                                status.Success = true;
                                status.ErrorMessage = "[SUCCESS]";
                                if (!string.IsNullOrEmpty(resp.EPolicyNo) || !string.IsNullOrEmpty(resp.EPolicyNoAdd)) {
                                    order.PolicyNo = resp.EPolicyNo;
                                    order.PolicyNoAdd = resp.EPolicyNoAdd;
                                    order.PolicyReceiveDate = DateTime.Now;
                                    order.UpdateDate = DateTime.Now;
                                    orderRepo.UpdateOrder_PolicyNo(new List<Order>() { order });
                                    if (!string.IsNullOrEmpty(order.PolicyNoAdd)) {
                                        orderRepo.UpdateOrder_PolicyNoAdd(new List<Order>() { order });
                                    }
                                }
                            } else {
                                status.ErrorMessage = "[ERROR] " + resp.ResponseMessage;
                            }
                        } catch (Exception ex) {
                            sb.AppendLine(" ---- [ERROR] " + ex.ToString());
                            status.ErrorMessage = "[ERROR][SELF] " + ex.ToString();
                        }
                        FileUtils.SaveTextAppend(sb.ToString(), logFile);

                        order.PartnerAPIStatus = (status.Success ? PartnerAPIStatus.Success : PartnerAPIStatus.Fail);
                        order.UpdateDate = DateTime.Now;
                        orderRepo.UpdateOrderStatus_PartnerAPI(order, status.ErrorMessage);
                    }
                }

            } catch {
                status.Success = false;
            }

            return Json(status, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SendMail(int orderId) {
            ActionResultStatus status = new ActionResultStatus();
            try {
                OrderRepository orderRepo = new OrderRepository();
                var order = orderRepo.GetOrder(orderId);
                if (order != null) {
                    ProductApplicationRepository appRepo = new ProductApplicationRepository();
                    var app = appRepo.GetProductApplication(order.ProductApplicationId);
                    ProductRepository productRepo = new ProductRepository();
                    var product = productRepo.GetProduct_Active(app.Language, app.ProductId, app.PremiumId);
                    if (app != null) {
                        var email = NullUtils.cvString(app.BuyerEmail);
                        var name = NullUtils.cvString(app.BuyerName);
                        var surname = NullUtils.cvString(app.BuyerSurname);
                        var telephone = NullUtils.cvString(app.BuyerTelephone);
                        var mobilephone = NullUtils.cvString(app.BuyerMobilePhone);
                        var category = ProductCategory.GetCategoryBySubCategory(app.ProductSubCategoryKey);
                        decimal amount = NullUtils.cvDec(app.DisplayPremium);
                        decimal discount = NullUtils.cvDec(app.TotalDiscount);
                        decimal total = amount - discount;
                        var promotions = GetPromotions(app, category);

                        if (category == ProductCategoryKey.Motor || category == ProductCategoryKey.PRB) {
                            if (app.ProductApplicationPolicyHolders != null && app.ProductApplicationPolicyHolders.Count > 0) {
                                email = NullUtils.cvString(app.ProductApplicationPolicyHolders[0].Email);
                                name = NullUtils.cvString(app.ProductApplicationPolicyHolders[0].Name);
                                surname = NullUtils.cvString(app.ProductApplicationPolicyHolders[0].Surname);
                                telephone = NullUtils.cvString(app.ProductApplicationPolicyHolders[0].Telephone);
                                mobilephone = NullUtils.cvString(app.ProductApplicationPolicyHolders[0].MobilePhone);
                            }
                        }

                        if (!string.IsNullOrEmpty(email)) {
                            string template = Server.MapPath("~/assets/mail/" + app.Language.ToString() + "/order.html");
                            string body = FileUtils.ReadText(template);
                            //string template_blank = Server.MapPath("~/assets/mail/template.html");
                            //string body_blank = FileUtils.ReadText(template_blank);

                            ConfigRepository configRepo = new ConfigRepository();
                            var configs = configRepo.ListConfigs(new List<ConfigName>() { ConfigName.EmailImageHeader, ConfigName.EmailImageFooter, ConfigName.ContactAddressTH, ConfigName.ContactAddressEN });
                            CountryRepository countryRepo = new CountryRepository();
                            ProductCategoryRepository categoryRepository = new ProductCategoryRepository();

                            System.Globalization.CultureInfo ciEmail = cultureInfo;
                            if (app.Language == Language.EN) ciEmail = cultureInfoEN;

                            StringBuilder sb = new StringBuilder();
                            string tobjod_address = (app.Language == Language.TH ? configs[ConfigName.ContactAddressTH] : configs[ConfigName.ContactAddressEN]);
                            string ph_name = name + " " + surname;
                            var ph = app.ProductApplicationPolicyHolders[0];
                            var nation = countryRepo.GetCountry_Admin(ph.NationalId);
                            string ph_nationality = (nation != null ? (app.Language == Language.TH ? nation.TitleTH : nation.TitleEN) : "");
                            string ph_id_card = ph.IDCard;
                            string ph_birthday = ph.Birthday.ToString("dd MMMM yyyy", ciEmail);
                            string ph_age = DateUtils.CalculateAge(ph.Birthday, (app.Language == Language.TH ? " �� " : " years "), (app.Language == Language.TH ? " ��͹" : " months"));
                            string ph_tel = ph.Telephone;
                            string ph_mobile = ph.MobilePhone;
                            string ph_email = ph.Email;
                            string ph_detail = "";
                            var subcategory = categoryRepository.GetProductSubCategory((int)product.SubCategoryId);
                            string product_category = (app.Language == Language.TH ? subcategory.TitleTH : subcategory.TitleEN);
                            string product_name = (app.Language == Language.TH ? product.TitleTH : product.TitleEN);
                            string product_company = product.Company;
                            string product_type = (app.EPolicy == 1 ? "EPolicy" : (app.Language == Language.TH ? "��ɳ���" : "Post"));
                            string order_no = order.OrderCode;
                            string payment_no = order.PaidRef;
                            string payment_type = order.GetPaymentTitle(app.Language);
                            string total_premium = order.PaymentTotal.ToString("n2");
                            string active_date = app.ActiveDate.ToString("dd MMMM yyyy", ciEmail);
                            string expire_date = app.ExpireDate.ToString("dd MMMM yyyy", ciEmail);
                            string payment_date = order.PaymentDate.ToString("dd MMMM yyyy HH:mm:ss", ciEmail);


                            string subject = ShowLang(app, "��ػ����觫��� ", "Order Summary ") + order_no;

                            #region old
                            /*
                            if (app.Language == Language.TH) {
                                sb.AppendLine(string.Format(row, "����-���ʡ��", name + " " + surname));
                                sb.AppendLine(string.Format(row, "�����", email));
                                sb.AppendLine(string.Format(row, "�������Ѿ��", telephone));
                                sb.AppendLine(string.Format(row, "�ôѡ��", NullUtils.cvString(app.ProductName)));
                                if (promotions != null) {
                                    var pGift = promotions.Where(o => o.Id == app.PromotionId && o.PromotionType == PromotionType.Gift).FirstOrDefault();
                                    var pDiscount = promotions.Where(o => o.Id == app.PromotionId && o.PromotionType == PromotionType.Discount).FirstOrDefault();
                                    if (pGift != null) {
                                        sb.AppendLine(string.Format(row, "�������", ""));
                                        sb.AppendLine(string.Format(row, pGift.Gift, "���"));
                                        var couponCode = app.CouponCode;
                                        if (!string.IsNullOrEmpty(couponCode)) {
                                            sb.AppendLine(string.Format(row, "��ǹŴ�ٻͧ", discount.ToString("n2") + " " + "�ҷ"));
                                        }
                                    }
                                    if (pDiscount != null) {
                                        sb.AppendLine(string.Format(row, "�������", ""));
                                        sb.AppendLine(string.Format(row, "��ǹŴ", pDiscount.DiscountType == PromotionDiscountType.Amount ? pDiscount.DiscountAmount + " %" : pDiscount.DiscountAmount.ToString("n2") + " �ҷ"));
                                    }
                                }

                                sb.AppendLine(string.Format(row, "�ӹǹ�Թ", total.ToString("n2") + " " + "�ҷ"));
                                sb.AppendLine(string.Format("<tr><th colspan='2'>{0}</th></tr>", "�س��ӡ�ê����Թ���º�������� �ͺ�س���"));
                                subject = "��ػ�����觫���";
                            } else {
                                address = configs[ConfigName.ContactAddressEN];
                                sb.AppendLine(string.Format(row, "Name", name + " " + surname));
                                sb.AppendLine(string.Format(row, "Email", email));
                                sb.AppendLine(string.Format(row, "Telephone", telephone));
                                sb.AppendLine(string.Format(row, "Product", NullUtils.cvString(app.ProductName)));
                                if (promotions != null) {
                                    var pGift = promotions.Where(o => o.Id == app.PromotionId && o.PromotionType == PromotionType.Gift).FirstOrDefault();
                                    var pDiscount = promotions.Where(o => o.Id == app.PromotionId && o.PromotionType == PromotionType.Discount).FirstOrDefault();
                                    if (pGift != null) {
                                        sb.AppendLine(string.Format(row, "Promotion", ""));
                                        sb.AppendLine(string.Format(row, pGift.Gift, "Free"));
                                        var couponCode = app.CouponCode;
                                        if (!string.IsNullOrEmpty(couponCode)) {
                                            sb.AppendLine(string.Format(row, "Coupon Discount", discount.ToString("n2") + " Baht"));
                                        }
                                    }
                                    if (pDiscount != null) {
                                        sb.AppendLine(string.Format(row, "Promotion", ""));
                                        sb.AppendLine(string.Format(row, "Discount", pDiscount.DiscountType == PromotionDiscountType.Amount ? pDiscount.DiscountAmount + " %" : pDiscount.DiscountAmount.ToString("n2") + " Baht"));
                                    }

                                }
                                sb.AppendLine(string.Format(row, "Total Amount", total.ToString("n2") + " " + "Baht"));
                                sb.AppendLine(string.Format("<tr><th colspan='2'>{0}</th></tr>", "Your payment has been successfully done. Thank you."));
                                subject = "Order Summary";
                            }
                            */
                            #endregion

                            string row = "<tr><th>{0}</th><td>{1}</td></tr>";
                            string rowAlt = "<tr style=\"background-color: #fff1e9;\"><th>{0}</th><td>{1}</td></tr>";
                            int i = 0;

                            product.CategoryId = (ProductCategoryKey)subcategory.CategoryId;
                            var columns = new Dictionary<string, String>();
                            var coverages = new Dictionary<string, String>();
                            var appDetail = new Dictionary<string, String>();
                            if (product.CategoryId == ProductCategoryKey.Motor) {
                                var premium = product.ProductMotorPremium[0];
                                if (app.Language == Language.TH) {
                                    columns.Add("����������µ��ö¹��", premium.OwnDamage.ToString("##,###"));
                                    columns.Add("�������������ǹ�á", premium.OwnExcess.ToString("##,###"));
                                    columns.Add("��ӷ���", premium.Flood.ToString("##,###"));
                                } else {
                                    columns.Add("Own Vehicle Damage", premium.OwnDamage.ToString("##,###"));
                                    columns.Add("Own Vehicle Excess", premium.OwnExcess.ToString("##,###"));
                                    columns.Add("Flood", premium.Flood.ToString("##,###"));
                                }
                            } else if (product.CategoryId == ProductCategoryKey.Home) {
                                var premium = product.ProductHomePremium[0];
                                if (app.Language == Language.TH) {
                                    columns.Add("�����", premium.FireTH);
                                    columns.Add("����������¨ҡ�غѵ��˵�", premium.AccidentalDamageTH);
                                    columns.Add("�������¸����ҵ� : ��������� / ����蹴Թ��� / ����١��� / ��¹�ӷ���", premium.NaturalRiskTH);
                                    columns.Add("�����ҷ��ѡ����ª��Ǥ���", premium.TempRentalTH);
                                    columns.Add("����á���", premium.BurglaryTH);
                                    columns.Add("�����Ѻ��ͺؤ����¹͡", premium.PublicLiabilityTH);
                                } else {
                                    columns.Add("Fire", premium.FireEN);
                                    columns.Add("Accidental Damage", premium.AccidentalDamageEN);
                                    columns.Add("Natural Risk: Windstorm / Earthquake / Hail / Flood", premium.NaturalRiskEN);
                                    columns.Add("Temporary residence rental expenses", premium.TempRentalEN);
                                    columns.Add("Burglary", premium.BurglaryEN);
                                    columns.Add("Public Liability ", premium.PublicLiabilityEN);
                                }
                            } else if (product.CategoryId == ProductCategoryKey.PA) {
                                var premium = product.ProductPAPremium[0];
                                if (app.Language == Language.TH) {
                                    columns.Add("�٭���ª��Ե, �٭���������� �ؾ���Ҿ��������ԧ", premium.LossLifeTH);
                                    columns.Add("����ѡ�Ҿ�Һ��", premium.MedicalExpenseTH);
                                    columns.Add("���������", premium.IncomeCompensationTH);
                                    columns.Add("��ö١�ҵá������Ͷ١��������ҧ���", premium.MurderAssaultTH);
                                    columns.Add("��âѺ������������ö�ѡ��ҹ¹��", premium.RidingPassengerMotorcycleTH);
                                    columns.Add("��һçȾ", premium.FuneralExpenseAccidentIllnessTH);
                                } else {
                                    columns.Add("Loss of life, Dismemberment or Total Permanent Disability", premium.LossLifeEN);
                                    columns.Add("Medical expense", premium.MedicalExpenseEN);
                                    columns.Add("Income compensation", premium.IncomeCompensationEN);
                                    columns.Add("Murder or assault", premium.MurderAssaultEN);
                                    columns.Add("Riding/being a passenger on a motorcycle", premium.RidingPassengerMotorcycleEN);
                                    columns.Add("Funeral expense due to accident and illness", premium.FuneralExpenseAccidentIllnessEN);
                                }
                            } else if (product.CategoryId == ProductCategoryKey.PH) {
                                var premium = product.ProductPHPremium[0];
                                if (app.Language == Language.TH) {
                                    columns.Add("����ѡ�Ҿ�Һ�ż������", premium.IPDTH);
                                    columns.Add("����ѡ�Ҿ�Һ�ż����¹͡ (OPD)", premium.OutpatientBenefitsTH);
                                    columns.Add("�����ͧ��Ф�������", premium.IPDRoomBoardTH);
                                    columns.Add("���������ѹ�Դ�ҡ��ü�ҵѴ ��һ�֡��ᾷ������ǡѺ��ü�ҵѴ", premium.SurgicalExpenseTH);
                                    columns.Add("����ѡ���ä�����", premium.CancerTreatmentTH);
                                    columns.Add("����¹����䢡�д١/������", premium.OrganTransplantTH);
                                } else {
                                    columns.Add("IPD", premium.IPDEN);
                                    columns.Add("Out Patient benefits", premium.OutpatientBenefitsEN);
                                    columns.Add("IPD Room and board", premium.IPDRoomBoardEN);
                                    columns.Add("Surgical expenses", premium.SurgicalExpenseEN);
                                    columns.Add("Cancer Treatment", premium.CancerTreatmentEN);
                                    columns.Add("Expanses for organ transplant, Bone marrow transplant, Hemodyalysis", premium.OrganTransplantEN);
                                }
                            } else if (product.CategoryId == ProductCategoryKey.TA) {
                                var premium = product.ProductTAPremium[0];
                                if (app.Language == Language.TH) {
                                    columns.Add("���ª��Ե,�٭����������,�ؾ���Ҿ����", premium.PersonalLossLifeTH);
                                    columns.Add("����ѡ�Ҿ�Һ�Ũҡ�غѵ��˵�", premium.MedicalExpenseEachAccidentTH);
                                    columns.Add("����ѡ�Ҿ�Һ��㹵�ҧ����� ", premium.MedicalExpenseAccidentSicknessTH);
                                    columns.Add("�������͹���©ء�Թ", premium.EmergencyMedialEvacuationTH);
                                    columns.Add("�������͹���¡�Ѻ���������", premium.RepatriationExpenseTH);
                                    columns.Add("��������㹡����Ⱦ��Ѻ�����", premium.RepatriationMortalRemainsTH);
                                    columns.Add("�������Թ�ҧ�٭��������������", premium.LossDamageBaggageTH);
                                } else {
                                    columns.Add("Loss of Life, Dismembermen or Total Permanent Disability", premium.PersonalLossLifeEN);
                                    columns.Add("Medical Expenses Each Accident", premium.MedicalExpenseEachAccidentEN);
                                    columns.Add("Medical Expense due to Accident and Sickness", premium.MedicalExpenseAccidentSicknessEN);
                                    columns.Add("Emergency Medical Evacuation and Repatriation Expenses", premium.EmergencyMedialEvacuationEN);
                                    columns.Add("Repatriation Expenses", premium.RepatriationExpenseEN);
                                    columns.Add("Repatriation of Mortal Remains", premium.RepatriationMortalRemainsEN);
                                    columns.Add("Loss or Damage of Baggage", premium.LossDamageBaggageEN);
                                }
                            } else if (product.CategoryId == ProductCategoryKey.TA) {
                                var premium = product.ProductTAPremium[0];
                                if (app.Language == Language.TH) {
                                    columns.Add("���ª��Ե,�٭����������,�ؾ���Ҿ����", premium.PersonalLossLifeTH);
                                    columns.Add("����ѡ�Ҿ�Һ�Ũҡ�غѵ��˵�", premium.MedicalExpenseEachAccidentTH);
                                    columns.Add("����ѡ�Ҿ�Һ��㹵�ҧ����� ", premium.MedicalExpenseAccidentSicknessTH);
                                    columns.Add("�������͹���©ء�Թ", premium.EmergencyMedialEvacuationTH);
                                    columns.Add("�������͹���¡�Ѻ���������", premium.RepatriationExpenseTH);
                                    columns.Add("��������㹡����Ⱦ��Ѻ�����", premium.RepatriationMortalRemainsTH);
                                    columns.Add("�������Թ�ҧ�٭��������������", premium.LossDamageBaggageTH);
                                } else {
                                    columns.Add("Loss of Life, Dismembermen or Total Permanent Disability", premium.PersonalLossLifeEN);
                                    columns.Add("Medical Expenses Each Accident", premium.MedicalExpenseEachAccidentEN);
                                    columns.Add("Medical Expense due to Accident and Sickness", premium.MedicalExpenseAccidentSicknessEN);
                                    columns.Add("Emergency Medical Evacuation and Repatriation Expenses", premium.EmergencyMedialEvacuationEN);
                                    columns.Add("Repatriation Expenses", premium.RepatriationExpenseEN);
                                    columns.Add("Repatriation of Mortal Remains", premium.RepatriationMortalRemainsEN);
                                    columns.Add("Loss or Damage of Baggage", premium.LossDamageBaggageEN);
                                }
                            } else if (product.CategoryId == ProductCategoryKey.PRB) {
                                var cov = ProductCompulsoryViewModel.GetCoverage(app.Language, app.ProductApplicationMotor.CarUsageTypeCode);
                                foreach (var c in cov) {
                                    columns.Add(c.Key, c.Value);
                                }
                            }
                            string covg = ShowLang(app, product.CoverageTH, product.CoverageEN).Replace("<tr><td></td><td></td></tr>", "");
                            coverages.Add(ShowLang(app, "����������ͧ��ѡ", "Coverage"), (string.IsNullOrWhiteSpace(covg) ? "-" : "<table>" + covg + "</table>"));
                            coverages.Add(ShowLang(app, "���͹䢡���Ѻ��Сѹ���", "Condition"), ValueUtils.EmptyStringDefault(ShowLang(app, product.ConditionTH, product.ConditionEN)));
                            coverages.Add(ShowLang(app, "���¡���", "Exception"), ValueUtils.EmptyStringDefault(ShowLang(app, product.ExceptionTH, product.ExceptionEN)));
                            coverages.Add(ShowLang(app, "�Է�Ծ����", "Privilege"), ValueUtils.EmptyStringDefault(ShowLang(app, product.PrivilegeTH, product.PrivilegeEN)));
                            coverages.Add(ShowLang(app, "���Ѻ�ͧ�����ŧ", "Consent"), ValueUtils.EmptyStringDefault(ShowLang(app, product.ConsentTH, product.ConsentEN)));

                            if (app.ProductApplicationMotor != null && app.ProductApplicationMotor.ApplicationId > 0) {
                                var masterRepo = new TobJod.Repositories.MasterOptionRepository();
                                var usage = "";
                                var carProvince = "";
                                var carBrand = "";
                                ProductMotorVehicleUsage vehicleUsage;
                                CarRepository carRepo = new CarRepository();
                                if (Enum.TryParse<ProductMotorVehicleUsage>(app.ProductApplicationMotor.CarUsageType.ToString(), out vehicleUsage)) {
                                    usage = ValueUtils.GetEnumDisplayName(vehicleUsage);
                                    if (usage == "0") { usage = ""; }
                                }
                                var cp = masterRepo.GetMasterOption_Active(app.ProductApplicationMotor.CarProvinceId);
                                if (app.Language == Language.TH) {
                                    if (cp != null) { carProvince = cp.TitleEN; }
                                    carBrand = carRepo.GetCarBrand_Active(Language.EN, NullUtils.cvInt(app.ProductApplicationMotor.CarBrand)).TitleEN;
                                } else {
                                    if (cp != null) { carProvince = cp.TitleTH; }
                                    carBrand = carRepo.GetCarBrand_Active(Language.TH, NullUtils.cvInt(app.ProductApplicationMotor.CarBrand)).TitleTH;
                                    switch (vehicleUsage) {
                                        case TobJod.Models.ProductMotorVehicleUsage.Normal_Commercial: usage = "Car"; break;
                                        case TobJod.Models.ProductMotorVehicleUsage.Normal_Personal: usage = "Car"; break;
                                        case TobJod.Models.ProductMotorVehicleUsage.Passenger_Personal: usage = "Van"; break;
                                        case TobJod.Models.ProductMotorVehicleUsage.Pickup_Commercial: usage = "Pick-up Truck"; break;
                                    }
                                }

                                if (!string.IsNullOrEmpty(usage)) { appDetail.Add(ShowLang(app, "������ö¹��", "Car Type"), usage); }
                                appDetail.Add(ShowLang(app, "���ö¹��", "Car Model"), carBrand + " " + app.ProductApplicationMotor.CarModel);
                                appDetail.Add(ShowLang(app, "�����Ţ����ͧ", "Car Engine Number"), app.ProductApplicationMotor.CarEngineNo);
                                appDetail.Add(ShowLang(app, "�ը�����¹", "Register Year"), app.ProductApplicationMotor.CarRegisterYear.ToString());
                                appDetail.Add(ShowLang(app, "�ѧ��Ѵ������¹", "Register Province"), carProvince);
                                appDetail.Add(ShowLang(app, "�����Ţ����¹ö¹��", "Plate Number"), app.ProductApplicationMotor.CarPlateNo1 + " " + app.ProductApplicationMotor.CarPlateNo2);

                                StringBuilder sbDetail = new StringBuilder();
                                foreach (var col in appDetail) {
                                    sbDetail.AppendLine(string.Format((i % 2 == 0 ? row : rowAlt), col.Key, col.Value.Replace("\n", "<br />")));
                                    i++;
                                }
                                ph_detail = sbDetail.ToString();
                            }


                            i = 0;
                            foreach (var col in columns) {
                                sb.AppendLine(string.Format((i%2 == 0 ? row : rowAlt), col.Key, col.Value.Replace("\n", "<br />")));
                                i++;
                            }

                            foreach (var col in coverages) {
                                string bg = (i % 2 == 0 ? "" : " style=\"background-color: #fff1e9;\"");
                                sb.AppendLine("<tr" + bg + "><th colspan=\"2\">" + col.Key + "</th></tr><tr" + bg + "><td colspan=\"2\" style=\"padding-left: 15px;\">" + col.Value.Replace("\n", "<br />") + "</td></tr>");
                                i++;
                            }

                            Document document = new Document();
                            document.Info.Title = "Order Summary";
                            document.DefaultPageSetup.TopMargin = Unit.FromCentimeter(1);

                            Style style = document.Styles["Normal"];
                            style.Font.Name = "Tahoma";
                            style.Font.Size = 9;

                            style = document.Styles.AddStyle("Table", "Normal");
                            style.Font.Name = "Tahoma";
                            style.Font.Size = 8;

                            Section sectionBody = document.AddSection();

                            MigraDoc.DocumentObjectModel.Shapes.Image image = sectionBody.AddImage(Server.MapPath("~/assets/mail/images/logo_tobjod.png"));
                            image.Height = "3.5cm";
                            image.LockAspectRatio = true;
                            image.RelativeVertical = MigraDoc.DocumentObjectModel.Shapes.RelativeVertical.Line;
                            image.RelativeHorizontal = MigraDoc.DocumentObjectModel.Shapes.RelativeHorizontal.Margin;
                            image.Top = MigraDoc.DocumentObjectModel.Shapes.ShapePosition.Top;
                            image.Left = MigraDoc.DocumentObjectModel.Shapes.ShapePosition.Left;
                            image.WrapFormat.Style = MigraDoc.DocumentObjectModel.Shapes.WrapStyle.Through;

                            image = sectionBody.AddImage(Server.MapPath("~/assets/mail/images/logo_tbroker.gif"));
                            image.Height = "1.6cm";
                            image.LockAspectRatio = true;
                            image.RelativeVertical = MigraDoc.DocumentObjectModel.Shapes.RelativeVertical.Line;
                            image.RelativeHorizontal = MigraDoc.DocumentObjectModel.Shapes.RelativeHorizontal.Margin;
                            image.Top = MigraDoc.DocumentObjectModel.Shapes.ShapePosition.Top;
                            image.Left = "9cm";
                            image.WrapFormat.Style = MigraDoc.DocumentObjectModel.Shapes.WrapStyle.Through;

                            MigraDoc.DocumentObjectModel.Shapes.TextFrame textFrame = sectionBody.AddTextFrame();
                            textFrame.Top = "1.6cm";
                            textFrame.Left = "9.5cm";
                            textFrame.Width = "6.5cm";
                            Paragraph paragraph = textFrame.AddParagraph(ShowLang(app, "����ѷ ���ҵ�á���� �ӡѴ", "Thanachart Broker Company Limited"));
                            paragraph.Format.Font.Bold = true;
                            paragraph = textFrame.AddParagraph(tobjod_address);
                            
                            MigraDoc.DocumentObjectModel.Tables.Table table = new MigraDoc.DocumentObjectModel.Tables.Table();
                            MigraDoc.DocumentObjectModel.Tables.Column columnTH = table.AddColumn(Unit.FromCentimeter(9));
                            MigraDoc.DocumentObjectModel.Tables.Column column = table.AddColumn(Unit.FromCentimeter(9));
                            columnTH.Format.Font.Bold = true;

                            MigraDoc.DocumentObjectModel.Tables.Row tr;
                            MigraDoc.DocumentObjectModel.Tables.Cell cell;


                            var tableColumns = new Dictionary<string, string>();
                            tableColumns.Add(ShowLang(app, "���ͼ������", "Name"), ph_name);
                            tableColumns.Add(ShowLang(app, "�ѭ�ҵ�", "Nationality"), ph_nationality);
                            tableColumns.Add(ShowLang(app, "�Ţ���ѵû�Шӵ�ǻ�ЪҪ� / ��ʻ���", "ID card number / Passport number"), ph_id_card);
                            tableColumns.Add(ShowLang(app, "�ѹ�Դ", "Birthday"), ph_birthday);
                            tableColumns.Add(ShowLang(app, "����", "Age"), ph_age);
                            tableColumns.Add(ShowLang(app, "���Ѿ��", "Telephone number"), ph_tel);
                            tableColumns.Add(ShowLang(app, "���Ѿ����Ͷ��", "Mobile phone number"), ph_mobile);
                            tableColumns.Add(ShowLang(app, "�����", "Email"), ph_email);
                            foreach (var it in appDetail) {
                                tableColumns.Add(it.Key, it.Value);
                            }

                            var tableOrders = new Dictionary<string, string>();
                            tableOrders.Add(ShowLang(app, "��������Сѹ", "Product Category"), product_category);
                            tableOrders.Add(ShowLang(app, "����Ἱ��Сѹ", "Product Name"), product_name);
                            tableOrders.Add(ShowLang(app, "����ѷ��Сѹ", "Company"), product_company);
                            tableOrders.Add(ShowLang(app, "�ٻẺ��������", "Type"), product_type);

                            tableOrders.Add(ShowLang(app, "�Ţ�����ҧ�ԧ�����觫���", "Order number"), order_no);
                            tableOrders.Add(ShowLang(app, "�Ţ�����ҧ�ԧ��ê����Թ", "Payment reference number"), payment_no);
                            tableOrders.Add(ShowLang(app, "ʶҹС�ê����Թ", "Payment status"), ShowLang(app, "�����", "Success"));
                            tableOrders.Add(ShowLang(app, "��������ê����Թ", "Payment method"), payment_type);
                            tableOrders.Add(ShowLang(app, "������»�Сѹ", "Payment amount"), total_premium);
                            tableOrders.Add(ShowLang(app, "�ѹ���������鹤�����ͧ", "Policy Active Date"), active_date);
                            tableOrders.Add(ShowLang(app, "�ѹ�������ش������ͧ", "Policy Expire Date"), expire_date);
                            tableOrders.Add(ShowLang(app, "�ѹ�������Թ", "Payment Date"), payment_date);

                            tr = table.AddRow();
                            cell = tr.Cells[0];
                            paragraph = cell.AddParagraph(ShowLang(app, "�����ż����һ�Сѹ", "Policy Holder Information"));
                            paragraph.Format.Font.Size = 11;
                            paragraph.Format.Font.Underline = Underline.Single;
                            paragraph.Format.Font.Bold = true;
                            paragraph.Format.SpaceAfter = 7;

                            foreach (var item in tableColumns) {
                                tr = table.AddRow();
                                cell = tr.Cells[0];
                                cell.AddParagraph(item.Key);
                                cell = tr.Cells[1];
                                cell.AddParagraph(NullUtils.cvString(item.Value));
                            }

                            tr = table.AddRow();
                            cell = tr.Cells[0];
                            paragraph = cell.AddParagraph(ShowLang(app, "��������¡����觫���", "Order Information"));
                            paragraph.Format.Font.Size = 11;
                            paragraph.Format.Font.Underline = Underline.Single;
                            paragraph.Format.Font.Bold = true;
                            paragraph.Format.SpaceAfter = 7;
                            paragraph.Format.SpaceBefore = 15;

                            foreach (var item in tableOrders) {
                                tr = table.AddRow();
                                cell = tr.Cells[0];
                                cell.AddParagraph(item.Key);
                                cell = tr.Cells[1];
                                cell.AddParagraph(NullUtils.cvString(item.Value));
                            }

                            tr = table.AddRow();
                            cell = tr.Cells[0];
                            paragraph = cell.AddParagraph(ShowLang(app, "�����ż�Ե�ѳ��", "Product Information"));
                            paragraph.Format.Font.Size = 11;
                            paragraph.Format.Font.Underline = Underline.Single;
                            paragraph.Format.Font.Bold = true;
                            paragraph.Format.SpaceAfter = 7;
                            paragraph.Format.SpaceBefore = 15;
                            
                            foreach (var item in columns) {
                                tr = table.AddRow();
                                cell = tr.Cells[0];
                                cell.AddParagraph(item.Key);
                                cell = tr.Cells[1];
                                cell.AddParagraph(item.Value);
                            }

                            document.LastSection.Add(table);

                            foreach (var item in coverages) {
                                paragraph = document.LastSection.AddParagraph(item.Key);
                                paragraph.Format.Font.Size = 10;
                                paragraph.Format.Font.Bold = true;
                                paragraph.Format.SpaceAfter = 7;
                                paragraph.Format.SpaceBefore = 7;
                                if (item.Value.StartsWith("<table")) {
                                    var doc = new HtmlAgilityPack.HtmlDocument();
                                    doc.LoadHtml(item.Value);

                                    var rows = new List<string[]>();
                                    var count = 0;
                                    if (doc != null && doc.DocumentNode != null && doc.DocumentNode.SelectNodes("//tr") != null) {

                                        MigraDoc.DocumentObjectModel.Tables.Table tableSub = new MigraDoc.DocumentObjectModel.Tables.Table();
                                        tableSub.AddColumn(Unit.FromCentimeter(9));
                                        tableSub.AddColumn(Unit.FromCentimeter(9));

                                        MigraDoc.DocumentObjectModel.Tables.Row rowSub = null;

                                        foreach (HtmlAgilityPack.HtmlNode vrow in doc.DocumentNode.SelectNodes("//tr")) {
                                            List<String> td = new List<string>();
                                            if (vrow.SelectNodes("td") != null) {
                                                foreach (HtmlAgilityPack.HtmlNode col in vrow.SelectNodes("td")) {
                                                    td.Add(col.InnerText);
                                                }
                                                if (td.Count > 1 && (!string.IsNullOrWhiteSpace(td[0]) || !string.IsNullOrWhiteSpace(td[1]))) {
                                                    count++;
                                                    rowSub = tableSub.AddRow();
                                                    rowSub.Cells[0].AddParagraph(td[0]);
                                                    rowSub.Cells[0].Format.LeftIndent = 10;
                                                    rowSub.Cells[1].AddParagraph(td[1]);
                                                }
                                            }
                                        }
                                        if (rowSub != null) {
                                            document.LastSection.Add(tableSub);
                                        }
                                    }
                                    if (count == 0) {
                                        paragraph = document.LastSection.AddParagraph("-");
                                        paragraph.Format.LeftIndent = 10;
                                        paragraph.Format.Font.Size = 9;
                                    }

                                } else {
                                    paragraph = document.LastSection.AddParagraph(item.Value);
                                    paragraph.Format.LeftIndent = 10;
                                    paragraph.Format.Font.Size = 9;
                                }
                            }


                            MemoryStream streamPdf = new MemoryStream();

                            MigraDoc.Rendering.PdfDocumentRenderer pdfRenderer = new MigraDoc.Rendering.PdfDocumentRenderer(true);
                            pdfRenderer.Document = document;
                            pdfRenderer.RenderDocument();
                            pdfRenderer.PdfDocument.Save(streamPdf, false);
                            //pdfRenderer.PdfDocument.Save("C:\\Temp\\x.pdf"); return null;
                            streamPdf.Position = 0;

                            //TEST NO SIGNATURE
                            //System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(streamPdf, "Order-" + order_no + ".pdf");
                            string filename = "Order-" + order_no + ".pdf";
                            System.Net.Mail.Attachment attachment = null;
                            StringBuilder sbLog = new StringBuilder();
                            try {
                                sbLog.AppendLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss", cultureInfoEN) + "\t[Request] File: " + filename);
                                SignService.SignSignatureClient api = new SignService.SignSignatureClient();
                                byte[] signed = api.sign(System.Configuration.ConfigurationManager.AppSettings["SignOnlineRecordNumber"], filename, streamPdf.ToArray());
                                attachment = new System.Net.Mail.Attachment(new MemoryStream(signed), filename);

                                sbLog.AppendLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss", cultureInfoEN) + "\t[Response] Status: Success");
                            } catch (Exception ex) {
                                //streamPdf.Position = 0;
                                //attachment = new System.Net.Mail.Attachment(streamPdf, filename);
                                sbLog.AppendLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss", cultureInfoEN) + "\t[Response] Status: Error, " + ex.ToString());
                            }
                            FileUtils.SaveTextAppend(sbLog.ToString() + Environment.NewLine, System.Configuration.ConfigurationManager.AppSettings["WebService_Log_Path"] + DateTime.Now.ToString("yyyyMMdd", cultureInfoEN) + "_SignSignature.log");


                            string configPath = HttpUtils.GetPathUrlAbsolute(System.Configuration.ConfigurationManager.AppSettings["Path_Config"]);
                            body = body.Replace("{{base_url}}", HttpUtils.GetBaseUrl())
                                        .Replace("{{email_image_header}}", configPath + configs[ConfigName.EmailImageHeader])
                                        .Replace("{{email_image_footer}}", configPath + configs[ConfigName.EmailImageFooter])
                                        .Replace("{{tobjod_address}}", tobjod_address)
                                        .Replace("{{ph_name}}", ph_name)
                                        .Replace("{{ph_nationality}}", ph_nationality)
                                        .Replace("{{ph_id_card}}", ph_id_card)
                                        .Replace("{{ph_birthday}}", ph_birthday)
                                        .Replace("{{ph_age}}", ph_age)
                                        .Replace("{{ph_tel}}", ph_tel)
                                        .Replace("{{ph_mobile}}", ph_mobile)
                                        .Replace("{{ph_email}}", ph_email)
                                        .Replace("{{ph_detail}}", ph_detail)
                                        .Replace("{{product_category}}", product_category)
                                        .Replace("{{product_name}}", product_name)
                                        .Replace("{{product_company}}", product_company)
                                        .Replace("{{product_type}}", product_type)
                                        .Replace("{{order_no}}", order_no)
                                        .Replace("{{payment_no}}", payment_no)
                                        .Replace("{{payment_type}}", payment_type)
                                        .Replace("{{payment_date}}", payment_date)
                                        .Replace("{{total_premium}}", total_premium)
                                        .Replace("{{active_date}}", active_date)
                                        .Replace("{{expire_date}}", expire_date)
                                        .Replace("{{tobjod_address}}", tobjod_address)
                                        .Replace("{{product_coverage}}", sb.ToString());
                            status = MailService.SendMailAttachment(System.Configuration.ConfigurationManager.AppSettings["Mail_From_Email"], System.Configuration.ConfigurationManager.AppSettings["Mail_From_Name"], email, subject, body, "", "", new List<System.Net.Mail.Attachment>() { attachment });

                            if (status.Success) {
                                //TODO: Update Order PartnerAPIStatus
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                status.Success = false;
                status.ErrorMessage = ex.ToString();
            }

            return Json(status, JsonRequestBehavior.AllowGet);
        }

        private Paragraph AddTextToCell(string instring, MigraDoc.DocumentObjectModel.Document document, MigraDoc.DocumentObjectModel.Tables.Cell cell, Unit fontsize) {
            PdfSharp.Pdf.PdfDocument pdfd = new PdfSharp.Pdf.PdfDocument();
            PdfSharp.Pdf.PdfPage pg = pdfd.AddPage();
            PdfSharp.Drawing.XGraphics oGFX = PdfSharp.Drawing.XGraphics.FromPdfPage(pg);
            Unit maxWidth = cell.Column.Width - (cell.Column.LeftPadding + cell.Column.RightPadding);
            Paragraph par;
            PdfSharp.Drawing.XFont font = new PdfSharp.Drawing.XFont(document.Styles["Table"].Font.Name, fontsize);
            if (oGFX.MeasureString(instring, font).Width < maxWidth.Value) {
                par = cell.AddParagraph(instring);
            } else // String does not fit - start the truncation process...
              {
                int stringlength = instring.Length;
                for (int i = 0; i < 3; i++) {
                    if (oGFX.MeasureString(instring.Substring(0, stringlength) + '\u2026', font).Width > maxWidth.Value)
                        stringlength -= (int)Math.Ceiling(instring.Length * Math.Pow(0.5f, i));
                    else
                        if (i < 2)
                        stringlength += (int)Math.Ceiling(instring.Length * Math.Pow(0.5f, i));
                }
                par = cell.AddParagraph(instring.Substring(0, stringlength) + '\u2026');
            }
            par.Format.Font.Size = fontsize;
            return par;
        }

        private string ShowLang(ProductApplication app, string th, string en) {
            return (app.Language == Language.TH) ? th : en;
        }

        private List<PromotionItem> GetPromotions(ProductApplication app, ProductCategoryKey category) {
            ProductRepository productRepository = new ProductRepository();
            if (category == ProductCategoryKey.Motor) {
                var productItem = productRepository.GetProductMotor_Active(Language.TH, app.ProductId, app.PremiumId);
                if (productItem != null && productItem.Promotion != null && productItem.Promotion.Count > 0) {
                    return productItem.Promotion;
                }
            }
            if (category == ProductCategoryKey.PRB) {
                var productItem = productRepository.GetProductPRB_Active(Language.TH, app.ProductId);
                if (productItem != null && productItem.Promotion != null && productItem.Promotion.Count > 0) {
                    return productItem.Promotion;
                }
            }
            if (category == ProductCategoryKey.TA) {
                var productItem = productRepository.GetProductTA_Active(Language.TH, app.ProductId, app.PremiumId);
                if (productItem != null && productItem.Promotion != null && productItem.Promotion.Count > 0) {
                    return productItem.Promotion;
                }
            }
            if (category == ProductCategoryKey.Home) {
                var productItem = productRepository.GetProductMotor_Active(Language.TH, app.ProductId, app.PremiumId);
                if (productItem != null && productItem.Promotion != null && productItem.Promotion.Count > 0) {
                    return productItem.Promotion;
                }
            }

            if (category == ProductCategoryKey.PA) {
                var productItem = productRepository.GetProductMotor_Active(Language.TH, app.ProductId, app.PremiumId);
                if (productItem != null && productItem.Promotion != null && productItem.Promotion.Count > 0) {
                    return productItem.Promotion;
                }
            }

            if (category == ProductCategoryKey.PH) {
                var productItem = productRepository.GetProductMotor_Active(Language.TH, app.ProductId, app.PremiumId);
                if (productItem != null && productItem.Promotion != null && productItem.Promotion.Count > 0) {
                    return productItem.Promotion;
                }
            }

            return null;
        }

        #endregion

    }
}

