﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TobJod.Web.Controllers {
    public class TermController : BaseController {
        // GET: Term
        public ActionResult Index() {
            return View(Language.ToString() + "/Index");
        }
    }
}