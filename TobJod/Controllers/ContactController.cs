﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;
using TobJod.Web.Models;

namespace TobJod.Web.Controllers {
    public class ContactController : BaseController {

        // GET: Contact
        public ActionResult Index() {
            ContactViewModel model = BindForm(new ContactViewModel());
            DayOffRepository dayOffRepo = new DayOffRepository();
            model.DayOff = dayOffRepo.ListDayOff_Active();
            model.EnableCaptcha = base.EnableChaptcha;
            //var d = DateTime.Today;
            //while (model.DayOff.Contains(d) || d.DayOfWeek == DayOfWeek.Saturday || d.DayOfWeek == DayOfWeek.Sunday) {
            //    d = d.AddDays(1);
            //}
            //model.CallBackDay = d.ToString("dd/MM/yyyy", cultureInfoEN);
            //model.CallBackTime = DateTime.Now.ToString("HH:mm");
            return View(Language.ToString() + "/Index", model);
        }

        // POST: Contact/Preview
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Preview(String body) {
            if (NullUtils.cvBoolean(System.Configuration.ConfigurationManager.AppSettings["Backend_Preview"])) {
                ConfigPageRepository repo = new ConfigPageRepository();
                ConfigPage item = repo.GetConfigPage_Admin(ConfigPageName.ContactUs);
                item.BodyTH = ReplaceTextEditorPathDisplay(body);
                item.BodyEN = ReplaceTextEditorPathDisplay(body);


                ContactViewModel model = BindForm(new ContactViewModel());
                DayOffRepository dayOffRepo = new DayOffRepository();
                model.DayOff = dayOffRepo.ListDayOff_Active();
                model.EnableCaptcha = base.EnableChaptcha;
                model.ConfigPage = item;

                return View(Language.ToString() + "/Index", model);
            } else {
                return Redirect("Index");
            }
        }

        // POST: Contact/Save
        public ActionResult Save(ContactViewModel model) {
            //ReCaptcha
            bool status = ValidateReCaptcha();
            if (status && ModelState.IsValid) {
                LeadRepository leadRepo = new LeadRepository();
                DateTime d;
                var dr = DateTime.TryParseExact(model.CallBackDay + " " + model.CallBackTime, "dd/MM/yyyy HH:mm", cultureInfoEN, System.Globalization.DateTimeStyles.None, out d);
                model.CallBackDate = d;
                model.Source = TobJod.Models.LeadSource.ContactUs;
                model.ProductId = 0;
                model.PremiumId = 0;
                model.Remark = "";
                if (model.Message == null) model.Message = "";
                model.LeadStatus = TobJod.Models.LeadStatus.Pending;
                model.OwnerAdminId = model.AssignAdminId = "";
                model.CreateDate = model.UpdateDate = DateTime.Now;
                model.CreateAdminId = model.UpdateAdminId = "";
                var result = leadRepo.AddLead(model);
                if (result > 0) {
                    SendMail(model);
                    Notify("ข้อมูลติดต่อของท่านถูกบันทึกแล้ว");
                    return RedirectToAction("Index");
                } else {
                    ViewBag.Notify = "ไม่สามารถบันทึกข้อมูลติดต่อของท่านได้ กรุณาลองใหม่อีกครั้ง";
                }
            } else {
                ViewBag.Notify = "ไม่สามารถบันทึกข้อมูลติดต่อของท่านได้ กรุณาลองใหม่อีกครั้ง";
            }
            model = BindForm(model);
            return View(Language.ToString() + "/Index", model);
        }

        private void SendMail(ContactViewModel model) {
            ContactSubCategoryRepository subRepo = new ContactSubCategoryRepository();
            var sub = subRepo.GetContactSubCategory_Active(model.SubjectId);

            string row = "<tr><th>{0}</th><td>{1}<td></tr>";
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<table style='margin=auto;'>");
            if (Language == TobJod.Models.Language.TH) {
                sb.AppendLine(string.Format(row, "ชื่อ-นามสกุล", model.Name + " " + model.Surname));
                sb.AppendLine(string.Format(row, "อีเมล", model.Email));
                sb.AppendLine(string.Format(row, "เบอร์โทรศัพท์", model.Tel));
                sb.AppendLine(string.Format(row, "วันเวลาสำหรับติดต่อกลับ", model.CallBackDate.ToString("dd/MM/yyyy HH:mm", cultureInfoEN) + " - " + model.CallBackDate.AddHours(2).ToString("HH:mm", cultureInfoEN)));
                sb.AppendLine(string.Format(row, "เรื่องที่ต้องการติดต่อ", sub.TitleTH));
                sb.AppendLine(string.Format(row, "ข้อความ", model.Message));
            } else {
                sb.AppendLine(string.Format(row, "Name", model.Name + " " + model.Surname));
                sb.AppendLine(string.Format(row, "Email", model.Email));
                sb.AppendLine(string.Format(row, "Tel", model.Tel));
                sb.AppendLine(string.Format(row, "Call Back Date", model.CallBackDate.ToString("dd/MM/yyyy HH:mm", cultureInfoEN) + " - " + model.CallBackDate.AddHours(2).ToString("HH:mm", cultureInfoEN)));
                sb.AppendLine(string.Format(row, "Subject", sub.TitleEN));
                sb.AppendLine(string.Format(row, "Message", model.Message));
            }
            sb.AppendLine("</table>");

            string template = Server.MapPath("~/assets/mail/" + Language.ToString() + "/contact.html");
            string body = FileUtils.ReadText(template);
            string template_blank = Server.MapPath("~/assets/mail/template.html");
            string body_blank = FileUtils.ReadText(template_blank);
            MailMessage message = new MailMessage();
            string configPath = HttpUtils.GetPathUrlAbsolute(System.Configuration.ConfigurationManager.AppSettings["Path_Config"]);
            ConfigRepository configRepo = new ConfigRepository();
            var configs = configRepo.ListConfigs(new List<ConfigName>() { ConfigName.EmailImageHeader, ConfigName.EmailImageFooter });
            try {
                message.Subject = "Tobjod - Contact Us";
                message.Body = body.Replace("{{base_url}}", HttpUtils.GetBaseUrl())
                                        .Replace("{{email_image_header}}", configPath + configs[ConfigName.EmailImageHeader])
                                        .Replace("{{email_image_footer}}", configPath + configs[ConfigName.EmailImageFooter])
                                    .Replace("{{message}}", sb.ToString());
                message.IsBodyHtml = true;

                message.From = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["Mail_From_Email"], System.Configuration.ConfigurationManager.AppSettings["Mail_From_Name"]);
                
                SmtpClient smtp = new SmtpClient();

                message.To.Add(model.Email);
                try { smtp.Send(message); } catch { }


                message.Body = body_blank.Replace("{{base_url}}", HttpUtils.GetBaseUrl())
                                        .Replace("{{email_image_header}}", configPath + configs[ConfigName.EmailImageHeader])
                                        .Replace("{{email_image_footer}}", configPath + configs[ConfigName.EmailImageFooter])
                                    .Replace("{{message}}", sb.ToString());
                message.To.Clear();
                var emails = sub.MailTo.Split(';');
                foreach (var mailTo in emails) {
                    if (!string.IsNullOrWhiteSpace(mailTo)) message.To.Add(mailTo.Trim());
                }
                smtp.Send(message);

            } catch { }
        }

        private ContactViewModel BindForm(ContactViewModel model) {
            ConfigPageRepository repo = new ConfigPageRepository();
            var config = repo.GetConfigPage(TobJod.Models.ConfigPageName.ContactUs);
            ContactSubCategoryRepository subRepo = new ContactSubCategoryRepository();
            var sub = subRepo.ListContactSubCategory_SelectList_Active(Language);
            model.ConfigPage = config;
            Dictionary<String, List<ContactSubCategory>> subC = new Dictionary<string, List<ContactSubCategory>>();
            foreach (var item in sub) {
                if (!subC.ContainsKey(item.Category)) subC.Add(item.Category, new List<ContactSubCategory>());
                subC[item.Category].Add(item);
            }

            List<SelectListItem> listItems = new List<SelectListItem>();
            foreach (var item in subC.Keys) {
                if (subC[item].Count == 1) {
                    var it = subC[item][0];
                    listItems.Add(new SelectListItem() { Text = (Language == Language.EN ? it.TitleEN : it.TitleTH), Value = it.Id.ToString(), Group = null });
                } else {
                    var group = new SelectListGroup() { Name = item };
                    foreach (var it in subC[item]) {
                        listItems.Add(new SelectListItem() { Text = (Language == Language.EN ? it.TitleEN : it.TitleTH), Value = it.Id.ToString(), Group = group });
                    }
                }
            }
            model.SubCategoryList = listItems;

            PageMeta.FromConfigPage(config, Language);
            return model;
        }
    }
}