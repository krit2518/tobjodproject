﻿using Facebook;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Web.Models;

namespace TobJod.Web.Controllers
{
    public class AuthenController : BaseController
    {

        private string GoogleClientId = ConfigurationManager.AppSettings["Google.ClientID"];
        private string GoogleSecretKey = ConfigurationManager.AppSettings["Google.SecretKey"];
        private string GoogleRedirectUrl = ConfigurationManager.AppSettings["Google.RedirectUrl"];
        private string FacebookClientId = ConfigurationManager.AppSettings["Facebook.ClientID"];
        private string FacebookSecretKey = ConfigurationManager.AppSettings["Facebook.SecretKey"];
        private string FacebookRedirectUrl = ConfigurationManager.AppSettings["Facebook.RedirectUrl"];

        // GET: Authen

        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public async Task<ActionResult> GoogleUser(string code, string state, string session_state)
        {

            if (string.IsNullOrEmpty(code))
            {
                return View("Error");
            }
            var httpClient = new HttpClient
            {
                BaseAddress = new Uri("https://www.googleapis.com")
            };
            var requestUrl = $"oauth2/v4/token?code={code}&client_id={GoogleClientId}&client_secret={GoogleSecretKey}&redirect_uri={GoogleRedirectUrl}&grant_type=authorization_code";

            var dict = new Dictionary<string, string>
            {
                { "Content-Type", "application/x-www-form-urlencoded" }
            };
            var req = new HttpRequestMessage(System.Net.Http.HttpMethod.Post, requestUrl) { Content = new FormUrlEncodedContent(dict) };
            var response = await httpClient.SendAsync(req);
            var token = JsonConvert.DeserializeObject<GmailToken>(await response.Content.ReadAsStringAsync());

            Session["UserAccount"] = token.AccessToken;

            var obj = await GoogleProfile(token.AccessToken);

            MemberRepository MemberRepo = new MemberRepository();
            Register reg = new Register();

            reg.Email = obj.Email != null ? obj.Email : "";
            reg.Member_Password = "";
            reg.LoginRefId = obj.Id != null ? obj.Id : "";
            reg.LoginChannel = 3;
            reg.Name = obj.GivenName != null ? obj.GivenName : "";
            reg.Surname = obj.FamilyName != null ? obj.FamilyName : "";
            reg.Telephone = "";
            reg.Sex = obj.Gender != null ? obj.Gender == "Male" ? 1 : 0 : 0;
            reg.Birthday = DateTime.Now;
            reg.MaritalStatus = 1;
            reg.CareerId = 1;
            reg.NationalId = 1;
            reg.Weight = 1;
            reg.Height = 0;
            reg.Image = obj.Picture != null ? obj.Picture : "";
            reg.AddressNo = "";
            reg.AddressMoo = "";
            reg.AddressVillage = "";
            reg.AddressFloor = "";
            reg.AddressSoi = "";
            reg.AddressRoad = "";
            reg.AddressDistrictId = 1;
            reg.AddressSubDistrictId = 1;
            reg.AddressProvinceId = 1;
            reg.AddressPostalCode = "";
            reg.LoginAttempt = 1;
            reg.Status = 1;
            reg.CreateDate = DateTime.Now;
            reg.UpdateDate = DateTime.Now;

            var result = MemberRepo.InsertRegisterGoogle(reg);

            return RedirectToAction("Index", "Home");
        }
        public async Task<GoogleAccountModel> GoogleProfile(string accesstoken)
        {
            var httpClient = new HttpClient
            {
                BaseAddress = new Uri("https://www.googleapis.com")
            };
            string url = $"https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token={accesstoken}";
            var response = await httpClient.GetAsync(url);
            return JsonConvert.DeserializeObject<GoogleAccountModel>(await response.Content.ReadAsStringAsync());
        }

        public ActionResult VerifyEmail(string Email)
        {
            MemberRepository LoginRepo = new MemberRepository();

            var result = LoginRepo.UpdateVerifyByEmail(Email);
            if (result > 0)
            {
                Notify("Verify Email Success");
                ViewBag.Notify = "Verify Email Success";
            }
            return RedirectToAction("Verify", "Authen");
        }

        [HttpGet]
        public async Task<ActionResult> FacebookUser(string code, string state, string session_state)
        {

            if (string.IsNullOrEmpty(code))
            {
                return View("Error");
            }
            var fb = new FacebookClient();

            dynamic result = fb.Get("oauth/access_token", new
            {
                client_id = FacebookClientId,
                client_secret = FacebookSecretKey,
                redirect_uri = FacebookRedirectUrl,
                code = code
            });
            var AccessToken = result != null ? result[0] : "";

            Session["UserAccount"] = AccessToken;

            FacebookAccountModel obj = FacebookProfile(AccessToken);

            MemberRepository MemberRepo = new MemberRepository();
            Register reg = new Register();

            reg.Email = "";
            reg.Member_Password = "";
            reg.LoginRefId = obj.RefId != null ? obj.RefId : "";
            reg.LoginChannel = 2;
            reg.Name = obj.Name != null ? obj.Name : "";
            reg.Surname = obj.FamilyName != null ? obj.FamilyName : "";
            reg.Telephone = "";
            reg.Sex = 0;
            reg.Birthday = DateTime.Now;
            reg.MaritalStatus = 1;
            reg.CareerId = 1;
            reg.NationalId = 1;
            reg.Weight = 1;
            reg.Height = 0;
            reg.Image = "";
            reg.AddressNo = "";
            reg.AddressMoo = "";
            reg.AddressVillage = "";
            reg.AddressFloor = "";
            reg.AddressSoi = "";
            reg.AddressRoad = "";
            reg.AddressDistrictId = 1;
            reg.AddressSubDistrictId = 1;
            reg.AddressProvinceId = 1;
            reg.AddressPostalCode = "";
            reg.LoginAttempt = 1;
            reg.Status = 1;
            reg.CreateDate = DateTime.Now;
            reg.UpdateDate = DateTime.Now;

            MemberRepo.InsertRegisterGoogle(reg);

            return RedirectToAction("Index", "Home");
        }
        public FacebookAccountModel FacebookProfile(string accesstoken)
        {
            var client = new FacebookClient(accesstoken);
            dynamic response = client.Get("/me", new { fields = "first_name,last_name,email", access_token = accesstoken });
            FacebookAccountModel obj = new FacebookAccountModel();
            obj.Name = response != null ? response[0] : "";
            obj.FamilyName = response != null ? response[1] : "";
            obj.RefId = response != null ? response[2] : "";
            return obj;
        }

    }
}