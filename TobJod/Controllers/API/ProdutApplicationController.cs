﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Web.Controllers.API {
    public class ProdutApplicationController : ApiController {
        // GET: /Api/GEO/District/
        [System.Web.Http.Route("api/GEO/District")]
        public List<NameValue> GetDistrict(string lang = "th", string provinceId = "0") {
            Language language = (string.Equals(lang.ToUpper(), "EN") ? Language.EN : Language.TH);
            DistrictRepository districtRepo = new DistrictRepository();

            List<NameValue> districts = new List<NameValue>();
            var data = districtRepo.ListDistrict_SelectList_Active(TobJod.Utils.NullUtils.cvInt(provinceId), language);

            foreach (var item in data) {
                districts.Add(new NameValue { Name = item.Text, Value = item.Value });
            }

            return districts;
        }

        // GET: /Api/GEO/SubDistrict/
        [System.Web.Http.Route("api/GEO/SubDistrict")]
        public List<SubDistrict> GetSubDistrict(string lang = "th", string districtId = "0") {
            Language language = (string.Equals(lang.ToUpper(), "EN") ? Language.EN : Language.TH);
            SubDistrictRepository subDistrictRepo = new SubDistrictRepository();

            return subDistrictRepo.ListSubDistrict_Active(TobJod.Utils.NullUtils.cvInt(districtId), language);
        }

        // GET: /Api/Counpon/
        [System.Web.Http.Route("api/Coupon")]
        public NameValue GetCoupon(string lang = "th", string couponCode = "", int productId = 0) {
            var systemMessageRepository = new SystemMessageRepository();
            var repo = new ProductApplicationRepository();
            var couponRepo = new CouponRepository();
            var coupon = repo.GetCoupon_Active(NullUtils.cvString(couponCode), NullUtils.cvInt(productId));
            var messages = systemMessageRepository.ListSystemMessage();
            var message = new SystemMessage();
            
            if (coupon != null) {
                if (coupon.CouponType == CouponType.OneTime) {
                    if (coupon.TotalUsed == 0) {
                        message = messages.Where(o => o.Category == SystemMessageCategory.Coupon && o.Code == ((int)SystemMessageStatus.CouponAvailable).ToString()).FirstOrDefault();
                        return new NameValue { Value = "200", Name = lang.ToLower() == "th" ? message.BodyTH : message.BodyEN };
                    }
                    message = messages.Where(o => o.Category == SystemMessageCategory.Coupon && o.Code == ((int)SystemMessageStatus.CouponUsed).ToString()).FirstOrDefault();
                    return new NameValue { Value = "400", Name = lang.ToLower() == "th" ? message.BodyTH : message.BodyEN };
                }

                if (coupon.CouponType == CouponType.Mass) {
                    if (coupon.TotalUsed < coupon.Total)
                    {
                        message = messages.Where(o => o.Category == SystemMessageCategory.Coupon && o.Code == ((int)SystemMessageStatus.CouponAvailable).ToString()).FirstOrDefault();
                        return new NameValue { Value = "200", Name = lang.ToLower() == "th" ? message.BodyTH : message.BodyEN };
                    }
                    else
                    {
                        message = messages.Where(o => o.Category == SystemMessageCategory.Coupon && o.Code == ((int)SystemMessageStatus.CouponUsed).ToString()).FirstOrDefault();
                        return new NameValue { Value = "400", Name = lang.ToLower() == "th" ? message.BodyTH : message.BodyEN };
                    }
                }
                
            }
            message = messages.Where(o => o.Category == SystemMessageCategory.Coupon && o.Code == ((int)SystemMessageStatus.CouponUnKnown).ToString()).FirstOrDefault();
            return new NameValue { Value = "400", Name = lang.ToLower() == "th" ? message.BodyTH : message.BodyEN };
        }


        // GET: /Api/Car/Model/
        [System.Web.Http.Route("api/Car/CC")]
        public NameValue GetCarModelCC(int carModelId)
        {
            CarRepository carRepo = new CarRepository();
            return carRepo.GetCarModelCC_NameValue(carModelId);
        }
    }
}