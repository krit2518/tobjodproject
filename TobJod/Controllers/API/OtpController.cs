﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Service;

namespace TobJod.Web.Controllers.API {

    public class OtpController : ApiController {

        // POST: /api/otp/send/
        [System.Web.Http.Route("api/otp/send")]
        [System.Web.Mvc.HttpPost]
        public ActionResultStatus Send([FromBody]OtpRequest req) {
            ActionResultStatus result = new ActionResultStatus();
            if (string.IsNullOrEmpty(req.mobile) || string.IsNullOrEmpty(req.id)) {
                result.Success = false;
                result.ErrorMessage = "Invalida data";
                return result;
            }
            SmsService sms = new SmsService();
            SmsServiceOtpRequest request = new SmsServiceOtpRequest();
            request.MobileNo = req.mobile;
            request.UniqueId = req.id;
            request.ChannelId = System.Configuration.ConfigurationManager.AppSettings["SMS_API_REQUEST_OTP_CHANNEL_ID"];
            request.Template = System.Configuration.ConfigurationManager.AppSettings["SMS_API_REQUEST_OTP_TEMPLATE"];
            result = sms.RequestOTP(request);
            result.ErrorMessage = "";
            return result;
        }

        // POST: /api/otp/verify/
        [System.Web.Http.Route("api/otp/verify")]
        [System.Web.Mvc.HttpPost]
        public ActionResultStatus Verify([FromBody]OtpRequest req) {
            ActionResultStatus result = new ActionResultStatus();
            if (string.IsNullOrEmpty(req.mobile) || string.IsNullOrEmpty(req.id) || (string.IsNullOrEmpty(req.otp) || req.otp.Length != 6)) {
                result.Success = false;
                result.ErrorMessage = "Invalida data";
                return result;
            }
            SmsService sms = new SmsService();
            SmsServiceOtpVerify request = new SmsServiceOtpVerify();
            request.MobileNo = req.mobile;
            request.UniqueId = req.id;
            request.ChannelId = System.Configuration.ConfigurationManager.AppSettings["SMS_API_REQUEST_OTP_CHANNEL_ID"];
            request.OTPPassword = req.otp;
            result = sms.VerifyOTP(request);

            SmsOtpRepository repo = new SmsOtpRepository();
            repo.AddSmsOtp(new SmsOtp() { ApplicationId = req.app, UniqueId = req.id, Mobile = req.mobile, VerifySuccess = result.Success, Status = (int)Status.Active, CreateDate = DateTime.Now });

            result.ErrorMessage = "";
            return result;
        }

        public class OtpRequest {
            public int app { get; set; }
            public string mobile { get; set; }
            public string id { get; set; }
            public string otp { get; set; }
        }
    }
}
