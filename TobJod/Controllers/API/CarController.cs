﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using TobJod.Models;
using TobJod.Repositories;

namespace TobJod.Web.Controllers.API {

    public class CarController : ApiController {

        // GET: /Api/Car/Family/
        [System.Web.Http.Route("api/Car/Family")]
        public List<NameValue> GetFamily(string lang, int brandId) {
            Language language = (string.Equals(lang, "EN") ? Language.EN : Language.TH);
            CarRepository carRepo = new CarRepository();
            return carRepo.ListCarFamilyByBrand_NameValue(language, brandId);
        }

        // GET: /Api/Car/Model/
        [System.Web.Http.Route("api/Car/Model")]
        public List<NameValue> GetModel(string lang, int familyId) {
            Language language = (string.Equals(lang, "EN") ? Language.EN : Language.TH);
            CarRepository carRepo = new CarRepository();
            return carRepo.ListCarModelByFamily_NameValue(language, familyId);
        }
    }
}
