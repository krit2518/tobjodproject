﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;
using TobJod.Web.Models;

namespace TobJod.Web.Controllers {
    public class HomeController : BaseController {

        public ActionResult Index() {
            HomeViewModel model = new HomeViewModel();
            model.PathBanner = HttpUtils.GetPathRelative(System.Configuration.ConfigurationManager.AppSettings["Path_Banner"]);
            model.PathPartnerBanner = HttpUtils.GetPathRelative(System.Configuration.ConfigurationManager.AppSettings["Path_PartnerBanner"]);
            model.PathConfig = HttpUtils.GetPathRelative(System.Configuration.ConfigurationManager.AppSettings["Path_Config"]);

            if (Session == null || Session["popup_" + Language.ToString()] == null) {
                PopupRepository popupRepo = new PopupRepository();
                model.Popup = popupRepo.ListPopupActive(Language, 1, 1).FirstOrDefault();
                Session["popup_" + Language.ToString()] = "1";
            }

            BannerRepository bannerRepo = new BannerRepository();
            model.Banner = bannerRepo.ListBannerActive(Language, 1, 5);

            PartnerBannerRepository partnerBannerRepo = new PartnerBannerRepository();
            model.PartnerBanner = partnerBannerRepo.ListPartnerBannerActive(Language);

            //ProductCategoryRepository categoryRepo = new ProductCategoryRepository();
            //model.ProductCategoryOnHome = categoryRepo.ListProductCategoryOnHome_Active(Language);

            UserTestimonialRepository testimonialRepo = new UserTestimonialRepository();
            model.UserTestimonial = testimonialRepo.ListUserTestimonial(DateTime.Now.DayOfYear);

            ConfigRepository configRepo = new ConfigRepository();
            List<ConfigName> idList = new List<ConfigName>();
            if (Language == Language.TH) {
                idList = new List<ConfigName>() { ConfigName.BannerTransition, ConfigName.BannerDuration, ConfigName.HomePromotion1ImageTH, ConfigName.HomePromotion1UrlTH, ConfigName.HomePromotion1Target, ConfigName.HomePromotion2ImageTH, ConfigName.HomePromotion2UrlTH, ConfigName.HomePromotion2Target, ConfigName.HomePromotion3ImageTH, ConfigName.HomePromotion3UrlTH, ConfigName.HomePromotion3Target, ConfigName.HomePromotion4ImageTH, ConfigName.HomePromotion4UrlTH, ConfigName.HomePromotion4Target, ConfigName.HomePromotion1ImageMobileTH, ConfigName.HomePromotion2ImageMobileTH, ConfigName.HomePromotion3ImageMobileTH, ConfigName.HomePromotion4ImageMobileTH };
                var configs = configRepo.ListConfigs(idList);
                model.Promotion1Image = configs[ConfigName.HomePromotion1ImageTH];
                model.Promotion2Image = configs[ConfigName.HomePromotion2ImageTH];
                model.Promotion3Image = configs[ConfigName.HomePromotion3ImageTH];
                model.Promotion4Image = configs[ConfigName.HomePromotion4ImageTH];
                model.Promotion1ImageMobile = configs[ConfigName.HomePromotion1ImageMobileTH];
                model.Promotion2ImageMobile = configs[ConfigName.HomePromotion2ImageMobileTH];
                model.Promotion3ImageMobile = configs[ConfigName.HomePromotion3ImageMobileTH];
                model.Promotion4ImageMobile = configs[ConfigName.HomePromotion4ImageMobileTH];
                model.Promotion1Url = configs[ConfigName.HomePromotion1UrlTH];
                model.Promotion2Url = configs[ConfigName.HomePromotion2UrlTH];
                model.Promotion3Url = configs[ConfigName.HomePromotion3UrlTH];
                model.Promotion4Url = configs[ConfigName.HomePromotion4UrlTH];
                model.Promotion1Target = configs[ConfigName.HomePromotion1Target];
                model.Promotion2Target = configs[ConfigName.HomePromotion2Target];
                model.Promotion3Target = configs[ConfigName.HomePromotion3Target];
                model.Promotion4Target = configs[ConfigName.HomePromotion4Target];
                model.BannerTransition = configs[ConfigName.BannerTransition];
                model.BannerDuration = configs[ConfigName.BannerDuration];
            } else {
                idList = new List<ConfigName>() { ConfigName.BannerTransition, ConfigName.BannerDuration, ConfigName.HomePromotion1ImageEN, ConfigName.HomePromotion1UrlEN, ConfigName.HomePromotion1Target, ConfigName.HomePromotion2ImageEN, ConfigName.HomePromotion2UrlEN, ConfigName.HomePromotion2Target, ConfigName.HomePromotion3ImageEN, ConfigName.HomePromotion3UrlEN, ConfigName.HomePromotion3Target, ConfigName.HomePromotion4ImageEN, ConfigName.HomePromotion4UrlEN, ConfigName.HomePromotion4Target, ConfigName.HomePromotion1ImageMobileEN, ConfigName.HomePromotion2ImageMobileEN, ConfigName.HomePromotion3ImageMobileEN, ConfigName.HomePromotion4ImageMobileEN };
                var configs = configRepo.ListConfigs(idList);
                model.Promotion1Image = configs[ConfigName.HomePromotion1ImageEN];
                model.Promotion2Image = configs[ConfigName.HomePromotion2ImageEN];
                model.Promotion3Image = configs[ConfigName.HomePromotion3ImageEN];
                model.Promotion4Image = configs[ConfigName.HomePromotion4ImageEN];
                model.Promotion1ImageMobile = configs[ConfigName.HomePromotion1ImageMobileEN];
                model.Promotion2ImageMobile = configs[ConfigName.HomePromotion2ImageMobileEN];
                model.Promotion3ImageMobile = configs[ConfigName.HomePromotion3ImageMobileEN];
                model.Promotion4ImageMobile = configs[ConfigName.HomePromotion4ImageMobileEN];
                model.Promotion1Url = configs[ConfigName.HomePromotion1UrlEN];
                model.Promotion2Url = configs[ConfigName.HomePromotion2UrlEN];
                model.Promotion3Url = configs[ConfigName.HomePromotion3UrlEN];
                model.Promotion4Url = configs[ConfigName.HomePromotion4UrlEN];
                model.Promotion1Target = configs[ConfigName.HomePromotion1Target];
                model.Promotion2Target = configs[ConfigName.HomePromotion2Target];
                model.Promotion3Target = configs[ConfigName.HomePromotion3Target];
                model.Promotion4Target = configs[ConfigName.HomePromotion4Target];
                model.BannerTransition = configs[ConfigName.BannerTransition];
                model.BannerDuration = configs[ConfigName.BannerDuration];
            }
            return View(Language.ToString() + "/Index", model);
        }
        
        public ActionResult Search(string search) {
            ViewBag.Search = search;
            ViewBag.UrlUpload = HttpUtils.GetPathRelative(System.Configuration.ConfigurationManager.AppSettings["Path_News"]);
            return View(Language.ToString() + "/Search");
        }
        
        public ActionResult SearchList(string search) {
            int pageNo = GetPageNo();

            NewsRepository repo = new NewsRepository();
            var model = repo.ListNewsSearch_Active(Language, pageNo, 6, search);
            
            var data = new { page = pageNo, data = model.Select((x) => new NewsListItemViewModel() { Title = (Language == Language.TH ? x.TitleTH : x.TitleEN), SubCategory = x.NewsSubCategory, Brief = (Language == Language.TH ? x.BriefTH.ReplacePath() : x.BriefEN.ReplacePath()), Image = (Language == Language.TH ? x.ImageTH : x.ImageEN), ImageAlt = (Language == Language.TH ? x.ImageAltTH : x.ImageAltEN), Url = Url.Action("Index", "News") + "/" + x.UrlPrefix + "/" + (Language == Language.TH ? x.UrlTH : x.UrlEN), Date = x.CreateDate.ToString("dd MMMM yyyy", cultureInfo) }).ToList() };

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public ActionResult SearchSuggest(string search) {
            TagRepository tagRepo = new TagRepository();
            var model = tagRepo.ListTag_Search(search);
            
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Subscribe(string email) {
            int second = NullUtils.cvInt(System.Configuration.ConfigurationManager.AppSettings["Mail_Subscribe_Spam_Second"], 1);
            Subscribe item = new Subscribe();
            item.Email = email;
            item.CreateDate = DateTime.Now;
            item.LocalIP = HttpUtils.GetPrivateIP();
            item.PublicIP = HttpUtils.GetPublicIP();
            SubscribeRepository repo = new SubscribeRepository();
            var lastSubscribe = repo.GetSubscribeIP(item.PublicIP, item.LocalIP);
            var result = 0;
            if (ValueUtils.IsValidEmail(email) && (lastSubscribe == null || lastSubscribe.CreateDate <= DateTime.Now.AddSeconds(-second))) {
                result = repo.AddSubscribe(item);
                if (result > 0) {
                    string template = Server.MapPath("~/assets/mail/" + Language.ToString() + "/subscribe.html");
                    string body = FileUtils.ReadText(template);
                    MailMessage message = new MailMessage();
                    try {
                        message.Subject = "Tobjod - Subscribe";
                        message.Body = body.Replace("{{base_url}}", HttpUtils.GetBaseUrl())
                                            .Replace("{{message}}", "");
                        message.IsBodyHtml = true;

                        message.From = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["Mail_From_Email"], System.Configuration.ConfigurationManager.AppSettings["Mail_From_Name"]);

                        message.To.Add(email);

                        SmtpClient smtp = new SmtpClient();
                        smtp.Send(message);
                    } catch { }
                }
            }
            return Json(result);
        }

 
    }
}