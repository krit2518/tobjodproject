﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Web.Controllers {
    public class ClaimProcessController : BaseController {

        // GET: ClaimProcess
        public ActionResult Index() {
            ConfigPageRepository repo = new ConfigPageRepository();
            var model = repo.GetConfigPage(TobJod.Models.ConfigPageName.ClaimProcess);
            PageMeta.FromConfigPage(model, Language);
            return View(Language.ToString() + "/Index", model);
        }

        // POST: ClaimProcess/Preview
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Preview(String body) {
            if (NullUtils.cvBoolean(System.Configuration.ConfigurationManager.AppSettings["Backend_Preview"])) {
                ConfigPageRepository repo = new ConfigPageRepository();
                ConfigPage model = repo.GetConfigPage_Admin(ConfigPageName.ClaimProcess);
                model.BodyTH = ReplaceTextEditorPathDisplay(body);
                model.BodyEN = ReplaceTextEditorPathDisplay(body);
                return View(Language.ToString() + "/Index", model);
            } else {
                return Redirect("Index");
            }
        }
    }
}