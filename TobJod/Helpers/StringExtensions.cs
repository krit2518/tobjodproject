﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using TobJod.Utils;

namespace TobJod.Web {
    public static class TobJodStringExtensions {
        
        public static string ReplacePath(this string original) {
            //return string.format(original, HttpUtils.GetPathRelative("~"));
            return original.Replace("{{url}}", HttpUtils.GetPathRelative("~"));
        }

    }
}