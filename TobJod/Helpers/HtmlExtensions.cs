﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using TobJod.Utils;

namespace TobJod.Web {
    public static class HtmlExtensions {

        public static MvcHtmlString RawHtml(this string original) {
            return MvcHtmlString.Create(original);
        }

    }
}