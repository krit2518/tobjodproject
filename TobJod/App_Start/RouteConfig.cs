﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using TobJod.Models;

namespace TobJod {
    public class RouteConfig {
        public static void RegisterRoutes(RouteCollection routes) {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");


            routes.MapMvcAttributeRoutes();
            routes.LowercaseUrls = true;

            routes.MapRoute(
                name: "News",
                url: "{language}/News/{year}/{slug}",
                defaults: new { controller = "News", action = "Detail", year = 2017, slug = "" },
                constraints: new { language = "(th|en)", year = "20[0-9]{2}", slug = ".+" }
            );

            routes.MapRoute(
                name: "Privilege",
                url: "{language}/Privilege/detail/{slug}",
                defaults: new { controller = "Privilege", action = "Detail", slug = "" },
                constraints: new { language = "(th|en)", slug = ".+" }
            );

            routes.MapRoute(
                name: "PaymentMethod",
                url: "{language}/PaymentMethod/detail/{slug}",
                defaults: new { controller = "PaymentMethod", action = "Detail", slug = "" },
                constraints: new { language = "(th|en)", slug = ".+" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{language}/{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional, language = "th" },
                constraints: new { language = "(th|en)" }
            );
            
            routes.MapRoute(
                name: "Error",
                url: "Error",
                defaults: new { controller = "Error", action = "Index" }
            );

        }
    }
}
