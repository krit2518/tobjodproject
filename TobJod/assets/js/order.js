﻿//OTP

$.ajaxSetup({ cache: false });

var sms_loading = false;

$(function () {

    var verified_id = '';
    var verified_mobile = '';
    
    $('label > span.checkbox_chk').each(function (ev) {
        var $label = $(this).parent();
        $label.on('click', function (event) {
            var nodeName = event.target.nodeName;
            if (nodeName.toUpperCase() == "LABEL") {
                return false;
            }
        });
    });

    $('div.input_hidden').each(function () {
        $(this).parent().on('click', function (ev) {
            var $file = $(this).find('input[type=file]');
            var $this = $(this); var nodeName = ev.target.nodeName;
            if (nodeName != 'INPUT' && !$(ev.target).hasClass('remove')) {
                $file[0].click();
            }
        });

        $('div.input_hidden input[type=file]').on('change', function (ev) {
            var $fv = $('#order_form').data('formValidation');
            var $this = $(this).parents('.form-group');
            var $file = $(this);
            if (true) {
                if ($this.find('.fa-trash').length == 0) {
                    var $trash = $('<span class="icon remove-file"> <a href="javascript:void(0);"><img class="width_icon remove" src="' + BASEURL + '/assets/images/icon/icon_remove.svg"></a></span>');
                    var $span = $this.find('span.icon');
                    $span.replaceWith($trash);
                    $trash.on('click', function (ev) {
                        $file.val('');
                        $fv.resetField($file[0].name);
                        $trash.replaceWith('<span class="icon"><img class="width_icon" src="' + BASEURL + '/assets/images/icon/icon_image.jpg" /></span>');
                        $this.find('.fake_input').children('.file_name').html('Please Select Attachment File');
                    })
                }
            } else {
                $this.find('span.icon').replaceWith('<span class="icon"><img class="width_icon" src=""' + BASEURL + '/assets/images/icon/icon_image.jpg" /></span>');
            }
        });
    });

    $('input:file').on('change', function (ev) {
        var $fv = $('#order_form').data('formValidation');
        //if (this.name) {
        //    if (!$fv.isValidField(this.name)) {
        //        var $this = $(this);
        //        var $i = $('<i class="fa fa-trash"><i>');
        //        $i.appendTo($('[data-fv-for="' + this.name + '"'));
        //        $i.on('click', function (ev) {
        //            //$this.parents('.input_hidden').siblings('.fake_input').children('.file_name').html('');
        //        });
        //    }
        //}
    });

    //$('div.policy-holder:not(:hidden)').find('select').each(function () { bind_msDropdown(this); });
    $('a.add_more_tab').on('click', function (ev) {
        $dd = $('div.policy-holder:hidden').first();$dd.show();
        //$dd.find('select').each(function () { bind_msDropdown(this); });
    });

    //$('div.driver:not(:hidden)').find('select').each(function () { bind_msDropdown(this); });
    $('a.add_more_tab_driver').on('click', function (ev) {
        $dd = $('div.driver:hidden').first(); $dd.show();
        //$dd.find('select').each(function () { bind_msDropdown(this); });
    });
    $('a.add_minus_tab_policy_holder').on('click', function (ev) {
        $dd = $($(this).data('target')); $dd.fadeOut('slow');
        //$dd.find('select').each(function () { $(this).data('dd').destroy(); });
    });

    $('.id-type').each(function (ev) {
        var $target = $($(this).data('target'));
        var $fv = $('#order_form').data('formValidation');
        if ($target.length > 0) {
            var val = $(this).val();
            switch (val) {
                case '1':
                    $fv.enableFieldValidators($target[0].name, true, 'id')
                        .revalidateField($target[0].name);
                    break;
                case '2':
                    $fv.enableFieldValidators($target[0].name, false, 'id')
                        .revalidateField($target[0].name);
                    break;
                default:
                    break;
            }
        }
    });

    $('.copy-address-holder').each(function (i, e) {
        if ($(e).data('checked')) {
            $(e).trigger('click'); $(e).trigger('click');
        }
    });

    $('.id-type').on('change', function (ev) {
        var $target = $($(this).data('target'));
        if ($target.length > 0) {
            var val = $(this).val();
            switch (val) {
                case '1':
                    //$formValidation.formValidation('enableFieldValidators', $target[0].name, true, 'id').formValidation('revalidateField', $target[0].name);
                    $target.attr('maxlength', 13);
                    var val = $target.val().trim().length;
                    if (val > 13) {
                        $target.val($target.val().substr(0, 13));
                    }
                    $formValidation.formValidation('enableFieldValidators', $target[0].name, true, 'id');
                    break;
                case '2':
                    //$formValidation.formValidation('enableFieldValidators', $target[0].name, false, 'id').formValidation('revalidateField', $target[0].name);
                    $formValidation.formValidation('enableFieldValidators', $target[0].name, false, 'id');
                    $target.attr('maxlength', 50);
                    break;
                default:
                    $target.attr('maxlength', 50);
                    break;
            }
            if ($target.val() != '') $formValidation.formValidation('revalidateField', $target[0].name);
            $target.on('blur', function (ev) {
                $formValidation.formValidation('revalidateField', $target[0].name);
            });
        }
    });

    $('.address-province-ddl').change(function (e) {
        $target = $($(this).data('target'));
        $target.prop('disabled', true);
        var url = BASEURL + 'api/GEO/District?lang=' + LANGUAGE + '&provinceId=' + $(this).val();
        $.getJSON(url, function (data) {
            if (!mobileAndTabletcheck()) $target.msDropdown().data("dd").destroy();
            var items = [];
            items.push('<option value="">' + $target.find('option:first').text() + '</option>');
            $.each(data, function (key, val) {
                items.push("<option value='" + val.Value + "'>" + val.Name + "</option>");
            });
            $target.html(items.join(""));
            $target.prop('disabled', false);

            if (!mobileAndTabletcheck()) $target.msDropdown().data("dd");
            //$('#order_form').data('formValidation').revalidateField($target.attr('name'));
            $target.trigger('change');
        });
    });

    $('.address-district-ddl').change(function (e) {
        $target = $($(this).data('target'));
        $target.prop('disabled', true);
        var url = BASEURL + 'api/GEO/SubDistrict?lang=' + LANGUAGE + '&districtId=' + $(this).val();
        $.getJSON(url, function (data) {

            if (!mobileAndTabletcheck()) $target.msDropdown().data("dd").destroy();
            var items = [];
            items.push('<option value="">' + $target.find('option:first').text() + '</option>');
            $.each(data, function (key, val) {
                items.push("<option data-zipcode='" + val.ZipCode + "' value='" + val.Id + "'>" + eval('val.Title' + LANGUAGE) + "</option>");
            });
            $target.html(items.join(""));
            $target.prop('disabled', false);

            if (!mobileAndTabletcheck()) $target.msDropdown().data("dd");
            $($target.data('target')).val('');
            //$('#order_form').data('formValidation').revalidateField($target.attr('name'));
            $target.trigger('change');
        });
    });

    $('.address-subdistrict-ddl').change(function (e) {
        $target = $($(this).data('target'));
        zipCode = $(this).find('option:selected').data('zipcode');
        $target.prop('disabled', true);
        $target.val("");
        $target.prop('disabled', false);
        $target.val(zipCode).trigger('change');
        if ($target.val() != '') $target.parent().removeClass('has-error'); //$('#order_form').data('formValidation').revalidateField($target.attr('name'));
    });

    $('.copy-address-holder').on('click', function (ev) {
        var isChecked = this.checked;
        var $target = $($(this).data('target'));
        var $source = $($(this).data('source'));
        var replaceText = $(this).data('replaceWith');
        if ($target.length > 0) {
            $target.find('input:text,select').each(function () {
                if (this.name.length > 0) {
                    $('#order_form').formValidation('enableFieldValidators', this.name, !isChecked, 'notEmpty');
                    var $elm = $source.find('[name="' + this.name.replace(replaceText, '') + '"]');
                    var $this = $(this);
                    $(this).prop('disabled', isChecked);
                    if ($(this).is('input:text')) {
                        //$(this).prop('disabled', isChecked);
                        if (isChecked) {
                            $elm.on('change', function () {
                                if ($this.is(':disabled') == false) return;
                                $this.val(this.value);
                            });
                            $this.val($elm.val());
                        } else {
                            //$(this).off('change');
                        }
                    } else {
                        $(this).prop('disabled', isChecked);
                        try { if (!mobileAndTabletcheck()) $(this).data('dd').set('disabled', isChecked); } catch (ex) { }
                        if (isChecked) {
                            var selected = $elm.val();
                            initDD($this, $elm, selected);
                            $elm.on('change', function () {
                                if ($this.is(':disabled') == false) return;
                                initDD($this, $elm, selected);
                            });
                        } else {
                            //if (!mobileAndTabletcheck()) $(this).msDropdown().data("dd");
                            //$elm.off('change');
                        }
                    }
                }
            })
        }
    });

    $('[name="ProductApplication[PromotionId]"]').on('click', function (ev) {
        var $coupon = $('[name="ProductApplication[CouponCode]"]');
        if (!$(this).hasClass('gift')) {
            $('.coupon_box').hide();
        } else {
            $('.coupon_box').show();
        }
    });

    $('[name="ProductApplication[PromotionId]"]').on('click', function (ev) {
        var $coupon = $('[name="ProductApplication[CouponCode]"]');
        if (!$(this).hasClass('gift')) {
            $('.coupon_box').hide();
        } else {
            $('.coupon_box').show();
        }
    });

    $('.radio-discount :radio').click(function () {
        $('[name="ProductApplication[CouponCode]"]').val('');
        $('div#error').html('');
    });

    $('[name="ProductApplication[CouponCode]"]').on('blur', function (ev) {
        if ($.trim(this.value).length == 0) $('div#error').html('');
    });

    $('.coupon_confirm').on('click', function (ev) {
        ev.preventDefault();
        var $chk = $('[name="ProductApplication[PromotionId]"]:checked');
        var $CouponCode = $('[name="ProductApplication[CouponCode]"]');
        if ($chk.length > 0 && $chk.hasClass('gift')) {
            if ($.trim($CouponCode.val()).length == 0) {
                $('div#error').html('<span style="color:red;">Coupon Code<span>'); return;
            }
            var url = BASEURL + 'api/Coupon?lang=' + LANGUAGE + '&couponCode=' + $CouponCode.val() + '&productId=' + application_product_id;
            $.getJSON(url, function (data) {
                if (data.Value != "200") {
                    $('div#error').html('<span style="color:red;">' + data.Name + '<span>');
                } else {
                    $('div#error').html('<span style="color:green;">' + data.Name + '<span>');
                }
            });
        }
    });

    $('[data-custom-alphanum="true"]').alphanum({
        allow: '',
    });

    $('[data-custom-numeric="true"]').numeric({
    });

    $('[data-custom-alphanum-address="true"]').alphanum({
        allow: '/,-,(,)',
    });

    $('[data-custom-alphanum-email="true"]').alphanum({
        allow: '@@',
    });

    $('[data-custom-alphanum-social="true"]').alphanum({
        allow: '@@,.',
    });

    $('#order_form input').filter(function () { return $(this).data('DateTimePicker'); }).each(function (o, i) {
        var $datePicker = $(this).data('DateTimePicker');
        var dateFormat = $datePicker.format();
        if (dateFormat == 'DD/MM/YYYY') {
            $(this).on('blur', function () {
                $datePicker.format(dateFormat);
            })
        }

        $(this).on('keydown', function () {
            return false;
        })
    });

    if ($('#sms-request-button').length > 0) {
        $('#otp-result input').prop('checked', false);
        $('#order_form').formValidation('revalidateField', 'otp-result');
        //OTP 6 digit
        $('#sms-request-button').click(function () {
            var $mobile_input = $('input[name="ProductApplicationPolicyHolders[0].MobilePhone"]');
            var $id_card_input = $('input[name="ProductApplicationPolicyHolders[0].IDCard"]');
            var mobile = $mobile_input.val();
            var id_card = $id_card_input.val();
            var app_id = $('input[name="ApplicationId"]').val();

            var error = [];

            if (id_card == '' || $id_card_input.closest('label').hasClass('has-error')) {
                $id_card_input.focus();
                error.push('- ' + $id_card_input.attr('placeholder'));
            }

            if (mobile == '' || $mobile_input.closest('.form-group').hasClass('has-error')) {
                $mobile_input.focus();
                error.push('- ' + $mobile_input.attr('placeholder'));
            }
            if (error.length > 0) {
                var msg = 'กรุณาใส่ข้อมูล:<br/>';
                if (LANGUAGE == 'EN') {
                    var msg = 'Please enter:<br/>';
                }
                $('#sms-request-message').html(msg + error.join('<br/>'));
                return false;
            } else {
                $('#sms-request-message').html('');
            }

            $('#sms-request-button').prop('disabled', false);
            $('.otp-verify-button').prop('disabled', false);
            $('.otp-send-again .otp-send-again-text').show();
            $('.otp-send-again .otp-send-sending-text').hide();

            sendOtp();

            $('.otp-message-error').hide();
            $('#application-mobile').blur();
            $.featherlight('#otp-popup', {
                afterContent: function (e) {
                    $('.featherlight-content .otp-password').click();
                }
            });

        });

        $('input[name="ProductApplicationPolicyHolders[0].MobilePhone"], input[name="ProductApplicationPolicyHolders[0].IDCard"]').keyup(function () {
            var check = $('#otp-result input').is(':checked');
            $('#sms-request-button').prop('disabled', false);
            if (check) {
                var is_change = false;
                if ($(this).attr('name') == 'ProductApplicationPolicyHolders[0].IDCard') {
                    is_change = ($(this).val() != verified_id);
                } else if ($(this).attr('name') == 'ProductApplicationPolicyHolders[0].MobilePhone') {
                    is_change = ($(this).val() != verified_mobile);
                }
                if (is_change) {
                    $('#otp-result input').prop('checked', false);
                    $('#order_form').formValidation('revalidateField', 'otp-result');
                }
            }
        });

        $('.otp-send-again').click(function () {
            sendOtp();
        });
    }

    if ($('.otp-verify-button').length > 0) {
        $('#form-otp').formValidation();
        $('.otp-verify-button').click(function () {
            if ($('#otp-verify-form label.has-success').length > 0) {
                $('.otp-verify-button').prop('disabled', true);
                var mobile = $('input[name="ProductApplicationPolicyHolders[0].MobilePhone"]').val();
                var id_card = $('input[name="ProductApplicationPolicyHolders[0].IDCard"]').val()
                var app_id = $('input[name="ApplicationId"]').val();
                var otp = $('.otp-password:last').val();
                var url = BASEURL + 'api/otp/verify';//?app=' + app_id + '&id=' + id_card + '&mobile=' + mobile + '&otp=' + otp;
                var param = { app: app_id, id: id_card, mobile: mobile, otp: otp };
                $.post(url, param, function (data) {
                    var result = (data.Success);
                    $('.otp-verify-button').prop('disabled', false);
                    if (result) {
                        verified_id = id_card;
                        verified_mobile = mobile;
                        $.featherlight.close();
                        $('.otp-message-error').hide();
                        $('#otp-result input').prop('checked', true);
                        $('#sms-request-button').prop('disabled', true);
                        $('#order_form').formValidation('revalidateField', 'otp-result');
                    } else {
                        $('.otp-message-error span').hide();
                        $('.otp-message-error-' + data.ErrorCode).show();
                        $('.otp-message-error').show();
                        $('#otp-result input').prop('checked', false);
                        if (data.ErrorCode == '998') {
                            $('.otp-verify-button').prop('disabled', true);
                        }
                    }
                }, "json");
            }
        });
    }


});


function sendOtp() {
    $('.otp-send-again').prop('disabled', true);
    $('.otp-send-again .otp-send-again-text').hide();
    $('.otp-send-again .otp-send-sending-text').show();
    $('.otp-send-again .fa').addClass('fa-spin');
    var mobile = $('input[name="ProductApplicationPolicyHolders[0].MobilePhone"]').val();
    var id_card = $('input[name="ProductApplicationPolicyHolders[0].IDCard"]').val();
    var app_id = $('input[name="ApplicationId"]').val();
    var url = BASEURL + 'api/otp/send'; //?app=' + app_id + '&id=' + id_card + '&mobile=' + mobile;
    var param = { app: app_id, id: id_card, mobile: mobile };
    $.post(url, param, function (data) {
        $('.otp-send-again .otp-send-again-text').show();
        $('.otp-send-again .otp-send-sending-text').hide();
        $('.otp-send-again .fa').removeClass('fa-spin');
        $('.otp-send-again').prop('disabled', false);
        $('.otp-verify-button').prop('disabled', false);
        $('.otp-message-error').hide();
        //if (data.Success) {
        //}
    }, "json");
}


function bind_msDropdown(obj) {
    $(obj).msDropdown({
        inherit_select_classes: true, on: {
            create: function (res) {
                var class_input = $(this).context.classList.value
                var element = $('#' + $(this)[0].id + '_msdd');
                if ($(element).parents('#fixed-and-float').length > 0) {
                    // $( element ).parents('.form-group').addClass(class_input);
                }
                else if ($(element).parents('.ng-pristine').length > 0) {
                    // $( element ).parents('.form-group').addClass(class_input);
                }


            },
        }
    }).data("dd required");
}

function initDD($this, $elm, selected) {
    if ($this !== undefined && $this.length > 0) {
        $this.html('');
        var options = $elm.find('option').map(function () {
            //console.log(this.value, selected);
            return this.outerHTML;
        });
        try { if (!mobileAndTabletcheck()) $this.data('dd').destroy(); } catch (ex) { }
        $this.html(options.toArray().join(' '));
        $this.find('option[value="' + $elm.val() + '"]').prop('selected', true);
        try { if (!mobileAndTabletcheck()) $this.msDropdown().data("dd"); } catch (ex) { }
    }
}



/*
if ($('.input_file_overflow').length > 0) {

}

function resizeImage() {
    var dataurl = null;
    var filesToUpload = document.getElementById('photo').files;
    var file = filesToUpload[0];

    // Create an image
    var img = document.createElement("img");
    // Create a file reader
    var reader = new FileReader();
    // Set the image once loaded into file reader
    reader.onload = function (e) {
        img.src = e.target.result;

        img.onload = function () {
            var canvas = document.createElement("canvas");
            var ctx = canvas.getContext("2d");
            ctx.drawImage(img, 0, 0);

            var MAX_WIDTH = 800;
            var MAX_HEIGHT = 600;
            var width = img.width;
            var height = img.height;

            if (height > width) { // flip orientation 
                MAX_HEIGHT = 800; MAX_WIDTH = 600;
            }

            if (width > maxWidth || height > maxHeight) {
                if (width > height) {
                    if (width > MAX_WIDTH) {
                        height *= MAX_WIDTH / width;
                        width = MAX_WIDTH;
                    }
                } else {
                    if (height > MAX_HEIGHT) {
                        width *= MAX_HEIGHT / height;
                        height = MAX_HEIGHT;
                    }
                }
            }
            
            canvas.width = width;
            canvas.height = height;
            var ctx = canvas.getContext("2d");
            ctx.drawImage(img, 0, 0, width, height);

            dataurl = canvas.toDataURL("image/jpeg");

            var blobBin = atob(dataurl.split(',')[1]);
            var array = [];
            for (var i = 0; i < blobBin.length; i++) {
                array.push(blobBin.charCodeAt(i));
            }
            var file = new Blob([new Uint8Array(array)], { type: 'image/png', name: "avatar.png" });


            //// Post the data
            //var fd = new FormData();
            //fd.append("name", "some_filename.jpg");
            //fd.append("image", file);
            //fd.append("info", "lah_de_dah");
            //$.ajax({
            //    url: '/ajax_photo',
            //    data: fd,
            //    cache: false,
            //    contentType: false,
            //    processData: false,
            //    type: 'POST',
            //    success: function (data) {
            //        $('#form_photo')[0].reset();
            //        location.reload();
            //    }
            //});
        } // img.onload
    }
    // Load files into file reader
    reader.readAsDataURL(file);
}
*/