var abs_url = 'http://192.168.0.89/html/2017/html_thanachart/';

var app = angular.module('myApp', []);

function preg_quote( str ) {
    // http://kevin.vanzonneveld.net
    // +   original by: booeyOH
    // +   improved by: Ates Goral (http://magnetiq.com)
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   bugfixed by: Onno Marsman
    // *     example 1: preg_quote("$40");
    // *     returns 1: '\$40'
    // *     example 2: preg_quote("*RRRING* Hello?");
    // *     returns 2: '\*RRRING\* Hello\?'
    // *     example 3: preg_quote("\\.+*?[^]$(){}=!<>|:");
    // *     returns 3: '\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:'

    return (str+'').replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
}


app.directive('myRepeatDirective', function() {
  return function($scope, element, attrs) {

    

  };
});

app.directive('imageonload', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.bind('load', function() {
                // alert('image is loaded');
            });
            element.bind('error', function(){
                // alert('image could not be loaded');
            });
        }
    };
});

app.directive('ngHtml', ['$compile', function ($compile) {
    return function (scope, elem, attrs) {
        if (attrs.ngHtml) {
            elem.html(scope.$eval(attrs.ngHtml));
            $compile(elem.contents())(scope);
        }
        scope.$watch(attrs.ngHtml, function (newValue, oldValue) {
            if (newValue && newValue !== oldValue) {
                elem.html(newValue);
                $compile(elem.contents())(scope);
            }
        });
    };
}]);


app.controller('Crtl_poi', function($scope, $http) {
      $scope.datalist = [];
      $scope.display = false;

      $scope.items = [ { label:'--- เลือกกลุ่มสถานที่ ---' , id:'0' }, { label:'โรงพยาบาล' , id:'1' } , { id:'2' ,label:'สถานนีตำรวจ' } , {id:'3', label:'อู่ซ่อมแซม'} ];
  		$scope.sample1 = $scope.items[1];
  		$scope.hospital_data = [ { label:'--- เลือกโรงพยาบาล ---' , id:'0' }, { label:'โรงพยาบาลพระราม9' , id:'1' }, { label:'โรงพยาบาลกรุงเทพ (พระราม9)' , id:'2' } ];
  		$scope.police_station_data = [ { label:'--- เลือกสถานีตำรวจ ---' , id:'0' }, { label:'สถานนีตำรวจ ห้วยขวาง' , id:'3' }, { label:'สถานนีตำรวจ บึงกุ่ม' , id:'4' } ];
  		$scope.garage_data = [ { label:'--- เลือกอู่ซ่อมมาตรฐาน ---' , id:'0' }, { label:'อู่นายช่าง เซอร์วิส' , id:'5' }, { label:'ร้าน เมืองทองยนตรการ' , id:'6' }, { label:'อู่อุดมการช่าง โดยนายอุดม เปรมสุข' , id:'7' } ];
  		$scope.data_items = $scope.hospital_data;
  		$scope.sample2 = $scope.data_items[0];

		$scope.update = function() {
      reset_ms_dropdown();
			if ( $scope.sample1.id == 1 ) {
        google_map_reset();
				$scope.data_items = $scope.hospital_data;
				$scope.sample2 = $scope.data_items[0];
			}
			else if ( $scope.sample1.id == 2 ) {
        google_map_call_police();
				$scope.data_items = $scope.police_station_data;
				$scope.sample2 = $scope.data_items[0];
			}
			else if ( $scope.sample1.id == 3 ) {
        google_map_call_garage();
				$scope.data_items = $scope.garage_data;
				$scope.sample2 = $scope.data_items[0];
			}
			else {
				$scope.data_items = $scope.hospital_data;
				$scope.sample2 = $scope.data_items[0];
        google_map_reset();
			}
		};

		$scope.update_map = function() {

			if ($scope.sample2.id > 0) {
				// google_map_clear($scope.sample2.id);
			}
			else {
				// google_map_reset();
			}
			
		};

      $scope.data_ajax_caller = function() {
          var Indata = {'csrf': $scope.scrf, 'perpage': 0, 'relate_id': $video_id};
          $http.post( json_data_url + "get_data/video_tag", Indata).
                  then(function (data, status, headers, config) {
                      //success
                      $scope.datalist = data.data.data_list;

                      if ( data.data.data_list.length > 0 ) {
                          $scope.display = true;
                      }
                      else {
                          $scope.display = false;
                      }
                   },
                     function (data, status, headers, config) { 
                      alert("error") 
                    }
            );
          }

});
