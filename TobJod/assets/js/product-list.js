var first_load = true;
var show_lead_form = false;
var lead_form_second = 0;

$(window).ready(function (e) {

    $('.panel-collapse').on('shown.bs.collapse', function () {
        $(this).prev().addClass('active');
    })
    $('.panel-collapse').on('hidden.bs.collapse', function () {
        $(this).prev().removeClass('active');
    })

});


$(document).ready(function () {
    var Compare = {
        item: {},
        add: function (obj) {
            var product_id = obj.data('product-id');
            Compare.item[product_id] = {
                image: obj.data('image'),
                price: obj.data('price'),
                name: obj.data('name')
            }

            var append_item = $('#compare-product-sticky .template').clone();
            append_item.attr('data-product-id', product_id);
            append_item.find('.name').html(obj.data('name'));
            append_item.find('.price').html(obj.data('price'));
            append_item.find('.image img').attr('src', obj.data('image'));
            append_item.find('.remove').attr('data-product-id', product_id);
            append_item.removeClass('hidden');
            append_item.removeClass('template');

            $('#compare-product-sticky .item-wrapper').append(append_item);
            $('#compare-product-sticky').removeClass('hidden');

        },

        delete: function (product_id) {
            delete Compare.item[product_id];
            $('#compare-product-sticky .compare-item[data-product-id="' + product_id + '"]').remove();
            if (Object.keys(Compare.item).length == 0) {
                $('#compare-product-sticky').addClass('hidden');
            }
        },

        initial: function () {

        }
    }

    $('.change-template-product button').on('click', function () {

        if ($(this).hasClass('active')) {
            return false;
        }
        $('.change-template-product button').toggleClass('active');

        var template = $(this).data('template');

        var $item_class = 'col-sm-12';
        var $image_class = 'col-sm-4';
        var $detail_class = 'col-sm-8 col-lg-8';

        $('#LayoutGridView').val((template == 'thumbnail' ? 'True' : 'False'));

        if (template === 'thumbnail') {
            $item_class = 'col-lg-4 col-sm-6';
            $image_class = 'col-sm-12';
            $detail_class = 'col-sm-12';

            $('#product-list').removeClass('view-list');
            setTimeout(function(){ $('#product-list .item-content').matchHeight({byRow: true, property: 'height', target: null, remove: false}); }, 300);
        } else {
            $('#product-list').addClass('view-list');
            setTimeout(function(){ $('#product-list .item-content').matchHeight({ remove: true }); }, 300);
        }

        // each product item
        $('#product-list .col').removeClass('col-sm-12');
        $('#product-list .col').removeClass('col-lg-4 col-sm-6');
        $('#product-list .col').addClass($item_class);

        // product image
        $('#product-list .item-content .thumb-img').removeClass('col-sm-12');
        $('#product-list .item-content .thumb-img').removeClass('col-sm-4');
        $('#product-list .item-content .thumb-img').addClass($image_class);

        // product detail
        $('#product-list .item-content .detail').removeClass('col-sm-12');
        $('#product-list .item-content .detail').removeClass('col-sm-8').removeClass('col-lg-8');
        $('#product-list .item-content .detail').addClass($detail_class);
    });

    $('#product-box').on('click', '.compare-select', function (e) {
        e.preventDefault();

        if ($(this).hasClass('active')) {
            Compare.delete($(this).data('product-id'));
        } else {
            var len = Object.keys(Compare.item).length;
            if (len >= 3) return false;

            Compare.add($(this));
        }

        $(this).toggleClass('active');
        var pr = $(this).parents('.item-content').parent('.col');
        pr.toggleClass('active');
    });

    $('#compare-product-sticky').on('click', '.remove', function () {
        var $item = $('#product-list .item-content[data-product-id="' + $(this).data('product-id') + '"]');
        $item.parent('.col').removeClass('active');
        $item.find('.compare-select').removeClass('active');

        Compare.delete($(this).data('product-id'));
    });

    $('.form-footer button').click(function () {
        $('.form-footer button').toggleClass('close-form');
        $('#form').slideToggle(500, function () {

            var button = $('.form-footer button');
            if ($('.form-footer button').hasClass('close-form')) {
                button.text(button.data('show-text'));
            } else {
                button.text(button.data('hide-text'));
            }
        });
    })
    $('#btn-compare').click(function () {
        var id = [];
        $('.compare-item').each(function() {
            id.push($(this).data('product-id'));
        });
        id.splice(0, 1);
        var page = $('#product-box').data('page');

        var url = $(this).data('url') + '?id=' + id.join(',') + '&listpage=' + page + '&' + $("#product-search").serialize();
        if (show_lead_form) {
            showLeadForm(url);
        } else {
            window.location = url;
        }
        //$(this).attr('href', );
    });
    $('#sorting a').click(function () {
        //$('#sorting a.active').removeClass('active');
        //$(this).addClass('active');
        var sort = $(this).data('id');
        if (sort == '1') {
            $(this).data('id', '2');
            $(this).text($(this).data('title-2'));
        } else {
            $(this).data('id', '1');
            $(this).text($(this).data('title-1'));
        }
        $('#sort').val(sort);
        LoadProductList(true);
    });
    $('#product-list-load-more').hide();

    //if ($('#product-list .item-content').length > 0) {
    //    console.log('x');
    //    $('#product-list .item-content').matchHeight({ byRow: true, property: 'height', target: null, remove: false });
    //}
});

//$('#product-search').submit(function (e) {
//    e.preventDefault();
//    LoadProductList(true);
//});

$('.search-advance:checkbox').click(function () {
    var name = $(this).data('target');
    var val = 0;
    $.each($('.search-advance[data-target=' + name + ']:checked'), function (i, e) {
        val += parseInt($(e).data('value'));
    });
    $('#search-' + name).val(val);
    LoadProductList(true);
});
//if ($('.driverage-radio').length > 0) {
//    $('.driverage-radio').click(function () {
//        $('#driverage').attr('disabled', $('.driverage-radio:checked').val() != '1');
//    });
    $('#driverage').change(function () {
        $('#search-driverage').val($(this).val());
        LoadProductList(true);
    });
//}

if ($("#price_range").length > 0) {
    $("#price_range").ionRangeSlider({
        type: "double",
        onFinish: function (data) {
            $('#search-price').val(data.from + ';' + data.to);
            LoadProductList(true);
        }
    });
}

$(window).scroll(function () {
    clearTimeout($.data(this, 'scrollTimer'));
    $.data(this, 'scrollTimer', setTimeout(function () {
        if ($('#product-list-load-more').length > 0 && $('#product-list-load-more').is(':visible') && isScrolledIntoView($('#product-list-load-more'))) {
            LoadProductList(false);
        }
    }, 200));
});

$('.btn-product-reset').click(function (e) {
    e.preventDefault();
    $form = $(this).closest('form');
    $form.find('input').val('');
    $form.find('select').val('');
    $form.find('input:radio[data-default]').click();

    if (!mobileAndTabletcheck()) {
        $form.find('select').each(function (i, e) {
            try { $(e).msDropdown().data("dd").set("selectedIndex", 0); } catch (e) {}
        });
    }
});

function LoadProductList(clearList, callback) {
    $.blockUI();
    var listPage = parseInt($('#product-box').data('list-page'));
    if (clearList) {
        $('#product-box').data('page', 0);
        $('#product-box').data('list-page', 0);
    }
    var page = parseInt($('#product-box').data('page')) + 1;
    var url = $('#product-box').data('load-url') + '?p=' + page + '&listpage=' + listPage + '&' + $("#product-search").serialize();
    $.get(url, function (data) {
        if (data.length < 200) {
            if (clearList) {
                $('#product-box').html('');
                //$('.product-filter').hide();
                $('#product-not-found').show();
                setTimeout(function () {
                    $('html,body').animate({ scrollTop: $('#page-product-list #product-container').offset().top }, 1000);
                }, 500);
            }
            $('#product-list-load-more').hide();
        } else {
            $('#product-not-found').hide();
            if (clearList) {
                $('#product-box').html(data);
                checkLayout($('#LayoutGridView').val() == 'true');
            } else {
                $('#product-box').append(data);
            }
            $('#product-box').data('page', $('#product-box').find('input[name=page]:last').val());
            var count = $('#product-box').find('input[name=count]:last').val();
            if (count < 6) {
                $('#product-list-load-more').hide();
            } else {
                $('#product-list-load-more').show();
            }
            if (page == 1) {
                setTimeout(function () {
                    $('html,body').animate({ scrollTop: $('#page-product-list #product-container').offset().top }, 1000);
                }, 500);
            }
            $('#product-list .item-content').matchHeight({ byRow: true, property: 'height', target: null, remove: false });
        }

        if (callback !== undefined && callback.length > 0) {
            for (var i = 0; i < callback.length; i++) {
                $('button[data-product-id="' + callback[i] + '"]').click();
            }
        }

        setTimeout(function () {
            showLeadForm('');
        }, (lead_form_second * 1000));

        $.unblockUI();
    });
}

if ( $('.display-view .display-wrapper button').length > 0 ) {
    $('.display-view .display-wrapper button').click(function(e){
        //check_element_p_list();
    });
}


function check_element_p_list()
{
    if ( $('.p-thumbnail.active').length <= 0 ) {
        $('#product-list .detail').each(function(index, ele){
            //if ( $(ele).hasClass('col-lg-12') ) {
                //$(ele).removeClass('col-lg-12').addClass('col-lg-8').removeClass('col-md-12').addClass('col-md-8').removeClass('col-sm-12').addClass('col-sm-8');
            //}
        });
    }
    else {
        $('#product-list .detail').each(function (index, ele) {
            //if ( $(ele).hasClass('col-lg-8') ) {
                //$(ele).removeClass('col-lg-8').addClass('col-lg-12').removeClass('col-md-8').addClass('col-md-12').removeClass('col-sm-8').addClass('col-sm-12');
            //}
        });
    }
}

function checkLayout(gridView) {
    if (first_load && gridView) {
        $('#product-list').removeClass('view-list');
        $('.p-list').addClass('active');
        $('.p-thumbnail').removeClass('active');
    }
    
    first_load = false;
}

//$('.btn-product-previous').click(function () {
//    historyBackWFallback($(this).attr('href'));
//    return false;
//});

function historyBackWFallback(fallbackUrl) {
    fallbackUrl = fallbackUrl || '/';
    var prevPage = window.location.href;

    window.history.go(-1);

    setTimeout(function () {
        if (window.location.href == prevPage) {
            window.location.href = fallbackUrl;
        }
    }, 500);
}

function bindLeadForm(seconds) {
    show_lead_form = true;
    lead_form_second = seconds;
    
    $('#product-list').on('click', '.btn-product-detail', function () {
        if (show_lead_form) {
            showLeadForm($(this).attr('href'));
            return false;
        }
    });
}

function showLeadForm(return_url) {
    if (show_lead_form) {
        show_lead_form = false;
        $('#lead_form_return_url').val(return_url);
        $("#lead-form-popup").modal("show");
    }
}
