﻿
$(window).scroll(function () {
    clearTimeout($.data(this, 'scrollTimer'));
    $.data(this, 'scrollTimer', setTimeout(function () {
        if ($('#news-list-load-more').length > 0 && $('#news-list-load-more').is(':visible') && isScrolledIntoView($('#news-list-load-more'))) {
            LoadNewsList(false);
        }
    }, 200));
});

function LoadNewsList(clearList) {
    $.blockUI();
    if (clearList) $('#news-box').data('page', 0);
    var page = parseInt($('#news-box').data('page')) + 1;
    var columnSize = ($('.filterContent .gridView').hasClass('active') ? 4 : 12);
    var url = $('#news-box').data('load-url') + '?p=' + page;
    if ($('#Search').length > 0) url = url + '&search=' + $('#Search').val(); 
    $.getJSON(url, function (json) {
        if (json.data.length === 0) {
            if (clearList) {
                $('#news-box').html('');
                $('#items-not-found').show();
            }
            $('#news-list-load-more').hide();
        } else if (json.page >= page) {
            $('#items-not-found').hide();
            $('#news-box').data('page', json.page);
            var itemTpl = $('script[data-template="newslistitem"]').text().split(/\$\{(.+?)\}/g);

            if (clearList) $('#news-box').html('');

            $('#news-box').append(json.data.map(function (item) {
                item['columnSize'] = columnSize;
                return itemTpl.map(renderTemplate(item)).join('');
            }));

            if (json.data.length >= 6) {
                $('#news-list-load-more').show();
            } else {
                $('#news-list-load-more').hide();
            }
            setTimeout(function(){ $('#news-box .item > a').matchHeight({ byRow: true, property: 'height', target: null, remove: false }); }, 300);
        }
        $.unblockUI();
    });
}

$(function () {
    LoadNewsList(true);
});