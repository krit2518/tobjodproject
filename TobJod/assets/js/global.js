window.mobileAndTabletcheck = function() {
  // Source From https://stackoverflow.com/questions/11381673/detecting-a-mobile-browser/#answer-11381730
  var check = false;
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
  return check;
};


$('.car-brand-ddl').change(function (e) {
    $target = $($(this).data('target'));
    $target.prop('disabled', true);
    //var url = BASEURL + 'api/Car/Family?lang=' + LANGUAGE + '&brandId=' + $(this).val();
    var brandId = $(this).val();
    try { localStorage.setItem("brandId", brandId); } catch (err) { };
    var url = BASEURL + 'api/Car/Family?lang=' + LANGUAGE + '&brandId=' + brandId;
    $.getJSON(url, function (data) {

        if ( ! mobileAndTabletcheck()) $target.msDropdown().data("dd").destroy();
        var items = [];
        items.push('<option value="">' + $target.find('option:first').text() + '</option>');
        $.each(data, function (key, val) {
            items.push("<option value='" + val.Value + "'>" + val.Name + "</option>");
        });
        $target.html(items.join(""));
        $target.prop('disabled', false);
        try { localStorage.setItem('familyItems', items); } catch (err) { }
        if ( ! mobileAndTabletcheck()) $target.msDropdown().data("dd");
    });
});

$('.car-family-ddl').change(function (e) {
    $target = $($(this).data('target'));
    $target.prop('disabled', true);
    //var url = BASEURL + 'api/Car/Model?lang=' + LANGUAGE + '&familyId=' + $(this).val();
    var familyId = $(this).val();
    try { localStorage.setItem("familyId", familyId); } catch (err) { };
    var url = BASEURL + 'api/Car/Model?lang=' + LANGUAGE + '&familyId=' + familyId;

    $.getJSON(url, function (data) {

        if ( ! mobileAndTabletcheck()) $target.msDropdown().data("dd").destroy();
        var items = [];
        items.push('<option value="">' + $target.find('option:first').text() + '</option>');
        $.each(data, function (key, val) {
            items.push("<option value='" + val.Value + "'>" + val.Name + "</option>");
        });
        $target.html(items.join(""));
        $target.prop('disabled', false);
        try { localStorage.setItem('modelItems', items); } catch (err) { }
        if (!mobileAndTabletcheck()) $target.msDropdown().data("dd");
    });
});

$('#side_car_model').on('change', function () {
    var modelId = $(this).val();
    try { localStorage.setItem("modelId", modelId); } catch (err) {}
});

$('.ta-trip-type').change(function () {
    $target = $($(this).data('target'));
    //$target_hidden = $($(this).data('target-hidden'));
    if ( ! mobileAndTabletcheck()) $target.msDropdown().data("dd").destroy();
    if ($(this).val() == $(this).data('value')) {
        $target.val('');//$(this).data('target-value'));
        $target.prop('disabled', true);
        //$target_hidden.val($(this).data('target-value')).prop('disabled', false);
    }
    else {
        $target.prop('disabled', false);
        //$target_hidden.val('').prop('disabled', true);
    }
    var ta_cov_first_load = true;
    if ( ! mobileAndTabletcheck()) $target.msDropdown().data("dd");
});

$('.ta-coverage-type').change(function (e) {
    var ta_cov_first_load = true;
    $target = $($(this).data('target'));

    //$target_hidden = $($(this).data('target-hidden'));
    // if ( ! mobileAndTabletcheck()) $target.msDropdown().data("dd").destroy();

    if ($(this).val() == $(this).data('value')) {
        $target.val($(this).data('target-value'));
        $target.prop('disabled', true);
        //$target_hidden.val($(this).data('target-value')).prop('disabled', false);
    }
    else {
        $target.prop('disabled', false);
        if ( ! ta_cov_first_load) {
            
            $target.val('1');
        }
        //$target_hidden.val('').prop('disabled', true);
    }
    ta_cov_first_load = false;
    if ( ! mobileAndTabletcheck()) $target.msDropdown().data("dd");
});


$('.input-combined-with').bind("keyup change", function () {
    var val = parseInt($(this).val());//.replace(/,/g,''));
    var valPlus = parseInt($($(this).data('combined-with')).val());//.replace(/,/g, ''));
    if (isNaN(val)) val = 0;
    if (isNaN(valPlus)) valPlus = 0;
    var total = val + valPlus;
    if (isNaN(total)) total = 0;
    $($(this).data('target')).val(total.formatNumber());
});

$('#form-subscribe').submit(function (e) {
    e.preventDefault();

    if (isValidEmailAddress($('#email_subscribe').val())) {
        var $form = $(this),
            url = $form.attr('action');

        var posting = $.post(url, { email: $('#email_subscribe').val() });
        
        posting.done(function (data) {
            if (data > 0) {
                $('#email_subscribe').val('');
                $('#popup-model').modal('toggle');
            }
        });
    }
    else {
        $('footer #footer-row-two .subscribe').addClass('Error');
    }
});

/**
 * Number.prototype.format(n, x)
 * 
 * @param integer n: length of decimal
 * @param integer x: length of sections
 */
Number.prototype.formatNumber = function (n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};

function renderTemplate(props) {
    return function (tok, i) {
        return (i % 2) ? props[tok] : tok;
    };
}

function calculateAge(birthdate) {
    var data = birthdate.split('/');
    //var x = (variable_config.server_year - data[1]);
    var x = Math.round(moment().diff(moment([data[1], data[0] - 1]), 'years', true));
    return (isNaN(x) ? '' : x);
}

function calculateDateDiff(date_from, date_to) {
    var timeDiff = 0
    if (date_to) {
        timeDiff = (date_to - date_from);// / 1000;
    }
    return Math.ceil(timeDiff / (24 * 60 * 60 * 1000)) + 1;
}   

$(function () {
    $('input[data-alphanum], textarea[data-alphanum]').alphanum({ allow: '.%' });
    $('input[data-alpha], textarea[data-alpha]').alpha({ allow: '', allowOtherCharSets: true });
    $('input[data-num], textarea[data-num]').numeric({ allow: '', allowMinus: false, maxLength: 10 });
    //$('input[data-number]').mask('###,###,##0', { reverse: true });

    $('body').on('keypress', 'input[type=number][maxlength]', function (event) {
        var key = event.keyCode || event.charCode;
        var charcodestring = String.fromCharCode(event.which);
        var txtVal = $(this).val();
        var maxlength = $(this).attr('maxlength');
        var regex = new RegExp('^[0-9]+$');
        // 8 = backspace 46 = Del 13 = Enter 39 = Left 37 = right Tab = 9
        if (key == 8 || key == 46 || key == 13 || key == 37 || key == 39 || key == 9) {
            return true;
        }
        // maxlength allready reached
        if (txtVal.length == maxlength) {
            return false;
        }
        // pressed key have to be a number
        if (!regex.test(charcodestring)) {
            return false;
        }
        return true;
    });

    $(".range-slider").each(function (i, e) {
        $(e).ionRangeSlider({
            type: "double",
            min: $(this).data('min'),
            max: $(this).data('max'),
            from: $(this).data('from'),
            to: $(this).data('to'),
            step: 100,
            min_interval: (parseInt($(this).data('max')) > 10000 ? 1000 : 100),
            prettify_enabled: true,
            prettify_separator: ","
        });
    });

    $('.birthday-age').datetimepicker({ format: 'MM/YYYY', maxDate: new Date(), minDate: moment().startOf('month').subtract(99, 'y') , ignoreReadonly: true})
        .on('dp.change', function (event) {
        var data = $(this).val().split('/');
        $($(this).data('target')).val(calculateAge($(this).val()));
        $(this).closest('form').formValidation('revalidateField', $(this).attr('name'));
        $(this).closest('form').formValidation('revalidateField', $($(this).data('target')).attr('name'));
    }).on('dp.show', function (event) {
        return true;
    });

    var tomorrow = moment().startOf('day').add(1, 'days');
    $('.date-from').each(function (i, e) {
        //if ( ! mobileAndTabletcheck() ) {
            var val = $(e).val();
            try {
                if (val != '') {
                    var vals = val.split('/');
                    var d = vals[2] + '-' + vals[1] + '-' + vals[0];
                    $(e).datetimepicker({ format: 'DD/MM/YYYY', ignoreReadonly: true, allowInputToggle: true, minDate: tomorrow, useCurrent: false });
                    return;
                }
            } catch (err) { }
            $(e).datetimepicker({ format: 'DD/MM/YYYY', ignoreReadonly: true, allowInputToggle: true, minDate: tomorrow });
        //}
    });
    
    //if ( ! mobileAndTabletcheck() ) {
        $('.date-to').datetimepicker({ format: 'DD/MM/YYYY', ignoreReadonly: true, allowInputToggle: true, useCurrent: false }); //Important! See issue #1075

        $(".date-from").on("dp.change", function (e) {
            var $dateto = $($(this).data('date-to'));
            var $target = $($(this).data('target'));
            var $targetSpan = $($(this).data('target-span'));
            $dateto.data("DateTimePicker").minDate(e.date.startOf('day'));
            var diff = calculateDateDiff($(this).data("DateTimePicker").date().startOf('day'), $dateto.data("DateTimePicker").date());
            $target.val(diff);
            $targetSpan.text(diff);
            $(this).closest('form').formValidation('revalidateField', $(this).attr('name'));
        });

        $(".date-to").on("dp.change", function (e) {
            var $datefrom = $($(this).data('date-from'));
            var $target = $($(this).data('target'));
            var $targetSpan = $($(this).data('target-span'));
            $datefrom.data("DateTimePicker").maxDate(e.date);
            var diff = calculateDateDiff($datefrom.data("DateTimePicker").date().startOf('day'), $(this).data("DateTimePicker").date().startOf('day'));
            $target.val(diff);
            $targetSpan.text(diff);
            $(this).closest('form').formValidation('revalidateField', $(this).attr('name'));
        });
    //}

    $('.birthday-age').each(function (e) {
         $($(this).data('target')).val(calculateAge($(this).val()));
    });

    //if ( ! mobileAndTabletcheck() ) {
        $(".date-from").each(function (e) {
            var $dateto = $($(this).data('date-to'));
            var $target = $($(this).data('target'));
            var $targetSpan = $($(this).data('target-span'));
            try { $dateto.data("DateTimePicker").minDate($(this).data("DateTimePicker").date().startOf('day')); } catch (err) { }
            //$(this).data("DateTimePicker").minDate($(this).data("DateTimePicker").date().startOf('day'));
            if ($dateto.val() == '') {
                $target.val('0');
                $targetSpan.text('0');
            } else {
                var diff = calculateDateDiff($(this).data("DateTimePicker").date(), $dateto.data("DateTimePicker").date());
                $target.val(diff);
                $targetSpan.text(diff);
            }
        });
    //}

    $('.input-combined-with').change();
    $('.ta-trip-type, .ta-coverage-type').each(function (i, e) { if ($(e).val() != '') $(e).change(); });
    $('.car-brand-ddl').each(function (i, e) { if ($(e).val() != '' && $($(e).data('target')).val() == '') $(e).change(); });
    $('.car-family-ddl').each(function (i, e) { if ($(e).val() != '' && $($(e).data('target')).val() == '') $(e).change(); });
    $('#fixed-and-float .inner .category-list .item').click(function () { $('#fixed-and-float .inner .category-list .item.active').removeClass('active'); $(this).addClass('active'); });

    var tag_suggest = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: BASEURL+LANGUAGE+'/home/searchsuggest/?search=%QUERY',
            wildcard: '%QUERY'
        }
    });

    $('.typeahead').typeahead(null, {
        name: 'search',
        source: tag_suggest
    });
});

$(function () {

    if ($('div.search-section-content select[name=brand]').val() > 0) {
        if ($('#side_car_family').length > 0 && $('#side_car_family option').length == 1) {
            var familyId = localStorage['familyId'];
            var items = localStorage['familyItems'];
            if (items && items.length > 0 && familyId > 0) {
                $('#side_car_family').html(items);
                $('#side_car_family').val(familyId);
            }
        }

        if ($('#side_car_model').length > 0 && $('#side_car_model option').length == 1) {
            var modelId = localStorage['modelId'];
            var items = localStorage['modelItems'];
            if (items && items.length > 0 && familyId > 0) {
                $('#side_car_model').html(items);
                $('#side_car_model').val(modelId);
            }
        }
    }
})

var calhome=function() {
    $('.calpopup').click(function(){
        $('#homecontent').featherlight();
    })


    
    $('#butclosed').click(function(){
        var current = $.featherlight.current();
        current.close();
    })
}

function calculateheightbox () {
    var tabmotor = $('#promotable').height();
    $('.motortransform-active').height(tabmotor);
}

$("#showbutt").click(function() {
    $('.motortransform').toggleClass('motortransform-active');
    $('.showbutt').html("ซ่อนรายละเอียดเงื่อนไขโปรโมชั่น");
    if(!$('.motortransform').hasClass('motortransform-active')) { 
        $('.box').height(0);
        $('.showbutt').html("คลิกดูรายละเอียดเงื่อนไขโปรโมชั่น");
    }
    else {
        calculateheightbox();
    }
    });

    calculateheightbox();

(function($) {

    var resizeTimer; // Set resizeTimer to empty so it resets on page load

    function resizeFunction() {
        calculateheightbox();
    };

    // On resize, run the function and reset the timeout
    // 250 is the delay in milliseconds. Change as you see fit.
    $(window).resize(function() {
        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(resizeFunction, 300);
    });

})(jQuery);