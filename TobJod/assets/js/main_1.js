function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
var _variable_1 = {'_1_':50,'_2_':70};
var bLazy = [];
var tools = [];
var scrollPos = '';

$(window).scroll(function(){
    scrollPos = $(document).scrollTop();
});
var savedScrollPos = scrollPos;

function is_iOS() {
  var iDevices = [
    'iPad Simulator',
    'iPhone Simulator',
    'iPod Simulator',
    'iPad',
    'iPhone',
    'iPod'
  ];
  while (iDevices.length) {
    if (navigator.platform === iDevices.pop()){ return true; }
  }
  return false;
}

/*
| -------------------------------------------------------------------------
| 20180330 - Ake
| -------------------------------------------------------------------------
*/
function scroll_to_element(ele) {
	if ( $(ele).length > 0 ) {
		$('html, body').animate({
	        scrollTop: $(ele).offset().top
	    }, 600);
	}
}
/*
| -------------------------------------------------------------------------
| 20180330 - Ake
| -------------------------------------------------------------------------
*/

$.fn.digits = function() {
    return this.each(function() {
        $(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") ); 
    })
}

function back_to_top_function() {
	if ( $(window).scrollTop() <= 300 ) {
		$('.fix-back-to-top').removeClass('active');
	}
	else {
		$('.fix-back-to-top').addClass('active');
	}
}

function top_phone_function() {
	//if ( $(window).scrollTop() <= 300 ) {
	//	$('header #row-one #menu-lv1 .top-phone1').hide();
	//}
	//else { 
	//	$('header #row-one #menu-lv1 .top-phone1').show();
	//}
}

function lazyLoad(s) {
    var d = window.document,
        b = d.body, /* appends at end of body, but you could use other methods to put where you want */
        e = d.createElement("script");
    
    e.async = true;
    e.src = s;
    b.appendChild(e);
}

function key_digit(e) {
	var KeyCode = (e.keyCode ? e.keyCode : e.which);
	var CharCode = (e.charCode ? e.charCode : 0);
	
	return ((KeyCode == 8) // backspace
		|| (KeyCode == 9) // tab
		|| (KeyCode == 37) // left arrow
		|| (KeyCode == 39) // right arrow
		|| ((KeyCode == 46) && (CharCode == 0)) // delete
		|| (CharCode == 0)
		|| ((KeyCode > 47) && (KeyCode < 58)) // 0 - 9
	);
}

function isValidEmailAddress(emailAddress) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

    return re.test(emailAddress);
}

window.mobilecheck = function() {
  // Source From https://stackoverflow.com/questions/11381673/detecting-a-mobile-browser/#answer-11381730
  var check = false;
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
  return check;
};

window.mobileAndTabletcheck = function() {
  // Source From https://stackoverflow.com/questions/11381673/detecting-a-mobile-browser/#answer-11381730
  var check = false;
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
  return check;
};


function call_tab_mobile_js_validate() {
	if ( mobileAndTabletcheck() ) {
		$('select.required').each( function( index, ele ) {
			$(ele).attr('required','required');
		} );
	}
}

function english_mail_format_only( event ) {
        var ew = event.which;

        if(ew === 45 || ew === 8 || ew === 0 || ew === 116 || ew === 46 || ew === 64 || ew === 13 )
            return true;
        if(48 <= ew && ew <= 57)
            return true;
        if(65 <= ew && ew <= 90)
            return true;
        if(97 <= ew && ew <= 122)
            return true;
        if(ew === 95)
            return true;
        return false;
}

function search_result_function_calculate() {
   if ( $('main.search_result').length > 0) {

   		if ($(window).width() <= 680) {
   			$('main #newsSection').removeClass('listView');
   		}
   		else {
   			$('main #newsSection').addClass('listView');
   		}	
   }
}

function auto_update_height() {
	if ( $('header #row-one #menu_float_right').length > 0 ) {
		$('header #row-one #menu_float_right').height( $(window).height() );
	}	
}

tools.load_lazy = function () {
		bLazy = new Blazy({
	        src: 'data-original',
	        selector:"img.lazy, img.blazy",
	        container: '.cat-item-wrapper, .slick-list.draggable, .slick-track, .fancybox-inner, .slider, .slick-track, .owl-stage',
	        loadInvisible: true,
	        validatezDelay: 30,
	        success: function(element) {
	            setTimeout(function() {
	            // We want to remove the loader gif now.
	            // First we find the parent container
	            // then we remove the "loading" class which holds the loader image
	            var parent = element.parentNode;
	            parent.className = parent.className.replace(/\bloading\b/,'');
	            }, 200);
	            var $ele = $(element);
	            $ele.addClass('blazy_success');
	            if( $('.gallery-view img').length > 0 ) {
	                // $ele.attr( 'src' , $ele.attr('data-original') );
	            }
	        }
	        , error: function(ele, msg) {
	            var $ele = $(ele);
	            if ( $ele.attr('deleted-permitted') == 1 ) {
	                $ele.remove();
	            }
	            else if(msg === 'missing') {
	                // Data-src is missing
	                $ele.attr('src', $ele.attr('error_src') );
	            }
	            else if(msg === 'invalid') {
	                // Data-src is invalid
	                $ele.attr('src', $ele.attr('error_src') );
	            }
	            else {
	                $ele.attr('src', $ele.attr('error_src') );
	            }
	        }
	});
}

function call_function_top_icon() {
		if ( $(window).width() <= 480 ) {
			hide_logo_top();
		}
		else if ($(window).width() > 480 && $(window).width() < 900 ) {
			hide_logo_top();
		}
}
var element_dd = [];
var couter = 0;
function reset_ms_dropdown() {
	$.each( element_dd, function( key, value ) {
	  	value[0].destroy();
	});

	setTimeout(function(){ 
		dropdown_poi();
	}
	,200);
}

function Initialized_ms_dropdown() {
	if ( ! mobileAndTabletcheck() ) {
			$(".dd_list").msDropdown( { inherit_select_classes: true, on : {create:function(res) {
		        var class_input = $(this).context.classList.value
		        var element = $( '#'+$(this)[0].id+'_msdd' );
		        if ( $(element).parents('#fixed-and-float').length > 0) {
		            // $( element ).parents('.form-group').addClass(class_input);
		        }
		        else if( $(element).parents('.ng-pristine').length > 0 ) {
		        	// $( element ).parents('.form-group').addClass(class_input);
		        }


		    },} } ).data("dd required");
	}
	dropdown_poi();

}

function dropdown_poi() {

	if ( ! mobileAndTabletcheck() ) {
		couter = 0;
			$(".dd_list2").msDropdown( { inherit_select_classes: true, on : {create:function(res) {
		        var class_input = $(this).context.classList.value
		        var element = $( '#'+$(this)[0].id+'_msdd' );
		        if ( $(element).parents('#fixed-and-float').length > 0) {
		            $( element ).parents('.form-group').addClass(class_input);
		        }

		        element_dd[couter++] = $(this);

	    },} } ).data("dd required");
	}
}

function get_a_gap() {
	var $_width_1 = 0;
	if ( $(window).width() > 1000 && $(window).width() <= 1280 ) {
		$_width_1 = 40;
	}
	
	return $_width_1;
}

$(window).load(function() {

	setTimeout(function(){
		if ( $('body.opacity').length > 0) {
				$('body.opacity').removeClass('opacity');
		}
	}, 1000);

	call_tab_mobile_js_validate();
	Initialized_ms_dropdown();

	if ( $('.owl-carousel9.opa').length > 0 ) {
		$('.owl-carousel9.opa').removeClass('opa');
	}

	if ( $('label.form-group.form-control').length > 0 ) {
		$('label.form-group.form-control').removeClass('form-control');
	}

	if ( $('#fixed-and-float').length > 0 ) {
		$('#fixed-and-float .category-list a').click(function(e){
			$('.mobile_search_newform .form-container .form-item').hide();
			$('#fixed-and-float .form-container > .form-item').hide();
			$('.' + $(this).attr('relate-data') ).show().css('display','block');
		});
		$('#fixed-and-float .category-list a').eq(0).click();
	}

	if ( $('.mobile_search_newform a.item').length > 0 ) {
		$('.mobile_search_newform a.item').click(function(e){
			$('.mobile_search_newform .form-container .form-item').hide();
			$('#fixed-and-float .form-container > .form-item').hide();
			$('.' + $(this).attr('relate-data') ).show().css('display','block');

			var _ko = $('.' + $(this).attr('relate-data') + ' form').height();
			$('.mobile_search_newform .filter-container').height( ( _ko+_variable_1._2_+( get_a_gap() > 0 ? get_a_gap() : 0 )) );
			$('.mobile_search_newform .form-container .form-item').height( ( _ko+_variable_1._1_) );
		});
	}

	if ( $('main.search_result').length > 0 ) {
		$('main.search_result #newsSection').removeClass('opa');
	}

	setTimeout( function(e){
		tools.load_lazy();
	} ,500);

	call_function_top_icon();
	update_height_banner_product_list();
	setTimeout(function(){

				if ($(".owl-carousel8").length > 0) {
					var duration = $(".owl-carousel8").data('duration');

					if ($('.owl-carousel8 .item').length > 2) {
						var $config_obj_1 = {
					            nav: true, navText: ["<img src='"+BASEURL+"assets/images/spacer.gif'>", "<img src='"+BASEURL+"assets/images/spacer.gif'>"], items: 3, onInitialized: function () {
					                if ($('.owl-carousel8 .item').length > 0) {
					                    $('.owl-carousel8 .owl-nav .owl-prev').html('<i class="sprite owl-carousel8-prev"></i>');
					                    $('.owl-carousel8 .owl-nav .owl-next').html('<i class="sprite owl-carousel8-next"></i>');
					                }
					            },
					            responsiveClass: true,
					            responsive: {
					            	320: {
					                    items: 1,
					                    nav: false,
					                },
					            	767: {
					                    items: 1,
					                    nav: false,
					                },
					            	768: {
					                    items: 3,
					                    nav: false,
					                },
					            	
					            	992: {
					                    items: 3,
					                    nav: true,
					                },
					                
					                
					            },
					            autoplay: false,
					            autoplayTimeout: duration,
					            loop: false,
					    };
					}
					else if ($('.owl-carousel8 .item').length > 1) {
						var $config_obj_1 = {
					            nav: true, navText: ["<img src='"+BASEURL+"assets/images/spacer.gif'>", "<img src='"+BASEURL+"assets/images/spacer.gif'>"], items: 2, onInitialized: function () {
					                if ($('.owl-carousel8 .item').length > 0) {
					                    $('.owl-carousel8 .owl-nav .owl-prev').html('<i class="sprite owl-carousel8-prev"></i>');
					                    $('.owl-carousel8 .owl-nav .owl-next').html('<i class="sprite owl-carousel8-next"></i>');
					                }
					            },
					            responsiveClass: true,
					            responsive: {
					            	320: {
					                    items: 1,
					                    nav: false,
					                },
					            	767: {
					                    items: 1,
					                    nav: false,
					                },
					            	768: {
					                    items: 2,
					                    nav: false,
					                },
					            	992: {
					                    items: 2,
					                    nav: true,
					                },
					            },
					            autoplay: false,
					            autoplayTimeout: duration,
					            loop: false,
					    };
					}
					else {
						var $config_obj_1 = {
					            nav: true, navText: ["<img src='"+BASEURL+"assets/images/spacer.gif'>", "<img src='"+BASEURL+"assets/images/spacer.gif'>"], items: 1, onInitialized: function () {
					                if ($('.owl-carousel8 .item').length > 0) {
					                    $('.owl-carousel8 .owl-nav .owl-prev').html('<i class="sprite owl-carousel8-prev"></i>');
					                    $('.owl-carousel8 .owl-nav .owl-next').html('<i class="sprite owl-carousel8-next"></i>');
					                }
					            },
					            responsiveClass: true,
					            responsive: {
					            	320: {
					                    items: 1,
					                    nav: false,
					                },
					            	767: {
					                    items: 1,
					                    nav: false,
					                },
					            	768: {
					                    items: 1,
					                    nav: false,
					                },
					            	992: {
					                    items: 1,
					                    nav: true,
					                },
					            },
					            autoplay: false,
					            autoplayTimeout: duration,
					            loop: false,
					    };
					}
					$(".owl-carousel8").owlCarousel( $config_obj_1 );
				}
	}, 1800);
	

    if ($(".owl-carousel7").length > 0) {

    	setTimeout(function(){

    			var duration = $(".owl-carousel7").data('duration');
			        if ($(".owl-carousel7").hasClass('fade-animation')) {
			            $(".owl-carousel7").owlCarousel({
			                nav: true, navText: ["<img src='"+BASEURL+"assets/images/spacer.gif'>", "<img src='"+BASEURL+"assets/images/spacer.gif'>"], items: 1, onInitialized: function () {
			                    if ($('.owl-carousel7 .item').length > 0) {
			                        $('.owl-carousel7 .owl-nav .owl-prev').html('<i class="sprite owl-carousel7-prev"></i>');
			                        $('.owl-carousel7 .owl-nav .owl-next').html('<i class="sprite owl-carousel7-next"></i>');
			                    }
			                    update_height_banner_home();
			                },
			                responsiveClass: true,
			                responsive: {
			                    320: {
			                        items: 1,
			                        nav: true,
			                    },
			                },
			                autoplay: true,
			                autoplayTimeout: duration,
			                loop: true,
			                animateIn: 'fadeIn',
			                animateOut: 'fadeOut'
			            });
			        }
			        else {
			            $(".owl-carousel7").owlCarousel({
			                nav: true, navText: ["<img src='"+BASEURL+"assets/images/spacer.gif'>", "<img src='"+BASEURL+"assets/images/spacer.gif'>"], items: 1, onInitialized: function () {
			                    if ($('.owl-carousel7 .item').length > 0) {
			                        $('.owl-carousel7 .owl-nav .owl-prev').html('<i class="sprite owl-carousel7-prev"></i>');
			                        $('.owl-carousel7 .owl-nav .owl-next').html('<i class="sprite owl-carousel7-next"></i>');
			                    }
			                    update_height_banner_home();
			                },
			                responsiveClass: true,
			                responsive: {
			                    320: {
			                        items: 1,
			                        nav: true,
			                    },
			                },
			                autoplay: true,
			                autoplayTimeout: duration,
			                loop: true
			            });
			        }

    	}, 1000);
	}

	if ( $(".owl-carousel6").length > 0 ) {
		$(".owl-carousel6").owlCarousel({ nav:true ,navText: ["<img src='"+BASEURL+"assets/images/spacer.gif'>","<img src='"+BASEURL+"assets/images/spacer.gif'>"] ,items:3, margin:20, onInitialized:function() { 
			if ($('.owl-carousel6 .item').length > 0 ) {
					setTimeout(function(){
					}, 200);
					$('.owl-carousel6 .owl-nav .owl-prev').html('<i class="sprite owl-carousel6-prev"></i>');
					$('.owl-carousel6 .owl-nav .owl-next').html('<i class="sprite owl-carousel6-next"></i>');
			} } ,
			responsiveClass:true,
		    responsive:{
		    	320:{
		            items:3,
		            nav:true,
		        },
		    }
		});
	}

	if ( $(".owl-carousel5").length > 0 ) {
		$(".owl-carousel5").owlCarousel({ nav:true ,navText: ["<img src='"+BASEURL+"assets/images/spacer.gif'>","<img src='"+BASEURL+"assets/images/spacer.gif'>"] ,items:1, autoWidth:true, onInitialized:function() { 
			if ($('.owl-carousel5 .item').length > 0 ) {
					setTimeout(function(){
						$('.owl-carousel5 .owl-stage-outer .owl-stage').width( ($('.owl-carousel5 .owl-stage-outer .owl-stage').width() + 3) );
						
					}, 200);
					$('.owl-carousel5 .owl-nav .owl-prev').html('<i class="sprite owl-carousel5-prev"></i>');
					$('.owl-carousel5 .owl-nav .owl-next').html('<i class="sprite owl-carousel5-next"></i>');
			} } ,
			responsiveClass:true,
		    responsive:{
		    	320:{
		            items:1,
		            nav:true,
		        },
		    	375:{
		            items:1,
		            nav:true,
		        },
		        667:{
		            items:2,
		            nav:true
		        },
		    	768:{
		            items:3,
		            nav:true
		        },
		        1024:{
		            items:3,
		            nav:true
		        },
		        1280:{
		            items:3,
		            nav:true
		        },
		        1366:{
		            items:5,
		            nav:true
		        },
		    }
		});
	}

	if ( $(".owl-carousel4").length > 0 ) {
		$(".owl-carousel4").owlCarousel({ nav:true ,navText: ["<img src='"+BASEURL+"assets/images/spacer.gif'>","<img src='"+BASEURL+"assets/images/spacer.gif'>"] ,items:5, onInitialized:function() { 
			if ($('.owl-carousel4 .item').length > 0 ) {
					setTimeout(function(){
						// $('.owl-carousel4.item-container .item').height( $('.owl-item').eq(0).height() +'px' );
					}, 200);
					$('.owl-carousel4 .owl-nav .owl-prev').html('<i class="sprite owl-carousel4-prev"></i>');
					$('.owl-carousel4 .owl-nav .owl-next').html('<i class="sprite owl-carousel4-next"></i>');
					$('.owl-carousel4 .owl-item').matchHeight({ byRow: true, property: 'height', target: null, remove: false});
			} } ,
			responsiveClass:true,
		    responsive:{
		    	320:{
		            items:1,
		            nav:true,
		        },
		    	375:{
		            items:1,
		            nav:true,
		        },
		        667:{
		            items:2,
		            nav:true
		        },
		    	768:{
		            items:3,
		            nav:true
		        },
		        1024:{
		            items:3,
		            nav:true
		        },
		        1280:{
		            items:3,
		            nav:true
		        },
		        1366:{
		            items:5,
		            nav:true
		        },
		    }
		});
	}

	if ( $(".owl-carousel3").length > 0 ) {
		$(".owl-carousel3").owlCarousel({ nav:true, navText: ["<img src='"+BASEURL+"assets/images/spacer.gif'>","<img src='"+BASEURL+"assets/images/spacer.gif'>"] , items:4, onInitialized:function() { 
			if ($('.owl-carousel3.item-container .sub-item').length > 0 ) {
					setTimeout(function(){
						$('.owl-carousel3.item-container .sub-item').height( $('.owl-item').eq(0).height() +'px' );
					}, 200);
			} } ,
			responsiveClass:true,
		    responsive:{
		    	320:{
		            items:2,
		            nav:true
		        },
		    	375:{
		            items:2,
		            nav:true
		        },
		    	768:{
		            items:3,
		            nav:true
		        },
		        1024:{
		            items:3,
		            nav:true
		        },
		        1280:{
		            items:4,
		            nav:true
		        },
		    }
		});
    }

	if ( $(".owl-carousel").length > 0 ) {
		$(".owl-carousel").owlCarousel({ nav:true, navText: ["<img src='"+BASEURL+"assets/images/spacer.gif'>","<img src='"+BASEURL+"assets/images/spacer.gif'>"] , items:2, onInitialized:function() { 
			if ($('.owl-carousel.item-container .sub-item').length > 0 ) {
					setTimeout(function(){
						if ( $(window).width() > 890 ) {
								$('.owl-carousel.item-container .sub-item').height( $('.owl-carousel .owl-item').eq(0).height() +'px' );
						}
						else {
							// $('.section-6-step .owl-carousel.item-container .owl-stage-outer').height( $('.section-6-step .owl-carousel.item-container').height()+'px' );
						}
					}, 200);
			} } ,
			responsiveClass:true,
		    responsive:{
		    	320:{
		            items:1,
		            nav:true
		        },
		    	430:{
		            items:2,
		            nav:true
		        },
		    	768:{
		            items:2,
		            nav:true
		        },
		        1024:{
		            items:2,
		            nav:true
		        },
		        1280:{
		            items:2,
		            nav:true
		        },
            }
		});
	}

	setTimeout(function(){

		if ( $(".owl-carousel2").length > 0 ) {
			$(".owl-carousel2").owlCarousel({items:6, nav:true,loop: true, autoplay:true, autoplayTimeout:3000, autoplayHoverPause:true, onInitialized: function() {
						setTimeout(function(){
							if ( $(window).width() > 1 ) {
								
									$('.owl-carousel2.item-container .item').height( ( $('.section-7-step').height() - 50) +'px' );
							}
							$('.owl-carousel2 .owl-nav .owl-prev').html('<i class="sprite owl-carousel2-prev"></i>');
							$('.owl-carousel2 .owl-nav .owl-next').html('<i class="sprite owl-carousel2-next"></i>');
							
						}, 400);
				},
				responsiveClass:true,
			    responsive:{
			    	320:{
			            items:1,
			            nav:true,
			            slideBy: 1,
			        },
			    	375:{

			            items:2,
			            nav:true,
			            slideBy: 1,
			        },
			        667:{
			            items:3,
			            nav:true,
			            slideBy: 1,
			        },
			    	768:{
			            items:3,
			            nav:true,
			            slideBy: 1,
			        },
			        1024:{
			            items:4,
			            nav:true,
			            slideBy: 1,
			        },
			        1280:{
			            items:4,
			            nav:true,
			            slideBy: 1,
			        },
	            },
	            autoplay: true,
	            loop: true
			});
		}
	},1000);
});

function isScrolledIntoView(elem) {
    var docViewTop = $(window).scrollTop(),
        docViewBottom = docViewTop + $(window).height(),
        elemTop = $(elem).offset().top,
     elemBottom = elemTop + $(elem).height();
   //Is more than half of the element visible
   return ((elemTop + ((elemBottom - elemTop)/2)) >= docViewTop && ((elemTop + ((elemBottom - elemTop)/2)) <= docViewBottom));
}

function news_list_item_calculate() {
		if ( $('#newsSection').length > 0 && $(window).width()<=767 ) {
			$('#newsSection .item').removeClass('col-sm-12').addClass('col-sm-4');
			$('#newsSection').removeClass('listView');
			$('.filterContent .gridView').removeClass('active');
			$('.filterContent .listView').addClass('active');
		}
}

function product_compare_page_cal_footer() {
	
	if ( $('#page-product-thumbnail.tomDev #compareSection .fix-float-label').length > 0 ) {
		
		var $width = $('#page-product-thumbnail.tomDev #compareSection .tableCompare table tr td:first-child').eq(1).width() + 18;
		var $height = $('#page-product-thumbnail.tomDev #compareSection .tableCompare table tr td').eq(2).height() + 18;

		$('#page-product-thumbnail.tomDev #compareSection .tableCompare .compare-col-l').width( $width );
		$('#page-product-thumbnail.tomDev #compareSection .tableCompare .compare-col-r').width( ($('#page-product-thumbnail.tomDev #compareSection .tableCompare .footer-data').width() - $width) );
	}
}

function update_height_banner_home() {
	if ( $('body header #row-one').length > 0 && $('body header #row-two').length > 0 && $('body .owl-carousel7').length > 0 ) {

		var _heighter_ = ( $(window).height() - ($('body header #row-one').height() + $('body header #row-two').height()) );
		_heighter_ = (_heighter_ / 1.2);
		var mywidth = ( $(window).width());
		//$('body .owl-carousel7').height( _heighter_ );
	}
}

function update_height_banner_product_list() {
	if ( $('#banner_section.product_banner .item figure > div').length > 0 ) {

		var _heighter_ = ( $(window).height() - ($('body header #row-one').height() + $('body header #row-two').height()) );
		_heighter_ = (_heighter_ - 0);
//		$('#banner_section.product_banner').height( _heighter_ );
	}
}

function update_mobile_height_search() {
	if ( $('.mobile_search_newform .filter-container .wagon').length > 0 ) {
		var _ko = ( ( $('.mobile_search_newform .filter-container .wagon > form').height() + 30) - ( mobilecheck() ? 0 : 0 ) );
		$('.mobile_search_newform .filter-container').height( ( _ko+_variable_1._2_+( get_a_gap() > 0 ? get_a_gap() : 0 )) );
		$('.mobile_search_newform .form-container .form-item').height( ( _ko+_variable_1._1_) );
	}
}

$(window).ready(function(e) {

	$('.match_height_2 .item-data-list span').matchHeight({ byRow: true, property: 'height', target: null, remove: false});
	
	back_to_top_function();
	top_phone_function();

	$('a').click(function(e){
		if ( $(this).attr('href')=='#' ) {
			e.preventDefault();
		}
		if ( $(this).attr('href')=='index.php' ) {
			e.preventDefault();
		}
	});

	update_mobile_height_search();

	if ( $('.owl-carousel10').length > 0 ) {

	    			var duration = $(".owl-carousel10").data('duration');
		        
		            $(".owl-carousel10").owlCarousel({
		                nav: false, navText: ["<img src='"+BASEURL+"assets/images/spacer.gif'>", "<img src='"+BASEURL+"assets/images/spacer.gif'>"], items: 1, onInitialized: function () {
		                    if ($('.owl-carousel10 .item').length > 0) {
		                    	
		                        $('.owl-carousel10 .owl-nav .owl-prev').html('<i class="sprite owl-carousel10-prev"></i>');
		                        $('.owl-carousel10 .owl-nav .owl-next').html('<i class="sprite owl-carousel10-next"></i>');
		                    }
		                    
		                    $('.owl-carousel10').height( $(window).width() );
		                },
		                items: 1,
		                responsiveClass: true,
		                responsive: {
		                    0: {
		                        items: 1,
		                        nav: false,
		                    },
		                    768: {
		                        items: 1,
		                        nav: false,
		                    },
		                },
		                autoplay: false,
		                autoplayTimeout: duration,
		                loop: false,
		                animateIn: 'fadeIn',
		                animateOut: 'fadeOut',
		                stagePadding: 15,
		            });
	}

	if ( $('.owl-carousel11').length > 0 ) {
			var duration = $(".owl-carousel11").data('duration');
            $(".owl-carousel11").owlCarousel({
                nav: false, navText: ["<img src='"+BASEURL+"assets/images/spacer.gif'>", "<img src='"+BASEURL+"assets/images/spacer.gif'>"], onInitialized: function () {
                    if ($('.owl-carousel11 .item').length > 0) {
                    	
                        $('.owl-carousel11 .owl-nav .owl-prev').html('<i class="sprite owl-carousel11-prev"></i>');
                        $('.owl-carousel11 .owl-nav .owl-next').html('<i class="sprite owl-carousel11-next"></i>');
                    }
                    
                },
                items:3,
                responsiveClass: false,
                responsive:{
			    	320:{
			            items:2,
			            nav:true,
			            slideBy: 1,
			        },
			    	375:{
			            items:2,
			            nav:true,
			            slideBy: 1,
			        },
			        480:{
			            items:3,
			            nav:true,
			        },
			        667:{
			            items:3,
			            nav:true,
			            slideBy: 1,
			        },
			    	768:{
			            items:3,
			            nav:true,
			            slideBy: 1,
			        },
			        1024:{
			            items:3,
			            nav:true,
			            slideBy: 1,
			        },
			        1280:{
			            items:3,
			            nav:true,
			            slideBy: 1,
			        },
	            },
                autoplay: false,
                // autoplayTimeout: duration,
                loop: false,
                // animateIn: 'fadeIn',
                // animateOut: 'fadeOut',
                stagePadding: 10,
            });
	}


	if ($(".owl-carousel9").length > 0) {

				setTimeout(function() {

	    			var duration = $(".owl-carousel9").data('duration');
					        
					            $(".owl-carousel9").owlCarousel({
					                nav: false, navText: ["<img src='"+BASEURL+"assets/images/spacer.gif'>", "<img src='"+BASEURL+"assets/images/spacer.gif'>"], items: 1, onInitialized: function () {
					                    if ($('.owl-carousel9 .item').length > 0) {
					                    	
					                        $('.owl-carousel9 .owl-nav .owl-prev').html('<i class="sprite owl-carousel9-prev"></i>');
					                        $('.owl-carousel9 .owl-nav .owl-next').html('<i class="sprite owl-carousel9-next"></i>');
					                    }
					                    update_height_banner_home();
					                    $('.mobile_search_newform .owl-stage .owl-item').click(function(e){
					                    	$('.mobile_search_newform .owl-stage .owl-item').removeClass('activated');
					                    	$(this).addClass('activated');
					                    });
					                    $('.mobile_search_newform .owl-stage .owl-item').eq(0).addClass('activated');
					                },
					                items: 6,
					                responsiveClass: true,
					                responsive: {
					                    0: {
					                        items: 6,
					                        nav: false,
					                    },
					                    768: {
					                        items: 6,
					                        nav: false,
					                    },
					                },
					                autoplay: false,
					                autoplayTimeout: duration,
					                loop: false,
					                animateIn: 'fadeIn',
					                animateOut: 'fadeOut',
					                stagePadding: 0,
					            });
	    		}, 1000);
		}

	if ( $("#range_03").length > 0 ) {
		$("#range_03").ionRangeSlider({
	    	type: "double",
		    // grid: true,
		    min: 0,
		    max: 20000,
		    // from: 200,
		    // to: 800,
		    // prefix: "$"
		});
	}

	if ( $('.new_form_v1 .add_more_tab').length > 0 ) {
		$( "body" ).on( "click", ".new_form_v1 .add_more_tab", function(e) {
			if ( $('.new_form_v1 .item-content').length > 3 ) {
				return false;
			}
			else {
				$('.new_form_v1 .item_target_tab').eq(0).clone().appendTo( ".clone_target_box" );

				$('.new_form_v1 .item-content').each(function(ele, item){
					$(item).find('.head_line_height .element-data-number').html( (ele+1) );
				});
			}

			e.preventDefault();
		});
	}

	if ( $('input[type="file"].input_file_overflow').length > 0 ) {
		$('input[type="file"].input_file_overflow').change(function(e){
            var fileName = e.target.files[0].name;
            $(this).parents('.input_hidden').siblings('.fake_input').children('.file_name').html(fileName);
        });
	}

	if ( $('#single_date_picker1').length > 0 ) {
		$('#single_date_picker1').datetimepicker({ format: 'DD/MM/YYYY', maxDate: new Date(), minDate: moment().startOf('month').subtract(99, 'y') }).on('dp.change', function (event) {
	        var data = $(this).val().split('/');
	        $('#out-age').val( data['2'] );
	    });
	}

	if ( $('#page-product-list .product-filter .col-r label').length > 0 ) {
		$('#page-product-list .product-filter .col-r label input[type=checkbox]').on( "click", function(){
			if ( $( this ).prop( "checked" ) ) {
				// Checked
				$( this ).siblings('.checkbox_chk').addClass('active');
			}
			else {
				// None Checked
				$( this ).siblings('.checkbox_chk').removeClass('active');
			}
		});
	}

	if ( $('.check_box_holder label').length > 0 ) {
		$('.check_box_holder label input[type=checkbox]').on( "click", function(){
			if ( $( this ).prop( "checked" ) ) {
				// Checked
				$( this ).siblings('.checkbox_chk').addClass('active');
			}
			else {
				// None Checked
				$( this ).siblings('.checkbox_chk').removeClass('active');
			}
		});
	}
	

	if ( $('#product-list.view-list .row .col .item-content .insurance-detail').length > 0 ) {
		$('#fixed-and-float').addClass('hide');
	}
	else {
		$('#fixed-and-float').removeAttr('style');
	}

	$('#email_subscribe').on("change paste keyup", function(e) {
	    var current_value = $(this).context.value;
	    if( current_value != '' && isValidEmailAddress( current_value ) ) {
          	$('footer #footer-row-two .subscribe').removeClass('Error');
      	}
      	else {
      		if ( current_value == '' ) {
      			$('footer #footer-row-two .subscribe').removeClass('Error');
      		}
      		else {
      			$('footer #footer-row-two .subscribe').addClass('Error');
      		}
      	}
	});
	
	if ( $('.home').length > 0 ) {
		$('#popup-model-colorbox-landing-page').modal('toggle');
		// setCookie('pop', '1',1);
	}

	$('.landing-pop-close').click(function(e){
		$('#popup-model-colorbox-landing-page').modal('toggle');
	});


	$('#contact-popup').on('hidden.bs.modal', function (e) {
	    destroy_function();
	});

	$('.call_pop_contact').click(function(e){
		$('#contact-popup').modal('toggle');
		init_function();
		grecaptcha.reset();
	});

	$('.call_pop_lead').click(function(e){
		$('#lead_form_popup').modal('toggle');
	});

	setTimeout(function(){
			if ( $('.eiei').length > 0 ) {
				$('#lead_form_popup').modal('toggle');
			}
	}, 1000);
	
	
	$('.fix-back-to-top .back-to-top').click(function(e) {
			$('html,body').animate({ scrollTop: 0 }, 'slow');
        	return false; 
	});

	$('.galleryBox .item img').click(function(e) {
		$('.galleryMain img').attr('src', $(this).attr('src') );
	});

	$('.sitemap .site-item li span a').click(function(e){
		if ( $(this).attr('href') == '#' ) {
			var parent = $(this).parents('li');
			if ( parent.next().hasClass('submenu') ) {
				parent.next().toggleClass('active');
			}
		}
	});

	news_list_item_calculate();
	search_result_function_calculate();
	product_compare_page_cal_footer();
	auto_update_height();

	$("input.numeric").each(function() {
	    $(this).keypress(function(e){
	        return key_digit(e);
	    });
    });


    $(".email").keypress(function(event) {
        return english_mail_format_only(event);
    });

    $(".prevent_type").keypress(function(event) {
        return false;
    });

	if ( $('#datetimepicker4').length > 0 ) {
		$('#datetimepicker4').datetimepicker({format: 'DD/MM/YYYY',defaultDate: variable_config.server_date});
	}

	if ( $('#datetimepicker3').length > 0 ) {
		$('#datetimepicker3').datetimepicker({format: 'HH:mm'});
	}

	if ( $('#date_of_birth').length > 0 ) {
		$('#date_of_birth').datetimepicker({format: 'MM/YYYY'}).on('dp.change',function(event){
			var data = $('#date_of_birth').val().split('/');
			$('#ages_customer_2').val( (variable_config.server_year-data[1]) );
			
  		});
	}

	//if ( $('#year_of_birth').length > 0 ) {
	//	$('#year_of_birth').datetimepicker({ format: 'MM/YYYY' }).on('dp.change',function(event){
	//		var data = $('#year_of_birth').val().split('/');
	//		$('#ages_customer_1').val( (variable_config.server_year-data[1]) );
 // 		});
	//}

	//if ( $('#travel_begin2').length > 0 && $('#travel_end2').length > 0 ) {
	//	$('#travel_begin2').datetimepicker({format: 'DD/MM/YYYY'});
	//	$('#travel_end2').datetimepicker({format: 'DD/MM/YYYY'
	//		, useCurrent: false //Important! See issue #1075
	//	});

	//	function CalcDiff2() {
	//	    var a = $("#travel_begin2").data("DateTimePicker").date();
	//	    var b = $('#travel_end2').data("DateTimePicker").date();
	//	    var timeDiff = 0
	//	    if (b) {
	//	        timeDiff = (b - a) / 1000;
	//	    }
	//	    var DateDiff = Math.floor(timeDiff / (60 * 60 * 24)) + 1;
	//	    var BalSecs = timeDiff - (DateDiff * (60 * 60 * 24));
 //           $('#cal_target_2').html(DateDiff);
 //           $('#ta_day').val(DateDiff);
	//	    // $('.time-here').val(Math.ceil(BalSecs / (60 * 60)))
	//	}

	//	$("#travel_begin2").on("dp.change", function (e) {
 //           $('#travel_end2').data("DateTimePicker").minDate(e.date);
 //           CalcDiff2();
 //       });
 //       $("#travel_end2").on("dp.change", function (e) {
 //           $('#travel_begin2').data("DateTimePicker").maxDate(e.date);
 //           CalcDiff2();
 //       });

 //       CalcDiff2();
	//}

	//if ( $('#travel_begin').length > 0 && $('#travel_end').length > 0 ) {
	//	$('#travel_begin').datetimepicker({format: 'DD/MM/YYYY'});
	//	$('#travel_end').datetimepicker({format: 'DD/MM/YYYY'
	//		, useCurrent: false //Important! See issue #1075
	//	});

	//	function CalcDiff() {
	//	    var a = $("#travel_begin").data("DateTimePicker").date();
	//	    var b = $('#travel_end').data("DateTimePicker").date();
	//	    var timeDiff = 0
	//	    if (b) {
	//	        timeDiff = (b - a) / 1000;
	//	    }
	//	    var DateDiff = Math.floor(timeDiff / (60 * 60 * 24));
	//	    var BalSecs = timeDiff - (DateDiff * (60 * 60 * 24));
	//	    $('#cal_target_1').val(DateDiff);
	//	    // $('.time-here').val(Math.ceil(BalSecs / (60 * 60)))
	//	}

	//	$("#travel_begin").on("dp.change", function (e) {
 //           $('#travel_end').data("DateTimePicker").minDate(e.date);
 //           CalcDiff();
 //       });
 //       $("#travel_end").on("dp.change", function (e) {
 //           $('#travel_begin').data("DateTimePicker").maxDate(e.date);
 //           CalcDiff();
 //       });
	//}

	$(".icon-magnifier-copy").click(function(e) {
		$(".search-fade").fadeIn();
		$(".search-page").focus();
	});

	$(".close-search").click(function(e) {
		$(".search-fade").fadeOut();
		$(".search-page").val("");
	});

	

	$('#page-product-thumbnail.tomDev #form.filterContent .viewDisplay > button').click(function(e) {
		$('#page-product-thumbnail.tomDev #form.filterContent .viewDisplay > button').removeClass('active');
		$(this).addClass('active');
		// listView setTop
		if ( $(this).hasClass('listView') ) {
			$('#newsSection').addClass('listView');
			$('#newsSection .item').removeClass('col-sm-4');

			setTimeout(function(){ $('#news-box .item > a').matchHeight({ remove: true }); }, 700);
		}
		else {
			$('#newsSection').removeClass('listView');
			$('#newsSection .item').addClass('col-sm-4');

			if ( $('#news-box').length > 0 ) {
				setTimeout(function(){ $('#news-box .item > a').matchHeight({ byRow: true, property: 'height', target: null, remove: false}); }, 700);
			}
		}
		
	});

    if ( $('#faqSection').length > 0 ) {

		$('#faqSection .tab').on('click', function(){
		
			$('#faqSection .tab').removeClass('active');
			$(this).addClass('active');

		
		});
    }

	$('#open-close').click(function() {
		if ( ! $('#fixed-and-float').hasClass('active') ) {
			$('#menu_float_right').removeClass('active');
		}
		else {
			// $('#menu_float_right').addClass('active');
		}
		$('#fixed-and-float').toggleClass('active');
		
	});

	$('a').click(function(e) {
		if ( $(this).attr('href') === '#' ) {
			e.preventDefault();
		}
	});

	$("footer .email_subscribe").on("change paste keyup", function(e) {
	    var current_value = $(this).context.value; 
	    if ( current_value != '') {
	    	$('footer #footer-row-two .subscribe label').addClass('notnull');
	    }
	    else {
	    	$('footer #footer-row-two .subscribe label').removeClass('notnull');
	    }
	});

	$('header #row-one #menu-lv1 #burger').click(function(e) {

		if ( ! $('header #row-one #menu_float_right').hasClass('active') ) {
			$('#fixed-and-float').removeClass('active');
		}
		else {
			// $('#menu_float_right').addClass('active');
		}
		$('header #row-one #menu_float_right').addClass('active');
	});

	$('header #row-one .close').click(function(e) {

		$('header #row-one #menu_float_right').removeClass('active');
	});

	$('#menu_float_right .something-else-semantic > a').click(function(e) {

		$('#menu_float_right .wrapper-submenu').css("height", "0px").removeClass('active');
		var $element = $(this).next('.wrapper-submenu');
		if ( $element.hasClass('wrapper-submenu') && ! $element.hasClass('active') ) {
			$element.addClass('active');
			$element.css("height", $element.children('.submenu-type').height()+"px");
			
			if( $(this).hasClass('active') ) {
				$(this).removeClass('active');
				$element.css("height", "0px");
				$element.removeClass('active');
			}
			else {
				$(this).addClass('active');
			}
		}
	});
	
});


function hide_logo_top() {
    $('header #row-two #icon-logo .inner img').addClass('opacity-hide');
    $('header #row-one .fix-icon a').removeClass('opacity-hide');
}

function show_logo_top() {
	$('header #row-two #icon-logo .inner img').removeClass('opacity-hide');
	$('header #row-one .fix-icon a').addClass('opacity-hide');
}

var CurrentScroll = 0;
var timeout_block_ui = true;
$(window).scroll(function() {
			var NextScroll = $(this).scrollTop();
			top_phone_function();
  			if ( NextScroll > 0 ) {
	              	hide_logo_top();
          	}
          	else {
          		if ( $(window).width() <= 992) {
          			hide_logo_top();
          		}
          		else {
          			show_logo_top(); 
          		}
          	}
          	back_to_top_function();
		
      clearTimeout($.data(this, 'scrollTimer'));
      $.data(this, 'scrollTimer', setTimeout(function() { 
          // do something       
          //last scroll will always save the current scroll position value
          var NextScroll = $(this).scrollTop();
          
          if( CurrentScroll !== 0 ) {
                  
                  if (NextScroll >= CurrentScroll) {
                      //write the codes related to down-ward scrolling here
                      
                  }
                  else {
                      //write the codes related to upward-scrolling here
                      if( NextScroll > 0) {
                         
                          clearTimeout($.data(this, 'scrollTimer'));
                      }
                      
                  }
          }

          //Updates current scroll position
          CurrentScroll = NextScroll;
			
          if( NextScroll === 0 ) {

          }

          
          //if ( $('#product-list-load-more').length > 0 && isScrolledIntoView($('#product-list-load-more')) ) {
	      //    	if ( timeout_block_ui ) {
	      //    			$.blockUI({ message: null, timeout: 2000 });
	      //    			timeout_block_ui = false;
		  //        		setTimeout(timeout_block_ui = true, 2300); 
	      //    	}
	      //    	// $('#product-list-load-more').remove();
          //}
          //else if ( $('#news-list-load-more').length > 0 && isScrolledIntoView($('#news-list-load-more')) ) {
	      //    	if ( timeout_block_ui ) {
	      //    			$.blockUI({ message: null, timeout: 2000 });
	      //    			timeout_block_ui = false;
		  //        		setTimeout(timeout_block_ui = true, 2300); 
	      //    	}
	      //    	// $('#news-list-load-more').remove();
          //}
      }, 200));
});


function resizedw() {
    // Haven't resized in 100ms!
    product_compare_page_cal_footer();
    auto_update_height();
    search_result_function_calculate();
    news_list_item_calculate();
}

var doit;
window.onresize = function(){
  clearTimeout(doit);
  doit = setTimeout(resizedw, 100);
};

