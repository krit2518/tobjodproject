function destroy_function() {
    if ($('#contact_agent_form').length > 0) {
        $('#contact_agent_form').formValidation('destroy', true);
    }
}

function contact_agent_form() {
    if ($('#contact_agent_form').length > 0) {

            $('#contact_agent_form').formValidation('destroy', true);
        
            $('#contact_agent_form').formValidation({
                framework: 'bootstrap',
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    validating: 'glyphicon glyphicon-refresh'
                },
                //addOns: {
                //    reCaptcha2: {
                //        element: 'captchaContainer',
                //        theme: 'light',
                //        siteKey: '6LfcwisUAAAAAH0useokQm42uo-RlPdzPRVthyEb',
                //        timeout: 120,
                //        message: 'The captcha is not valid'
                //    }
                //},
                fields: {
                    Name: {
                        validators: {
                            notEmpty: {
                                message: 'กรุณากรอกชื่อ'
                            }
                        }
                    },
                    Surname: {
                        validators: {
                            notEmpty: {
                                message: 'กรุณากรอกที่อยู่'
                            }
                        }
                    },
                    Tel: {
                        validators: {
                            notEmpty: {
                                message: 'กรุณากรอกเบอร์โทรศัพท์'
                            },
                            stringLength: {
                                message: 'Post content must be less than 120 characters',
                                max: function (value, validator, $field) {
                                    return 15 - (value.match(/\r/g) || []).length;
                                },
                                min: function (value, validator, $field) {
                                    return 10 - (value.match(/\r/g) || []).length;
                                }
                            },
                            regexp: {
                                regexp: '^(0)[0-9]{8,9}$',
                                message: 'รูปแบบเบอร์โทรศัพท์ไม่ถูกต้อง'
                            }
                        }
                    },
                    Email: {
                        validators: {
                            notEmpty: {
                                message: 'กรุณากรอกอีเมล์'
                            },
                            regexp: {
                                regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                                message: 'รูปแบบอีเมล์ไม่ถูกต้อง'
                            }
                        }
                    },
                    CallBackDay: {
                        validators: {
                            notEmpty: {
                                message: ''
                            }
                        }
                    },
                    CallBackTime: {
                        validators: {
                            notEmpty: {
                                message: ''
                            }
                        }
                    },
                    condition: {
                        validators: {
                            notEmpty: {
                                message: ''
                            }
                        }
                    }
                }
            })
            .on('err.field.fv', function (e, data) {

                $('.help-block').hide();
            });
        }
}

function init_function() {
    
    contact_agent_form();
    
    if ($('#lead_form').length > 0) {
        $('#lead_form').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                Name: {
                    validators: {
                        notEmpty: {
                            message: 'กรุณากรอกชื่อ'
                        }
                    }
                },
                Surname: {
                    validators: {
                        notEmpty: {
                            message: 'กรุณากรอกที่อยู่'
                        }
                    }
                },
                Tel: {
                    validators: {
                        notEmpty: {
                            message: 'กรุณากรอกเบอร์โทรศัพท์'
                        },
                        stringLength: {
                            message: 'Post content must be less than 120 characters',
                            max: function (value, validator, $field) {
                                return 15 - (value.match(/\r/g) || []).length;
                            },
                            min: function (value, validator, $field) {
                                return 10 - (value.match(/\r/g) || []).length;
                            }
                        },
                        regexp: {
                            regexp: '^(0)[0-9]{8,9}$',
                            message: 'รูปแบบเบอร์โทรศัพท์ไม่ถูกต้อง'
                        }
                    }
                },
                Email: {
                    validators: {
                        notEmpty: {
                            message: 'กรุณากรอกอีเมล์'
                        },
                        regexp: {
                            regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                            message: 'รูปแบบอีเมล์ไม่ถูกต้อง'
                        }
                    }
                }
            }
        })
            .on('err.field.fv', function (e, data) {

                $('.help-block').hide();
            });
    }
}


$(document).ready(function () {

    if ($('.form-home-page').length > 0) {
        $('.form-home-page').formValidation({
            framework: 'bootstrap',
            fields: {
                propertytype: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                constructiontype: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                year: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                //propertyValue: {
                //    validators: {
                //        notEmpty: {
                //            message: '...'
                //        }
                //    }
                //},
                constructionValue: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },

            }
        })
            .on('err.field.fv', function (e, data) {
                $('.help-block').hide();
            });
    }

    if ($('.form-health-page').length > 0) {
        $('.form-health-page').formValidation({
            framework: 'bootstrap',
            fields: {
                birthday: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                age: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                weight: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                height: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                career: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },

            }
        })
            .on('err.field.fv', function (e, data) {
                $('.help-block').hide();
            });
    }

    if ($('.form-pa-page').length > 0) {
        $('.form-pa-page').formValidation({
            framework: 'bootstrap',
            fields: {
                birthday: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                age: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                career: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
            }
        })
            .on('err.field.fv', function (e, data) {
                $('.help-block').hide();
            });
    }

    if ($('.form-ta-page').length > 0) {
        $('.form-ta-page').formValidation({
            framework: 'bootstrap',
            fields: {
                coverageOption: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                tripType: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                country: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                DateFrom: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                DateTo: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                coverageType: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },

            }
        })
            .on('err.field.fv', function (e, data) {
                $('.help-block').hide();
            });
    }

    if ($('.form-psu-page').length > 0) {
        $('.form-psu-page').formValidation({
            framework: 'bootstrap',
            fields: {
                company: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },

            }
        })
            .on('err.field.fv', function (e, data) {
                $('.help-block').hide();
            });
    }

    var validate_error = {};
    if ($('.form-motor-page').length > 0) {
        validate_error.motor = false;
        $('.form-motor-page').formValidation({
            framework: 'bootstrap',
            fields: {
                category: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                year: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                brand: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                family: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                model: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                province: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                
            }
        }).on('err.field.fv', function (e, data) {
            $('.help-block').hide();

        }).on('err.validator.fv', function (e, data) {
            // data.field     --> The field name
            // data.element   --> The field element
            // data.validator --> The validator name
            // data.result    --> The data returned by the validator

            if (!validate_error.motor) {
                validate_error.motor = true;

            }
        });
    }

    if ($('#form-motor').length > 0) {
        $('#form-motor').formValidation({
            framework: 'bootstrap',
            fields: {
                category: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                year: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                brand: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                family: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                model: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                province: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },

            }
        })
            .on('err.field.fv', function (e, data) {
                $('.help-block').hide();
            });
    }

    if ($('#form-psu').length > 0) {
        $('#form-psu').formValidation({
            framework: 'bootstrap',
            fields: {
                company: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },

            }
        })
            .on('err.field.fv', function (e, data) {
                $('.help-block').hide();
            });
    }

    if ($('#form-ta').length > 0) {
        $('#form-ta').formValidation({
            framework: 'bootstrap',
            fields: {
                coverageOption: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                tripType: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                country: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                DateFrom: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                DateTo: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                coverageType: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },

            }
        })
            .on('err.field.fv', function (e, data) {
                $('.help-block').hide();
            });
    }

    if ($('#form-pa').length > 0) {
        $('#form-pa').formValidation({
            framework: 'bootstrap',
            fields: {
                birthday: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                age: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                Career: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },

            }
        })
            .on('err.field.fv', function (e, data) {
                $('.help-block').hide();
            });
    }

    if ($('#form-health').length > 0) {
        $('#form-health').formValidation({
            framework: 'bootstrap',
            fields: {
                Birthday: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                age: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                weight: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                height: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                career: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },

            }
        })
            .on('err.field.fv', function (e, data) {
                $('.help-block').hide();
            });
    }

    if ($('#form-fire').length > 0) {
        $('#form-fire').formValidation({
            framework: 'bootstrap',
            fields: {
                propertytype: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                ConstructionType: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                Year: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                //propertyValue: {
                //    validators: {
                //        notEmpty: {
                //            message: '...'
                //        }
                //    }
                //},
                constructionValue: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },

            }
        })
            .on('err.field.fv', function (e, data) {
                $('.help-block').hide();
            });
    }

    if ($('#form-motor-mobile').length > 0) {
        $('#form-motor-mobile').formValidation({
            framework: 'bootstrap',
            fields: {
                category: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                year: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                brand: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                family: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                model: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                province: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },

            }
        })
            .on('err.field.fv', function (e, data) {
                $('.help-block').hide();
            });
    }

    if ($('#form-psu-mobile').length > 0) {
        $('#form-psu-mobile').formValidation({
            framework: 'bootstrap',
            fields: {
                company: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },

            }
        })
            .on('err.field.fv', function (e, data) {
                $('.help-block').hide();
            });
    }

    if ($('#form-ta-mobile').length > 0) {
        $('#form-ta-mobile').formValidation({
            framework: 'bootstrap',
            fields: {
                coverageOption: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                tripType: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                country: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                DateFrom: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                DateTo: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                coverageType: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },

            }
        })
            .on('err.field.fv', function (e, data) {
                $('.help-block').hide();
            });
    }

    if ($('#form-pa-mobile').length > 0) {
        $('#form-pa-mobile').formValidation({
            framework: 'bootstrap',
            fields: {
                birthday: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                age: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                Career: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },

            }
        })
            .on('err.field.fv', function (e, data) {
                $('.help-block').hide();
            });
    }

    if ($('#form-health-mobile').length > 0) {
        $('#form-health-mobile').formValidation({
            framework: 'bootstrap',
            fields: {
                Birthday: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                age: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                weight: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                height: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                career: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },

            }
        })
            .on('err.field.fv', function (e, data) {
                $('.help-block').hide();
            });
    }

    if ($('#form-fire-mobile').length > 0) {
        $('#form-fire-mobile').formValidation({
            framework: 'bootstrap',
            fields: {
                propertytype: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                ConstructionType: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                Year: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },
                //propertyValue: {
                //    validators: {
                //        notEmpty: {
                //            message: '...'
                //        }
                //    }
                //},
                constructionValue: {
                    validators: {
                        notEmpty: {
                            message: '...'
                        }
                    }
                },

            }
        })
            .on('err.field.fv', function (e, data) {
                $('.help-block').hide();
            });
    }

    init_function();


    $('#contactForm').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            firstname: {
                validators: {
                    notEmpty: {
                        message: 'กรุณากรอกชื่อ'
                    }
                }
            },
            lastname: {
                validators: {
                    notEmpty: {
                        message: 'กรุณากรอกนามสกุล'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'กรุณากรอกอีเมล์'
                    },
                    regexp: {
                        regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                        message: 'รูปแบบอีเมล์ไม่ถูกต้อง'
                    }
                }
            },
            message: {
                validators: {
                    notEmpty: {
                        message: 'กรุณากรอกข้อความ'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'กรุณากรอกรหัสผ่าน'
                    },
                    regexp: {
                        regexp: '^(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$',
                   
                        message: 'รูปแบบรหัสผ่านไม่ถูกต้อง'
                    }
                }
            }



        }
    });

    $('#form-contact').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            validating: 'glyphicon glyphicon-refresh'
        },
        //addOns: {
        //    reCaptcha2: {
        //        element: 'captchaContainer2',
        //        theme: 'light',
        //        siteKey: '6LfcwisUAAAAAH0useokQm42uo-RlPdzPRVthyEb',
        //        timeout: 120,
        //        message: 'The captcha is not valid'
        //    }
        //},
        fields: {
            Name: {
                validators: {
                    notEmpty: {
                        message: 'กรุณากรอกชื่อ'
                    }
                }
            },
            Surname: {
                validators: {
                    notEmpty: {
                        message: 'กรุณากรอกนามสกุล'
                    }
                }
            },
            Tel: {
                validators: {
                    notEmpty: {
                        message: 'กรุณากรอกเบอร์โทรศัพท์'
                    },
                    regexp: {
                        regexp: '^(0)[0-9]{8,9}$',
                        message: 'รูปแบบเบอร์โทรศัพท์ไม่ถูกต้อง'
                    }
                }
            },
            Email: {
                validators: {
                    notEmpty: {
                        message: 'กรุณากรอกอีเมล์'
                    },
                    regexp: {
                        regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                        message: 'รูปแบบอีเมล์ไม่ถูกต้อง'
                    }
                }
            },
            CallBackDay: {
                validators: {
                    notEmpty: {
                        message: ''
                    }
                }
            },
            CallBackTime: {
                validators: {
                    notEmpty: {
                        message: ''
                    }
                }
            },
            Agreement: {
                validators: {
                    notEmpty: {
                        message: ''
                    }
                }
            }


        }
    })
        .on('err.field.fv', function (e, data) {

            $('.help-block').hide();
            //}).on('success.form.fv', function(e) {
            //        //$('#popup-model-contact').modal('toggle');
            //    return true;
        });




});