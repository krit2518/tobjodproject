﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TobJod.Models;

namespace TobJod.Web.Models {
    public class ProductCompulsoryViewModel : BaseViewModel {
        public int SubCategory { get; set; }
        public int Company { get; set; }

        //public List<Company> Companies { get; set; }

        public List<ProductItem> Products { get; set; }
        
        public string PathCompany { get; set; }
        
        public static Dictionary<string, string> GetCoverage(Language language, string usage) {
            Dictionary<string, string> list = new Dictionary<string, string>();
            var compulsoryJson = JObject.Parse(System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("../../assets/json/compulsory.json")));
            var json = compulsoryJson[language.ToString()]["Coverage"][usage];
            foreach (var prop in json.Select(x => ((JProperty)x).Name).ToList()) {
                list.Add(prop, json[prop].ToString());
            }
            return list;
        }
    }
}