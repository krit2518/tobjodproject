﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TobJod.Models;

namespace TobJod.Web.Models {
    public class LoginViewModel  {

        public ConfigPage ConfigPage { get; set; }

        //[Display(Name = "Email"), MaxLength(250), DataType(DataType.EmailAddress), Required]
        public string Email { get; set; }
       // [Display(Name = "Password"), MaxLength(50), Required]
         public string Password { get; set; }


        public bool Rememberme { get; set; }
     
    }
}