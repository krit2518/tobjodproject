﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TobJod.Models;

namespace TobJod.Web.Models {
    public class ProductHomeViewModel : BaseViewModel {

        public int PropertyType { get; set; }
        public int ConstructionType { get; set; }
        public int Year { get; set; }
        public int ConstructionValue { get; set; }
        public int PropertyValue { get; set; }
        public int TotalValue { get; set; }
        

        public string PropertyTypeTitle { get; set; }
        public string ConstructionTypeTitle { get; set; }

        public List<ProductItem> Products { get; set; }
        
        public string PathCompany { get; set; }

        public bool LayoutGridView { get; set; }

        public ProductHomeViewModelOrderType Order { get; set; }

        public enum ProductHomeViewModelOrderType {
            PriceAsc = 1,
            PriceDesc = 2,
            TitleAsc = 3,
            TitleDesc = 4
        }
        
    }
}