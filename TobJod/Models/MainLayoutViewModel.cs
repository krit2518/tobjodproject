﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TobJod.Models;

namespace TobJod.Web.Models {
    public class MainLayoutViewModel {
        public List<SelectListItem> CarBrands { get; set; }
        public List<SelectListItem> PRBCompanies { get; set; }
        public List<SelectListItem> CarProvinces { get; set; }
        public List<SelectListItem> Countries { get; set; }
        public List<SelectListItem> Careers { get; set; }
        public List<SelectListItem> ConstructionTypes { get; set; }
        public List<SelectListItem> PropertyTypes { get; set; }
        public Dictionary<int, ProductCategory> ProductCategory { get; set; }

        public int ProductMotorPriceMin { get; set; }
        public int ProductMotorPriceMax { get; set; }
        public int ProductPAPriceMin { get; set; }
        public int ProductPAPriceMax { get; set; }
        public int ProductPHPriceMin { get; set; }
        public int ProductPHPriceMax { get; set; }
        public int ProductTAPriceMin { get; set; }
        public int ProductTAPriceMax { get; set; }
        public int ProductTAPersonMax { get; set; }
        public int ProductHomeYearMax { get; set; }

        public string ContactAddressTH { get; set; }
        public string ContactAddressEN { get; set; }
        public string ContactTelephoneTH { get; set; }
        public string ContactTelephoneEN { get; set; }
        public string ContactFaxTH { get; set; }
        public string ContactFaxEN { get; set; }
        public string ContactEmailTH { get; set; }
        public string ContactEmailEN { get; set; }

        public int CountryThailand { get; set; }
        public int CountrySchengen { get { return TobJod.Models.Country.SchengenId; } }

        public Language Lanugage { get; set; }

        public bool EnableCaptcha { get; set; }
        public bool EnableMember { get; set; }
    }
}