﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TobJod.Models;

namespace TobJod.Web.Models {
    public class NewsListViewModel : BaseViewModel {
        public NewsCategory Category { get; set; }
        public int SubCategory { get; set; }
        public List<News> NewsList { get; set; }

        public News News { get; set; }
        public string UrlUpload { get; set; }

        public SelectList SubCategoryList { get; set; }
    }

    public class NewsListItemViewModel {
        public string Title { get; set; }
        public string SubCategory { get; set; }
        public string Brief { get; set; }
        public string Image { get; set; }
        public string ImageAlt { get; set; }
        public string ImageTitle { get; set; }
        public string Url { get; set; }
        public string Date { get; set; }
    }
}