﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TobJod.Models;

namespace TobJod.Web.Models {
    public class PrivilegeListViewModel : BaseViewModel {
        public Privilege Item { get; set; }
        public string UrlUpload { get; set; }

    }

    public class PrivilegeListItemViewModel {
        public string Title { get; set; }
        public string Brief { get; set; }
        public string Image { get; set; }
        public string ImageAlt { get; set; }
        public string ImageTitle { get; set; }
        public string Url { get; set; }
        public string Date { get; set; }
    }
}