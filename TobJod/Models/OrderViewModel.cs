﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TobJod.Models;
using TobJod.Repositories;

namespace TobJod.Web.Models {
    public class BaseOrderViewModel {
        public string ALLOWED_UPLOAD_FILE_TYPES = "JPG,JPEG,PNG,PDF,DOC,DOCX";
        public int MAXIMUM_FILE_SIZE = 2097152;
        public string Token { get; set; }
        public List<SelectListItem> Salutations { get; set; }
        public List<SelectListItem> Countries { get; set; }
        public List<int> CountriesTA { get; set; }
        public List<SelectListItem> Provinces { get; set; }
        public List<SelectListItem> CarProvinces { get; set; }
        public List<SelectListItem> Relationships { get; set; }
        public List<SelectListItem> HomeDistricts { get; set; }
        public List<SelectListItem> HomeSubDistricts { get; set; }
        public List<SelectListItem> Districts { get; set; }
        public List<SelectListItem> SubDistricts { get; set; }
        public List<SelectListItem> BillingDistricts { get; set; }
        public List<SelectListItem> BillingSubDistricts { get; set; }
        public List<SelectListItem> ShippingDistricts { get; set; }
        public List<SelectListItem> ShippingSubDistricts { get; set; }
        public List<SelectListItem> TravelMethods { get; set; }
        public List<SelectListItem> LocationUsages { get; set; }
        public List<SelectListItem> Careers { get; set; }
        public List<SelectListItem> CarBrands { get; set; }
        public List<NameValue> CarFamilies { get; set; }
        public List<NameValue> CarModels { get; set; }

        public List<SelectListItem> Banks { get; set; }
        public List<SelectListItem> ConstructionTypes { get; set; }
        public List<SelectListItem> PropertyTypes { get; set; }

        public BaseOrderViewModel() {
            Provinces = new List<SelectListItem>();
            Districts = new List<SelectListItem>();
            SubDistricts = new List<SelectListItem>();
            BillingDistricts = new List<SelectListItem>();
            BillingSubDistricts = new List<SelectListItem>();
            ShippingDistricts = new List<SelectListItem>();
            ShippingSubDistricts = new List<SelectListItem>();
            Relationships = new List<SelectListItem>();
            TravelMethods = new List<SelectListItem>();
            LocationUsages = new List<SelectListItem>();
            Careers = new List<SelectListItem>();
            CarProvinces = new List<SelectListItem>();
            CarBrands = new List<SelectListItem>();
            Banks = new List<SelectListItem>();
            ConstructionTypes = new List<SelectListItem>();
            PropertyTypes = new List<SelectListItem>();
            CountriesTA = new List<int>();
            CarModels = new List<NameValue>();
            CarFamilies = new List<NameValue>();
        }
    }
    public class OrderViewModel : BaseOrderViewModel {
        public string Regex_CarPlate { get { return @"^(([0-9][ก-ฮ])|([0-9]?[ก-ฮ][ก-ฮ]))[0-9]{1,4}$"; } }
        public string ThailandNational { get; set; }
        public string DateFormat { get { return "dd/MM/yyyy"; } }
        public CultureInfo CultureGB { get { return new CultureInfo("en-GB"); } }
        public int ProductId { get; set; }
        public string Page { get; set; }
        public int Driver_Size { get; set; }
        public int PolicyHolder_Size { get; set; }
        public UserAction UserAction { get; set; }
        public int ApplicationId { get; set; }
        public int PremiumId { get; set; }
        public int MinAge { get; set; }
        public int MaxAge { get; set; }
        public int MinWeight { get; set; }
        public int MaxWeight { get; set; }
        public int MinHeight { get; set; }

        public int MaxHeight { get; set; }


        public string TravelType { get; set; }
        public string TravelBy { get; set; }
        public string CoverageType { get; set; }
        public string CoverageOption { get; set; }
        public string University { get; set; }
        public string Country { get; set; }
        public string PropertyType { get; set; }
        public string ConstructionType { get; set; }
        public Dictionary<int, ProductApplicationField> ProductApplicationFields { get; set; }
        public ProductApplication ProductApplication { get; set; }
        public ProductApplicationPolicyHolder ProductApplicationPolicyHolder { get; set; }
        public ProductApplicationPolicyHolder[] ProductApplicationPolicyHolders { get; set; }
        public ProductApplicationDriver ProductApplicationDriver { get; set; }
        public ProductApplicationDriver[] ProductApplicationDrivers { get; set; }

        public ProductApplicationAttachment ProductApplicationAttachment { get; set; }
        public List<ProductApplicationAttachment> ProductApplicationAttachments { get; set; }
        public ProductApplicationHome ProductApplicationHome { get; set; }
        public ProductApplicationMotor ProductApplicationMotor { get; set; }
        public ProductApplicationPA ProductApplicationPA { get; set; }
        public ProductApplicationPH ProductApplicationPH { get; set; }
        public ProductApplicationTA ProductApplicationTA { get; set; }


        public string CarBrand { get; set; }
        public string CarFamily { get; set; }
        public string CarModel { get; set; }
        public string CarProvince { get; set; }
        public decimal Price { get; set; }
        public decimal PriceMin { get; set; }
        public decimal PriceMax { get; set; }
        public int Career { get; set; }
        public string BirthDay { get; set; }
        public ProductItem ProductItem { get; set; }
        public decimal TotalPremium { get; set; }
        public decimal TotalDiscount { get; set; }
        public bool ShowMotorContactAgent { get; set; }
        public OrderViewModel() {
            ProductApplication = new ProductApplication();
            ProductApplicationPolicyHolder = new ProductApplicationPolicyHolder();
            ProductApplicationDriver = new ProductApplicationDriver();
            ProductApplicationAttachment = new ProductApplicationAttachment();

            ProductApplicationHome = new ProductApplicationHome();
            ProductApplicationMotor = new ProductApplicationMotor();
            ProductApplicationTA = new ProductApplicationTA();
            ProductApplicationPH = new ProductApplicationPH();
            ProductApplicationPA = new ProductApplicationPA();

            ProductItem = new ProductItem();
            UserAction = UserAction.Unknown;
            PolicyHolder_Size = 4;
            Driver_Size = 4;
            TotalPremium = 0;
            TotalDiscount = 0;
            ShowMotorContactAgent = false;


            CountryRepository countryRepo = new CountryRepository();
            var countryThai = countryRepo.ListCountry_ByZone_Active(Language.TH, CountryZone.Thailand);
            if (countryThai.Count > 0) ThailandNational = countryThai[0].Id.ToString();
        }


        public TobJod.Models.ProductCategoryKey ProductCategoryKey { get; set; }

        public ProductMotorCCTV CCTV { get; set; }
        public bool PRB { get; set; }
        public decimal PRBPrice { get; set; }
        public AllYesNoFlag Accessory { get; set; }
        public AllYesNoFlag Garage { get; set; }
        public AllYesNoFlag DriverSpecify { get; set; }
        public int DriverAge { get; set; }

        public string CarPlateNo { get; set; }

        public int State { get; set; }
        public string PreviouseUrl { get; set; }

        [DataType(DataType.Upload)]
        public HttpPostedFileBase IDCardFile { get; set; }

        [DataType(DataType.Upload)]
        public HttpPostedFileBase CCTVFile { get; set; }

        [DataType(DataType.Upload)]
        public HttpPostedFileBase CarRegistrationCopyFile { get; set; }

        [DataType(DataType.Upload)]
        public HttpPostedFileBase File1 { get; set; }

        [DataType(DataType.Upload)]
        public HttpPostedFileBase File2 { get; set; }

        [DataType(DataType.Upload)]
        public HttpPostedFileBase File3 { get; set; }

        [DataType(DataType.Upload)]
        public HttpPostedFileBase File4 { get; set; }
        [DataType(DataType.Upload)]
        public HttpPostedFileBase File5 { get; set; }
        [DataType(DataType.Upload)]
        public HttpPostedFileBase File6 { get; set; }
    }

    public class OrderProductViewModel : ProductMotorViewModel {
        public int ProductId { get; set; }
        public int PremiumId { get; set; }
    }

    public class ProductApplicationState {
        public Stack<ProductApplicationStep> Step { get; set; }
        public ProductApplicationStep State { get; set; }
        public int ApplicationId { get; set; }

        public OrderViewModel OrderViewModel { get; set; }
        public ProductMotorViewModel ProductMotorViewModel { get; set; }

        public ProductApplicationState() {
            this.Step = new Stack<ProductApplicationStep>();
            this.State = ProductApplicationStep.Unknown;
            this.OrderViewModel = new OrderViewModel();
        }
    }
}