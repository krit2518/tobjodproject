﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TobJod.Models;

namespace TobJod.Web.Models {
    public class ProductMotorViewModel : BaseViewModel {
        public List<SelectListItem> CarBrands { get; set; }
        public List<NameValue> CarFamilies { get; set; }
        public List<NameValue> CarModels { get; set; }
        public List<SelectListItem> CarProvinces { get; set; }
        public List<SelectListItem> DriverAges { get; set; }
        public int Category { get; set; }
        public int Brand { get; set; }
        public int Family { get; set; }
        public int Model { get; set; }
        public int Province { get; set; }
        public int Usage { get; set; }
        public int Year { get; set; }
        public string Price { get; set; }
        public AllYesNoFlag CCTV { get; set; }
        public int Compulsory { get; set; }
        //public AllYesNoFlag Accessory { get; set; }
        public AllYesNoFlag Garage { get; set; }
        public AllYesNoFlag DriverSpecify { get; set; }
        public int DriverAge { get; set; }

        public int PriceFrom { get; set; }
        public int PriceTo { get; set; }
        //public int PriceMin { get; set; }
        //public int PriceMax { get; set; }

        public string BrandTitle { get; set; }
        public string ModelTitle { get; set; }

        public List<ProductItem> Products { get; set; }
        
        public string PathCompany { get; set; }

        public bool LayoutGridView { get; set; }

        public ProductMotorViewModelOrderType Order { get; set; }

        public enum ProductMotorViewModelOrderType {
            PriceAsc = 1,
            PriceDesc = 2,
            TitleAsc = 3,
            TitleDesc = 4
        }

        public ProductMotorViewModel() {
            Compulsory = 0;
        }
    }
}