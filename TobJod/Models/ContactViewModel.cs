﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TobJod.Models;

namespace TobJod.Web.Models {
    public class ContactViewModel : Lead {
        public ConfigPage ConfigPage { get; set; }
        public string CallBackDay { get; set; }
        public string CallBackTime { get; set; }

        public List<SelectListItem> SubCategoryList { get; set; }
        public List<DateTime> DayOff { get; set; }

        public bool EnableCaptcha { get; set; }
        public ContactViewModel() {
            DayOff = new List<DateTime>();
        }
    }
}