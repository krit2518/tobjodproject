﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TobJod.Models;

namespace TobJod.Web.Models {
    public class HomeViewModel {
        public List<Banner> Banner { get; set; }
        public List<PartnerBanner> PartnerBanner { get; set; }
        public string BannerTransition { get; set; }
        public string BannerDuration { get; set; }
        public string Promotion1Image { get; set; }
        public string Promotion1ImageMobile { get; set; }
        public string Promotion1Url { get; set; }
        public string Promotion1Target { get; set; }
        public string Promotion2Image { get; set; }
        public string Promotion2ImageMobile { get; set; }
        public string Promotion2Url { get; set; }
        public string Promotion2Target { get; set; }
        public string Promotion3Image { get; set; }
        public string Promotion3ImageMobile { get; set; }
        public string Promotion3Url { get; set; }
        public string Promotion3Target { get; set; }
        public string Promotion4Image { get; set; }
        public string Promotion4ImageMobile { get; set; }
        public string Promotion4Url { get; set; }
        public string Promotion4Target { get; set; }

        //public Dictionary<int, ProductCategory> ProductCategoryOnHome { get; set; }
        public List<UserTestimonial> UserTestimonial { get; set; }

        public Popup Popup { get; set; }

        public string PathBanner { get; set; }
        public string PathPartnerBanner { get; set; }
        public string PathConfig { get; set; }
        public string PathTestimonial { get; set; }
    }
}