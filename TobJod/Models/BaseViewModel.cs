﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace TobJod.Web.Models {
    public class BaseViewModel {
        public string FORMAT_MONEY { get { return "#,##0.00";  } }
        public CultureInfo CultureTH;
        public CultureInfo CultureEN;
        public BaseViewModel() {
            CultureEN = new CultureInfo("en-GB");
            CultureTH = new CultureInfo("th-TH");
        }
    }
}