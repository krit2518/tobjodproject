﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TobJod.Models;

namespace TobJod.Web.Models {
    public class ProductTAViewModel : BaseViewModel {

        public int SchengenId { get { return TobJod.Models.Country.SchengenId; } }

        public int TripType { get; set; }
        public int Zone { get; set; }
        public int CoverageOption { get; set; }
        public int CoverageType { get; set; }
        public int Day { get; set; }
        public string Price { get; set; }
        public int Country { get; set; }
        public int Person { get; set; }

        public string DateFrom { get; set; }
        public string DateTo { get; set; }

        public bool IsSchengen { get { return Country == SchengenId; } }

        public int PriceFrom { get; set; }
        public int PriceTo { get; set; }

        public string CountryTitle { get; set; }
        public string TripTypeTitle { get; set; }
        public string CoverageOptionTitle { get; set; }
        public string CoverageTypeTitle { get; set; }

        public List<ProductItem> Products { get; set; }
        
        public string PathCompany { get; set; }

        public bool LayoutGridView { get; set; }

        public ProductTAViewModelOrderType Order { get; set; }

        public enum ProductTAViewModelOrderType {
            PriceAsc = 1,
            PriceDesc = 2,
            TitleAsc = 3,
            TitleDesc = 4
        }
        
    }
}