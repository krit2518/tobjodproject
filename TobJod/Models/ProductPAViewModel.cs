﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TobJod.Models;

namespace TobJod.Web.Models {
    public class ProductPAViewModel : BaseViewModel {

        public int Career { get; set; }
        public DateTime BirthDay { get; set; }
        public string Price { get; set; }


        public int Age { get; set; }

        public int PriceFrom { get; set; }
        public int PriceTo { get; set; }

        public string CareerTitle { get; set; }

        public List<ProductItem> Products { get; set; }
        
        public string PathCompany { get; set; }

        public bool LayoutGridView { get; set; }

        public ProductPAViewModelOrderType Order { get; set; }

        public enum ProductPAViewModelOrderType {
            PriceAsc = 1,
            PriceDesc = 2,
            TitleAsc = 3,
            TitleDesc = 4
        }
        
    }
}