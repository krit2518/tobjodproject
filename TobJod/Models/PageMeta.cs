﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TobJod.Models;
using TobJod.Utils;

namespace TobJod.Web.Models {
    public class PageMeta {
        public string Title { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeyword { get; set; }
        public string OGTitle { get; set; }
        public string OGImage { get; set; }
        public string OGDescription { get; set; }

        public void FromConfigPage(ConfigPage item, Language langauge) {
            if (langauge == Language.TH) {
                Title = item.MetaTitleTH;
                MetaDescription = item.MetaDescriptionTH;
                MetaKeyword = item.MetaKeywordTH;
                OGTitle = item.OGTitleTH;
                OGImage = (!string.IsNullOrEmpty(item.OGImageTH) ? HttpUtils.GetPathUrlAbsolute(System.Configuration.ConfigurationManager.AppSettings["Path_ConfigPage"]) + item.OGImageTH : OGImage);
                OGDescription = item.OGDescriptionTH;
            } else {
                Title = item.MetaTitleEN;
                MetaDescription = item.MetaDescriptionEN;
                MetaKeyword = item.MetaKeywordEN;
                OGTitle = item.OGTitleEN;
                OGImage = (!string.IsNullOrEmpty(item.OGImageEN) ? HttpUtils.GetPathUrlAbsolute(System.Configuration.ConfigurationManager.AppSettings["Path_ConfigPage"]) + item.OGImageEN : OGImage);
                OGDescription = item.OGDescriptionEN;
            }
        }
    }
}