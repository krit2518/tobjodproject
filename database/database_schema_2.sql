USE [master]
GO
/****** Object:  Database [TOBJOD_2]    Script Date: 20/12/2560 15:48:37 ******/
CREATE DATABASE [TOBJOD_2]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'TOBJOD_2_Data', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\TOBJOD_2_Data.mdf' , SIZE = 24320KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10%)
 LOG ON 
( NAME = N'TOBJOD_2_Log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\TOBJOD_2_Log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 1024KB )
GO
ALTER DATABASE [TOBJOD_2] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [TOBJOD_2].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [TOBJOD_2] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [TOBJOD_2] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [TOBJOD_2] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [TOBJOD_2] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [TOBJOD_2] SET ARITHABORT OFF 
GO
ALTER DATABASE [TOBJOD_2] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [TOBJOD_2] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [TOBJOD_2] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [TOBJOD_2] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [TOBJOD_2] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [TOBJOD_2] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [TOBJOD_2] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [TOBJOD_2] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [TOBJOD_2] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [TOBJOD_2] SET  ENABLE_BROKER 
GO
ALTER DATABASE [TOBJOD_2] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [TOBJOD_2] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [TOBJOD_2] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [TOBJOD_2] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [TOBJOD_2] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [TOBJOD_2] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [TOBJOD_2] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [TOBJOD_2] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [TOBJOD_2] SET  MULTI_USER 
GO
ALTER DATABASE [TOBJOD_2] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [TOBJOD_2] SET DB_CHAINING OFF 
GO
ALTER DATABASE [TOBJOD_2] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [TOBJOD_2] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [TOBJOD_2] SET DELAYED_DURABILITY = DISABLED 
GO
USE [TOBJOD_2]
GO
/****** Object:  Table [dbo].[Admin]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Admin](
	[Id] [varchar](8) NOT NULL,
	[EmployeeId] [varchar](8) NOT NULL,
	[AdminGroupId] [int] NOT NULL,
	[Name] [varchar](250) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Admin_Shadow]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Admin_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [varchar](8) NOT NULL,
	[EmployeeId] [varchar](250) NOT NULL,
	[AdminGroupId] [int] NOT NULL,
	[Name] [varchar](250) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AdminAuthenLog]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AdminAuthenLog](
	[AdminId] [varchar](8) NOT NULL,
	[SessionId] [varchar](250) NOT NULL,
	[Login] [datetime] NOT NULL,
	[Logout] [datetime] NULL,
	[PublicIP] [varchar](50) NOT NULL,
	[LocalIP] [varchar](50) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AdminGroup]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AdminGroup](
	[Id] [int] NOT NULL,
	[Title] [varchar](250) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AdminGroup_Shadow]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AdminGroup_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[Title] [varchar](250) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AdminGroupPermission]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AdminGroupPermission](
	[AdminGroupId] [int] NOT NULL,
	[Module] [varchar](50) NOT NULL,
	[Can_View] [bit] NOT NULL,
	[Can_Add] [bit] NOT NULL,
	[Can_Edit] [bit] NOT NULL,
	[Can_Delete] [bit] NOT NULL,
	[Can_Approve] [bit] NOT NULL,
	[Can_Assign] [bit] NOT NULL,
	[Can_Export] [bit] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AdminMenu]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AdminMenu](
	[Module] [varchar](50) NOT NULL,
	[ParentModule] [varchar](50) NOT NULL,
	[Title] [varchar](50) NOT NULL,
	[Icon] [varchar](50) NOT NULL,
	[Has_View] [bit] NOT NULL,
	[Has_Add] [bit] NOT NULL,
	[Has_Edit] [bit] NOT NULL,
	[Has_Delete] [bit] NOT NULL,
	[Has_Approve] [bit] NOT NULL,
	[Has_Assign] [bit] NOT NULL,
	[Has_Export] [bit] NOT NULL,
	[Sequence] [int] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[MenuType] [tinyint] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Bank]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Bank](
	[Id] [int] NOT NULL,
	[CoId] [varchar](3) NOT NULL,
	[CoCode] [varchar](10) NOT NULL,
	[CoTitleTH] [varchar](80) NOT NULL,
	[CoTitleEN] [varchar](80) NOT NULL,
	[ReId] [varchar](10) NOT NULL,
	[ReCode] [varchar](10) NOT NULL,
	[ReTitleTH] [varchar](50) NOT NULL,
	[ReTitleEN] [varchar](50) NOT NULL,
	[AlId] [varchar](3) NOT NULL,
	[AlCode] [varchar](10) NOT NULL,
	[AlTitleTH] [varchar](80) NOT NULL,
	[AlTitleEN] [varchar](80) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Banner]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Banner](
	[Id] [int] NOT NULL,
	[ImageTH] [varchar](250) NOT NULL,
	[ImageEN] [varchar](250) NOT NULL,
	[ImageMobileTH] [varchar](250) NOT NULL,
	[ImageMobileEN] [varchar](250) NOT NULL,
	[UrlTH] [varchar](250) NOT NULL,
	[UrlEN] [varchar](250) NOT NULL,
	[ActiveDate] [date] NOT NULL,
	[ExpireDate] [date] NOT NULL,
	[Sequence] [float] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[ApproveStatus] [tinyint] NOT NULL,
	[ApproveAdminId] [varchar](8) NOT NULL,
	[OwnerAdminId] [varchar](8) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Banner_Shadow]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Banner_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[ImageTH] [varchar](250) NOT NULL,
	[ImageEN] [varchar](250) NOT NULL,
	[ImageMobileTH] [varchar](250) NOT NULL,
	[ImageMobileEN] [varchar](250) NOT NULL,
	[UrlTH] [varchar](250) NOT NULL,
	[UrlEN] [varchar](250) NOT NULL,
	[ActiveDate] [date] NOT NULL,
	[ExpireDate] [date] NOT NULL,
	[Sequence] [float] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[ApproveStatus] [tinyint] NOT NULL,
	[ApproveAdminId] [varchar](8) NOT NULL,
	[OwnerAdminId] [varchar](8) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Campaign]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Campaign](
	[Id] [int] NOT NULL,
	[ParentId] [int] NOT NULL,
	[Code] [varchar](50) NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[BodyTH] [varchar](max) NOT NULL,
	[BodyEN] [varchar](max) NOT NULL,
	[ImageTH] [varchar](250) NOT NULL,
	[ImageEN] [varchar](250) NOT NULL,
	[ImageAltTH] [varchar](250) NOT NULL,
	[ImageAltEN] [varchar](250) NOT NULL,
	[IsGuest] [bit] NOT NULL,
	[IsMemberSilver] [bit] NOT NULL,
	[IsMemberGold] [bit] NOT NULL,
	[IsMemberPlatinum] [bit] NOT NULL,
	[PromotionAll] [bit] NOT NULL,
	[ActiveDate] [date] NOT NULL,
	[ExpireDate] [date] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[ApproveStatus] [tinyint] NOT NULL,
	[ApproveAdminId] [varchar](8) NOT NULL,
	[OwnerAdminId] [varchar](8) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Campaign_Shadow]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Campaign_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[ParentId] [int] NOT NULL,
	[Code] [varchar](50) NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[BodyTH] [varchar](max) NOT NULL,
	[BodyEN] [varchar](max) NOT NULL,
	[ImageTH] [varchar](250) NOT NULL,
	[ImageEN] [varchar](250) NOT NULL,
	[ImageAltTH] [varchar](250) NOT NULL,
	[ImageAltEN] [varchar](250) NOT NULL,
	[IsGuest] [bit] NOT NULL,
	[IsMemberSilver] [bit] NOT NULL,
	[IsMemberGold] [bit] NOT NULL,
	[IsMemberPlatinum] [bit] NOT NULL,
	[PromotionAll] [bit] NOT NULL,
	[ActiveDate] [date] NOT NULL,
	[ExpireDate] [date] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[ApproveStatus] [tinyint] NOT NULL,
	[ApproveAdminId] [varchar](8) NOT NULL,
	[OwnerAdminId] [varchar](8) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL,
	[Serialize] [varchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CampaignPromotion]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CampaignPromotion](
	[CampaignId] [int] NOT NULL,
	[PromotionId] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CarBrand]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CarBrand](
	[Id] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[MapName] [varchar](250) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CarBrand_Shadow]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CarBrand_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[MapName] [varchar](250) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CarModel]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CarModel](
	[Id] [int] NOT NULL,
	[BrandId] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[MapName] [varchar](250) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CarModel_Shadow]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CarModel_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[BrandId] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[MapName] [varchar](250) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Company]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Company](
	[Id] [int] NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[PrefixTH] [varchar](15) NOT NULL,
	[PrefixEN] [varchar](15) NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[CoCode] [varchar](10) NOT NULL,
	[CoPrefixTH] [varchar](15) NOT NULL,
	[CoPrefixEN] [varchar](15) NOT NULL,
	[CoTitleTH] [varchar](255) NOT NULL,
	[CoTitleEN] [varchar](255) NOT NULL,
	[ReCode] [varchar](5) NOT NULL,
	[RePrefixTH] [varchar](15) NOT NULL,
	[RePrefixEN] [varchar](15) NOT NULL,
	[ReTitleTH] [varchar](50) NOT NULL,
	[ReTitleEN] [varchar](50) NOT NULL,
	[AlCode] [varchar](10) NOT NULL,
	[AlPrefixTH] [varchar](15) NOT NULL,
	[AlPrefixEN] [varchar](15) NOT NULL,
	[AlTitleTH] [varchar](255) NOT NULL,
	[AlTitleEN] [varchar](255) NOT NULL,
	[ImageTH] [varchar](250) NOT NULL,
	[ImageEN] [varchar](250) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Company_Shadow]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Company_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[PrefixTH] [varchar](15) NOT NULL,
	[PrefixEN] [varchar](15) NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[CoCode] [varchar](10) NOT NULL,
	[CoPrefixTH] [varchar](15) NOT NULL,
	[CoPrefixEN] [varchar](15) NOT NULL,
	[CoTitleTH] [varchar](255) NOT NULL,
	[CoTitleEN] [varchar](255) NOT NULL,
	[ReCode] [varchar](5) NOT NULL,
	[RePrefixTH] [varchar](15) NOT NULL,
	[RePrefixEN] [varchar](15) NOT NULL,
	[ReTitleTH] [varchar](50) NOT NULL,
	[ReTitleEN] [varchar](50) NOT NULL,
	[AlCode] [varchar](10) NOT NULL,
	[AlPrefixTH] [varchar](15) NOT NULL,
	[AlPrefixEN] [varchar](15) NOT NULL,
	[AlTitleTH] [varchar](255) NOT NULL,
	[AlTitleEN] [varchar](255) NOT NULL,
	[ImageTH] [varchar](250) NOT NULL,
	[ImageEN] [varchar](250) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Config]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Config](
	[Id] [int] NOT NULL,
	[Title] [varchar](250) NOT NULL,
	[Body] [text] NOT NULL,
	[AutoLoad] [bit] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Config_Shadow]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Config_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[Title] [varchar](250) NOT NULL,
	[Body] [text] NOT NULL,
	[AutoLoad] [bit] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ConfigPage]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ConfigPage](
	[Id] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[BodyTH] [varchar](max) NOT NULL,
	[BodyEN] [varchar](max) NOT NULL,
	[MetaTitleTH] [varchar](250) NOT NULL,
	[MetaTitleEN] [varchar](250) NOT NULL,
	[MetaKeywordTH] [varchar](max) NOT NULL,
	[MetaKeywordEN] [varchar](max) NOT NULL,
	[MetaDescriptionTH] [varchar](max) NOT NULL,
	[MetaDescriptionEN] [varchar](max) NOT NULL,
	[OGTitleTH] [varchar](250) NOT NULL,
	[OGTitleEN] [varchar](250) NOT NULL,
	[OGImageTH] [varchar](250) NOT NULL,
	[OGImageEN] [varchar](250) NOT NULL,
	[OGDescriptionTH] [varchar](max) NOT NULL,
	[OGDescriptionEN] [varchar](max) NOT NULL,
	[Data1] [varchar](max) NOT NULL,
	[Data2] [varchar](max) NOT NULL,
	[Data3] [varchar](max) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ConfigPage_Shadow]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ConfigPage_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[BodyTH] [varchar](max) NOT NULL,
	[BodyEN] [varchar](max) NOT NULL,
	[MetaTitleTH] [varchar](250) NOT NULL,
	[MetaTitleEN] [varchar](250) NOT NULL,
	[MetaKeywordTH] [varchar](max) NOT NULL,
	[MetaKeywordEN] [varchar](max) NOT NULL,
	[MetaDescriptionTH] [varchar](max) NOT NULL,
	[MetaDescriptionEN] [varchar](max) NOT NULL,
	[OGTitleTH] [varchar](250) NOT NULL,
	[OGTitleEN] [varchar](250) NOT NULL,
	[OGImageTH] [varchar](250) NOT NULL,
	[OGImageEN] [varchar](250) NOT NULL,
	[OGDescriptionTH] [varchar](max) NOT NULL,
	[OGDescriptionEN] [varchar](max) NOT NULL,
	[Data1] [varchar](max) NOT NULL,
	[Data2] [varchar](max) NOT NULL,
	[Data3] [varchar](max) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ContactCategory]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContactCategory](
	[Id] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[Sequence] [float] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ContactCategory_Shadow]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContactCategory_Shadow](
	[Action] [varchar](250) NOT NULL,
	[Id] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[Sequence] [float] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ContactSubCategory]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContactSubCategory](
	[Id] [int] NOT NULL,
	[CategoryId] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[MailTo] [varchar](max) NOT NULL,
	[Sequence] [float] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ContactSubCategory_Shadow]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContactSubCategory_Shadow](
	[Action] [varchar](250) NOT NULL,
	[Id] [int] NOT NULL,
	[CategoryId] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[MailTo] [varchar](max) NOT NULL,
	[Sequence] [float] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Country]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Country](
	[Id] [int] NOT NULL,
	[Code] [varchar](250) NOT NULL,
	[Zone] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Country_Shadow]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Country_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[Code] [varchar](50) NOT NULL,
	[Zone] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Coupon]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Coupon](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CouponType] [tinyint] NOT NULL,
	[TitleTH] [varchar](50) NOT NULL,
	[TitleEN] [varchar](50) NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Total] [int] NOT NULL,
	[DiscountType] [tinyint] NOT NULL,
	[DiscountAmount] [money] NOT NULL,
	[ActiveDate] [date] NOT NULL,
	[ExpireDate] [date] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateBy] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateBy] [varchar](8) NOT NULL,
 CONSTRAINT [PK_Coupon] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CouponCode]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CouponCode](
	[CouponId] [int] NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Used] [int] NOT NULL,
 CONSTRAINT [PK_CouponCode] PRIMARY KEY CLUSTERED 
(
	[CouponId] ASC,
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CouponCodeLog]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CouponCodeLog](
	[CouponId] [int] NOT NULL,
	[CouponCode] [varchar](10) NOT NULL,
	[CreateDate] [datetime] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DayOff]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DayOff](
	[Id] [int] NOT NULL,
	[EventDate] [date] NOT NULL,
	[TitleTH] [varchar](50) NOT NULL,
	[TitleEN] [varchar](50) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DayOff_Shadow]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DayOff_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[EventDate] [date] NOT NULL,
	[TitleTH] [varchar](50) NOT NULL,
	[TitleEN] [varchar](50) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Faq]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Faq](
	[Id] [int] NOT NULL,
	[CategoryId] [int] NOT NULL,
	[QuestionTH] [varchar](250) NOT NULL,
	[QuestionEN] [varchar](250) NOT NULL,
	[AnswerTH] [varchar](max) NOT NULL,
	[AnswerEN] [varchar](max) NOT NULL,
	[Pin] [bit] NOT NULL,
	[Sequence] [float] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Faq_Shadow]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Faq_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[CategoryId] [int] NOT NULL,
	[QuestionTH] [varchar](250) NOT NULL,
	[QuestionEN] [varchar](250) NOT NULL,
	[AnswerTH] [varchar](max) NOT NULL,
	[AnswerEN] [varchar](max) NOT NULL,
	[Pin] [bit] NOT NULL,
	[Sequence] [float] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Lead]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Lead](
	[Id] [int] NOT NULL,
	[Source] [tinyint] NOT NULL,
	[ProductId] [int] NOT NULL,
	[PremiumId] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Surname] [varchar](50) NOT NULL,
	[Tel] [varchar](50) NOT NULL,
	[Email] [varchar](250) NOT NULL,
	[CallBackDate] [datetime] NOT NULL,
	[SubjectId] [int] NOT NULL,
	[Message] [varchar](max) NOT NULL,
	[Remark] [varchar](max) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[LeadStatus] [tinyint] NOT NULL,
	[OwnerAdminId] [varchar](8) NOT NULL,
	[AssignAdminId] [varchar](8) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Lead_Shadow]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Lead_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[Source] [tinyint] NOT NULL,
	[ProductId] [int] NOT NULL,
	[PremiumId] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Surname] [varchar](50) NOT NULL,
	[Tel] [varchar](50) NOT NULL,
	[Email] [varchar](250) NOT NULL,
	[CallBackDate] [datetime] NOT NULL,
	[SubjectId] [int] NOT NULL,
	[Message] [varchar](max) NOT NULL,
	[Remark] [varchar](max) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[LeadStatus] [tinyint] NOT NULL,
	[OwnerAdminId] [varchar](8) NOT NULL,
	[AssignAdminId] [varchar](8) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LeadCriteria]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LeadCriteria](
	[LeadId] [int] NOT NULL,
	[SubCategoryId] [int] NOT NULL,
	[HousePropertyType] [int] NOT NULL,
	[HouseConstructionType] [int] NOT NULL,
	[HousePropertyValue] [int] NOT NULL,
	[HouseConstructionValue] [int] NOT NULL,
	[HouseYear] [int] NOT NULL,
	[MotorBrandId] [int] NOT NULL,
	[MotorModelId] [int] NOT NULL,
	[MotorUsage] [int] NOT NULL,
	[MotorYear] [int] NOT NULL,
	[MotorCCTV] [bit] NOT NULL,
	[MotorPRB] [bit] NOT NULL,
	[MotorAccessory] [bit] NOT NULL,
	[MotorGarage] [bit] NOT NULL,
	[CareerId] [int] NOT NULL,
	[Birthday] [date] NOT NULL,
	[Sex] [tinyint] NOT NULL,
	[Weight] [int] NOT NULL,
	[Height] [int] NOT NULL,
	[TATripType] [int] NOT NULL,
	[TAZone] [int] NOT NULL,
	[TACoverageOption] [int] NOT NULL,
	[TACoverageType] [int] NOT NULL,
	[TADays] [tinyint] NOT NULL,
	[TACountryId] [int] NOT NULL,
	[TAPerson] [tinyint] NOT NULL,
	[PriceFrom] [int] NOT NULL,
	[PriceTo] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LeadForm]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LeadForm](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Surname] [varchar](50) NOT NULL,
	[Telephone] [varchar](50) NOT NULL,
	[Email] [varchar](250) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_LeadForm] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LeadLog]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LeadLog](
	[Id] [int] NOT NULL,
	[LeadId] [int] NOT NULL,
	[LeadStatus] [tinyint] NOT NULL,
	[Remark] [varchar](max) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[AssignAdminId] [varchar](8) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MailLog]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MailLog](
	[EmailFrom] [varchar](250) NOT NULL,
	[EmailTo] [varchar](max) NOT NULL,
	[EmailCC] [varchar](max) NOT NULL,
	[EmailBCC] [varchar](max) NOT NULL,
	[Subject] [varchar](max) NOT NULL,
	[Success] [bit] NOT NULL,
	[ErrorMessage] [varchar](max) NOT NULL,
	[CreateDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MasterOption]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MasterOption](
	[Id] [int] NOT NULL,
	[Category] [int] NOT NULL,
	[RefId] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[Code] [varchar](250) NOT NULL,
	[Mode] [tinyint] NOT NULL,
	[Sequence] [float] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MasterOption_Shadow]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MasterOption_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[Category] [int] NOT NULL,
	[RefId] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[Code] [varchar](250) NOT NULL,
	[Mode] [tinyint] NOT NULL,
	[Sequence] [float] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[News]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[News](
	[Id] [int] NOT NULL,
	[NewsSubCategoryId] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[UrlPrefix] [varchar](50) NOT NULL,
	[UrlTH] [varchar](250) NOT NULL,
	[UrlEN] [varchar](250) NOT NULL,
	[BriefTH] [varchar](max) NOT NULL,
	[BriefEN] [varchar](max) NOT NULL,
	[BodyTH] [varchar](max) NOT NULL,
	[BodyEN] [varchar](max) NOT NULL,
	[ImageTH] [varchar](250) NOT NULL,
	[ImageEN] [varchar](250) NOT NULL,
	[ImageTitleTH] [varchar](250) NOT NULL,
	[ImageTitleEN] [varchar](250) NOT NULL,
	[ImageAltTH] [varchar](250) NOT NULL,
	[ImageAltEN] [varchar](250) NOT NULL,
	[MetaTitleTH] [varchar](250) NOT NULL,
	[MetaTitleEN] [varchar](250) NOT NULL,
	[MetaKeywordTH] [varchar](max) NOT NULL,
	[MetaKeywordEN] [varchar](max) NOT NULL,
	[MetaDescriptionTH] [varchar](max) NOT NULL,
	[MetaDescriptionEN] [varchar](max) NOT NULL,
	[OGTitleTH] [varchar](250) NOT NULL,
	[OGTitleEN] [varchar](250) NOT NULL,
	[OGImageTH] [varchar](250) NOT NULL,
	[OGImageEN] [varchar](250) NOT NULL,
	[OGDescriptionTH] [varchar](max) NOT NULL,
	[OGDescriptionEN] [varchar](max) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[ApproveStatus] [tinyint] NOT NULL,
	[ApproveAdminId] [varchar](8) NOT NULL,
	[OwnerAdminId] [varchar](8) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[News_Shadow]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[News_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[NewsSubCategoryId] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[UrlPrefix] [varchar](50) NOT NULL,
	[UrlTH] [varchar](250) NOT NULL,
	[UrlEN] [varchar](250) NOT NULL,
	[BriefTH] [varchar](max) NOT NULL,
	[BriefEN] [varchar](max) NOT NULL,
	[BodyTH] [varchar](max) NOT NULL,
	[BodyEN] [varchar](max) NOT NULL,
	[ImageTH] [varchar](250) NOT NULL,
	[ImageEN] [varchar](250) NOT NULL,
	[ImageTitleTH] [varchar](250) NOT NULL,
	[ImageTitleEN] [varchar](250) NOT NULL,
	[ImageAltTH] [varchar](250) NOT NULL,
	[ImageAltEN] [varchar](250) NOT NULL,
	[MetaTitleTH] [varchar](250) NOT NULL,
	[MetaTitleEN] [varchar](250) NOT NULL,
	[MetaKeywordTH] [varchar](max) NOT NULL,
	[MetaKeywordEN] [varchar](max) NOT NULL,
	[MetaDescriptionTH] [varchar](max) NOT NULL,
	[MetaDescriptionEN] [varchar](max) NOT NULL,
	[OGTitleTH] [varchar](250) NOT NULL,
	[OGTitleEN] [varchar](250) NOT NULL,
	[OGImageTH] [varchar](250) NOT NULL,
	[OGImageEN] [varchar](250) NOT NULL,
	[OGDescriptionTH] [varchar](max) NOT NULL,
	[OGDescriptionEN] [varchar](max) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[ApproveStatus] [tinyint] NOT NULL,
	[ApproveAdminId] [varchar](8) NOT NULL,
	[OwnerAdminId] [varchar](8) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewsImage]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsImage](
	[Id] [int] NOT NULL,
	[NewsId] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[Image] [varchar](250) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewsSubCategory]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsSubCategory](
	[Id] [int] NOT NULL,
	[NewsCategory] [tinyint] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewsSubCategory_Shadow]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsSubCategory_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[NewsCategory] [tinyint] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewsTag]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsTag](
	[NewsId] [int] NOT NULL,
	[TagId] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Order]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Order](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[PremiumId] [int] NOT NULL,
	[CANo] [varchar](50) NOT NULL,
	[PaymentChannel] [tinyint] NOT NULL,
	[PaymentStatus] [smallint] NOT NULL,
	[PaymentDate] [datetime] NOT NULL,
	[PaymentInstallment] [tinyint] NOT NULL,
	[PaymentTotal] [money] NOT NULL,
	[PartnerAPIStatus] [tinyint] NOT NULL,
	[BackendAPIStatus] [tinyint] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Order] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrderBackendAPILog]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrderBackendAPILog](
	[OrderId] [int] NOT NULL,
	[Response] [varchar](max) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrderPartnerAPILog]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrderPartnerAPILog](
	[OrderId] [int] NOT NULL,
	[Response] [varchar](max) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrderPaymentAPILog]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrderPaymentAPILog](
	[OrderId] [int] NOT NULL,
	[Response] [varchar](max) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrderPaymentLog]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderPaymentLog](
	[QuotationId] [int] NOT NULL,
	[PaymentStatus] [smallint] NOT NULL,
	[CreateDate] [datetime] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OrderProductHomePremium]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrderProductHomePremium](
	[OrderId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[PropertyTypeId] [int] NOT NULL,
	[ConstructionTypeId] [int] NOT NULL,
	[CoverPeriod] [tinyint] NOT NULL,
	[SumInsured] [money] NOT NULL,
	[NetPremium] [money] NOT NULL,
	[Duty] [money] NOT NULL,
	[Vat] [money] NOT NULL,
	[Premium] [money] NOT NULL,
	[DisplayPremium] [money] NULL,
	[FireExplosionVehicleWater] [money] NOT NULL,
	[AccidentalDamage] [money] NOT NULL,
	[NaturalRisk] [money] NOT NULL,
	[TempRental] [money] NOT NULL,
	[PublicLiability] [money] NOT NULL,
	[Burglary] [money] NOT NULL,
	[EPolicy] [tinyint] NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrderProductMotorPremium]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderProductMotorPremium](
	[OrderId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[BrandId] [int] NOT NULL,
	[ModelId] [int] NOT NULL,
	[YearMin] [tinyint] NOT NULL,
	[YearMax] [tinyint] NOT NULL,
	[RegisterYear] [int] NOT NULL,
	[VehicleUsage110] [bit] NOT NULL,
	[VehicleUsage120] [bit] NOT NULL,
	[VehicleUsage210] [bit] NOT NULL,
	[VehicleUsage220] [bit] NOT NULL,
	[VehicleUsage230] [bit] NOT NULL,
	[VehicleUsage320] [bit] NOT NULL,
	[GarageType] [tinyint] NOT NULL,
	[CarInspector] [bit] NOT NULL,
	[CCTV] [tinyint] NOT NULL,
	[DriverAgeMin] [tinyint] NULL,
	[DriverAgeMax] [tinyint] NULL,
	[SumInsured] [money] NOT NULL,
	[NetPremium] [money] NOT NULL,
	[Duty] [money] NOT NULL,
	[Vat] [money] NOT NULL,
	[Premium] [money] NOT NULL,
	[DisplayPremium] [money] NULL,
	[CompulsoryPremium] [money] NULL,
	[DisplayCompulsoryPremium] [money] NULL,
	[CombinePremium] [money] NULL,
	[DisplayCombinePremium] [money] NULL,
	[OwnDamage] [money] NOT NULL,
	[OwnExcess] [money] NOT NULL,
	[Flood] [money] NOT NULL,
	[EPolicy] [tinyint] NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [int] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OrderProductPAPremium]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrderProductPAPremium](
	[OrderId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[InsuranceType] [tinyint] NOT NULL,
	[AgeCalculation] [tinyint] NOT NULL,
	[MinAge] [tinyint] NOT NULL,
	[MaxAge] [tinyint] NOT NULL,
	[CareerClass1] [bit] NOT NULL,
	[CareerClass2] [bit] NOT NULL,
	[CareerClass3] [bit] NOT NULL,
	[CareerClass4] [bit] NOT NULL,
	[NetPremium] [money] NOT NULL,
	[Duty] [money] NOT NULL,
	[Vat] [money] NOT NULL,
	[Premium] [money] NOT NULL,
	[DisplayPremium] [money] NULL,
	[LossLife] [money] NOT NULL,
	[MedicalExpense] [money] NOT NULL,
	[MurderAssault] [money] NOT NULL,
	[RidingPassengerMotorcycle] [money] NOT NULL,
	[IncomeCompensationTH] [varchar](250) NOT NULL,
	[IncomeCompensationEN] [varchar](250) NOT NULL,
	[FuneralExpenseAccidentIllness] [money] NOT NULL,
	[EPolicy] [tinyint] NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [int] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [int] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrderProductPHPremium]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrderProductPHPremium](
	[OrderId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[Sex] [tinyint] NOT NULL,
	[AgeCalculation] [tinyint] NOT NULL,
	[MinAgeMonth] [tinyint] NOT NULL,
	[MinAge] [tinyint] NOT NULL,
	[MaxAge] [tinyint] NOT NULL,
	[CareerClass1] [bit] NOT NULL,
	[CareerClass2] [bit] NOT NULL,
	[CareerClass3] [bit] NOT NULL,
	[CareerClass4] [bit] NOT NULL,
	[MinWeight] [tinyint] NOT NULL,
	[MaxWeight] [tinyint] NOT NULL,
	[MinHeight] [tinyint] NOT NULL,
	[MaxHeight] [tinyint] NOT NULL,
	[SumInsured] [money] NOT NULL,
	[NetPremium] [money] NOT NULL,
	[Duty] [money] NOT NULL,
	[Vat] [money] NOT NULL,
	[Premium] [money] NOT NULL,
	[DisplayPremium] [money] NULL,
	[IPDPerYear] [money] NOT NULL,
	[IPDRoomBoard] [money] NOT NULL,
	[SurgicalSpecialistTH] [varchar](250) NOT NULL,
	[SurgicalSpecialistEN] [varchar](250) NOT NULL,
	[OutpatientBenefits] [money] NOT NULL,
	[TransplantExpenseTH] [varchar](250) NOT NULL,
	[TransplantExpenseEN] [varchar](250) NOT NULL,
	[CancerTreatment] [money] NOT NULL,
	[EPolicy] [tinyint] NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [int] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [int] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrderProductTAPremium]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrderProductTAPremium](
	[OrderId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[TripType] [tinyint] NOT NULL,
	[Zone] [tinyint] NOT NULL,
	[CoverageOption] [tinyint] NOT NULL,
	[CoverageType] [tinyint] NOT NULL,
	[MinDays] [smallint] NULL,
	[MaxDays] [smallint] NULL,
	[Persons] [tinyint] NOT NULL,
	[SumInsured] [money] NOT NULL,
	[NetPremium] [money] NOT NULL,
	[Duty] [money] NOT NULL,
	[Vat] [money] NOT NULL,
	[Premium] [money] NOT NULL,
	[DisplayPremium] [money] NULL,
	[PersonalLossLife] [money] NOT NULL,
	[MedicalExpenseEachAccident] [money] NOT NULL,
	[MedicalExpenseAccidentSickness] [money] NOT NULL,
	[EmergencyMedialEvacuation] [money] NOT NULL,
	[RepatriationExpense] [money] NULL,
	[RepatriationMortalRemains] [money] NOT NULL,
	[LossDamageBaggageTH] [varchar](250) NOT NULL,
	[LossDamageBaggageEN] [varchar](250) NOT NULL,
	[EPolicy] [tinyint] NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [int] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [int] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PartnerBanner]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PartnerBanner](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[ImageTH] [varchar](50) NOT NULL,
	[ImageEN] [varchar](50) NOT NULL,
	[Sequence] [int] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
 CONSTRAINT [PK_PartnerBanner] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PartnerBanner_Shadow]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[PartnerBanner_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[ImageTH] [varchar](50) NOT NULL,
	[ImageEN] [varchar](50) NOT NULL,
	[Sequence] [int] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PaymentMethod]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PaymentMethod](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[UrlTH] [varchar](250) NOT NULL,
	[UrlEN] [varchar](250) NOT NULL,
	[BriefTH] [varchar](max) NOT NULL,
	[BriefEN] [varchar](max) NOT NULL,
	[BodyTH] [varchar](max) NOT NULL,
	[BodyEN] [varchar](max) NOT NULL,
	[ImageTH] [varchar](250) NOT NULL,
	[ImageEN] [varchar](250) NOT NULL,
	[ImageTitleTH] [varchar](250) NOT NULL,
	[ImageTitleEN] [varchar](250) NOT NULL,
	[ImageAltTH] [varchar](250) NOT NULL,
	[ImageAltEN] [varchar](250) NOT NULL,
	[MetaTitleTH] [varchar](250) NOT NULL,
	[MetaTitleEN] [varchar](250) NOT NULL,
	[MetaKeywordTH] [varchar](max) NOT NULL,
	[MetaKeywordEN] [varchar](max) NOT NULL,
	[MetaDescriptionTH] [varchar](max) NOT NULL,
	[MetaDescriptionEN] [varchar](max) NOT NULL,
	[OGTitleTH] [varchar](250) NOT NULL,
	[OGTitleEN] [varchar](250) NOT NULL,
	[OGImageTH] [varchar](250) NOT NULL,
	[OGImageEN] [varchar](250) NOT NULL,
	[OGDescriptionTH] [varchar](max) NOT NULL,
	[OGDescriptionEN] [varchar](max) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[ApproveStatus] [tinyint] NOT NULL,
	[ApproveAdminId] [varchar](8) NOT NULL,
	[OwnerAdminId] [varchar](8) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
 CONSTRAINT [PK_PaymentMethod] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PaymentMethod_Shadow]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PaymentMethod_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[UrlTH] [varchar](250) NOT NULL,
	[UrlEN] [varchar](250) NOT NULL,
	[BriefTH] [varchar](max) NOT NULL,
	[BriefEN] [varchar](max) NOT NULL,
	[BodyTH] [varchar](max) NOT NULL,
	[BodyEN] [varchar](max) NOT NULL,
	[ImageTH] [varchar](250) NOT NULL,
	[ImageEN] [varchar](250) NOT NULL,
	[ImageTitleTH] [varchar](250) NOT NULL,
	[ImageTitleEN] [varchar](250) NOT NULL,
	[ImageAltTH] [varchar](250) NOT NULL,
	[ImageAltEN] [varchar](250) NOT NULL,
	[MetaTitleTH] [varchar](250) NOT NULL,
	[MetaTitleEN] [varchar](250) NOT NULL,
	[MetaKeywordTH] [varchar](max) NOT NULL,
	[MetaKeywordEN] [varchar](max) NOT NULL,
	[MetaDescriptionTH] [varchar](max) NOT NULL,
	[MetaDescriptionEN] [varchar](max) NOT NULL,
	[OGTitleTH] [varchar](250) NOT NULL,
	[OGTitleEN] [varchar](250) NOT NULL,
	[OGImageTH] [varchar](250) NOT NULL,
	[OGImageEN] [varchar](250) NOT NULL,
	[OGDescriptionTH] [varchar](max) NOT NULL,
	[OGDescriptionEN] [varchar](max) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[ApproveStatus] [tinyint] NOT NULL,
	[ApproveAdminId] [varchar](8) NOT NULL,
	[OwnerAdminId] [varchar](8) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Poi]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Poi](
	[Id] [int] NOT NULL,
	[Category] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[Latitude] [float] NOT NULL,
	[Longitude] [float] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Poi_Shadow]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Poi_Shadow](
	[Action] [varchar](250) NOT NULL,
	[Id] [int] NOT NULL,
	[Category] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[Latitude] [float] NOT NULL,
	[Longitude] [float] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Popup]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Popup](
	[Id] [int] NOT NULL,
	[ImageTH] [varchar](250) NOT NULL,
	[ImageEN] [varchar](250) NOT NULL,
	[UrlTH] [varchar](250) NOT NULL,
	[UrlEN] [varchar](250) NOT NULL,
	[ActiveDate] [date] NOT NULL,
	[ExpireDate] [date] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[ApproveStatus] [tinyint] NOT NULL,
	[ApproveAdminId] [varchar](8) NOT NULL,
	[OwnerAdminId] [varchar](8) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Popup_Shadow]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Popup_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[ImageTH] [varchar](250) NOT NULL,
	[ImageEN] [varchar](250) NOT NULL,
	[UrlTH] [varchar](250) NOT NULL,
	[UrlEN] [varchar](250) NOT NULL,
	[ActiveDate] [date] NOT NULL,
	[ExpireDate] [date] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[ApproveStatus] [tinyint] NOT NULL,
	[ApproveAdminId] [varchar](8) NOT NULL,
	[OwnerAdminId] [varchar](8) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Privilege]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Privilege](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[UrlTH] [varchar](250) NOT NULL,
	[UrlEN] [varchar](250) NOT NULL,
	[BriefTH] [varchar](max) NOT NULL,
	[BriefEN] [varchar](max) NOT NULL,
	[BodyTH] [varchar](max) NOT NULL,
	[BodyEN] [varchar](max) NOT NULL,
	[ImageTH] [varchar](250) NOT NULL,
	[ImageEN] [varchar](250) NOT NULL,
	[ImageTitleTH] [varchar](250) NOT NULL,
	[ImageTitleEN] [varchar](250) NOT NULL,
	[ImageAltTH] [varchar](250) NOT NULL,
	[ImageAltEN] [varchar](250) NOT NULL,
	[MetaTitleTH] [varchar](250) NOT NULL,
	[MetaTitleEN] [varchar](250) NOT NULL,
	[MetaKeywordTH] [varchar](max) NOT NULL,
	[MetaKeywordEN] [varchar](max) NOT NULL,
	[MetaDescriptionTH] [varchar](max) NOT NULL,
	[MetaDescriptionEN] [varchar](max) NOT NULL,
	[OGTitleTH] [varchar](250) NOT NULL,
	[OGTitleEN] [varchar](250) NOT NULL,
	[OGImageTH] [varchar](250) NOT NULL,
	[OGImageEN] [varchar](250) NOT NULL,
	[OGDescriptionTH] [varchar](max) NOT NULL,
	[OGDescriptionEN] [varchar](max) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[ApproveStatus] [tinyint] NOT NULL,
	[ApproveAdminId] [varchar](8) NOT NULL,
	[OwnerAdminId] [varchar](8) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
 CONSTRAINT [PK_Privilege] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Privilege_Shadow]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Privilege_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[UrlTH] [varchar](250) NOT NULL,
	[UrlEN] [varchar](250) NOT NULL,
	[BriefTH] [varchar](max) NOT NULL,
	[BriefEN] [varchar](max) NOT NULL,
	[BodyTH] [varchar](max) NOT NULL,
	[BodyEN] [varchar](max) NOT NULL,
	[ImageTH] [varchar](250) NOT NULL,
	[ImageEN] [varchar](250) NOT NULL,
	[ImageTitleTH] [varchar](250) NOT NULL,
	[ImageTitleEN] [varchar](250) NOT NULL,
	[ImageAltTH] [varchar](250) NOT NULL,
	[ImageAltEN] [varchar](250) NOT NULL,
	[MetaTitleTH] [varchar](250) NOT NULL,
	[MetaTitleEN] [varchar](250) NOT NULL,
	[MetaKeywordTH] [varchar](max) NOT NULL,
	[MetaKeywordEN] [varchar](max) NOT NULL,
	[MetaDescriptionTH] [varchar](max) NOT NULL,
	[MetaDescriptionEN] [varchar](max) NOT NULL,
	[OGTitleTH] [varchar](250) NOT NULL,
	[OGTitleEN] [varchar](250) NOT NULL,
	[OGImageTH] [varchar](250) NOT NULL,
	[OGImageEN] [varchar](250) NOT NULL,
	[OGDescriptionTH] [varchar](max) NOT NULL,
	[OGDescriptionEN] [varchar](max) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[ApproveStatus] [tinyint] NOT NULL,
	[ApproveAdminId] [varchar](8) NOT NULL,
	[OwnerAdminId] [varchar](8) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Product]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Product](
	[Id] [int] NOT NULL,
	[SubCategoryId] [int] NOT NULL,
	[CompanyId] [int] NOT NULL,
	[Class] [varchar](2) NOT NULL,
	[SubClass] [varchar](3) NOT NULL,
	[ProductCode] [varchar](19) NOT NULL,
	[InsuranceProductCode] [varchar](50) NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[ActiveDate] [date] NOT NULL,
	[ExpireDate] [date] NOT NULL,
	[CoverageTH] [varchar](max) NOT NULL,
	[CoverageEN] [varchar](max) NOT NULL,
	[ConditionTH] [varchar](max) NOT NULL,
	[ConditionEN] [varchar](max) NOT NULL,
	[ExceptionTH] [varchar](max) NOT NULL,
	[ExceptionEN] [varchar](max) NOT NULL,
	[PrivilegeTH] [varchar](max) NOT NULL,
	[PrivilegeEN] [varchar](max) NOT NULL,
	[CheckListTH] [varchar](max) NULL,
	[CheckListEN] [varchar](max) NULL,
	[ConsentTH] [varchar](max) NULL,
	[ConsentEN] [varchar](max) NULL,
	[Status] [tinyint] NOT NULL,
	[ApproveStatus] [tinyint] NOT NULL,
	[ApproveAdminId] [varchar](8) NOT NULL,
	[OwnerAdminId] [varchar](8) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Product_Shadow]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Product_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[SubCategoryId] [int] NOT NULL,
	[CompanyId] [int] NOT NULL,
	[Class] [varchar](2) NOT NULL,
	[SubClass] [varchar](3) NOT NULL,
	[ProductCode] [varchar](19) NOT NULL,
	[InsuranceProductCode] [varchar](50) NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[ActiveDate] [date] NOT NULL,
	[ExpireDate] [date] NOT NULL,
	[CoverageTH] [varchar](max) NOT NULL,
	[CoverageEN] [varchar](max) NOT NULL,
	[ConditionTH] [varchar](max) NOT NULL,
	[ConditionEN] [varchar](max) NOT NULL,
	[ExceptionTH] [varchar](max) NOT NULL,
	[ExceptionEN] [varchar](max) NOT NULL,
	[PrivilegeTH] [varchar](max) NOT NULL,
	[PrivilegeEN] [varchar](max) NOT NULL,
	[CheckListTH] [varchar](max) NULL,
	[CheckListEN] [varchar](max) NULL,
	[ConsentTH] [varchar](max) NULL,
	[ConsentEN] [varchar](max) NULL,
	[Status] [tinyint] NOT NULL,
	[ApproveStatus] [tinyint] NOT NULL,
	[ApproveAdminId] [varchar](8) NOT NULL,
	[OwnerAdminId] [varchar](8) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL,
	[Serialize] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductApplication]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductApplication](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[PremiumId] [int] NOT NULL,
	[InsuranceCode] [varchar](50) NOT NULL,
	[ProductCode] [varchar](50) NOT NULL,
	[ProductName] [varchar](50) NOT NULL,
	[SumInsured] [money] NOT NULL,
	[NetPremium] [money] NOT NULL,
	[Duty] [money] NOT NULL,
	[Tax] [money] NOT NULL,
	[Premium] [money] NOT NULL,
	[DisplayPremium] [money] NOT NULL,
	[ActiveDate] [date] NOT NULL,
	[ExpireDate] [date] NOT NULL,
	[CustomerType] [tinyint] NOT NULL,
	[BuyerName] [varchar](50) NOT NULL,
	[BuyerTelephone] [varchar](50) NOT NULL,
	[BuyerMobilePhone] [varchar](50) NOT NULL,
	[BuyerEmail] [varchar](250) NOT NULL,
	[BuyerLineId] [varchar](50) NOT NULL,
	[AddressBranchType] [tinyint] NOT NULL,
	[AddressBranchNo] [tinyint] NOT NULL,
	[AddressNo] [varchar](50) NOT NULL,
	[AddressMoo] [varchar](50) NOT NULL,
	[AddressVillage] [varchar](50) NOT NULL,
	[AddressFloor] [varchar](50) NOT NULL,
	[AddressSoi] [varchar](50) NOT NULL,
	[AddressRoad] [varchar](50) NOT NULL,
	[AddressDistrict] [varchar](50) NOT NULL,
	[AddressSubDistrict] [varchar](50) NOT NULL,
	[AddressProvince] [varchar](50) NOT NULL,
	[AddressPostalCode] [varchar](5) NOT NULL,
	[BillingAddressBranchType] [tinyint] NOT NULL,
	[BillingAddressBranchNo] [tinyint] NOT NULL,
	[BillingAddressNo] [varchar](50) NOT NULL,
	[BillingAddressMoo] [varchar](50) NOT NULL,
	[BillingAddressVillage] [varchar](50) NOT NULL,
	[BillingAddressFloor] [varchar](50) NOT NULL,
	[BillingAddressSoi] [varchar](50) NOT NULL,
	[BillingAddressRoad] [varchar](50) NOT NULL,
	[BillingAddressDistrict] [varchar](50) NOT NULL,
	[BillingAddressSubDistrict] [varchar](50) NOT NULL,
	[BillingAddressProvince] [varchar](50) NOT NULL,
	[BillingAddressPostalCode] [varchar](5) NOT NULL,
	[ShippingAddressBranchType] [tinyint] NOT NULL,
	[ShippingAddressBranchNo] [tinyint] NOT NULL,
	[ShippingAddressNo] [varchar](50) NOT NULL,
	[ShippingAddressMoo] [varchar](50) NOT NULL,
	[ShippingAddressVillage] [varchar](50) NOT NULL,
	[ShippingAddressFloor] [varchar](50) NOT NULL,
	[ShippingAddressSoi] [varchar](50) NOT NULL,
	[ShippingAddressRoad] [varchar](50) NOT NULL,
	[ShippingAddressDistrict] [varchar](50) NOT NULL,
	[ShippingAddressSubDistrict] [varchar](50) NOT NULL,
	[ShippingAddressProvince] [varchar](50) NOT NULL,
	[ShippingAddressPostalCode] [varchar](5) NOT NULL,
 CONSTRAINT [PK_ProductApplication] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductApplicationAttachment]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductApplicationAttachment](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ApplicationId] [int] NOT NULL,
	[FileCategory] [int] NOT NULL,
	[FileName] [varchar](250) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdateDate] [datetime] NULL,
 CONSTRAINT [PK_ProductApplicationAttachment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductApplicationField]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductApplicationField](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductCategoryId] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[DataType] [tinyint] NOT NULL,
	[RefId] [int] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateBy] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateBy] [varchar](8) NOT NULL,
 CONSTRAINT [PK_ProductApplicationField] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductApplicationFieldData]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductApplicationFieldData](
	[ApplicationId] [int] NOT NULL,
	[ApplicationFieldId] [int] NOT NULL,
	[DataText] [varchar](max) NOT NULL,
	[DataDate] [date] NOT NULL,
	[DataNumber] [int] NOT NULL,
	[DateMoney] [money] NOT NULL,
 CONSTRAINT [PK_ProductApplicationFieldData] PRIMARY KEY CLUSTERED 
(
	[ApplicationId] ASC,
	[ApplicationFieldId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductApplicationMotor]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductApplicationMotor](
	[ApplicationId] [int] NOT NULL,
	[CarModelId] [int] NOT NULL,
	[CarBrand] [varchar](50) NOT NULL,
	[CarFamily] [varchar](50) NOT NULL,
	[CarModel] [varchar](50) NOT NULL,
	[CarRegisterYear] [smallint] NOT NULL,
	[CarCC] [int] NOT NULL,
	[CarWeight] [int] NOT NULL,
	[CarEngineNo] [varchar](50) NOT NULL,
	[CarSerialNo] [varchar](50) NOT NULL,
	[CarPlateNo1] [varchar](50) NOT NULL,
	[CarPlateNo2] [varchar](50) NOT NULL,
	[CarProvinceId] [int] NOT NULL,
	[CarUsageTypeCode] [varchar](50) NOT NULL,
	[CarUsageType] [varchar](50) NOT NULL,
	[CarCCTV] [tinyint] NOT NULL,
	[CarGarageType] [tinyint] NOT NULL,
	[CarAccessories] [varchar](max) NOT NULL,
	[CompulsoryPremium] [money] NOT NULL,
	[CombinePremium] [money] NOT NULL,
 CONSTRAINT [PK_ProductApplicationMotor] PRIMARY KEY CLUSTERED 
(
	[ApplicationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductApplicationPolicyHolder]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductApplicationPolicyHolder](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ApplicationId] [int] NOT NULL,
	[Sequence] [tinyint] NOT NULL,
	[TitleId] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Surname] [varchar](50) NOT NULL,
	[IDType] [tinyint] NOT NULL,
	[IDCard] [varchar](50) NOT NULL,
	[RefIDCard] [varchar](50) NOT NULL,
	[NationalId] [int] NOT NULL,
	[Birthday] [date] NOT NULL,
	[Age] [tinyint] NOT NULL,
	[Sex] [tinyint] NOT NULL,
	[MaritalStatus] [tinyint] NOT NULL,
	[Wegith] [tinyint] NOT NULL,
	[Height] [tinyint] NOT NULL,
	[CareerId] [int] NOT NULL,
	[Position] [varchar](50) NOT NULL,
	[PositionDescription] [varchar](max) NOT NULL,
	[AnnualIncome] [money] NOT NULL,
	[Telephone] [varchar](50) NOT NULL,
	[MobilePhone] [varchar](50) NOT NULL,
	[Email] [varchar](250) NOT NULL,
	[LineId] [varchar](50) NOT NULL,
	[Instagram] [varchar](50) NOT NULL,
	[Facebook] [varchar](50) NOT NULL,
	[BeneficiaryName] [varchar](50) NOT NULL,
	[BeneficiaryIDCard] [varchar](50) NOT NULL,
	[BeneficiaryRelationship] [varchar](50) NOT NULL,
	[BeneficiaryMobilePhone] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ProductApplicationPolicyHolder_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductCategory]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductCategory](
	[Id] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[ImageTH] [varchar](250) NOT NULL,
	[ImageEN] [varchar](250) NOT NULL,
	[BriefTH] [varchar](max) NOT NULL,
	[BriefEN] [varchar](max) NOT NULL,
	[HomeText1TH] [varchar](30) NOT NULL,
	[HomeText1EN] [varchar](30) NOT NULL,
	[HomeText2TH] [varchar](100) NOT NULL,
	[HomeText2EN] [varchar](100) NOT NULL,
	[DisplayHome] [bit] NOT NULL,
	[PaymentCreditCard] [bit] NOT NULL,
	[PaymentCreditCardInstallment] [bit] NOT NULL,
	[PaymentCounterService] [bit] NOT NULL,
	[UrlTH] [varchar](250) NOT NULL,
	[UrlEN] [varchar](250) NOT NULL,
	[Sequence] [int] NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductCategory_Shadow]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductCategory_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[ImageTH] [varchar](250) NOT NULL,
	[ImageEN] [varchar](250) NOT NULL,
	[BriefTH] [varchar](max) NOT NULL,
	[BriefEN] [varchar](max) NOT NULL,
	[HomeText1TH] [varchar](30) NOT NULL,
	[HomeText1EN] [varchar](30) NOT NULL,
	[HomeText2TH] [varchar](100) NOT NULL,
	[HomeText2EN] [varchar](100) NOT NULL,
	[DisplayHome] [bit] NOT NULL,
	[PaymentCreditCard] [bit] NOT NULL,
	[PaymentCreditCardInstallment] [bit] NOT NULL,
	[PaymentCounterService] [bit] NOT NULL,
	[UrlTH] [varchar](250) NOT NULL,
	[UrlEN] [varchar](250) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductCompareLog]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductCompareLog](
	[Id] [int] NOT NULL,
	[MemberId] [int] NOT NULL,
	[SessionId] [varchar](50) NOT NULL,
	[ProductId1] [int] NOT NULL,
	[PremiumId1] [int] NOT NULL,
	[ProductId2] [int] NOT NULL,
	[PremiumId2] [int] NOT NULL,
	[ProductId3] [int] NOT NULL,
	[PremiumId3] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductHomePremium]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductHomePremium](
	[Id] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[PropertyTypeId] [int] NOT NULL,
	[ConstructionTypeId] [int] NOT NULL,
	[CoverPeriod] [tinyint] NOT NULL,
	[SumInsured] [money] NOT NULL,
	[NetPremium] [money] NOT NULL,
	[Duty] [money] NOT NULL,
	[Vat] [money] NOT NULL,
	[Premium] [money] NOT NULL,
	[DisplayPremium] [money] NULL,
	[FireExplosionVehicleWater] [money] NOT NULL,
	[AccidentalDamage] [money] NOT NULL,
	[NaturalRisk] [money] NOT NULL,
	[TempRental] [money] NOT NULL,
	[PublicLiability] [money] NOT NULL,
	[Burglary] [money] NOT NULL,
	[EPolicy] [tinyint] NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductMotorCarModel]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductMotorCarModel](
	[ProductId] [int] NOT NULL,
	[CarModelId] [int] NOT NULL,
 CONSTRAINT [PK_ProductMotorCarModel] PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC,
	[CarModelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProductMotorPremium]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductMotorPremium](
	[Id] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[BrandId] [int] NOT NULL,
	[ModelId] [int] NOT NULL,
	[YearMin] [tinyint] NOT NULL,
	[YearMax] [tinyint] NOT NULL,
	[RegisterYear] [int] NOT NULL,
	[VehicleUsage110] [bit] NOT NULL,
	[VehicleUsage120] [bit] NOT NULL,
	[VehicleUsage210] [bit] NOT NULL,
	[VehicleUsage220] [bit] NOT NULL,
	[VehicleUsage230] [bit] NOT NULL,
	[VehicleUsage320] [bit] NOT NULL,
	[GarageType] [tinyint] NOT NULL,
	[CarInspector] [bit] NOT NULL,
	[CCTV] [tinyint] NOT NULL,
	[DriverAgeMin] [tinyint] NULL,
	[DriverAgeMax] [tinyint] NULL,
	[SumInsured] [money] NOT NULL,
	[NetPremium] [money] NOT NULL,
	[Duty] [money] NOT NULL,
	[Vat] [money] NOT NULL,
	[Premium] [money] NOT NULL,
	[DisplayPremium] [money] NULL,
	[CompulsoryPremium] [money] NULL,
	[DisplayCompulsoryPremium] [money] NULL,
	[CombinePremium] [money] NULL,
	[DisplayCombinePremium] [money] NULL,
	[OwnDamage] [money] NOT NULL,
	[OwnExcess] [money] NOT NULL,
	[Flood] [money] NOT NULL,
	[EPolicy] [tinyint] NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [int] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProductMotorRegisterProvince]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductMotorRegisterProvince](
	[ProductId] [int] NOT NULL,
	[ProvinceId] [int] NOT NULL,
	[RegisterProvinceId] [int] NOT NULL,
	[IsUsed] [bit] NOT NULL,
 CONSTRAINT [PK_ProductMotorRegisterProvince] PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC,
	[ProvinceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProductPACareer]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductPACareer](
	[ProductId] [int] NOT NULL,
	[CareerId] [int] NOT NULL,
	[CareerClass] [tinyint] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProductPAPremium]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductPAPremium](
	[Id] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[InsuranceType] [tinyint] NOT NULL,
	[AgeCalculation] [tinyint] NOT NULL,
	[MinAge] [tinyint] NOT NULL,
	[MaxAge] [tinyint] NOT NULL,
	[CareerClass1] [bit] NOT NULL,
	[CareerClass2] [bit] NOT NULL,
	[CareerClass3] [bit] NOT NULL,
	[CareerClass4] [bit] NOT NULL,
	[NetPremium] [money] NOT NULL,
	[Duty] [money] NOT NULL,
	[Vat] [money] NOT NULL,
	[Premium] [money] NOT NULL,
	[DisplayPremium] [money] NULL,
	[LossLife] [money] NOT NULL,
	[MedicalExpense] [money] NOT NULL,
	[MurderAssault] [money] NOT NULL,
	[RidingPassengerMotorcycle] [money] NOT NULL,
	[IncomeCompensationTH] [varchar](250) NOT NULL,
	[IncomeCompensationEN] [varchar](250) NOT NULL,
	[FuneralExpenseAccidentIllness] [money] NOT NULL,
	[EPolicy] [tinyint] NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [int] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [int] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductPHCareer]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductPHCareer](
	[ProductId] [int] NOT NULL,
	[CareerId] [int] NOT NULL,
	[CareerClass] [tinyint] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProductPHPremium]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductPHPremium](
	[Id] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[Sex] [tinyint] NOT NULL,
	[AgeCalculation] [tinyint] NOT NULL,
	[MinAgeMonth] [tinyint] NOT NULL,
	[MinAge] [tinyint] NOT NULL,
	[MaxAge] [tinyint] NOT NULL,
	[CareerClass1] [bit] NOT NULL,
	[CareerClass2] [bit] NOT NULL,
	[CareerClass3] [bit] NOT NULL,
	[CareerClass4] [bit] NOT NULL,
	[MinWeight] [tinyint] NOT NULL,
	[MaxWeight] [tinyint] NOT NULL,
	[MinHeight] [tinyint] NOT NULL,
	[MaxHeight] [tinyint] NOT NULL,
	[SumInsured] [money] NOT NULL,
	[NetPremium] [money] NOT NULL,
	[Duty] [money] NOT NULL,
	[Vat] [money] NOT NULL,
	[Premium] [money] NOT NULL,
	[DisplayPremium] [money] NULL,
	[IPDPerYear] [money] NOT NULL,
	[IPDRoomBoard] [money] NOT NULL,
	[SurgicalSpecialistTH] [varchar](250) NOT NULL,
	[SurgicalSpecialistEN] [varchar](250) NOT NULL,
	[OutpatientBenefits] [money] NOT NULL,
	[TransplantExpenseTH] [varchar](250) NOT NULL,
	[TransplantExpenseEN] [varchar](250) NOT NULL,
	[CancerTreatment] [money] NOT NULL,
	[EPolicy] [tinyint] NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [int] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [int] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductPRB]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductPRB](
	[ProductId] [int] NOT NULL,
	[NetPremium] [money] NOT NULL,
	[Duty] [money] NOT NULL,
	[Vat] [money] NOT NULL,
	[Premium] [money] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProductSubCategory]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductSubCategory](
	[Id] [int] NOT NULL,
	[CategoryId] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductSubClass]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductSubClass](
	[CategoryId] [int] NOT NULL,
	[Class] [varchar](3) NOT NULL,
	[SubClass] [varchar](3) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductTACountry]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductTACountry](
	[ProductId] [int] NOT NULL,
	[CountryId] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProductTAPremium]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductTAPremium](
	[Id] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[TripType] [tinyint] NOT NULL,
	[Zone] [tinyint] NOT NULL,
	[CoverageOption] [tinyint] NOT NULL,
	[CoverageType] [tinyint] NOT NULL,
	[MinDays] [smallint] NULL,
	[MaxDays] [smallint] NULL,
	[Persons] [tinyint] NOT NULL,
	[SumInsured] [money] NOT NULL,
	[NetPremium] [money] NOT NULL,
	[Duty] [money] NOT NULL,
	[Vat] [money] NOT NULL,
	[Premium] [money] NOT NULL,
	[DisplayPremium] [money] NULL,
	[PersonalLossLife] [money] NOT NULL,
	[MedicalExpenseEachAccident] [money] NOT NULL,
	[MedicalExpenseAccidentSickness] [money] NOT NULL,
	[EmergencyMedialEvacuation] [money] NOT NULL,
	[RepatriationExpense] [money] NULL,
	[RepatriationMortalRemains] [money] NOT NULL,
	[LossDamageBaggageTH] [varchar](250) NOT NULL,
	[LossDamageBaggageEN] [varchar](250) NOT NULL,
	[EPolicy] [tinyint] NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [int] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [int] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Promotion]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Promotion](
	[Id] [int] NOT NULL,
	[Code] [varchar](50) NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[BodyTH] [varchar](max) NOT NULL,
	[BodyEN] [varchar](max) NOT NULL,
	[PromotionType] [tinyint] NOT NULL,
	[PaymentType] [tinyint] NOT NULL,
	[DiscountType] [tinyint] NOT NULL,
	[DiscountAmount] [money] NOT NULL,
	[ProductType] [tinyint] NOT NULL,
	[ActiveDate] [date] NOT NULL,
	[ExpireDate] [date] NOT NULL,
	[ApproveStatus] [tinyint] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[ApproveAdminId] [varchar](8) NOT NULL,
	[OwnerAdminId] [varchar](8) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Promotion_Shadow]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Promotion_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[Code] [varchar](50) NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[BodyTH] [varchar](max) NOT NULL,
	[BodyEN] [varchar](max) NOT NULL,
	[PromotionType] [tinyint] NOT NULL,
	[PaymentType] [tinyint] NOT NULL,
	[DiscountType] [tinyint] NOT NULL,
	[DiscountAmount] [money] NOT NULL,
	[ProductType] [tinyint] NOT NULL,
	[ActiveDate] [date] NOT NULL,
	[ExpireDate] [date] NOT NULL,
	[ApproveStatus] [tinyint] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[ApproveAdminId] [varchar](8) NOT NULL,
	[OwnerAdminId] [varchar](8) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL,
	[Serialize] [varchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PromotionBank]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PromotionBank](
	[PromotionId] [int] NOT NULL,
	[BankId] [int] NOT NULL,
	[DiscountType] [tinyint] NOT NULL,
	[DiscountAmount] [money] NOT NULL,
	[IsInstallment] [bit] NOT NULL,
	[InstallmentMonth] [tinyint] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PromotionCompany]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PromotionCompany](
	[PromotionId] [int] NOT NULL,
	[CompanyId] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PromotionGift]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PromotionGift](
	[Id] [int] NOT NULL,
	[PromotionId] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PromotionProduct]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PromotionProduct](
	[PromotionId] [int] NOT NULL,
	[ProductId] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PromotionProductCategory]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PromotionProductCategory](
	[PromotionId] [int] NOT NULL,
	[ProductCategoryId] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[QuotationPaymentLog]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuotationPaymentLog](
	[OrderId] [int] NOT NULL,
	[PaymentStatus] [smallint] NOT NULL,
	[CreateDate] [datetime] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Subscribe]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Subscribe](
	[Id] [int] NOT NULL,
	[Email] [varchar](250) NOT NULL,
	[PublicIP] [varchar](50) NOT NULL,
	[LocalIP] [varchar](50) NOT NULL,
	[CreateDate] [datetime] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Subscribe_Shadow]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Subscribe_Shadow](
	[Action] [varchar](250) NOT NULL,
	[Id] [int] NOT NULL,
	[Email] [varchar](250) NOT NULL,
	[PublicIP] [varchar](50) NOT NULL,
	[LocalIP] [varchar](50) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[AdminId] [int] NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SystemMessage]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SystemMessage](
	[Id] [int] NOT NULL,
	[BodyTH] [varchar](max) NOT NULL,
	[BodyEN] [varchar](max) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateBy] [varchar](8) NOT NULL,
 CONSTRAINT [PK_SystemMessage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tag]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tag](
	[Id] [int] NOT NULL,
	[Title] [varchar](250) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tag_Shadow]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tag_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[Title] [varchar](250) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserTestimonial]    Script Date: 20/12/2560 15:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserTestimonial](
	[Id] [int] NOT NULL,
	[Image] [varchar](250) NOT NULL,
	[NameTH] [varchar](50) NOT NULL,
	[NameEN] [varchar](50) NOT NULL,
	[QuoteTH] [varchar](max) NOT NULL,
	[QuoteEN] [varchar](max) NOT NULL,
	[Sequence] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Index [IX_Order]    Script Date: 20/12/2560 15:48:37 ******/
CREATE NONCLUSTERED INDEX [IX_Order] ON [dbo].[Order]
(
	[ProductId] ASC,
	[PremiumId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_OrderBackendAPILog]    Script Date: 20/12/2560 15:48:37 ******/
CREATE NONCLUSTERED INDEX [IX_OrderBackendAPILog] ON [dbo].[OrderBackendAPILog]
(
	[OrderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_OrderPartnerAPILog]    Script Date: 20/12/2560 15:48:37 ******/
CREATE NONCLUSTERED INDEX [IX_OrderPartnerAPILog] ON [dbo].[OrderPartnerAPILog]
(
	[OrderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_OrderPaymentAPILog]    Script Date: 20/12/2560 15:48:37 ******/
CREATE NONCLUSTERED INDEX [IX_OrderPaymentAPILog] ON [dbo].[OrderPaymentAPILog]
(
	[OrderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_QuotationPaymentLog]    Script Date: 20/12/2560 15:48:37 ******/
CREATE NONCLUSTERED INDEX [IX_QuotationPaymentLog] ON [dbo].[OrderPaymentLog]
(
	[QuotationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ProductApplicationPolicyHolder]    Script Date: 20/12/2560 15:48:37 ******/
CREATE NONCLUSTERED INDEX [IX_ProductApplicationPolicyHolder] ON [dbo].[ProductApplicationPolicyHolder]
(
	[ApplicationId] ASC,
	[Sequence] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_OrderPaymentLog]    Script Date: 20/12/2560 15:48:37 ******/
CREATE NONCLUSTERED INDEX [IX_OrderPaymentLog] ON [dbo].[QuotationPaymentLog]
(
	[OrderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
USE [master]
GO
ALTER DATABASE [TOBJOD_2] SET  READ_WRITE 
GO
