USE [master]
GO
/****** Object:  Database [TOBJOD]    Script Date: 25/1/2561 16:16:55 ******/
CREATE DATABASE [TOBJOD]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'TOBJOD', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\TOBJOD.mdf' , SIZE = 21504KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'TOBJOD_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\TOBJOD_log.ldf' , SIZE = 16576KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [TOBJOD] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [TOBJOD].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [TOBJOD] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [TOBJOD] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [TOBJOD] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [TOBJOD] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [TOBJOD] SET ARITHABORT OFF 
GO
ALTER DATABASE [TOBJOD] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [TOBJOD] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [TOBJOD] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [TOBJOD] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [TOBJOD] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [TOBJOD] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [TOBJOD] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [TOBJOD] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [TOBJOD] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [TOBJOD] SET  DISABLE_BROKER 
GO
ALTER DATABASE [TOBJOD] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [TOBJOD] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [TOBJOD] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [TOBJOD] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [TOBJOD] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [TOBJOD] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [TOBJOD] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [TOBJOD] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [TOBJOD] SET  MULTI_USER 
GO
ALTER DATABASE [TOBJOD] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [TOBJOD] SET DB_CHAINING OFF 
GO
ALTER DATABASE [TOBJOD] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [TOBJOD] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [TOBJOD] SET DELAYED_DURABILITY = DISABLED 
GO
USE [TOBJOD]
GO
/****** Object:  Table [dbo].[Admin]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Admin](
	[Id] [varchar](8) NOT NULL,
	[EmployeeId] [varchar](8) NOT NULL,
	[AdminGroupId] [int] NOT NULL,
	[Name] [varchar](250) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
 CONSTRAINT [PK_Admin] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_Admin] UNIQUE NONCLUSTERED 
(
	[EmployeeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Admin_Shadow]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Admin_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [varchar](8) NOT NULL,
	[EmployeeId] [varchar](250) NOT NULL,
	[AdminGroupId] [int] NOT NULL,
	[Name] [varchar](250) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AdminAuthenLog]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AdminAuthenLog](
	[AdminId] [varchar](8) NOT NULL,
	[SessionId] [varchar](250) NOT NULL,
	[Login] [datetime] NOT NULL,
	[Logout] [datetime] NULL,
	[PublicIP] [varchar](50) NOT NULL,
	[LocalIP] [varchar](50) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AdminGroup]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AdminGroup](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](250) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
 CONSTRAINT [PK_AdminGroup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AdminGroup_Shadow]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AdminGroup_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[Title] [varchar](250) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AdminGroupPermission]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AdminGroupPermission](
	[AdminGroupId] [int] NOT NULL,
	[Module] [varchar](50) NOT NULL,
	[Can_View] [bit] NOT NULL,
	[Can_Add] [bit] NOT NULL,
	[Can_Edit] [bit] NOT NULL,
	[Can_Delete] [bit] NOT NULL,
	[Can_Approve] [bit] NOT NULL,
	[Can_Assign] [bit] NOT NULL,
	[Can_Export] [bit] NOT NULL,
 CONSTRAINT [PK_AdminGroupPermission] PRIMARY KEY CLUSTERED 
(
	[AdminGroupId] ASC,
	[Module] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AdminMenu]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AdminMenu](
	[Module] [varchar](50) NOT NULL,
	[ParentModule] [varchar](50) NOT NULL,
	[Title] [varchar](50) NOT NULL,
	[Icon] [varchar](50) NOT NULL,
	[Has_View] [bit] NOT NULL,
	[Has_Add] [bit] NOT NULL,
	[Has_Edit] [bit] NOT NULL,
	[Has_Delete] [bit] NOT NULL,
	[Has_Approve] [bit] NOT NULL,
	[Has_Assign] [bit] NOT NULL,
	[Has_Export] [bit] NOT NULL,
	[Sequence] [int] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[MenuType] [tinyint] NOT NULL,
 CONSTRAINT [PK_AdminMenu] PRIMARY KEY CLUSTERED 
(
	[Module] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Bank]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Bank](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[PaymentGatewayCode] [varchar](10) NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
 CONSTRAINT [PK_Bank] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Bank_Shadow]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[Bank_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[PaymentGatewayCode] [varchar](10) NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Banner]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Banner](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ImageTH] [varchar](250) NOT NULL,
	[ImageEN] [varchar](250) NOT NULL,
	[ImageMobileTH] [varchar](250) NOT NULL,
	[ImageMobileEN] [varchar](250) NOT NULL,
	[UrlTH] [varchar](250) NOT NULL,
	[UrlEN] [varchar](250) NOT NULL,
	[ActiveDate] [date] NOT NULL,
	[ExpireDate] [date] NOT NULL,
	[Sequence] [float] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[ApproveStatus] [tinyint] NOT NULL CONSTRAINT [DF_Banner_ApproveStatus]  DEFAULT ((0)),
	[ApproveAdminId] [varchar](8) NOT NULL,
	[OwnerAdminId] [varchar](8) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
 CONSTRAINT [PK_Banner] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Banner_Shadow]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Banner_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[ImageTH] [varchar](250) NOT NULL,
	[ImageEN] [varchar](250) NOT NULL,
	[ImageMobileTH] [varchar](250) NOT NULL,
	[ImageMobileEN] [varchar](250) NOT NULL,
	[UrlTH] [varchar](250) NOT NULL,
	[UrlEN] [varchar](250) NOT NULL,
	[ActiveDate] [date] NOT NULL,
	[ExpireDate] [date] NOT NULL,
	[Sequence] [float] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[ApproveStatus] [tinyint] NOT NULL,
	[ApproveAdminId] [varchar](8) NOT NULL,
	[OwnerAdminId] [varchar](8) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Campaign]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Campaign](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ParentId] [int] NOT NULL,
	[Code] [varchar](50) NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[BodyTH] [varchar](max) NOT NULL,
	[BodyEN] [varchar](max) NOT NULL,
	[ImageTH] [varchar](250) NOT NULL,
	[ImageEN] [varchar](250) NOT NULL,
	[ImageAltTH] [varchar](250) NOT NULL,
	[ImageAltEN] [varchar](250) NOT NULL,
	[IsGuest] [bit] NOT NULL,
	[IsMemberSilver] [bit] NOT NULL,
	[IsMemberGold] [bit] NOT NULL,
	[IsMemberPlatinum] [bit] NOT NULL,
	[PromotionAll] [bit] NOT NULL,
	[ActiveDate] [date] NOT NULL,
	[ExpireDate] [date] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[ApproveStatus] [tinyint] NOT NULL,
	[ApproveAdminId] [varchar](8) NOT NULL,
	[OwnerAdminId] [varchar](8) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
 CONSTRAINT [PK_Campaign] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Campaign_Shadow]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Campaign_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[ParentId] [int] NOT NULL,
	[Code] [varchar](50) NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[BodyTH] [varchar](max) NOT NULL,
	[BodyEN] [varchar](max) NOT NULL,
	[ImageTH] [varchar](250) NOT NULL,
	[ImageEN] [varchar](250) NOT NULL,
	[ImageAltTH] [varchar](250) NOT NULL,
	[ImageAltEN] [varchar](250) NOT NULL,
	[IsGuest] [bit] NOT NULL,
	[IsMemberSilver] [bit] NOT NULL,
	[IsMemberGold] [bit] NOT NULL,
	[IsMemberPlatinum] [bit] NOT NULL,
	[PromotionAll] [bit] NOT NULL,
	[ActiveDate] [date] NOT NULL,
	[ExpireDate] [date] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[ApproveStatus] [tinyint] NOT NULL,
	[ApproveAdminId] [varchar](8) NOT NULL,
	[OwnerAdminId] [varchar](8) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL,
	[Serialize] [varchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CampaignPromotion]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CampaignPromotion](
	[CampaignId] [int] NOT NULL,
	[PromotionId] [int] NOT NULL,
 CONSTRAINT [PK_CampaignPromotion] PRIMARY KEY CLUSTERED 
(
	[CampaignId] ASC,
	[PromotionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CarBrand]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CarBrand](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[MapName] [varchar](250) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
 CONSTRAINT [PK_CarBrand] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CarBrand_Shadow]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CarBrand_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[MapName] [varchar](250) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CarFamily]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[CarFamily](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BrandId] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[MapName] [varchar](250) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
 CONSTRAINT [PK_CarFamily] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CarFamily_Shadow]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[CarFamily_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[BrandId] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[MapName] [varchar](250) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CarModel]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CarModel](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[FamilyId] [int] NOT NULL CONSTRAINT [DF__CarModel__Family__3A379A64]  DEFAULT ((0)),
	[Spec] [varchar](250) NOT NULL,
	[CC] [int] NOT NULL CONSTRAINT [DF__CarModel__CC__3B2BBE9D]  DEFAULT ((0)),
	[KG] [int] NOT NULL CONSTRAINT [DF__CarModel__KG__3C1FE2D6]  DEFAULT ((0)),
	[Seat] [tinyint] NOT NULL CONSTRAINT [DF__CarModel__Seat__3D14070F]  DEFAULT ((0)),
	[Door] [tinyint] NOT NULL CONSTRAINT [DF__CarModel__Door__3E082B48]  DEFAULT ((0)),
	[Year] [int] NOT NULL,
	[MapName] [varchar](250) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
 CONSTRAINT [PK_CarModel] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CarModel_Shadow]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CarModel_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[Spec] [varchar](250) NOT NULL,
	[CC] [int] NOT NULL CONSTRAINT [DF__CarModel_Sha__CC__40E497F3]  DEFAULT ((0)),
	[KG] [int] NOT NULL CONSTRAINT [DF__CarModel_Sha__KG__41D8BC2C]  DEFAULT ((0)),
	[Seat] [tinyint] NOT NULL CONSTRAINT [DF__CarModel_S__Seat__42CCE065]  DEFAULT ((0)),
	[Door] [tinyint] NOT NULL CONSTRAINT [DF__CarModel_S__Door__43C1049E]  DEFAULT ((0)),
	[Year] [int] NOT NULL,
	[MapName] [varchar](250) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL,
	[FamilyId] [int] NOT NULL CONSTRAINT [DF__CarModel___Famil__3FF073BA]  DEFAULT ((0))
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Company]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Company](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[PrefixTH] [varchar](15) NOT NULL,
	[PrefixEN] [varchar](15) NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[CoCode] [varchar](10) NOT NULL,
	[CoPrefixTH] [varchar](15) NOT NULL,
	[CoPrefixEN] [varchar](15) NOT NULL,
	[CoTitleTH] [varchar](255) NOT NULL,
	[CoTitleEN] [varchar](255) NOT NULL,
	[ReCode] [varchar](5) NOT NULL,
	[RePrefixTH] [varchar](15) NOT NULL,
	[RePrefixEN] [varchar](15) NOT NULL,
	[ReTitleTH] [varchar](50) NOT NULL,
	[ReTitleEN] [varchar](50) NOT NULL,
	[AlCode] [varchar](10) NOT NULL,
	[AlPrefixTH] [varchar](15) NOT NULL,
	[AlPrefixEN] [varchar](15) NOT NULL,
	[AlTitleTH] [varchar](255) NOT NULL,
	[AlTitleEN] [varchar](255) NOT NULL,
	[ImageTH] [varchar](250) NOT NULL,
	[ImageEN] [varchar](250) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[Company] ADD [MailTo] [varchar](max) NOT NULL DEFAULT ('')
ALTER TABLE [dbo].[Company] ADD [APIChannel] [tinyint] NOT NULL DEFAULT ((0))
SET ANSI_PADDING ON
ALTER TABLE [dbo].[Company] ADD [APIUrl] [varchar](250) NOT NULL DEFAULT ('')
ALTER TABLE [dbo].[Company] ADD [PaymentMerchantId] [varchar](250) NOT NULL DEFAULT ('')
ALTER TABLE [dbo].[Company] ADD [PaymentSecretKey] [varchar](250) NOT NULL DEFAULT ('')
 CONSTRAINT [PK_Company] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Company_Shadow]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[Company_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[PrefixTH] [varchar](15) NOT NULL,
	[PrefixEN] [varchar](15) NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[CoCode] [varchar](10) NOT NULL,
	[CoPrefixTH] [varchar](15) NOT NULL,
	[CoPrefixEN] [varchar](15) NOT NULL,
	[CoTitleTH] [varchar](255) NOT NULL,
	[CoTitleEN] [varchar](255) NOT NULL,
	[ReCode] [varchar](5) NOT NULL,
	[RePrefixTH] [varchar](15) NOT NULL,
	[RePrefixEN] [varchar](15) NOT NULL,
	[ReTitleTH] [varchar](50) NOT NULL,
	[ReTitleEN] [varchar](50) NOT NULL,
	[AlCode] [varchar](10) NOT NULL,
	[AlPrefixTH] [varchar](15) NOT NULL,
	[AlPrefixEN] [varchar](15) NOT NULL,
	[AlTitleTH] [varchar](255) NOT NULL,
	[AlTitleEN] [varchar](255) NOT NULL,
	[ImageTH] [varchar](250) NOT NULL,
	[ImageEN] [varchar](250) NOT NULL,
	[MailTo] [varchar](max) NOT NULL DEFAULT (''),
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL,
	[APIChannel] [tinyint] NOT NULL DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
SET ANSI_PADDING ON
ALTER TABLE [dbo].[Company_Shadow] ADD [APIUrl] [varchar](250) NOT NULL DEFAULT ('')
ALTER TABLE [dbo].[Company_Shadow] ADD [PaymentMerchantId] [varchar](250) NOT NULL DEFAULT ('')
ALTER TABLE [dbo].[Company_Shadow] ADD [PaymentSecretKey] [varchar](250) NOT NULL DEFAULT ('')

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Config]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Config](
	[Id] [int] NOT NULL,
	[Title] [varchar](250) NOT NULL,
	[Body] [text] NOT NULL,
	[AutoLoad] [bit] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
 CONSTRAINT [PK_Config_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Config_Shadow]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Config_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[Title] [varchar](250) NOT NULL,
	[Body] [text] NOT NULL,
	[AutoLoad] [bit] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ConfigPage]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ConfigPage](
	[Id] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[BodyTH] [varchar](max) NOT NULL,
	[BodyEN] [varchar](max) NOT NULL,
	[MetaTitleTH] [varchar](250) NOT NULL,
	[MetaTitleEN] [varchar](250) NOT NULL,
	[MetaKeywordTH] [varchar](max) NOT NULL,
	[MetaKeywordEN] [varchar](max) NOT NULL,
	[MetaDescriptionTH] [varchar](max) NOT NULL,
	[MetaDescriptionEN] [varchar](max) NOT NULL,
	[OGTitleTH] [varchar](250) NOT NULL,
	[OGTitleEN] [varchar](250) NOT NULL,
	[OGImageTH] [varchar](250) NOT NULL,
	[OGImageEN] [varchar](250) NOT NULL,
	[OGDescriptionTH] [varchar](max) NOT NULL,
	[OGDescriptionEN] [varchar](max) NOT NULL,
	[Data1] [varchar](max) NOT NULL,
	[Data2] [varchar](max) NOT NULL,
	[Data3] [varchar](max) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[ApproveStatus] [tinyint] NOT NULL DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[ConfigPage] ADD [ApproveAdminId] [varchar](8) NOT NULL DEFAULT ('')
 CONSTRAINT [PK_Config_Page] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ConfigPage_Shadow]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ConfigPage_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[BodyTH] [varchar](max) NOT NULL,
	[BodyEN] [varchar](max) NOT NULL,
	[MetaTitleTH] [varchar](250) NOT NULL,
	[MetaTitleEN] [varchar](250) NOT NULL,
	[MetaKeywordTH] [varchar](max) NOT NULL,
	[MetaKeywordEN] [varchar](max) NOT NULL,
	[MetaDescriptionTH] [varchar](max) NOT NULL,
	[MetaDescriptionEN] [varchar](max) NOT NULL,
	[OGTitleTH] [varchar](250) NOT NULL,
	[OGTitleEN] [varchar](250) NOT NULL,
	[OGImageTH] [varchar](250) NOT NULL,
	[OGImageEN] [varchar](250) NOT NULL,
	[OGDescriptionTH] [varchar](max) NOT NULL,
	[OGDescriptionEN] [varchar](max) NOT NULL,
	[Data1] [varchar](max) NOT NULL,
	[Data2] [varchar](max) NOT NULL,
	[Data3] [varchar](max) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL,
	[ApproveStatus] [tinyint] NOT NULL DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[ConfigPage_Shadow] ADD [ApproveAdminId] [varchar](8) NOT NULL DEFAULT ('')

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ContactCategory]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContactCategory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[Sequence] [float] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
 CONSTRAINT [PK_ContactCategory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ContactCategory_Shadow]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContactCategory_Shadow](
	[Action] [varchar](250) NOT NULL,
	[Id] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[Sequence] [float] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ContactSubCategory]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContactSubCategory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CategoryId] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[MailTo] [varchar](max) NOT NULL,
	[Sequence] [float] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
 CONSTRAINT [PK_ContactSubCategory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ContactSubCategory_Shadow]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContactSubCategory_Shadow](
	[Action] [varchar](250) NOT NULL,
	[Id] [int] NOT NULL,
	[CategoryId] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[MailTo] [varchar](max) NOT NULL,
	[Sequence] [float] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Country]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Country](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](250) NOT NULL,
	[Zone] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
 CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Country_Shadow]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Country_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[Code] [varchar](50) NOT NULL,
	[Zone] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Coupon]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Coupon](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CouponType] [tinyint] NOT NULL,
	[TitleTH] [varchar](50) NOT NULL,
	[TitleEN] [varchar](50) NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Total] [int] NOT NULL,
	[DiscountType] [tinyint] NOT NULL,
	[DiscountAmount] [money] NOT NULL,
	[ProductType] [tinyint] NOT NULL,
	[ActiveDate] [date] NOT NULL,
	[ExpireDate] [date] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
 CONSTRAINT [PK_Coupon] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Coupon_Shadow]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Coupon_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[CouponType] [tinyint] NOT NULL,
	[TitleTH] [varchar](50) NOT NULL,
	[TitleEN] [varchar](50) NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Total] [int] NOT NULL,
	[DiscountType] [tinyint] NOT NULL,
	[DiscountAmount] [money] NOT NULL,
	[ProductType] [tinyint] NOT NULL,
	[ActiveDate] [date] NOT NULL,
	[ExpireDate] [date] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NOT NULL,
	[Serialize] [varchar](max) NOT NULL,
	[ParentId] [int] NOT NULL CONSTRAINT [DF_Coupon_Shadow_ParentId]  DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CouponCode]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CouponCode](
	[CouponId] [int] NOT NULL,
	[Code] [varchar](20) NOT NULL,
	[Used] [int] NOT NULL,
 CONSTRAINT [PK_CouponCode] PRIMARY KEY CLUSTERED 
(
	[CouponId] ASC,
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CouponCodeLog]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CouponCodeLog](
	[CouponId] [int] NOT NULL,
	[CouponCode] [varchar](20) NOT NULL,
	[OrderId] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CouponCompany]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CouponCompany](
	[CouponId] [int] NOT NULL,
	[CompanyId] [int] NOT NULL,
 CONSTRAINT [PK_CouponCompany] PRIMARY KEY CLUSTERED 
(
	[CouponId] ASC,
	[CompanyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CouponProduct]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CouponProduct](
	[CouponId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
 CONSTRAINT [PK_CouponProduct] PRIMARY KEY CLUSTERED 
(
	[CouponId] ASC,
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CouponProductCategory]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CouponProductCategory](
	[CouponId] [int] NOT NULL,
	[ProductCategoryId] [int] NOT NULL,
 CONSTRAINT [PK_CouponProductCategory] PRIMARY KEY CLUSTERED 
(
	[CouponId] ASC,
	[ProductCategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DayOff]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DayOff](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EventDate] [date] NOT NULL,
	[TitleTH] [varchar](50) NOT NULL,
	[TitleEN] [varchar](50) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
 CONSTRAINT [PK_DayOff] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_DayOff] UNIQUE NONCLUSTERED 
(
	[EventDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DayOff_Shadow]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DayOff_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[EventDate] [date] NOT NULL,
	[TitleTH] [varchar](50) NOT NULL,
	[TitleEN] [varchar](50) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[District]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[District](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProvinceId] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[Code] [varchar](250) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
 CONSTRAINT [PK_GeoDistrict] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[District_Shadow]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[District_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[ProvinceId] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[Code] [varchar](250) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Faq]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Faq](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CategoryId] [int] NOT NULL,
	[QuestionTH] [varchar](250) NOT NULL,
	[QuestionEN] [varchar](250) NOT NULL,
	[AnswerTH] [varchar](max) NOT NULL,
	[AnswerEN] [varchar](max) NOT NULL,
	[Pin] [bit] NOT NULL,
	[Sequence] [float] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
 CONSTRAINT [PK_Faq] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Faq_Shadow]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Faq_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[CategoryId] [int] NOT NULL,
	[QuestionTH] [varchar](250) NOT NULL,
	[QuestionEN] [varchar](250) NOT NULL,
	[AnswerTH] [varchar](max) NOT NULL,
	[AnswerEN] [varchar](max) NOT NULL,
	[Pin] [bit] NOT NULL,
	[Sequence] [float] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Lead]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Lead](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Source] [tinyint] NOT NULL,
	[ProductId] [int] NOT NULL,
	[PremiumId] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Surname] [varchar](50) NOT NULL,
	[Tel] [varchar](50) NOT NULL,
	[Email] [varchar](250) NOT NULL,
	[CallBackDate] [datetime] NOT NULL,
	[SubjectId] [int] NOT NULL,
	[Message] [varchar](max) NOT NULL,
	[Remark] [varchar](max) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[LeadStatus] [tinyint] NOT NULL,
	[OwnerAdminId] [varchar](8) NOT NULL,
	[AssignAdminId] [varchar](8) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
 CONSTRAINT [PK_Lead] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Lead_Shadow]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Lead_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[Source] [tinyint] NOT NULL,
	[ProductId] [int] NOT NULL,
	[PremiumId] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Surname] [varchar](50) NOT NULL,
	[Tel] [varchar](50) NOT NULL,
	[Email] [varchar](250) NOT NULL,
	[CallBackDate] [datetime] NOT NULL,
	[SubjectId] [int] NOT NULL,
	[Message] [varchar](max) NOT NULL,
	[Remark] [varchar](max) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[LeadStatus] [tinyint] NOT NULL,
	[OwnerAdminId] [varchar](8) NOT NULL,
	[AssignAdminId] [varchar](8) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LeadCriteria]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LeadCriteria](
	[LeadId] [int] NOT NULL,
	[SubCategoryId] [int] NOT NULL,
	[HousePropertyType] [int] NOT NULL,
	[HouseConstructionType] [int] NOT NULL,
	[HousePropertyValue] [int] NOT NULL,
	[HouseConstructionValue] [int] NOT NULL,
	[HouseYear] [int] NOT NULL,
	[MotorBrandId] [int] NOT NULL,
	[MotorModelId] [int] NOT NULL,
	[MotorUsage] [int] NOT NULL,
	[MotorYear] [int] NOT NULL,
	[MotorCCTV] [tinyint] NOT NULL,
	[MotorPRB] [bit] NOT NULL,
	[MotorAccessory] [bit] NOT NULL,
	[MotorGarage] [tinyint] NOT NULL,
	[CareerId] [int] NOT NULL,
	[Birthday] [date] NOT NULL,
	[Sex] [tinyint] NOT NULL,
	[Weight] [int] NOT NULL,
	[Height] [int] NOT NULL,
	[TATripType] [int] NOT NULL,
	[TAZone] [int] NOT NULL,
	[TACoverageOption] [int] NOT NULL,
	[TACoverageType] [int] NOT NULL,
	[TADays] [tinyint] NOT NULL,
	[TACountryId] [int] NOT NULL,
	[TAPerson] [tinyint] NOT NULL,
	[PriceFrom] [int] NOT NULL,
	[PriceTo] [int] NOT NULL,
	[MotorFamilyId] [int] NOT NULL CONSTRAINT [DF__LeadCrite__Motor__666B225D]  DEFAULT ((0)),
	[MotorRegisterProvinceId] [int] NOT NULL CONSTRAINT [DF__LeadCrite__Motor__675F4696]  DEFAULT ((0)),
	[MotorDriverAgeId] [int] NOT NULL CONSTRAINT [DF__LeadCrite__Motor__08C03A61]  DEFAULT ((0)),
	[MotorDriverAge] [tinyint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_LeadCriteria] PRIMARY KEY CLUSTERED 
(
	[LeadId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LeadForm]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LeadForm](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Surname] [varchar](50) NOT NULL,
	[Tel] [varchar](50) NOT NULL,
	[Email] [varchar](250) NOT NULL,
	[SessionId] [varchar](50) NOT NULL,
	[IPPublic] [varchar](20) NOT NULL,
	[IPPrivate] [varchar](20) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_LeadForm] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LeadLog]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LeadLog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LeadId] [int] NOT NULL,
	[LeadStatus] [tinyint] NOT NULL,
	[Remark] [varchar](max) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[AssignAdminId] [varchar](8) NOT NULL,
 CONSTRAINT [PK_LeadLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MailLog]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MailLog](
	[EmailFrom] [varchar](250) NOT NULL,
	[EmailTo] [varchar](max) NOT NULL,
	[EmailCC] [varchar](max) NOT NULL,
	[EmailBCC] [varchar](max) NOT NULL,
	[Subject] [varchar](max) NOT NULL,
	[Success] [bit] NOT NULL,
	[ErrorMessage] [varchar](max) NOT NULL,
	[CreateDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MasterOption]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MasterOption](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Category] [int] NOT NULL,
	[RefId] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[Code] [varchar](250) NOT NULL,
	[Mode] [tinyint] NOT NULL,
	[Sequence] [float] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[MIN] [int] NOT NULL DEFAULT ((0)),
	[MAX] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_MasterOption] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MasterOption_Shadow]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MasterOption_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[Category] [int] NOT NULL,
	[RefId] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[Code] [varchar](250) NOT NULL,
	[Mode] [tinyint] NOT NULL,
	[Sequence] [float] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL,
	[MIN] [int] NOT NULL DEFAULT ((0)),
	[MAX] [int] NOT NULL DEFAULT ((0))
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[News]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[News](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NewsSubCategoryId] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[UrlPrefix] [varchar](50) NOT NULL,
	[UrlTH] [varchar](250) NOT NULL,
	[UrlEN] [varchar](250) NOT NULL,
	[BriefTH] [varchar](max) NOT NULL,
	[BriefEN] [varchar](max) NOT NULL,
	[BodyTH] [varchar](max) NOT NULL,
	[BodyEN] [varchar](max) NOT NULL,
	[ImageTH] [varchar](250) NOT NULL,
	[ImageEN] [varchar](250) NOT NULL,
	[ImageTitleTH] [varchar](250) NOT NULL,
	[ImageTitleEN] [varchar](250) NOT NULL,
	[ImageAltTH] [varchar](250) NOT NULL,
	[ImageAltEN] [varchar](250) NOT NULL,
	[MetaTitleTH] [varchar](250) NOT NULL,
	[MetaTitleEN] [varchar](250) NOT NULL,
	[MetaKeywordTH] [varchar](max) NOT NULL,
	[MetaKeywordEN] [varchar](max) NOT NULL,
	[MetaDescriptionTH] [varchar](max) NOT NULL,
	[MetaDescriptionEN] [varchar](max) NOT NULL,
	[OGTitleTH] [varchar](250) NOT NULL,
	[OGTitleEN] [varchar](250) NOT NULL,
	[OGImageTH] [varchar](250) NOT NULL,
	[OGImageEN] [varchar](250) NOT NULL,
	[OGDescriptionTH] [varchar](max) NOT NULL,
	[OGDescriptionEN] [varchar](max) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[ApproveStatus] [tinyint] NOT NULL,
	[ApproveAdminId] [varchar](8) NOT NULL,
	[OwnerAdminId] [varchar](8) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
 CONSTRAINT [PK_News] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[News_Shadow]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[News_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[NewsSubCategoryId] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[UrlPrefix] [varchar](50) NOT NULL,
	[UrlTH] [varchar](250) NOT NULL,
	[UrlEN] [varchar](250) NOT NULL,
	[BriefTH] [varchar](max) NOT NULL,
	[BriefEN] [varchar](max) NOT NULL,
	[BodyTH] [varchar](max) NOT NULL,
	[BodyEN] [varchar](max) NOT NULL,
	[ImageTH] [varchar](250) NOT NULL,
	[ImageEN] [varchar](250) NOT NULL,
	[ImageTitleTH] [varchar](250) NOT NULL,
	[ImageTitleEN] [varchar](250) NOT NULL,
	[ImageAltTH] [varchar](250) NOT NULL,
	[ImageAltEN] [varchar](250) NOT NULL,
	[MetaTitleTH] [varchar](250) NOT NULL,
	[MetaTitleEN] [varchar](250) NOT NULL,
	[MetaKeywordTH] [varchar](max) NOT NULL,
	[MetaKeywordEN] [varchar](max) NOT NULL,
	[MetaDescriptionTH] [varchar](max) NOT NULL,
	[MetaDescriptionEN] [varchar](max) NOT NULL,
	[OGTitleTH] [varchar](250) NOT NULL,
	[OGTitleEN] [varchar](250) NOT NULL,
	[OGImageTH] [varchar](250) NOT NULL,
	[OGImageEN] [varchar](250) NOT NULL,
	[OGDescriptionTH] [varchar](max) NOT NULL,
	[OGDescriptionEN] [varchar](max) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[ApproveStatus] [tinyint] NOT NULL,
	[ApproveAdminId] [varchar](8) NOT NULL,
	[OwnerAdminId] [varchar](8) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewsImage]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsImage](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NewsId] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[Image] [varchar](250) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
 CONSTRAINT [PK_NewsImage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewsSubCategory]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsSubCategory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NewsCategory] [tinyint] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
 CONSTRAINT [PK_NewsSubCategory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewsSubCategory_Shadow]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsSubCategory_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[NewsCategory] [tinyint] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewsTag]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsTag](
	[NewsId] [int] NOT NULL,
	[TagId] [int] NOT NULL,
 CONSTRAINT [PK_NewsTag] PRIMARY KEY CLUSTERED 
(
	[NewsId] ASC,
	[TagId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Order]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Order](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LeadFormId] [int] NOT NULL,
	[ProductApplicationId] [int] NOT NULL,
	[CANo] [varchar](50) NOT NULL,
	[PaymentId] [varchar](50) NOT NULL,
	[PaymentChannel] [tinyint] NOT NULL,
	[PaymentStatus] [smallint] NOT NULL,
	[PaymentDate] [datetime] NOT NULL,
	[PaymentInstallment] [tinyint] NOT NULL,
	[PaymentTotal] [money] NOT NULL,
	[PartnerAPIStatus] [tinyint] NOT NULL,
	[BackendAPIStatus] [tinyint] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Order] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Order_Shadow]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[Order_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[LeadFormId] [int] NOT NULL,
	[ProductApplicationId] [int] NOT NULL,
	[CANo] [varchar](50) NOT NULL,
	[PaymentId] [varchar](50) NOT NULL,
	[PaymentChannel] [tinyint] NOT NULL,
	[PaymentStatus] [smallint] NOT NULL,
	[PaymentDate] [datetime] NOT NULL,
	[PaymentInstallment] [tinyint] NOT NULL,
	[PaymentTotal] [money] NOT NULL,
	[PartnerAPIStatus] [tinyint] NOT NULL,
	[BackendAPIStatus] [tinyint] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrderBackendAPILog]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrderBackendAPILog](
	[OrderId] [int] NOT NULL,
	[Response] [varchar](max) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrderPartnerAPILog]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrderPartnerAPILog](
	[OrderId] [int] NOT NULL,
	[Response] [varchar](max) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrderPaymentAPILog]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrderPaymentAPILog](
	[OrderId] [int] NOT NULL,
	[Response] [varchar](max) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrderPaymentLog]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderPaymentLog](
	[QuotationId] [int] NOT NULL,
	[PaymentStatus] [smallint] NOT NULL,
	[CreateDate] [datetime] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OrderProductHomePremium]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrderProductHomePremium](
	[OrderId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[PremiumId] [int] NOT NULL,
	[PropertyTypeId] [int] NOT NULL,
	[ConstructionTypeId] [int] NOT NULL,
	[CoverPeriod] [tinyint] NOT NULL,
	[SumInsured] [money] NOT NULL,
	[NetPremium] [money] NOT NULL,
	[Duty] [money] NOT NULL,
	[Vat] [money] NOT NULL,
	[Premium] [money] NOT NULL,
	[DisplayPremium] [money] NOT NULL,
	[Fire] [money] NOT NULL,
	[AccidentalDamage] [money] NOT NULL,
	[NaturalRisk] [money] NOT NULL,
	[TempRental] [money] NOT NULL,
	[Burglary] [money] NOT NULL,
	[PublicLiability] [money] NOT NULL,
	[EPolicy] [bit] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrderProductMotorPremium]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderProductMotorPremium](
	[OrderId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[PremiumId] [int] NOT NULL,
	[BuyCompulsory] [bit] NOT NULL,
	[YearMin] [tinyint] NOT NULL,
	[YearMax] [tinyint] NOT NULL,
	[RegisterYear] [int] NOT NULL,
	[DriverAgeMin] [tinyint] NOT NULL,
	[DriverAgeMax] [tinyint] NOT NULL,
	[VehicleUsage110] [bit] NOT NULL,
	[VehicleUsage120] [bit] NOT NULL,
	[VehicleUsage210] [bit] NOT NULL,
	[VehicleUsage220] [bit] NOT NULL,
	[VehicleUsage230] [bit] NOT NULL,
	[VehicleUsage320] [bit] NOT NULL,
	[RegisterProvinceAll] [bit] NOT NULL,
	[GarageType] [tinyint] NOT NULL,
	[CarInspector] [bit] NOT NULL,
	[CCTV] [tinyint] NOT NULL,
	[SumInsured] [money] NOT NULL,
	[NetPremium] [money] NOT NULL,
	[Duty] [money] NOT NULL,
	[Vat] [money] NOT NULL,
	[Premium] [money] NOT NULL,
	[DisplayPremium] [money] NOT NULL,
	[CompulsoryPremium] [money] NOT NULL,
	[CombinePremium] [money] NOT NULL,
	[DisplayCombinePremium] [money] NOT NULL,
	[OwnDamage] [money] NOT NULL,
	[OwnExcess] [money] NOT NULL,
	[Flood] [money] NOT NULL,
	[EPolicy] [bit] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [int] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OrderProductPAPremium]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderProductPAPremium](
	[OrderId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[PremiumId] [int] NOT NULL,
	[InsuranceType] [tinyint] NOT NULL,
	[AgeCalculation] [tinyint] NOT NULL,
	[MinAge] [tinyint] NOT NULL,
	[MaxAge] [tinyint] NOT NULL,
	[CareerClass1] [bit] NOT NULL,
	[CareerClass2] [bit] NOT NULL,
	[CareerClass3] [bit] NOT NULL,
	[CareerClass4] [bit] NOT NULL,
	[SumInsured] [money] NOT NULL,
	[NetPremium] [money] NOT NULL,
	[Duty] [money] NOT NULL,
	[Vat] [money] NOT NULL,
	[Premium] [money] NOT NULL,
	[DisplayPremium] [money] NOT NULL,
	[LossLife] [money] NOT NULL,
	[MedicalExpense] [money] NOT NULL,
	[MurderAssault] [money] NOT NULL,
	[RidingPassengerMotorcycle] [money] NOT NULL,
	[FuneralExpenseAccidentIllness] [money] NOT NULL,
	[IncomeCompensation] [money] NOT NULL,
	[EPolicy] [bit] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [int] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OrderProductPHPremium]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderProductPHPremium](
	[OrderId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[PremiumId] [int] NOT NULL,
	[Sex] [tinyint] NOT NULL,
	[AgeCalculation] [tinyint] NOT NULL,
	[MinAgeMonth] [tinyint] NOT NULL,
	[MinAge] [tinyint] NOT NULL,
	[MaxAge] [tinyint] NOT NULL,
	[CareerClass1] [bit] NOT NULL,
	[CareerClass2] [bit] NOT NULL,
	[CareerClass3] [bit] NOT NULL,
	[CareerClass4] [bit] NOT NULL,
	[MinWeight] [tinyint] NOT NULL,
	[MaxWeight] [tinyint] NOT NULL,
	[MinHeight] [tinyint] NOT NULL,
	[MaxHeight] [tinyint] NOT NULL,
	[SumInsured] [money] NOT NULL,
	[NetPremium] [money] NOT NULL,
	[Duty] [money] NOT NULL,
	[Vat] [money] NOT NULL,
	[Premium] [money] NOT NULL,
	[DisplayPremium] [money] NOT NULL,
	[IPD] [money] NOT NULL,
	[IPDRoomBoard] [money] NOT NULL,
	[OutpatientBenefits] [money] NOT NULL,
	[SurgicalExpense] [money] NOT NULL,
	[CancerTreatment] [money] NOT NULL,
	[OrganTransplant] [money] NOT NULL,
	[EPolicy] [bit] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [int] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OrderProductTAPremium]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderProductTAPremium](
	[OrderId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[PremiumId] [int] NOT NULL,
	[TripType] [tinyint] NOT NULL,
	[Zone] [tinyint] NOT NULL,
	[CoverageOption] [tinyint] NOT NULL,
	[CoverageType] [tinyint] NOT NULL,
	[MinDays] [smallint] NOT NULL,
	[MaxDays] [smallint] NOT NULL,
	[Persons] [tinyint] NOT NULL,
	[SumInsured] [money] NOT NULL,
	[NetPremium] [money] NOT NULL,
	[Duty] [money] NOT NULL,
	[Vat] [money] NOT NULL,
	[Premium] [money] NOT NULL,
	[DisplayPremium] [money] NOT NULL,
	[PersonalLossLife] [money] NOT NULL,
	[MedicalExpenseEachAccident] [money] NOT NULL,
	[MedicalExpenseAccidentSickness] [money] NOT NULL,
	[EmergencyMedialEvacuation] [money] NOT NULL,
	[RepatriationExpense] [money] NOT NULL,
	[RepatriationMortalRemains] [money] NOT NULL,
	[LossDamageBaggage] [money] NOT NULL,
	[EPolicy] [bit] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [int] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PartnerBanner]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PartnerBanner](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[ImageTH] [varchar](50) NOT NULL,
	[ImageEN] [varchar](50) NOT NULL,
	[Sequence] [int] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
 CONSTRAINT [PK_PartnerBanner] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PartnerBanner_Shadow]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[PartnerBanner_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[ImageTH] [varchar](50) NOT NULL,
	[ImageEN] [varchar](50) NOT NULL,
	[Sequence] [int] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PaymentMethod]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PaymentMethod](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[UrlTH] [varchar](250) NOT NULL,
	[UrlEN] [varchar](250) NOT NULL,
	[BriefTH] [varchar](max) NOT NULL,
	[BriefEN] [varchar](max) NOT NULL,
	[BodyTH] [varchar](max) NOT NULL,
	[BodyEN] [varchar](max) NOT NULL,
	[ImageTH] [varchar](250) NOT NULL,
	[ImageEN] [varchar](250) NOT NULL,
	[ImageTitleTH] [varchar](250) NOT NULL,
	[ImageTitleEN] [varchar](250) NOT NULL,
	[ImageAltTH] [varchar](250) NOT NULL,
	[ImageAltEN] [varchar](250) NOT NULL,
	[MetaTitleTH] [varchar](250) NOT NULL,
	[MetaTitleEN] [varchar](250) NOT NULL,
	[MetaKeywordTH] [varchar](max) NOT NULL,
	[MetaKeywordEN] [varchar](max) NOT NULL,
	[MetaDescriptionTH] [varchar](max) NOT NULL,
	[MetaDescriptionEN] [varchar](max) NOT NULL,
	[OGTitleTH] [varchar](250) NOT NULL,
	[OGTitleEN] [varchar](250) NOT NULL,
	[OGImageTH] [varchar](250) NOT NULL,
	[OGImageEN] [varchar](250) NOT NULL,
	[OGDescriptionTH] [varchar](max) NOT NULL,
	[OGDescriptionEN] [varchar](max) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[ApproveStatus] [tinyint] NOT NULL,
	[ApproveAdminId] [varchar](8) NOT NULL,
	[OwnerAdminId] [varchar](8) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
 CONSTRAINT [PK_PaymentMethod] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PaymentMethod_Shadow]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PaymentMethod_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[UrlTH] [varchar](250) NOT NULL,
	[UrlEN] [varchar](250) NOT NULL,
	[BriefTH] [varchar](max) NOT NULL,
	[BriefEN] [varchar](max) NOT NULL,
	[BodyTH] [varchar](max) NOT NULL,
	[BodyEN] [varchar](max) NOT NULL,
	[ImageTH] [varchar](250) NOT NULL,
	[ImageEN] [varchar](250) NOT NULL,
	[ImageTitleTH] [varchar](250) NOT NULL,
	[ImageTitleEN] [varchar](250) NOT NULL,
	[ImageAltTH] [varchar](250) NOT NULL,
	[ImageAltEN] [varchar](250) NOT NULL,
	[MetaTitleTH] [varchar](250) NOT NULL,
	[MetaTitleEN] [varchar](250) NOT NULL,
	[MetaKeywordTH] [varchar](max) NOT NULL,
	[MetaKeywordEN] [varchar](max) NOT NULL,
	[MetaDescriptionTH] [varchar](max) NOT NULL,
	[MetaDescriptionEN] [varchar](max) NOT NULL,
	[OGTitleTH] [varchar](250) NOT NULL,
	[OGTitleEN] [varchar](250) NOT NULL,
	[OGImageTH] [varchar](250) NOT NULL,
	[OGImageEN] [varchar](250) NOT NULL,
	[OGDescriptionTH] [varchar](max) NOT NULL,
	[OGDescriptionEN] [varchar](max) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[ApproveStatus] [tinyint] NOT NULL,
	[ApproveAdminId] [varchar](8) NOT NULL,
	[OwnerAdminId] [varchar](8) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Poi]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Poi](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Category] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[Latitude] [float] NOT NULL,
	[Longitude] [float] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
 CONSTRAINT [PK_Poi] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Poi_Shadow]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Poi_Shadow](
	[Action] [varchar](250) NOT NULL,
	[Id] [int] NOT NULL,
	[Category] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[Latitude] [float] NOT NULL,
	[Longitude] [float] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Popup]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Popup](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ImageTH] [varchar](250) NOT NULL,
	[ImageEN] [varchar](250) NOT NULL,
	[UrlTH] [varchar](250) NOT NULL,
	[UrlEN] [varchar](250) NOT NULL,
	[ActiveDate] [date] NOT NULL,
	[ExpireDate] [date] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[ApproveStatus] [tinyint] NOT NULL CONSTRAINT [DF_Popup_ApproveStatus]  DEFAULT ((0)),
	[ApproveAdminId] [varchar](8) NOT NULL,
	[OwnerAdminId] [varchar](8) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
 CONSTRAINT [PK_Popup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Popup_Shadow]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Popup_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[ImageTH] [varchar](250) NOT NULL,
	[ImageEN] [varchar](250) NOT NULL,
	[UrlTH] [varchar](250) NOT NULL,
	[UrlEN] [varchar](250) NOT NULL,
	[ActiveDate] [date] NOT NULL,
	[ExpireDate] [date] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[ApproveStatus] [tinyint] NOT NULL,
	[ApproveAdminId] [varchar](8) NOT NULL,
	[OwnerAdminId] [varchar](8) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Privilege]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Privilege](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[UrlTH] [varchar](250) NOT NULL,
	[UrlEN] [varchar](250) NOT NULL,
	[BriefTH] [varchar](max) NOT NULL,
	[BriefEN] [varchar](max) NOT NULL,
	[BodyTH] [varchar](max) NOT NULL,
	[BodyEN] [varchar](max) NOT NULL,
	[ImageTH] [varchar](250) NOT NULL,
	[ImageEN] [varchar](250) NOT NULL,
	[ImageTitleTH] [varchar](250) NOT NULL,
	[ImageTitleEN] [varchar](250) NOT NULL,
	[ImageAltTH] [varchar](250) NOT NULL,
	[ImageAltEN] [varchar](250) NOT NULL,
	[MetaTitleTH] [varchar](250) NOT NULL,
	[MetaTitleEN] [varchar](250) NOT NULL,
	[MetaKeywordTH] [varchar](max) NOT NULL,
	[MetaKeywordEN] [varchar](max) NOT NULL,
	[MetaDescriptionTH] [varchar](max) NOT NULL,
	[MetaDescriptionEN] [varchar](max) NOT NULL,
	[OGTitleTH] [varchar](250) NOT NULL,
	[OGTitleEN] [varchar](250) NOT NULL,
	[OGImageTH] [varchar](250) NOT NULL,
	[OGImageEN] [varchar](250) NOT NULL,
	[OGDescriptionTH] [varchar](max) NOT NULL,
	[OGDescriptionEN] [varchar](max) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[ApproveStatus] [tinyint] NOT NULL,
	[ApproveAdminId] [varchar](8) NOT NULL,
	[OwnerAdminId] [varchar](8) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
 CONSTRAINT [PK_Privilege] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Privilege_Shadow]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Privilege_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[UrlTH] [varchar](250) NOT NULL,
	[UrlEN] [varchar](250) NOT NULL,
	[BriefTH] [varchar](max) NOT NULL,
	[BriefEN] [varchar](max) NOT NULL,
	[BodyTH] [varchar](max) NOT NULL,
	[BodyEN] [varchar](max) NOT NULL,
	[ImageTH] [varchar](250) NOT NULL,
	[ImageEN] [varchar](250) NOT NULL,
	[ImageTitleTH] [varchar](250) NOT NULL,
	[ImageTitleEN] [varchar](250) NOT NULL,
	[ImageAltTH] [varchar](250) NOT NULL,
	[ImageAltEN] [varchar](250) NOT NULL,
	[MetaTitleTH] [varchar](250) NOT NULL,
	[MetaTitleEN] [varchar](250) NOT NULL,
	[MetaKeywordTH] [varchar](max) NOT NULL,
	[MetaKeywordEN] [varchar](max) NOT NULL,
	[MetaDescriptionTH] [varchar](max) NOT NULL,
	[MetaDescriptionEN] [varchar](max) NOT NULL,
	[OGTitleTH] [varchar](250) NOT NULL,
	[OGTitleEN] [varchar](250) NOT NULL,
	[OGImageTH] [varchar](250) NOT NULL,
	[OGImageEN] [varchar](250) NOT NULL,
	[OGDescriptionTH] [varchar](max) NOT NULL,
	[OGDescriptionEN] [varchar](max) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[ApproveStatus] [tinyint] NOT NULL,
	[ApproveAdminId] [varchar](8) NOT NULL,
	[OwnerAdminId] [varchar](8) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Product]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Product](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SubCategoryId] [int] NOT NULL,
	[CompanyId] [int] NOT NULL,
	[Class] [varchar](2) NOT NULL,
	[SubClass] [varchar](20) NOT NULL,
	[ProductCode] [varchar](19) NOT NULL,
	[InsuranceProductCode] [varchar](20) NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[ActiveDate] [date] NOT NULL,
	[ExpireDate] [date] NOT NULL,
	[CoverageTH] [varchar](max) NOT NULL,
	[CoverageEN] [varchar](max) NOT NULL,
	[ConditionTH] [varchar](max) NOT NULL,
	[ConditionEN] [varchar](max) NOT NULL,
	[ExceptionTH] [varchar](max) NOT NULL,
	[ExceptionEN] [varchar](max) NOT NULL,
	[PrivilegeTH] [varchar](max) NOT NULL,
	[PrivilegeEN] [varchar](max) NOT NULL,
	[SaleChannel] [tinyint] NOT NULL,
	[Status] [tinyint] NOT NULL CONSTRAINT [DF_Product_Status]  DEFAULT ((0)),
	[ApproveStatus] [tinyint] NOT NULL,
	[ApproveAdminId] [varchar](8) NOT NULL,
	[OwnerAdminId] [varchar](8) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[CheckListTH] [varchar](max) NOT NULL CONSTRAINT [DF__Product__CheckLi__4E3E9311]  DEFAULT (''),
	[CheckListEN] [varchar](max) NOT NULL CONSTRAINT [DF__Product__CheckLi__4F32B74A]  DEFAULT (''),
	[ConsentTH] [varchar](max) NOT NULL CONSTRAINT [DF__Product__Consent__5026DB83]  DEFAULT (''),
	[ConsentEN] [varchar](max) NOT NULL CONSTRAINT [DF__Product__ConsenT__511AFFBC]  DEFAULT ('')
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[Product] ADD [ApproveMessage] [varchar](max) NOT NULL DEFAULT ('')
SET ANSI_PADDING ON
ALTER TABLE [dbo].[Product] ADD [SpecialCoverageTH] [varchar](max) NOT NULL DEFAULT ('')
ALTER TABLE [dbo].[Product] ADD [SpecialCoverageEN] [varchar](max) NOT NULL DEFAULT ('')
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Product_Shadow]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Product_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[SubCategoryId] [int] NOT NULL,
	[CompanyId] [int] NOT NULL,
	[Class] [varchar](2) NOT NULL,
	[SubClass] [varchar](20) NOT NULL,
	[ProductCode] [varchar](19) NOT NULL,
	[InsuranceProductCode] [varchar](20) NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[ActiveDate] [date] NOT NULL,
	[ExpireDate] [date] NOT NULL,
	[CoverageTH] [varchar](max) NOT NULL,
	[CoverageEN] [varchar](max) NOT NULL,
	[ConditionTH] [varchar](max) NOT NULL,
	[ConditionEN] [varchar](max) NOT NULL,
	[ExceptionTH] [varchar](max) NOT NULL,
	[ExceptionEN] [varchar](max) NOT NULL,
	[PrivilegeTH] [varchar](max) NOT NULL,
	[PrivilegeEN] [varchar](max) NOT NULL,
	[SaleChannel] [tinyint] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[ApproveStatus] [tinyint] NOT NULL,
	[ApproveAdminId] [varchar](8) NOT NULL,
	[OwnerAdminId] [varchar](8) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL,
	[Serialize] [varchar](max) NULL,
	[CheckListTH] [varchar](max) NOT NULL CONSTRAINT [DF__Product_S__Check__520F23F5]  DEFAULT (''),
	[CheckListEN] [varchar](max) NOT NULL CONSTRAINT [DF__Product_S__Check__5303482E]  DEFAULT (''),
	[ConsentTH] [varchar](max) NOT NULL CONSTRAINT [DF__Product_S__Conse__53F76C67]  DEFAULT (''),
	[ConsentEN] [varchar](max) NOT NULL CONSTRAINT [DF__Product_S__Conse__54EB90A0]  DEFAULT ('')
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[Product_Shadow] ADD [ApproveMessage] [varchar](max) NOT NULL DEFAULT ('')
SET ANSI_PADDING ON
ALTER TABLE [dbo].[Product_Shadow] ADD [SpecialCoverageTH] [varchar](max) NOT NULL DEFAULT ('')
ALTER TABLE [dbo].[Product_Shadow] ADD [SpecialCoverageEN] [varchar](max) NOT NULL DEFAULT ('')

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductApplication]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductApplication](
	[Id] [int] IDENTITY(1000,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[PremiumId] [int] NOT NULL,
	[InsuranceCode] [varchar](50) NOT NULL,
	[ProductCode] [varchar](50) NOT NULL,
	[ProductName] [varchar](50) NOT NULL,
	[SumInsured] [money] NOT NULL,
	[NetPremium] [money] NOT NULL,
	[Duty] [money] NOT NULL,
	[Tax] [money] NOT NULL,
	[Premium] [money] NOT NULL,
	[DisplayPremium] [money] NOT NULL,
	[ActiveDate] [date] NOT NULL,
	[ExpireDate] [date] NOT NULL,
	[EPolicy] [tinyint] NOT NULL,
	[CustomerType] [tinyint] NOT NULL,
	[BuyerName] [varchar](50) NOT NULL,
	[BuyerSurname] [varchar](50) NOT NULL,
	[BuyerTelephone] [varchar](50) NOT NULL,
	[BuyerMobilePhone] [varchar](50) NOT NULL,
	[BuyerEmail] [varchar](250) NOT NULL,
	[BuyerLineId] [varchar](50) NOT NULL,
	[ContactName] [varchar](50) NOT NULL,
	[ContactSurname] [varchar](50) NOT NULL,
	[ContactTelephone] [varchar](50) NOT NULL,
	[ContactMobilePhone] [varchar](50) NOT NULL,
	[ContactEmail] [varchar](250) NOT NULL,
	[ContactLineId] [varchar](50) NOT NULL,
	[AddressBranchType] [tinyint] NOT NULL,
	[AddressBranchNo] [varchar](50) NOT NULL,
	[AddressNo] [varchar](50) NOT NULL,
	[AddressMoo] [varchar](50) NOT NULL,
	[AddressVillage] [varchar](50) NOT NULL,
	[AddressFloor] [varchar](50) NOT NULL,
	[AddressSoi] [varchar](50) NOT NULL,
	[AddressRoad] [varchar](50) NOT NULL,
	[AddressDistrict] [varchar](50) NOT NULL,
	[AddressSubDistrict] [varchar](50) NOT NULL,
	[AddressProvince] [varchar](50) NOT NULL,
	[AddressPostalCode] [varchar](5) NOT NULL,
	[BillingAddressBranchType] [tinyint] NOT NULL,
	[BillingAddressBranchNo] [varchar](50) NOT NULL,
	[BillingAddressNo] [varchar](50) NOT NULL,
	[BillingAddressMoo] [varchar](50) NOT NULL,
	[BillingAddressVillage] [varchar](50) NOT NULL,
	[BillingAddressFloor] [varchar](50) NOT NULL,
	[BillingAddressSoi] [varchar](50) NOT NULL,
	[BillingAddressRoad] [varchar](50) NOT NULL,
	[BillingAddressDistrict] [varchar](50) NOT NULL,
	[BillingAddressSubDistrict] [varchar](50) NOT NULL,
	[BillingAddressProvince] [varchar](50) NOT NULL,
	[BillingAddressPostalCode] [varchar](5) NOT NULL,
	[ShippingAddressBranchType] [tinyint] NOT NULL,
	[ShippingAddressBranchNo] [varchar](50) NOT NULL,
	[ShippingAddressNo] [varchar](50) NOT NULL,
	[ShippingAddressMoo] [varchar](50) NOT NULL,
	[ShippingAddressVillage] [varchar](50) NOT NULL,
	[ShippingAddressFloor] [varchar](50) NOT NULL,
	[ShippingAddressSoi] [varchar](50) NOT NULL,
	[ShippingAddressRoad] [varchar](50) NOT NULL,
	[ShippingAddressDistrict] [varchar](50) NOT NULL,
	[ShippingAddressSubDistrict] [varchar](50) NOT NULL,
	[ShippingAddressProvince] [varchar](50) NOT NULL,
	[ShippingAddressPostalCode] [varchar](5) NOT NULL,
	[Steps] [varchar](50) NOT NULL,
	[OverSteps] [varchar](50) NULL,
	[Step] [int] NOT NULL,
 CONSTRAINT [PK_ProductApplication] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductApplicationAttachment]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductApplicationAttachment](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ApplicationId] [int] NOT NULL,
	[FileCategory] [int] NOT NULL,
	[FileName] [varchar](250) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdateDate] [datetime] NULL,
 CONSTRAINT [PK_ProductApplicationAttachment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductApplicationDriver]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductApplicationDriver](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ApplicationId] [int] NOT NULL,
	[Sequence] [tinyint] NOT NULL,
	[TitleId] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Surname] [varchar](50) NOT NULL,
	[IDType] [tinyint] NOT NULL,
	[IDCard] [varchar](50) NOT NULL,
	[RefIDCard] [varchar](50) NOT NULL,
	[NationalId] [int] NOT NULL,
	[Birthday] [date] NOT NULL,
 CONSTRAINT [PK_ProductApplicationDriver_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductApplicationField]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductApplicationField](
	[Id] [int] NOT NULL,
	[ProductCategoryId] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL CONSTRAINT [DF_ProductApplicationField_TitleEN]  DEFAULT ('ไม่ระบุ'),
	[DataType] [tinyint] NOT NULL CONSTRAINT [DF_ProductApplicationField_DataType]  DEFAULT ((1)),
	[RefId] [int] NOT NULL,
	[Mode] [tinyint] NOT NULL,
	[Page] [int] NOT NULL,
	[Step] [tinyint] NOT NULL,
	[Sequence] [int] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
 CONSTRAINT [PK_ProductApplicationField] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductApplicationField_Shadow]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductApplicationField_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[ProductCategoryId] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[DataType] [tinyint] NOT NULL,
	[RefId] [int] NOT NULL,
	[Mode] [tinyint] NOT NULL,
	[Step] [tinyint] NOT NULL,
	[Sequence] [int] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductApplicationHome]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductApplicationHome](
	[ApplicationId] [int] NOT NULL,
	[PropertyType] [tinyint] NOT NULL,
	[LocationUsage] [tinyint] NOT NULL,
	[BuyerStatus] [tinyint] NOT NULL,
	[ConstructionType] [int] NOT NULL,
	[Floor] [tinyint] NOT NULL,
	[RoomAmount] [int] NOT NULL,
	[SquareMeter] [int] NOT NULL,
	[ConstructionValue] [int] NOT NULL,
	[FunitureValue] [int] NOT NULL,
	[CoverageYear] [int] NOT NULL,
	[AddressBranchType] [tinyint] NOT NULL,
	[AddressBranchNo] [varchar](50) NOT NULL,
	[AddressNo] [varchar](50) NOT NULL,
	[AddressMoo] [varchar](50) NOT NULL,
	[AddressVillage] [varchar](50) NOT NULL,
	[AddressFloor] [varchar](50) NOT NULL,
	[AddressSoi] [varchar](50) NOT NULL,
	[AddressRoad] [varchar](50) NOT NULL,
	[AddressDistrict] [varchar](50) NOT NULL,
	[AddressSubDistrict] [varchar](50) NOT NULL,
	[AddressProvince] [varchar](50) NOT NULL,
	[AddressPostalCode] [varchar](5) NOT NULL,
 CONSTRAINT [PK_ProductApplicationHome] PRIMARY KEY CLUSTERED 
(
	[ApplicationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductApplicationMotor]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductApplicationMotor](
	[ApplicationId] [int] NOT NULL,
	[CarModelId] [int] NOT NULL,
	[CarBrand] [varchar](50) NOT NULL,
	[CarFamily] [varchar](50) NOT NULL,
	[CarModel] [varchar](50) NOT NULL,
	[CarRegisterYear] [smallint] NOT NULL,
	[CarCC] [int] NOT NULL,
	[CarWeight] [int] NOT NULL,
	[CarEngineNo] [varchar](50) NOT NULL,
	[CarSerialNo] [varchar](50) NOT NULL,
	[CarPlateNo1] [varchar](50) NOT NULL,
	[CarPlateNo2] [varchar](50) NOT NULL,
	[CarProvinceId] [int] NOT NULL,
	[CarUsageTypeCode] [varchar](50) NOT NULL,
	[CarUsageType] [varchar](50) NOT NULL,
	[CarCCTV] [tinyint] NOT NULL,
	[CarGarageType] [tinyint] NOT NULL,
	[CarAccessories] [varchar](max) NOT NULL,
	[CompulsoryPremium] [money] NOT NULL,
	[CombinePremium] [money] NOT NULL,
 CONSTRAINT [PK_ProductApplicationMotor] PRIMARY KEY CLUSTERED 
(
	[ApplicationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductApplicationPolicyHolder]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductApplicationPolicyHolder](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ApplicationId] [int] NOT NULL,
	[Sequence] [tinyint] NOT NULL,
	[TitleId] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Surname] [varchar](50) NOT NULL,
	[IDType] [tinyint] NOT NULL,
	[IDCard] [varchar](50) NOT NULL,
	[RefIDCard] [varchar](50) NOT NULL,
	[NationalId] [int] NOT NULL,
	[Birthday] [date] NOT NULL,
	[Age] [tinyint] NOT NULL,
	[Sex] [tinyint] NOT NULL,
	[MaritalStatus] [tinyint] NOT NULL,
	[Weight] [tinyint] NOT NULL,
	[Height] [tinyint] NOT NULL,
	[CareerId] [int] NOT NULL,
	[Position] [varchar](50) NOT NULL,
	[PositionDescription] [varchar](max) NOT NULL,
	[AnnualIncome] [money] NOT NULL,
	[Telephone] [varchar](50) NOT NULL,
	[MobilePhone] [varchar](50) NOT NULL,
	[Email] [varchar](250) NOT NULL,
	[LineId] [varchar](50) NOT NULL,
	[Instagram] [varchar](50) NOT NULL,
	[Facebook] [varchar](50) NOT NULL,
	[BeneficiaryName] [varchar](50) NOT NULL,
	[BeneficiarySurname] [varchar](50) NOT NULL,
	[BeneficiaryIDCard] [varchar](50) NOT NULL,
	[BeneficiaryRelationship] [varchar](50) NOT NULL,
	[BeneficiaryMobilePhone] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ProductApplicationPolicyHolder_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductApplicationTA]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductApplicationTA](
	[ApplicationId] [int] NOT NULL,
	[TravelType] [tinyint] NOT NULL,
	[TravelBy] [int] NOT NULL,
	[CoverageType] [tinyint] NOT NULL,
	[CoverageOption] [tinyint] NOT NULL,
	[Persons] [int] NOT NULL,
	[Day] [int] NOT NULL,
	[DepartureDate] [date] NOT NULL,
	[ReturnDate] [date] NOT NULL,
	[University] [varchar](100) NOT NULL,
	[Country] [int] NOT NULL,
 CONSTRAINT [PK_ProductApplicationTA] PRIMARY KEY CLUSTERED 
(
	[ApplicationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductCategory]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductCategory](
	[Id] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[ImageTH] [varchar](250) NOT NULL,
	[ImageEN] [varchar](250) NOT NULL,
	[BriefTH] [varchar](max) NOT NULL,
	[BriefEN] [varchar](max) NOT NULL,
	[HomeText1TH] [varchar](30) NOT NULL,
	[HomeText1EN] [varchar](30) NOT NULL,
	[HomeText2TH] [varchar](100) NOT NULL,
	[HomeText2EN] [varchar](100) NOT NULL,
	[DisplayHome] [bit] NOT NULL,
	[PaymentCreditCard] [bit] NOT NULL,
	[PaymentCreditCardInstallment] [bit] NOT NULL,
	[PaymentCounterService] [bit] NOT NULL,
	[UrlTH] [varchar](250) NOT NULL,
	[UrlEN] [varchar](250) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[Sequence] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_ProductCategory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductCategory_Shadow]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductCategory_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[ImageTH] [varchar](250) NOT NULL,
	[ImageEN] [varchar](250) NOT NULL,
	[BriefTH] [varchar](max) NOT NULL,
	[BriefEN] [varchar](max) NOT NULL,
	[HomeText1TH] [varchar](30) NOT NULL,
	[HomeText1EN] [varchar](30) NOT NULL,
	[HomeText2TH] [varchar](100) NOT NULL,
	[HomeText2EN] [varchar](100) NOT NULL,
	[DisplayHome] [bit] NOT NULL,
	[PaymentCreditCard] [bit] NOT NULL,
	[PaymentCreditCardInstallment] [bit] NOT NULL,
	[PaymentCounterService] [bit] NOT NULL,
	[UrlTH] [varchar](250) NOT NULL,
	[UrlEN] [varchar](250) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL,
	[Sequence] [int] NOT NULL DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductCompareLog]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductCompareLog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MemberId] [int] NOT NULL,
	[SessionId] [varchar](50) NOT NULL,
	[ProductId1] [int] NOT NULL,
	[PremiumId1] [int] NOT NULL,
	[ProductId2] [int] NOT NULL,
	[PremiumId2] [int] NOT NULL,
	[ProductId3] [int] NOT NULL,
	[PremiumId3] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_ProductCompareLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductHomePremium]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductHomePremium](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[PropertyTypeId] [int] NOT NULL,
	[ConstructionTypeId] [int] NOT NULL,
	[CoverPeriod] [tinyint] NOT NULL,
	[SumInsured] [money] NOT NULL,
	[NetPremium] [money] NOT NULL,
	[Duty] [money] NOT NULL,
	[Vat] [money] NOT NULL,
	[Premium] [money] NOT NULL,
	[DisplayPremium] [money] NOT NULL,
	[Fire] [money] NOT NULL,
	[AccidentalDamage] [money] NOT NULL,
	[NaturalRisk] [money] NOT NULL,
	[TempRental] [money] NOT NULL,
	[Burglary] [money] NOT NULL,
	[PublicLiability] [money] NOT NULL,
	[EPolicy] [bit] NOT NULL CONSTRAINT [DF_ProductHomePremium_EPolicy]  DEFAULT ((0)),
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL
) ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[ProductHomePremium] ADD [Code] [varchar](50) NOT NULL DEFAULT ('')
 CONSTRAINT [PK_ProductHomePremium] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductMotorCarModel]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductMotorCarModel](
	[ProductId] [int] NOT NULL,
	[CarModelId] [int] NOT NULL,
 CONSTRAINT [PK_ProductMotorCarModel] PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC,
	[CarModelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProductMotorPremium]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[ProductMotorPremium](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[YearMin] [tinyint] NOT NULL,
	[YearMax] [tinyint] NOT NULL,
	[RegisterYear] [int] NOT NULL,
	[DriverAgeMin] [tinyint] NOT NULL,
	[DriverAgeMax] [tinyint] NOT NULL,
	[VehicleUsage110] [bit] NOT NULL,
	[VehicleUsage120] [bit] NOT NULL,
	[VehicleUsage210] [bit] NOT NULL,
	[VehicleUsage220] [bit] NOT NULL,
	[VehicleUsage230] [bit] NOT NULL,
	[VehicleUsage320] [bit] NOT NULL,
	[RegisterProvinceAll] [bit] NOT NULL,
	[GarageType] [tinyint] NOT NULL,
	[CarInspector] [bit] NOT NULL,
	[CCTV] [tinyint] NOT NULL,
	[SumInsured] [money] NOT NULL,
	[NetPremium] [money] NOT NULL,
	[Duty] [money] NOT NULL,
	[Vat] [money] NOT NULL,
	[Premium] [money] NOT NULL,
	[DisplayPremium] [money] NULL,
	[CompulsoryPremium] [money] NULL,
	[CombinePremium] [money] NULL,
	[DisplayCombinePremium] [money] NULL,
	[OwnDamage] [money] NOT NULL,
	[OwnExcess] [money] NOT NULL,
	[Flood] [money] NOT NULL,
	[EPolicy] [bit] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [int] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [int] NOT NULL,
	[Code] [varchar](50) NOT NULL DEFAULT (''),
	[CarInspectorMethod] [tinyint] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_ProductMotorPremium] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductMotorPremiumRegisterProvince]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductMotorPremiumRegisterProvince](
	[PremiumId] [int] NOT NULL,
	[RegisterProvinceCode] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ProductMotorPremiumRegisterProvince] PRIMARY KEY CLUSTERED 
(
	[PremiumId] ASC,
	[RegisterProvinceCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductMotorRegisterProvince]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductMotorRegisterProvince](
	[ProductId] [int] NOT NULL,
	[RegisterProvinceId] [int] NOT NULL,
	[RegisterProvinceCode] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ProductMotorRegisterProvince] PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC,
	[RegisterProvinceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductPACareer]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductPACareer](
	[ProductId] [int] NOT NULL,
	[CareerId] [int] NOT NULL,
	[CareerClass] [tinyint] NOT NULL,
 CONSTRAINT [PK_ProductPACareerClass] PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC,
	[CareerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProductPAPremium]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[ProductPAPremium](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[InsuranceType] [tinyint] NOT NULL,
	[AgeCalculation] [tinyint] NOT NULL,
	[MinAge] [tinyint] NOT NULL,
	[MaxAge] [tinyint] NOT NULL,
	[CareerClass1] [bit] NOT NULL,
	[CareerClass2] [bit] NOT NULL,
	[CareerClass3] [bit] NOT NULL,
	[CareerClass4] [bit] NOT NULL,
	[SumInsured] [money] NOT NULL,
	[NetPremium] [money] NOT NULL,
	[Duty] [money] NOT NULL,
	[Vat] [money] NOT NULL,
	[Premium] [money] NOT NULL,
	[DisplayPremium] [money] NOT NULL CONSTRAINT [DF_ProductPAPremium_DisplayPremium]  DEFAULT ((0)),
	[LossLife] [money] NOT NULL,
	[MedicalExpense] [money] NOT NULL,
	[MurderAssault] [money] NOT NULL,
	[RidingPassengerMotorcycle] [money] NOT NULL,
	[FuneralExpenseAccidentIllness] [money] NOT NULL,
	[IncomeCompensation] [money] NOT NULL,
	[EPolicy] [bit] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [int] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [int] NOT NULL,
	[Code] [varchar](50) NOT NULL DEFAULT (''),
 CONSTRAINT [PK_ProductPAPremium] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductPHCareer]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductPHCareer](
	[ProductId] [int] NOT NULL,
	[CareerId] [int] NOT NULL,
	[CareerClass] [tinyint] NOT NULL,
 CONSTRAINT [PK_ProductPHCareerClass] PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC,
	[CareerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProductPHPremium]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[ProductPHPremium](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[Sex] [tinyint] NOT NULL,
	[AgeCalculation] [tinyint] NOT NULL,
	[MinAgeMonth] [tinyint] NOT NULL,
	[MinAge] [tinyint] NOT NULL,
	[MaxAge] [tinyint] NOT NULL,
	[CareerClass1] [bit] NOT NULL,
	[CareerClass2] [bit] NOT NULL,
	[CareerClass3] [bit] NOT NULL,
	[CareerClass4] [bit] NOT NULL,
	[MinWeight] [tinyint] NOT NULL,
	[MaxWeight] [tinyint] NOT NULL,
	[MinHeight] [tinyint] NOT NULL,
	[MaxHeight] [tinyint] NOT NULL,
	[SumInsured] [money] NOT NULL,
	[NetPremium] [money] NOT NULL,
	[Duty] [money] NOT NULL,
	[Vat] [money] NOT NULL,
	[Premium] [money] NOT NULL,
	[DisplayPremium] [money] NOT NULL,
	[IPD] [money] NOT NULL,
	[IPDRoomBoard] [money] NOT NULL,
	[OutpatientBenefits] [money] NOT NULL,
	[SurgicalExpense] [money] NOT NULL,
	[CancerTreatment] [money] NOT NULL,
	[OrganTransplant] [money] NOT NULL,
	[EPolicy] [bit] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [int] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [int] NOT NULL,
	[Code] [varchar](50) NOT NULL DEFAULT (''),
 CONSTRAINT [PK_ProductPHPremium] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductPRB]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductPRB](
	[ProductId] [int] NOT NULL,
	[NetPremium] [money] NOT NULL,
	[Duty] [money] NOT NULL,
	[Vat] [money] NOT NULL,
	[Premium] [money] NOT NULL,
 CONSTRAINT [PK_ProductPRB] PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProductSubCategory]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductSubCategory](
	[Id] [int] NOT NULL,
	[CategoryId] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[Code] [varchar](20) NOT NULL,
 CONSTRAINT [PK_ProductSubCategory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductSubClass]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductSubClass](
	[CategoryId] [int] NOT NULL,
	[Class] [varchar](20) NOT NULL,
	[SubClass] [varchar](20) NOT NULL,
	[SubClassConfGroup] [varchar](250) NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductTACountry]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductTACountry](
	[ProductId] [int] NOT NULL,
	[CountryId] [int] NOT NULL,
 CONSTRAINT [PK_ProductTACountry] PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC,
	[CountryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProductTAPremium]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[ProductTAPremium](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[TripType] [tinyint] NOT NULL,
	[Zone] [tinyint] NOT NULL,
	[CoverageOption] [tinyint] NOT NULL,
	[CoverageType] [tinyint] NOT NULL,
	[MinDays] [smallint] NULL,
	[MaxDays] [smallint] NULL,
	[Persons] [tinyint] NOT NULL,
	[SumInsured] [money] NOT NULL,
	[NetPremium] [money] NOT NULL,
	[Duty] [money] NOT NULL,
	[Vat] [money] NOT NULL,
	[Premium] [money] NOT NULL,
	[DisplayPremium] [money] NOT NULL,
	[PersonalLossLife] [money] NOT NULL,
	[MedicalExpenseEachAccident] [money] NOT NULL,
	[MedicalExpenseAccidentSickness] [money] NOT NULL,
	[EmergencyMedialEvacuation] [money] NOT NULL,
	[RepatriationExpense] [money] NOT NULL,
	[RepatriationMortalRemains] [money] NOT NULL,
	[LossDamageBaggage] [money] NOT NULL,
	[EPolicy] [bit] NOT NULL CONSTRAINT [DF_ProductTAPremium_EPolicy]  DEFAULT ((0)),
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [int] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [int] NOT NULL,
	[Code] [varchar](50) NOT NULL DEFAULT (''),
 CONSTRAINT [PK_ProductTAPremium] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Promotion]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Promotion](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[BodyTH] [varchar](max) NOT NULL,
	[BodyEN] [varchar](max) NOT NULL,
	[PromotionType] [tinyint] NOT NULL,
	[PaymentType] [tinyint] NOT NULL,
	[DiscountType] [tinyint] NOT NULL,
	[DiscountAmount] [money] NOT NULL,
	[ProductType] [tinyint] NOT NULL,
	[ActiveDate] [date] NOT NULL,
	[ExpireDate] [date] NOT NULL,
	[ApproveStatus] [tinyint] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[ApproveAdminId] [varchar](8) NOT NULL,
	[OwnerAdminId] [varchar](8) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[ParentId] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_Promotion] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Promotion_Shadow]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Promotion_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[Code] [varchar](50) NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[BodyTH] [varchar](max) NOT NULL,
	[BodyEN] [varchar](max) NOT NULL,
	[PromotionType] [tinyint] NOT NULL,
	[PaymentType] [tinyint] NOT NULL,
	[DiscountType] [tinyint] NOT NULL,
	[DiscountAmount] [money] NOT NULL,
	[ProductType] [tinyint] NOT NULL,
	[ActiveDate] [date] NOT NULL,
	[ExpireDate] [date] NOT NULL,
	[ApproveStatus] [tinyint] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[ApproveAdminId] [varchar](8) NOT NULL,
	[OwnerAdminId] [varchar](8) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL,
	[Serialize] [varchar](max) NOT NULL,
	[ParentId] [int] NOT NULL DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PromotionBank]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PromotionBank](
	[PromotionId] [int] NOT NULL,
	[BankId] [int] NOT NULL,
	[DiscountType] [tinyint] NOT NULL,
	[DiscountAmount] [money] NOT NULL,
	[IsInstallment] [bit] NOT NULL,
	[InstallmentMonth] [tinyint] NOT NULL,
 CONSTRAINT [PK_PromotionBank] PRIMARY KEY CLUSTERED 
(
	[PromotionId] ASC,
	[BankId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PromotionCompany]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PromotionCompany](
	[PromotionId] [int] NOT NULL,
	[CompanyId] [int] NOT NULL,
 CONSTRAINT [PK_PromotionCompany] PRIMARY KEY CLUSTERED 
(
	[PromotionId] ASC,
	[CompanyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PromotionGift]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PromotionGift](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PromotionId] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
 CONSTRAINT [PK_PromotionGift] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PromotionProduct]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PromotionProduct](
	[PromotionId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
 CONSTRAINT [PK_PromotionProduct] PRIMARY KEY CLUSTERED 
(
	[PromotionId] ASC,
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PromotionProductCategory]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PromotionProductCategory](
	[PromotionId] [int] NOT NULL,
	[ProductCategoryId] [int] NOT NULL,
 CONSTRAINT [PK_PromotionProductCategory] PRIMARY KEY CLUSTERED 
(
	[PromotionId] ASC,
	[ProductCategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Province]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Province](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[Code] [varchar](250) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
 CONSTRAINT [PK_GeoProvince] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Province_Shadow]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[Province_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[Code] [varchar](250) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SubDistrict]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[SubDistrict](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DistrictId] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[Code] [varchar](250) NOT NULL,
	[ZipCode] [varchar](5) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
 CONSTRAINT [PK_GeoSubDistrict] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SubDistrict_Shadow]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[SubDistrict_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[DistrictId] [int] NOT NULL,
	[TitleTH] [varchar](250) NOT NULL,
	[TitleEN] [varchar](250) NOT NULL,
	[Code] [varchar](250) NOT NULL,
	[ZipCode] [varchar](5) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Subscribe]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Subscribe](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Email] [varchar](250) NOT NULL,
	[PublicIP] [varchar](50) NOT NULL,
	[LocalIP] [varchar](50) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Subscribe] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_Subscribe] UNIQUE NONCLUSTERED 
(
	[Email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Subscribe_Shadow]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Subscribe_Shadow](
	[Action] [varchar](250) NOT NULL,
	[Id] [int] NOT NULL,
	[Email] [varchar](250) NOT NULL,
	[PublicIP] [varchar](50) NOT NULL,
	[LocalIP] [varchar](50) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[AdminId] [int] NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SystemMessage]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SystemMessage](
	[Id] [int] NOT NULL,
	[Category] [int] NOT NULL,
	[Code] [varchar](50) NOT NULL,
	[SubCode] [varchar](50) NOT NULL,
	[BodyTH] [varchar](max) NOT NULL,
	[BodyEN] [varchar](max) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
 CONSTRAINT [PK_SystemMessage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SystemMessage_Shadow]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[SystemMessage_Shadow](
	[Action] [varchar](250) NOT NULL,
	[Id] [int] NOT NULL,
	[Category] [int] NOT NULL,
	[Code] [varchar](50) NOT NULL,
	[SubCode] [varchar](50) NOT NULL,
	[BodyTH] [varchar](max) NOT NULL,
	[BodyEN] [varchar](max) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [int] NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tag]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tag](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](250) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
 CONSTRAINT [PK_Tag] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_Tag] UNIQUE NONCLUSTERED 
(
	[Title] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tag_Shadow]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tag_Shadow](
	[Action] [varchar](50) NOT NULL,
	[Id] [int] NOT NULL,
	[Title] [varchar](250) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateAdminId] [varchar](8) NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[UpdateAdminId] [varchar](8) NOT NULL,
	[AdminId] [varchar](8) NOT NULL,
	[ShadowDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserTestimonial]    Script Date: 25/1/2561 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserTestimonial](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Image] [varchar](250) NOT NULL,
	[NameTH] [varchar](50) NOT NULL,
	[NameEN] [varchar](50) NOT NULL,
	[QuoteTH] [varchar](max) NOT NULL,
	[QuoteEN] [varchar](max) NOT NULL,
	[Sequence] [int] NOT NULL,
 CONSTRAINT [PK_UserTestimonial] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_AdminAuthenLog]    Script Date: 25/1/2561 16:16:55 ******/
CREATE NONCLUSTERED INDEX [IX_AdminAuthenLog] ON [dbo].[AdminAuthenLog]
(
	[AdminId] ASC,
	[SessionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_CouponCode]    Script Date: 25/1/2561 16:16:55 ******/
CREATE NONCLUSTERED INDEX [IX_CouponCode] ON [dbo].[CouponCode]
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_LeadForm]    Script Date: 25/1/2561 16:16:55 ******/
CREATE NONCLUSTERED INDEX [IX_LeadForm] ON [dbo].[LeadForm]
(
	[SessionId] ASC,
	[IPPublic] ASC,
	[IPPrivate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_LeadLog]    Script Date: 25/1/2561 16:16:55 ******/
CREATE NONCLUSTERED INDEX [IX_LeadLog] ON [dbo].[LeadLog]
(
	[LeadId] ASC,
	[LeadStatus] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_MasterOption]    Script Date: 25/1/2561 16:16:55 ******/
CREATE NONCLUSTERED INDEX [IX_MasterOption] ON [dbo].[MasterOption]
(
	[Category] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_News_Url_EN]    Script Date: 25/1/2561 16:16:55 ******/
CREATE NONCLUSTERED INDEX [IX_News_Url_EN] ON [dbo].[News]
(
	[UrlPrefix] ASC,
	[UrlEN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_News_Url_TH]    Script Date: 25/1/2561 16:16:55 ******/
CREATE NONCLUSTERED INDEX [IX_News_Url_TH] ON [dbo].[News]
(
	[UrlPrefix] ASC,
	[UrlTH] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_NewsImage]    Script Date: 25/1/2561 16:16:55 ******/
CREATE NONCLUSTERED INDEX [IX_NewsImage] ON [dbo].[NewsImage]
(
	[NewsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ProductHomePremium]    Script Date: 25/1/2561 16:16:55 ******/
CREATE NONCLUSTERED INDEX [IX_ProductHomePremium] ON [dbo].[ProductHomePremium]
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ProductHomePremium_Search]    Script Date: 25/1/2561 16:16:55 ******/
CREATE NONCLUSTERED INDEX [IX_ProductHomePremium_Search] ON [dbo].[ProductHomePremium]
(
	[PropertyTypeId] ASC,
	[ConstructionTypeId] ASC,
	[CoverPeriod] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ProductMotorPremium]    Script Date: 25/1/2561 16:16:55 ******/
CREATE NONCLUSTERED INDEX [IX_ProductMotorPremium] ON [dbo].[ProductMotorPremium]
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_ProductMotorRegisterProvince]    Script Date: 25/1/2561 16:16:55 ******/
CREATE NONCLUSTERED INDEX [IX_ProductMotorRegisterProvince] ON [dbo].[ProductMotorRegisterProvince]
(
	[RegisterProvinceCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ProductPAPremium]    Script Date: 25/1/2561 16:16:55 ******/
CREATE NONCLUSTERED INDEX [IX_ProductPAPremium] ON [dbo].[ProductPAPremium]
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ProductPAPremium_Search]    Script Date: 25/1/2561 16:16:55 ******/
CREATE NONCLUSTERED INDEX [IX_ProductPAPremium_Search] ON [dbo].[ProductPAPremium]
(
	[AgeCalculation] ASC,
	[MinAge] ASC,
	[MaxAge] ASC,
	[Premium] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ProductPHPremium]    Script Date: 25/1/2561 16:16:55 ******/
CREATE NONCLUSTERED INDEX [IX_ProductPHPremium] ON [dbo].[ProductPHPremium]
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ProductPHPremium_Search]    Script Date: 25/1/2561 16:16:55 ******/
CREATE NONCLUSTERED INDEX [IX_ProductPHPremium_Search] ON [dbo].[ProductPHPremium]
(
	[Sex] ASC,
	[MinAge] ASC,
	[MaxAge] ASC,
	[MinWeight] ASC,
	[MaxWeight] ASC,
	[MinHeight] ASC,
	[MaxHeight] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ProductTAPremium]    Script Date: 25/1/2561 16:16:55 ******/
CREATE NONCLUSTERED INDEX [IX_ProductTAPremium] ON [dbo].[ProductTAPremium]
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ProductTAPremium_Search]    Script Date: 25/1/2561 16:16:55 ******/
CREATE NONCLUSTERED INDEX [IX_ProductTAPremium_Search] ON [dbo].[ProductTAPremium]
(
	[TripType] ASC,
	[CoverageOption] ASC,
	[CoverageType] ASC,
	[MinDays] ASC,
	[MaxDays] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OrderProductHomePremium] ADD  CONSTRAINT [DF_OrderProductHomePremium_EPolicy]  DEFAULT ((0)) FOR [EPolicy]
GO
ALTER TABLE [dbo].[OrderProductPAPremium] ADD  CONSTRAINT [DF_OrderProductPAPremium_DisplayPremium]  DEFAULT ((0)) FOR [DisplayPremium]
GO
ALTER TABLE [dbo].[OrderProductTAPremium] ADD  CONSTRAINT [DF_OrderProductTAPremium_EPolicy]  DEFAULT ((0)) FOR [EPolicy]
GO
USE [master]
GO
ALTER DATABASE [TOBJOD] SET  READ_WRITE 
GO
