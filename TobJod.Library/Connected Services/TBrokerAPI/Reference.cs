﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TobJod.TBrokerAPI {
    using System.Data;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="TBrokerAPI.ServiceDigitalSoap")]
    public interface ServiceDigitalSoap {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/CheckDuplicatePolicyMT", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Data.DataSet CheckDuplicatePolicyMT(System.Data.DataSet ds);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/CheckDuplicatePolicyMT", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Data.DataSet> CheckDuplicatePolicyMTAsync(System.Data.DataSet ds);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ReservePolicyMT", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Data.DataSet ReservePolicyMT(System.Data.DataSet ds);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ReservePolicyMT", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Data.DataSet> ReservePolicyMTAsync(System.Data.DataSet ds);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/SubmitPolicyMT", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Data.DataSet SubmitPolicyMT(System.Data.DataSet ds);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/SubmitPolicyMT", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Data.DataSet> SubmitPolicyMTAsync(System.Data.DataSet ds);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/SubmitPolicyFR", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Data.DataSet SubmitPolicyFR(System.Data.DataSet ds);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/SubmitPolicyFR", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Data.DataSet> SubmitPolicyFRAsync(System.Data.DataSet ds);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/SubmitPolicyMI", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Data.DataSet SubmitPolicyMI(System.Data.DataSet ds);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/SubmitPolicyMI", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Data.DataSet> SubmitPolicyMIAsync(System.Data.DataSet ds);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/GetReceivedPolicy", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Data.DataSet GetReceivedPolicy(System.DateTime date, string[] channel, string[] mkt_team);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/GetReceivedPolicy", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Data.DataSet> GetReceivedPolicyAsync(System.DateTime date, string[] channel, string[] mkt_team);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface ServiceDigitalSoapChannel : TobJod.TBrokerAPI.ServiceDigitalSoap, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class ServiceDigitalSoapClient : System.ServiceModel.ClientBase<TobJod.TBrokerAPI.ServiceDigitalSoap>, TobJod.TBrokerAPI.ServiceDigitalSoap {
        
        public ServiceDigitalSoapClient() {
        }
        
        public ServiceDigitalSoapClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public ServiceDigitalSoapClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ServiceDigitalSoapClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ServiceDigitalSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public System.Data.DataSet CheckDuplicatePolicyMT(System.Data.DataSet ds) {
            return base.Channel.CheckDuplicatePolicyMT(ds);
        }
        
        public System.Threading.Tasks.Task<System.Data.DataSet> CheckDuplicatePolicyMTAsync(System.Data.DataSet ds) {
            return base.Channel.CheckDuplicatePolicyMTAsync(ds);
        }
        
        public System.Data.DataSet ReservePolicyMT(System.Data.DataSet ds) {
            return base.Channel.ReservePolicyMT(ds);
        }
        
        public System.Threading.Tasks.Task<System.Data.DataSet> ReservePolicyMTAsync(System.Data.DataSet ds) {
            return base.Channel.ReservePolicyMTAsync(ds);
        }
        
        public System.Data.DataSet SubmitPolicyMT(System.Data.DataSet ds) {
            return base.Channel.SubmitPolicyMT(ds);
        }
        
        public System.Threading.Tasks.Task<System.Data.DataSet> SubmitPolicyMTAsync(System.Data.DataSet ds) {
            return base.Channel.SubmitPolicyMTAsync(ds);
        }
        
        public System.Data.DataSet SubmitPolicyFR(System.Data.DataSet ds) {
            return base.Channel.SubmitPolicyFR(ds);
        }
        
        public System.Threading.Tasks.Task<System.Data.DataSet> SubmitPolicyFRAsync(System.Data.DataSet ds) {
            return base.Channel.SubmitPolicyFRAsync(ds);
        }
        
        public System.Data.DataSet SubmitPolicyMI(System.Data.DataSet ds) {
            return base.Channel.SubmitPolicyMI(ds);
        }
        
        public System.Threading.Tasks.Task<System.Data.DataSet> SubmitPolicyMIAsync(System.Data.DataSet ds) {
            return base.Channel.SubmitPolicyMIAsync(ds);
        }
        
        public System.Data.DataSet GetReceivedPolicy(System.DateTime date, string[] channel, string[] mkt_team) {
            return base.Channel.GetReceivedPolicy(date, channel, mkt_team);
        }
        
        public System.Threading.Tasks.Task<System.Data.DataSet> GetReceivedPolicyAsync(System.DateTime date, string[] channel, string[] mkt_team) {
            return base.Channel.GetReceivedPolicyAsync(date, channel, mkt_team);
        }
    }
}
