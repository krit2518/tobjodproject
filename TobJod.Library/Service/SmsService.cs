﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using TobJod.Models;
using TobJod.Utils;

namespace TobJod.Service {
    public class SmsService {

        public ActionResultStatus RequestOTP(SmsServiceOtpRequest model) {
            ActionResultStatus result = new ActionResultStatus();
            StringBuilder sb = new StringBuilder();
            string request = string.Empty; string response = string.Empty; string error = string.Empty;
            try {
                string url = System.Configuration.ConfigurationManager.AppSettings["SMS_API_URL_REQUEST_OTP"];
                request = JsonConvert.SerializeObject(model);
                response = HttpUtils.HttpPostJson(url, request);
                SmsServiceOtpResponse resp = new JavaScriptSerializer().Deserialize<SmsServiceOtpResponse>(response);
                result.Success = (string.Equals(resp.ResponseCode, "000"));
            } catch (Exception ex) {
                result.Success = false;
                result.ErrorMessage = ex.ToString();
                error = ex.ToString();
            }

            Log("[ REQUEST_OTP ]", request, response, error);

            return result;
        }

        public ActionResultStatus VerifyOTP(SmsServiceOtpVerify model) {
            ActionResultStatus result = new ActionResultStatus();
            StringBuilder sb = new StringBuilder();
            string request = string.Empty; string response = string.Empty; string error = string.Empty;
            try {
                string url = System.Configuration.ConfigurationManager.AppSettings["SMS_API_URL_VERIFY_OTP"];
                request = JsonConvert.SerializeObject(model);
                response = HttpUtils.HttpPostJson(url, request);
                SmsServiceOtpResponse resp = new JavaScriptSerializer().Deserialize<SmsServiceOtpResponse>(response);
                result.Success = (string.Equals(resp.ResponseCode, "000"));
                result.ErrorCode = NullUtils.cvInt(resp.ResponseCode);
            } catch (Exception ex) {
                result.Success = false;
                result.ErrorMessage = ex.ToString();
                error = ex.ToString();
            }

            Log("[ VERIFY ]", request, response, error);

            return result;
        }

        public void Log(string action, string request, string response, string error) {
            var ci = new System.Globalization.CultureInfo("en-GB");
            string logFile = System.Configuration.ConfigurationManager.AppSettings["WebService_Log_Path"] + DateTime.Now.ToString("yyyyMMdd", ci) + "_SMSServiceAPI.log";
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss", ci));
            sb.AppendLine(action);
            sb.AppendLine("[REQUEST]");
            sb.AppendLine(request);
            if (!string.IsNullOrEmpty(response)) {
                sb.AppendLine("[RESPONSE]");
                sb.AppendLine(response);
            }
            if (!string.IsNullOrEmpty(error)) {
                sb.AppendLine("[ERROR]");
                sb.AppendLine(error);
            }
            FileUtils.SaveTextAppend(sb.ToString(), logFile);
        }

    }

    public class SmsServiceOtpRequest {
        [JsonProperty(PropertyName = "uniqueId")]
        public string UniqueId { get; set; }
        [JsonProperty(PropertyName = "mobileNo")]
        public string MobileNo { get; set; }
        [JsonProperty(PropertyName = "channelId")]
        public string ChannelId { get; set; }
        [JsonProperty(PropertyName = "template")]
        public string Template { get; set; }
        [JsonProperty(PropertyName = "refCode")]
        public string RefCode { get; set; }
        [JsonProperty(PropertyName = "refValue")]
        public string RefValue { get; set; }
    }

    public class SmsServiceOtpVerify {
        [JsonProperty(PropertyName = "uniqueId")]
        public string UniqueId { get; set; }
        [JsonProperty(PropertyName = "mobileNo")]
        public string MobileNo { get; set; }
        [JsonProperty(PropertyName = "channelId")]
        public string ChannelId { get; set; }
        [JsonProperty(PropertyName = "otppsw")]
        public string OTPPassword { get; set; }
    }

    public class SmsServiceOtpResponse {
        [JsonProperty(PropertyName = "responseCode")]
        public string ResponseCode { get; set; }

        private bool ResponseCodeStatus() {
            return string.Equals(ResponseCode, "000");
        }
        /*
         000    Successful
         001    Invalid Verification
         998    Verify Limit Exceeded
         999    System Error
         */
    }

}
