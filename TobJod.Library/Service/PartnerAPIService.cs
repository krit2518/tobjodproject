﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Reflection;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Service {
    public class PartnerAPIService : BaseAPIService {

        public PartnerAPIService() {
            DisplayAsTitle = true;
        }
        
        public Tuple<ActionResultStatus, Dictionary<ProductCategoryKey, byte[]>> GenerateExcel(List<int> orderIdList, bool setPassword = false) {
            ActionResultStatus result = new ActionResultStatus();
            Dictionary<ProductCategoryKey, byte[]> books = new Dictionary<ProductCategoryKey, byte[]>();
            //try {
                OrderRepository orderRepository = new OrderRepository();
                ProductApplicationRepository appRepository = new ProductApplicationRepository();
                ProductRepository productRepository = new ProductRepository();

                Dictionary<ProductCategoryKey, List<OrderApplicationProduct>> OrderApplicationProductList = new Dictionary<ProductCategoryKey, List<OrderApplicationProduct>>();


                foreach (var orderId in orderIdList) {
                    Order order = orderRepository.GetOrder_Admin(orderId);
                    if (order == null) continue;
                    ProductApplication app = appRepository.GetProductApplicationForm(order.ProductApplicationId);
                    if (app == null) continue;
                    Product product = productRepository.GetProduct_Admin(app.ProductId);
                    if (product == null) continue;
                    switch (product.CategoryId) {
                        case ProductCategoryKey.Motor:
                            //product.ProductMotorPremium = new List<ProductMotorPremium>();
                            //product.ProductMotorPremium.Add(productRepository.GetProductMotorPremium_Admin(app.PremiumId.ToString()));
                            //break;
                        case ProductCategoryKey.PRB:
                            product.ProductMotorPremium = new List<ProductMotorPremium>();
                            product.ProductMotorPremium.Add(productRepository.GetProductMotorPremium_Admin(app.PremiumId.ToString()));
                            break;
                        case ProductCategoryKey.Home:
                            product.ProductHomePremium = new List<ProductHomePremium>();
                            product.ProductHomePremium.Add(productRepository.GetProductHomePremium_Admin(app.PremiumId.ToString()));
                            break;
                        case ProductCategoryKey.PA:
                            product.ProductPAPremium = new List<ProductPAPremium>();
                            product.ProductPAPremium.Add(productRepository.GetProductPAPremium_Admin(app.PremiumId.ToString()));
                            break;
                        case ProductCategoryKey.PH:
                            product.ProductPHPremium = new List<ProductPHPremium>();
                            product.ProductPHPremium.Add(productRepository.GetProductPHPremium_Admin(app.PremiumId.ToString()));
                            break;
                        case ProductCategoryKey.TA:
                            product.ProductTAPremium = new List<ProductTAPremium>();
                            product.ProductTAPremium.Add(productRepository.GetProductTAPremium_Admin(app.PremiumId.ToString()));
                            break;
                    }

                    OrderApplicationProduct orderApp = OrderApplicationProduct.Map(order);
                    ApplicationProduct appProduct = ApplicationProduct.Map(app);
                    appProduct.Product = product;
                    orderApp.ApplicationProduct = new List<ApplicationProduct>() { appProduct };

                    if (!OrderApplicationProductList.ContainsKey(product.CategoryId)) OrderApplicationProductList.Add(product.CategoryId, new List<OrderApplicationProduct>());
                    OrderApplicationProductList[product.CategoryId].Add(orderApp);

                    if (order.OrderProductMotorPremium != null && order.OrderProductMotorPremium.BuyCompulsory) {
                        if (!OrderApplicationProductList.ContainsKey(ProductCategoryKey.PRB)) OrderApplicationProductList.Add(ProductCategoryKey.PRB, new List<OrderApplicationProduct>());
                        OrderApplicationProductList[ProductCategoryKey.PRB].Add(orderApp);
                    }
                }

                if (OrderApplicationProductList.ContainsKey(ProductCategoryKey.Motor) || OrderApplicationProductList.ContainsKey(ProductCategoryKey.PRB)) {
                    books.Add(ProductCategoryKey.Motor, GenerateExcelProductMotor(
                        (OrderApplicationProductList.ContainsKey(ProductCategoryKey.Motor) ? OrderApplicationProductList[ProductCategoryKey.Motor] : new List<OrderApplicationProduct>()),
                        (OrderApplicationProductList.ContainsKey(ProductCategoryKey.PRB) ? OrderApplicationProductList[ProductCategoryKey.PRB] : new List<OrderApplicationProduct>()), setPassword));
                }
                foreach (var orderApp in OrderApplicationProductList) {
                    switch (orderApp.Key) {
                        //case ProductCategoryKey.Motor:
                        //    books.Add(orderApp.Key, GenerateExcelProductMotor(orderApp.Value));
                        //    break;
                        //case ProductCategoryKey.PRB:
                        //    books.Add(orderApp.Key, GenerateExcelProductCompulsory(orderApp.Value));
                        //    break;
                        case ProductCategoryKey.Home:
                            books.Add(orderApp.Key, GenerateExcelProductHome(orderApp.Value, setPassword));
                            break;
                        case ProductCategoryKey.PA:
                            books.Add(orderApp.Key, GenerateExcelProductPA(orderApp.Value, setPassword));
                            break;
                        case ProductCategoryKey.PH:
                            books.Add(orderApp.Key, GenerateExcelProductPH(orderApp.Value, setPassword));
                            break;
                        case ProductCategoryKey.TA:
                            books.Add(orderApp.Key, GenerateExcelProductTA(orderApp.Value, setPassword));
                            break;
                    }
                }
                result.Success = true;
            //} catch (Exception ex) {
            //    result.Success = false;
            //    result.ErrorCode = 500;
            //    result.ErrorMessage = ex.ToString();
            //}
            return Tuple.Create(result, books);
        }

        public Tuple<ActionResultStatus, string> GenerateObjectJson(int orderId) {
            string json = string.Empty;
            ActionResultStatus result = new ActionResultStatus();
            Dictionary<ProductCategoryKey, object> list = new Dictionary<ProductCategoryKey, object>();
            try {
                OrderRepository orderRepository = new OrderRepository();
                ProductApplicationRepository appRepository = new ProductApplicationRepository();
                ProductRepository productRepository = new ProductRepository();

                Dictionary<ProductCategoryKey, List<OrderApplicationProduct>> OrderApplicationProductList = new Dictionary<ProductCategoryKey, List<OrderApplicationProduct>>();


                Order order = orderRepository.GetOrder_Admin(orderId);
                if (order == null) { result.Success = false; }
                ProductApplication app = appRepository.GetProductApplicationForm(order.ProductApplicationId);
                if (app == null) { result.Success = false; }
                Product product = productRepository.GetProduct_Admin(app.ProductId);
                if (product == null) { result.Success = false; }
                product.ProductMotorPremium = new List<ProductMotorPremium>();
                product.ProductMotorPremium.Add(productRepository.GetProductMotorPremium_Admin(app.PremiumId.ToString()));


                OrderApplicationProduct orderApp = OrderApplicationProduct.Map(order);
                ApplicationProduct appProduct = ApplicationProduct.Map(app);
                appProduct.Product = product;
                orderApp.ApplicationProduct = new List<ApplicationProduct>() { appProduct };

                List<string> ignoreColumns = new List<string>() { "Facebook", "Instagram", "LineId", "Telelphone", "Mobilephone", "PaymentReference", "BeneficiaryTelephone", "BuyerTel", "ContactTel", "ShippingIsChecked", "BillingIsChecked", "PolicyHolderTitleText", "PolicyNo" };
                List<string> ignoreColumnsMotor = new List<string>();
                ignoreColumnsMotor.AddRange(ignoreColumns);
                ignoreColumnsMotor.AddRange(new string[] { "CompulsoryDuty", "CompulsoryVat" });
                List<string> ignoreColumnsPA = new List<string>();
                ignoreColumnsPA.AddRange(ignoreColumns);
                ignoreColumnsPA.Add("Sex");

                IgnorableSerializerContractResolver contractResolver = new IgnorableSerializerContractResolver();// { NamingStrategy = new SnakeCaseNamingStrategy() };
                contractResolver.Ignore(typeof(PartnerApplication), ignoreColumns.ToArray());
                contractResolver.Ignore(typeof(System.Data.Objects.DataClasses.EntityObject));
                var jsonSettings = new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore, ContractResolver = contractResolver };

                IgnorableSerializerContractResolver contractResolverMotor = new IgnorableSerializerContractResolver();// { NamingStrategy = new SnakeCaseNamingStrategy() };
                contractResolverMotor.Ignore(typeof(PartnerApplication), ignoreColumnsMotor.ToArray());
                contractResolverMotor.Ignore(typeof(System.Data.Objects.DataClasses.EntityObject));
                var jsonSettingsMotor = new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore, ContractResolver = contractResolver };

                switch (product.CategoryId) {
                    case ProductCategoryKey.Motor:
                        json = JsonConvert.SerializeObject(GetPartnerApplicationMotor(orderApp), jsonSettingsMotor);
                        break;
                    case ProductCategoryKey.PRB:
                        json = JsonConvert.SerializeObject(GetPartnerApplicationPRB(orderApp), jsonSettingsMotor);
                        break;
                    case ProductCategoryKey.Home:
                        json = JsonConvert.SerializeObject(GetPartnerApplicationHome(orderApp), jsonSettings);
                        break;
                    case ProductCategoryKey.PA:
                        IgnorableSerializerContractResolver contractResolverPA = new IgnorableSerializerContractResolver();// { NamingStrategy = new SnakeCaseNamingStrategy() };
                        contractResolver.Ignore(typeof(PartnerApplication), ignoreColumnsPA.ToArray());
                        contractResolver.Ignore(typeof(System.Data.Objects.DataClasses.EntityObject));
                        var jsonSettingsPA = new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore, ContractResolver = contractResolver };
                        json = JsonConvert.SerializeObject(GetPartnerApplicationPA(orderApp), jsonSettingsPA);
                        break;
                    case ProductCategoryKey.PH:
                        json = JsonConvert.SerializeObject(GetPartnerApplicationPH(orderApp), jsonSettings);
                        break;
                    case ProductCategoryKey.TA:
                        json = JsonConvert.SerializeObject(GetPartnerApplicationTA(orderApp), jsonSettings);
                        break;
                }
                result.Success = true;
            } catch (Exception ex) {
                result.Success = false;
                result.ErrorCode = 500;
                result.ErrorMessage = ex.ToString();
            }
            return Tuple.Create(result, json);
        }

        public class IgnorableSerializerContractResolver : DefaultContractResolver {
            protected readonly Dictionary<Type, HashSet<string>> Ignores;

            public IgnorableSerializerContractResolver() {
                this.Ignores = new Dictionary<Type, HashSet<string>>();
            }

            /// <summary>
            /// Explicitly ignore the given property(s) for the given type
            /// </summary>
            /// <param name="type"></param>
            /// <param name="propertyName">one or more properties to ignore.  Leave empty to ignore the type entirely.</param>
            public void Ignore(Type type, params string[] propertyName) {
                // start bucket if DNE
                if (!this.Ignores.ContainsKey(type)) this.Ignores[type] = new HashSet<string>();

                foreach (var prop in propertyName) {
                    this.Ignores[type].Add(prop);
                }
            }

            /// <summary>
            /// Is the given property for the given type ignored?
            /// </summary>
            /// <param name="type"></param>
            /// <param name="propertyName"></param>
            /// <returns></returns>
            public bool IsIgnored(Type type, string propertyName) {
                if (!this.Ignores.ContainsKey(type)) return false;

                // if no properties provided, ignore the type entirely
                if (this.Ignores[type].Count == 0) return true;

                return this.Ignores[type].Contains(propertyName);
            }

            /// <summary>
            /// The decision logic goes here
            /// </summary>
            /// <param name="member"></param>
            /// <param name="memberSerialization"></param>
            /// <returns></returns>
            protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization) {
                JsonProperty property = base.CreateProperty(member, memberSerialization);

                if (this.IsIgnored(property.DeclaringType, property.PropertyName)
                // need to check basetype as well for EF -- @per comment by user576838
                || this.IsIgnored(property.DeclaringType.BaseType, property.PropertyName)) {
                    property.ShouldSerialize = instance => { return false; };
                }

                return property;
            }
        }

        protected byte[] GenerateExcelProductMotor(List<OrderApplicationProduct> order, List<OrderApplicationProduct> orderCompulsory, bool setPassword = false) {
            MasterOptionRepository masterRepo = new MasterOptionRepository();
            CarRepository carRepo = new CarRepository();
            var moTitle = masterRepo.ListMasterOption_DictionaryIdTitleTH_Admin(MasterOptionCategory.Salutation);
            var moProvince = masterRepo.ListMasterOption_DictionaryIdTitleTH_Admin(MasterOptionCategory.MotorProvince);

            string companyCode = "";
            DateTime date = DateTime.Now;
            using (ExcelPackage pck = new ExcelPackage()) {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Application");
                int row = 1;
                int col = 0;
                int maxColumn = 0;

                //TH
                col++; ws.Cells[row, col].Value = "หมายเลขคำสั่งซื้อ"; ws.Cells[row + 1, col].Value = "OrderId";
                col++; ws.Cells[row, col].Value = "เลขที่สัญญา"; ws.Cells[row + 1, col].Value = "CANo";
                col++; ws.Cells[row, col].Value = "ช่องทางการชำระเงิน"; ws.Cells[row + 1, col].Value = "PaymentChannel";
                col++; ws.Cells[row, col].Value = "รหัสธนาคาร"; ws.Cells[row + 1, col].Value = "PaymentBankCode";
                col++; ws.Cells[row, col].Value = "สถานะการชำระเงิน"; ws.Cells[row + 1, col].Value = "PaymentStatus";
                col++; ws.Cells[row, col].Value = "จำนวนเงินทั้งหมดที่ชำระ"; ws.Cells[row + 1, col].Value = "PaymentTotal";
                col++; ws.Cells[row, col].Value = "วันที่ชำระเงิน"; ws.Cells[row + 1, col].Value = "PaymentDate";
                col++; ws.Cells[row, col].Value = "วันที่ทำรายการ"; ws.Cells[row + 1, col].Value = "TransactionDateTime";
                col++; ws.Cells[row, col].Value = "ประเภทลูกค้า (1.นิติบุคคล, 2.บุคลธรรมดา)"; ws.Cells[row + 1, col].Value = "CustomerType";
                col++; ws.Cells[row, col].Value = "เลขที่นิติบุคคล"; ws.Cells[row + 1, col].Value = "CompanyRegistrationNo";
                col++; ws.Cells[row, col].Value = "คำนำหน้าชื่อผู้เอาประกันภัย"; ws.Cells[row + 1, col].Value = "Title";
                col++; ws.Cells[row, col].Value = "ชื่อ"; ws.Cells[row + 1, col].Value = "Name";
                col++; ws.Cells[row, col].Value = "นามสกุล"; ws.Cells[row + 1, col].Value = "Surname";
                col++; ws.Cells[row, col].Value = "สัญชาติ"; ws.Cells[row + 1, col].Value = "Nationality";
                col++; ws.Cells[row, col].Value = "วันเกิด"; ws.Cells[row + 1, col].Value = "Birthday";
                col++; ws.Cells[row, col].Value = "อายุ"; ws.Cells[row + 1, col].Value = "Age";
                col++; ws.Cells[row, col].Value = "เพศ"; ws.Cells[row + 1, col].Value = "Sex (1=Male, 2=Female)";
                col++; ws.Cells[row, col].Value = "ประเภทของบัตร"; ws.Cells[row + 1, col].Value = "IDCardType (1=บัตรประชาชน, 2=Passport)";
                col++; ws.Cells[row, col].Value = "เลขที่บัตรประชาชน หรือ Passport"; ws.Cells[row + 1, col].Value = "IDCardNo";
                col++; ws.Cells[row, col].Value = "อีเมล์"; ws.Cells[row + 1, col].Value = "Email";
                //col++; ws.Cells[row, col].Value = "โทรศัพท์มือถือ"; ws.Cells[row + 1, col].Value = "MobilePhone";
                //col++; ws.Cells[row, col].Value = "โทรศัพท์"; ws.Cells[row + 1, col].Value = "Telephone";
                //col++; ws.Cells[row, col].Value = "Line Id"; ws.Cells[row + 1, col].Value = "LineId";
                col++; ws.Cells[row, col].Value = "เลขที่ใบขับขี่"; ws.Cells[row + 1, col].Value = "DriverLicenseNo";
                col++; ws.Cells[row, col].Value = "เงื่อนไขการรับกรมธรรม์"; ws.Cells[row + 1, col].Value = "PolicyFormat (E=E-policy, P=ส่งทาง ปณ.)";

                col++; ws.Cells[row, col].Value = "เลขเครื่อง"; ws.Cells[row + 1, col].Value = "CarSerialNo";
                col++; ws.Cells[row, col].Value = "เลขถัง"; ws.Cells[row + 1, col].Value = "CarEngineNo";
                col++; ws.Cells[row, col].Value = "เลขทะเบียนรถ 1"; ws.Cells[row + 1, col].Value = "CarPlateNo1";
                col++; ws.Cells[row, col].Value = "เลขทะเบียนรถ 2"; ws.Cells[row + 1, col].Value = "CarPlateNo2";
                col++; ws.Cells[row, col].Value = "ทะเบียนจังหวัด"; ws.Cells[row + 1, col].Value = "CarPlateProvince";

                col++; ws.Cells[row, col].Value = "ผู้ขับขี่ 1"; ws.Cells[row + 1, col].Value = "ลำดับ"; 
                col++; ws.Cells[row + 1, col].Value = "คำนำหน้า";
                col++; ws.Cells[row + 1, col].Value = "ชื่อ";
                col++; ws.Cells[row + 1, col].Value = "นามสกุล";
                col++; ws.Cells[row + 1, col].Value = "ประเภทบัตร";
                col++; ws.Cells[row + 1, col].Value = "เลขที่บัตรประชาชน หรือ Passport";
                col++; ws.Cells[row + 1, col].Value = "เลขที่ใบขับขี่";
                col++; ws.Cells[row + 1, col].Value = "สัญชาติ";
                col++; ws.Cells[row + 1, col].Value = "วันเดือนปีเกิด";
                ws.Cells[row, (col - 8), row, col].Merge = true;
                col++; ws.Cells[row, col].Value = "ผู้ขับขี่ 2"; ws.Cells[row + 1, col].Value = "ลำดับ";
                col++; ws.Cells[row + 1, col].Value = "คำนำหน้า";
                col++; ws.Cells[row + 1, col].Value = "ชื่อ";
                col++; ws.Cells[row + 1, col].Value = "นามสกุล";
                col++; ws.Cells[row + 1, col].Value = "ประเภทบัตร";
                col++; ws.Cells[row + 1, col].Value = "เลขที่บัตรประชาชน หรือ Passport";
                col++; ws.Cells[row + 1, col].Value = "เลขที่ใบขับขี่";
                col++; ws.Cells[row + 1, col].Value = "สัญชาติ";
                col++; ws.Cells[row + 1, col].Value = "วันเดือนปีเกิด";
                ws.Cells[row, (col - 8), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ที่อยู่ปัจจุบัน"; ws.Cells[row + 1, col].Value = "บ้านเลขที่";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "หมู่";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชื่อหมู่บ้าน/ชื่ออาคาร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชั้นอาคาร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ซอย";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ถนน";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "แขวง/ตำบล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เขต/อำเภอ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "จังหวัด";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "รหัสไปรษณีย์";
                ws.Cells[row, (col - 9), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ที่อยู่ส่งกรมธรรม์"; ws.Cells[row + 1, col].Value = "บ้านเลขที่";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "หมู่";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชื่อหมู่บ้าน/ชื่ออาคาร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชั้นอาคาร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ซอย";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ถนน";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "แขวง/ตำบล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เขต/อำเภอ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "จังหวัด";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "รหัสไปรษณีย์";
                ws.Cells[row, (col - 9), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ที่อยู่ออกใบเสร็จ"; ws.Cells[row + 1, col].Value = "บ้านเลขที่";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "หมู่";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชื่อหมู่บ้าน/ชื่ออาคาร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชั้นอาคาร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ซอย";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ถนน";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "แขวง/ตำบล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เขต/อำเภอ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "จังหวัด";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "รหัสไปรษณีย์";
                ws.Cells[row, (col - 9), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ชื่อตัวแทน"; ws.Cells[row + 1, col].Value = "BrokerName";
                col++; ws.Cells[row, col].Value = "รหัสของ Product"; ws.Cells[row + 1, col].Value = "ProductCode";
                col++; ws.Cells[row, col].Value = "ชื่อ Product"; ws.Cells[row + 1, col].Value = "ProductName";
                col++; ws.Cells[row, col].Value = "รหัสบริษัทประกัน"; ws.Cells[row + 1, col].Value = "InsuranceCompanyCode";

                col++; ws.Cells[row, col].Value = "CCTV"; ws.Cells[row + 1, col].Value = "CarCCTV";
                col++; ws.Cells[row, col].Value = "ประเภทของอู่"; ws.Cells[row + 1, col].Value = "CarGarageType";
                col++; ws.Cells[row, col].Value = "อุปกรณ์เสริม"; ws.Cells[row + 1, col].Value = "CarAccessory";
                col++; ws.Cells[row, col].Value = "มูลค่าอุปกรณ์เสริม"; ws.Cells[row + 1, col].Value = "CarAccessoryPrice";
                col++; ws.Cells[row, col].Value = "ปีจดทะเบียนรถ"; ws.Cells[row + 1, col].Value = "CarRegisterYear";

                col++; ws.Cells[row, col].Value = "ทุนประกัน"; ws.Cells[row + 1, col].Value = "SumInsured";
                col++; ws.Cells[row, col].Value = "เบี้ยกรมธรรม์สุทธิ"; ws.Cells[row + 1, col].Value = "NetPremium";
                col++; ws.Cells[row, col].Value = "เบี้ยกรมธรรม์รวมภาษีอากร"; ws.Cells[row + 1, col].Value = "Premium";

                col++; ws.Cells[row, col].Value = "รหัส Package ของ พ.ร.บ."; ws.Cells[row + 1, col].Value = "CompulsoryCode";
                col++; ws.Cells[row, col].Value = "เบี้ยพรบสุทธิ"; ws.Cells[row + 1, col].Value = "CompulsoryNetPremium";
                col++; ws.Cells[row, col].Value = "เบี้ยพรบรวม"; ws.Cells[row + 1, col].Value = "CompulsoryPremium";

                col++; ws.Cells[row, col].Value = "วันเที่ให้เริ่มคุ้มครอง"; ws.Cells[row + 1, col].Value = "PolicyActiveDate";
                col++; ws.Cells[row, col].Value = "วันที่สิ้นสุดคุ้มครอง"; ws.Cells[row + 1, col].Value = "PolicyExpireDate";

                col++; ws.Cells[row, col].Value = "วันทีเริ่มคุ้มครองพรบ"; ws.Cells[row + 1, col].Value = "CompulsoryActiveDate";
                col++; ws.Cells[row, col].Value = "วันที่สิ้นสุดคุ้มครองพรบ"; ws.Cells[row + 1, col].Value = "CompulsoryExpireDate";

                col++; ws.Cells[row, col].Value = "ยี่ห้อ"; ws.Cells[row + 1, col].Value = "CarBrand";
                col++; ws.Cells[row, col].Value = "รุ่น"; ws.Cells[row + 1, col].Value = "CarFamily";
                col++; ws.Cells[row, col].Value = "รุ่นย่อย"; ws.Cells[row + 1, col].Value = "CarModel";
                col++; ws.Cells[row, col].Value = "ปี"; ws.Cells[row + 1, col].Value = "CarYear";
                col++; ws.Cells[row, col].Value = "CC"; ws.Cells[row + 1, col].Value = "CarCC";
                col++; ws.Cells[row, col].Value = "ประเภทการใช้รถ"; ws.Cells[row + 1, col].Value = "VehicleUsage";

                maxColumn = col;
                row++;
                row++;


                foreach (OrderApplicationProduct orderApp in order) {
                    PartnerApplicationMotor item = GetPartnerApplicationMotor(orderApp);
                    
                    col = 0;
                    col++; ws.Cells[row, col].Value = item.OrderId;
                    col++; ws.Cells[row, col].Value = item.CANo;
                    col++; ws.Cells[row, col].Value = item.PaymentChannel;
                    col++; ws.Cells[row, col].Value = item.PaymentBankCode;
                    col++; ws.Cells[row, col].Value = "SUCCESS";
                    col++; ws.Cells[row, col].Value = item.PaymentTotal;
                    col++; ws.Cells[row, col].Value = FormatExcelDateTime(item.PaymentDate);
                    col++; ws.Cells[row, col].Value = FormatExcelDateTime(item.TransactionDate);

                    col++; ws.Cells[row, col].Value = "2";// item.CustomerType;
                    col++; ws.Cells[row, col].Value = ""; // item.CompanyRegistrationNo;
                    col++; ws.Cells[row, col].Value = item.PolicyHolderTitle;
                    col++; ws.Cells[row, col].Value = item.PolicyHolderName;
                    col++; ws.Cells[row, col].Value = item.PolicyHolderSurname;
                    col++; ws.Cells[row, col].Value = item.PolicyHolderNationality;
                    col++; ws.Cells[row, col].Value = FormatExcelDate(item.PolicyHolderBirthday);
                    col++; ws.Cells[row, col].Value = item.PolicyHolderAge;
                    col++; ws.Cells[row, col].Value = FormatSex(item.PolicyHolderSex);
                    col++; ws.Cells[row, col].Value = item.PolicyHolderCardType;
                    col++; ws.Cells[row, col].Value = item.PolicyHolderIdCard;
                    col++; ws.Cells[row, col].Value = item.PolicyHolderEmail;
                    col++; ws.Cells[row, col].Value = item.PolicyHolderDriverLicenseNo;
                    col++; ws.Cells[row, col].Value = item.PolicyFormat;

                    col++; ws.Cells[row, col].Value = item.CarSerialNo;
                    col++; ws.Cells[row, col].Value = item.CarEngineNo;
                    col++; ws.Cells[row, col].Value = item.CarPlateNo1;
                    col++; ws.Cells[row, col].Value = item.CarPlateNo2;
                    col++; ws.Cells[row, col].Value = item.CarPlateProvince;
                    //col++; ws.Cells[row, col].Value = item.PolicyHolderMobilePhone;
                    //col++; ws.Cells[row, col].Value = item.PolicyHolderTelephone;
                    //col++; ws.Cells[row, col].Value = item.PolicyHolderLineId;

                    if (item.Driver.Count > 0) {
                        col++; ws.Cells[row, col].Value = "1";
                        col++; ws.Cells[row, col].Value = item.Driver[0].Title;
                        col++; ws.Cells[row, col].Value = item.Driver[0].Name;
                        col++; ws.Cells[row, col].Value = item.Driver[0].Surname;
                        col++; ws.Cells[row, col].Value = item.Driver[0].IDCardType;
                        col++; ws.Cells[row, col].Value = item.Driver[0].IDCardNo;
                        col++; ws.Cells[row, col].Value = item.Driver[0].DriverLicenseNo;
                        col++; ws.Cells[row, col].Value = item.Driver[0].Nationality;
                        col++; ws.Cells[row, col].Value = FormatExcelDate(item.Driver[0].Birthday);
                        if (item.Driver.Count > 1) {
                            col++; ws.Cells[row, col].Value = "2";
                            col++; ws.Cells[row, col].Value = item.Driver[1].Title;
                            col++; ws.Cells[row, col].Value = item.Driver[1].Name;
                            col++; ws.Cells[row, col].Value = item.Driver[1].Surname;
                            col++; ws.Cells[row, col].Value = item.Driver[1].IDCardType;
                            col++; ws.Cells[row, col].Value = item.Driver[1].IDCardNo;
                            col++; ws.Cells[row, col].Value = item.Driver[1].DriverLicenseNo;
                            col++; ws.Cells[row, col].Value = item.Driver[1].Nationality;
                            col++; ws.Cells[row, col].Value = FormatExcelDate(item.Driver[1].Birthday);
                        } else {
                            col += 9;
                        }
                    } else {
                        col += 18;
                    }

                    col++; ws.Cells[row, col].Value = item.AddressNo;
                    col++; ws.Cells[row, col].Value = item.AddressMoo;
                    col++; ws.Cells[row, col].Value = item.AddressVillage;
                    col++; ws.Cells[row, col].Value = item.AddressFloor;
                    col++; ws.Cells[row, col].Value = item.AddressSoi;
                    col++; ws.Cells[row, col].Value = item.AddressRoad;
                    col++; ws.Cells[row, col].Value = item.AddressSubDistrict;
                    col++; ws.Cells[row, col].Value = item.AddressDistrict;
                    col++; ws.Cells[row, col].Value = item.AddressProvince;
                    col++; ws.Cells[row, col].Value = item.AddressPostalCode;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressNo;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressMoo;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressVillage;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressFloor;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressSoi;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressRoad;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressSubDistrict;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressDistrict;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressProvince;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressPostalCode;
                    col++; ws.Cells[row, col].Value = item.BillingAddressNo;
                    col++; ws.Cells[row, col].Value = item.BillingAddressMoo;
                    col++; ws.Cells[row, col].Value = item.BillingAddressVillage;
                    col++; ws.Cells[row, col].Value = item.BillingAddressFloor;
                    col++; ws.Cells[row, col].Value = item.BillingAddressSoi;
                    col++; ws.Cells[row, col].Value = item.BillingAddressRoad;
                    col++; ws.Cells[row, col].Value = item.BillingAddressSubDistrict;
                    col++; ws.Cells[row, col].Value = item.BillingAddressDistrict;
                    col++; ws.Cells[row, col].Value = item.BillingAddressProvince;
                    col++; ws.Cells[row, col].Value = item.BillingAddressPostalCode;

                    col++; ws.Cells[row, col].Value = item.TransactionBy;
                    col++; ws.Cells[row, col].Value = item.ProductCode;
                    col++; ws.Cells[row, col].Value = item.ProductName;
                    col++; ws.Cells[row, col].Value = item.InsuranceCompanyCode;

                    col++; ws.Cells[row, col].Value = (string.Equals(item.CarCCTV, "1") ? "Y" : "N");
                    col++; ws.Cells[row, col].Value = item.CarGarageType;
                    col++; ws.Cells[row, col].Value = item.CarAccessory;
                    col++; ws.Cells[row, col].Value = item.CarAccessoryPrice;
                    col++; ws.Cells[row, col].Value = item.CarRegisterYear;

                    col++; ws.Cells[row, col].Value = item.SumInsured;
                    col++; ws.Cells[row, col].Value = item.NetPremium;
                    col++; ws.Cells[row, col].Value = item.Premium;

                    col++; ws.Cells[row, col].Value = item.CompulsoryCode;
                    col++; ws.Cells[row, col].Value = item.CompulsoryNetPremium;
                    col++; ws.Cells[row, col].Value = item.CompulsoryPremium;

                    col++; ws.Cells[row, col].Value = FormatExcelDate(item.PolicyActiveDate);
                    col++; ws.Cells[row, col].Value = FormatExcelDate(item.PolicyExpireDate);

                    col++; ws.Cells[row, col].Value = (item.BuyCompulsory ? FormatExcelDate(item.PolicyActiveDate) : ""); //FormatExcelDate(item.CompulsoryActiveDate); //Compulsory
                    col++; ws.Cells[row, col].Value = (item.BuyCompulsory ? FormatExcelDate(item.PolicyExpireDate) : ""); //FormatExcelDate(item.CompulsoryExpireDate); //Compulsory

                    col++; ws.Cells[row, col].Value = item.CarBrand;
                    col++; ws.Cells[row, col].Value = item.CarFamily;
                    col++; ws.Cells[row, col].Value = item.CarModel;
                    col++; ws.Cells[row, col].Value = item.CarYear;
                    col++; ws.Cells[row, col].Value = item.CarCC;
                    col++; ws.Cells[row, col].Value = item.VehicleUsage;

                    if (row == 3) {
                        companyCode = item.InsuranceCompanyCode;
                        date = item.PaymentDate;
                    }

                    row++;
                }

                foreach (OrderApplicationProduct orderApp in orderCompulsory) {
                    PartnerApplicationMotor item = GetPartnerApplicationPRB(orderApp);

                    col = 0;
                    col++; ws.Cells[row, col].Value = item.OrderId;
                    col++; ws.Cells[row, col].Value = item.CANo;
                    col++; ws.Cells[row, col].Value = item.PaymentChannel;
                    col++; ws.Cells[row, col].Value = item.PaymentBankCode;
                    col++; ws.Cells[row, col].Value = "SUCCESS";
                    col++; ws.Cells[row, col].Value = item.PaymentTotal;
                    col++; ws.Cells[row, col].Value = FormatExcelDateTime(item.PaymentDate);
                    col++; ws.Cells[row, col].Value = FormatExcelDateTime(item.TransactionDate);

                    col++; ws.Cells[row, col].Value = "2";// item.CustomerType;
                    col++; ws.Cells[row, col].Value = ""; // item.CompanyRegistrationNo;
                    col++; ws.Cells[row, col].Value = item.PolicyHolderTitle;
                    col++; ws.Cells[row, col].Value = item.PolicyHolderName;
                    col++; ws.Cells[row, col].Value = item.PolicyHolderSurname;
                    col++; ws.Cells[row, col].Value = item.PolicyHolderNationality;
                    col++; ws.Cells[row, col].Value = FormatExcelDate(item.PolicyHolderBirthday);
                    col++; ws.Cells[row, col].Value = item.PolicyHolderAge;
                    col++; ws.Cells[row, col].Value = FormatSex(item.PolicyHolderSex);
                    col++; ws.Cells[row, col].Value = item.PolicyHolderCardType;
                    col++; ws.Cells[row, col].Value = item.PolicyHolderIdCard;
                    col++; ws.Cells[row, col].Value = item.PolicyHolderEmail;
                    col++; ws.Cells[row, col].Value = item.PolicyHolderDriverLicenseNo;
                    col++; ws.Cells[row, col].Value = item.PolicyFormat;

                    col++; ws.Cells[row, col].Value = item.CarSerialNo;
                    col++; ws.Cells[row, col].Value = item.CarEngineNo;
                    col++; ws.Cells[row, col].Value = item.CarPlateNo1;
                    col++; ws.Cells[row, col].Value = item.CarPlateNo2;
                    col++; ws.Cells[row, col].Value = item.CarPlateProvince;
                    //col++; ws.Cells[row, col].Value = item.PolicyHolderMobilePhone;
                    //col++; ws.Cells[row, col].Value = item.PolicyHolderTelephone;
                    //col++; ws.Cells[row, col].Value = item.PolicyHolderLineId;

                    if (item.Driver.Count > 0) {
                        col++; ws.Cells[row, col].Value = "1";
                        col++; ws.Cells[row, col].Value = item.Driver[0].Title;
                        col++; ws.Cells[row, col].Value = item.Driver[0].Name;
                        col++; ws.Cells[row, col].Value = item.Driver[0].Surname;
                        col++; ws.Cells[row, col].Value = item.Driver[0].IDCardType;
                        col++; ws.Cells[row, col].Value = item.Driver[0].IDCardNo;
                        col++; ws.Cells[row, col].Value = item.Driver[0].DriverLicenseNo;
                        col++; ws.Cells[row, col].Value = item.Driver[0].Nationality;
                        col++; ws.Cells[row, col].Value = FormatExcelDate(item.Driver[0].Birthday);
                        if (item.Driver.Count > 1) {
                            col++; ws.Cells[row, col].Value = "2";
                            col++; ws.Cells[row, col].Value = item.Driver[1].Title;
                            col++; ws.Cells[row, col].Value = item.Driver[1].Name;
                            col++; ws.Cells[row, col].Value = item.Driver[1].Surname;
                            col++; ws.Cells[row, col].Value = item.Driver[1].IDCardType;
                            col++; ws.Cells[row, col].Value = item.Driver[1].IDCardNo;
                            col++; ws.Cells[row, col].Value = item.Driver[1].DriverLicenseNo;
                            col++; ws.Cells[row, col].Value = item.Driver[1].Nationality;
                            col++; ws.Cells[row, col].Value = FormatExcelDate(item.Driver[1].Birthday);
                        } else {
                            col += 9;
                        }
                    } else {
                        col += 18;
                    }

                    col++; ws.Cells[row, col].Value = item.AddressNo;
                    col++; ws.Cells[row, col].Value = item.AddressMoo;
                    col++; ws.Cells[row, col].Value = item.AddressVillage;
                    col++; ws.Cells[row, col].Value = item.AddressFloor;
                    col++; ws.Cells[row, col].Value = item.AddressSoi;
                    col++; ws.Cells[row, col].Value = item.AddressRoad;
                    col++; ws.Cells[row, col].Value = item.AddressSubDistrict;
                    col++; ws.Cells[row, col].Value = item.AddressDistrict;
                    col++; ws.Cells[row, col].Value = item.AddressProvince;
                    col++; ws.Cells[row, col].Value = item.AddressPostalCode;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressNo;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressMoo;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressVillage;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressFloor;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressSoi;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressRoad;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressSubDistrict;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressDistrict;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressProvince;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressPostalCode;
                    col++; ws.Cells[row, col].Value = item.BillingAddressNo;
                    col++; ws.Cells[row, col].Value = item.BillingAddressMoo;
                    col++; ws.Cells[row, col].Value = item.BillingAddressVillage;
                    col++; ws.Cells[row, col].Value = item.BillingAddressFloor;
                    col++; ws.Cells[row, col].Value = item.BillingAddressSoi;
                    col++; ws.Cells[row, col].Value = item.BillingAddressRoad;
                    col++; ws.Cells[row, col].Value = item.BillingAddressSubDistrict;
                    col++; ws.Cells[row, col].Value = item.BillingAddressDistrict;
                    col++; ws.Cells[row, col].Value = item.BillingAddressProvince;
                    col++; ws.Cells[row, col].Value = item.BillingAddressPostalCode;

                    col++; ws.Cells[row, col].Value = item.TransactionBy;
                    col++; ws.Cells[row, col].Value = item.ProductCode;
                    col++; ws.Cells[row, col].Value = item.ProductName;
                    col++; ws.Cells[row, col].Value = item.InsuranceCompanyCode;

                    col++; ws.Cells[row, col].Value = (string.Equals(item.CarCCTV, "1") ? "Y" : "N");
                    col++; ws.Cells[row, col].Value = item.CarGarageType;
                    col++; ws.Cells[row, col].Value = item.CarAccessory;
                    col++; ws.Cells[row, col].Value = item.CarAccessoryPrice;
                    col++; ws.Cells[row, col].Value = item.CarRegisterYear;

                    col++; ws.Cells[row, col].Value = 0;
                    col++; ws.Cells[row, col].Value = 0;
                    col++; ws.Cells[row, col].Value = 0;

                    col++; ws.Cells[row, col].Value = item.ProductCode;
                    col++; ws.Cells[row, col].Value = item.NetPremium;
                    col++; ws.Cells[row, col].Value = item.Premium;

                    col++; ws.Cells[row, col].Value = FormatExcelDate(item.PolicyActiveDate);
                    col++; ws.Cells[row, col].Value = FormatExcelDate(item.PolicyExpireDate);

                    col++; ws.Cells[row, col].Value = FormatExcelDate(item.PolicyActiveDate);
                    col++; ws.Cells[row, col].Value = FormatExcelDate(item.PolicyExpireDate);

                    col++; ws.Cells[row, col].Value = item.CarBrand;
                    col++; ws.Cells[row, col].Value = item.CarFamily;
                    col++; ws.Cells[row, col].Value = item.CarModel;
                    col++; ws.Cells[row, col].Value = item.CarYear;
                    col++; ws.Cells[row, col].Value = item.CarCC;
                    col++; ws.Cells[row, col].Value = item.VehicleUsage;

                    if (row == 3) {
                        companyCode = item.InsuranceCompanyCode;
                        date = item.PaymentDate;
                    }

                    row++;
                }


                //Format the border
                using (ExcelRange rng = ws.Cells[1, 1, 1, maxColumn]) {
                    rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 192, 0));
                    rng.Style.WrapText = true;
                    rng.AutoFitColumns();
                }
                using (ExcelRange rng = ws.Cells[2, 1, 2, maxColumn]) {
                    rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(248, 203, 173));
                    rng.Style.WrapText = true;
                }
                using (ExcelRange rng = ws.Cells[1, 1, (row - 1), maxColumn]) {
                    rng.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Top.Color.SetColor(Color.Black);
                    rng.Style.Border.Bottom.Color.SetColor(Color.Black);
                    rng.Style.Border.Left.Color.SetColor(Color.Black);
                    rng.Style.Border.Right.Color.SetColor(Color.Black);
                }

                if (setPassword) {
                    string password = GenerateExcelPassword(companyCode, DateTime.Now);// date);
                    pck.Encryption.Algorithm = EncryptionAlgorithm.AES128;
                    pck.Encryption.Version = EncryptionVersion.Standard;
                    pck.Workbook.Protection.LockStructure = true;
                    pck.Workbook.Protection.LockWindows = true;
                    ws.Protection.IsProtected = true;
                    pck.Encryption.Password = password;
                }
                return pck.GetAsByteArray();
            }
        }

        //protected byte[] GenerateExcelProductCompulsory(List<OrderApplicationProduct> order, bool setPassword = false) {
        //    MasterOptionRepository masterRepo = new MasterOptionRepository();
        //    CarRepository carRepo = new CarRepository();
        //    var moTitle = masterRepo.ListMasterOption_DictionaryIdTitleTH_Admin(MasterOptionCategory.Salutation);
        //    var moProvince = masterRepo.ListMasterOption_DictionaryIdTitleTH_Admin(MasterOptionCategory.MotorProvince);

        //    string companyCode = "";
        //    DateTime date = DateTime.Now;
        //    using (ExcelPackage pck = new ExcelPackage()) {
        //        ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Application");
        //        int row = 1;
        //        int col = 0;
        //        int maxColumn = 0;

        //        //TH
        //        col++; ws.Cells[row, col].Value = "หมายเลขคำสั่งซื้อ"; ws.Cells[row + 1, col].Value = "OrderId";
        //        col++; ws.Cells[row, col].Value = "เลขที่สัญญา"; ws.Cells[row + 1, col].Value = "CANo";
        //        col++; ws.Cells[row, col].Value = "ช่องทางการชำระเงิน"; ws.Cells[row + 1, col].Value = "PaymentChannel";
        //        col++; ws.Cells[row, col].Value = "รหัสธนาคาร"; ws.Cells[row + 1, col].Value = "PaymentBankCode";
        //        col++; ws.Cells[row, col].Value = "สถานะการชำระเงิน"; ws.Cells[row + 1, col].Value = "PaymentStatus";
        //        col++; ws.Cells[row, col].Value = "จำนวนเงินทั้งหมดที่ชำระ"; ws.Cells[row + 1, col].Value = "PaymentTotal";
        //        col++; ws.Cells[row, col].Value = "วันที่ชำระเงิน"; ws.Cells[row + 1, col].Value = "PaymentDate";
        //        col++; ws.Cells[row, col].Value = "วันที่ทำรายการ"; ws.Cells[row + 1, col].Value = "TransactionDateTime";
        //        col++; ws.Cells[row, col].Value = "ประเภทลูกค้า (1.นิติบุคคล, 2.บุคลธรรมดา)"; ws.Cells[row + 1, col].Value = "CustomerType";
        //        col++; ws.Cells[row, col].Value = "เลขที่นิติบุคคล"; ws.Cells[row + 1, col].Value = "CompanyRegistrationNo";
        //        col++; ws.Cells[row, col].Value = "คำนำหน้าชื่อผู้เอาประกันภัย"; ws.Cells[row + 1, col].Value = "Title";
        //        col++; ws.Cells[row, col].Value = "ชื่อ"; ws.Cells[row + 1, col].Value = "Name";
        //        col++; ws.Cells[row, col].Value = "นามสกุล"; ws.Cells[row + 1, col].Value = "Surname";
        //        col++; ws.Cells[row, col].Value = "สัญชาติ"; ws.Cells[row + 1, col].Value = "Nationality";
        //        col++; ws.Cells[row, col].Value = "วันเกิด"; ws.Cells[row + 1, col].Value = "Birthday";
        //        col++; ws.Cells[row, col].Value = "อายุ"; ws.Cells[row + 1, col].Value = "Age";
        //        col++; ws.Cells[row, col].Value = "เพศ"; ws.Cells[row + 1, col].Value = "Sex (1=Male, 2=Female)";
        //        col++; ws.Cells[row, col].Value = "ประเภทของบัตร"; ws.Cells[row + 1, col].Value = "IDCardType (1=บัตรประชาชน, 2=Passport)";
        //        col++; ws.Cells[row, col].Value = "เลขที่บัตรประชาชน หรือ Passport"; ws.Cells[row + 1, col].Value = "IDCardNo";
        //        col++; ws.Cells[row, col].Value = "อีเมล์"; ws.Cells[row + 1, col].Value = "Email";
        //        //col++; ws.Cells[row, col].Value = "โทรศัพท์มือถือ"; ws.Cells[row + 1, col].Value = "MobilePhone";
        //        //col++; ws.Cells[row, col].Value = "โทรศัพท์"; ws.Cells[row + 1, col].Value = "Telephone";
        //        //col++; ws.Cells[row, col].Value = "Line Id"; ws.Cells[row + 1, col].Value = "LineId";
        //        col++; ws.Cells[row, col].Value = "เลขที่ใบขับขี่"; ws.Cells[row + 1, col].Value = "DriverLicenseNo";
        //        col++; ws.Cells[row, col].Value = "เงื่อนไขการรับกรมธรรม์"; ws.Cells[row + 1, col].Value = "PolicyFormat (E=E-policy, P=ส่งทาง ปณ.)";

        //        col++; ws.Cells[row, col].Value = "เลขเครื่อง"; ws.Cells[row + 1, col].Value = "CarSerialNo";
        //        col++; ws.Cells[row, col].Value = "เลขถัง"; ws.Cells[row + 1, col].Value = "CarEngineNo";
        //        col++; ws.Cells[row, col].Value = "เลขทะเบียนรถ 1"; ws.Cells[row + 1, col].Value = "CarPlateNo1";
        //        col++; ws.Cells[row, col].Value = "เลขทะเบียนรถ 2"; ws.Cells[row + 1, col].Value = "CarPlateNo2";
        //        col++; ws.Cells[row, col].Value = "ทะเบียนจังหวัด"; ws.Cells[row + 1, col].Value = "CarPlateProvince";

        //        col++; ws.Cells[row, col].Value = "ผู้ขับขี่ 1"; ws.Cells[row + 1, col].Value = "ลำดับ";
        //        col++; ws.Cells[row + 1, col].Value = "คำนำหน้า";
        //        col++; ws.Cells[row + 1, col].Value = "ชื่อ";
        //        col++; ws.Cells[row + 1, col].Value = "นามสกุล";
        //        //col++; ws.Cells[row + 1, col].Value = "ประเภทบัตร";
        //        col++; ws.Cells[row + 1, col].Value = "เลขที่บัตรประชาชน";
        //        col++; ws.Cells[row + 1, col].Value = "เลขที่ใบขับขี่";
        //        col++; ws.Cells[row + 1, col].Value = "สัญชาติ";
        //        col++; ws.Cells[row + 1, col].Value = "วันเดือนปีเกิด";
        //        ws.Cells[row, (col - 7), row, col].Merge = true;
        //        col++; ws.Cells[row, col].Value = "ผู้ขับขี่ 2"; ws.Cells[row + 1, col].Value = "ลำดับ";
        //        col++; ws.Cells[row + 1, col].Value = "คำนำหน้า";
        //        col++; ws.Cells[row + 1, col].Value = "ชื่อ";
        //        col++; ws.Cells[row + 1, col].Value = "นามสกุล";
        //        //col++; ws.Cells[row + 1, col].Value = "ประเภทบัตร";
        //        col++; ws.Cells[row + 1, col].Value = "เลขที่บัตรประชาชน";
        //        col++; ws.Cells[row + 1, col].Value = "เลขที่ใบขับขี่";
        //        col++; ws.Cells[row + 1, col].Value = "สัญชาติ";
        //        col++; ws.Cells[row + 1, col].Value = "วันเดือนปีเกิด";
        //        ws.Cells[row, (col - 7), row, col].Merge = true;

        //        col++; ws.Cells[row, col].Value = "ที่อยู่ปัจจุบัน"; ws.Cells[row + 1, col].Value = "บ้านเลขที่";
        //        col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "หมู่";
        //        col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชื่อหมู่บ้าน/ชื่ออาคาร";
        //        col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชั้นอาคาร";
        //        col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ซอย";
        //        col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ถนน";
        //        col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "แขวง/ตำบล";
        //        col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เขต/อำเภอ";
        //        col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "จังหวัด";
        //        col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "รหัสไปรษณีย์";
        //        ws.Cells[row, (col - 9), row, col].Merge = true;

        //        col++; ws.Cells[row, col].Value = "ที่อยู่ส่งกรมธรรม์"; ws.Cells[row + 1, col].Value = "บ้านเลขที่";
        //        col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "หมู่";
        //        col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชื่อหมู่บ้าน/ชื่ออาคาร";
        //        col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชั้นอาคาร";
        //        col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ซอย";
        //        col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ถนน";
        //        col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "แขวง/ตำบล";
        //        col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เขต/อำเภอ";
        //        col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "จังหวัด";
        //        col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "รหัสไปรษณีย์";
        //        ws.Cells[row, (col - 9), row, col].Merge = true;

        //        col++; ws.Cells[row, col].Value = "ที่อยู่ออกใบเสร็จ"; ws.Cells[row + 1, col].Value = "บ้านเลขที่";
        //        col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "หมู่";
        //        col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชื่อหมู่บ้าน/ชื่ออาคาร";
        //        col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชั้นอาคาร";
        //        col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ซอย";
        //        col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ถนน";
        //        col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "แขวง/ตำบล";
        //        col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เขต/อำเภอ";
        //        col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "จังหวัด";
        //        col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "รหัสไปรษณีย์";
        //        ws.Cells[row, (col - 9), row, col].Merge = true;

        //        col++; ws.Cells[row, col].Value = "ชื่อตัวแทน"; ws.Cells[row + 1, col].Value = "BrokerName";
        //        col++; ws.Cells[row, col].Value = "รหัสของ Product"; ws.Cells[row + 1, col].Value = "ProductCode";
        //        col++; ws.Cells[row, col].Value = "ชื่อ Product"; ws.Cells[row + 1, col].Value = "ProductName";
        //        col++; ws.Cells[row, col].Value = "รหัสบริษัทประกัน"; ws.Cells[row + 1, col].Value = "InsuranceCompanyCode";

        //        col++; ws.Cells[row, col].Value = "CCTV"; ws.Cells[row + 1, col].Value = "CarCCTV";
        //        col++; ws.Cells[row, col].Value = "ประเภทของอู่"; ws.Cells[row + 1, col].Value = "CarGarageType";
        //        col++; ws.Cells[row, col].Value = "อุปกรณ์เสริม"; ws.Cells[row + 1, col].Value = "CarAccessory";
        //        col++; ws.Cells[row, col].Value = "มูลค่าอุปกรณ์เสริม"; ws.Cells[row + 1, col].Value = "CarAccessoryPrice";
        //        col++; ws.Cells[row, col].Value = "ปีจดทะเบียนรถ"; ws.Cells[row + 1, col].Value = "CarRegisterYear";

        //        col++; ws.Cells[row, col].Value = "ทุนประกัน"; ws.Cells[row + 1, col].Value = "SumInsured";
        //        col++; ws.Cells[row, col].Value = "เบี้ยกรมธรรม์รวมภาษีอากร"; ws.Cells[row + 1, col].Value = "Premium";

        //        col++; ws.Cells[row, col].Value = "รหัส Package ของ พ.ร.บ."; ws.Cells[row + 1, col].Value = "CompulsoryCode";
        //        col++; ws.Cells[row, col].Value = "เบี้ยพรบรวม"; ws.Cells[row + 1, col].Value = "CompulsoryPremium";

        //        col++; ws.Cells[row, col].Value = "วันเที่ให้เริ่มคุ้มครอง"; ws.Cells[row + 1, col].Value = "PolicyActiveDate";
        //        col++; ws.Cells[row, col].Value = "วันที่สิ้นสุดคุ้มครอง"; ws.Cells[row + 1, col].Value = "PolicyExpireDate";

        //        col++; ws.Cells[row, col].Value = "วันทีเริ่มคุ้มครองพรบ"; ws.Cells[row + 1, col].Value = "CompulsoryActiveDate";
        //        col++; ws.Cells[row, col].Value = "วันที่สิ้นสุดคุ้มครองพรบ"; ws.Cells[row + 1, col].Value = "CompulsoryExpireDate";

        //        col++; ws.Cells[row, col].Value = "ยี่ห้อ"; ws.Cells[row + 1, col].Value = "CarBrand";
        //        col++; ws.Cells[row, col].Value = "รุ่น"; ws.Cells[row + 1, col].Value = "CarFamily";
        //        col++; ws.Cells[row, col].Value = "รุ่นย่อย"; ws.Cells[row + 1, col].Value = "CarModel";
        //        col++; ws.Cells[row, col].Value = "ปี"; ws.Cells[row + 1, col].Value = "CarYear";
        //        col++; ws.Cells[row, col].Value = "CC"; ws.Cells[row + 1, col].Value = "CarCC";
        //        col++; ws.Cells[row, col].Value = "ประเภทการใช้รถ"; ws.Cells[row + 1, col].Value = "VehicleUsage";

        //        maxColumn = col;
        //        row++;
        //        row++;


        //        foreach (OrderApplicationProduct orderApp in order) {
        //            PartnerApplicationMotor item = GetPartnerApplicationPRB(orderApp);
        //            col = 0;
        //            col++; ws.Cells[row, col].Value = item.OrderId;
        //            col++; ws.Cells[row, col].Value = item.CANo;
        //            col++; ws.Cells[row, col].Value = item.PaymentChannel;
        //            col++; ws.Cells[row, col].Value = item.PaymentBankCode;
        //            col++; ws.Cells[row, col].Value = "SUCCESS";
        //            col++; ws.Cells[row, col].Value = item.PaymentTotal;
        //            col++; ws.Cells[row, col].Value = FormatExcelDateTime(item.PaymentDate);
        //            col++; ws.Cells[row, col].Value = FormatExcelDateTime(item.TransactionDate);

        //            col++; ws.Cells[row, col].Value = "2";// item.CustomerType;
        //            col++; ws.Cells[row, col].Value = ""; // item.CompanyRegistrationNo;
        //            col++; ws.Cells[row, col].Value = item.PolicyHolderTitle;
        //            col++; ws.Cells[row, col].Value = item.PolicyHolderName;
        //            col++; ws.Cells[row, col].Value = item.PolicyHolderSurname;
        //            col++; ws.Cells[row, col].Value = item.PolicyHolderNationality;
        //            col++; ws.Cells[row, col].Value = FormatExcelDate(item.PolicyHolderBirthday);
        //            col++; ws.Cells[row, col].Value = item.PolicyHolderAge;
        //            col++; ws.Cells[row, col].Value = FormatSex(item.PolicyHolderSex);
        //            col++; ws.Cells[row, col].Value = item.PolicyHolderCardType;
        //            col++; ws.Cells[row, col].Value = item.PolicyHolderIdCard;
        //            col++; ws.Cells[row, col].Value = item.PolicyHolderEmail;
        //            col++; ws.Cells[row, col].Value = item.PolicyHolderDriverLicenseNo;
        //            col++; ws.Cells[row, col].Value = item.PolicyFormat;

        //            col++; ws.Cells[row, col].Value = item.CarSerialNo;
        //            col++; ws.Cells[row, col].Value = item.CarEngineNo;
        //            col++; ws.Cells[row, col].Value = item.CarPlateNo1;
        //            col++; ws.Cells[row, col].Value = item.CarPlateNo2;
        //            col++; ws.Cells[row, col].Value = item.CarPlateProvince;
        //            //col++; ws.Cells[row, col].Value = item.PolicyHolderMobilePhone;
        //            //col++; ws.Cells[row, col].Value = item.PolicyHolderTelephone;
        //            //col++; ws.Cells[row, col].Value = item.PolicyHolderLineId;

        //            if (item.Driver.Count > 0) {
        //                col++; ws.Cells[row, col].Value = "1";
        //                col++; ws.Cells[row, col].Value = item.Driver[0].Title;
        //                col++; ws.Cells[row, col].Value = item.Driver[0].Name;
        //                col++; ws.Cells[row, col].Value = item.Driver[0].Surname;
        //                //col++; ws.Cells[row, col].Value = item.Driver[0].IDCardType;
        //                col++; ws.Cells[row, col].Value = item.Driver[0].IDCardNo;
        //                col++; ws.Cells[row, col].Value = item.Driver[0].DriverLicenseNo;
        //                col++; ws.Cells[row, col].Value = item.Driver[0].Nationality;
        //                col++; ws.Cells[row, col].Value = FormatExcelDate(item.Driver[0].Birthday);
        //                if (item.Driver.Count > 1) {
        //                    col++; ws.Cells[row, col].Value = "2";
        //                    col++; ws.Cells[row, col].Value = item.Driver[1].Title;
        //                    col++; ws.Cells[row, col].Value = item.Driver[1].Name;
        //                    col++; ws.Cells[row, col].Value = item.Driver[1].Surname;
        //                    //col++; ws.Cells[row, col].Value = item.Driver[1].IDCardType;
        //                    col++; ws.Cells[row, col].Value = item.Driver[1].IDCardNo;
        //                    col++; ws.Cells[row, col].Value = item.Driver[1].DriverLicenseNo;
        //                    col++; ws.Cells[row, col].Value = item.Driver[1].Nationality;
        //                    col++; ws.Cells[row, col].Value = FormatExcelDate(item.Driver[1].Birthday);
        //                } else {
        //                    col += 8;
        //                }
        //            } else {
        //                col += 16;
        //            }

        //            col++; ws.Cells[row, col].Value = item.AddressNo;
        //            col++; ws.Cells[row, col].Value = item.AddressMoo;
        //            col++; ws.Cells[row, col].Value = item.AddressVillage;
        //            col++; ws.Cells[row, col].Value = item.AddressFloor;
        //            col++; ws.Cells[row, col].Value = item.AddressSoi;
        //            col++; ws.Cells[row, col].Value = item.AddressRoad;
        //            col++; ws.Cells[row, col].Value = item.AddressSubDistrict;
        //            col++; ws.Cells[row, col].Value = item.AddressDistrict;
        //            col++; ws.Cells[row, col].Value = item.AddressProvince;
        //            col++; ws.Cells[row, col].Value = item.AddressPostalCode;
        //            col++; ws.Cells[row, col].Value = item.ShippingAddressNo;
        //            col++; ws.Cells[row, col].Value = item.ShippingAddressMoo;
        //            col++; ws.Cells[row, col].Value = item.ShippingAddressVillage;
        //            col++; ws.Cells[row, col].Value = item.ShippingAddressFloor;
        //            col++; ws.Cells[row, col].Value = item.ShippingAddressSoi;
        //            col++; ws.Cells[row, col].Value = item.ShippingAddressRoad;
        //            col++; ws.Cells[row, col].Value = item.ShippingAddressSubDistrict;
        //            col++; ws.Cells[row, col].Value = item.ShippingAddressDistrict;
        //            col++; ws.Cells[row, col].Value = item.ShippingAddressProvince;
        //            col++; ws.Cells[row, col].Value = item.ShippingAddressPostalCode;
        //            col++; ws.Cells[row, col].Value = item.BillingAddressNo;
        //            col++; ws.Cells[row, col].Value = item.BillingAddressMoo;
        //            col++; ws.Cells[row, col].Value = item.BillingAddressVillage;
        //            col++; ws.Cells[row, col].Value = item.BillingAddressFloor;
        //            col++; ws.Cells[row, col].Value = item.BillingAddressSoi;
        //            col++; ws.Cells[row, col].Value = item.BillingAddressRoad;
        //            col++; ws.Cells[row, col].Value = item.BillingAddressSubDistrict;
        //            col++; ws.Cells[row, col].Value = item.BillingAddressDistrict;
        //            col++; ws.Cells[row, col].Value = item.BillingAddressProvince;
        //            col++; ws.Cells[row, col].Value = item.BillingAddressPostalCode;

        //            col++; ws.Cells[row, col].Value = item.TransactionBy;
        //            col++; ws.Cells[row, col].Value = item.ProductCode;
        //            col++; ws.Cells[row, col].Value = item.ProductName;
        //            col++; ws.Cells[row, col].Value = item.InsuranceCompanyCode;

        //            col++; ws.Cells[row, col].Value = (string.Equals(item.CarCCTV, "1") ? "Y" : "N");
        //            col++; ws.Cells[row, col].Value = item.CarGarageType;
        //            col++; ws.Cells[row, col].Value = item.CarAccessory;
        //            col++; ws.Cells[row, col].Value = item.CarAccessoryPrice;
        //            col++; ws.Cells[row, col].Value = item.CarRegisterYear;

        //            col++; ws.Cells[row, col].Value = item.SumInsured;
        //            col++; ws.Cells[row, col].Value = item.Premium;

        //            col++; ws.Cells[row, col].Value = item.CompulsoryCode; //(item.BuyCompulsory ? "C" : "");
        //            col++; ws.Cells[row, col].Value = item.CompulsoryPremium;

        //            col++; ws.Cells[row, col].Value = FormatExcelDate(item.PolicyActiveDate);
        //            col++; ws.Cells[row, col].Value = FormatExcelDate(item.PolicyExpireDate);

        //            col++; ws.Cells[row, col].Value = FormatExcelDate(item.PolicyActiveDate); //FormatExcelDate(item.CompulsoryActiveDate); //Compulsory
        //            col++; ws.Cells[row, col].Value = FormatExcelDate(item.PolicyExpireDate); //FormatExcelDate(item.CompulsoryExpireDate); //Compulsory

        //            col++; ws.Cells[row, col].Value = item.CarBrand;
        //            col++; ws.Cells[row, col].Value = item.CarFamily;
        //            col++; ws.Cells[row, col].Value = item.CarModel;
        //            col++; ws.Cells[row, col].Value = item.CarYear;
        //            col++; ws.Cells[row, col].Value = item.CarCC;
        //            col++; ws.Cells[row, col].Value = item.VehicleUsage;

        //            if (row == 1) {
        //                companyCode = item.InsuranceCompanyCode;
        //                date = item.PaymentDate;
        //            }

        //            row++;
        //        }


        //        //Format the border
        //        using (ExcelRange rng = ws.Cells[1, 1, 1, maxColumn]) {
        //            rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
        //            rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 192, 0));
        //            rng.Style.WrapText = true;
        //            rng.AutoFitColumns();
        //        }
        //        using (ExcelRange rng = ws.Cells[2, 1, 2, maxColumn]) {
        //            rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
        //            rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(248, 203, 173));
        //            rng.Style.WrapText = true;
        //        }
        //        using (ExcelRange rng = ws.Cells[1, 1, (row - 1), maxColumn]) {
        //            rng.Style.Border.Top.Style = ExcelBorderStyle.Thin;
        //            rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
        //            rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
        //            rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
        //            rng.Style.Border.Top.Color.SetColor(Color.Black);
        //            rng.Style.Border.Bottom.Color.SetColor(Color.Black);
        //            rng.Style.Border.Left.Color.SetColor(Color.Black);
        //            rng.Style.Border.Right.Color.SetColor(Color.Black);
        //        }

        //        if (setPassword) {
        //            string password = GenerateExcelPassword(companyCode, date);
        //            return pck.GetAsByteArray(password);
        //        }
        //        return pck.GetAsByteArray();
        //    }
        //}

        protected byte[] GenerateExcelProductHome(List<OrderApplicationProduct> order, bool setPassword = false) {
            string companyCode = "";
            DateTime date = DateTime.Now;
            using (ExcelPackage pck = new ExcelPackage()) {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Application");
                int row = 1;
                int col = 0;
                int maxColumn = 0;

                //TH
                col++; ws.Cells[row, col].Value = "หมายเลขคำสั่งซื้อ"; ws.Cells[row + 1, col].Value = "OrderId";
                col++; ws.Cells[row, col].Value = "เลขที่สัญญา"; ws.Cells[row + 1, col].Value = "CANo";
                col++; ws.Cells[row, col].Value = "ช่องทางการชำระเงิน"; ws.Cells[row + 1, col].Value = "PaymentChannel";
                col++; ws.Cells[row, col].Value = "รหัสธนาคาร"; ws.Cells[row + 1, col].Value = "PaymentBankCode";
                col++; ws.Cells[row, col].Value = "สถานะการชำระเงิน"; ws.Cells[row + 1, col].Value = "PaymentStatus";
                col++; ws.Cells[row, col].Value = "จำนวนเงินทั้งหมดที่ชำระ"; ws.Cells[row + 1, col].Value = "PaymentTotal";
                col++; ws.Cells[row, col].Value = "วันที่ชำระเงิน"; ws.Cells[row + 1, col].Value = "PaymentDate";
                col++; ws.Cells[row, col].Value = "วันที่ทำรายการ"; ws.Cells[row + 1, col].Value = "TransactionDateTime";
                col++; ws.Cells[row, col].Value = "ประเภทลูกค้า (1.นิติบุคคล, 2.บุคลธรรมดา)"; ws.Cells[row + 1, col].Value = "CustomerType";
                col++; ws.Cells[row, col].Value = "เลขที่นิติบุคคล"; ws.Cells[row + 1, col].Value = "CompanyRegistrationNo";
                col++; ws.Cells[row, col].Value = "คำนำหน้าชื่อผู้เอาประกันภัย"; ws.Cells[row + 1, col].Value = "Title";
                col++; ws.Cells[row, col].Value = "ชื่อ"; ws.Cells[row + 1, col].Value = "Name";
                col++; ws.Cells[row, col].Value = "นามสกุล"; ws.Cells[row + 1, col].Value = "Surname";
                col++; ws.Cells[row, col].Value = "สัญชาติ"; ws.Cells[row + 1, col].Value = "Nationality";
                col++; ws.Cells[row, col].Value = "วันเกิด"; ws.Cells[row + 1, col].Value = "Birthday";
                col++; ws.Cells[row, col].Value = "อายุ"; ws.Cells[row + 1, col].Value = "Age";
                col++; ws.Cells[row, col].Value = "เพศ"; ws.Cells[row + 1, col].Value = "Sex (1=Male, 2=Female)";
                col++; ws.Cells[row, col].Value = "ประเภทของบัตร"; ws.Cells[row + 1, col].Value = "IDCardType (1=บัตรประชาชน, 2=Passport)";
                col++; ws.Cells[row, col].Value = "เลขที่บัตรประชาชน หรือ Passport"; ws.Cells[row + 1, col].Value = "IDCardNo";
                col++; ws.Cells[row, col].Value = "อีเมล์"; ws.Cells[row + 1, col].Value = "Email";
                //col++; ws.Cells[row, col].Value = "โทรศัพท์มือถือ"; ws.Cells[row + 1, col].Value = "MobilePhone";
                //col++; ws.Cells[row, col].Value = "โทรศัพท์"; ws.Cells[row + 1, col].Value = "Telephone";
                //col++; ws.Cells[row, col].Value = "Line Id"; ws.Cells[row + 1, col].Value = "LineId";
                col++; ws.Cells[row, col].Value = "อาชีพ"; ws.Cells[row + 1, col].Value = "Job";
                col++; ws.Cells[row, col].Value = "เงื่อนไขการรับกรมธรรม์"; ws.Cells[row + 1, col].Value = "PolicyFormat (E=E-policy, P=ส่งทาง ปณ.)";

                col++; ws.Cells[row, col].Value = "ที่อยู่ปัจจุบัน"; ws.Cells[row + 1, col].Value = "บ้านเลขที่";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "หมู่";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชื่อหมู่บ้าน/ชื่ออาคาร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชั้นอาคาร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ซอย";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ถนน";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "แขวง/ตำบล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เขต/อำเภอ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "จังหวัด";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "รหัสไปรษณีย์";
                ws.Cells[row, (col - 9), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ที่ตั้งทรัพย์สิน"; ws.Cells[row + 1, col].Value = "บ้านเลขที่";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "หมู่";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชื่อหมู่บ้าน/ชื่ออาคาร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชั้นอาคาร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ซอย";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ถนน";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "แขวง/ตำบล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เขต/อำเภอ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "จังหวัด";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "รหัสไปรษณีย์";
                ws.Cells[row, (col - 9), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ที่อยู่ส่งกรมธรรม์"; ws.Cells[row + 1, col].Value = "บ้านเลขที่";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "หมู่";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชื่อหมู่บ้าน/ชื่ออาคาร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชั้นอาคาร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ซอย";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ถนน";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "แขวง/ตำบล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เขต/อำเภอ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "จังหวัด";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "รหัสไปรษณีย์";
                ws.Cells[row, (col - 9), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ที่อยู่ออกใบเสร็จ"; ws.Cells[row + 1, col].Value = "บ้านเลขที่";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "หมู่";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชื่อหมู่บ้าน/ชื่ออาคาร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชั้นอาคาร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ซอย";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ถนน";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "แขวง/ตำบล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เขต/อำเภอ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "จังหวัด";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "รหัสไปรษณีย์";
                ws.Cells[row, (col - 9), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ข้อมูลผู้รับผลประโยขน์"; ws.Cells[row + 1, col].Value = "ชื่อ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "นามสกุล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ประเภทของบัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เลขที่บัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ความสัมพันธ์";
                //col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เบอร์ติดต่อ";
                ws.Cells[row, (col - 4), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ชื่อผู้ขอเอาประกันภัย(กรณีซื้อแทนผู้เอาประกันภัย)"; ws.Cells[row + 1, col].Value = "ชื่อ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "นามสกุล";
                //col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เบอร์ติดต่อ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "อีเมล";
                ws.Cells[row, (col - 2), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ผู้ติดต่อกรณีฉุกเฉิน"; ws.Cells[row + 1, col].Value = "ชื่อ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "นามสกุล";
                //col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เบอร์ติดต่อ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "อีเมล";
                ws.Cells[row, (col - 2), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ประเภทอาคาร"; ws.Cells[row + 1, col].Value = "PropertyType";
                col++; ws.Cells[row, col].Value = "รูปแบบสถานที่ใช้"; ws.Cells[row + 1, col].Value = "UsageType";
                col++; ws.Cells[row, col].Value = "ลักษณะสิ่งปลูกสร้าง"; ws.Cells[row + 1, col].Value = "ConstructionType";
                col++; ws.Cells[row, col].Value = "ขนาดพื้นที่ของอาคาร"; ws.Cells[row + 1, col].Value = "Size";
                col++; ws.Cells[row, col].Value = "จำนวนคูหา/หลัง/ห้อง"; ws.Cells[row + 1, col].Value = "RoomAmount";
                col++; ws.Cells[row, col].Value = "จำนวนชั้น"; ws.Cells[row + 1, col].Value = "Floor";
                col++; ws.Cells[row, col].Value = "ผู้ขอเอาประกันภัยมีฐานะเป็น"; ws.Cells[row + 1, col].Value = "OwnerStatus";
                col++; ws.Cells[row, col].Value = "มูลค่าอาคารสิ่งปลูกสร้าง"; ws.Cells[row + 1, col].Value = "ConstructionValue";
                col++; ws.Cells[row, col].Value = "มูลค่าเฟอร์นิเจอร์"; ws.Cells[row + 1, col].Value = "FurnitureValue";

                col++; ws.Cells[row, col].Value = "รหัสบริษัทประกัน"; ws.Cells[row + 1, col].Value = "InsuranceCompanyCode";
                col++; ws.Cells[row, col].Value = "รหัสของ Product"; ws.Cells[row + 1, col].Value = "ProductCode";
                col++; ws.Cells[row, col].Value = "ชื่อ Product"; ws.Cells[row + 1, col].Value = "ProductName";

                col++; ws.Cells[row, col].Value = "ทุนประกัน"; ws.Cells[row + 1, col].Value = "SumInsured";
                col++; ws.Cells[row, col].Value = "เบี้ยกรมธรรม์สุทธิ"; ws.Cells[row + 1, col].Value = "NetPremium";
                col++; ws.Cells[row, col].Value = "เบี้ยกรมธรรม์รวมภาษีอากร"; ws.Cells[row + 1, col].Value = "Premium";

                col++; ws.Cells[row, col].Value = "วันเที่ให้เริ่มคุ้มครอง"; ws.Cells[row + 1, col].Value = "PolicyActiveDate";
                col++; ws.Cells[row, col].Value = "วันที่สิ้นสุดคุ้มครอง"; ws.Cells[row + 1, col].Value = "PolicyExpireDate";

                col++; ws.Cells[row, col].Value = "ชื่อตัวแทน"; ws.Cells[row + 1, col].Value = "BrokerName";
                
                maxColumn = col;
                row++;
                row++;


                foreach (OrderApplicationProduct orderApp in order) {
                    PartnerApplicationHome item = GetPartnerApplicationHome(orderApp);
                    col = 0;
                    col++; ws.Cells[row, col].Value = item.OrderId;
                    col++; ws.Cells[row, col].Value = item.CANo;
                    col++; ws.Cells[row, col].Value = item.PaymentChannel;
                    col++; ws.Cells[row, col].Value = item.PaymentBankCode;
                    col++; ws.Cells[row, col].Value = "SUCCESS";
                    col++; ws.Cells[row, col].Value = item.PaymentTotal;
                    col++; ws.Cells[row, col].Value = FormatExcelDateTime(item.PaymentDate);
                    col++; ws.Cells[row, col].Value = FormatExcelDateTime(item.TransactionDate);
                    col++; ws.Cells[row, col].Value = "2"; //item.CustomerType
                    col++; ws.Cells[row, col].Value = ""; //item.CompanyRegistrationNo
                    col++; ws.Cells[row, col].Value = item.PolicyHolderTitle;
                    col++; ws.Cells[row, col].Value = item.PolicyHolderName;
                    col++; ws.Cells[row, col].Value = item.PolicyHolderSurname;
                    col++; ws.Cells[row, col].Value = item.PolicyHolderNationality;
                    col++; ws.Cells[row, col].Value = FormatExcelDate(item.PolicyHolderBirthday);
                    col++; ws.Cells[row, col].Value = item.PolicyHolderAge;
                    col++; ws.Cells[row, col].Value = FormatSex(item.PolicyHolderSex);
                    col++; ws.Cells[row, col].Value = item.PolicyHolderCardType;
                    col++; ws.Cells[row, col].Value = item.PolicyHolderIdCard;
                    col++; ws.Cells[row, col].Value = item.PolicyHolderEmail;
                    col++; ws.Cells[row, col].Value = item.PolicyHolderJob;
                    col++; ws.Cells[row, col].Value = item.PolicyFormat;
                    
                    col++; ws.Cells[row, col].Value = item.AddressNo;
                    col++; ws.Cells[row, col].Value = item.AddressMoo;
                    col++; ws.Cells[row, col].Value = item.AddressVillage;
                    col++; ws.Cells[row, col].Value = item.AddressFloor;
                    col++; ws.Cells[row, col].Value = item.AddressSoi;
                    col++; ws.Cells[row, col].Value = item.AddressRoad;
                    col++; ws.Cells[row, col].Value = item.AddressSubDistrict;
                    col++; ws.Cells[row, col].Value = item.AddressDistrict;
                    col++; ws.Cells[row, col].Value = item.AddressProvince;
                    col++; ws.Cells[row, col].Value = item.AddressPostalCode;

                    col++; ws.Cells[row, col].Value = item.PropertyAddressNo;
                    col++; ws.Cells[row, col].Value = item.PropertyAddressMoo;
                    col++; ws.Cells[row, col].Value = item.PropertyAddressVillage;
                    col++; ws.Cells[row, col].Value = item.PropertyAddressFloor;
                    col++; ws.Cells[row, col].Value = item.PropertyAddressSoi;
                    col++; ws.Cells[row, col].Value = item.PropertyAddressRoad;
                    col++; ws.Cells[row, col].Value = item.PropertyAddressSubDistrict;
                    col++; ws.Cells[row, col].Value = item.PropertyAddressDistrict;
                    col++; ws.Cells[row, col].Value = item.PropertyAddressProvince;
                    col++; ws.Cells[row, col].Value = item.PropertyAddressPostalCode;

                    col++; ws.Cells[row, col].Value = item.ShippingAddressNo;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressMoo;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressVillage;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressFloor;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressSoi;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressRoad;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressSubDistrict;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressDistrict;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressProvince;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressPostalCode;
                    col++; ws.Cells[row, col].Value = item.BillingAddressNo;
                    col++; ws.Cells[row, col].Value = item.BillingAddressMoo;
                    col++; ws.Cells[row, col].Value = item.BillingAddressVillage;
                    col++; ws.Cells[row, col].Value = item.BillingAddressFloor;
                    col++; ws.Cells[row, col].Value = item.BillingAddressSoi;
                    col++; ws.Cells[row, col].Value = item.BillingAddressRoad;
                    col++; ws.Cells[row, col].Value = item.BillingAddressSubDistrict;
                    col++; ws.Cells[row, col].Value = item.BillingAddressDistrict;
                    col++; ws.Cells[row, col].Value = item.BillingAddressProvince;
                    col++; ws.Cells[row, col].Value = item.BillingAddressPostalCode;

                    col++; ws.Cells[row, col].Value = item.BeneficiaryName;
                    col++; ws.Cells[row, col].Value = item.BeneficiarySurname;
                    col++; ws.Cells[row, col].Value = item.BeneficiaryIdType;
                    col++; ws.Cells[row, col].Value = item.BeneficiaryIdCard;
                    col++; ws.Cells[row, col].Value = item.BeneficiaryRelationship;
                    //col++; ws.Cells[row, col].Value = item.BeneficiaryTelephone;

                    col++; ws.Cells[row, col].Value = item.BuyerName;
                    col++; ws.Cells[row, col].Value = item.BuyerSurname;
                    //col++; ws.Cells[row, col].Value = item.ContactTel;
                    col++; ws.Cells[row, col].Value = item.BuyerEmail;

                    col++; ws.Cells[row, col].Value = item.ContactName;
                    col++; ws.Cells[row, col].Value = item.ContactSurname;
                    //col++; ws.Cells[row, col].Value = item.ContactTel;
                    col++; ws.Cells[row, col].Value = item.ContactEmail;

                    col++; ws.Cells[row, col].Value = item.PropertyType;
                    col++; ws.Cells[row, col].Value = item.UsageType;
                    col++; ws.Cells[row, col].Value = item.ConstructionType;
                    col++; ws.Cells[row, col].Value = item.Size;
                    col++; ws.Cells[row, col].Value = item.RoomAmount;
                    col++; ws.Cells[row, col].Value = item.Floor;
                    col++; ws.Cells[row, col].Value = item.OwnerStatus;
                    col++; ws.Cells[row, col].Value = item.ConstructionValue;
                    col++; ws.Cells[row, col].Value = item.FunitureValue;

                    col++; ws.Cells[row, col].Value = item.InsuranceCompanyCode;
                    col++; ws.Cells[row, col].Value = item.ProductCode;
                    col++; ws.Cells[row, col].Value = item.ProductName;
                    col++; ws.Cells[row, col].Value = item.SumInsured;
                    col++; ws.Cells[row, col].Value = item.NetPremium;
                    col++; ws.Cells[row, col].Value = item.Premium;

                    col++; ws.Cells[row, col].Value = FormatExcelDate(item.PolicyActiveDate);
                    col++; ws.Cells[row, col].Value = FormatExcelDate(item.PolicyExpireDate);
                    col++; ws.Cells[row, col].Value = item.TransactionBy;

                    if (row == 3) {
                        companyCode = item.InsuranceCompanyCode;
                        date = item.PaymentDate;
                    }

                    row++;
                }


                //Format the border
                using (ExcelRange rng = ws.Cells[1, 1, 1, maxColumn]) {
                    rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 192, 0));
                    rng.Style.WrapText = true;
                }
                using (ExcelRange rng = ws.Cells[2, 1, 2, maxColumn]) {
                    rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(248, 203, 173));
                    rng.Style.WrapText = true;
                }
                using (ExcelRange rng = ws.Cells[1, 1, (row - 1), maxColumn]) {
                    rng.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Top.Color.SetColor(Color.Black);
                    rng.Style.Border.Bottom.Color.SetColor(Color.Black);
                    rng.Style.Border.Left.Color.SetColor(Color.Black);
                    rng.Style.Border.Right.Color.SetColor(Color.Black);
                }

                if (setPassword) {
                    string password = GenerateExcelPassword(companyCode, DateTime.Now);// date);
                    pck.Encryption.Algorithm = EncryptionAlgorithm.AES128;
                    pck.Encryption.Version = EncryptionVersion.Standard;
                    pck.Workbook.Protection.LockStructure = true;
                    pck.Workbook.Protection.LockWindows = true;
                    ws.Protection.IsProtected = true;
                    pck.Encryption.Password = password;
                }
                return pck.GetAsByteArray();
            }
        }

        protected byte[] GenerateExcelProductPA(List<OrderApplicationProduct> order, bool setPassword = false) {
            string companyCode = "";
            DateTime date = DateTime.Now;
            using (ExcelPackage pck = new ExcelPackage()) {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Application");
                int row = 1;
                int col = 0;
                int maxColumn = 0;

                //TH
                col++; ws.Cells[row, col].Value = "หมายเลขคำสั่งซื้อ"; ws.Cells[row + 1, col].Value = "OrderId";
                col++; ws.Cells[row, col].Value = "เลขที่สัญญา"; ws.Cells[row + 1, col].Value = "CANo";
                col++; ws.Cells[row, col].Value = "ช่องทางการชำระเงิน"; ws.Cells[row + 1, col].Value = "PaymentChannel";
                col++; ws.Cells[row, col].Value = "รหัสธนาคาร"; ws.Cells[row + 1, col].Value = "PaymentBankCode";
                col++; ws.Cells[row, col].Value = "สถานะการชำระเงิน"; ws.Cells[row + 1, col].Value = "PaymentStatus";
                col++; ws.Cells[row, col].Value = "จำนวนเงินทั้งหมดที่ชำระ"; ws.Cells[row + 1, col].Value = "PaymentTotal";
                col++; ws.Cells[row, col].Value = "วันที่ชำระเงิน"; ws.Cells[row + 1, col].Value = "PaymentDate";
                col++; ws.Cells[row, col].Value = "วันที่ทำรายการ"; ws.Cells[row + 1, col].Value = "TransactionDateTime";
                col++; ws.Cells[row, col].Value = "ประเภทลูกค้า (1.นิติบุคคล, 2.บุคลธรรมดา)"; ws.Cells[row + 1, col].Value = "CustomerType";
                col++; ws.Cells[row, col].Value = "เลขที่นิติบุคคล"; ws.Cells[row + 1, col].Value = "CompanyRegistrationNo";
                col++; ws.Cells[row, col].Value = "คำนำหน้าชื่อผู้เอาประกันภัย"; ws.Cells[row + 1, col].Value = "Title";
                col++; ws.Cells[row, col].Value = "ชื่อ"; ws.Cells[row + 1, col].Value = "Name";
                col++; ws.Cells[row, col].Value = "นามสกุล"; ws.Cells[row + 1, col].Value = "Surname";
                col++; ws.Cells[row, col].Value = "สัญชาติ"; ws.Cells[row + 1, col].Value = "Nationality";
                col++; ws.Cells[row, col].Value = "วันเกิด"; ws.Cells[row + 1, col].Value = "Birthday";
                col++; ws.Cells[row, col].Value = "อายุ"; ws.Cells[row + 1, col].Value = "Age";
                //col++; ws.Cells[row, col].Value = "เพศ"; ws.Cells[row + 1, col].Value = "Sex (1=Male, 2=Female)";
                col++; ws.Cells[row, col].Value = "ประเภทของบัตร"; ws.Cells[row + 1, col].Value = "IDCardType (1=บัตรประชาชน, 2=Passport)";
                col++; ws.Cells[row, col].Value = "เลขที่บัตรประชาชน หรือ Passport"; ws.Cells[row + 1, col].Value = "IDCardNo";
                col++; ws.Cells[row, col].Value = "อีเมล์"; ws.Cells[row + 1, col].Value = "Email";
                //col++; ws.Cells[row, col].Value = "โทรศัพท์มือถือ"; ws.Cells[row + 1, col].Value = "MobilePhone";
                //col++; ws.Cells[row, col].Value = "โทรศัพท์"; ws.Cells[row + 1, col].Value = "Telephone";
                //col++; ws.Cells[row, col].Value = "Line Id"; ws.Cells[row + 1, col].Value = "LineId";
                col++; ws.Cells[row, col].Value = "อาชีพ"; ws.Cells[row + 1, col].Value = "Job";
                col++; ws.Cells[row, col].Value = "เงื่อนไขการรับกรมธรรม์"; ws.Cells[row + 1, col].Value = "PolicyFormat (E=E-policy, P=ส่งทาง ปณ.)";

                col++; ws.Cells[row, col].Value = "ที่อยู่ปัจจุบัน"; ws.Cells[row + 1, col].Value = "บ้านเลขที่";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "หมู่";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชื่อหมู่บ้าน/ชื่ออาคาร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชั้นอาคาร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ซอย";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ถนน";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "แขวง/ตำบล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เขต/อำเภอ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "จังหวัด";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "รหัสไปรษณีย์";
                ws.Cells[row, (col - 9), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ที่อยู่ส่งกรมธรรม์"; ws.Cells[row + 1, col].Value = "บ้านเลขที่";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "หมู่";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชื่อหมู่บ้าน/ชื่ออาคาร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชั้นอาคาร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ซอย";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ถนน";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "แขวง/ตำบล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เขต/อำเภอ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "จังหวัด";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "รหัสไปรษณีย์";
                ws.Cells[row, (col - 9), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ที่อยู่ออกใบเสร็จ"; ws.Cells[row + 1, col].Value = "บ้านเลขที่";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "หมู่";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชื่อหมู่บ้าน/ชื่ออาคาร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชั้นอาคาร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ซอย";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ถนน";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "แขวง/ตำบล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เขต/อำเภอ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "จังหวัด";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "รหัสไปรษณีย์";
                ws.Cells[row, (col - 9), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ข้อมูลผู้รับผลประโยขน์"; ws.Cells[row + 1, col].Value = "ชื่อ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "นามสกุล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ประเภทของบัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เลขที่บัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ความสัมพันธ์";
                //col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เบอร์ติดต่อ";
                ws.Cells[row, (col - 4), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ผู้ติดต่อกรณีฉุกเฉิน"; ws.Cells[row + 1, col].Value = "ชื่อ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "นามสกุล";
                //col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เบอร์ติดต่อ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "อีเมล";
                ws.Cells[row, (col - 2), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ชื่อผู้ขอเอาประกันภัย(กรณีซื้อแทนผู้เอาประกันภัย)"; ws.Cells[row + 1, col].Value = "ชื่อ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "นามสกุล";
                //col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เบอร์ติดต่อ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "อีเมล";
                ws.Cells[row, (col - 2), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ข้อมูลผู้เอาประกันภัยคนที่ 2"; ws.Cells[row + 1, col].Value = "ลำดับ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "คำนำหน้า";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชื่อ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "นามสกุล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "สัญชาติ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "วันเดือนปีเกิด";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "อายุ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ประเภทของบัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เลขที่บัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "อีเมล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "อาชีพ";
                ws.Cells[row, (col - 10), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ข้อมูลผู้รับผลประโยขน์ ผู้เอาประกันภัยคนที่ 2"; ws.Cells[row + 1, col].Value = "ชื่อ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "นามสกุล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ประเภทของบัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เลขที่บัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ความสัมพันธ์";
                //col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เบอร์ติดต่อ";
                ws.Cells[row, (col - 4), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ข้อมูลผู้เอาประกันภัยคนที่ 3"; ws.Cells[row + 1, col].Value = "ลำดับ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "คำนำหน้า";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชื่อ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "นามสกุล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "สัญชาติ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "วันเดือนปีเกิด";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "อายุ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ประเภทของบัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เลขที่บัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "อีเมล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "อาชีพ";
                ws.Cells[row, (col - 10), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ข้อมูลผู้รับผลประโยขน์ ผู้เอาประกันภัยคนที่ 3"; ws.Cells[row + 1, col].Value = "ชื่อ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "นามสกุล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ประเภทของบัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เลขที่บัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ความสัมพันธ์";
                //col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เบอร์ติดต่อ";
                ws.Cells[row, (col - 4), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ข้อมูลผู้เอาประกันภัยคนที่ 4"; ws.Cells[row + 1, col].Value = "ลำดับ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "คำนำหน้า";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชื่อ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "นามสกุล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "สัญชาติ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "วันเดือนปีเกิด";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "อายุ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ประเภทของบัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เลขที่บัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "อีเมล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "อาชีพ";
                ws.Cells[row, (col - 10), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ข้อมูลผู้รับผลประโยขน์ ผู้เอาประกันภัยคนที่ 4"; ws.Cells[row + 1, col].Value = "ชื่อ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "นามสกุล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ประเภทของบัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เลขที่บัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ความสัมพันธ์";
                //col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เบอร์ติดต่อ";
                ws.Cells[row, (col - 4), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "รหัสบริษัทประกัน"; ws.Cells[row + 1, col].Value = "InsuranceCompanyCode";
                col++; ws.Cells[row, col].Value = "รหัสของ Product"; ws.Cells[row + 1, col].Value = "ProductCode";
                col++; ws.Cells[row, col].Value = "ชื่อ Product"; ws.Cells[row + 1, col].Value = "ProductName";

                col++; ws.Cells[row, col].Value = "ทุนประกัน"; ws.Cells[row + 1, col].Value = "SumInsured";
                col++; ws.Cells[row, col].Value = "เบี้ยกรมธรรม์สุทธิ"; ws.Cells[row + 1, col].Value = "NetPremium";
                col++; ws.Cells[row, col].Value = "เบี้ยกรมธรรม์รวมภาษีอากร"; ws.Cells[row + 1, col].Value = "Premium";

                col++; ws.Cells[row, col].Value = "วันเที่ให้เริ่มคุ้มครอง"; ws.Cells[row + 1, col].Value = "PolicyActiveDate";
                col++; ws.Cells[row, col].Value = "วันที่สิ้นสุดคุ้มครอง"; ws.Cells[row + 1, col].Value = "PolicyExpireDate";

                col++; ws.Cells[row, col].Value = "ชื่อตัวแทน"; ws.Cells[row + 1, col].Value = "BrokerName";

                maxColumn = col;
                row++;
                row++;


                foreach (OrderApplicationProduct orderApp in order) {
                    PartnerApplicationPA item = GetPartnerApplicationPA(orderApp);
                    col = 0;
                    col++; ws.Cells[row, col].Value = item.OrderId;
                    col++; ws.Cells[row, col].Value = item.CANo;
                    col++; ws.Cells[row, col].Value = item.PaymentChannel;
                    col++; ws.Cells[row, col].Value = item.PaymentBankCode;
                    col++; ws.Cells[row, col].Value = "SUCCESS";
                    col++; ws.Cells[row, col].Value = item.PaymentTotal;
                    col++; ws.Cells[row, col].Value = FormatExcelDateTime(item.PaymentDate);
                    col++; ws.Cells[row, col].Value = FormatExcelDateTime(item.TransactionDate);
                    col++; ws.Cells[row, col].Value = "2"; //item.CustomerType
                    col++; ws.Cells[row, col].Value = ""; //item.CompanyRegistrationNo
                    col++; ws.Cells[row, col].Value = item.PolicyHolderTitle;
                    col++; ws.Cells[row, col].Value = item.PolicyHolderName;
                    col++; ws.Cells[row, col].Value = item.PolicyHolderSurname;
                    col++; ws.Cells[row, col].Value = item.PolicyHolderNationality;
                    col++; ws.Cells[row, col].Value = FormatExcelDate(item.PolicyHolderBirthday);
                    col++; ws.Cells[row, col].Value = item.PolicyHolderAge;
                    //col++; ws.Cells[row, col].Value = item.PolicyHolderSex;
                    col++; ws.Cells[row, col].Value = item.PolicyHolderCardType;
                    col++; ws.Cells[row, col].Value = item.PolicyHolderIdCard;
                    col++; ws.Cells[row, col].Value = item.PolicyHolderEmail;
                    col++; ws.Cells[row, col].Value = item.PolicyHolderJob;
                    col++; ws.Cells[row, col].Value = item.PolicyFormat;

                    col++; ws.Cells[row, col].Value = item.AddressNo;
                    col++; ws.Cells[row, col].Value = item.AddressMoo;
                    col++; ws.Cells[row, col].Value = item.AddressVillage;
                    col++; ws.Cells[row, col].Value = item.AddressFloor;
                    col++; ws.Cells[row, col].Value = item.AddressSoi;
                    col++; ws.Cells[row, col].Value = item.AddressRoad;
                    col++; ws.Cells[row, col].Value = item.AddressSubDistrict;
                    col++; ws.Cells[row, col].Value = item.AddressDistrict;
                    col++; ws.Cells[row, col].Value = item.AddressProvince;
                    col++; ws.Cells[row, col].Value = item.AddressPostalCode;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressNo;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressMoo;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressVillage;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressFloor;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressSoi;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressRoad;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressSubDistrict;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressDistrict;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressProvince;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressPostalCode;
                    col++; ws.Cells[row, col].Value = item.BillingAddressNo;
                    col++; ws.Cells[row, col].Value = item.BillingAddressMoo;
                    col++; ws.Cells[row, col].Value = item.BillingAddressVillage;
                    col++; ws.Cells[row, col].Value = item.BillingAddressFloor;
                    col++; ws.Cells[row, col].Value = item.BillingAddressSoi;
                    col++; ws.Cells[row, col].Value = item.BillingAddressRoad;
                    col++; ws.Cells[row, col].Value = item.BillingAddressSubDistrict;
                    col++; ws.Cells[row, col].Value = item.BillingAddressDistrict;
                    col++; ws.Cells[row, col].Value = item.BillingAddressProvince;
                    col++; ws.Cells[row, col].Value = item.BillingAddressPostalCode;

                    col++; ws.Cells[row, col].Value = item.BeneficiaryName;
                    col++; ws.Cells[row, col].Value = item.BeneficiarySurname;
                    col++; ws.Cells[row, col].Value = item.BeneficiaryIdType;
                    col++; ws.Cells[row, col].Value = item.BeneficiaryIdCard;
                    col++; ws.Cells[row, col].Value = item.BeneficiaryRelationship;
                    //col++; ws.Cells[row, col].Value = item.BeneficiaryTelephone;

                    col++; ws.Cells[row, col].Value = item.ContactName;
                    col++; ws.Cells[row, col].Value = item.ContactSurname;
                    //col++; ws.Cells[row, col].Value = item.ContactTel;
                    col++; ws.Cells[row, col].Value = item.ContactEmail;

                    col++; ws.Cells[row, col].Value = item.BuyerName;
                    col++; ws.Cells[row, col].Value = item.BuyerSurname;
                    //col++; ws.Cells[row, col].Value = item.BuyerTel;
                    col++; ws.Cells[row, col].Value = item.BuyerEmail;


                    for (int i = 2; i <= 4; i++) {
                        if (item.PolicyHolder == null || item.PolicyHolder.Count < i) {
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            //col++; ws.Cells[row, col].Value = "";
                        } else {
                            var p = item.PolicyHolder[i - 1];
                            col++; ws.Cells[row, col].Value = i.ToString();
                            col++; ws.Cells[row, col].Value = p.Title;
                            col++; ws.Cells[row, col].Value = p.Name;
                            col++; ws.Cells[row, col].Value = p.Surname;
                            col++; ws.Cells[row, col].Value = p.Nationality;
                            col++; ws.Cells[row, col].Value = FormatExcelDate(p.Birthday);
                            col++; ws.Cells[row, col].Value = p.Age;
                            col++; ws.Cells[row, col].Value = p.IDCardType;
                            col++; ws.Cells[row, col].Value = p.IDCardNo;
                            col++; ws.Cells[row, col].Value = p.Email;
                            col++; ws.Cells[row, col].Value = p.Job;
                            col++; ws.Cells[row, col].Value = p.BeneficiaryName;
                            col++; ws.Cells[row, col].Value = p.BeneficiarySurname;
                            col++; ws.Cells[row, col].Value = p.BeneficiaryIdType;
                            col++; ws.Cells[row, col].Value = p.BeneficiaryIdCard;
                            col++; ws.Cells[row, col].Value = p.BeneficiaryRelationship;
                            //col++; ws.Cells[row, col].Value = p.BeneficiaryTelephone;
                        }
                    }


                    col++; ws.Cells[row, col].Value = item.InsuranceCompanyCode;
                    col++; ws.Cells[row, col].Value = item.ProductCode;
                    col++; ws.Cells[row, col].Value = item.ProductName;
                    col++; ws.Cells[row, col].Value = item.SumInsured;
                    col++; ws.Cells[row, col].Value = item.NetPremium;
                    col++; ws.Cells[row, col].Value = item.Premium;
                    col++; ws.Cells[row, col].Value = FormatExcelDate(item.PolicyActiveDate);
                    col++; ws.Cells[row, col].Value = FormatExcelDate(item.PolicyExpireDate);
                    col++; ws.Cells[row, col].Value = item.TransactionBy;

                    if (row == 3) {
                        companyCode = item.InsuranceCompanyCode;
                        date = item.PaymentDate;
                    }

                    row++;
                }


                //Format the border
                using (ExcelRange rng = ws.Cells[1, 1, 1, maxColumn]) {
                    rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 192, 0));
                    rng.Style.WrapText = true;
                }
                using (ExcelRange rng = ws.Cells[2, 1, 2, maxColumn]) {
                    rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(248, 203, 173));
                    rng.Style.WrapText = true;
                }
                using (ExcelRange rng = ws.Cells[1, 1, (row - 1), maxColumn]) {
                    rng.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Top.Color.SetColor(Color.Black);
                    rng.Style.Border.Bottom.Color.SetColor(Color.Black);
                    rng.Style.Border.Left.Color.SetColor(Color.Black);
                    rng.Style.Border.Right.Color.SetColor(Color.Black);
                }

                if (setPassword) {
                    string password = GenerateExcelPassword(companyCode, DateTime.Now);// date);
                    pck.Encryption.Algorithm = EncryptionAlgorithm.AES128;
                    pck.Encryption.Version = EncryptionVersion.Standard;
                    pck.Workbook.Protection.LockStructure = true;
                    pck.Workbook.Protection.LockWindows = true;
                    ws.Protection.IsProtected = true;
                    pck.Encryption.Password = password;
                }
                return pck.GetAsByteArray();
            }
        }

        protected byte[] GenerateExcelProductPH(List<OrderApplicationProduct> order, bool setPassword = false) {
            string companyCode = "";
            DateTime date = DateTime.Now;
            using (ExcelPackage pck = new ExcelPackage()) {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Application");
                int row = 1;
                int col = 0;
                int maxColumn = 0;

                //TH
                col++; ws.Cells[row, col].Value = "หมายเลขคำสั่งซื้อ"; ws.Cells[row + 1, col].Value = "OrderId";
                col++; ws.Cells[row, col].Value = "เลขที่สัญญา"; ws.Cells[row + 1, col].Value = "CANo";
                col++; ws.Cells[row, col].Value = "ช่องทางการชำระเงิน"; ws.Cells[row + 1, col].Value = "PaymentChannel";
                col++; ws.Cells[row, col].Value = "รหัสธนาคาร"; ws.Cells[row + 1, col].Value = "PaymentBankCode";
                col++; ws.Cells[row, col].Value = "สถานะการชำระเงิน"; ws.Cells[row + 1, col].Value = "PaymentStatus";
                col++; ws.Cells[row, col].Value = "จำนวนเงินทั้งหมดที่ชำระ"; ws.Cells[row + 1, col].Value = "PaymentTotal";
                col++; ws.Cells[row, col].Value = "วันที่ชำระเงิน"; ws.Cells[row + 1, col].Value = "PaymentDate";
                col++; ws.Cells[row, col].Value = "วันที่ทำรายการ"; ws.Cells[row + 1, col].Value = "TransactionDateTime";
                col++; ws.Cells[row, col].Value = "ประเภทลูกค้า (1.นิติบุคคล, 2.บุคลธรรมดา)"; ws.Cells[row + 1, col].Value = "CustomerType";
                col++; ws.Cells[row, col].Value = "เลขที่นิติบุคคล"; ws.Cells[row + 1, col].Value = "CompanyRegistrationNo";
                col++; ws.Cells[row, col].Value = "คำนำหน้าชื่อผู้เอาประกันภัย"; ws.Cells[row + 1, col].Value = "Title";
                col++; ws.Cells[row, col].Value = "ชื่อ"; ws.Cells[row + 1, col].Value = "Name";
                col++; ws.Cells[row, col].Value = "นามสกุล"; ws.Cells[row + 1, col].Value = "Surname";
                col++; ws.Cells[row, col].Value = "สัญชาติ"; ws.Cells[row + 1, col].Value = "Nationality";
                col++; ws.Cells[row, col].Value = "วันเกิด"; ws.Cells[row + 1, col].Value = "Birthday";
                col++; ws.Cells[row, col].Value = "อายุ"; ws.Cells[row + 1, col].Value = "Age";
                col++; ws.Cells[row, col].Value = "เพศ"; ws.Cells[row + 1, col].Value = "Sex (1=Male, 2=Female)";
                col++; ws.Cells[row, col].Value = "สถานะภาพ"; ws.Cells[row + 1, col].Value = "MaritalStatus (1=โสด, 2=สมรส, 3=หย่าร้าง, 4=ม่าย)";
                col++; ws.Cells[row, col].Value = "ความสูง"; ws.Cells[row + 1, col].Value = "Height";
                col++; ws.Cells[row, col].Value = "น้ำหนัก"; ws.Cells[row + 1, col].Value = "Weight";
                col++; ws.Cells[row, col].Value = "ประเภทของบัตร"; ws.Cells[row + 1, col].Value = "IDCardType (1=บัตรประชาชน, 2=Passport)";
                col++; ws.Cells[row, col].Value = "เลขที่บัตรประชาชน หรือ Passport"; ws.Cells[row + 1, col].Value = "IDCardNo";
                col++; ws.Cells[row, col].Value = "อีเมล์"; ws.Cells[row + 1, col].Value = "Email";
                //col++; ws.Cells[row, col].Value = "โทรศัพท์มือถือ"; ws.Cells[row + 1, col].Value = "MobilePhone";
                //col++; ws.Cells[row, col].Value = "โทรศัพท์"; ws.Cells[row + 1, col].Value = "Telephone";
                //col++; ws.Cells[row, col].Value = "Line Id"; ws.Cells[row + 1, col].Value = "LineId";
                col++; ws.Cells[row, col].Value = "อาชีพ"; ws.Cells[row + 1, col].Value = "Job";
                col++; ws.Cells[row, col].Value = "เงื่อนไขการรับกรมธรรม์"; ws.Cells[row + 1, col].Value = "PolicyFormat (E=E-policy, P=ส่งทาง ปณ.)";

                col++; ws.Cells[row, col].Value = "ที่อยู่ปัจจุบัน"; ws.Cells[row + 1, col].Value = "บ้านเลขที่";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "หมู่";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชื่อหมู่บ้าน/ชื่ออาคาร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชั้นอาคาร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ซอย";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ถนน";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "แขวง/ตำบล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เขต/อำเภอ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "จังหวัด";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "รหัสไปรษณีย์";
                ws.Cells[row, (col - 9), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ที่อยู่ส่งกรมธรรม์"; ws.Cells[row + 1, col].Value = "บ้านเลขที่";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "หมู่";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชื่อหมู่บ้าน/ชื่ออาคาร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชั้นอาคาร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ซอย";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ถนน";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "แขวง/ตำบล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เขต/อำเภอ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "จังหวัด";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "รหัสไปรษณีย์";
                ws.Cells[row, (col - 9), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ที่อยู่ออกใบเสร็จ"; ws.Cells[row + 1, col].Value = "บ้านเลขที่";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "หมู่";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชื่อหมู่บ้าน/ชื่ออาคาร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชั้นอาคาร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ซอย";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ถนน";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "แขวง/ตำบล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เขต/อำเภอ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "จังหวัด";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "รหัสไปรษณีย์";
                ws.Cells[row, (col - 9), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ข้อมูลผู้รับผลประโยขน์"; ws.Cells[row + 1, col].Value = "ชื่อ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "นามสกุล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ประเภทของบัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เลขที่บัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ความสัมพันธ์";
                //col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เบอร์ติดต่อ";
                ws.Cells[row, (col - 4), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ชื่อผู้ขอเอาประกันภัย(กรณีซื้อแทนผู้เอาประกันภัย)"; ws.Cells[row + 1, col].Value = "ชื่อ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "นามสกุล";
                //col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เบอร์ติดต่อ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "อีเมล";
                ws.Cells[row, (col - 2), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ผู้ติดต่อกรณีฉุกเฉิน"; ws.Cells[row + 1, col].Value = "ชื่อ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "นามสกุล";
                //col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เบอร์ติดต่อ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "อีเมล";
                ws.Cells[row, (col - 2), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ข้อมูลผู้เอาประกันภัยคนที่ 2"; ws.Cells[row + 1, col].Value = "ลำดับ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "คำนำหน้า";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชื่อ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "นามสกุล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "สัญชาติ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "วันเดือนปีเกิด";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "อายุ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ประเภทของบัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เลขที่บัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "อีเมล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "อาชีพ";
                ws.Cells[row, (col - 10), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ข้อมูลผู้รับผลประโยขน์ ผู้เอาประกันภัยคนที่ 2"; ws.Cells[row + 1, col].Value = "ชื่อ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "นามสกุล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ประเภทของบัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เลขที่บัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ความสัมพันธ์";
                //col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เบอร์ติดต่อ";
                ws.Cells[row, (col - 4), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ข้อมูลผู้เอาประกันภัยคนที่ 3"; ws.Cells[row + 1, col].Value = "ลำดับ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "คำนำหน้า";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชื่อ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "นามสกุล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "สัญชาติ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "วันเดือนปีเกิด";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "อายุ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ประเภทของบัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เลขที่บัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "อีเมล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "อาชีพ";
                ws.Cells[row, (col - 10), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ข้อมูลผู้รับผลประโยขน์ ผู้เอาประกันภัยคนที่ 3"; ws.Cells[row + 1, col].Value = "ชื่อ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "นามสกุล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ประเภทของบัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เลขที่บัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ความสัมพันธ์";
                //col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เบอร์ติดต่อ";
                ws.Cells[row, (col - 4), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ข้อมูลผู้เอาประกันภัยคนที่ 4"; ws.Cells[row + 1, col].Value = "ลำดับ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "คำนำหน้า";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชื่อ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "นามสกุล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "สัญชาติ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "วันเดือนปีเกิด";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "อายุ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ประเภทของบัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เลขที่บัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "อีเมล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "อาชีพ";
                ws.Cells[row, (col - 10), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ข้อมูลผู้รับผลประโยขน์ ผู้เอาประกันภัยคนที่ 4"; ws.Cells[row + 1, col].Value = "ชื่อ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "นามสกุล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ประเภทของบัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เลขที่บัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ความสัมพันธ์";
                //col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เบอร์ติดต่อ";
                ws.Cells[row, (col - 4), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "รหัสบริษัทประกัน"; ws.Cells[row + 1, col].Value = "InsuranceCompanyCode";
                col++; ws.Cells[row, col].Value = "รหัสของ Product"; ws.Cells[row + 1, col].Value = "ProductCode";
                col++; ws.Cells[row, col].Value = "ชื่อ Product"; ws.Cells[row + 1, col].Value = "ProductName";

                col++; ws.Cells[row, col].Value = "ทุนประกัน"; ws.Cells[row + 1, col].Value = "SumInsured";
                col++; ws.Cells[row, col].Value = "เบี้ยกรมธรรม์สุทธิ"; ws.Cells[row + 1, col].Value = "NetPremium";
                col++; ws.Cells[row, col].Value = "เบี้ยกรมธรรม์รวมภาษีอากร"; ws.Cells[row + 1, col].Value = "Premium";

                col++; ws.Cells[row, col].Value = "วันเที่ให้เริ่มคุ้มครอง"; ws.Cells[row + 1, col].Value = "PolicyActiveDate";
                col++; ws.Cells[row, col].Value = "วันที่สิ้นสุดคุ้มครอง"; ws.Cells[row + 1, col].Value = "PolicyExpireDate";

                col++; ws.Cells[row, col].Value = "ชื่อตัวแทน"; ws.Cells[row + 1, col].Value = "BrokerName";

                maxColumn = col;
                row++;
                row++;


                foreach (OrderApplicationProduct orderApp in order) {
                    PartnerApplicationPH item = GetPartnerApplicationPH(orderApp);
                    col = 0;
                    col++; ws.Cells[row, col].Value = item.OrderId;
                    col++; ws.Cells[row, col].Value = item.CANo;
                    col++; ws.Cells[row, col].Value = item.PaymentChannel;
                    col++; ws.Cells[row, col].Value = item.PaymentBankCode;
                    col++; ws.Cells[row, col].Value = "SUCCESS";
                    col++; ws.Cells[row, col].Value = item.PaymentTotal;
                    col++; ws.Cells[row, col].Value = FormatExcelDateTime(item.PaymentDate);
                    col++; ws.Cells[row, col].Value = FormatExcelDateTime(item.TransactionDate);
                    col++; ws.Cells[row, col].Value = "2"; //item.CustomerType
                    col++; ws.Cells[row, col].Value = ""; //item.CompanyRegistrationNo
                    col++; ws.Cells[row, col].Value = item.PolicyHolderTitle;
                    col++; ws.Cells[row, col].Value = item.PolicyHolderName;
                    col++; ws.Cells[row, col].Value = item.PolicyHolderSurname;
                    col++; ws.Cells[row, col].Value = item.PolicyHolderNationality;
                    col++; ws.Cells[row, col].Value = FormatExcelDate(item.PolicyHolderBirthday);
                    col++; ws.Cells[row, col].Value = item.PolicyHolderAge;
                    col++; ws.Cells[row, col].Value = FormatSex(item.PolicyHolderSex);
                    col++; ws.Cells[row, col].Value = FormatMaritalStatus(item.PolicyHolderMaritalStatus);
                    col++; ws.Cells[row, col].Value = item.Height;
                    col++; ws.Cells[row, col].Value = item.Weight;
                    col++; ws.Cells[row, col].Value = item.PolicyHolderCardType;
                    col++; ws.Cells[row, col].Value = item.PolicyHolderIdCard;
                    col++; ws.Cells[row, col].Value = item.PolicyHolderEmail;
                    col++; ws.Cells[row, col].Value = item.PolicyHolderJob;
                    col++; ws.Cells[row, col].Value = item.PolicyFormat;

                    col++; ws.Cells[row, col].Value = item.AddressNo;
                    col++; ws.Cells[row, col].Value = item.AddressMoo;
                    col++; ws.Cells[row, col].Value = item.AddressVillage;
                    col++; ws.Cells[row, col].Value = item.AddressFloor;
                    col++; ws.Cells[row, col].Value = item.AddressSoi;
                    col++; ws.Cells[row, col].Value = item.AddressRoad;
                    col++; ws.Cells[row, col].Value = item.AddressSubDistrict;
                    col++; ws.Cells[row, col].Value = item.AddressDistrict;
                    col++; ws.Cells[row, col].Value = item.AddressProvince;
                    col++; ws.Cells[row, col].Value = item.AddressPostalCode;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressNo;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressMoo;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressVillage;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressFloor;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressSoi;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressRoad;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressSubDistrict;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressDistrict;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressProvince;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressPostalCode;
                    col++; ws.Cells[row, col].Value = item.BillingAddressNo;
                    col++; ws.Cells[row, col].Value = item.BillingAddressMoo;
                    col++; ws.Cells[row, col].Value = item.BillingAddressVillage;
                    col++; ws.Cells[row, col].Value = item.BillingAddressFloor;
                    col++; ws.Cells[row, col].Value = item.BillingAddressSoi;
                    col++; ws.Cells[row, col].Value = item.BillingAddressRoad;
                    col++; ws.Cells[row, col].Value = item.BillingAddressSubDistrict;
                    col++; ws.Cells[row, col].Value = item.BillingAddressDistrict;
                    col++; ws.Cells[row, col].Value = item.BillingAddressProvince;
                    col++; ws.Cells[row, col].Value = item.BillingAddressPostalCode;

                    col++; ws.Cells[row, col].Value = item.BeneficiaryName;
                    col++; ws.Cells[row, col].Value = item.BeneficiarySurname;
                    col++; ws.Cells[row, col].Value = item.BeneficiaryIdType;
                    col++; ws.Cells[row, col].Value = item.BeneficiaryIdCard;
                    col++; ws.Cells[row, col].Value = item.BeneficiaryRelationship;
                    //col++; ws.Cells[row, col].Value = item.BeneficiaryTelephone;

                    col++; ws.Cells[row, col].Value = item.BuyerName;
                    col++; ws.Cells[row, col].Value = item.BuyerSurname;
                    //col++; ws.Cells[row, col].Value = item.ContactTel;
                    col++; ws.Cells[row, col].Value = item.BuyerEmail;

                    col++; ws.Cells[row, col].Value = item.ContactName;
                    col++; ws.Cells[row, col].Value = item.ContactSurname;
                    //col++; ws.Cells[row, col].Value = item.ContactTel;
                    col++; ws.Cells[row, col].Value = item.ContactEmail;

                    for (int i = 2; i <= 4; i++) {
                        if (item.PolicyHolder == null || item.PolicyHolder.Count < i) {
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            //col++; ws.Cells[row, col].Value = "";
                        } else {
                            var p = item.PolicyHolder[i - 1];
                            col++; ws.Cells[row, col].Value = i.ToString();
                            col++; ws.Cells[row, col].Value = p.Title;
                            col++; ws.Cells[row, col].Value = p.Name;
                            col++; ws.Cells[row, col].Value = p.Surname;
                            col++; ws.Cells[row, col].Value = p.Nationality;
                            col++; ws.Cells[row, col].Value = FormatExcelDate(p.Birthday);
                            col++; ws.Cells[row, col].Value = p.Age;
                            col++; ws.Cells[row, col].Value = p.IDCardType;
                            col++; ws.Cells[row, col].Value = p.IDCardNo;
                            col++; ws.Cells[row, col].Value = p.Email;
                            col++; ws.Cells[row, col].Value = p.Job;
                            col++; ws.Cells[row, col].Value = p.BeneficiaryName;
                            col++; ws.Cells[row, col].Value = p.BeneficiarySurname;
                            col++; ws.Cells[row, col].Value = p.BeneficiaryIdType;
                            col++; ws.Cells[row, col].Value = p.BeneficiaryIdCard;
                            col++; ws.Cells[row, col].Value = p.BeneficiaryRelationship;
                            //col++; ws.Cells[row, col].Value = p.BeneficiaryTelephone;
                        }
                    }
                    
                    col++; ws.Cells[row, col].Value = item.InsuranceCompanyCode;
                    col++; ws.Cells[row, col].Value = item.ProductCode;
                    col++; ws.Cells[row, col].Value = item.ProductName;
                    col++; ws.Cells[row, col].Value = item.SumInsured;
                    col++; ws.Cells[row, col].Value = item.NetPremium;
                    col++; ws.Cells[row, col].Value = item.Premium;
                    col++; ws.Cells[row, col].Value = FormatExcelDate(item.PolicyActiveDate);
                    col++; ws.Cells[row, col].Value = FormatExcelDate(item.PolicyExpireDate);
                    col++; ws.Cells[row, col].Value = item.TransactionBy;

                    if (row == 3) {
                        companyCode = item.InsuranceCompanyCode;
                        date = item.PaymentDate;
                    }

                    row++;
                }


                //Format the border
                using (ExcelRange rng = ws.Cells[1, 1, 1, maxColumn]) {
                    rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 192, 0));
                    rng.Style.WrapText = true;
                }
                using (ExcelRange rng = ws.Cells[2, 1, 2, maxColumn]) {
                    rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(248, 203, 173));
                    rng.Style.WrapText = true;
                }
                using (ExcelRange rng = ws.Cells[1, 1, (row - 1), maxColumn]) {
                    rng.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Top.Color.SetColor(Color.Black);
                    rng.Style.Border.Bottom.Color.SetColor(Color.Black);
                    rng.Style.Border.Left.Color.SetColor(Color.Black);
                    rng.Style.Border.Right.Color.SetColor(Color.Black);
                }

                if (setPassword) {
                    string password = GenerateExcelPassword(companyCode, DateTime.Now);// date);
                    pck.Encryption.Algorithm = EncryptionAlgorithm.AES128;
                    pck.Encryption.Version = EncryptionVersion.Standard;
                    pck.Workbook.Protection.LockStructure = true;
                    pck.Workbook.Protection.LockWindows = true;
                    ws.Protection.IsProtected = true;
                    pck.Encryption.Password = password;
                }
                return pck.GetAsByteArray();
            }
        }

        protected byte[] GenerateExcelProductTA(List<OrderApplicationProduct> order, bool setPassword = false) {
            string companyCode = "";
            DateTime date = DateTime.Now;
            using (ExcelPackage pck = new ExcelPackage()) {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Application");
                int row = 1;
                int col = 0;
                int maxColumn = 0;

                //TH
                col++; ws.Cells[row, col].Value = "หมายเลขคำสั่งซื้อ"; ws.Cells[row + 1, col].Value = "OrderId";
                col++; ws.Cells[row, col].Value = "เลขที่สัญญา"; ws.Cells[row + 1, col].Value = "CANo";
                col++; ws.Cells[row, col].Value = "ช่องทางการชำระเงิน"; ws.Cells[row + 1, col].Value = "PaymentChannel";
                col++; ws.Cells[row, col].Value = "รหัสธนาคาร"; ws.Cells[row + 1, col].Value = "PaymentBankCode";
                col++; ws.Cells[row, col].Value = "สถานะการชำระเงิน"; ws.Cells[row + 1, col].Value = "PaymentStatus";
                col++; ws.Cells[row, col].Value = "จำนวนเงินทั้งหมดที่ชำระ"; ws.Cells[row + 1, col].Value = "PaymentTotal";
                col++; ws.Cells[row, col].Value = "วันที่ชำระเงิน"; ws.Cells[row + 1, col].Value = "PaymentDate";
                col++; ws.Cells[row, col].Value = "วันที่ทำรายการ"; ws.Cells[row + 1, col].Value = "TransactionDateTime";
                col++; ws.Cells[row, col].Value = "ประเภทลูกค้า (1.นิติบุคคล, 2.บุคลธรรมดา)"; ws.Cells[row + 1, col].Value = "CustomerType";
                col++; ws.Cells[row, col].Value = "เลขที่นิติบุคคล"; ws.Cells[row + 1, col].Value = "CompanyRegistrationNo";
                col++; ws.Cells[row, col].Value = "คำนำหน้าชื่อผู้เอาประกันภัย"; ws.Cells[row + 1, col].Value = "Title";
                col++; ws.Cells[row, col].Value = "ชื่อ"; ws.Cells[row + 1, col].Value = "Name";
                col++; ws.Cells[row, col].Value = "นามสกุล"; ws.Cells[row + 1, col].Value = "Surname";
                col++; ws.Cells[row, col].Value = "สัญชาติ"; ws.Cells[row + 1, col].Value = "Nationality";
                col++; ws.Cells[row, col].Value = "วันเกิด"; ws.Cells[row + 1, col].Value = "Birthday";
                col++; ws.Cells[row, col].Value = "อายุ"; ws.Cells[row + 1, col].Value = "Age";
                col++; ws.Cells[row, col].Value = "เพศ"; ws.Cells[row + 1, col].Value = "Sex (1=Male, 2=Female)";
                col++; ws.Cells[row, col].Value = "สถานะภาพ"; ws.Cells[row + 1, col].Value = "MaritalStatus (1=โสด, 2=สมรส, 3=หย่าร้าง, 4=ม่าย)";
                col++; ws.Cells[row, col].Value = "ประเภทของบัตร"; ws.Cells[row + 1, col].Value = "IDCardType (1=บัตรประชาชน, 2=Passport)";
                col++; ws.Cells[row, col].Value = "เลขที่บัตรประชาชน หรือ Passport"; ws.Cells[row + 1, col].Value = "IDCardNo";
                col++; ws.Cells[row, col].Value = "อีเมล์"; ws.Cells[row + 1, col].Value = "Email";
                //col++; ws.Cells[row, col].Value = "โทรศัพท์มือถือ"; ws.Cells[row + 1, col].Value = "MobilePhone";
                //col++; ws.Cells[row, col].Value = "โทรศัพท์"; ws.Cells[row + 1, col].Value = "Telephone";
                //col++; ws.Cells[row, col].Value = "Line Id"; ws.Cells[row + 1, col].Value = "LineId";
                col++; ws.Cells[row, col].Value = "อาชีพ"; ws.Cells[row + 1, col].Value = "Job";
                col++; ws.Cells[row, col].Value = "เงื่อนไขการรับกรมธรรม์"; ws.Cells[row + 1, col].Value = "PolicyFormat (E=E-policy, P=ส่งทาง ปณ.)";

                col++; ws.Cells[row, col].Value = "ที่อยู่ปัจจุบัน"; ws.Cells[row + 1, col].Value = "บ้านเลขที่";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "หมู่";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชื่อหมู่บ้าน/ชื่ออาคาร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชั้นอาคาร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ซอย";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ถนน";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "แขวง/ตำบล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เขต/อำเภอ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "จังหวัด";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "รหัสไปรษณีย์";
                ws.Cells[row, (col - 9), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ที่อยู่ส่งกรมธรรม์"; ws.Cells[row + 1, col].Value = "บ้านเลขที่";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "หมู่";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชื่อหมู่บ้าน/ชื่ออาคาร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชั้นอาคาร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ซอย";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ถนน";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "แขวง/ตำบล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เขต/อำเภอ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "จังหวัด";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "รหัสไปรษณีย์";
                ws.Cells[row, (col - 9), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ที่อยู่ออกใบเสร็จ"; ws.Cells[row + 1, col].Value = "บ้านเลขที่";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "หมู่";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชื่อหมู่บ้าน/ชื่ออาคาร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชั้นอาคาร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ซอย";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ถนน";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "แขวง/ตำบล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เขต/อำเภอ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "จังหวัด";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "รหัสไปรษณีย์";
                ws.Cells[row, (col - 9), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ข้อมูลผู้รับผลประโยขน์"; ws.Cells[row + 1, col].Value = "ชื่อ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "นามสกุล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ประเภทของบัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เลขที่บัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ความสัมพันธ์";
                //col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เบอร์ติดต่อ";
                ws.Cells[row, (col - 4), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ชื่อผู้ขอเอาประกันภัย(กรณีซื้อแทนผู้เอาประกันภัย)"; ws.Cells[row + 1, col].Value = "ชื่อ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "นามสกุล";
                //col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เบอร์ติดต่อ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "อีเมล";
                ws.Cells[row, (col - 2), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ผู้ติดต่อกรณีฉุกเฉิน"; ws.Cells[row + 1, col].Value = "ชื่อ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "นามสกุล";
                //col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เบอร์ติดต่อ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "อีเมล";
                ws.Cells[row, (col - 2), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ข้อมูลผู้เอาประกันภัยคนที่ 2"; ws.Cells[row + 1, col].Value = "ลำดับ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "คำนำหน้า";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชื่อ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "นามสกุล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "สัญชาติ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "วันเดือนปีเกิด";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "อายุ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เพศ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "สถานะภาพ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ประเภทของบัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เลขที่บัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "อีเมล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "อาชีพ";
                ws.Cells[row, (col - 12), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ข้อมูลผู้รับผลประโยขน์ ผู้เอาประกันภัยคนที่ 2"; ws.Cells[row + 1, col].Value = "ชื่อ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "นามสกุล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ประเภทของบัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เลขที่บัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ความสัมพันธ์";
                //col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เบอร์ติดต่อ";
                ws.Cells[row, (col - 4), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ข้อมูลผู้เอาประกันภัยคนที่ 3"; ws.Cells[row + 1, col].Value = "ลำดับ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "คำนำหน้า";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชื่อ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "นามสกุล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "สัญชาติ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "วันเดือนปีเกิด";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "อายุ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เพศ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "สถานะภาพ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ประเภทของบัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เลขที่บัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "อีเมล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "อาชีพ";
                ws.Cells[row, (col - 12), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ข้อมูลผู้รับผลประโยขน์ ผู้เอาประกันภัยคนที่ 3"; ws.Cells[row + 1, col].Value = "ชื่อ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "นามสกุล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ประเภทของบัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เลขที่บัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ความสัมพันธ์";
                //col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เบอร์ติดต่อ";
                ws.Cells[row, (col - 4), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ข้อมูลผู้เอาประกันภัยคนที่ 4"; ws.Cells[row + 1, col].Value = "ลำดับ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "คำนำหน้า";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ชื่อ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "นามสกุล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "สัญชาติ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "วันเดือนปีเกิด";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "อายุ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เพศ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "สถานะภาพ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ประเภทของบัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เลขที่บัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "อีเมล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "อาชีพ";
                ws.Cells[row, (col - 12), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "ข้อมูลผู้รับผลประโยขน์ ผู้เอาประกันภัยคนที่ 4"; ws.Cells[row + 1, col].Value = "ชื่อ";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "นามสกุล";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ประเภทของบัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เลขที่บัตร";
                col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "ความสัมพันธ์";
                //col++; ws.Cells[row, col].Value = ""; ws.Cells[row + 1, col].Value = "เบอร์ติดต่อ";
                ws.Cells[row, (col - 4), row, col].Merge = true;

                col++; ws.Cells[row, col].Value = "รหัสบริษัทประกัน"; ws.Cells[row + 1, col].Value = "InsuranceCompanyCode";
                col++; ws.Cells[row, col].Value = "รหัสของ Product"; ws.Cells[row + 1, col].Value = "ProductCode";
                col++; ws.Cells[row, col].Value = "ชื่อ Product"; ws.Cells[row + 1, col].Value = "ProductName";

                col++; ws.Cells[row, col].Value = "ทุนประกัน"; ws.Cells[row + 1, col].Value = "SumInsured";
                col++; ws.Cells[row, col].Value = "เบี้ยกรมธรรม์สุทธิ"; ws.Cells[row + 1, col].Value = "NetPremium";
                col++; ws.Cells[row, col].Value = "เบี้ยกรมธรรม์รวมภาษีอากร"; ws.Cells[row + 1, col].Value = "Premium";

                col++; ws.Cells[row, col].Value = "ประเภทการประกันภัย"; ws.Cells[row + 1, col].Value = "CoverageOption";
                col++; ws.Cells[row, col].Value = "ประเทศ"; ws.Cells[row + 1, col].Value = "Country";
                col++; ws.Cells[row, col].Value = "จำนวนวัน"; ws.Cells[row + 1, col].Value = "TotalDay";
                //col++; ws.Cells[row, col].Value = "ประเภทการเดินทาง"; ws.Cells[row + 1, col].Value = "TripType";
                col++; ws.Cells[row, col].Value = "แบบประกัน"; ws.Cells[row + 1, col].Value = "CoverageType";
                col++; ws.Cells[row, col].Value = "จำนวนคน"; ws.Cells[row + 1, col].Value = "TotalPerson";

                col++; ws.Cells[row, col].Value = "วันเที่ให้เริ่มคุ้มครอง"; ws.Cells[row + 1, col].Value = "PolicyActiveDate";
                col++; ws.Cells[row, col].Value = "วันที่สิ้นสุดคุ้มครอง"; ws.Cells[row + 1, col].Value = "PolicyExpireDate";

                col++; ws.Cells[row, col].Value = "ชื่อตัวแทน"; ws.Cells[row + 1, col].Value = "BrokerName";
                
                maxColumn = col;
                row++;
                row++;

                foreach (OrderApplicationProduct orderApp in order) {
                    PartnerApplicationTA item = GetPartnerApplicationTA(orderApp);
                    col = 0;
                    col++; ws.Cells[row, col].Value = item.OrderId;
                    col++; ws.Cells[row, col].Value = item.CANo;
                    col++; ws.Cells[row, col].Value = item.PaymentChannel;
                    col++; ws.Cells[row, col].Value = item.PaymentBankCode;
                    col++; ws.Cells[row, col].Value = "SUCCESS";
                    col++; ws.Cells[row, col].Value = item.PaymentTotal;
                    col++; ws.Cells[row, col].Value = FormatExcelDateTime(item.PaymentDate);
                    col++; ws.Cells[row, col].Value = FormatExcelDateTime(item.TransactionDate);
                    col++; ws.Cells[row, col].Value = "2"; //item.CustomerType
                    col++; ws.Cells[row, col].Value = ""; //item.CompanyRegistrationNo
                    col++; ws.Cells[row, col].Value = item.PolicyHolderTitle;
                    col++; ws.Cells[row, col].Value = item.PolicyHolderName;
                    col++; ws.Cells[row, col].Value = item.PolicyHolderSurname;
                    col++; ws.Cells[row, col].Value = item.PolicyHolderNationality;
                    col++; ws.Cells[row, col].Value = FormatExcelDate(item.PolicyHolderBirthday);
                    col++; ws.Cells[row, col].Value = item.PolicyHolderAge;
                    col++; ws.Cells[row, col].Value = FormatSex(item.PolicyHolderSex);
                    col++; ws.Cells[row, col].Value = FormatMaritalStatus(item.PolicyHolderMaritalStatus);
                    col++; ws.Cells[row, col].Value = item.PolicyHolderCardType;
                    col++; ws.Cells[row, col].Value = item.PolicyHolderIdCard;
                    col++; ws.Cells[row, col].Value = item.PolicyHolderEmail;
                    col++; ws.Cells[row, col].Value = item.PolicyHolderJob;
                    col++; ws.Cells[row, col].Value = item.PolicyFormat;

                    col++; ws.Cells[row, col].Value = item.AddressNo;
                    col++; ws.Cells[row, col].Value = item.AddressMoo;
                    col++; ws.Cells[row, col].Value = item.AddressVillage;
                    col++; ws.Cells[row, col].Value = item.AddressFloor;
                    col++; ws.Cells[row, col].Value = item.AddressSoi;
                    col++; ws.Cells[row, col].Value = item.AddressRoad;
                    col++; ws.Cells[row, col].Value = item.AddressSubDistrict;
                    col++; ws.Cells[row, col].Value = item.AddressDistrict;
                    col++; ws.Cells[row, col].Value = item.AddressProvince;
                    col++; ws.Cells[row, col].Value = item.AddressPostalCode;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressNo;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressMoo;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressVillage;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressFloor;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressSoi;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressRoad;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressSubDistrict;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressDistrict;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressProvince;
                    col++; ws.Cells[row, col].Value = item.ShippingAddressPostalCode;
                    col++; ws.Cells[row, col].Value = item.BillingAddressNo;
                    col++; ws.Cells[row, col].Value = item.BillingAddressMoo;
                    col++; ws.Cells[row, col].Value = item.BillingAddressVillage;
                    col++; ws.Cells[row, col].Value = item.BillingAddressFloor;
                    col++; ws.Cells[row, col].Value = item.BillingAddressSoi;
                    col++; ws.Cells[row, col].Value = item.BillingAddressRoad;
                    col++; ws.Cells[row, col].Value = item.BillingAddressSubDistrict;
                    col++; ws.Cells[row, col].Value = item.BillingAddressDistrict;
                    col++; ws.Cells[row, col].Value = item.BillingAddressProvince;
                    col++; ws.Cells[row, col].Value = item.BillingAddressPostalCode;

                    col++; ws.Cells[row, col].Value = item.BeneficiaryName;
                    col++; ws.Cells[row, col].Value = item.BeneficiarySurname;
                    col++; ws.Cells[row, col].Value = item.BeneficiaryIdType;
                    col++; ws.Cells[row, col].Value = item.BeneficiaryIdCard;
                    col++; ws.Cells[row, col].Value = item.BeneficiaryRelationship;
                    //col++; ws.Cells[row, col].Value = item.BeneficiaryTelephone;

                    col++; ws.Cells[row, col].Value = item.BuyerName;
                    col++; ws.Cells[row, col].Value = item.BuyerSurname;
                    //col++; ws.Cells[row, col].Value = item.ContactTel;
                    col++; ws.Cells[row, col].Value = item.BuyerEmail;

                    col++; ws.Cells[row, col].Value = item.ContactName;
                    col++; ws.Cells[row, col].Value = item.ContactSurname;
                    //col++; ws.Cells[row, col].Value = item.ContactTel;
                    col++; ws.Cells[row, col].Value = item.ContactEmail;

                    for (int i = 2; i <= 4; i++) {
                        if (item.PolicyHolder == null || item.PolicyHolder.Count < i) {
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            col++; ws.Cells[row, col].Value = "";
                            //col++; ws.Cells[row, col].Value = "";
                        } else {
                            var p = item.PolicyHolder[i - 1];
                            col++; ws.Cells[row, col].Value = i.ToString();
                            col++; ws.Cells[row, col].Value = p.Title;
                            col++; ws.Cells[row, col].Value = p.Name;
                            col++; ws.Cells[row, col].Value = p.Surname;
                            col++; ws.Cells[row, col].Value = p.Nationality;
                            col++; ws.Cells[row, col].Value = FormatExcelDate(p.Birthday);
                            col++; ws.Cells[row, col].Value = p.Age;
                            col++; ws.Cells[row, col].Value = FormatSex(p.Sex);
                            col++; ws.Cells[row, col].Value = FormatMaritalStatus(p.MaritalStatus);
                            col++; ws.Cells[row, col].Value = p.IDCardType;
                            col++; ws.Cells[row, col].Value = p.IDCardNo;
                            col++; ws.Cells[row, col].Value = p.Email;
                            col++; ws.Cells[row, col].Value = p.Job;
                            col++; ws.Cells[row, col].Value = p.BeneficiaryName;
                            col++; ws.Cells[row, col].Value = p.BeneficiarySurname;
                            col++; ws.Cells[row, col].Value = p.BeneficiaryIdType;
                            col++; ws.Cells[row, col].Value = p.BeneficiaryIdCard;
                            col++; ws.Cells[row, col].Value = p.BeneficiaryRelationship;
                            //col++; ws.Cells[row, col].Value = p.BeneficiaryTelephone;
                        }
                    }
                    col++; ws.Cells[row, col].Value = item.InsuranceCompanyCode;
                    col++; ws.Cells[row, col].Value = item.ProductCode;
                    col++; ws.Cells[row, col].Value = item.ProductName;
                    col++; ws.Cells[row, col].Value = item.SumInsured;
                    col++; ws.Cells[row, col].Value = item.NetPremium;
                    col++; ws.Cells[row, col].Value = item.Premium;

                    //col++; ws.Cells[row, col].Value = item.CoverageOption;
                    col++; ws.Cells[row, col].Value = item.TripType;
                    col++; ws.Cells[row, col].Value = item.Country;
                    col++; ws.Cells[row, col].Value = item.TotalDay;
                    //col++; ws.Cells[row, col].Value = item.TripType;
                    col++; ws.Cells[row, col].Value = item.CoverageType;
                    col++; ws.Cells[row, col].Value = item.TotalPerson;

                    col++; ws.Cells[row, col].Value = FormatExcelDate(item.PolicyActiveDate);
                    col++; ws.Cells[row, col].Value = FormatExcelDate(item.PolicyExpireDate);
                    col++; ws.Cells[row, col].Value = item.TransactionBy;

                    if (row == 3) {
                        companyCode = item.InsuranceCompanyCode;
                        date = item.PaymentDate;
                    }

                    row++;
                }


                //Format the border
                using (ExcelRange rng = ws.Cells[1, 1, 1, maxColumn]) {
                    rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 192, 0));
                    rng.Style.WrapText = true;
                }
                using (ExcelRange rng = ws.Cells[2, 1, 2, maxColumn]) {
                    rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(248, 203, 173));
                    rng.Style.WrapText = true;
                }
                using (ExcelRange rng = ws.Cells[1, 1, (row - 1), maxColumn]) {
                    rng.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Top.Color.SetColor(Color.Black);
                    rng.Style.Border.Bottom.Color.SetColor(Color.Black);
                    rng.Style.Border.Left.Color.SetColor(Color.Black);
                    rng.Style.Border.Right.Color.SetColor(Color.Black);
                }

                if (setPassword) {
                    string password = GenerateExcelPassword(companyCode, DateTime.Now);// date);
                    pck.Encryption.Algorithm = EncryptionAlgorithm.AES128;
                    pck.Encryption.Version = EncryptionVersion.Standard;
                    pck.Workbook.Protection.LockStructure = true;
                    pck.Workbook.Protection.LockWindows = true;
                    ws.Protection.IsProtected = true;
                    pck.Encryption.Password = password;
                }
                return pck.GetAsByteArray();
            }
        }

        protected string FormatSex(String val) {
            if (string.Equals(val, Sex.Male.ToString())) return ((int)Sex.Male).ToString();
            if (string.Equals(val, Sex.Female.ToString())) return ((int)Sex.Female).ToString();
            return "";
        }

        protected string FormatMaritalStatus(String val) {
            //if (string.Equals(val, MaritalStatus.Single.ToString())) return ((int)MaritalStatus.Single).ToString();
            //if (string.Equals(val, MaritalStatus.Married.ToString())) return ((int)MaritalStatus.Single).ToString();
            //if (string.Equals(val, MaritalStatus.Divorced.ToString())) return ((int)MaritalStatus.Divorced).ToString();
            //if (string.Equals(val, MaritalStatus.Widowed.ToString())) return ((int)MaritalStatus.Widowed).ToString();
            return val;
        }

        protected string FormatExcelDate(DateTime val) {
            if (val == DateTime.MinValue) return "";
            return val.ToString("yyyy-MM-dd", new CultureInfo("en-GB"));
        }

        protected string FormatExcelDateTime(DateTime val) {
            if (val == DateTime.MinValue) return "";
            return val.ToString("yyyy-MM-dd HH:mm:ss", new CultureInfo("en-GB"));
        }

        protected string FormatExcelAddress(string no, string moo, string village, string soi, string road) {
            return no + " " + moo + " " + village + " " + soi + " " + road;
        }


        protected string GenerateExcelPassword(string companyCode, DateTime date) {
            return companyCode + date.ToString("ddMMyyyy", new CultureInfo("en-GB"));
        }
        
    }


    class CustomDateConverter : IsoDateTimeConverter {
        public CustomDateConverter() {
            base.DateTimeFormat = "yyyy-MM-dd";
            base.Culture = new CultureInfo("en-GB");
        }
    }

    class CustomDateTimeConverter : IsoDateTimeConverter {
        public CustomDateTimeConverter() {
            base.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";
            base.Culture = new CultureInfo("en-GB");
        }
    }

    public class PartnerAPIResponse {
        public string OrderId { get; set; }
        public string Success { get; set; }
        public string ResponseCode { get; set; }
        public string ResponseMessage { get; set; }
        public string ResponseDetail { get; set; }
        public string EPolicyNo { get; set; }
        public string EPolicyNoAdd { get; set; }
        public DateTime ResponseDateTime { get; set; }
        public bool IsSuccess { get { return string.Equals(Success, "Y"); } }
    }
}
