﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using TobJod.Models;

namespace TobJod.Service {
    public class MailService {
        public static ActionResultStatus SendMail(string fromEmail, string fromName, string toEmails, string subject, string body, string ccEmails, string bccEmails) {
            return SendMailAttachment(fromEmail, fromName, toEmails, subject, body, ccEmails, bccEmails, null);
        }

        public static ActionResultStatus SendMailAttachment(string fromEmail, string fromName, string toEmails, string subject, string body, string ccEmails, string bccEmails, List<Attachment> attachments) {
            ActionResultStatus result = new ActionResultStatus();
            try {
                MailMessage message = new MailMessage();
                message.From = new MailAddress(fromEmail, fromName);
                var emails = toEmails.Split(';');
                foreach (var mailTo in emails) {
                    if (!string.IsNullOrWhiteSpace(mailTo)) message.To.Add(mailTo.Trim());
                }
                if (!string.IsNullOrEmpty(ccEmails)) message.CC.Add(ccEmails);
                if (!string.IsNullOrEmpty(bccEmails)) message.Bcc.Add(bccEmails);
                message.IsBodyHtml = true;
                message.Subject = subject;
                message.Body = body;
                if (attachments != null) {
                    foreach (var attachment in attachments) {
                        message.Attachments.Add(attachment);
                    }
                }

                SmtpClient smtp = new SmtpClient();
                smtp.Send(message);
                result.Success = true;
            } catch (Exception ex) {
                result.Success = false;
                result.ErrorMessage = ex.ToString();
            }
            return result;
        }

    }
}