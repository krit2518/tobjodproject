﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading.Tasks;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Service {
    public class TBrokerAPIService : BaseAPIService {

        public TBrokerAPIService() {
            DisplayAsTitle = false;
        }

        public ActionResultStatus Motor_CheckDuplicatePolicy(string serialNo, string engineNo, string plateNo1, string plateNo2, string plateProvince, DateTime activeDate, bool IsCompulsory) {
            ActionResultStatus result = new ActionResultStatus() { Success = false };
            try {
                using (DataSet ds = new DataSet()) {
                    DataTable dt = ds.Tables.Add("table");
                    dt.Columns.Add("CAR_SN_BODY");
                    dt.Columns.Add("CAR_ENGINE_NO");
                    dt.Columns.Add("CAR_PLAT_NO_1");
                    dt.Columns.Add("CAR_PLAT_NO_2");
                    dt.Columns.Add("CAR_PLAT_PROVINCE_ID");
                    dt.Columns.Add("BGDATE");
                    dt.Columns.Add("SUBCLASS");

                    DataRow dr = dt.NewRow();
                    dr["CAR_SN_BODY"] = serialNo;
                    dr["CAR_ENGINE_NO"] = engineNo;
                    dr["CAR_PLAT_NO_1"] = plateNo1;
                    dr["CAR_PLAT_NO_2"] = plateNo2;
                    dr["CAR_PLAT_PROVINCE_ID"] = plateProvince;
                    dr["BGDATE"] = FormatDate(activeDate);
                    dr["SUBCLASS"] = (IsCompulsory ? "C" : "V");
                    dt.Rows.Add(dr);

                    TBrokerAPI.ServiceDigitalSoapClient api = new TBrokerAPI.ServiceDigitalSoapClient();
                    var inspector = new LogEndpointBehavior(System.Configuration.ConfigurationManager.AppSettings["WebService_Log_Path"], "CheckDuplicatePolicyMT", DateTime.Now);
                    api.Endpoint.EndpointBehaviors.Add(inspector);
                    var dsResult = api.CheckDuplicatePolicyMT(ds);

                    if (dsResult != null && dsResult.Tables.Count > 0 && dsResult.Tables[0].Rows.Count > 0) {
                        if (string.Equals(dsResult.Tables[0].Rows[0]["SALE_ALLOW"], "Y")) {
                            result.Success = true;
                        } else {
                            result.Success = false;
                            result.ErrorMessage = string.Equals(dsResult.Tables[0].Rows[0]["CAR_DUPLICATE"], "Y") ? "Car Duplicate" : "";
                        }
                    }
                }
            } catch (Exception ex) { result.Success = true; result.ErrorMessage = ex.ToString(); }
            return result;
        }

        public ActionResultStatus Motor_ReservePolicy(string serialNo, string engineNo, string plateNo1, string plateNo2, string plateProvince, DateTime expireDate) {
            ActionResultStatus result = new ActionResultStatus() { Success = false };
            try {
                using (DataSet ds = new DataSet()) {
                    DataTable dt = ds.Tables.Add("table");
                    dt.Columns.Add("CAR_SN_BODY");
                    dt.Columns.Add("CAR_ENGINE_NO");
                    dt.Columns.Add("CAR_PLAT_NO_1");
                    dt.Columns.Add("CAR_PLAT_NO_2");
                    dt.Columns.Add("CAR_PLAT_PROVINCE_ID");
                    dt.Columns.Add("EXPIRATION_DATE");

                    DataRow dr = dt.NewRow();
                    dr["CAR_SN_BODY"] = serialNo;
                    dr["CAR_ENGINE_NO"] = engineNo;
                    dr["CAR_PLAT_NO_1"] = plateNo1;
                    dr["CAR_PLAT_NO_2"] = plateNo2;
                    dr["CAR_PLAT_PROVINCE_ID"] = plateProvince;
                    dr["EXPIRATION_DATE"] = FormatDateTime(expireDate);
                    dt.Rows.Add(dr);

                    TBrokerAPI.ServiceDigitalSoapClient api = new TBrokerAPI.ServiceDigitalSoapClient();
                    var inspector = new LogEndpointBehavior(System.Configuration.ConfigurationManager.AppSettings["WebService_Log_Path"], "ReservePolicyMT", DateTime.Now);
                    api.Endpoint.EndpointBehaviors.Add(inspector);
                    var dsResult = api.ReservePolicyMT(ds);

                    if (dsResult != null && dsResult.Tables.Count > 0 && dsResult.Tables[0].Rows.Count > 0) {
                        if (string.Equals(dsResult.Tables[0].Rows[0]["RESERVE_SUCCEED"], "Y")) {
                            result.Success = true;
                        } else {
                            result.Success = false;
                            result.ErrorMessage = string.Equals(dsResult.Tables[0].Rows[0]["CAR _DUPLICATE"], "Y") ? "Car Duplicate" : "";
                        }
                    }
                }
            } catch (Exception ex) { result.Success = true; result.ErrorMessage = ex.ToString(); }
            return result;
        }

        public Dictionary<string, ActionResultStatus> SubmitPolicy(List<int> orderIdList) {
            ActionResultStatus result = new ActionResultStatus() { Success = false };
            Dictionary<string, ActionResultStatus> results = new Dictionary<string, ActionResultStatus>();
            List<string> done = new List<string>();
            try {
                OrderRepository orderRepository = new OrderRepository();
                ProductApplicationRepository appRepository = new ProductApplicationRepository();
                ProductRepository productRepository = new ProductRepository();

                //Motor
                using (DataSet ds = new DataSet()) {
                    DataTable dt = ds.Tables.Add("table");
                    dt.Columns.Add("ORDER_NO");
                    dt.Columns.Add("CA_NO");
                    dt.Columns.Add("CA_GROUP");
                    dt.Columns.Add("SALE_CHANNEL");
                    dt.Columns.Add("CARD_TYPE");
                    dt.Columns.Add("CUST_ID_CARD");
                    dt.Columns.Add("CUST_TITLE");
                    dt.Columns.Add("CUST_FNAME");
                    dt.Columns.Add("CUST_LNAME");
                    dt.Columns.Add("CUST_BIRTHDAY");
                    dt.Columns.Add("CUST_PHONE");
                    dt.Columns.Add("CUST_MOBILE");
                    dt.Columns.Add("CUST_EMAIL");
                    dt.Columns.Add("CUST_FACEBOOK");
                    dt.Columns.Add("CUST_INSTAGRAM");
                    dt.Columns.Add("CUST_LINEID");
                    dt.Columns.Add("CUST_OCCUPATION");
                    dt.Columns.Add("PROD_FLAG");
                    dt.Columns.Add("POLICY_LEVEL");
                    dt.Columns.Add("POLICY_CLASS");
                    dt.Columns.Add("POLICY_SUBCLASS");
                    dt.Columns.Add("POLICY_PACKAGE_CODE");
                    dt.Columns.Add("POLICY_INS_CODE");
                    dt.Columns.Add("POLICY_AMOUNT");
                    dt.Columns.Add("POLICY_NET_AMT");
                    dt.Columns.Add("POLICY_DISC_AMOUNT");
                    dt.Columns.Add("POLICY_DISC_PRECEN");
                    dt.Columns.Add("POLICY_REVSTAMP");
                    dt.Columns.Add("POLICY_VAT");
                    dt.Columns.Add("POLICY_TOTAL_AMOUNT");
                    dt.Columns.Add("POLICY_NO");
                    dt.Columns.Add("POLICY_BGDATE");
                    dt.Columns.Add("POLICY_ENDDATE");
                    dt.Columns.Add("ACT_LEVEL");
                    dt.Columns.Add("ACT_CLASS");
                    dt.Columns.Add("ACT_SUBCLASS");
                    dt.Columns.Add("ACT_PACKAGE_CODE");
                    dt.Columns.Add("ACT_INS_CODE");
                    dt.Columns.Add("ACT_AMOUNT");
                    dt.Columns.Add("ACT_NET_AMOUNT");
                    dt.Columns.Add("ACT_DISC_AMOUNT");
                    dt.Columns.Add("ACT_DISC_PERCENT");
                    dt.Columns.Add("ACT_REVSTAMP");
                    dt.Columns.Add("ACT_VAT");
                    dt.Columns.Add("ACT_TOTAL_AMOUNT");
                    dt.Columns.Add("ACT_POLICY_NO");
                    dt.Columns.Add("ACT_BGDATE");
                    dt.Columns.Add("ACT_ENDDATE");
                    dt.Columns.Add("DRIV1_STATUS");
                    dt.Columns.Add("DRIV1_CARD_TYPE");
                    dt.Columns.Add("DRIV1_ID_CARD");
                    dt.Columns.Add("DRIV1_FNAME");
                    dt.Columns.Add("DRIV1_LNAME");
                    dt.Columns.Add("DRIV1_BIRTHDAY");
                    dt.Columns.Add("DRIV2_STATUS");
                    dt.Columns.Add("DRIV2_CARD_TYPE");
                    dt.Columns.Add("DRIV2_ID_CARD");
                    dt.Columns.Add("DRIV2_FNAME");
                    dt.Columns.Add("DRIV2_LNAME");
                    dt.Columns.Add("DRIV2_BIRTHDAY");
                    dt.Columns.Add("CAR_BRAND_DESC");
                    dt.Columns.Add("CAR_MODEL_DESC");
                    dt.Columns.Add("CAR_ENGINE_CC");
                    dt.Columns.Add("CAR_YEAR");
                    dt.Columns.Add("CAR_STATUS");
                    dt.Columns.Add("CAR_PLAT_NO_1");
                    dt.Columns.Add("CAR_PLAT_NO_2");
                    dt.Columns.Add("CAR_PLAT_PROVINCE_ID");
                    dt.Columns.Add("CAR_TYPE");
                    dt.Columns.Add("CAR_SN_BODY");
                    dt.Columns.Add("CAR_SN_ENGINE");
                    dt.Columns.Add("CAR_REPAIR_TYPE");
                    dt.Columns.Add("NCB");
                    dt.Columns.Add("NCB_NET_AMOUNT");
                    dt.Columns.Add("SEND_INSURANCE_FLAG");
                    dt.Columns.Add("ADD_IDCARD_NO");
                    dt.Columns.Add("ADD_IDCARD_VILLAGE");
                    dt.Columns.Add("ADD_IDCARD_NO_MU");
                    dt.Columns.Add("ADD_IDCARD_ALLEY");
                    dt.Columns.Add("ADD_IDCARD_ROAD");
                    dt.Columns.Add("ADD_IDCARD_DISTRICT");
                    dt.Columns.Add("ADD_IDCARD_COUNTY");
                    dt.Columns.Add("ADD_IDCARD_PROVINCE");
                    dt.Columns.Add("ADD_IDCARD_ZIPCODE");
                    dt.Columns.Add("ADD_CURRENT_NO_ADD");
                    dt.Columns.Add("ADD_CURRENT_VILLAGE");
                    dt.Columns.Add("ADD_CURRENT_NO_MU");
                    dt.Columns.Add("ADD_CURRENT_ALLEY");
                    dt.Columns.Add("ADD_CURRENT_ROAD");
                    dt.Columns.Add("ADD_CURRENT_DISTRICT");
                    dt.Columns.Add("ADD_CURRENT_COUNTY");
                    dt.Columns.Add("ADD_CURRENT_PROVINCE");
                    dt.Columns.Add("ADD_CURRENT_ZIPCODE");
                    dt.Columns.Add("ADD_INVOICE_NAME");
                    dt.Columns.Add("ADD_INVOICE_NO_ADD");
                    dt.Columns.Add("ADD_INVOICE_VILLAGE");
                    dt.Columns.Add("ADD_INVOICE_NO_MU");
                    dt.Columns.Add("ADD_INVOICE_ALLEY");
                    dt.Columns.Add("ADD_INVOICE_ROAD");
                    dt.Columns.Add("ADD_INVOICE_DISTRICT");
                    dt.Columns.Add("ADD_INVOICE_AMPHUR");
                    dt.Columns.Add("ADD_INVOICE_PROVINCE");
                    dt.Columns.Add("ADD_INVOICE_ZIPCODE");
                    dt.Columns.Add("ADD_POLICY_NO");
                    dt.Columns.Add("ADD_POLICY_VILLAGE");
                    dt.Columns.Add("ADD_POLICY_NO_MU");
                    dt.Columns.Add("ADD_POLICY_ALLEY");
                    dt.Columns.Add("ADD_POLICY_ROAD");
                    dt.Columns.Add("ADD_POLICY_DISTRICT");
                    dt.Columns.Add("ADD_POLICY_COUNTY");
                    dt.Columns.Add("ADD_POLICY_PROVINCE");
                    dt.Columns.Add("ADD_POLICY_ZIPCODE");
                    dt.Columns.Add("PAY_TYPE");
                    dt.Columns.Add("PAY_INSTALLMENT");
                    dt.Columns.Add("PAY_BANK_CODE");
                    dt.Columns.Add("PAY_STATUS");
                    dt.Columns.Add("PAY_MAILING_TYPE");
                    dt.Columns.Add("PAY_POLICY_STATUS");
                    dt.Columns.Add("PAY_GATEWAY_CODE");
                    dt.Columns.Add("PAY_REF_NO");
                    dt.Columns.Add("PAY_PAID_DATE");

                    foreach (var orderId in orderIdList) {
                        Order order = orderRepository.GetOrder_Admin(orderId);
                        if (order == null) continue;
                        ProductApplication app = appRepository.GetProductApplicationForm(order.ProductApplicationId);
                        if (app == null) continue;
                        Product product = productRepository.GetProduct_Admin(app.ProductId);
                        if (product == null) continue;
                        if (product.CategoryId == ProductCategoryKey.Motor || product.CategoryId == ProductCategoryKey.PRB) {
                            switch (product.CategoryId) {
                                case ProductCategoryKey.Motor:
                                    product.ProductMotorPremium = new List<ProductMotorPremium>();
                                    product.ProductMotorPremium.Add(productRepository.GetProductMotorPremium_Admin(app.PremiumId.ToString()));
                                    break;
                            }


                            OrderApplicationProduct orderApp = OrderApplicationProduct.Map(order);
                            ApplicationProduct appProduct = ApplicationProduct.Map(app);
                            appProduct.Product = product;
                            orderApp.ApplicationProduct = new List<ApplicationProduct>() { appProduct };

                            switch (product.CategoryId) {
                                case ProductCategoryKey.Motor: dt.Rows.Add(CreateSubmitPolicyRowMotor(dt.NewRow(), orderApp)); break;
                                case ProductCategoryKey.PRB: dt.Rows.Add(CreateSubmitPolicyRowCompulsory(dt.NewRow(), orderApp)); break;
                            }
                        }
                    }


                    if (dt.Rows.Count > 0) {
                        TBrokerAPI.ServiceDigitalSoapClient api = new TBrokerAPI.ServiceDigitalSoapClient();
                        var inspector = new LogEndpointBehavior(System.Configuration.ConfigurationManager.AppSettings["WebService_Log_Path"], "SubmitPolicyMT", DateTime.Now);
                        api.Endpoint.EndpointBehaviors.Add(inspector);
                        var dsResult = api.SubmitPolicyMT(ds);

                        if (dsResult != null && dsResult.Tables.Count > 0 && dsResult.Tables[0].Rows.Count > 0) {
                            foreach (DataRow row in dsResult.Tables[0].Rows) {
                                //if (row.Table.Columns.Contains("SUBMIT_SUCCEED")) {
                                    if (string.Equals(row["SUBMIT_SUCCEED"], "Y")) {
                                        result.Success = true;
                                        results.Add(NullUtils.cvString(row["ORDER_NO"]), new ActionResultStatus() { Success = true, Data = NullUtils.cvString(row["CA_NO"]) });
                                    } else {
                                        result.Success = false;
                                        result.ErrorMessage = NullUtils.cvString(row["N_REASON"]);
                                        results.Add(NullUtils.cvString(row["ORDER_NO"]), new ActionResultStatus() { Success = false, ErrorMessage = NullUtils.cvString(row["N_REASON"]) });
                                    }
                                //} else if (row.Table.Columns.Contains("Error")) {
                                //    result.Success = false;
                                //    result.ErrorMessage = NullUtils.cvString(row["Error"]);
                                //    results.Add(NullUtils.cvString(row["ORDER_NO"]), new ActionResultStatus() { Success = false, ErrorMessage = NullUtils.cvString(row["N_REASON"]) });
                                //} else {
                                //    result.Success = false;
                                //    results.Add(NullUtils.cvString(row["ORDER_NO"]), new ActionResultStatus() { Success = false, ErrorMessage = NullUtils.cvString(row["N_REASON"]) });
                                //}
                                done.Add(NullUtils.cvString(row["ORDER_NO"]));
                            }
                        } else {
                            results.Add("", new ActionResultStatus() { Success = false, ErrorMessage = "No Response" });
                        }
                    }
                }

                if (done.Count < orderIdList.Count) {
                    //Non-Motor
                    using (DataSet ds = new DataSet()) {
                        DataTable dt = ds.Tables.Add("table");
                        dt.Columns.Add("ORDER_NO");
                        dt.Columns.Add("CA_NO");
                        dt.Columns.Add("CA_GROUP");
                        dt.Columns.Add("CHANNEL");
                        dt.Columns.Add("APP_DATE");
                        dt.Columns.Add("CARD_TYPE");
                        dt.Columns.Add("CUST_ID_CARD");
                        dt.Columns.Add("CUST_TITLE");
                        dt.Columns.Add("CUST_FNAME");
                        dt.Columns.Add("CUST_LNAME");
                        dt.Columns.Add("CUST_BIRTHDAY");
                        dt.Columns.Add("CUST_PHONE");
                        dt.Columns.Add("CUST_MOBILE");
                        dt.Columns.Add("CUST_EMAIL");
                        dt.Columns.Add("CUST_FACEBOOK");
                        dt.Columns.Add("CUST_INSTAGRAM");
                        dt.Columns.Add("CUST_LINEID");
                        dt.Columns.Add("CUST_OCCUPATION");
                        dt.Columns.Add("CUST_TYPE");
                        dt.Columns.Add("DEPARTMENT_NAME");
                        dt.Columns.Add("OWNER_NAME");
                        dt.Columns.Add("CUST_AGE");
                        dt.Columns.Add("BENEFIT_NM");
                        dt.Columns.Add("RELATION_BENEFIT");
                        dt.Columns.Add("INS_CODE");
                        dt.Columns.Add("INS_NM");
                        dt.Columns.Add("OUTLET");
                        dt.Columns.Add("HUB");
                        dt.Columns.Add("AGENT_CODE");
                        dt.Columns.Add("AGENT_NAME");
                        dt.Columns.Add("GROUPHEAD");
                        dt.Columns.Add("POLICY_TYPE");
                        dt.Columns.Add("INDIVIDUAL_TYPE");
                        dt.Columns.Add("INS_CUST_NUM");
                        dt.Columns.Add("INS_DETAIL");
                        dt.Columns.Add("COUNTRY");
                        dt.Columns.Add("MIN_AGE");
                        dt.Columns.Add("MAX_AGE");
                        dt.Columns.Add("PLAN");
                        dt.Columns.Add("CLASS");
                        dt.Columns.Add("SUBCLASS");
                        dt.Columns.Add("POLICY_NO_OLD");
                        dt.Columns.Add("EFF_DATE");
                        dt.Columns.Add("EXP_DATE");
                        dt.Columns.Add("SUMINSURE");
                        dt.Columns.Add("PREMIUM");
                        dt.Columns.Add("DUTY");
                        dt.Columns.Add("TAX");
                        dt.Columns.Add("NETPREMIUM");
                        dt.Columns.Add("PAY_CODE");
                        dt.Columns.Add("INS_NM_PAY");
                        dt.Columns.Add("SEND_CODE");
                        dt.Columns.Add("CIF");
                        dt.Columns.Add("AO_CODE");
                        dt.Columns.Add("OFF_NAME");
                        dt.Columns.Add("DEP_CODE");
                        dt.Columns.Add("DEPT_NM");
                        dt.Columns.Add("MATCH_CHANNEL");
                        dt.Columns.Add("RM");
                        dt.Columns.Add("PACKAGE_CODE");
                        dt.Columns.Add("RISK_EXP_CODE");
                        dt.Columns.Add("BUILD_CLASS_CODE");
                        //dt.Columns.Add("LOCATION_ADDR");
                        dt.Columns.Add("LOCATION_NO");
                        dt.Columns.Add("LOCATION_VILLAGE");
                        dt.Columns.Add("LOCATION_MU");
                        dt.Columns.Add("LOCATION_SOI");
                        dt.Columns.Add("LOCATION_ROAD");
                        dt.Columns.Add("LOCATION_DISTRICT");
                        dt.Columns.Add("LOCATION_AMPHUR");
                        dt.Columns.Add("LOCATION_PROVINCE");
                        dt.Columns.Add("LOCATION_ZIPCODE");
                        dt.Columns.Add("RATE");
                        dt.Columns.Add("FIRE_DISCRATE");
                        dt.Columns.Add("CODEPAY_DESCRIPTION");
                        //dt.Columns.Add("SHIPPING_ADDR");
                        dt.Columns.Add("SHIPPING_FLAG");
                        dt.Columns.Add("SHIPPING_NAME");
                        dt.Columns.Add("SHIPPING_NO");
                        dt.Columns.Add("SHIPPING_VILLAGE");
                        dt.Columns.Add("SHIPPING_MU");
                        dt.Columns.Add("SHIPPING_SOI");
                        dt.Columns.Add("SHIPPING_ROAD");
                        dt.Columns.Add("SHIPPING_DISTRICT");
                        dt.Columns.Add("SHIPPING_AUMPHUR");
                        dt.Columns.Add("SHIPPING_PROVINCE");
                        dt.Columns.Add("SHIPPING_ZIPCODE");
                        dt.Columns.Add("IB_FLAG");
                        dt.Columns.Add("LOAN_NO");
                        dt.Columns.Add("POLICY_NO");
                        dt.Columns.Add("ADD_INFO_CODE1");
                        dt.Columns.Add("ADD_INFO_VALUE1");
                        dt.Columns.Add("ADD_INFO_CODE2");
                        dt.Columns.Add("ADD_INFO_VALUE2");
                        dt.Columns.Add("ADD_INFO_CODE3");
                        dt.Columns.Add("ADD_INFO_VALUE3");
                        dt.Columns.Add("ADD_INFO_CODE4");
                        dt.Columns.Add("ADD_INFO_VALUE4");
                        dt.Columns.Add("ADD_INFO_CODE5");
                        dt.Columns.Add("ADD_INFO_VALUE5");
                        dt.Columns.Add("ADD_IDCARD_NO");
                        dt.Columns.Add("ADD_IDCARD_VILLAGE");
                        dt.Columns.Add("ADD_IDCARD_NO_MU");
                        dt.Columns.Add("ADD_IDCARD_ALLEY");
                        dt.Columns.Add("ADD_IDCARD_ROAD");
                        dt.Columns.Add("ADD_IDCARD_DISTRICT");
                        dt.Columns.Add("ADD_IDCARD_COUNTY");
                        dt.Columns.Add("ADD_IDCARD_PROVINCE");
                        dt.Columns.Add("ADD_IDCARD_ZIPCODE");
                        dt.Columns.Add("ADD_CURRENT_NO_ADD");
                        dt.Columns.Add("ADD_CURRENT_VILLAGE");
                        dt.Columns.Add("ADD_CURRENT_NO_MU");
                        dt.Columns.Add("ADD_CURRENT_ALLEY");
                        dt.Columns.Add("ADD_CURRENT_ROAD");
                        dt.Columns.Add("ADD_CURRENT_DISTRICT");
                        dt.Columns.Add("ADD_CURRENT_COUNTY");
                        dt.Columns.Add("ADD_CURRENT_PROVINCE");
                        dt.Columns.Add("ADD_CURRENT_ZIPCODE");
                        dt.Columns.Add("ADD_INVOICE_NAME");
                        dt.Columns.Add("ADD_INVOICE_NO_ADD");
                        dt.Columns.Add("ADD_INVOICE_VILLAGE");
                        dt.Columns.Add("ADD_INVOICE_NO_MU");
                        dt.Columns.Add("ADD_INVOICE_ALLEY");
                        dt.Columns.Add("ADD_INVOICE_ROAD");
                        dt.Columns.Add("ADD_INVOICE_DISTRICT");
                        dt.Columns.Add("ADD_INVOICE_AMPHUR");
                        dt.Columns.Add("ADD_INVOICE_PROVINCE");
                        dt.Columns.Add("ADD_INVOICE_ZIPCODE");
                        dt.Columns.Add("ADD_POLICY_NO");
                        dt.Columns.Add("ADD_POLICY_VILLAGE");
                        dt.Columns.Add("ADD_POLICY_NO_MU");
                        dt.Columns.Add("ADD_POLICY_ALLEY");
                        dt.Columns.Add("ADD_POLICY_ROAD");
                        dt.Columns.Add("ADD_POLICY_DISTRICT");
                        dt.Columns.Add("ADD_POLICY_COUNTY");
                        dt.Columns.Add("ADD_POLICY_PROVINCE");
                        dt.Columns.Add("ADD_POLICY_ZIPCODE");
                        dt.Columns.Add("TAX_IS_INSURED");
                        dt.Columns.Add("TAX_PERSON_TYPE");
                        dt.Columns.Add("TAX_OCCUPATION");
                        dt.Columns.Add("TAX_PERSON_CARD_TYPE");
                        dt.Columns.Add("TAX_PERSON_CARD_ID");
                        dt.Columns.Add("TAX_ID");
                        dt.Columns.Add("PAY_CREDIT_TERM_TYPE");
                        dt.Columns.Add("PAY_INSTALLMENT");
                        dt.Columns.Add("PAY_TYPE");
                        dt.Columns.Add("PAY_BANK_CODE");
                        dt.Columns.Add("PAY_STATUS");
                        dt.Columns.Add("PAY_MAILING_TYPE");
                        dt.Columns.Add("PAY_POLI_NO");
                        dt.Columns.Add("PAY_POLICY_STATUS");
                        dt.Columns.Add("PAY_GATEWAY_CODE");
                        dt.Columns.Add("PAY_REF_NO");
                        dt.Columns.Add("PAY_PAID_DATE");
                        dt.Columns.Add("AUTO_DOC_FLAG");
                        dt.Columns.Add("AUTO_RV_FLAG");
                        dt.Columns.Add("PAYIN_DATE");
                        dt.Columns.Add("TRANS_CODE");
                        dt.Columns.Add("AGREEMENT_PREMIUM");

                        foreach (var orderId in orderIdList) {
                            Order order = orderRepository.GetOrder_Admin(orderId);
                            if (order == null) continue;
                            ProductApplication app = appRepository.GetProductApplicationForm(order.ProductApplicationId);
                            if (app == null) continue;
                            Product product = productRepository.GetProduct_Admin(app.ProductId);
                            if (product == null) continue;
                            if (product.CategoryId == ProductCategoryKey.Home || product.CategoryId == ProductCategoryKey.PA || product.CategoryId == ProductCategoryKey.PH || product.CategoryId == ProductCategoryKey.TA) {
                                switch (product.CategoryId) {
                                    case ProductCategoryKey.Home:
                                        product.ProductHomePremium = new List<ProductHomePremium>();
                                        product.ProductHomePremium.Add(productRepository.GetProductHomePremium_Admin(app.PremiumId.ToString()));
                                        break;
                                    case ProductCategoryKey.PA:
                                        product.ProductPAPremium = new List<ProductPAPremium>();
                                        product.ProductPAPremium.Add(productRepository.GetProductPAPremium_Admin(app.PremiumId.ToString()));
                                        break;
                                    case ProductCategoryKey.PH:
                                        product.ProductPHPremium = new List<ProductPHPremium>();
                                        product.ProductPHPremium.Add(productRepository.GetProductPHPremium_Admin(app.PremiumId.ToString()));
                                        break;
                                    case ProductCategoryKey.TA:
                                        product.ProductTAPremium = new List<ProductTAPremium>();
                                        product.ProductTAPremium.Add(productRepository.GetProductTAPremium_Admin(app.PremiumId.ToString()));
                                        break;
                                }


                                OrderApplicationProduct orderApp = OrderApplicationProduct.Map(order);
                                ApplicationProduct appProduct = ApplicationProduct.Map(app);
                                appProduct.Product = product;
                                orderApp.ApplicationProduct = new List<ApplicationProduct>() { appProduct };

                                switch (product.CategoryId) {
                                    case ProductCategoryKey.Home: dt.Rows.Add(CreateSubmitPolicyRowHome(dt, orderApp)); break;
                                    case ProductCategoryKey.PA: dt.Rows.Add(CreateSubmitPolicyRowPA(dt, orderApp)); break;
                                    case ProductCategoryKey.PH: dt.Rows.Add(CreateSubmitPolicyRowPH(dt, orderApp)); break;
                                    case ProductCategoryKey.TA: dt.Rows.Add(CreateSubmitPolicyRowTA(dt, orderApp)); break;
                                }
                            }
                        }


                        if (dt.Rows.Count > 0) {
                            TBrokerAPI.ServiceDigitalSoapClient api = new TBrokerAPI.ServiceDigitalSoapClient();
                            var inspector = new LogEndpointBehavior(System.Configuration.ConfigurationManager.AppSettings["WebService_Log_Path"], "SubmitPolicyMI", DateTime.Now);
                            api.Endpoint.EndpointBehaviors.Add(inspector);
                            var dsResult = api.SubmitPolicyMI(ds);

                            if (dsResult != null && dsResult.Tables.Count > 0 && dsResult.Tables[0].Rows.Count > 0) {
                                foreach (DataRow row in dsResult.Tables[0].Rows) {
                                    if (string.Equals(row["SUBMIT_SUCCEED"], "Y")) {
                                        result.Success = true;
                                        results.Add(NullUtils.cvString(row["ORDER_NO"]), new ActionResultStatus() { Success = true });
                                    } else {
                                        result.Success = false;
                                        result.ErrorMessage = NullUtils.cvString(row["N_REASON"]);
                                        results.Add(NullUtils.cvString(row["ORDER_NO"]), new ActionResultStatus() { Success = false, ErrorMessage = NullUtils.cvString(row["N_REASON"]) });
                                    }
                                    done.Add(NullUtils.cvString(row["ORDER_NO"]));
                                }
                            } else {
                                results.Add("", new ActionResultStatus() { Success = false, ErrorMessage = "No Response" });
                            }
                        }
                    }
                }
            } catch (Exception ex) { 
                results.Add("", new ActionResultStatus() { Success = false, ErrorMessage = ex.ToString() });
            }

            return results;
        }

        protected DataRow CreateSubmitPolicyRowMotor(DataRow dr, OrderApplicationProduct order) {
            PartnerApplicationMotor item = GetPartnerApplicationMotor(order);
            ApplicationProduct app = order.ApplicationProduct[0];
            ProductMotorPremium premium = app.Product.ProductMotorPremium[0];
            //DataRow dr = dt.NewRow();
            dr["ORDER_NO"] = order.OrderCode;
            dr["CA_NO"] = order.CANo;
            dr["CA_GROUP"] = "NEW";
            dr["SALE_CHANNEL"] = "01";
            dr["CARD_TYPE"] = "0" + item.PolicyHolderCardType;
            dr["CUST_ID_CARD"] = item.PolicyHolderIdCard;
            dr["CUST_TITLE"] = item.PolicyHolderTitle;
            dr["CUST_FNAME"] = item.PolicyHolderName;
            dr["CUST_LNAME"] = item.PolicyHolderSurname;
            dr["CUST_BIRTHDAY"] = FormatDate(item.PolicyHolderBirthday);
            dr["CUST_PHONE"] = item.PolicyHolderTelephone;
            dr["CUST_MOBILE"] = item.PolicyHolderMobilePhone;
            dr["CUST_EMAIL"] = item.PolicyHolderEmail;
            dr["CUST_FACEBOOK"] = item.PolicyHolderFacebook;
            dr["CUST_INSTAGRAM"] = item.PolicyHolderInstagram;
            dr["CUST_LINEID"] = item.PolicyHolderLineId;
            dr["CUST_OCCUPATION"] = item.PolicyHolderJob;
            dr["POLICY_LEVEL"] = item.ProductPackageCode;
            dr["POLICY_CLASS"] = "MO";
            dr["POLICY_SUBCLASS"] = "V";
            dr["POLICY_PACKAGE_CODE"] = app.Product.InsuranceProductCode;
            dr["POLICY_INS_CODE"] = app.Product.CompanyCode;
            dr["POLICY_AMOUNT"] = premium.SumInsured;
            dr["POLICY_NET_AMT"] = premium.NetPremium;
            dr["POLICY_DISC_AMOUNT"] = 0; //TODO: Discount
            dr["POLICY_DISC_PRECEN"] = 0; //TODO: Discount Percentage
            dr["POLICY_REVSTAMP"] = premium.Duty;
            dr["POLICY_VAT"] = premium.Vat;
            dr["POLICY_TOTAL_AMOUNT"] = premium.Premium;
            dr["POLICY_NO"] = order.PolicyNo;
            dr["POLICY_BGDATE"] = FormatDate(app.ActiveDate);
            dr["POLICY_ENDDATE"] = FormatDate(app.ExpireDate);
            if (item.BuyCompulsory) {
                dr["PROD_FLAG"] = "3";
                dr["ACT_CLASS"] = "MO";
                dr["ACT_SUBCLASS"] = "C";
                dr["ACT_PACKAGE_CODE"] = app.Product.InsuranceProductCode;
                dr["ACT_LEVEL"] = "C";
                dr["ACT_INS_CODE"] = app.Product.CompanyCode;
                dr["ACT_AMOUNT"] = 0;
                dr["ACT_NET_AMOUNT"] = item.CompulsoryNetPremium;
                dr["ACT_DISC_AMOUNT"] = 0;
                dr["ACT_DISC_PERCENT"] = 0;
                dr["ACT_REVSTAMP"] = item.CompulsoryDuty;
                dr["ACT_VAT"] = item.CompulsoryVat;
                dr["ACT_TOTAL_AMOUNT"] = item.CompulsoryPremium;
                dr["ACT_POLICY_NO"] = order.PolicyNoAdd;
                dr["ACT_BGDATE"] = FormatDate(item.CompulsoryActiveDate);
                dr["ACT_ENDDATE"] = FormatDate(item.CompulsoryExpireDate);
            } else {
                dr["PROD_FLAG"] = "1";
                dr["ACT_CLASS"] = "";
                dr["ACT_SUBCLASS"] = "";
                dr["ACT_PACKAGE_CODE"] = "";
                dr["ACT_LEVEL"] = "";
                dr["ACT_INS_CODE"] = "";
                dr["ACT_AMOUNT"] = 0;
                dr["ACT_NET_AMOUNT"] = 0;
                dr["ACT_DISC_AMOUNT"] = 0;
                dr["ACT_DISC_PERCENT"] = 0;
                dr["ACT_REVSTAMP"] = 0;
                dr["ACT_VAT"] = 0;
                dr["ACT_TOTAL_AMOUNT"] = 0;
                dr["ACT_POLICY_NO"] = "";
                dr["ACT_BGDATE"] = "";// FormatDateNull();
                dr["ACT_ENDDATE"] = "";//FormatDateNull();
            }
            if (item.Driver != null && item.Driver.Count > 0) {
                dr["DRIV1_STATUS"] = "Y";
                dr["DRIV1_CARD_TYPE"] = "0" + item.Driver[0].IDCardType;
                dr["DRIV1_ID_CARD"] = item.Driver[0].IDCardNo;
                dr["DRIV1_FNAME"] = item.Driver[0].Name;
                dr["DRIV1_LNAME"] = item.Driver[0].Surname;
                dr["DRIV1_BIRTHDAY"] = FormatDate(item.Driver[0].Birthday);
            } else {
                dr["DRIV1_STATUS"] = "N";
                dr["DRIV1_CARD_TYPE"] = "";
                dr["DRIV1_ID_CARD"] = "";
                dr["DRIV1_FNAME"] = "";
                dr["DRIV1_LNAME"] = "";
                dr["DRIV1_BIRTHDAY"] = FormatDateNull();
            }
            if (item.Driver != null && item.Driver.Count > 1) {
                dr["DRIV2_STATUS"] = "Y";
                dr["DRIV2_CARD_TYPE"] = "0" + item.Driver[1].IDCardType;
                dr["DRIV2_ID_CARD"] = item.Driver[1].IDCardNo;
                dr["DRIV2_FNAME"] = item.Driver[1].Name;
                dr["DRIV2_LNAME"] = item.Driver[1].Surname;
                dr["DRIV2_BIRTHDAY"] = FormatDate(item.Driver[1].Birthday);
            } else {
                dr["DRIV2_STATUS"] = "N";
                dr["DRIV2_CARD_TYPE"] = "";
                dr["DRIV2_ID_CARD"] = "";
                dr["DRIV2_FNAME"] = "";
                dr["DRIV2_LNAME"] = "";
                dr["DRIV2_BIRTHDAY"] = FormatDateNull();
            }
            dr["CAR_BRAND_DESC"] = item.CarBrand;
            dr["CAR_MODEL_DESC"] = item.CarModel;
            dr["CAR_ENGINE_CC"] = item.CarCC;
            dr["CAR_YEAR"] = item.CarRegisterYear;
            dr["CAR_STATUS"] = premium.CarInspector ? "Y" : "N";
            dr["CAR_PLAT_NO_1"] = item.CarPlateNo1;
            dr["CAR_PLAT_NO_2"] = item.CarPlateNo2;
            dr["CAR_PLAT_PROVINCE_ID"] = item.CarPlateProvince;
            dr["CAR_TYPE"] = FormatCarTypeTBroker(NullUtils.cvInt(item.VehicleUsage));
            dr["CAR_SN_BODY"] = item.CarSerialNo;
            dr["CAR_SN_ENGINE"] = item.CarEngineNo;
            dr["CAR_REPAIR_TYPE"] = (string.Equals(item.CarGarageType, "D") ? "01" : "02");
            dr["NCB"] = 0;
            dr["NCB_NET_AMOUNT"] = 0;
            dr["SEND_INSURANCE_FLAG"] = "N";
            dr["ADD_IDCARD_NO"] = "";
            dr["ADD_IDCARD_VILLAGE"] = "";
            dr["ADD_IDCARD_NO_MU"] = "";
            dr["ADD_IDCARD_ALLEY"] = "";
            dr["ADD_IDCARD_ROAD"] = "";
            dr["ADD_IDCARD_DISTRICT"] = "";
            dr["ADD_IDCARD_COUNTY"] = "";
            dr["ADD_IDCARD_PROVINCE"] = "";
            dr["ADD_IDCARD_ZIPCODE"] = "";
            dr["ADD_CURRENT_NO_ADD"] = item.AddressNo;
            dr["ADD_CURRENT_VILLAGE"] = item.AddressVillage;
            dr["ADD_CURRENT_NO_MU"] = item.AddressMoo;
            dr["ADD_CURRENT_ALLEY"] = item.AddressSoi;
            dr["ADD_CURRENT_ROAD"] = item.AddressRoad;
            dr["ADD_CURRENT_DISTRICT"] = item.AddressSubDistrict;
            dr["ADD_CURRENT_COUNTY"] = item.AddressDistrict;
            dr["ADD_CURRENT_PROVINCE"] = item.AddressProvince;
            dr["ADD_CURRENT_ZIPCODE"] = item.AddressPostalCode;
            dr["ADD_INVOICE_NAME"] = item.PolicyHolderName + " " + item.PolicyHolderSurname; //TODO: Check
            dr["ADD_INVOICE_NO_ADD"] = item.BillingAddressNo;
            dr["ADD_INVOICE_VILLAGE"] = item.BillingAddressVillage;
            dr["ADD_INVOICE_NO_MU"] = item.BillingAddressMoo;
            dr["ADD_INVOICE_ALLEY"] = item.BillingAddressSoi;
            dr["ADD_INVOICE_ROAD"] = item.BillingAddressRoad;
            dr["ADD_INVOICE_DISTRICT"] = item.BillingAddressSubDistrict;
            dr["ADD_INVOICE_AMPHUR"] = item.BillingAddressDistrict;
            dr["ADD_INVOICE_PROVINCE"] = item.BillingAddressProvince;
            dr["ADD_INVOICE_ZIPCODE"] = item.BillingAddressPostalCode;
            dr["ADD_POLICY_NO"] = item.ShippingAddressNo;
            dr["ADD_POLICY_VILLAGE"] = item.ShippingAddressVillage;
            dr["ADD_POLICY_NO_MU"] = item.ShippingAddressMoo;
            dr["ADD_POLICY_ALLEY"] = item.ShippingAddressSoi;
            dr["ADD_POLICY_ROAD"] = item.ShippingAddressRoad;
            dr["ADD_POLICY_DISTRICT"] = item.ShippingAddressSubDistrict;
            dr["ADD_POLICY_COUNTY"] = item.ShippingAddressDistrict;
            dr["ADD_POLICY_PROVINCE"] = item.ShippingAddressProvince;
            dr["ADD_POLICY_ZIPCODE"] = item.ShippingAddressPostalCode;
            dr["PAY_TYPE"] = FormatPaymentChannel(order.PaymentChannel);
            dr["PAY_INSTALLMENT"] = order.PaymentInstallment;
            dr["PAY_BANK_CODE"] = NullUtils.cvString(item.PaymentBankCode);
            dr["PAY_STATUS"] = "Y";
            dr["PAY_MAILING_TYPE"] = FormatEPolicy(app.EPolicy);
            dr["PAY_POLICY_STATUS"] = "1";
            dr["PAY_GATEWAY_CODE"] = "2C2P";
            dr["PAY_REF_NO"] = item.PaymentReference;
            dr["PAY_PAID_DATE"] = FormatDate(item.PaymentDate);

            return dr;
        }

        protected DataRow CreateSubmitPolicyRowCompulsory(DataRow dr, OrderApplicationProduct order) {
            PartnerApplicationMotor item = GetPartnerApplicationPRB(order);
            ApplicationProduct app = order.ApplicationProduct[0];
            ProductPRB premium = app.Product.ProductPRB;
            //DataRow dr = dt.NewRow();
            dr["ORDER_NO"] = order.OrderCode;
            dr["CA_NO"] = order.CANo;
            dr["CA_GROUP"] = "NEW";
            dr["SALE_CHANNEL"] = "01";
            dr["CARD_TYPE"] = "0" + item.PolicyHolderCardType;
            dr["CUST_ID_CARD"] = item.PolicyHolderIdCard;
            dr["CUST_TITLE"] = item.PolicyHolderTitle;
            dr["CUST_FNAME"] = item.PolicyHolderName;
            dr["CUST_LNAME"] = item.PolicyHolderSurname;
            dr["CUST_BIRTHDAY"] = FormatDate(item.PolicyHolderBirthday);
            dr["CUST_PHONE"] = item.PolicyHolderTelephone;
            dr["CUST_MOBILE"] = item.PolicyHolderMobilePhone;
            dr["CUST_EMAIL"] = item.PolicyHolderEmail;
            dr["CUST_FACEBOOK"] = item.PolicyHolderFacebook;
            dr["CUST_INSTAGRAM"] = item.PolicyHolderInstagram;
            dr["CUST_LINEID"] = item.PolicyHolderLineId;
            dr["CUST_OCCUPATION"] = item.PolicyHolderJob;
            dr["POLICY_LEVEL"] = "C";
            dr["POLICY_CLASS"] = "MO";
            dr["POLICY_SUBCLASS"] = "C";
            dr["POLICY_PACKAGE_CODE"] = app.Product.InsuranceProductCode;
            dr["POLICY_INS_CODE"] = app.Product.CompanyCode;
            dr["POLICY_AMOUNT"] = 0;
            dr["POLICY_NET_AMT"] = 0;
            dr["POLICY_DISC_AMOUNT"] = 0; 
            dr["POLICY_DISC_PRECEN"] = 0; 
            dr["POLICY_REVSTAMP"] = 0;
            dr["POLICY_VAT"] = 0;
            dr["POLICY_TOTAL_AMOUNT"] = 0;
            dr["POLICY_NO"] = "";
            dr["POLICY_BGDATE"] = FormatDateNull();
            dr["POLICY_ENDDATE"] = FormatDateNull();
            dr["PROD_FLAG"] = "2";
            dr["ACT_LEVEL"] = "C";
            dr["ACT_CLASS"] = "MO";
            dr["ACT_SUBCLASS"] = "C";
            dr["ACT_PACKAGE_CODE"] = app.Product.InsuranceProductCode;
            dr["ACT_INS_CODE"] = item.InsuranceCompanyCode;
            dr["ACT_AMOUNT"] = 0;
            dr["ACT_NET_AMOUNT"] = premium.NetPremium;
            dr["ACT_DISC_AMOUNT"] = 0;
            dr["ACT_DISC_PERCENT"] = 0;
            dr["ACT_REVSTAMP"] = premium.Duty;
            dr["ACT_VAT"] = premium.Vat;
            dr["ACT_TOTAL_AMOUNT"] = premium.Premium;
            dr["ACT_POLICY_NO"] = order.PolicyNo;
            dr["ACT_BGDATE"] = FormatDate(app.ActiveDate);
            dr["ACT_ENDDATE"] = FormatDate(app.ExpireDate);
            if (item.Driver != null && item.Driver.Count > 0) {
                dr["DRIV1_STATUS"] = "Y";
                dr["DRIV1_CARD_TYPE"] = "0" + item.Driver[0].IDCardType;
                dr["DRIV1_ID_CARD"] = item.Driver[0].IDCardNo;
                dr["DRIV1_FNAME"] = item.Driver[0].Name;
                dr["DRIV1_LNAME"] = item.Driver[0].Surname;
                dr["DRIV1_BIRTHDAY"] = FormatDate(item.Driver[0].Birthday);
            } else {
                dr["DRIV1_STATUS"] = "N";
                dr["DRIV1_CARD_TYPE"] = "";
                dr["DRIV1_ID_CARD"] = "";
                dr["DRIV1_FNAME"] = "";
                dr["DRIV1_LNAME"] = "";
                dr["DRIV1_BIRTHDAY"] = "01/01/1790";
            }
            if (item.Driver != null && item.Driver.Count > 1) {
                dr["DRIV2_STATUS"] = "Y";
                dr["DRIV2_CARD_TYPE"] = "0" + item.Driver[1].IDCardType;
                dr["DRIV2_ID_CARD"] = item.Driver[1].IDCardNo;
                dr["DRIV2_FNAME"] = item.Driver[1].Name;
                dr["DRIV2_LNAME"] = item.Driver[1].Surname;
                dr["DRIV2_BIRTHDAY"] = FormatDate(item.Driver[1].Birthday);
            } else {
                dr["DRIV2_STATUS"] = "N";
                dr["DRIV2_CARD_TYPE"] = "";
                dr["DRIV2_ID_CARD"] = "";
                dr["DRIV2_FNAME"] = "";
                dr["DRIV2_LNAME"] = "";
                dr["DRIV2_BIRTHDAY"] = "01/01/1790";
            }
            dr["CAR_BRAND_DESC"] = item.CarBrand;
            dr["CAR_MODEL_DESC"] = item.CarModel;
            dr["CAR_ENGINE_CC"] = item.CarCC;
            dr["CAR_YEAR"] = item.CarRegisterYear;
            dr["CAR_STATUS"] = "N";
            dr["CAR_PLAT_NO_1"] = item.CarPlateNo1;
            dr["CAR_PLAT_NO_2"] = item.CarPlateNo2;
            dr["CAR_PLAT_PROVINCE_ID"] = item.CarPlateProvince;
            dr["CAR_TYPE"] = FormatCarTypeTBroker(NullUtils.cvInt(item.VehicleUsage));
            dr["CAR_SN_BODY"] = item.CarSerialNo;
            dr["CAR_SN_ENGINE"] = item.CarEngineNo;
            dr["CAR_REPAIR_TYPE"] = (string.Equals(item.CarGarageType, "D") ? "01" : "02");
            dr["NCB"] = 0;
            dr["NCB_NET_AMOUNT"] = 0;
            dr["SEND_INSURANCE_FLAG"] = "N";
            dr["ADD_IDCARD_NO"] = "";
            dr["ADD_IDCARD_VILLAGE"] = "";
            dr["ADD_IDCARD_NO_MU"] = "";
            dr["ADD_IDCARD_ALLEY"] = "";
            dr["ADD_IDCARD_ROAD"] = "";
            dr["ADD_IDCARD_DISTRICT"] = "";
            dr["ADD_IDCARD_COUNTY"] = "";
            dr["ADD_IDCARD_PROVINCE"] = "";
            dr["ADD_IDCARD_ZIPCODE"] = "";
            dr["ADD_CURRENT_NO_ADD"] = item.AddressNo;
            dr["ADD_CURRENT_VILLAGE"] = item.AddressVillage;
            dr["ADD_CURRENT_NO_MU"] = item.AddressMoo;
            dr["ADD_CURRENT_ALLEY"] = item.AddressSoi;
            dr["ADD_CURRENT_ROAD"] = item.AddressRoad;
            dr["ADD_CURRENT_DISTRICT"] = item.AddressSubDistrict;
            dr["ADD_CURRENT_COUNTY"] = item.AddressDistrict;
            dr["ADD_CURRENT_PROVINCE"] = item.AddressProvince;
            dr["ADD_CURRENT_ZIPCODE"] = item.AddressPostalCode;
            dr["ADD_INVOICE_NAME"] = item.PolicyHolderName + " " + item.PolicyHolderSurname; //TODO: Check
            dr["ADD_INVOICE_NO_ADD"] = item.BillingAddressNo;
            dr["ADD_INVOICE_VILLAGE"] = item.BillingAddressVillage;
            dr["ADD_INVOICE_NO_MU"] = item.BillingAddressMoo;
            dr["ADD_INVOICE_ALLEY"] = item.BillingAddressSoi;
            dr["ADD_INVOICE_ROAD"] = item.BillingAddressRoad;
            dr["ADD_INVOICE_DISTRICT"] = item.BillingAddressSubDistrict;
            dr["ADD_INVOICE_AMPHUR"] = item.BillingAddressDistrict;
            dr["ADD_INVOICE_PROVINCE"] = item.BillingAddressProvince;
            dr["ADD_INVOICE_ZIPCODE"] = item.BillingAddressPostalCode;
            dr["ADD_POLICY_NO"] = item.ShippingAddressNo;
            dr["ADD_POLICY_VILLAGE"] = item.ShippingAddressVillage;
            dr["ADD_POLICY_NO_MU"] = item.ShippingAddressMoo;
            dr["ADD_POLICY_ALLEY"] = item.ShippingAddressSoi;
            dr["ADD_POLICY_ROAD"] = item.ShippingAddressRoad;
            dr["ADD_POLICY_DISTRICT"] = item.ShippingAddressSubDistrict;
            dr["ADD_POLICY_COUNTY"] = item.ShippingAddressDistrict;
            dr["ADD_POLICY_PROVINCE"] = item.ShippingAddressProvince;
            dr["ADD_POLICY_ZIPCODE"] = item.ShippingAddressPostalCode;
            dr["PAY_TYPE"] = FormatPaymentChannel(order.PaymentChannel);
            dr["PAY_INSTALLMENT"] = order.PaymentInstallment;
            dr["PAY_BANK_CODE"] = NullUtils.cvString(item.PaymentBankCode);
            dr["PAY_STATUS"] = "Y";
            dr["PAY_MAILING_TYPE"] = FormatEPolicy(app.EPolicy);
            dr["PAY_POLICY_STATUS"] = "1";
            dr["PAY_GATEWAY_CODE"] = "2C2P";
            dr["PAY_REF_NO"] = item.PaymentReference;
            dr["PAY_PAID_DATE"] = FormatDate(item.PaymentDate);

            return dr;
        }

        protected DataRow CreateSubmitPolicyRowHome(DataTable dt, OrderApplicationProduct order) {
            PartnerApplicationHome item = GetPartnerApplicationHome(order);
            ApplicationProduct app = order.ApplicationProduct[0];
            ProductHomePremium premium = app.Product.ProductHomePremium[0];
            DataRow dr = dt.NewRow();
            dr["ORDER_NO"] = item.OrderId;
            dr["CA_NO"] = item.CANo;
            dr["CA_GROUP"] = "NEW";
            dr["CHANNEL"] = "ODGT";
            dr["APP_DATE"] = FormatDate(item.TransactionDate);
            dr["CARD_TYPE"] = "0" + item.PolicyHolderCardType;
            dr["CUST_ID_CARD"] = item.PolicyHolderIdCard;
            dr["CUST_TITLE"] = item.PolicyHolderTitle;
            dr["CUST_FNAME"] = item.PolicyHolderName;
            dr["CUST_LNAME"] = item.PolicyHolderSurname;
            dr["CUST_BIRTHDAY"] = FormatDate(item.PolicyHolderBirthday);
            dr["CUST_PHONE"] = item.PolicyHolderTelephone;
            dr["CUST_MOBILE"] = item.PolicyHolderMobilePhone;
            dr["CUST_EMAIL"] = item.PolicyHolderEmail;
            dr["CUST_FACEBOOK"] = item.PolicyHolderFacebook;
            dr["CUST_INSTAGRAM"] = item.PolicyHolderInstagram;
            dr["CUST_LINEID"] = item.PolicyHolderLineId;
            dr["CUST_OCCUPATION"] = item.PolicyHolderJob;
            dr["CUST_TYPE"] = "P";
            dr["DEPARTMENT_NAME"] = "MKT_Team";
            dr["OWNER_NAME"] = "DIGITAL";
            dr["CUST_AGE"] = item.PolicyHolderAge;
            dr["BENEFIT_NM"] = item.BeneficiaryName + " " + item.BeneficiarySurname;
            dr["RELATION_BENEFIT"] = item.BeneficiaryRelationship;
            dr["INS_CODE"] = GetCompanyCorporateCode(item.InsuranceCompanyCode);
            dr["INS_NM"] = item.InsuranceCompany;
            dr["OUTLET"] = "999";
            dr["HUB"] = "ธนชาตโบรกเกอร์";
            dr["AGENT_CODE"] = "999999";
            dr["AGENT_NAME"] = "";
            dr["GROUPHEAD"] = "";
            dr["POLICY_TYPE"] = "New";
            dr["INDIVIDUAL_TYPE"] = "";
            dr["INS_CUST_NUM"] = Math.Max(1, item.PolicyHolder.Count);
            dr["INS_DETAIL"] = "";
            dr["COUNTRY"] = "";
            dr["MIN_AGE"] = "";
            dr["MAX_AGE"] = "";
            dr["PLAN"] = premium.PlanCode;
            dr["CLASS"] = app.Product.Class;
            dr["SUBCLASS"] = app.Product.SubClass;
            dr["POLICY_NO_OLD"] = "";
            dr["EFF_DATE"] = FormatDate(item.PolicyActiveDate);
            dr["EXP_DATE"] = FormatDate(item.PolicyExpireDate);
            dr["SUMINSURE"] = premium.SumInsured;
            dr["PREMIUM"] = premium.Premium;
            dr["DUTY"] = premium.Duty;
            dr["TAX"] = premium.Vat;
            dr["NETPREMIUM"] = premium.NetPremium;
            dr["PAY_CODE"] = "C23";
            dr["INS_NM_PAY"] = "";
            dr["SEND_CODE"] = "B10";
            dr["CIF"] = "";
            dr["AO_CODE"] = "";
            dr["OFF_NAME"] = "";
            dr["DEP_CODE"] = "";
            dr["DEPT_NM"] = "";
            dr["MATCH_CHANNEL"] = "";
            dr["RM"] = "";
            dr["PACKAGE_CODE"] = app.Product.InsuranceProductCode;
            dr["RISK_EXP_CODE"] = "1032";
            dr["BUILD_CLASS_CODE"] = item.ConstructionType;
            //dr["LOCATION_ADDR"] = item.PropertyAddressNo;
            dr["LOCATION_NO"] = item.PropertyAddressNo;
            dr["LOCATION_VILLAGE"] = item.PropertyAddressVillage;
            dr["LOCATION_MU"] = item.PropertyAddressMoo;
            dr["LOCATION_SOI"] = item.PropertyAddressSoi;
            dr["LOCATION_ROAD"] = item.PropertyAddressRoad;
            dr["LOCATION_DISTRICT"] = item.PropertyAddressSubDistrict;
            dr["LOCATION_AMPHUR"] = item.PropertyAddressDistrict;
            dr["LOCATION_PROVINCE"] = item.PropertyAddressProvince;
            dr["LOCATION_ZIPCODE"] = item.PropertyAddressPostalCode;
            dr["RATE"] = "";
            dr["FIRE_DISCRATE"] = "";
            dr["CODEPAY_DESCRIPTION"] = "";
            //dr["SHIPPING_ADDR"] = "";
            if (!item.ShippingIsChecked) {
                dr["SHIPPING_FLAG"] = "N";
                dr["SHIPPING_NAME"] = item.PolicyHolderTitleText + " " + item.PolicyHolderName + " " + item.PolicyHolderSurname;
                dr["SHIPPING_NO"] = item.ShippingAddressNo;
                dr["SHIPPING_VILLAGE"] = item.ShippingAddressVillage;
                dr["SHIPPING_MU"] = item.ShippingAddressMoo;
                dr["SHIPPING_SOI"] = item.ShippingAddressSoi;
                dr["SHIPPING_ROAD"] = item.ShippingAddressRoad;
                dr["SHIPPING_DISTRICT"] = item.ShippingAddressSubDistrict;
                dr["SHIPPING_AUMPHUR"] = item.ShippingAddressDistrict;
                dr["SHIPPING_PROVINCE"] = item.ShippingAddressProvince;
                dr["SHIPPING_ZIPCODE"] = item.ShippingAddressPostalCode;
            } else {
                dr["SHIPPING_FLAG"] = "Y";
                dr["SHIPPING_NAME"] = "";
                dr["SHIPPING_NO"] = "";
                dr["SHIPPING_VILLAGE"] = "";
                dr["SHIPPING_MU"] = "";
                dr["SHIPPING_SOI"] = "";
                dr["SHIPPING_ROAD"] = "";
                dr["SHIPPING_DISTRICT"] = "";
                dr["SHIPPING_AUMPHUR"] = "";
                dr["SHIPPING_PROVINCE"] = "";
                dr["SHIPPING_ZIPCODE"] = "";
            }
            dr["LOAN_NO"] = "";
            dr["IB_FLAG"] = "I"; // item.PropertyType;
            dr["POLICY_NO"] = "";
            dr["ADD_INFO_CODE1"] = "";
            dr["ADD_INFO_VALUE1"] = "";
            dr["ADD_INFO_CODE2"] = "";
            dr["ADD_INFO_VALUE2"] = "";
            dr["ADD_INFO_CODE3"] = "";
            dr["ADD_INFO_VALUE3"] = "";
            dr["ADD_INFO_CODE4"] = "";
            dr["ADD_INFO_VALUE4"] = "";
            dr["ADD_INFO_CODE5"] = "";
            dr["ADD_INFO_VALUE5"] = "";
            dr["ADD_IDCARD_NO"] = "";
            dr["ADD_IDCARD_VILLAGE"] = "";
            dr["ADD_IDCARD_NO_MU"] = "";
            dr["ADD_IDCARD_ALLEY"] = "";
            dr["ADD_IDCARD_ROAD"] = "";
            dr["ADD_IDCARD_DISTRICT"] = "";
            dr["ADD_IDCARD_COUNTY"] = "";
            dr["ADD_IDCARD_PROVINCE"] = "";
            dr["ADD_IDCARD_ZIPCODE"] = "";
            dr["ADD_CURRENT_NO_ADD"] = item.AddressNo;
            dr["ADD_CURRENT_VILLAGE"] = item.AddressVillage;
            dr["ADD_CURRENT_NO_MU"] = item.AddressMoo;
            dr["ADD_CURRENT_ALLEY"] = item.AddressSoi;
            dr["ADD_CURRENT_ROAD"] = item.AddressRoad;
            dr["ADD_CURRENT_DISTRICT"] = item.AddressSubDistrict;
            dr["ADD_CURRENT_COUNTY"] = item.AddressDistrict;
            dr["ADD_CURRENT_PROVINCE"] = item.AddressProvince;
            dr["ADD_CURRENT_ZIPCODE"] = item.AddressPostalCode;
            dr["ADD_POLICY_NO"] = item.ShippingAddressNo;
            dr["ADD_POLICY_VILLAGE"] = item.ShippingAddressVillage;
            dr["ADD_POLICY_NO_MU"] = item.ShippingAddressMoo;
            dr["ADD_POLICY_ALLEY"] = item.ShippingAddressSoi;
            dr["ADD_POLICY_ROAD"] = item.ShippingAddressRoad;
            dr["ADD_POLICY_DISTRICT"] = item.ShippingAddressDistrict;
            dr["ADD_POLICY_COUNTY"] = item.ShippingAddressSubDistrict;
            dr["ADD_POLICY_PROVINCE"] = item.ShippingAddressProvince;
            dr["ADD_POLICY_ZIPCODE"] = item.ShippingAddressPostalCode;
            if (!item.BillingIsChecked) {
                dr["TAX_IS_INSURED"] = "N";
                dr["TAX_PERSON_TYPE"] = "P";
                dr["TAX_OCCUPATION"] = (!string.IsNullOrEmpty(item.PolicyHolderJob) ? item.PolicyHolderJob : "อื่นๆ");
                dr["TAX_PERSON_CARD_TYPE"] = "0" + item.PolicyHolderCardType;
                dr["TAX_PERSON_CARD_ID"] = item.PolicyHolderIdCard;
                dr["TAX_ID"] = "";
                dr["ADD_INVOICE_NAME"] = item.PolicyHolderTitleText + " " + item.PolicyHolderName + " " + item.PolicyHolderSurname;
                dr["ADD_INVOICE_NO_ADD"] = item.BillingAddressNo;
                dr["ADD_INVOICE_VILLAGE"] = item.BillingAddressVillage;
                dr["ADD_INVOICE_NO_MU"] = item.BillingAddressMoo;
                dr["ADD_INVOICE_ALLEY"] = item.BillingAddressSoi;
                dr["ADD_INVOICE_ROAD"] = item.BillingAddressRoad;
                dr["ADD_INVOICE_DISTRICT"] = item.BillingAddressSubDistrict;
                dr["ADD_INVOICE_AMPHUR"] = item.BillingAddressDistrict;
                dr["ADD_INVOICE_PROVINCE"] = item.BillingAddressProvince;
                dr["ADD_INVOICE_ZIPCODE"] = item.BillingAddressPostalCode;
            } else {
                dr["TAX_IS_INSURED"] = "Y";
                dr["TAX_PERSON_TYPE"] = "";
                dr["TAX_OCCUPATION"] = "";
                dr["TAX_PERSON_CARD_TYPE"] = "";
                dr["TAX_PERSON_CARD_ID"] = "";
                dr["TAX_ID"] = "";
                dr["ADD_INVOICE_NAME"] = "";
                dr["ADD_INVOICE_NO_ADD"] = "";
                dr["ADD_INVOICE_VILLAGE"] = "";
                dr["ADD_INVOICE_NO_MU"] = "";
                dr["ADD_INVOICE_ALLEY"] = "";
                dr["ADD_INVOICE_ROAD"] = "";
                dr["ADD_INVOICE_DISTRICT"] = "";
                dr["ADD_INVOICE_AMPHUR"] = "";
                dr["ADD_INVOICE_PROVINCE"] = "";
                dr["ADD_INVOICE_ZIPCODE"] = "";
            }
            dr["PAY_CREDIT_TERM_TYPE"] = "ปกติ";
            dr["PAY_INSTALLMENT"] = order.PaymentInstallment;
            dr["PAY_TYPE"] = FormatPaymentChannel(order.PaymentChannel);
            dr["PAY_BANK_CODE"] = NullUtils.cvString(item.PaymentBankCode);
            dr["PAY_STATUS"] = "Y";
            dr["PAY_MAILING_TYPE"] = FormatEPolicy(app.EPolicy);
            dr["PAY_POLI_NO"] = app.Order.PolicyNo;
            dr["PAY_POLICY_STATUS"] = "1";
            dr["PAY_GATEWAY_CODE"] = "2C2P";
            dr["PAY_REF_NO"] = item.PaymentReference;
            dr["PAY_PAID_DATE"] = FormatDate(item.PaymentDate);
            dr["AUTO_DOC_FLAG"] = (String.IsNullOrEmpty(item.PolicyNo) ? "N" : "Y");
            dr["AUTO_RV_FLAG"] = "Y";
            dr["PAYIN_DATE"] = FormatDate(item.PaymentDate);
            dr["TRANS_CODE"] = "RPM";
            dr["AGREEMENT_PREMIUM"] = 0;
            return dr;
        }

        protected DataRow CreateSubmitPolicyRowPA(DataTable dt, OrderApplicationProduct order) {
            PartnerApplicationPA item = GetPartnerApplicationPA(order);
            ApplicationProduct app = order.ApplicationProduct[0];
            ProductPAPremium premium = app.Product.ProductPAPremium[0];
            DataRow dr = dt.NewRow();
            dr["ORDER_NO"] = item.OrderId;
            dr["CA_NO"] = item.CANo;
            dr["CA_GROUP"] = "NEW";
            dr["CHANNEL"] = "ODGT";
            dr["APP_DATE"] = FormatDate(item.TransactionDate);
            dr["CARD_TYPE"] = "0" + item.PolicyHolderCardType;
            dr["CUST_ID_CARD"] = item.PolicyHolderIdCard;
            dr["CUST_TITLE"] = item.PolicyHolderTitle;
            dr["CUST_FNAME"] = item.PolicyHolderName;
            dr["CUST_LNAME"] = item.PolicyHolderSurname;
            dr["CUST_BIRTHDAY"] = FormatDate(item.PolicyHolderBirthday);
            dr["CUST_PHONE"] = item.PolicyHolderTelephone;
            dr["CUST_MOBILE"] = item.PolicyHolderMobilePhone;
            dr["CUST_EMAIL"] = item.PolicyHolderEmail;
            dr["CUST_FACEBOOK"] = item.PolicyHolderFacebook;
            dr["CUST_INSTAGRAM"] = item.PolicyHolderInstagram;
            dr["CUST_LINEID"] = item.PolicyHolderLineId;
            dr["CUST_OCCUPATION"] = item.PolicyHolderJob;
            dr["CUST_TYPE"] = "P";
            dr["DEPARTMENT_NAME"] = "MKT_Team";
            dr["OWNER_NAME"] = "DIGITAL";
            dr["CUST_AGE"] = item.PolicyHolderAge;
            dr["BENEFIT_NM"] = item.BeneficiaryName + " " + item.BeneficiarySurname;
            dr["RELATION_BENEFIT"] = item.BeneficiaryRelationship;
            dr["INS_CODE"] = GetCompanyCorporateCode(item.InsuranceCompanyCode);
            dr["INS_NM"] = item.InsuranceCompany;
            dr["OUTLET"] = "999";
            dr["HUB"] = "ธนชาตโบรกเกอร์";
            dr["AGENT_CODE"] = "999999";
            dr["AGENT_NAME"] = "";
            dr["GROUPHEAD"] = "";
            dr["POLICY_TYPE"] = "New";
            dr["INDIVIDUAL_TYPE"] = "";
            dr["INS_CUST_NUM"] = Math.Max(1, item.PolicyHolder.Count);
            dr["INS_DETAIL"] = "";
            dr["COUNTRY"] = "";
            dr["MIN_AGE"] = premium.MinAge;
            dr["MAX_AGE"] = premium.MaxAge;
            dr["PLAN"] = premium.PlanCode;
            dr["CLASS"] = app.Product.Class;
            dr["SUBCLASS"] = app.Product.SubClass;
            dr["POLICY_NO_OLD"] = "";
            dr["EFF_DATE"] = FormatDate(item.PolicyActiveDate);
            dr["EXP_DATE"] = FormatDate(item.PolicyExpireDate);
            dr["SUMINSURE"] = premium.SumInsured;
            dr["PREMIUM"] = premium.Premium;
            dr["DUTY"] = premium.Duty;
            dr["TAX"] = premium.Vat;
            dr["NETPREMIUM"] = premium.NetPremium;
            dr["PAY_CODE"] = "C23";
            dr["INS_NM_PAY"] = "";
            dr["SEND_CODE"] = "B10";
            dr["CIF"] = "";
            dr["AO_CODE"] = "";
            dr["OFF_NAME"] = "";
            dr["DEP_CODE"] = "";
            dr["DEPT_NM"] = "";
            dr["MATCH_CHANNEL"] = "";
            dr["RM"] = "";
            dr["PACKAGE_CODE"] = app.Product.InsuranceProductCode;
            dr["RISK_EXP_CODE"] = "";
            dr["BUILD_CLASS_CODE"] = "";
            //dr["LOCATION_ADDR"] = "";
            dr["LOCATION_NO"] = "";
            dr["LOCATION_VILLAGE"] = "";
            dr["LOCATION_MU"] = "";
            dr["LOCATION_SOI"] = "";
            dr["LOCATION_ROAD"] = "";
            dr["LOCATION_DISTRICT"] = "";
            dr["LOCATION_AMPHUR"] = "";
            dr["LOCATION_PROVINCE"] = "";
            dr["LOCATION_ZIPCODE"] = "";
            dr["RATE"] = "";
            dr["FIRE_DISCRATE"] = "";
            dr["CODEPAY_DESCRIPTION"] = "";
            //dr["SHIPPING_ADDR"] = 
            if (!item.ShippingIsChecked) {
                dr["SHIPPING_FLAG"] = "N";
                dr["SHIPPING_NAME"] = item.PolicyHolderTitleText + " " + item.PolicyHolderName + " " + item.PolicyHolderSurname;
                dr["SHIPPING_NO"] = item.ShippingAddressNo;
                dr["SHIPPING_VILLAGE"] = item.ShippingAddressVillage;
                dr["SHIPPING_MU"] = item.ShippingAddressMoo;
                dr["SHIPPING_SOI"] = item.ShippingAddressSoi;
                dr["SHIPPING_ROAD"] = item.ShippingAddressRoad;
                dr["SHIPPING_DISTRICT"] = item.ShippingAddressSubDistrict;
                dr["SHIPPING_AUMPHUR"] = item.ShippingAddressDistrict;
                dr["SHIPPING_PROVINCE"] = item.ShippingAddressProvince;
                dr["SHIPPING_ZIPCODE"] = item.ShippingAddressPostalCode;
            } else {
                dr["SHIPPING_FLAG"] = "Y";
                dr["SHIPPING_NAME"] = "";
                dr["SHIPPING_NO"] = "";
                dr["SHIPPING_VILLAGE"] = "";
                dr["SHIPPING_MU"] = "";
                dr["SHIPPING_SOI"] = "";
                dr["SHIPPING_ROAD"] = "";
                dr["SHIPPING_DISTRICT"] = "";
                dr["SHIPPING_AUMPHUR"] = "";
                dr["SHIPPING_PROVINCE"] = "";
                dr["SHIPPING_ZIPCODE"] = "";
            }
            dr["LOAN_NO"] = "";
            dr["IB_FLAG"] = "";
            dr["POLICY_NO"] = "";
            dr["ADD_INFO_CODE1"] = "";
            dr["ADD_INFO_VALUE1"] = "";
            dr["ADD_INFO_CODE2"] = "";
            dr["ADD_INFO_VALUE2"] = "";
            dr["ADD_INFO_CODE3"] = "";
            dr["ADD_INFO_VALUE3"] = "";
            dr["ADD_INFO_CODE4"] = "";
            dr["ADD_INFO_VALUE4"] = "";
            dr["ADD_INFO_CODE5"] = "";
            dr["ADD_INFO_VALUE5"] = "";
            dr["ADD_IDCARD_NO"] = "";
            dr["ADD_IDCARD_VILLAGE"] = "";
            dr["ADD_IDCARD_NO_MU"] = "";
            dr["ADD_IDCARD_ALLEY"] = "";
            dr["ADD_IDCARD_ROAD"] = "";
            dr["ADD_IDCARD_DISTRICT"] = "";
            dr["ADD_IDCARD_COUNTY"] = "";
            dr["ADD_IDCARD_PROVINCE"] = "";
            dr["ADD_IDCARD_ZIPCODE"] = "";
            dr["ADD_CURRENT_NO_ADD"] = item.AddressNo;
            dr["ADD_CURRENT_VILLAGE"] = item.AddressVillage;
            dr["ADD_CURRENT_NO_MU"] = item.AddressMoo;
            dr["ADD_CURRENT_ALLEY"] = item.AddressSoi;
            dr["ADD_CURRENT_ROAD"] = item.AddressRoad;
            dr["ADD_CURRENT_DISTRICT"] = item.AddressSubDistrict;
            dr["ADD_CURRENT_COUNTY"] = item.AddressDistrict;
            dr["ADD_CURRENT_PROVINCE"] = item.AddressProvince;
            dr["ADD_CURRENT_ZIPCODE"] = item.AddressPostalCode;
            dr["ADD_POLICY_NO"] = item.ShippingAddressNo;
            dr["ADD_POLICY_VILLAGE"] = item.ShippingAddressVillage;
            dr["ADD_POLICY_NO_MU"] = item.ShippingAddressMoo;
            dr["ADD_POLICY_ALLEY"] = item.ShippingAddressSoi;
            dr["ADD_POLICY_ROAD"] = item.ShippingAddressRoad;
            dr["ADD_POLICY_DISTRICT"] = item.ShippingAddressDistrict;
            dr["ADD_POLICY_COUNTY"] = item.ShippingAddressSubDistrict;
            dr["ADD_POLICY_PROVINCE"] = item.ShippingAddressProvince;
            dr["ADD_POLICY_ZIPCODE"] = item.ShippingAddressPostalCode;
            if (!item.BillingIsChecked) {
                dr["TAX_IS_INSURED"] = "N";
                dr["TAX_PERSON_TYPE"] = "P";
                dr["TAX_OCCUPATION"] = (!string.IsNullOrEmpty(item.PolicyHolderJob) ? item.PolicyHolderJob : "อื่นๆ");
                dr["TAX_PERSON_CARD_TYPE"] = "0" + item.PolicyHolderCardType;
                dr["TAX_PERSON_CARD_ID"] = item.PolicyHolderIdCard;
                dr["TAX_ID"] = "";
                dr["ADD_INVOICE_NAME"] = item.PolicyHolderTitleText + " " + item.PolicyHolderName + " " + item.PolicyHolderSurname;
                dr["ADD_INVOICE_NO_ADD"] = item.BillingAddressNo;
                dr["ADD_INVOICE_VILLAGE"] = item.BillingAddressVillage;
                dr["ADD_INVOICE_NO_MU"] = item.BillingAddressMoo;
                dr["ADD_INVOICE_ALLEY"] = item.BillingAddressSoi;
                dr["ADD_INVOICE_ROAD"] = item.BillingAddressRoad;
                dr["ADD_INVOICE_DISTRICT"] = item.BillingAddressSubDistrict;
                dr["ADD_INVOICE_AMPHUR"] = item.BillingAddressDistrict;
                dr["ADD_INVOICE_PROVINCE"] = item.BillingAddressProvince;
                dr["ADD_INVOICE_ZIPCODE"] = item.BillingAddressPostalCode;
            } else {
                dr["TAX_IS_INSURED"] = "Y";
                dr["TAX_PERSON_TYPE"] = "";
                dr["TAX_OCCUPATION"] = "";
                dr["TAX_PERSON_CARD_TYPE"] = "";
                dr["TAX_PERSON_CARD_ID"] = "";
                dr["TAX_ID"] = "";
                dr["ADD_INVOICE_NAME"] = "";
                dr["ADD_INVOICE_NO_ADD"] = "";
                dr["ADD_INVOICE_VILLAGE"] = "";
                dr["ADD_INVOICE_NO_MU"] = "";
                dr["ADD_INVOICE_ALLEY"] = "";
                dr["ADD_INVOICE_ROAD"] = "";
                dr["ADD_INVOICE_DISTRICT"] = "";
                dr["ADD_INVOICE_AMPHUR"] = "";
                dr["ADD_INVOICE_PROVINCE"] = "";
                dr["ADD_INVOICE_ZIPCODE"] = "";
            }
            dr["PAY_CREDIT_TERM_TYPE"] = "ปกติ";
            dr["PAY_INSTALLMENT"] = order.PaymentInstallment;
            dr["PAY_TYPE"] = FormatPaymentChannel(order.PaymentChannel);
            dr["PAY_BANK_CODE"] = NullUtils.cvString(item.PaymentBankCode);
            dr["PAY_STATUS"] = "Y";
            dr["PAY_MAILING_TYPE"] = FormatEPolicy(app.EPolicy);
            dr["PAY_POLI_NO"] = app.Order.PolicyNo;
            dr["PAY_POLICY_STATUS"] = "1";
            dr["PAY_GATEWAY_CODE"] = "2C2P";
            dr["PAY_REF_NO"] = item.PaymentReference;
            dr["PAY_PAID_DATE"] = FormatDate(item.PaymentDate);
            dr["AUTO_DOC_FLAG"] = (String.IsNullOrEmpty(item.PolicyNo) ? "N" : "Y");
            dr["AUTO_RV_FLAG"] = "Y";
            dr["PAYIN_DATE"] = FormatDate(item.PaymentDate);
            dr["TRANS_CODE"] = "RPM";
            dr["AGREEMENT_PREMIUM"] = 0;
            return dr;
        }

        protected DataRow CreateSubmitPolicyRowPH(DataTable dt, OrderApplicationProduct order) {
            PartnerApplicationPH item = GetPartnerApplicationPH(order);
            ApplicationProduct app = order.ApplicationProduct[0];
            ProductPHPremium premium = app.Product.ProductPHPremium[0];
            DataRow dr = dt.NewRow();
            dr["ORDER_NO"] = item.OrderId;
            dr["CA_NO"] = item.CANo;
            dr["CA_GROUP"] = "NEW";
            dr["CHANNEL"] = "ODGT";
            dr["APP_DATE"] = FormatDate(item.TransactionDate);
            dr["CARD_TYPE"] = "0" + item.PolicyHolderCardType;
            dr["CUST_ID_CARD"] = item.PolicyHolderIdCard;
            dr["CUST_TITLE"] = item.PolicyHolderTitle;
            dr["CUST_FNAME"] = item.PolicyHolderName;
            dr["CUST_LNAME"] = item.PolicyHolderSurname;
            dr["CUST_BIRTHDAY"] = FormatDate(item.PolicyHolderBirthday);
            dr["CUST_PHONE"] = item.PolicyHolderTelephone;
            dr["CUST_MOBILE"] = item.PolicyHolderMobilePhone;
            dr["CUST_EMAIL"] = item.PolicyHolderEmail;
            dr["CUST_FACEBOOK"] = item.PolicyHolderFacebook;
            dr["CUST_INSTAGRAM"] = item.PolicyHolderInstagram;
            dr["CUST_LINEID"] = item.PolicyHolderLineId;
            dr["CUST_OCCUPATION"] = item.PolicyHolderJob;
            dr["CUST_TYPE"] = "P";
            dr["DEPARTMENT_NAME"] = "MKT_Team";
            dr["OWNER_NAME"] = "DIGITAL";
            dr["CUST_AGE"] = item.PolicyHolderAge;
            dr["BENEFIT_NM"] = item.BeneficiaryName + " " + item.BeneficiarySurname;
            dr["RELATION_BENEFIT"] = item.BeneficiaryRelationship;
            dr["INS_CODE"] = GetCompanyCorporateCode(item.InsuranceCompanyCode);
            dr["INS_NM"] = item.InsuranceCompany;
            dr["OUTLET"] = "999";
            dr["HUB"] = "ธนชาตโบรกเกอร์";
            dr["AGENT_CODE"] = "999999";
            dr["AGENT_NAME"] = "";
            dr["GROUPHEAD"] = "";
            dr["POLICY_TYPE"] = "New";
            dr["INDIVIDUAL_TYPE"] = "";
            dr["INS_CUST_NUM"] = Math.Max(1, item.PolicyHolder.Count);
            dr["INS_DETAIL"] = "";
            dr["COUNTRY"] = "";
            dr["MIN_AGE"] = premium.MinAge;
            dr["MAX_AGE"] = premium.MaxAge;
            dr["PLAN"] = premium.PlanCode;
            dr["CLASS"] = app.Product.Class;
            dr["SUBCLASS"] = app.Product.SubClass;
            dr["POLICY_NO_OLD"] = "";
            dr["EFF_DATE"] = FormatDate(item.PolicyActiveDate);
            dr["EXP_DATE"] = FormatDate(item.PolicyExpireDate);
            dr["SUMINSURE"] = premium.SumInsured;
            dr["PREMIUM"] = premium.Premium;
            dr["DUTY"] = premium.Duty;
            dr["TAX"] = premium.Vat;
            dr["NETPREMIUM"] = premium.NetPremium;
            dr["PAY_CODE"] = "C23";
            dr["INS_NM_PAY"] = "";
            dr["SEND_CODE"] = "B10";
            dr["CIF"] = "";
            dr["AO_CODE"] = "";
            dr["OFF_NAME"] = "";
            dr["DEP_CODE"] = "";
            dr["DEPT_NM"] = "";
            dr["MATCH_CHANNEL"] = "";
            dr["RM"] = "";
            dr["PACKAGE_CODE"] = app.Product.InsuranceProductCode;
            dr["RISK_EXP_CODE"] = "";
            dr["BUILD_CLASS_CODE"] = "";
            //dr["LOCATION_ADDR"] = "";
            dr["LOCATION_NO"] = "";
            dr["LOCATION_VILLAGE"] = "";
            dr["LOCATION_MU"] = "";
            dr["LOCATION_SOI"] = "";
            dr["LOCATION_ROAD"] = "";
            dr["LOCATION_DISTRICT"] = "";
            dr["LOCATION_AMPHUR"] = "";
            dr["LOCATION_PROVINCE"] = "";
            dr["LOCATION_ZIPCODE"] = "";
            dr["RATE"] = "";
            dr["FIRE_DISCRATE"] = "";
            dr["CODEPAY_DESCRIPTION"] = "";
            //dr["SHIPPING_ADDR"] = "";
            if (!item.ShippingIsChecked) {
                dr["SHIPPING_FLAG"] = "N";
                dr["SHIPPING_NAME"] = item.PolicyHolderTitleText + " " + item.PolicyHolderName + " " + item.PolicyHolderSurname;
                dr["SHIPPING_NO"] = item.ShippingAddressNo;
                dr["SHIPPING_VILLAGE"] = item.ShippingAddressVillage;
                dr["SHIPPING_MU"] = item.ShippingAddressMoo;
                dr["SHIPPING_SOI"] = item.ShippingAddressSoi;
                dr["SHIPPING_ROAD"] = item.ShippingAddressRoad;
                dr["SHIPPING_DISTRICT"] = item.ShippingAddressSubDistrict;
                dr["SHIPPING_AUMPHUR"] = item.ShippingAddressDistrict;
                dr["SHIPPING_PROVINCE"] = item.ShippingAddressProvince;
                dr["SHIPPING_ZIPCODE"] = item.ShippingAddressPostalCode;
            } else {
                dr["SHIPPING_FLAG"] = "Y";
                dr["SHIPPING_NAME"] = "";
                dr["SHIPPING_NO"] = "";
                dr["SHIPPING_VILLAGE"] = "";
                dr["SHIPPING_MU"] = "";
                dr["SHIPPING_SOI"] = "";
                dr["SHIPPING_ROAD"] = "";
                dr["SHIPPING_DISTRICT"] = "";
                dr["SHIPPING_AUMPHUR"] = "";
                dr["SHIPPING_PROVINCE"] = "";
                dr["SHIPPING_ZIPCODE"] = "";
            }
            dr["LOAN_NO"] = "";
            dr["IB_FLAG"] = "";
            dr["POLICY_NO"] = "";
            dr["ADD_INFO_CODE1"] = "";
            dr["ADD_INFO_VALUE1"] = "";
            dr["ADD_INFO_CODE2"] = "";
            dr["ADD_INFO_VALUE2"] = "";
            dr["ADD_INFO_CODE3"] = "";
            dr["ADD_INFO_VALUE3"] = "";
            dr["ADD_INFO_CODE4"] = "";
            dr["ADD_INFO_VALUE4"] = "";
            dr["ADD_INFO_CODE5"] = "";
            dr["ADD_INFO_VALUE5"] = "";
            dr["ADD_IDCARD_NO"] = "";
            dr["ADD_IDCARD_VILLAGE"] = "";
            dr["ADD_IDCARD_NO_MU"] = "";
            dr["ADD_IDCARD_ALLEY"] = "";
            dr["ADD_IDCARD_ROAD"] = "";
            dr["ADD_IDCARD_DISTRICT"] = "";
            dr["ADD_IDCARD_COUNTY"] = "";
            dr["ADD_IDCARD_PROVINCE"] = "";
            dr["ADD_IDCARD_ZIPCODE"] = "";
            dr["ADD_CURRENT_NO_ADD"] = item.AddressNo;
            dr["ADD_CURRENT_VILLAGE"] = item.AddressVillage;
            dr["ADD_CURRENT_NO_MU"] = item.AddressMoo;
            dr["ADD_CURRENT_ALLEY"] = item.AddressSoi;
            dr["ADD_CURRENT_ROAD"] = item.AddressRoad;
            dr["ADD_CURRENT_DISTRICT"] = item.AddressSubDistrict;
            dr["ADD_CURRENT_COUNTY"] = item.AddressDistrict;
            dr["ADD_CURRENT_PROVINCE"] = item.AddressProvince;
            dr["ADD_CURRENT_ZIPCODE"] = item.AddressPostalCode;
            dr["ADD_POLICY_NO"] = item.ShippingAddressNo;
            dr["ADD_POLICY_VILLAGE"] = item.ShippingAddressVillage;
            dr["ADD_POLICY_NO_MU"] = item.ShippingAddressMoo;
            dr["ADD_POLICY_ALLEY"] = item.ShippingAddressSoi;
            dr["ADD_POLICY_ROAD"] = item.ShippingAddressRoad;
            dr["ADD_POLICY_DISTRICT"] = item.ShippingAddressDistrict;
            dr["ADD_POLICY_COUNTY"] = item.ShippingAddressSubDistrict;
            dr["ADD_POLICY_PROVINCE"] = item.ShippingAddressProvince;
            dr["ADD_POLICY_ZIPCODE"] = item.ShippingAddressPostalCode;
            if (!item.BillingIsChecked) {
                dr["TAX_IS_INSURED"] = "N";
                dr["TAX_PERSON_TYPE"] = "P";
                dr["TAX_OCCUPATION"] = (!string.IsNullOrEmpty(item.PolicyHolderJob) ? item.PolicyHolderJob : "อื่นๆ");
                dr["TAX_PERSON_CARD_TYPE"] = "0" + item.PolicyHolderCardType;
                dr["TAX_PERSON_CARD_ID"] = item.PolicyHolderIdCard;
                dr["TAX_ID"] = "";
                dr["ADD_INVOICE_NAME"] = item.PolicyHolderTitleText + " " + item.PolicyHolderName + " " + item.PolicyHolderSurname;
                dr["ADD_INVOICE_NO_ADD"] = item.BillingAddressNo;
                dr["ADD_INVOICE_VILLAGE"] = item.BillingAddressVillage;
                dr["ADD_INVOICE_NO_MU"] = item.BillingAddressMoo;
                dr["ADD_INVOICE_ALLEY"] = item.BillingAddressSoi;
                dr["ADD_INVOICE_ROAD"] = item.BillingAddressRoad;
                dr["ADD_INVOICE_DISTRICT"] = item.BillingAddressSubDistrict;
                dr["ADD_INVOICE_AMPHUR"] = item.BillingAddressDistrict;
                dr["ADD_INVOICE_PROVINCE"] = item.BillingAddressProvince;
                dr["ADD_INVOICE_ZIPCODE"] = item.BillingAddressPostalCode;
            } else {
                dr["TAX_IS_INSURED"] = "Y";
                dr["TAX_PERSON_TYPE"] = "";
                dr["TAX_OCCUPATION"] = "";
                dr["TAX_PERSON_CARD_TYPE"] = "";
                dr["TAX_PERSON_CARD_ID"] = "";
                dr["TAX_ID"] = "";
                dr["ADD_INVOICE_NAME"] = "";
                dr["ADD_INVOICE_NO_ADD"] = "";
                dr["ADD_INVOICE_VILLAGE"] = "";
                dr["ADD_INVOICE_NO_MU"] = "";
                dr["ADD_INVOICE_ALLEY"] = "";
                dr["ADD_INVOICE_ROAD"] = "";
                dr["ADD_INVOICE_DISTRICT"] = "";
                dr["ADD_INVOICE_AMPHUR"] = "";
                dr["ADD_INVOICE_PROVINCE"] = "";
                dr["ADD_INVOICE_ZIPCODE"] = "";
            }
            dr["PAY_CREDIT_TERM_TYPE"] = "ปกติ";
            dr["PAY_INSTALLMENT"] = order.PaymentInstallment;
            dr["PAY_TYPE"] = FormatPaymentChannel(order.PaymentChannel);
            dr["PAY_BANK_CODE"] = NullUtils.cvString(item.PaymentBankCode);
            dr["PAY_STATUS"] = "Y";
            dr["PAY_MAILING_TYPE"] = FormatEPolicy(app.EPolicy);
            dr["PAY_POLI_NO"] = app.Order.PolicyNo;
            dr["PAY_POLICY_STATUS"] = "1";
            dr["PAY_GATEWAY_CODE"] = "2C2P";
            dr["PAY_REF_NO"] = item.PaymentReference;
            dr["PAY_PAID_DATE"] = FormatDate(item.PaymentDate);
            dr["AUTO_DOC_FLAG"] = (String.IsNullOrEmpty(item.PolicyNo) ? "N" : "Y");
            dr["AUTO_RV_FLAG"] = "Y";
            dr["PAYIN_DATE"] = FormatDate(item.PaymentDate);
            dr["TRANS_CODE"] = "RPM";
            dr["AGREEMENT_PREMIUM"] = 0;
            return dr;
        }

        protected DataRow CreateSubmitPolicyRowTA(DataTable dt, OrderApplicationProduct order) {
            PartnerApplicationTA item = GetPartnerApplicationTA(order);
            ApplicationProduct app = order.ApplicationProduct[0];
            ProductTAPremium premium = app.Product.ProductTAPremium[0];
            DataRow dr = dt.NewRow();
            dr["ORDER_NO"] = item.OrderId;
            dr["CA_NO"] = item.CANo;
            dr["CA_GROUP"] = "NEW";
            dr["CHANNEL"] = "ODGT";
            dr["APP_DATE"] = FormatDate(item.TransactionDate);
            dr["CARD_TYPE"] = "0" + item.PolicyHolderCardType;
            dr["CUST_ID_CARD"] = item.PolicyHolderIdCard;
            dr["CUST_TITLE"] = item.PolicyHolderTitle;
            dr["CUST_FNAME"] = item.PolicyHolderName;
            dr["CUST_LNAME"] = item.PolicyHolderSurname;
            dr["CUST_BIRTHDAY"] = FormatDate(item.PolicyHolderBirthday);
            dr["CUST_PHONE"] = item.PolicyHolderTelephone;
            dr["CUST_MOBILE"] = item.PolicyHolderMobilePhone;
            dr["CUST_EMAIL"] = item.PolicyHolderEmail;
            dr["CUST_FACEBOOK"] = item.PolicyHolderFacebook;
            dr["CUST_INSTAGRAM"] = item.PolicyHolderInstagram;
            dr["CUST_LINEID"] = item.PolicyHolderLineId;
            dr["CUST_OCCUPATION"] = (!string.IsNullOrEmpty(item.PolicyHolderJob) ? item.PolicyHolderJob : "อื่นๆ");
            dr["CUST_TYPE"] = "P";
            dr["DEPARTMENT_NAME"] = "MKT_Team";
            dr["OWNER_NAME"] = "DIGITAL";
            dr["CUST_AGE"] = item.PolicyHolderAge;
            dr["BENEFIT_NM"] = item.BeneficiaryName + " " + item.BeneficiarySurname;
            dr["RELATION_BENEFIT"] = item.BeneficiaryRelationship;
            dr["INS_CODE"] = GetCompanyCorporateCode(item.InsuranceCompanyCode);
            dr["INS_NM"] = item.InsuranceCompany;
            dr["OUTLET"] = "999";
            dr["HUB"] = "ธนชาตโบรกเกอร์";
            dr["AGENT_CODE"] = "999999";
            dr["AGENT_NAME"] = "";
            dr["GROUPHEAD"] = "";
            dr["POLICY_TYPE"] = "New";
            dr["INDIVIDUAL_TYPE"] = "";
            dr["INS_CUST_NUM"] = Math.Max(1, item.PolicyHolder.Count);
            dr["INS_DETAIL"] = "";
            dr["COUNTRY"] = item.Country;
            dr["MIN_AGE"] = "";
            dr["MAX_AGE"] = "";
            dr["PLAN"] = premium.PlanCode;
            dr["CLASS"] = app.Product.Class;
            dr["SUBCLASS"] = app.Product.SubClass;
            dr["POLICY_NO_OLD"] = "";
            dr["EFF_DATE"] = FormatDate(item.PolicyActiveDate);
            dr["EXP_DATE"] = FormatDate(item.PolicyExpireDate);
            dr["SUMINSURE"] = premium.SumInsured;
            dr["PREMIUM"] = premium.Premium;
            dr["DUTY"] = premium.Duty;
            dr["TAX"] = premium.Vat;
            dr["NETPREMIUM"] = premium.NetPremium;
            dr["PAY_CODE"] = "C23";
            dr["INS_NM_PAY"] = "";
            dr["SEND_CODE"] = "B10";
            dr["CIF"] = "";
            dr["AO_CODE"] = "";
            dr["OFF_NAME"] = "";
            dr["DEP_CODE"] = "";
            dr["DEPT_NM"] = "";
            dr["MATCH_CHANNEL"] = "";
            dr["RM"] = "";
            dr["PACKAGE_CODE"] = app.Product.InsuranceProductCode;
            dr["RISK_EXP_CODE"] = "";
            dr["BUILD_CLASS_CODE"] = "";
            //dr["LOCATION_ADDR"] = "";
            dr["LOCATION_NO"] = "";
            dr["LOCATION_VILLAGE"] = "";
            dr["LOCATION_MU"] = "";
            dr["LOCATION_SOI"] = "";
            dr["LOCATION_ROAD"] = "";
            dr["LOCATION_DISTRICT"] = "";
            dr["LOCATION_AMPHUR"] = "";
            dr["LOCATION_PROVINCE"] = "";
            dr["LOCATION_ZIPCODE"] = "";
            dr["RATE"] = "";
            dr["FIRE_DISCRATE"] = "";
            dr["CODEPAY_DESCRIPTION"] = "";
            //dr["SHIPPING_ADDR"] = "";
            if (!item.ShippingIsChecked) {
                dr["SHIPPING_FLAG"] = "N";
                dr["SHIPPING_NAME"] = item.PolicyHolderTitleText + " " + item.PolicyHolderName + " " + item.PolicyHolderSurname;
                dr["SHIPPING_NO"] = item.ShippingAddressNo;
                dr["SHIPPING_VILLAGE"] = item.ShippingAddressVillage;
                dr["SHIPPING_MU"] = item.ShippingAddressMoo;
                dr["SHIPPING_SOI"] = item.ShippingAddressSoi;
                dr["SHIPPING_ROAD"] = item.ShippingAddressRoad;
                dr["SHIPPING_DISTRICT"] = item.ShippingAddressSubDistrict;
                dr["SHIPPING_AUMPHUR"] = item.ShippingAddressDistrict;
                dr["SHIPPING_PROVINCE"] = item.ShippingAddressProvince;
                dr["SHIPPING_ZIPCODE"] = item.ShippingAddressPostalCode;
            } else {
                dr["SHIPPING_FLAG"] = "Y";
                dr["SHIPPING_NAME"] = "";
                dr["SHIPPING_NO"] = "";
                dr["SHIPPING_VILLAGE"] = "";
                dr["SHIPPING_MU"] = "";
                dr["SHIPPING_SOI"] = "";
                dr["SHIPPING_ROAD"] = "";
                dr["SHIPPING_DISTRICT"] = "";
                dr["SHIPPING_AUMPHUR"] = "";
                dr["SHIPPING_PROVINCE"] = "";
                dr["SHIPPING_ZIPCODE"] = "";
            }
            dr["LOAN_NO"] = "";
            dr["IB_FLAG"] = "";
            dr["POLICY_NO"] = "";
            dr["ADD_INFO_CODE1"] = "";
            dr["ADD_INFO_VALUE1"] = "";
            dr["ADD_INFO_CODE2"] = "";
            dr["ADD_INFO_VALUE2"] = "";
            dr["ADD_INFO_CODE3"] = "";
            dr["ADD_INFO_VALUE3"] = "";
            dr["ADD_INFO_CODE4"] = "";
            dr["ADD_INFO_VALUE4"] = "";
            dr["ADD_INFO_CODE5"] = "";
            dr["ADD_INFO_VALUE5"] = "";
            dr["ADD_IDCARD_NO"] = "";
            dr["ADD_IDCARD_VILLAGE"] = "";
            dr["ADD_IDCARD_NO_MU"] = "";
            dr["ADD_IDCARD_ALLEY"] = "";
            dr["ADD_IDCARD_ROAD"] = "";
            dr["ADD_IDCARD_DISTRICT"] = "";
            dr["ADD_IDCARD_COUNTY"] = "";
            dr["ADD_IDCARD_PROVINCE"] = "";
            dr["ADD_IDCARD_ZIPCODE"] = "";
            dr["ADD_CURRENT_NO_ADD"] = item.AddressNo;
            dr["ADD_CURRENT_VILLAGE"] = item.AddressVillage;
            dr["ADD_CURRENT_NO_MU"] = item.AddressMoo;
            dr["ADD_CURRENT_ALLEY"] = item.AddressSoi;
            dr["ADD_CURRENT_ROAD"] = item.AddressRoad;
            dr["ADD_CURRENT_DISTRICT"] = item.AddressSubDistrict;
            dr["ADD_CURRENT_COUNTY"] = item.AddressDistrict;
            dr["ADD_CURRENT_PROVINCE"] = item.AddressProvince;
            dr["ADD_CURRENT_ZIPCODE"] = item.AddressPostalCode;
            dr["ADD_POLICY_NO"] = item.ShippingAddressNo;
            dr["ADD_POLICY_VILLAGE"] = item.ShippingAddressVillage;
            dr["ADD_POLICY_NO_MU"] = item.ShippingAddressMoo;
            dr["ADD_POLICY_ALLEY"] = item.ShippingAddressSoi;
            dr["ADD_POLICY_ROAD"] = item.ShippingAddressRoad;
            dr["ADD_POLICY_DISTRICT"] = item.ShippingAddressDistrict;
            dr["ADD_POLICY_COUNTY"] = item.ShippingAddressSubDistrict;
            dr["ADD_POLICY_PROVINCE"] = item.ShippingAddressProvince;
            dr["ADD_POLICY_ZIPCODE"] = item.ShippingAddressPostalCode;
            if (!item.BillingIsChecked) {
                dr["TAX_IS_INSURED"] = "N";
                dr["TAX_PERSON_TYPE"] = "P";
                dr["TAX_OCCUPATION"] = (!string.IsNullOrEmpty(item.PolicyHolderJob) ? item.PolicyHolderJob : "อื่นๆ");
                dr["TAX_PERSON_CARD_TYPE"] = "0" + item.PolicyHolderCardType;
                dr["TAX_PERSON_CARD_ID"] = item.PolicyHolderIdCard;
                dr["TAX_ID"] = "";
                dr["ADD_INVOICE_NAME"] = item.PolicyHolderTitleText + " " + item.PolicyHolderName + " " + item.PolicyHolderSurname;
                dr["ADD_INVOICE_NO_ADD"] = item.BillingAddressNo;
                dr["ADD_INVOICE_VILLAGE"] = item.BillingAddressVillage;
                dr["ADD_INVOICE_NO_MU"] = item.BillingAddressMoo;
                dr["ADD_INVOICE_ALLEY"] = item.BillingAddressSoi;
                dr["ADD_INVOICE_ROAD"] = item.BillingAddressRoad;
                dr["ADD_INVOICE_DISTRICT"] = item.BillingAddressSubDistrict;
                dr["ADD_INVOICE_AMPHUR"] = item.BillingAddressDistrict;
                dr["ADD_INVOICE_PROVINCE"] = item.BillingAddressProvince;
                dr["ADD_INVOICE_ZIPCODE"] = item.BillingAddressPostalCode;
            } else {
                dr["TAX_IS_INSURED"] = "Y";
                dr["TAX_PERSON_TYPE"] = "";
                dr["TAX_OCCUPATION"] = "";
                dr["TAX_PERSON_CARD_TYPE"] = "";
                dr["TAX_PERSON_CARD_ID"] = "";
                dr["TAX_ID"] = "";
                dr["ADD_INVOICE_NAME"] = "";
                dr["ADD_INVOICE_NO_ADD"] = "";
                dr["ADD_INVOICE_VILLAGE"] = "";
                dr["ADD_INVOICE_NO_MU"] = "";
                dr["ADD_INVOICE_ALLEY"] = "";
                dr["ADD_INVOICE_ROAD"] = "";
                dr["ADD_INVOICE_DISTRICT"] = "";
                dr["ADD_INVOICE_AMPHUR"] = "";
                dr["ADD_INVOICE_PROVINCE"] = "";
                dr["ADD_INVOICE_ZIPCODE"] = "";
            }
            dr["PAY_CREDIT_TERM_TYPE"] = "ปกติ";
            dr["PAY_INSTALLMENT"] = order.PaymentInstallment;
            dr["PAY_TYPE"] = FormatPaymentChannel(order.PaymentChannel);
            dr["PAY_BANK_CODE"] = NullUtils.cvString(item.PaymentBankCode);
            dr["PAY_STATUS"] = "Y";
            dr["PAY_MAILING_TYPE"] = FormatEPolicy(app.EPolicy);
            dr["PAY_POLI_NO"] = app.Order.PolicyNo;
            dr["PAY_POLICY_STATUS"] = "1";
            dr["PAY_GATEWAY_CODE"] = "2C2P";
            dr["PAY_REF_NO"] = item.PaymentReference;
            dr["PAY_PAID_DATE"] = FormatDate(item.PaymentDate);
            dr["AUTO_DOC_FLAG"] = (String.IsNullOrEmpty(item.PolicyNo) ? "N" : "Y");
            dr["AUTO_RV_FLAG"] = "Y";
            dr["PAYIN_DATE"] = FormatDate(item.PaymentDate);
            dr["TRANS_CODE"] = "RPM";
            dr["AGREEMENT_PREMIUM"] = 0;
            return dr;
        }
        
        protected string FormatCarTypeTBroker(int value) {
            switch(value) {
                case (int)ProductMotorVehicleUsage.Normal_Commercial: return "2.10";
                case (int)ProductMotorVehicleUsage.Normal_Personal: return "1.10";
                case (int)ProductMotorVehicleUsage.Passenger_Personal: return "1.20A";
                case (int)ProductMotorVehicleUsage.Pickup_Commercial: return "2.40A";
            }
            return "";
        }

        protected string FormatPaymentChannel(PaymentChannel value) {
            switch(value) {
                case PaymentChannel.BillPayment: return "3";
                case PaymentChannel.CreditCard: return "5";
                case PaymentChannel.DebitCard: return "5";
                case PaymentChannel.IPP: return "5"; ///TODO: Check
                case PaymentChannel.Cash: return "3";
            }
            return "5";
        }

        protected string FormatEPolicy(int val) {
            if (val == 1) return "2";
            return "1";
        }

        protected string FormatDate(DateTime date) {
            //if (date.Year < 1800) return "";
            return date.ToString("dd/MM/") + date.Year;
        }

        protected string FormatDateTime(DateTime date) {
            //if (date.Year < 1800) return "";
            return date.ToString("dd/MM/") + date.Year + " " + date.ToString("HH:mm:ss");
        }

        protected string FormatDateNull() {
            return ""; // "01/01/1790";
        }

        protected string GetCompanyCorporateCode(string code) {
            CompanyRepository companyRepo = new CompanyRepository();
            var item = companyRepo.GetCompanyByCode(code);
            if (item != null) return item.CoCode;
            return code;
        }

        //public class DispatchMessageInspector : IDispatchMessageInspector {
        //    public object AfterReceiveRequest(ref System.ServiceModel.Channels.Message request, IClientChannel channel, InstanceContext instanceContext) {
        //        MessageBuffer buffer = request.CreateBufferedCopy(Int32.MaxValue);
        //        request = buffer.CreateMessage();                
        //        LogMessage("Received:\n{0}", buffer.CreateMessage().ToString());
        //        return null;
        //    }

        //    public void BeforeSendReply(ref System.ServiceModel.Channels.Message reply, object correlationState) {
        //        MessageBuffer buffer = reply.CreateBufferedCopy(Int32.MaxValue);
        //        reply = buffer.CreateMessage();
        //        LogMessage("Sending:\n{0}", buffer.CreateMessage().ToString());
        //    }
        //}

        public class ClientMessageInspector : IClientMessageInspector {
            public string LogPath { get; set; }
            public string ActionName { get; set; }
            public DateTime LogDate { get; set; }
            private System.Globalization.CultureInfo ci { get; set; }

            public ClientMessageInspector(string logPath, string actionName, DateTime logDate) {
                LogPath = logPath;
                ActionName = actionName;
                LogDate = logDate;
                ci = new System.Globalization.CultureInfo("en-GB");
            }

            void IClientMessageInspector.AfterReceiveReply(ref System.ServiceModel.Channels.Message reply, object correlationState) {
                MessageBuffer buffer = reply.CreateBufferedCopy(Int32.MaxValue);
                reply = buffer.CreateMessage();
                System.ServiceModel.Channels.Message m = buffer.CreateMessage();
                StringBuilder sb = new StringBuilder();
                using (System.Xml.XmlWriter xw = System.Xml.XmlWriter.Create(sb)) {
                    m.WriteMessage(xw);
                    xw.Close();
                }
                LogMessage("[RESPONSE]" + Environment.NewLine + sb.ToString() + Environment.NewLine);
            }

            object IClientMessageInspector.BeforeSendRequest(ref System.ServiceModel.Channels.Message request, IClientChannel channel) {
                MessageBuffer buffer = request.CreateBufferedCopy(Int32.MaxValue);
                request = buffer.CreateMessage();
                LogMessage("[REQUEST]" + Environment.NewLine + buffer.CreateMessage().ToString() + Environment.NewLine);
                return null;
            }

            void LogMessage(string val) {
                var date = LogDate;
                FileUtils.SaveTextAppend(date.ToString("dd/MM/yyyy HH:mm:ss", ci) + "\t" + val + Environment.NewLine, LogPath + date.ToString("yyyyMMdd", ci) + "_" + ActionName + ".log");
            }
        }

        public class LogEndpointBehavior : IEndpointBehavior {
            public string LogPath { get; set; }
            public string ActionName { get; set; }
            public DateTime LogDate { get; set; }

            public LogEndpointBehavior(string logPath, string actionName, DateTime logDate) {
                LogPath = logPath;
                ActionName = actionName;
                LogDate = logDate;
            }

            public void AddBindingParameters(ServiceEndpoint endpoint, System.ServiceModel.Channels.BindingParameterCollection bindingParameters) {
                // No implementation necessary  
            }

            public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime) {
                clientRuntime.MessageInspectors.Add(new ClientMessageInspector(LogPath, ActionName, LogDate));
            }

            public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher) {
                // No implementation necessary  
            }

            public void Validate(ServiceEndpoint endpoint) {
                // No implementation necessary  
            }
        }

    }
}
