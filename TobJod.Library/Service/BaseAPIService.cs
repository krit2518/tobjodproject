﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Service {
    public class BaseAPIService {

        protected bool DisplayAsTitle { get; set; }

        public Tuple<ActionResultStatus, Dictionary<ProductCategoryKey, object>> GenerateObject(List<int> orderIdList) {
            ActionResultStatus result = new ActionResultStatus();
            Dictionary<ProductCategoryKey, object> list = new Dictionary<ProductCategoryKey, object>();
            try {
                OrderRepository orderRepository = new OrderRepository();
                ProductApplicationRepository appRepository = new ProductApplicationRepository();
                ProductRepository productRepository = new ProductRepository();

                Dictionary<ProductCategoryKey, List<OrderApplicationProduct>> OrderApplicationProductList = new Dictionary<ProductCategoryKey, List<OrderApplicationProduct>>();


                foreach (var orderId in orderIdList) {
                    Order order = orderRepository.GetOrder_Admin(orderId);
                    if (order == null) continue;
                    ProductApplication app = appRepository.GetProductApplicationForm(order.ProductApplicationId);
                    if (app == null) continue;
                    Product product = productRepository.GetProduct_Admin(app.ProductId);
                    if (product == null) continue;
                    switch (product.CategoryId) {
                        case ProductCategoryKey.Motor:
                            product.ProductMotorPremium = new List<ProductMotorPremium>();
                            product.ProductMotorPremium.Add(productRepository.GetProductMotorPremium_Admin(app.PremiumId.ToString()));
                            break;
                        case ProductCategoryKey.Home:
                            product.ProductHomePremium = new List<ProductHomePremium>();
                            product.ProductHomePremium.Add(productRepository.GetProductHomePremium_Admin(app.PremiumId.ToString()));
                            break;
                        case ProductCategoryKey.PA:
                            product.ProductPAPremium = new List<ProductPAPremium>();
                            product.ProductPAPremium.Add(productRepository.GetProductPAPremium_Admin(app.PremiumId.ToString()));
                            break;
                        case ProductCategoryKey.PH:
                            product.ProductPHPremium = new List<ProductPHPremium>();
                            product.ProductPHPremium.Add(productRepository.GetProductPHPremium_Admin(app.PremiumId.ToString()));
                            break;
                        case ProductCategoryKey.TA:
                            product.ProductTAPremium = new List<ProductTAPremium>();
                            product.ProductTAPremium.Add(productRepository.GetProductTAPremium_Admin(app.PremiumId.ToString()));
                            break;
                    }


                    OrderApplicationProduct orderApp = OrderApplicationProduct.Map(order);
                    ApplicationProduct appProduct = ApplicationProduct.Map(app);
                    appProduct.Product = product;
                    orderApp.ApplicationProduct = new List<ApplicationProduct>() { appProduct };

                    if (!OrderApplicationProductList.ContainsKey(product.CategoryId)) OrderApplicationProductList.Add(product.CategoryId, new List<OrderApplicationProduct>());
                    OrderApplicationProductList[product.CategoryId].Add(orderApp);

                    if (order.OrderProductMotorPremium != null && order.OrderProductMotorPremium.BuyCompulsory) {
                        if (!OrderApplicationProductList.ContainsKey(ProductCategoryKey.PRB)) OrderApplicationProductList.Add(ProductCategoryKey.PRB, new List<OrderApplicationProduct>());
                        OrderApplicationProductList[ProductCategoryKey.PRB].Add(orderApp);
                    }
                }

                foreach (var orderApp in OrderApplicationProductList) {
                    switch (orderApp.Key) {
                        case ProductCategoryKey.Motor:
                            list.Add(orderApp.Key, GetPartnerApplicationMotor(orderApp.Value));
                            break;
                        case ProductCategoryKey.PRB:
                            list.Add(orderApp.Key, GetPartnerApplicationMotor(orderApp.Value));
                            break;
                        case ProductCategoryKey.PA:
                            list.Add(orderApp.Key, GetPartnerApplicationPA(orderApp.Value));
                            break;
                        case ProductCategoryKey.PH:
                            list.Add(orderApp.Key, GetPartnerApplicationPH(orderApp.Value));
                            break;
                        case ProductCategoryKey.TA:
                            list.Add(orderApp.Key, GetPartnerApplicationTA(orderApp.Value));
                            break;
                    }
                }
                result.Success = true;
            } catch (Exception ex) {
                result.Success = false;
                result.ErrorCode = 500;
                result.ErrorMessage = ex.ToString();
            }
            return Tuple.Create(result, list);
        }

        protected List<PartnerApplication> GetPartnerApplication(List<OrderApplicationProduct> order) {
            List<PartnerApplication> list = new List<PartnerApplication>();
            foreach (var item in order) {
                list.Add(GetPartnerApplication(item));
            }
            return list;
        }

        protected List<PartnerApplicationMotor> GetPartnerApplicationMotor(List<OrderApplicationProduct> order) {
            List<PartnerApplicationMotor> list = new List<PartnerApplicationMotor>();
            foreach (var item in order) {
                list.Add(GetPartnerApplicationMotor(item));
            }
            return list;
        }

        protected List<PartnerApplicationHome> GetPartnerApplicationHome(List<OrderApplicationProduct> order) {
            List<PartnerApplicationHome> list = new List<PartnerApplicationHome>();
            foreach (var item in order) {
                list.Add(GetPartnerApplicationHome(item));
            }
            return list;
        }

        protected List<PartnerApplicationPA> GetPartnerApplicationPA(List<OrderApplicationProduct> order) {
            List<PartnerApplicationPA> list = new List<PartnerApplicationPA>();
            foreach (var item in order) {
                list.Add(GetPartnerApplicationPA(item));
            }
            return list;
        }

        protected List<PartnerApplicationPH> GetPartnerApplicationPH(List<OrderApplicationProduct> order) {
            List<PartnerApplicationPH> list = new List<PartnerApplicationPH>();
            foreach (var item in order) {
                list.Add(GetPartnerApplicationPH(item));
            }
            return list;
        }

        protected List<PartnerApplicationTA> GetPartnerApplicationTA(List<OrderApplicationProduct> order) {
            List<PartnerApplicationTA> list = new List<PartnerApplicationTA>();
            foreach (var item in order) {
                list.Add(GetPartnerApplicationTA(item));
            }
            return list;
        }

        protected PartnerApplication GetPartnerApplication(OrderApplicationProduct order) {
            PartnerApplication item = new PartnerApplication();
            ApplicationProduct app = order.ApplicationProduct[0];
            ProductApplicationPolicyHolder policyHolder = (app.ProductApplicationPolicyHolders == null ? new ProductApplicationPolicyHolder() : app.ProductApplicationPolicyHolders[0]);

            MasterOptionRepository masterRepo = new MasterOptionRepository();
            CountryRepository countryRepo = new CountryRepository();
            ProductRepository productRepo = new ProductRepository();
            BankRepository bankRepo = new BankRepository();

            item.OrderId = order.OrderCode.ToString();
            item.CANo = NullUtils.cvString(order.CANo);
            item.PolicyNo = NullUtils.cvString(order.PolicyNo);
            item.CustomerType = "01";
            item.CompanyRegistrationNo = "";

            item.PolicyHolderIdCard = policyHolder.IDCard;
            item.PolicyHolderName = policyHolder.Name;
            item.PolicyHolderSurname = policyHolder.Surname;
            item.PolicyHolderNationality = countryRepo.GetCountry_Admin(policyHolder.NationalId)?.TitleTH ?? "";
            item.PolicyHolderBirthday = policyHolder.Birthday;
            item.PolicyHolderAge = policyHolder.Age.ToString();
            item.PolicyHolderSex = ((Sex)policyHolder.Sex).ToString();
            item.PolicyHolderTitleText = masterRepo.GetMasterOption_Admin(policyHolder.TitleId)?.TitleTH ?? "";
            if (DisplayAsTitle) {
                item.PolicyHolderMaritalStatus = policyHolder.MaritalStatus.ToString();
                //try { item.PolicyHolderMaritalStatus = ValueUtils.GetEnumDisplayName((MaritalStatus)policyHolder.MaritalStatus); } catch { item.PolicyHolderMaritalStatus = ""; }
                item.PolicyHolderTitle = item.PolicyHolderTitleText;
                item.BeneficiaryRelationship = masterRepo.GetMasterOption_Admin(NullUtils.cvInt(policyHolder.BeneficiaryRelationship))?.TitleTH ?? "";
            } else { 
                item.PolicyHolderMaritalStatus = policyHolder.MaritalStatus.ToString();
                item.PolicyHolderTitle = masterRepo.GetMasterOption_Admin(policyHolder.TitleId)?.Code ?? "";
                item.BeneficiaryRelationship = masterRepo.GetMasterOption_Admin(NullUtils.cvInt(policyHolder.BeneficiaryRelationship))?.Code ?? "";
            }
            item.PolicyHolderJob = masterRepo.GetMasterOption_Admin(policyHolder.CareerId)?.TitleTH ?? "";
            item.PolicyHolderCardType = policyHolder.IDType.ToString();
            item.PolicyHolderEmail = policyHolder.Email;
            item.PolicyHolderTelephone = policyHolder.Telephone;
            item.PolicyHolderMobilePhone = policyHolder.MobilePhone;
            item.PolicyHolderFacebook = policyHolder.Facebook;
            item.PolicyHolderInstagram = policyHolder.Instagram;
            item.PolicyHolderLineId = policyHolder.LineId;

            item.PolicyHolder = new List<PartnerApplicationPolicyHolder>();
            if (app.ProductApplicationPolicyHolders != null && app.ProductApplicationPolicyHolders.Count > 1) {
                int i = 1;
                foreach (var ph in app.ProductApplicationPolicyHolders) {
                    var marital = "";
                    if (DisplayAsTitle) {
                        try { marital = ValueUtils.GetEnumDisplayName((MaritalStatus)ph.MaritalStatus); } catch { item.PolicyHolderMaritalStatus = ""; }
                    } else {
                        marital = ph.MaritalStatus.ToString();
                    }
                    item.PolicyHolder.Add(new PartnerApplicationPolicyHolder() {
                        Age = ph.Age,
                        Birthday = ph.Birthday,
                        Email = ph.Email,
                        IDCardNo = ph.IDCard,
                        IDCardType = ph.IDType.ToString(),
                        Job = masterRepo.GetMasterOption_Admin(ph.CareerId)?.TitleTH ?? "",
                        MaritalStatus = marital,
                        Name = ph.Name,
                        Nationality = countryRepo.GetCountry_Admin(ph.NationalId)?.TitleTH ?? "",
                        No = i,
                        Sex = ((Sex)ph.Sex).ToString(),
                        Surname = ph.Surname,
                        Title = masterRepo.GetMasterOption_Admin(ph.TitleId)?.TitleTH ?? "",
                        BeneficiaryName = ph.BeneficiaryName,
                        BeneficiarySurname = ph.BeneficiarySurname,
                        BeneficiaryIdType = ph.BeneficiaryIDType.ToString(),
                        BeneficiaryIdCard = ph.BeneficiaryIDCard,
                        BeneficiaryRelationship = (DisplayAsTitle ? masterRepo.GetMasterOption_Admin(NullUtils.cvInt(ph.BeneficiaryRelationship))?.TitleTH : masterRepo.GetMasterOption_Admin(NullUtils.cvInt(ph.BeneficiaryRelationship))?.Code) ?? "",
                        BeneficiaryTelephone = ph.BeneficiaryMobilePhone
                });
                    i++;
                }
            }
            
            item.Beneficiary = policyHolder.BeneficiaryName + " " + policyHolder.BeneficiarySurname;
            item.BeneficiaryName = policyHolder.BeneficiaryName;
            item.BeneficiarySurname = policyHolder.BeneficiarySurname;
            item.BeneficiaryIdType = policyHolder.BeneficiaryIDType.ToString();
            item.BeneficiaryIdCard = policyHolder.BeneficiaryIDCard;
            //item.BeneficiaryRelationship = (DisplayAsTitle ? masterRepo.GetMasterOption_Admin(NullUtils.cvInt(policyHolder.BeneficiaryRelationship))?.TitleTH : masterRepo.GetMasterOption_Admin(NullUtils.cvInt(policyHolder.BeneficiaryRelationship))?.Code) ?? "";
            item.BeneficiaryTelephone = policyHolder.BeneficiaryMobilePhone;
            item.PolicyActiveDate = app.ActiveDate;
            item.PolicyExpireDate = app.ExpireDate;
            item.SumInsured = 0;
            item.NetPremium = 0;
            item.Premium = 0;
            item.TransactionDate = DateTime.Now;// order.CreateDate;
            item.TransactionBy = "บริษัทธนชาตโบรคเกอร์";
            item.InsuranceCompany = app.Product.Company;
            item.InsuranceCompanyCode = app.Product.CompanyCode;
            item.InsuranceCategory = ValueUtils.GetEnumDisplayName(app.Product.SubCategoryId);
            item.AddressNo = app.AddressNo;
            item.AddressMoo = app.AddressMoo;
            item.AddressVillage = app.AddressVillage;
            item.AddressFloor = app.AddressFloor;
            item.AddressSoi = app.AddressSoi;
            item.AddressRoad = app.AddressRoad;
            //item.AddressSubdistrict = app.AddressSubDistrict;
            //item.AddressDistrict = app.AddressDistrict;
            //item.AddressProvince = app.AddressProvince;
            item.AddressPostalCode = app.AddressPostalCode;
            item.ShippingIsChecked = app.ShippingAddressIsChecked == YesNo.Yes;
            item.ShippingAddressNo = app.ShippingAddressNo;
            item.ShippingAddressMoo = app.ShippingAddressMoo;
            item.ShippingAddressVillage = app.ShippingAddressVillage;
            item.ShippingAddressFloor = app.ShippingAddressFloor;
            item.ShippingAddressSoi = app.ShippingAddressSoi;
            item.ShippingAddressRoad = app.ShippingAddressRoad;
            //item.ShippingAddressSubdistrict = app.ShippingAddressSubDistrict;
            //item.ShippingAddressDistrict = app.ShippingAddressDistrict;
            //item.ShippingAddressProvince = app.ShippingAddressProvince;
            item.ShippingAddressPostalCode = app.ShippingAddressPostalCode;
            item.BillingIsChecked = app.BillingAddressIsChecked == YesNo.Yes;
            item.BillingAddressNo = app.BillingAddressNo;
            item.BillingAddressMoo = app.BillingAddressMoo;
            item.BillingAddressVillage = app.BillingAddressVillage;
            item.BillingAddressFloor = app.BillingAddressFloor;
            item.BillingAddressSoi = app.BillingAddressSoi;
            item.BillingAddressRoad = app.BillingAddressRoad;
            //item.BillingAddressSubdistrict = app.BillingAddressSubDistrict;
            //item.BillingAddressDistrict = app.BillingAddressDistrict;
            //item.BillingAddressProvince = app.BillingAddressProvince;
            item.BillingAddressPostalCode = app.BillingAddressPostalCode;
            item.PaymentChannel = ValueUtils.GetEnumDisplayName(order.PaymentChannel);
            item.PaymentDate = order.PaymentDate;
            item.PaymentTotal = order.PaymentTotal;
            item.PaymentReference = order.PaidRef;
            item.ProductCategory = app.Product.CategoryId.ToString();
            item.ProductCode = app.Product.InsuranceProductCode;
            item.ProductName = app.Product.TitleTH;
            item.ProductPackageCode = productRepo.GetProductSubCategoryCode((int)app.Product.SubCategoryId)?.ToString();
            //var bank = bankRepo.GetBankByPaymentGatewayCode(order.PaidAgent);
            if (!string.IsNullOrEmpty(order.Bank)) {
                item.PaymentBankCode = order.Bank;
            } else {
                item.PaymentBankCode = System.Configuration.ConfigurationManager.AppSettings["Payment_Bank_Code"];
            }

            item.ContactName = app.ContactName;
            item.ContactSurname = app.ContactSurname;
            item.ContactTel = app.ContactTelephone;
            item.ContactEmail = app.ContactEmail;

            item.BuyerName = app.BuyerName;
            item.BuyerSurname = app.BuyerSurname;
            item.BuyerTel = app.BuyerTelephone;
            item.BuyerEmail = app.BuyerEmail;

            //var bank = bankRepo.GetBankByPaymentGatewayCode(order.PaymentChannel);

            ProvinceRepository provinceRepo = new ProvinceRepository();
            DistrictRepository districtRepo = new DistrictRepository();
            SubDistrictRepository subDistrictRepo = new SubDistrictRepository();
            if (DisplayAsTitle) {
                item.AddressSubDistrict = subDistrictRepo.GetSubDistrict_Admin(NullUtils.cvInt(app.AddressSubDistrict))?.TitleTH;
                item.AddressDistrict = districtRepo.GetDistrict_Admin(NullUtils.cvInt(app.AddressDistrict))?.TitleTH;
                item.AddressProvince = provinceRepo.GetProvince_Admin(NullUtils.cvInt(app.AddressProvince))?.TitleTH;
                item.ShippingAddressSubDistrict = subDistrictRepo.GetSubDistrict_Admin(NullUtils.cvInt(app.ShippingAddressSubDistrict))?.TitleTH;
                item.ShippingAddressDistrict = districtRepo.GetDistrict_Admin(NullUtils.cvInt(app.ShippingAddressDistrict))?.TitleTH;
                item.ShippingAddressProvince = provinceRepo.GetProvince_Admin(NullUtils.cvInt(app.ShippingAddressProvince))?.TitleTH;
                item.BillingAddressSubDistrict = subDistrictRepo.GetSubDistrict_Admin(NullUtils.cvInt(app.BillingAddressSubDistrict))?.TitleTH;
                item.BillingAddressDistrict = districtRepo.GetDistrict_Admin(NullUtils.cvInt(app.BillingAddressDistrict))?.TitleTH;
                item.BillingAddressProvince = provinceRepo.GetProvince_Admin(NullUtils.cvInt(app.BillingAddressProvince))?.TitleTH;
            } else {
                item.AddressSubDistrict = subDistrictRepo.GetSubDistrict_Admin(NullUtils.cvInt(app.AddressSubDistrict))?.Code;
                item.AddressDistrict = districtRepo.GetDistrict_Admin(NullUtils.cvInt(app.AddressDistrict))?.Code;
                item.AddressProvince = provinceRepo.GetProvince_Admin(NullUtils.cvInt(app.AddressProvince))?.Code;
                item.ShippingAddressSubDistrict = subDistrictRepo.GetSubDistrict_Admin(NullUtils.cvInt(app.ShippingAddressSubDistrict))?.Code;
                item.ShippingAddressDistrict = districtRepo.GetDistrict_Admin(NullUtils.cvInt(app.ShippingAddressDistrict))?.Code;
                item.ShippingAddressProvince = provinceRepo.GetProvince_Admin(NullUtils.cvInt(app.ShippingAddressProvince))?.Code;
                item.BillingAddressSubDistrict = subDistrictRepo.GetSubDistrict_Admin(NullUtils.cvInt(app.BillingAddressSubDistrict))?.Code;
                item.BillingAddressDistrict = districtRepo.GetDistrict_Admin(NullUtils.cvInt(app.BillingAddressDistrict))?.Code;
                item.BillingAddressProvince = provinceRepo.GetProvince_Admin(NullUtils.cvInt(app.BillingAddressProvince))?.Code;
            }

            return item;
        }

        protected PartnerApplicationMotor GetPartnerApplicationMotor(OrderApplicationProduct order) {
            PartnerApplicationMotor item = PartnerApplicationMotor.Map(GetPartnerApplication(order));
            ApplicationProduct app = order.ApplicationProduct[0];
            ProductApplicationMotor appMotor = app.ProductApplicationMotor;
            ProductApplicationPolicyHolder policyHolder = (app.ProductApplicationPolicyHolders == null && app.ProductApplicationPolicyHolders.Count > 0 ? new ProductApplicationPolicyHolder() : app.ProductApplicationPolicyHolders[0]);
            ProductMotorPremium premium = app.Product.ProductMotorPremium[0];
            //OrderProductMotorPremium orderProductMotorPremium = order.OrderProductMotorPremium;
            //if (orderProductMotorPremium == null) orderProductMotorPremium = new OrderProductMotorPremium(); //DUMMY

            MasterOptionRepository masterRepo = new MasterOptionRepository();
            CountryRepository countryRepo = new CountryRepository();
            CarRepository carRepo = new CarRepository();

            item.SumInsured = premium.SumInsured;
            item.NetPremium = premium.NetPremium;
            item.Premium = premium.Premium;

            item.BuyCompulsory = (appMotor.Compulsory == 1);

            if (item.BuyCompulsory) {
                var compulsoryPremium = GetCompulsoryPremium(premium.CompulsoryPremium, item.InsuranceCompanyCode);
                if (compulsoryPremium == null || compulsoryPremium.ProductPRB == null || compulsoryPremium.ProductPRB.Premium == 0) {
                    compulsoryPremium = new Product();
                    compulsoryPremium.ProductPRB = new ProductPRB();
                    compulsoryPremium.ProductPRB.Premium = item.CompulsoryPremium;
                }
                item.CompulsoryDuty = compulsoryPremium.ProductPRB.Duty;
                item.CompulsoryVat = compulsoryPremium.ProductPRB.Vat;
                item.CompulsoryNetPremium = compulsoryPremium.ProductPRB.NetPremium;
                item.CompulsoryPremium = premium.CompulsoryPremium;
                item.CompulsoryCode = compulsoryPremium.InsuranceProductCode;
            } else {
                item.CompulsoryNetPremium = 0;
                item.CompulsoryPremium = 0;
            }
            item.TotalPrice = (item.BuyCompulsory ? premium.DisplayCombinePremium : premium.DisplayPremium);
            item.EPolicy = premium.EPolicy;

            item.PolicyHolderDriverLicenseNo = (app.ProductApplicationDrivers == null || app.ProductApplicationDrivers.Count < 1 ? "" : app.ProductApplicationDrivers[0].RefIDCard);
            var carModelInfo = carRepo.GetCarModel_Admin(NullUtils.cvInt(appMotor.CarModelId));

            item.CompulsoryActiveDate = (item.BuyCompulsory ? app.ActiveDate : DateTime.MinValue); //TODO
            item.CompulsoryExpireDate = (item.BuyCompulsory ? app.ExpireDate : DateTime.MinValue); //TODO
            item.CarPlateNo = appMotor.CarPlateNo1 + appMotor.CarPlateNo2;
            item.CarPlateNo1 = appMotor.CarPlateNo1;
            item.CarPlateNo2 = appMotor.CarPlateNo2;
            if (DisplayAsTitle) {
                item.CarPlateProvince = masterRepo.GetMasterOption_Admin(appMotor.CarProvinceId)?.TitleTH ?? "";
                item.CarBrand = carRepo.GetCarBrand_Admin(NullUtils.cvInt(appMotor.CarBrand))?.TitleTH ?? "";
            } else {
                item.CarPlateProvince = masterRepo.GetMasterOption_Admin(appMotor.CarProvinceId)?.Code ?? "";
                item.CarBrand = carRepo.GetCarBrand_Admin(NullUtils.cvInt(appMotor.CarBrand))?.MapName ?? "";
            }
            item.CarFamily = carRepo.GetCarFamily_Admin(NullUtils.cvInt(appMotor.CarFamily))?.TitleTH;
            item.CarModel = carModelInfo?.TitleTH ?? "";
            item.CarYear = (carModelInfo == null? 0 : carModelInfo.Year);
            item.CarRegisterYear = appMotor.CarRegisterYear.ToString();
            item.CarCC = carModelInfo?.CC.ToString();
            item.CarCCTV = appMotor.CarCCTV.ToString();
            item.CarGarageType = premium.GarageType.ToString().Substring(0,1);
            item.CarSerialNo = appMotor.CarSerialNo;
            item.CarEngineNo = appMotor.CarEngineNo;
            item.CarAccessory = appMotor.CarAccessories;
            item.CarAccessoryPrice = appMotor.CarAccessoryValue;
            //item.VehicleUsage = ValueUtils.GetEnumDisplayName((ProductMotorVehicleUsage)appMotor.CarUsageType);
            //item.VehicleUsage = (string.Equals(item.VehicleUsage, "0") ? "" : item.VehicleUsage);
            item.VehicleUsage = (appMotor.CarUsageType > 0 ? appMotor.CarUsageType.ToString() : FormatCarType(NullUtils.cvInt(appMotor.CarUsageTypeCode)));


            item.BuyCompulsory = appMotor.Compulsory > 0;

            item.Driver = new List<PartnerApplicationDriver>();
            if (app.ProductApplicationDrivers != null && app.ProductApplicationDrivers.Count > 0) {
                int index = 1;
                foreach (var driver in app.ProductApplicationDrivers) {
                    if (driver.Id > 0) {
                        item.Driver.Add(new PartnerApplicationDriver() {
                            No = index,
                            Title = masterRepo.GetMasterOption_Admin(driver.TitleId)?.TitleTH,
                            Name = driver.Name,
                            Surname = driver.Surname,
                            IDCardType = driver.IDType.ToString(),
                            IDCardNo = driver.IDCard,
                            DriverLicenseNo = driver.RefIDCard,
                            Nationality = countryRepo.GetCountry_Admin(driver.NationalId)?.TitleTH,
                            Birthday = driver.Birthday
                        });
                    }
                    index++;
                }
            }

            return item;
        }

        protected PartnerApplicationMotor GetPartnerApplicationPRB(OrderApplicationProduct order) {
            PartnerApplicationMotor item = PartnerApplicationMotor.Map(GetPartnerApplication(order));
            ApplicationProduct app = order.ApplicationProduct[0];
            ProductApplicationMotor appMotor = app.ProductApplicationMotor;
            ProductApplicationPolicyHolder policyHolder = (app.ProductApplicationPolicyHolders == null && app.ProductApplicationPolicyHolders.Count > 0 ? new ProductApplicationPolicyHolder() : app.ProductApplicationPolicyHolders[0]);
            ProductPRB premium = app.Product.ProductPRB;
            //OrderProductMotorPremium orderProductMotorPremium = order.OrderProductMotorPremium;
            //if (orderProductMotorPremium == null) orderProductMotorPremium = new OrderProductMotorPremium(); //DUMMY

            MasterOptionRepository masterRepo = new MasterOptionRepository();
            CountryRepository countryRepo = new CountryRepository();
            CarRepository carRepo = new CarRepository();

            item.BuyCompulsory = false;

            item.SumInsured = 0;
            item.NetPremium = premium.NetPremium;
            item.Premium = premium.Premium;
            item.CompulsoryPremium = 0;
            item.TotalPrice = item.Premium;
            item.EPolicy = false;// premium.EPolicy;

            item.PolicyHolderDriverLicenseNo = (app.ProductApplicationDrivers == null || app.ProductApplicationDrivers.Count < 1 ? "" : app.ProductApplicationDrivers[0].RefIDCard);
            var carModelInfo = carRepo.GetCarModel_Admin(NullUtils.cvInt(appMotor.CarModelId));

            item.CompulsoryActiveDate = (item.BuyCompulsory ? app.ActiveDate : DateTime.MinValue); //TODO
            item.CompulsoryExpireDate = (item.BuyCompulsory ? app.ExpireDate : DateTime.MinValue); //TODO
            item.CarPlateNo = appMotor.CarPlateNo1 + appMotor.CarPlateNo2;
            item.CarPlateNo1 = appMotor.CarPlateNo1;
            item.CarPlateNo2 = appMotor.CarPlateNo2;
            if (DisplayAsTitle) {
                item.CarPlateProvince = masterRepo.GetMasterOption_Admin(appMotor.CarProvinceId)?.TitleTH ?? "";
                item.CarBrand = carRepo.GetCarBrand_Admin(NullUtils.cvInt(appMotor.CarBrand))?.TitleTH ?? "";
            } else {
                item.CarPlateProvince = masterRepo.GetMasterOption_Admin(appMotor.CarProvinceId)?.Code ?? "";
                item.CarBrand = carRepo.GetCarBrand_Admin(NullUtils.cvInt(appMotor.CarBrand))?.MapName ?? "";
            }
            item.CarFamily = carRepo.GetCarFamily_Admin(NullUtils.cvInt(appMotor.CarFamily))?.TitleTH;
            item.CarModel = carModelInfo?.TitleTH ?? "";
            item.CarYear = (carModelInfo == null ? 0 : carModelInfo.Year);
            item.CarRegisterYear = appMotor.CarRegisterYear.ToString();
            item.CarCC = carModelInfo?.CC.ToString();
            item.CarCCTV = appMotor.CarCCTV.ToString();
            item.CarGarageType = "";// premium.GarageType.ToString().Substring(0, 1);
            item.CarSerialNo = appMotor.CarSerialNo;
            item.CarEngineNo = appMotor.CarEngineNo;
            item.CarAccessory = appMotor.CarAccessories;
            item.CarAccessoryPrice = appMotor.CarAccessoryValue;
            //item.VehicleUsage = ValueUtils.GetEnumDisplayName((ProductMotorVehicleUsage)appMotor.CarUsageType);
            //item.VehicleUsage = (string.Equals(item.VehicleUsage, "0") ? "" : item.VehicleUsage);
            item.VehicleUsage = (appMotor.CarUsageType > 0 ? appMotor.CarUsageType.ToString() : FormatCarType(NullUtils.cvInt(appMotor.CarUsageTypeCode)));

            item.BuyCompulsory = appMotor.Compulsory > 0;

            item.Driver = new List<PartnerApplicationDriver>();
            if (app.ProductApplicationDrivers != null && app.ProductApplicationDrivers.Count > 0) {
                int index = 1;
                foreach (var driver in app.ProductApplicationDrivers) {
                    item.Driver.Add(new PartnerApplicationDriver() {
                        No = index,
                        Title = masterRepo.GetMasterOption_Admin(driver.TitleId)?.TitleTH,
                        Name = driver.Name,
                        Surname = driver.Surname,
                        IDCardType = driver.IDType.ToString(),
                        IDCardNo = driver.IDCard,
                        DriverLicenseNo = driver.RefIDCard,
                        Nationality = countryRepo.GetCountry_Admin(driver.NationalId)?.TitleTH,
                        Birthday = driver.Birthday
                    });
                    index++;
                }
            }

            return item;
        }

        protected PartnerApplicationHome GetPartnerApplicationHome(OrderApplicationProduct order) {
            PartnerApplicationHome item = PartnerApplicationHome.Map(GetPartnerApplication(order));
            ApplicationProduct app = order.ApplicationProduct[0];
            ProductApplicationHome appHome = app.ProductApplicationHome;
            ProductApplicationPolicyHolder policyHolder = (app.ProductApplicationPolicyHolders == null && app.ProductApplicationPolicyHolders.Count > 0 ? new ProductApplicationPolicyHolder() : app.ProductApplicationPolicyHolders[0]);
            ProductHomePremium premium = app.Product.ProductHomePremium[0];
            OrderProductHomePremium orderProductHomePremium = order.OrderProductHomePremium;
            if (orderProductHomePremium == null) orderProductHomePremium = new OrderProductHomePremium(); //DUMMY

            MasterOptionRepository masterRepo = new MasterOptionRepository();

            item.SumInsured = premium.SumInsured;
            item.NetPremium = premium.NetPremium;
            item.Premium = premium.Premium;
            item.EPolicy = premium.EPolicy;

            item.Size = appHome.SquareMeter;
            item.RoomAmount = appHome.RoomAmount;
            item.Floor = appHome.Floor;            
            item.ConstructionValue = appHome.ConstructionValue;
            item.FunitureValue = appHome.FunitureValue;
            item.OwnerStatus = "";

            item.PropertyAddressNo = appHome.AddressNo;
            item.PropertyAddressVillage = appHome.AddressVillage;
            item.PropertyAddressFloor = appHome.AddressFloor;
            item.PropertyAddressMoo = appHome.AddressMoo;
            item.PropertyAddressSoi = appHome.AddressSoi;
            item.PropertyAddressRoad = appHome.AddressRoad;
            item.PropertyAddressPostalCode = appHome.AddressPostalCode;

            try { item.OwnerStatus = ValueUtils.GetEnumDisplayName((BuyerStatus)appHome.BuyerStatus); } catch { }

            ProvinceRepository provinceRepo = new ProvinceRepository();
            DistrictRepository districtRepo = new DistrictRepository();
            SubDistrictRepository subDistrictRepo = new SubDistrictRepository();
            if (DisplayAsTitle) {
                item.PropertyType = masterRepo.GetMasterOption_Admin(appHome.PropertyType)?.TitleTH ?? "";
                item.ConstructionType = masterRepo.GetMasterOption_Admin(appHome.ConstructionType)?.TitleTH ?? "";
                item.UsageType = masterRepo.GetMasterOption_Admin(appHome.LocationUsage)?.TitleTH ?? "";
                item.PropertyAddressSubDistrict = subDistrictRepo.GetSubDistrict_Admin(NullUtils.cvInt(appHome.AddressSubDistrict))?.TitleTH;
                item.PropertyAddressDistrict = districtRepo.GetDistrict_Admin(NullUtils.cvInt(appHome.AddressDistrict))?.TitleTH;
                item.PropertyAddressProvince = provinceRepo.GetProvince_Admin(NullUtils.cvInt(appHome.AddressProvince))?.TitleTH;
            } else {
                item.PropertyType = masterRepo.GetMasterOption_Admin(appHome.PropertyType)?.Code ?? "";
                item.ConstructionType = masterRepo.GetMasterOption_Admin(appHome.ConstructionType)?.Code ?? "";
                item.UsageType = masterRepo.GetMasterOption_Admin(appHome.LocationUsage)?.Code ?? "";
                item.PropertyAddressSubDistrict = subDistrictRepo.GetSubDistrict_Admin(NullUtils.cvInt(appHome.AddressSubDistrict))?.Code;
                item.PropertyAddressDistrict = districtRepo.GetDistrict_Admin(NullUtils.cvInt(appHome.AddressDistrict))?.Code;
                item.PropertyAddressProvince = provinceRepo.GetProvince_Admin(NullUtils.cvInt(appHome.AddressProvince))?.Code;
            }

            return item;
        }

        protected PartnerApplicationPA GetPartnerApplicationPA(OrderApplicationProduct order) {
            PartnerApplicationPA item = PartnerApplicationPA.Map(GetPartnerApplication(order));
            ApplicationProduct app = order.ApplicationProduct[0];
            ProductPAPremium premium = app.Product.ProductPAPremium[0];

            item.SumInsured = premium.SumInsured;
            item.NetPremium = premium.NetPremium;
            item.Premium = premium.Premium;
            item.EPolicy = premium.EPolicy;

            //item.BuyerName = app.BuyerName;
            //item.BuyerSurname = app.BuyerSurname;
            //item.BuyerEmail = app.BuyerEmail;
            //item.BuyerTel = app.BuyerTelephone;
            
            return item;
        }

        protected PartnerApplicationPH GetPartnerApplicationPH(OrderApplicationProduct order) {
            PartnerApplicationPH item = PartnerApplicationPH.Map(GetPartnerApplication(order));
            ApplicationProduct app = order.ApplicationProduct[0];
            ProductApplicationPolicyHolder policyHolder = (app.ProductApplicationPolicyHolders == null && app.ProductApplicationPolicyHolders.Count > 0 ? new ProductApplicationPolicyHolder() : app.ProductApplicationPolicyHolders[0]);
            ProductPHPremium premium = app.Product.ProductPHPremium[0];

            item.SumInsured = premium.SumInsured;
            item.NetPremium = premium.NetPremium;
            item.Premium = premium.Premium;
            item.EPolicy = premium.EPolicy;

            item.Height = policyHolder.Height;
            item.Weight = policyHolder.Weight;
            return item;
        }

        protected PartnerApplicationTA GetPartnerApplicationTA(OrderApplicationProduct order) {
            PartnerApplicationTA item = PartnerApplicationTA.Map(GetPartnerApplication(order));
            ApplicationProduct app = order.ApplicationProduct[0];
            ProductApplicationTA appTA = app.ProductApplicationTA;
            ProductApplicationPolicyHolder policyHolder = (app.ProductApplicationPolicyHolders == null && app.ProductApplicationPolicyHolders.Count > 0 ? new ProductApplicationPolicyHolder() : app.ProductApplicationPolicyHolders[0]);
            ProductTAPremium premium = app.Product.ProductTAPremium[0];
            OrderProductTAPremium orderProductTAPremium = order.OrderProductTAPremium;
            if (orderProductTAPremium == null) orderProductTAPremium = new OrderProductTAPremium(); //DUMMY

            MasterOptionRepository masterRepo = new MasterOptionRepository();
            CountryRepository countryRepo = new CountryRepository();

            item.SumInsured = premium.SumInsured;
            item.NetPremium = premium.NetPremium;
            item.Premium = premium.Premium;
            item.EPolicy = premium.EPolicy;

            try { item.TripType = ValueUtils.GetEnumDisplayName((ProductTATripType)appTA.TravelType).ToString(); } catch { }
            try { item.CoverageType = ValueUtils.GetEnumDisplayName((ProductTACoverageType)appTA.CoverageType).ToString(); } catch { }
            try { item.CoverageOption = ValueUtils.GetEnumDisplayName((ProductTACoverageOption)appTA.CoverageOption).ToString(); } catch { }
            
            item.Country = countryRepo.GetCountry_Admin(appTA.Country)?.TitleTH ?? "";
            item.TotalDay = appTA.Day;
            item.TotalPerson = appTA.Persons;

            return item;
        }

        protected Product GetCompulsoryPremium(decimal premium, string companyCode) {
            ProductRepository repo = new ProductRepository();
            Product item = repo.GetProductPRBCompanyTotal_Active(premium, companyCode);
            if (item != null) return item;
            return new Product();
        }


        protected class OrderApplicationProduct : Order {
            public List<ApplicationProduct> ApplicationProduct { get; set; }

            public static OrderApplicationProduct Map(Order baseObject) {
                var serializedParent = JsonConvert.SerializeObject(baseObject);
                return JsonConvert.DeserializeObject<OrderApplicationProduct>(serializedParent);
            }
        }

        protected class ApplicationProduct : ProductApplication {
            public Product Product { get; set; }

            public static ApplicationProduct Map(ProductApplication baseObject) {
                var serializedParent = JsonConvert.SerializeObject(baseObject);
                return JsonConvert.DeserializeObject<ApplicationProduct>(serializedParent);
            }
        }

        protected class PartnerApplication {
            public string OrderId { get; set; }
            public string CANo { get; set; }
            public string PolicyNo { get; set; }
            //public string SaleChannelCode { get { return "01"; } }
            //public string SaleChannel { get { return "Digital"; } }
            public string PaymentChannel { get; set; }
            public string PaymentBankCode { get; set; }
            public string PaymentStatus { get { return "Success"; } }
            public string PaymentReference { get; set; }
            public decimal PaymentTotal { get; set; }
            [JsonConverter(typeof(CustomDateTimeConverter))]
            public DateTime PaymentDate { get; set; }
            [JsonProperty(PropertyName = "TransactionDateTime")]
            [JsonConverter(typeof(CustomDateTimeConverter))]
            public DateTime TransactionDate { get; set; }
            [JsonProperty(PropertyName = "Title")]
            public string PolicyHolderTitle { get; set; }
            public string PolicyHolderTitleText { get; set; }
            [JsonProperty(PropertyName = "Name")]
            public string PolicyHolderName { get; set; }
            [JsonProperty(PropertyName = "Surname")]
            public string PolicyHolderSurname { get; set; }
            [JsonProperty(PropertyName = "Nationality")]
            public string PolicyHolderNationality { get; set; }
            [JsonProperty(PropertyName = "Birthday")]
            [JsonConverter(typeof(CustomDateConverter))]
            public DateTime PolicyHolderBirthday { get; set; }
            [JsonProperty(PropertyName = "Age")]
            public string PolicyHolderAge { get; set; }
            [JsonProperty(PropertyName = "Sex")]
            public string PolicyHolderSex { get; set; }
            [JsonProperty(PropertyName = "IDCardType")]
            public string PolicyHolderCardType { get; set; }
            [JsonProperty(PropertyName = "IDCardNo")]
            public string PolicyHolderIdCard { get; set; }
            [JsonProperty(PropertyName = "Telelphone")]
            public string PolicyHolderTelephone { get; set; }
            [JsonProperty(PropertyName = "Mobilephone")]
            public string PolicyHolderMobilePhone { get; set; }
            [JsonProperty(PropertyName = "Email")]
            public string PolicyHolderEmail { get; set; }
            [JsonProperty(PropertyName = "Facebook")]
            //[JsonIgnore]
            public string PolicyHolderFacebook { get; set; }
            [JsonProperty(PropertyName = "Instagram")]
            //[JsonIgnore]
            public string PolicyHolderInstagram { get; set; }
            [JsonProperty(PropertyName = "LineId")]
            //[JsonIgnore]
            public string PolicyHolderLineId { get; set; }
            [JsonProperty(PropertyName = "MaritalStatus")]
            public string PolicyHolderMaritalStatus { get; set; }
            [JsonProperty(PropertyName = "Job")]
            public string PolicyHolderJob { get; set; }
            [JsonIgnore]
            public string Beneficiary { get; set; }
            public string BeneficiaryName { get; set; }
            public string BeneficiarySurname { get; set; }
            public string BeneficiaryIdType { get; set; }
            public string BeneficiaryIdCard { get; set; }
            //public string BeneficiaryPassport { get; set; }
            public string BeneficiaryRelationship { get; set; }
            public string BeneficiaryTelephone { get; set; }
            [JsonConverter(typeof(CustomDateTimeConverter))]
            public DateTime PolicyActiveDate { get; set; }
            [JsonConverter(typeof(CustomDateTimeConverter))]
            public DateTime PolicyExpireDate { get; set; }

            public string ProductCategory { get; set; }
            public string ProductPackageCode { get; set; }
            public string ProductCode { get; set; }
            public string ProductName { get; set; }
            public decimal SumInsured { get; set; }
            //[JsonIgnore]
            public decimal NetPremium { get; set; }
            public decimal Premium { get; set; }
            //[JsonIgnore]
            [JsonProperty(PropertyName = "BrokerName")]
            public string TransactionBy { get; set; }
            public string InsuranceCompanyCode { get; set; }
            //[JsonIgnore]
            public string InsuranceCompany { get; set; }
            public string InsuranceCategory { get; set; }
            [JsonIgnore]
            public string Address { get { return string.Concat(AddressNo, " ", AddressMoo, " ", AddressVillage, " ", AddressSoi, " ", AddressRoad); } }
            public string AddressNo { get; set; }
            public string AddressMoo { get; set; }
            public string AddressVillage { get; set; }
            public string AddressFloor { get; set; }
            public string AddressSoi { get; set; }
            public string AddressRoad { get; set; }
            [JsonProperty(PropertyName = "AddressSubdistrict")]
            public string AddressSubDistrict { get; set; }
            public string AddressDistrict { get; set; }
            public string AddressProvince { get; set; }
            public string AddressPostalCode { get; set; }
            [JsonIgnore]
            public string ShippingAddress { get { return string.Concat(ShippingAddressNo, " ", ShippingAddressMoo, " ", ShippingAddressVillage, " ", ShippingAddressSoi, " ", ShippingAddressRoad); } }
            public bool ShippingIsChecked { get; set; }
            public string ShippingAddressNo { get; set; }
            public string ShippingAddressMoo { get; set; }
            public string ShippingAddressVillage { get; set; }
            public string ShippingAddressFloor { get; set; }
            public string ShippingAddressSoi { get; set; }
            public string ShippingAddressRoad { get; set; }
            [JsonProperty(PropertyName = "ShippingAddressSubdistrict")]
            public string ShippingAddressSubDistrict { get; set; }
            public string ShippingAddressDistrict { get; set; }
            public string ShippingAddressProvince { get; set; }
            public string ShippingAddressPostalCode { get; set; }
            [JsonIgnore]
            public string BillingAddress { get { return string.Concat(BillingAddressNo, " ", BillingAddressMoo, " ", BillingAddressVillage, " ", BillingAddressSoi, " ", BillingAddressRoad); } }
            public bool BillingIsChecked { get; set; }
            public string BillingAddressNo { get; set; }
            public string BillingAddressMoo { get; set; }
            public string BillingAddressVillage { get; set; }
            public string BillingAddressFloor { get; set; }
            public string BillingAddressSoi { get; set; }
            public string BillingAddressRoad { get; set; }
            [JsonProperty(PropertyName = "BillingAddressSubdistrict")]
            public string BillingAddressSubDistrict { get; set; }
            public string BillingAddressDistrict { get; set; }
            public string BillingAddressProvince { get; set; }
            public string BillingAddressPostalCode { get; set; }
            public string PolicyFormat { get { return (EPolicy ? "E" : "P"); } }

            public string BuyerName { get; set; }
            public string BuyerSurname { get; set; }
            public string BuyerTel { get; set; }
            public string BuyerEmail { get; set; }

            public string ContactName { get; set; }
            public string ContactSurname { get; set; }
            public string ContactTel { get; set; }
            public string ContactEmail { get; set; }

            public string CustomerType { get; set; }
            public string CompanyRegistrationNo { get; set; }
           
            public List<PartnerApplicationPolicyHolder> PolicyHolder { get; set; }

            [JsonIgnore]
            public bool EPolicy { get; set; }
        }

        protected class PartnerApplicationPolicyHolder {
            public int No { get; set; }
            public string Title { get; set; }
            public string Name { get; set; }
            public string Surname { get; set; }
            public string Nationality { get; set; }
            [JsonConverter(typeof(CustomDateConverter))]
            public DateTime Birthday { get; set; }
            public int Age { get; set; }
            public string Sex { get; set; }
            public string MaritalStatus { get; set; }
            public string IDCardType { get; set; }
            public string IDCardNo { get; set; }
            public string Email { get; set; }
            public string Job { get; set; }
            public string BeneficiaryName { get; set; }
            public string BeneficiarySurname { get; set; }
            public string BeneficiaryIdType { get; set; }
            public string BeneficiaryIdCard { get; set; }
            public string BeneficiaryRelationship { get; set; }
            public string BeneficiaryTelephone { get; set; }
        }

        protected class PartnerApplicationDriver {
            public int No { get; set; }
            public string Title { get; set; }
            public string Name { get; set; }
            public string Surname { get; set; }
            public string Nationality { get; set; }
            [JsonConverter(typeof(CustomDateConverter))]
            public DateTime Birthday { get; set; }
            public string IDCardType { get; set; }
            public string IDCardNo { get; set; }
            public string DriverLicenseNo { get; set; }
        }

        protected class PartnerApplicationMotor : PartnerApplication {
            [JsonProperty(PropertyName = "DriverLicenseNo")]
            public string PolicyHolderDriverLicenseNo { get; set; }
            [JsonConverter(typeof(CustomDateTimeConverter))]
            public DateTime CompulsoryActiveDate { get; set; }
            [JsonConverter(typeof(CustomDateTimeConverter))]
            public DateTime CompulsoryExpireDate { get; set; }
            //[JsonIgnore]
            public string CarPlateNo1 { get; set; }
            public string CarPlateNo2 { get; set; }
            public string CarPlateNo { get; set; }
            public string CarPlateProvince { get; set; }
            public string CompulsoryCode{ get; set; }
            public decimal CompulsoryDuty { get; set; }
            public decimal CompulsoryVat { get; set; }
            public decimal CompulsoryNetPremium { get; set; }
            public decimal CompulsoryPremium { get; set; }
            public decimal TotalPrice { get; set; }
            public string CarBrand { get; set; }
            public string CarFamily { get; set; }
            public string CarModel { get; set; }
            public int CarYear { get; set; }
            public string CarCC { get; set; }
            public string CarRegisterYear { get; set; }
            public string CarCCTV { get; set; }
            public string CarGarageType { get; set; }
            public string CarSerialNo { get; set; }
            public string CarEngineNo { get; set; }
            public string VehicleUsage { get; set; }
            public string CarAccessory { get; set; }
            public decimal CarAccessoryPrice { get; set; }


            [JsonIgnore]
            public bool BuyCompulsory { get; set; }

            //[JsonIgnore]
            //public int VehicleUsageId { get; set; }

            public List<PartnerApplicationDriver> Driver { get; set; }

            public static PartnerApplicationMotor Map(PartnerApplication baseObject) {
                var serializedParent = JsonConvert.SerializeObject(baseObject);
                return JsonConvert.DeserializeObject<PartnerApplicationMotor>(serializedParent);
            }
        }

        protected class PartnerApplicationHome : PartnerApplication {
            public string PropertyType { get; set; }
            public string ConstructionType { get; set; }
            public string UsageType { get; set; }
            public decimal Size { get; set; }
            public int RoomAmount { get; set; }
            public int Floor { get; set; }
            public string OwnerStatus { get; set; }
            public decimal ConstructionValue { get; set; }
            public decimal FunitureValue { get; set; }
            public string PropertyAddressNo { get; set; }
            public string PropertyAddressMoo { get; set; }
            public string PropertyAddressFloor { get; set; }
            public string PropertyAddressVillage { get; set; }
            public string PropertyAddressSoi { get; set; }
            public string PropertyAddressRoad { get; set; }
            public string PropertyAddressSubDistrict { get; set; }
            public string PropertyAddressDistrict { get; set; }
            public string PropertyAddressProvince { get; set; }
            public string PropertyAddressPostalCode { get; set; }

            public static PartnerApplicationHome Map(PartnerApplication baseObject) {
                var serializedParent = JsonConvert.SerializeObject(baseObject);
                return JsonConvert.DeserializeObject<PartnerApplicationHome>(serializedParent);
            }
        }

        protected class PartnerApplicationTA : PartnerApplication {
            public string TripType { get; set; }
            public string Country { get; set; }
            public string CoverageType { get; set; }
            public string CoverageOption { get; set; }
            public int TotalDay { get; set; }
            public int TotalPerson { get; set; }

            public static PartnerApplicationTA Map(PartnerApplication baseObject) {
                var serializedParent = JsonConvert.SerializeObject(baseObject);
                return JsonConvert.DeserializeObject<PartnerApplicationTA>(serializedParent);
            }
        }

        protected class PartnerApplicationPA : PartnerApplication {
            //public string BuyerName { get; set; }
            //public string BuyerSurname { get; set; }
            //public string BuyerTel { get; set; }
            //public string BuyerEmail { get; set; }

            public static PartnerApplicationPA Map(PartnerApplication baseObject) {
                var serializedParent = JsonConvert.SerializeObject(baseObject);
                return JsonConvert.DeserializeObject<PartnerApplicationPA>(serializedParent);
            }
        }

        protected class PartnerApplicationPH : PartnerApplication {
            public int Height { get; set; }
            public int Weight { get; set; }

            public static PartnerApplicationPH Map(PartnerApplication baseObject) {
                var serializedParent = JsonConvert.SerializeObject(baseObject);
                return JsonConvert.DeserializeObject<PartnerApplicationPH>(serializedParent);
            }
        }


        protected string FormatCarType(int value) {
            switch (value) {
                case (int)ProductSubCategoryKey.PRB_Car: return "110";
                case (int)ProductSubCategoryKey.PRB_Pickup: return "320";
                case (int)ProductSubCategoryKey.PRB_Van: return "210";
            }
            return "";
        }

    }

}
