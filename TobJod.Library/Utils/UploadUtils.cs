﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace TobJod.Utils {
    public class UploadUtils {

        public static string SaveFile(HttpPostedFileBase file, string path) {
            return SaveFile(file, path, false);
        }

        public static string SaveFile(HttpPostedFileBase file, string path, bool createFilename) {
            if (file == null) { return ""; }
            String filename;
            if (createFilename) {
                filename = FileUtils.GenerateFileName() + Path.GetExtension(file.FileName);
            } else {
                filename = file.FileName;
            }
            return SaveFile(file, path, filename);
        }

        public static string SaveFile(HttpPostedFileBase file, string path, string newFileName) {
            try {
                if ((file != null) && (file.ContentLength > 0)) {
                    FileUtils.CreateFolder(path);
                    newFileName = System.IO.Path.GetFileName(newFileName);
                    newFileName = FileUtils.GetFileName(path, newFileName);
                    file.SaveAs(path + newFileName);
                    return newFileName;
                } else {
                    return "";
                }
            } catch (Exception ex) {

                throw new Exception("Cannot save file. path:" + path + "filename: " + newFileName + " x"  + ex.ToString());
            }
        }

        public static string SaveAndReplaceFile(HttpPostedFileBase file, string path, string oldfilename) {
            return SaveAndReplaceFile(file, path, oldfilename, false);
        }

        public static string SaveAndReplaceFile(HttpPostedFileBase file, string path, string oldfilename, bool createFileName) {
            string filename;
            if ((file != null) && (file.ContentLength > 0)) {
                filename = SaveFile(file, path, createFileName);

                if (!string.Equals(filename, oldfilename, StringComparison.CurrentCultureIgnoreCase)) {
                    FileUtils.DeleteFile(path + oldfilename);
                }
            } else {
                filename = oldfilename ?? String.Empty;
            }
            return filename;
        }

        public static string SaveOrDuplicateFile(HttpPostedFileBase file, string path, string oldfilename) {
            return SaveOrDuplicateFile(file, path, oldfilename, false);
        }

        public static string SaveOrDuplicateFile(HttpPostedFileBase file, string path, string oldfilename, bool createFileName) {
            string filename;
            if ((file != null) && (file.ContentLength > 0)) {
                filename = SaveFile(file, path, createFileName);

                if (!string.Equals(filename, oldfilename, StringComparison.CurrentCultureIgnoreCase)) {
                    FileUtils.DeleteFile(path + oldfilename);
                }
            } else {
                filename = oldfilename ?? String.Empty;
                String newFilename = FileUtils.GetFileName(path, filename);
                File.Copy(path + filename, path + newFilename);
                filename = newFilename;
            }
            return filename;
        }

        public static string SaveAndReplaceFileOverwrite(HttpPostedFileBase file, string path, string oldfilename) {
            string filename = "";
            if ((file != null) && (file.ContentLength > 0)) {
                filename = file.FileName;
                try {
                    if ((file != null) && (file.ContentLength > 0)) {
                        FileUtils.CreateFolder(path);
                        FileUtils.DeleteFile(path + file.FileName);
                        file.SaveAs(path + filename);
                    } else {
                        return "";
                    }
                } catch (Exception ex) {
                    throw new Exception("Cannot save file." + ex.Message);
                }

                if (!string.Equals(filename, oldfilename, StringComparison.CurrentCultureIgnoreCase)) {
                    FileUtils.DeleteFile(path + oldfilename);
                }
            } else {
                filename = oldfilename;
            }
            return filename;
        }

        public static string SaveImageWithThumbnail(HttpPostedFileBase file, string path, string prefix, string suffix, int width, int height, out string thumbnail) {
            return SaveImageWithThumbnail(file, path, prefix, suffix, width, height, false, out thumbnail);
        }

        public static string SaveImageWithThumbnail(HttpPostedFileBase file, string path, string prefix, string suffix, int width, int height, bool createFileName, out string thumbnail) {
            string filename;
            if ((file != null) && (file.ContentLength > 0)) {
                filename = SaveFile(file, path, createFileName);
                thumbnail = ImageUtils.CreateThumbnail(path, filename, prefix, suffix, width, height);
            } else {
                filename = "";
                thumbnail = "";
            }
            return filename;
        }

        public static string SaveAndReplaceImageWithThumbnail(HttpPostedFileBase file, string path, string oldfilename, string oldthumbnail, string prefix, string suffix, int width, int height, out string thumbnail) {
            return SaveAndReplaceImageWithThumbnail(file, path, oldfilename, oldthumbnail, prefix, suffix, width, height, out thumbnail, false);
        }

        public static string SaveAndReplaceImageWithThumbnail(HttpPostedFileBase file, string path, string oldfilename, string oldthumbnail, string prefix, string suffix, int width, int height, out string thumbnail, bool createFileName) {
            string filename;
            if ((file != null) && (file.ContentLength > 0)) {
                filename = SaveFile(file, path, createFileName);
                if (!string.Equals(filename, oldfilename, StringComparison.CurrentCultureIgnoreCase)) {
                    FileUtils.DeleteFiles(path, oldfilename, oldthumbnail);
                }
                thumbnail = ImageUtils.CreateThumbnail(path, filename, prefix, suffix, width, height);
            } else {
                filename = oldfilename;
                thumbnail = oldthumbnail;
            }
            return filename;
        }


        #region image

        public static string SaveImageResize(HttpPostedFileBase file, string path, int width, int height, bool createFileName) {
            string filename;
            if ((file != null) && (file.ContentLength > 0)) {
                filename = SaveFile(file, path, createFileName);
                filename = ImageUtils.CreateThumbnail(path, filename, "", "", width, height);
            } else {
                filename = "";
            }
            return filename;
        }

        public static string SaveAndReplaceImageResize(HttpPostedFileBase file, string path, string oldfilename, int width, int height, bool createFileName) {
            string filename;
            if ((file != null) && (file.ContentLength > 0)) {
                filename = SaveFile(file, path, createFileName);
                filename = ImageUtils.CreateThumbnail(path, filename, "", "", width, height);
                if (!string.Equals(filename, oldfilename, StringComparison.CurrentCultureIgnoreCase)) {
                    FileUtils.DeleteFiles(path, oldfilename);
                }
            } else {
                filename = oldfilename;
            }
            return filename;
        }

        #endregion

        public static string GetContentType(string path) {
            switch (Path.GetExtension(path).ToLower()) {
                case "pdf":
                    return "application/pdf";
                case "zip":
                    return "application/zip";
                case "doc":
                case "docx":
                    return "application/msword";
                case "ppt":
                case "pptx":
                    return "application/powerpoint";
                case "xls":
                case "xlsx":
                    return "application/excel";
                default: return "";
            }
        }

        public static bool IsExcel(HttpPostedFileBase file) {
            if (file == null || file.ContentLength == 0) return true;

            string[] formats = new string[] { ".xls", ".xlsx" };
            return formats.Any(item => file.FileName.EndsWith(item, StringComparison.OrdinalIgnoreCase));

        }

        public static bool IsImage(HttpPostedFileBase file) {
            if (file == null || file.ContentLength == 0) return true;
            if (file.ContentType.Contains("image")) {
                return true;
            }

            string[] formats = new string[] { ".jpg", ".png", ".gif", ".jpeg" }; 
            return formats.Any(item => file.FileName.EndsWith(item, StringComparison.OrdinalIgnoreCase));

        }

        public static bool IsImage(params HttpPostedFileBase[] files) {
            var result = true;
            foreach (var item in files) {
                result &= IsImage(item);
            }
            return result;
        }

    }

}
