﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TobJod.Utils {

    public class ImageUtils {

        public static ImageValidationStaus ValidateImage(string fileName, int Width, int Height, long Size) {
            if (File.Exists(fileName)) {

                //Is Image?
                System.Drawing.Image image = GetImage(fileName);
                if (image == null) {
                    return ImageValidationStaus.IsNotImage;
                }

                //File Size
                FileInfo f = new FileInfo(fileName);
                if (f.Length > Size) {
                    return ImageValidationStaus.SizeOverLimit;
                }

                //Dimension
                if (Width > 0 && image.Width > Width) {
                    return ImageValidationStaus.WidthOverLimit;
                } else if (Height > 0 && image.Height > Height) {
                    return ImageValidationStaus.HeightOverLimit;
                } else {
                    return ImageValidationStaus.Valid;
                }
            } else {
                return ImageValidationStaus.Nothing;
            }
        }

        public static System.Drawing.Image GetImage(string fileName) {
            try {
                System.Drawing.Image image = System.Drawing.Image.FromFile(fileName);
                return image;
            } catch (Exception) {
                return null;
            }
        }

        public static int GetImageWidth(string fileName) {
            try {
                System.Drawing.Image image = System.Drawing.Image.FromFile(fileName);
                return image.Width;
            } catch (Exception) {
                return 0;
            }
        }

        public static int GetImageHeight(string fileName) {
            try {
                System.Drawing.Image image = System.Drawing.Image.FromFile(fileName);
                return image.Height;
            } catch (Exception) {
                return 0;
            }
        }

        public static string CreateThumbnail(string path, string sourceFileName, string prefix, string suffix, int width, int height) {
            string thumbnailFilename = prefix + Path.GetFileNameWithoutExtension(path + sourceFileName) + suffix + Path.GetExtension(sourceFileName);
            return CreateThumbnail(path, sourceFileName, thumbnailFilename, width, height);
        }

        public static string CreateThumbnail(string path, string sourceFilename, string thumbnailFilename, int width, int height) {
            System.Drawing.Bitmap bmpOut = null;
            try {
                using (Bitmap loBMP = new Bitmap(path + sourceFilename)) {
                    System.Drawing.Imaging.ImageFormat loFormat = loBMP.RawFormat;
                    int new_width = 0;
                    int new_height = 0;

                    if (loBMP.Width <= width && loBMP.Height <= width) {
                        if (!string.Equals(sourceFilename, thumbnailFilename)) {
                            loBMP.Save(path + thumbnailFilename);
                        }
                        return thumbnailFilename;
                    } else {
                        if (loBMP.Width > width) {
                            new_width = width;
                        } else {
                            new_width = loBMP.Width;
                        }
                        new_height = (int)(loBMP.Height / (loBMP.Width / new_width));

                        if (new_height > height) {
                            new_height = height;
                            new_width = (int)(loBMP.Width / (loBMP.Height / new_height));
                        }

                        bmpOut = new Bitmap(new_width, new_height);
                        Graphics g = Graphics.FromImage(bmpOut);
                        g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                        g.FillRectangle(Brushes.White, 0, 0, new_width, new_height);
                        g.DrawImage(loBMP, 0, 0, new_width, new_height);
                        loBMP.Dispose();
                        bmpOut.Save(path + thumbnailFilename);
                        return thumbnailFilename;
                    }
                }
            } catch {
                bmpOut = null;
            } finally {
                if (bmpOut != null) { bmpOut.Dispose(); }
            }
            return "";
        }

    }

    public enum ImageValidationStaus {
        Nothing = 0,
        Valid = 1,
        IsNotImage = 2,
        SizeOverLimit = 3,
        WidthOverLimit = 4,
        HeightOverLimit = 5
    }
}
