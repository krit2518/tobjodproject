﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TobJod.Utils {
    public class FileUtils {

        public static string GenerateFileName() {
            return DateUtils.GetUnixTimeStamp(DateTime.Now).ToString();
        }

        #region Folder

        public static bool CreateFolder(String path) {
            if (Directory.Exists(path)) {
                return true;
            } else {
                return Directory.CreateDirectory(path).Exists;
            }
        }

        public static void CopyFolder(String sourcePath, String destPath) {
            if (!Directory.Exists(destPath)) {
                Directory.CreateDirectory(destPath);
            }
            String dest;
            foreach (String f in Directory.GetFiles(sourcePath)) {
                dest = Path.Combine(destPath, Path.GetFileName(f));
                File.Copy(f, dest);
            }
            foreach (String fd in Directory.GetDirectories(sourcePath)) {
                dest = Path.Combine(destPath, Path.GetFileName(fd));
                CopyFolder(fd, dest);
            }
        }

        public static bool MoveFolder(String sourcePath, String destPath) {
            try {
                Directory.Move(sourcePath, destPath);
                return true;
            } catch (Exception) {
                return false;
            }
        }

        public static bool DeleteFolder(String path) {
            try {
                Directory.Delete(path, true);
                return true;
            } catch (Exception) {
                return false;
            }
        }

        #endregion

        #region File

        public static string GetFileName(String fullPath, String fileName) {
            return GetFileName(string.Concat(fullPath, @"\", fileName));
        }

        public static bool MoveFile(string sourceFileName, string destFileName) {
            try {
                File.Move(sourceFileName, destFileName);
                return true;
            } catch { }
            return false;
        }

        public static bool DeleteFile(string fileName) {
            try {
                if (!String.IsNullOrEmpty(fileName) && (File.Exists(fileName))) {
                    File.Delete(fileName);
                    return true;
                }
            } catch { }
            return false;
        }

        public static bool DeleteFiles(string path, params string[] fileNames) {
            bool result = true;
            foreach (string file in fileNames) {
                if ((string.IsNullOrEmpty(file)) == false && (DeleteFile(Path.Combine(path, file)) == false)) {
                    result = false;
                }
            }
            return result;
        }

        public static string GetFileName(String fullPath) {
            String path = Path.GetDirectoryName(fullPath);
            String filename = Path.GetFileNameWithoutExtension(fullPath).Replace(" ", "");
            String ext = Path.GetExtension(fullPath);
            String newpath = string.Concat(path, @"\", filename, ext);
            int i = 1;
            String file = filename;
            while (File.Exists(newpath)) {
                file = string.Concat(filename, "-", i);
                newpath = string.Concat(path, @"\", file, ext);
                i += 1;
            }
            return file + ext;
        }

        #endregion

        #region text

        public static string ReadText(string path) {
            return File.ReadAllText(path);
        }

        public static bool SaveText(string text, string path) {
            return SaveText(text, path, Encoding.UTF8);
        }

        public static bool SaveText(string text, string path, Encoding encoding) {
            using (StreamWriter s = new StreamWriter(path, false, encoding)) {
                s.Write(text);
                s.Close();
            }
            return true;
        }

        public static bool SaveTextAppend(string text, string path) {
            return SaveTextAppend(text, path, Encoding.UTF8);
        }

        public static bool SaveTextAppend(string text, string path, Encoding encoding) {
            using (StreamWriter s = new StreamWriter(path, true, encoding)) {
                s.Write(text);
                s.Close();
            }
            return true;
        }

        #endregion
    }
}
