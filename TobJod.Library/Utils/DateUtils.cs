﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TobJod.Utils {
    public class DateUtils {

        public static Int64 GetUnixTimeStamp(DateTime date) {
            DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            TimeSpan ts = date.ToUniversalTime() - dt;
            return (Int64)ts.TotalSeconds;
        }

        public static DateTime GetDateWithoutTime(DateTime date) {
            if (date == DateTime.MinValue) {
                return date;
            } else {
                return new DateTime(date.Year, date.Month, date.Day);
            }
        }

        public static DateTime GetFirstDayOfMonth(DateTime date) {
            if (date == DateTime.MinValue) {
                return date;
            } else {
                return new DateTime(date.Year, date.Month, 1);
            }
        }

        public static DateTime ExtendToEndofDate(DateTime date) {
            if (date == DateTime.MinValue) {
                return date;
            } else {
                return new DateTime(date.Year, date.Month, date.Day).AddDays(1).AddSeconds(-1);
            }
        }

        public static DateTime UnixTimeStampToDateTime(double val) {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(val).ToLocalTime();
            return dtDateTime;
        }
        public static DateTime JavaTimeStampToDateTime(double val) {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(Math.Round(val / 1000)).ToLocalTime();
            return dtDateTime;
        }

        public static DateTime SqlMinDate() {
            return new DateTime(1753, 1, 1);
        }
        
        public static string CalculateAge(DateTime birthday, string yearTitle, string monthTitle) {
            DateTime today = DateTime.Today;
            TimeSpan diff = today - birthday;
            int year = today.Year - birthday.Year;
            int month = today.Month - birthday.Month;

            if (today.Day < birthday.Day) {
                month--;
            }

            if (month < 0) {
                year--;
                month += 12;
            }
            if (month > 0) {
                return year + yearTitle + month + monthTitle;
            } else {
                return year + yearTitle;
            }
        }
    }
}
