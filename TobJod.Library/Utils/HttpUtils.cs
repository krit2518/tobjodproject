﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace TobJod.Utils {

    public class HttpUtils {

        public static string GetBaseUrl() {
            var request = HttpContext.Current.Request;
            var appUrl = HttpRuntime.AppDomainAppVirtualPath;

            if (appUrl != "/") appUrl += "/";

            var baseUrl = string.Format("{0}://{1}{2}", request.Url.Scheme, request.Url.Authority, appUrl);

            return baseUrl;
        }

        public static string ResolveUrl(string url) {
            if (url.StartsWith("~")) {
                return ("" + url.Substring(1)).Replace("////", "//");
            } else {
                return url;
            }
        }

        public static string GetPathPhysical(string url) {
            return HttpContext.Current.Server.MapPath(url);
        }

        public static string GetPathRelative(string url) {
            return VirtualPathUtility.ToAbsolute(url);
        }

        public static string GetPathUrlAbsolute(string url) {
            Uri contextUri = HttpContext.Current.Request.Url;

            var baseUri = string.Format("{0}://{1}{2}", contextUri.Scheme,
               contextUri.Host, contextUri.Port == 80 ? string.Empty : ":" + contextUri.Port);

            return string.Format("{0}{1}", baseUri, VirtualPathUtility.ToAbsolute(url));
        }

        public static string BuidQueryStringWithIgnoreKey(params string[] keys) {
            StringBuilder sb = new StringBuilder();
            List<string> keylist = new List<string>();
            keylist.AddRange(keys);
            if (HttpContext.Current.Request.QueryString != null) {
                string v;
                foreach (string k in HttpContext.Current.Request.QueryString.AllKeys) {
                    if (!keylist.Contains(k)) {
                        v = HttpContext.Current.Request.QueryString[k];
                        if (!string.IsNullOrEmpty(v)) {
                            sb.Append("&amp;" + k + "=" + v);
                        }
                    }

                }
            }
            return sb.ToString();
        }

        public static string BuidQueryString(params string[] keys) {
            if (HttpContext.Current.Request.QueryString == null) { return ""; }
            StringBuilder sb = new StringBuilder();
            string key, val;
            for (int i = 0; i < keys.Length; i++) {
                key = keys[i];
                val = HttpContext.Current.Request.QueryString[key];
                if (!string.IsNullOrEmpty(val)) {
                    sb.Append("&amp;" + key + "=" + val);
                }
            }
            return sb.ToString();
        }

        public static NameValueCollection RetrieveQueryString() {
            return HttpContext.Current.Request.QueryString;
        }

        public static NameValueCollection RetrieveForm() {
            return HttpContext.Current.Request.Form;
        }

        public static String RequestGet(String url) {
            string html = string.Empty;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.AutomaticDecompression = DecompressionMethods.GZip;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream)) {
                html = reader.ReadToEnd();
            }
            return html;
        }

        public static String HttpPost(String url, String data) {
            string html = string.Empty;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            byte[] byteArray = Encoding.UTF8.GetBytes(data);
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = byteArray.Length;
            //request.AutomaticDecompression = DecompressionMethods.GZip;

            using (Stream stream = request.GetRequestStream()) {
                stream.Write(byteArray, 0, byteArray.Length);
                stream.Close();
            }

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream)) {
                html = reader.ReadToEnd();
            }
            return html;
        }

        public static String HttpPostJson(String url, String data) {
            string html = string.Empty;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/json";

            using (var streamWriter = new StreamWriter(request.GetRequestStream())) {
                streamWriter.Write(data);
            }

            var httpResponse = (HttpWebResponse)request.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream())) {
                return streamReader.ReadToEnd();
            }
        }

        //public static String RequestPost(String url) {
        //    List<KeyValuePair<string, string>> values = new List<KeyValuePair<string, string>>();
        //    values.Add(new KeyValuePair<string, string>("txtBrokerId", SiteConfig.BROKER_ID));
        //    values.Add(new KeyValuePair<string, string>("txtParam", param));
        //    var response = HttpUtils.HttpPost(url, values);
        //    string responseBody = await response.Content.ReadAsStringAsync();
        //    responseBody = responseBody.Trim();
        //    logRepo.AddLog(url, StringUtils.ToJson(values), responseBody, DateTime.Now);
        //    if (response.IsSuccessStatusCode && responseBody.Equals("0")) {
        //        return true;
        //    }
        //}

        //public static HttpResponseMessage HttpPost(String url, List<KeyValuePair<string, string>> parameters) {
        //    HttpClient client = new HttpClient();
        //    client.BaseAddress = new Uri(url);
        //    HttpContent content = new FormUrlEncodedContent(parameters);
        //    return client.PostAsync(url, content).Result;
        //}

        public static String GetPublicIP() {
            if (HttpContext.Current.Request.UserHostAddress.Length > 0) {
                return HttpContext.Current.Request.UserHostAddress;
            }
            return "";
        }

        public static String GetPrivateIP() {
            if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null) {
                return HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            }
            return HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        }

        public static string GenerateSlug(string title, int maxLength) {
            return GenerateSlug(title, maxLength, true);
        }

        public static string GenerateSlug(string title, int maxLength, bool toLowerCase) {
            string str;
            str = System.Text.RegularExpressions.Regex.Replace(title, "[^0-9a-zA-Zก-๙]|\\s", "-");
            str = System.Text.RegularExpressions.Regex.Replace(str, "-+", "-");
            if (str.Length > maxLength) {
                str = str.Substring(0, maxLength);
            }
            str = str.Trim('-');
            if (toLowerCase) {
                return str.ToLower();
            } else {
                return str;
            }
        }

        public static string GetFormSingle(string name) {
            var val = HttpContext.Current.Request.Form.GetValues(name);
            if (val != null && val.Length > 0) return val[0];
            return HttpContext.Current.Request.Form[name];
        }

        public static bool IsMobileOrTablet() {
            string u = HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"];
            var b = new System.Text.RegularExpressions.Regex(@"(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            var v = new System.Text.RegularExpressions.Regex(@"1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            return (b.IsMatch(u) || v.IsMatch(u.Substring(0, 4)));
        }
    }

}
