﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TobJod.Utils {
    public class NullUtils {

        public static bool cvBoolean(object val) {
            return cvBoolean(val, false);
        }

        public static bool cvBoolean(object val, bool defaultValue) {
            if (val is bool) {
                return (bool)val;
            } else {
                try {
                    return Convert.ToBoolean(val);
                } catch {
                    return defaultValue;
                }
            }
        }

        public static DateTime cvDateTime(object val) {
            return cvDateTime(val, DateTime.MinValue);
        }

        public static DateTime cvDateTime(object val, DateTime defaultValue) {
            if (val is DateTime) {
                return (DateTime)val;
            } else {
                return defaultValue;
            }
        }

        public static int cvInt(object val) {
            return cvInt(val, 0);
        }

        public static int cvInt(object val, int defaultValue) {
            if (val is int) {
                return (int)val;
            } else if (val is string) {
                int r;
                if (Int32.TryParse(val.ToString(), out r)) {
                    return r;
                } else {
                    return defaultValue;
                }
            } else if (val is uint) {
                return (int)(uint)val;
            } else if (val is Int16) {
                return (int)(Int16)val;
            } else if (val is UInt16) {
                return (int)(UInt16)val;
            } else if (val is Int64) {
                return (int)(Int64)val;
            } else if (val is UInt64) {
                return (int)(UInt64)val;
            } else if (val is Double) {
                return (int)(Double)val;
            } else if (val is byte) {
                return (int)(byte)val;
            } else if (val is sbyte) {
                return (int)(sbyte)val;
            } else {
                return defaultValue;
            }
        }

        public static decimal cvDec(object val) {
            return cvDec(val, 0);
        }

        public static decimal cvDec(object val, int defaultValue) {
            if (val is decimal) {
                return (Decimal)val;
            } else if (val is string) {
                decimal r;
                if (Decimal.TryParse(val.ToString(), out r)) {
                    return r;
                } else {
                    return defaultValue;
                }
            } else if (val is uint) {
                return (decimal)(uint)val;
            } else if (val is Int16) {
                return (decimal)(Int16)val;
            } else if (val is UInt16) {
                return (decimal)(UInt16)val;
            } else if (val is Int64) {
                return (decimal)(Int64)val;
            } else if (val is UInt64) {
                return (decimal)(UInt64)val;
            } else if (val is byte) {
                return (decimal)(byte)val;
            } else if (val is sbyte) {
                return (decimal)(sbyte)val;
            } else {
                return defaultValue;
            }
        }

        public static double cvDouble(object val) {
            return cvDouble(val, 0);
        }

        public static double cvDouble(object val, int defaultValue) {
            if (val is double) {
                return (double)val;
            } else if (val is string) {
                double r;
                if (double.TryParse(val.ToString(), out r)) {
                    return r;
                } else {
                    return defaultValue;
                }
            } else if (val is uint) {
                return (double)(uint)val;
            } else if (val is Int16) {
                return (double)(Int16)val;
            } else if (val is UInt16) {
                return (double)(UInt16)val;
            } else if (val is Int64) {
                return (double)(Int64)val;
            } else if (val is UInt64) {
                return (double)(UInt64)val;
            } else if (val is byte) {
                return (double)(byte)val;
            } else if (val is sbyte) {
                return (double)(sbyte)val;
            } else {
                return defaultValue;
            }
        }

        public static string cvString(object val) {
            return cvString(val, string.Empty);
        }

        public static string cvString(object val, string defaultValue) {
            if (val is string) {
                return (string)val;
            } else if (val != null) {
                try {
                    return val.ToString();
                } catch { }
            }   
            return defaultValue;
        }

        public static T cvEnum<T>(object val, T defaultVal) {
            bool parse = false;
            return cvEnum<T>(val, defaultVal, ref parse);
        }

        public static T cvEnum<T>(object val, T defaultVal, ref bool parse) {
            if (val == null) return defaultVal;
            if (Enum.IsDefined(typeof(T), val)) {
                parse = true;
                return (T)val;
            }
            return defaultVal;
        }
    }
}
