﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Reflection;

namespace TobJod.Utils {
    public class ValueUtils {

        public static DateTime GetDate(object val, string format = "dd/MM/yyyy", bool isThaiCalendar = false) {
            DateTime date;
            System.Globalization.CultureInfo ci;
            if (isThaiCalendar) {
                ci = new System.Globalization.CultureInfo("th-TH");
            } else {
                ci = new System.Globalization.CultureInfo("en-GB");
            }
            string v = NullUtils.cvString(val);
            if (DateTime.TryParseExact(v, format, ci, System.Globalization.DateTimeStyles.None, out date)) {
                return date;
            } else {
                return new DateTime(1753, 1, 1);
            }
        }

        public static DateTime GetDateTime(object val, string format = "dd/MM/yyyy HH:mm", bool isThaiCalendar = false) {
            DateTime date;
            System.Globalization.CultureInfo ci;
            if (isThaiCalendar) {
                ci = new System.Globalization.CultureInfo("th-TH");
            } else {
                ci = new System.Globalization.CultureInfo("en-GB");
            }
            string v = NullUtils.cvString(val);
            if (DateTime.TryParseExact(v, format, ci, System.Globalization.DateTimeStyles.None, out date)) {
                return date;
            } else {
                return new DateTime(1753, 1, 1);
            }
        }

        public static DateTime GetDateTimeDefaultFormat(object val) {
            DateTime date;
            if (DateTime.TryParse(NullUtils.cvString(val), out date)) {
                return date;
            } else {
                return new DateTime(1753, 1, 1);
            }
        }

        public static DateTime ExtendToEndOfDate(DateTime val) {
            return new DateTime(val.Year, val.Month, val.Day, 23, 59, 59);
        }

        public static DateTime ExtendToEndOfMonth(DateTime val) {
            var d = new DateTime(val.Year, val.Month, 1);
            d = d.AddMonths(1).AddDays(-1);
            return d;
        }

        public static Tuple<int, int> GetIntRange(object val, int defaultMin = 0, int defaultMax = 0, char separator = '-') {
            var vals = GetStringSplit(val, new char[] { separator });
            int i1, i2;
            if (vals.Length == 2 && int.TryParse(vals[0], out i1) && int.TryParse(vals[1], out i2)) {
                return Tuple.Create(Math.Max(i1, defaultMin), Math.Min(i2, defaultMax));
            } else {
                return Tuple.Create(defaultMin, defaultMax);
            }
        }

        public static string[] GetStringSplit(object val) {
            return GetStringSplit(val, new char[] { ',' });
        }

        public static string[] GetStringSplit(object val, char[] separators ) {
            return NullUtils.cvString(val).Replace(" ", "").Split(separators, StringSplitOptions.RemoveEmptyEntries);
        }

        public static int[] GetIntSplit(object val, char[] separators) {
            return GetIntSplit(val, separators, false);
        }

        public static int[] GetIntSplit(object val, char[] separators, bool allowZero) {
            List<int> items = new List<int>();
            var t = NullUtils.cvString(val).Replace(" ", "").Split(separators, StringSplitOptions.RemoveEmptyEntries);
            foreach (var item in t) {
                var i = NullUtils.cvInt(item);
                if (i > 0) {
                    items.Add(i);
                } else if (i == 0 && allowZero) {
                    items.Add(i);
                }
            }
            return items.ToArray();
        }

        public static int BoolToInt(bool val) {
            return val ? 1 : 0;
        }

        public static string NewLineToBr(string val) {
            if (val == null) return "";
            return Regex.Replace(val, @"\r\n?|\n", "<br/>");
        }

        public static string BrToNewsLine(string val) {
            if (val == null) return "";
            return Regex.Replace(val, @"<br/>", "\n");
        }

        public static string EmptyStringDefault(string val) {
            return EmptyStringDefault(val, "-");
        }
        public static string EmptyStringDefault(string val, string defaultVal) {
            return (String.IsNullOrEmpty(val) ? defaultVal : val);    
        }

        public static bool IsValidEmail(string email) {
            return (new EmailAddressAttribute().IsValid(email));
        }

        public static string GetEnumDisplayName(Enum enumValue) {
            try {
                return enumValue.GetType()
                                .GetMember(enumValue.ToString())
                                .First()
                                .GetCustomAttribute<DisplayAttribute>()
                                .GetName();
            } catch { }
            return enumValue.ToString();
        }

    }
}
