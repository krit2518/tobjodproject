﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TobJod.Models {
    public class Subscribe {
        public int Id { get; set; }
        [Display(Name = "Email"), DataType(DataType.EmailAddress), Required]
        public string Email { get; set; }
        public string PublicIP { get; set; }
        public string LocalIP { get; set; }
        public DateTime CreateDate { get; set; }
    }
}