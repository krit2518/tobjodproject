﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TobJod.Models {

     public class ContactCategory {
        public int Id { get; set; }
        [Display(Name = "Title (TH)"), MaxLength(250), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string TitleTH { get; set; }
        [Display(Name = "Title (EN)"), MaxLength(250), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string TitleEN { get; set; }
        public int Sequence { get; set; }
        public Status Status { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateAdminId { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateAdminId { get; set; }

        public string CreateAdmin { get; set; }
        public string UpdateAdmin { get; set; }

        public List<ContactSubCategory> SubCategories { get; set; }
    }

    public class ContactSubCategory {
        public int Id { get; set; }
        [Display(Name = "Category"), Required]
        public int CategoryId { get; set; }
        [Display(Name = "Title (TH)"), MaxLength(250), Required]
        [RegularExpression("^[^<>!@#%/:;~]+$", ErrorMessage = "Special character (^ < > ! @ # / : ; ~) should not be entered")]
        public string TitleTH { get; set; }
        [Display(Name = "Title (EN)"), MaxLength(250), Required]
        [RegularExpression("^[^<>!@#%/:;~]+$", ErrorMessage = "Special character (^ < > ! @ # / : ; ~) should not be entered")]
        public string TitleEN { get; set; }
        [Display(Name = "Mail To (use ; for multiple email)"), RegularExpression("^([\\w+-.%]+@[\\w-.]+\\.[A-Za-z]{2,4};?)+$", ErrorMessage = "Please enter Email address (for muliple addresses, separate by ';' without space)"), Required]
        public string MailTo { get; set; }
        public int Sequence { get; set; }
        public Status Status { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateAdminId { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateAdminId { get; set; }

        public string CreateAdmin { get; set; }
        public string UpdateAdmin { get; set; }

        public string Category { get; set; }
    }
}
