﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TobJod.Models {
    public class Message {
        public MessageName Name { get; set; }
        public string Title { get; set; }
        [Required]
        public string MessageTH { get; set; }
        [Required]
        public string MessageEN { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateAdminId { get; set; }
        
        public string UpdateAdmin { get; set; }
    }

    public enum MessageName {

        Login_Invalid_Username_Password
    }
}
