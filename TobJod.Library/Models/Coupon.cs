﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TobJod.Models {
    public class Coupon {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Coupon Type")]
        public CouponType CouponType { get; set; }
        [Display(Name = "Title (TH)"), MaxLength(250), Required]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string TitleTH { get; set; }
        [Display(Name = "Title (EN)"), MaxLength(250), Required]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string TitleEN { get; set; }
        [Display(Name = "Coupon Code/Prefix"), MaxLength(10), Required]
        [RegularExpression("^[a-zA-Z0-9]+$", ErrorMessage = "only a-z, 0-9 is allowed")]
        public string Code { get; set; }
        [Required(ErrorMessage = "Please enter number between 1 - 100000")]
        [Display(Name = "Total Coupon"), Range(1, 100000, ErrorMessage = "Please enter number between 1 - 100000")]
        public int Total { get; set; }
        [Required]
        [Display(Name = "Discount Type")]
        public CouponDiscountType DiscountType { get; set; }
        [Required]
        [Display(Name = "Discount Amount")]
        public decimal DiscountAmount { get; set; }
        [Display(Name = "Product Type"), Required]
        public CouponProductType ProductType { get; set; }

        [Required]
        [Display(Name = "Active Date")]
        public DateTime ActiveDate { get; set; }
        [Required]
        [Display(Name = "Expire Date")]
        public DateTime ExpireDate { get; set; }
        [Required]
        public Status Status { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateAdminId { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateAdminId { get; set; }

        public string CreateAdmin { get; set; }
        public string UpdateAdmin { get; set; }

        public int TotalUsed { get; set; }
        public List<CouponCode> CouponCode { get; set; }
        public List<CouponCodeLog> CouponCodeLog { get; set; }
        [Display(Name = "Product")]
        public List<CouponProduct> CouponProducts { get; set; }
        [Display(Name = "Product Category")]
        public List<CouponProductCategory> CouponProductCategories { get; set; }
        [Display(Name = "Company")]
        public List<CouponCompany> CouponCompanies { get; set; }

        public Coupon() {
            CouponProducts = new List<CouponProduct>();
            CouponProductCategories = new List<CouponProductCategory>();
            CouponCompanies = new List<CouponCompany>();
        }
    }

    public class CouponProduct : Product {
        public int CouponId { get; set; }
        public int ProductId { get; set; }

        public string Product { get; set; }
    }

    public class CouponProductCategory : ProductCategory {
        public int CouponId { get; set; }
        public int ProductCategoryId { get; set; }

        public string ProductCategory { get; set; }
    }

    public class CouponCompany : Company {
        public int CouponId { get; set; }
        public int CompanyId { get; set; }

        public string Company { get; set; }
    }

    public class CouponCode {
        public int CouponId { get; set; }
        [Display(Name = "Coupon Code"), MaxLength(20), Required]
        public string Code { get; set; }
        public bool Used { get; set; }
    }

    public class CouponCodeLog {
        public int CouponId { get; set; }
        public string CouponCode { get; set; }
        public int OrderId { get; set; }
        public DateTime CreateDate { get; set; }
    }

    public enum CouponType {
        [Display(Name = "One Time")]
        OneTime = 1,
        [Display(Name = "Mass")]
        Mass = 2
    }

    public enum CouponDiscountType {
        [Display(Name = "Amount")]
        Amount = 1,
        [Display(Name = "Percent")]
        Percent = 2
    }

    public enum CouponProductType {
        [Display(Name = "All", Order = 4)]
        All = 1,
        [Display(Name = "By Product", Order = 1)]
        ByProduct = 2,
        [Display(Name = "By Category", Order = 2)]
        ByCategory = 3,
        [Display(Name = "By Company", Order = 3)]
        ByCompany = 4,
    }

}
