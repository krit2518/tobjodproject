﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TobJod.Models {
    public class ActionResultStatus {
        public bool Success { get; set; }
        public int ErrorCode { get; set; }
        public string ErrorTitle { get; set; }
        public string ErrorMessage { get; set; }
        public string Data { get; set; }
    }
}
