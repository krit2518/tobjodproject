﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TobJod.Models {
    public enum YesNo {
        Yes = 2,
        No = 1
    }
    public enum AllYesNo {
        All = 0,
        Yes = 1,
        No = 2
    }

    [Flags]
    public enum AllYesNoFlag {
        None = 0,
        All = 3,
        Yes = 1,
        No = 2
    }
}
