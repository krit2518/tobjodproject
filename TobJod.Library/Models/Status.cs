﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TobJod.Models {
    public enum Status {
        [Display(Name = "Inactive", Order = 2)]
        Inactive = 1,
        [Display(Name = "Active", Order = 1)]
        Active = 2,
    }
    
    public enum ApproveStatus {
        Pending = 0,
        Reject = 1,
        Approve = 2
    }
}
