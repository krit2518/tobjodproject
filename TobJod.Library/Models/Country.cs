﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TobJod.Models {
    public class Country : BaseModel {
        public int Id { get; set; }
        public string Code { get; set; }
        public CountryZone Zone { get; set; }
        [Display(Name = "Title (TH)"), MaxLength(250), Required]
        [RegularExpression("^[^<>!@#%/:;~]+$", ErrorMessage = "Special character (^ < > ! @ # / : ; ~) should not be entered")]
        public string TitleTH { get; set; }
        [Display(Name = "Title (EN)"), MaxLength(250), Required]
        [RegularExpression("^[^<>!@#%/:;~]+$", ErrorMessage = "Special character (^ < > ! @ # / : ; ~) should not be entered")]
        public string TitleEN { get; set; }
        [Required]
        public Status Status { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateAdminId { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateAdminId { get; set; }
        
        public string CreateAdmin { get; set; }
        public string UpdateAdmin { get; set; }

        public static int SchengenId { get { return 9999999; } }
    }

    public enum CountryZone {
        [Display(Name = "All")]
        All = 0,
        [Display(Name = "Thailand")]
        Thailand = 4,
        [Display(Name = "Worldwide")]
        Worldwide = 1,
        [Display(Name = "Asia")]
        Asia = 2,
        [Display(Name = "Schengen")]
        Schengen = 3
    }
}