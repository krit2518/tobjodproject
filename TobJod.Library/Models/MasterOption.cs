﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TobJod.Models {
    public class MasterOption {
        public int Id { get; set; }
        public int RefId { get; set; }
        [Required]
        public MasterOptionCategory Category { get; set; }
        [Display(Name = "Title (TH)"), MaxLength(250), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string TitleTH { get; set; }
        [Display(Name = "Title (EN)"), MaxLength(250), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string TitleEN { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Required]
        public string Code { get; set; }
        public int Mode { get; set; }
        public int Min { get; set; }
        public int Max { get; set; }
        [Required]
        public float Sequence { get; set; }
        [Required]
        public Status Status { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateAdminId { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateAdminId { get; set; }

        public string CreateAdmin { get; set; }
        public string UpdateAdmin { get; set; }
    }

    public enum MasterOptionCategory {
        [Display(Name = "คำนำหน้าชื่อ")]
        Salutation = 1001,

        [Display(Name = "ทะเบียนจังหวัด")]
        MotorProvince = 101,
        [Display(Name = "อายุผู้ขับขี่")]
        DriverAge = 102,

        [Display(Name = "อาชีพ")]
        Career = 301,

        [Display(Name = "วิธีการเดินทาง")]
        TravelMethod = 401,

        [Display(Name = "ลักษณะอาคาร")]
        PropertyType = 501,
        [Display(Name = "ลักษณะสิ่งปลูกสร้าง")]
        ConstructionType = 502,
        [Display(Name = "สถานที่ใช้เป็น")]
        LocationUsage = 503,

        [Display(Name = "ความสัมพันธ์")]
        Relationship = 901,

        [Display(Name = "FAQ Category")]
        FAQCategory = 9001
    }
}
