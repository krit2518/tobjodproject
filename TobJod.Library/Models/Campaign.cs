﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace TobJod.Models {
    public class Campaign {
        public int Id { get; set; }
        [Display(Name = "Sub Category"), Required]
        public int ParentId { get; set; }
        public string Code { get; set; }
        [Display(Name = "Title (TH)"), MaxLength(250), Required]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public String TitleTH { get; set; }
        [Display(Name = "Title (EN)"), MaxLength(250), Required]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public String TitleEN { get; set; }
        [Display(Name = "Body (TH)"), DataType(DataType.MultilineText), AllowHtml, DisplayFormat(ConvertEmptyStringToNull = false), Required]
        public String BodyTH { get; set; }
        [Display(Name = "Body (EN)"), DataType(DataType.MultilineText), AllowHtml, DisplayFormat(ConvertEmptyStringToNull = false), Required]
        public String BodyEN { get; set; }
        [Display(Name = "Image (TH)"), MaxLength(250)]
        public String ImageTH { get; set; }
        [Display(Name = "Image (EN)"), MaxLength(250)]
        public String ImageEN { get; set; }
        [Display(Name = "Image Alternate (TH)"), MaxLength(250), Required]  
        [RegularExpression("^[^<>.!@#%/:;~]+$", ErrorMessage = "Special character (^ < > . ! @ # / : ; ~) should not be entered")]
        public String ImageAltTH { get; set; }
        [Display(Name = "Image Alternate (EN)"), MaxLength(250), Required]
        [RegularExpression("^[^<>.!@#%/:;~]+$", ErrorMessage = "Special character (^ < > . ! @ # / : ; ~) should not be entered")]
        public String ImageAltEN { get; set; }
        [Display(Name = "Guest")]
        public bool IsGuest { get; set; }
        [Display(Name = "Member: Silver")]
        public bool IsMemberSilver { get; set; }
        [Display(Name = "Member: Gold")]
        public bool IsMemberGold { get; set; }
        [Display(Name = "Member: Platinum")]
        public bool IsMemberPlatinum { get; set; }
        [Display(Name = "All Promotion")]
        public bool PromotionAll { get; set; }
        [Display(Name = "Active Date"), DataType(DataType.Date), Required]
        public DateTime ActiveDate { get; set; }
        [Display(Name = "Expire Date"), DataType(DataType.Date), Required]
        public DateTime ExpireDate { get; set; }
        public ApproveStatus ApproveStatus { get; set; }
        [Required]
        public Status Status { get; set; }
        [Display(Name = "Owner")]
        public string OwnerAdminId { get; set; }
        public string ApproveAdminId { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateAdminId { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateAdminId { get; set; }
        
        public string OwnerAdmin { get; set; }
        public string ApproveAdmin { get; set; }
        public string CreateAdmin { get; set; }
        public string UpdateAdmin { get; set; }

        public int ChildId { get; set; }

        [Display(Name = "Promotion")]
        public List<CampaignPromotion> CampaignPromotions { get; set; }
    }

    public class CampaignPromotion : Promotion {
        public int CampaignId { get; set; }
        public int PromotionId { get; set; }
    }
}
