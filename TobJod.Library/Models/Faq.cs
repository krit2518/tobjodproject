﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace TobJod.Models {
    public class Faq {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        [Display(Name = "Question (TH)"), MaxLength(250), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & {{ }} [ ]) should not be entered")]
        public String QuestionTH { get; set; }
        [Display(Name = "Question (EN)"), MaxLength(250), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & {{ }} [ ]) should not be entered")]
        public String QuestionEN { get; set; }
        [Display(Name = "Answer (TH)"), DataType(DataType.MultilineText), AllowHtml, Required]
        public String AnswerTH { get; set; }
        [Display(Name = "Answer (EN)"), DataType(DataType.MultilineText), AllowHtml, Required]
        public String AnswerEN { get; set; }
        [Display(Name = "Pin")]
        public bool Pin { get; set; }
        [Display(Name = "Order"), Required]
        public float Sequence { get; set; }
        [Required]
        public Status Status { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateAdminId { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateAdminId { get; set; }

        [Display(Name = "Category")]
        public string Category { get; set; }
        public string CreateAdmin { get; set; }
        public string UpdateAdmin { get; set; }
    }
}
