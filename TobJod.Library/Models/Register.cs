﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TobJod.Models
{
    public class Register
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Member_Password { get; set; }
        public string LoginRefId { get; set; }
        public int LoginChannel { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Telephone { get; set; }
        public int Sex { get; set; }
        public DateTime Birthday { get; set; }
        public int MaritalStatus  { get; set; }
        public int CareerId  { get; set; }
        public int NationalId  { get; set; }
        public int Weight  { get; set; }
        public int Height  { get; set; }
        public string Image { get; set; }
        public string AddressNo { get; set; }
        public string AddressMoo { get; set; }
        public string AddressVillage { get; set; }
        public string AddressFloor { get; set; }
        public string AddressSoi { get; set; }
        public string AddressRoad { get; set; }
        public int AddressDistrictId { get; set; }
        public int AddressSubDistrictId { get; set; }
        public int AddressProvinceId { get; set; }
        public string AddressPostalCode { get; set; }
        public int LoginAttempt { get; set; }
        public int Status { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
    }






    public enum RegisterStatus
    {
        [Display(Name = "Pending")]
        Pending = 0,
        [Display(Name = "Allocated")]
        Allocated = 1,
        [Display(Name = "Follow Up")]
        FollowUp = 2,
        [Display(Name = "Yes")]
        Yes = 3,
        [Display(Name = "No")]
        No = 4,
        [Display(Name = "Spam")]
        Spam = 5,
        [Display(Name = "Closed")]
        Closed = 9
    }

    public enum RegisterSource
    {
        [Display(Name = "Contact Us")]
        ContactUs = 1,
        [Display(Name = "Product")]
        Product = 2
    }

}
