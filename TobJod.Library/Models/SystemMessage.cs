﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TobJod.Models {
    public class SystemMessage : BaseModel {
        public int Id { get; set; }
        public SystemMessageCategory Category { get; set; }
        public string Code { get; set; }
        [Display(Name = "Sub Code")]
        public string SubCode { get; set; }
        [Display(Name = "Message (TH)"), Required]
        [RegularExpression("^[^<>!@#%/:;~]+$", ErrorMessage = "Special character (^ < > ! @ # / : ; ~) should not be entered")]
        public string BodyTH { get; set; }
        [Display(Name = "Message (EN)"), Required]
        [RegularExpression("^[^<>!@#%/:;~]+$", ErrorMessage = "Special character (^ < > ! @ # / : ; ~) should not be entered")]
        public string BodyEN { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateAdminId { get; set; }

        public string UpdateAdmin { get; set; }
    }

    public enum SystemMessageCategory {
        [Display(Name = "2C2P")]
        Payment2C2P = 1,

        [Display(Name = "Coupon")]
        Coupon = 2,

        [Display(Name = "Offline Payment")]
        OfflinePayment = 3,
    }

    public enum SystemMessageStatus
    {
        CouponAvailable = 2001,
        CouponUsed = 2002,
        CouponUnKnown = 2003,
        CouponTopUp = 2004,

        OfflinePayment = 3001,
    }

  
}
