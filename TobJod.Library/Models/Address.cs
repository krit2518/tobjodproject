﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TobJod.Models {
    public class Address {
        public string HouseNo { get; set; }
        public string Moo { get; set; }
        public string Building { get; set; }
        public string Soi { get; set; }
        public string Road { get; set; }
        public int DistrictId { get; set; }
        public int SubdistrictId { get; set; }
        public int ProvinceId { get; set; }
        public string PostalCode { get; set; }
        public int CountryId { get; set; }

        public string District { get; set; }
        public string Subdistrict { get; set; }
        public string Province { get; set; }
        public string Country { get; set; }
    }
}
    