﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TobJod.Models {
    public class DayOff {
        public int Id { get; set; }
        [Display(Name = "Date"), DataType(DataType.Date), Required]
        public DateTime EventDate { get; set; }
        [Display(Name = "Title (TH)"), MaxLength(250), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public String TitleTH { get; set; }
        [Display(Name = "Title (EN)"), MaxLength(250), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public String TitleEN { get; set; }
        [Required]
        public Status Status { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateAdminId { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateAdminId { get; set; }

        public string CreateAdmin { get; set; }
        public string UpdateAdmin { get; set; }
    }
}
