﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TobJod.Models {

    public class SmsOtp {
        public int Id { get; set; }
        public int ApplicationId { get; set; }
        public string UniqueId { get; set; }
        public string Mobile { get; set; }
        public bool VerifySuccess { get; set; }
        public int Status { get; set; }
        public DateTime CreateDate { get; set; }
    }
    
}
