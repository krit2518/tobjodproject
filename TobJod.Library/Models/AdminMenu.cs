﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TobJod.Models {
    public class AdminMenu {
        public string Module { get; set; }
        public string ParentModule { get; set; }
        public string Title { get; set; }
        public string Icon { get; set; }
        public bool Has_View { get; set; }
        public bool Has_Add { get; set; }
        public bool Has_Edit { get; set; }
        public bool Has_Delete { get; set; }
        public bool Has_Approve { get; set; }
        public bool Has_Assign { get; set; }
        public bool Has_Export { get; set; }
        public bool Can_View { get; set; }
        public bool Can_Add { get; set; }
        public bool Can_Edit { get; set; }
        public bool Can_Delete { get; set; }
        public bool Can_Approve { get; set; }
        public bool Can_Assign { get; set; }
        public bool Can_Export { get; set; }
        public AdminMenuType MenuType { get; set; }
        public List<AdminMenu> SubMenu { get; set; }

        //public AdminMenu(string module, string title, string icon, List<AdminMenu> submenu) {
        //    this.Module = module;
        //    this.Title = title;
        //    this.Icon = icon;
        //    this.SubMenu = submenu;
        //}
    }

    public enum AdminMenuType {
        Normal = 0,
        Parent = 1,
        Child = 2
    }
}