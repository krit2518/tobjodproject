﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TobJod.Models {
    public class ListModel<T> {
        public List<T> Items { get; set; }

        public ListModel(List<T> list) {
            Items = list;
        }
    }
}