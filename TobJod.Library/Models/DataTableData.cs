﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace TobJod.Models {

    public class DataTableData<T> {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get { return recordsTotal; } }
        public List<string[]> data { get; set; }

        [ScriptIgnore]
        public List<T> dataRaw { get; set; }
    }

    public class DataTableParameter {
        public int draw { get; set; }
        public int start { get; set; }
        public int length { get; set; }
        public int sortColumn { get; set; }
        public string sortDirection { get; set; }
        [AllowHtml]
        public string searchValue { get; set; }
        public bool hasSearch { get; set; }
    }
}