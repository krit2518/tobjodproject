﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TobJod.Models {
    public class Login
    {
        public int Id { get; set; }
        public LeadSource Source { get; set; }
        public int ProductId { get; set; }
        public int PremiumId { get; set; }

 
        [Display(Name = "Email"), MaxLength(250), DataType(DataType.EmailAddress), Required]
         public string Email { get; set; }

        [Display(Name = "Password"), MaxLength(50), Required]
        //[RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string Password { get; set; }

        public bool Rememberme { get; set; }

        public LeadStatus LeadStatus { get; set; }
        public Status Status { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateAdminId { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateAdminId { get; set; }
        [Display(Name = "Owner")]
        public string OwnerAdminId { get; set; }
        public string AssignAdminId { get; set; }

        public string Product { get; set; }
        public string Fullname { get; set; }
        public string Subject { get; set; }
        public string SubjectCategory { get; set; }
        public string ProductCategory { get; set; }

        public string CreateAdmin { get; set; }
        public string UpdateAdmin { get; set; }
        public string OwnerAdmin { get; set; }
        public string AssignAdmin { get; set; }

        public List<LeadLog> Log { get; set; }
        public LeadCriteria LeadCriteria { get; set; }

        public static Dictionary<string, string> CallBackTimeList() {
            var val = new Dictionary<string, string>();
            val.Add("10:00", "10:00 - 12:00");
            val.Add("13:00", "13:00 - 15:00");
            val.Add("15:00", "15:00 - 17:00");
            return val;
        }
    }


    public class LoginLog {
        public int Id { get; set; }
        public int LeadId { get; set; }
        public LeadStatus LeadStatus { get; set; }
        [DataType(DataType.MultilineText), DisplayFormat(ConvertEmptyStringToNull = false), Required]
        public string Remark { get; set; }
        public DateTime CreateDate { get; set; }
        [Display(Name = "Follow Up Date")]
        public DateTime? FollowUpDate { get; set; }
        public string CreateAdminId { get; set; }
        public string AssignAdminId { get; set; }

        public string CreateAdmin { get; set; }
        public string AssignAdmin { get; set; }
    }

    public class LoginCriteria {
        public int LeadId { get; set; }
        public ProductSubCategoryKey SubCategoryId { get; set; }
        public ProductCategoryKey Category { get; set; }
        public int PriceFrom { get; set; }
        public int PriceTo { get; set; }
        public int HousePropertyType { get; set; }
        public int HouseConstructionType { get; set; }
        public int HousePropertyValue { get; set; }
        public int HouseConstructionValue { get; set; }
        public int HouseYear { get; set; }
        public int MotorBrandId { get; set; }
        public int MotorFamilyId { get; set; }
        public int MotorModelId { get; set; }
        public int MotorUsage { get; set; }
        public int MotorYear { get; set; }
        public int MotorCCTV { get; set; }
        public bool MotorPRB { get; set; }
        public bool MotorAccessory { get; set; }
        public int MotorGarage { get; set; }
        public int MotorRegisterProvinceId { get; set; }
        public int MotorDriverAge { get; set; }
        public int MotorDriverAgeId { get; set; }
        public int CareerId { get; set; }
        public DateTime Birthday { get; set; }
        public int Sex { get; set; }
        public int Weight { get; set; }
        public int Height { get; set; }
        public int TATripType { get; set; }
        public int TAZone { get; set; }
        public int TACoverageOption { get; set; }
        public int TACoverageType { get; set; }
        public int TADays { get; set; }
        public int TACountryId { get; set; }
        public int TAPerson { get; set; }

        public string HousePropertyTypeTitle { get; set; }
        public string HouseConstructionTypeTitle { get; set; }
        public string MotorBrandIdTitle { get; set; }
        public string MotorFamilyIdTitle { get; set; }
        public string MotorModelIdTitle { get; set; }
        public string MotorUsageTitle { get; set; }
        public string MotorCCTVTitle { get; set; }
        public string MotorPRBTitle { get; set; }
        public string MotorAccessoryTitle { get; set; }
        public string MotorGarageTitle { get; set; }
        public string MotorRegisterProvinceIdTitle { get; set; }
        public string MotorDriverAgeTitle { get; set; }
        public string MotorDriverAgeIdTitle { get; set; }
        public string CareerIdTitle { get; set; }
        public string SexTitle { get; set; }
        public string TATripTypeTitle { get; set; }
        public string TAZoneTitle { get; set; }
        public string TACoverageOptionTitle { get; set; }
        public string TACoverageTypeTitle { get; set; }
        public string TACountryIdTitle { get; set; }

        public string CompanyTitle { get; set; }

        public decimal PremiumPrice { get; set; }
    }

    public enum LoginStatus {
        [Display(Name = "Pending")]
        Pending = 0,
        [Display(Name = "Allocated")]
        Allocated = 1,
        [Display(Name = "Follow Up")]
        FollowUp = 2,
        [Display(Name = "Yes")]
        Yes = 3,
        [Display(Name = "No")]
        No = 4,
        [Display(Name = "Spam")]
        Spam = 5,
        [Display(Name = "Closed")]
        Closed = 9
    }

    public enum LoginSource {
        [Display(Name = "Contact Us")]
        ContactUs = 1,
        [Display(Name = "Product")]
        Product = 2
    }

}
