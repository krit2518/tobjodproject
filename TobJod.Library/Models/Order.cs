﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace TobJod.Models {
    public class Order : BaseModel {
        public int Id { get; set; }
        public int LeadFormId { get; set; }
        public int ProductApplicationId { get; set; }
        public string CANo { get; set; }
        public string PaymentId { get; set; }
        public string PolicyNoAdd { get; set; }
        public string PolicyNo { get; set; }
        public DateTime PolicyReceiveDate { get; set; }
        public string OrderCode { get; set; }
        public PaymentChannel PaymentChannel { get; set; }
        public PaymentStatus PaymentStatus { get; set; }
        public DateTime PaymentDate { get; set; }
        public int PaymentInstallment { get; set; }
        public decimal PaymentTotal { get; set; }
        public PartnerAPIStatus PartnerAPIStatus { get; set; }
        public BackendAPIStatus BackendAPIStatus { get; set; }

        [Required]
        public OrderStatus Status { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string PaidRef { get; set; }
        public string PaidAgent { get; set; }
        public string PaidChannel { get; set; }
        public string Bank { get; set; }
        public ProductCategoryKey Category { get; set; }

        public OrderProductHomePremium OrderProductHomePremium { get; set; }
        public OrderProductMotorPremium OrderProductMotorPremium { get; set; }
        public OrderProductPRBPremium OrderProductPRBPremium { get; set; }
        public OrderProductPAPremium OrderProductPAPremium { get; set; }
        public OrderProductPHPremium OrderProductPHPremium { get; set; }
        public OrderProductTAPremium OrderProductTAPremium { get; set; }

        public Product Product { get; set; }
        public ProductItem ProductItem { get; set; }
        public Promotion Promotion { get; set; }
        public ProductApplication ProductApplication { get; set; }
        public ProductApplicationMotor ProductApplicationMotor { get;set;}
        public List<ProductApplicationDriver> ProductApplicationDrivers { get; set; }
        public ProductApplicationTA ProductApplicationTA { get; set; }
        public ProductApplicationHome ProductApplicationHome { get; set; }
        public List<ProductApplicationPolicyHolder> ProductApplicationPolicyHolders { get; set; }

        public string GetPaymentTitle(Language language) {
            switch (this.PaymentChannel) {
                case PaymentChannel.CreditCard: return (language == Language.TH ? "บัตรเครดิต" : "Credit Card");
                case PaymentChannel.DebitCard: return (language == Language.TH ? "บัตรเดบิต" : "Debit Card");
                case PaymentChannel.BillPayment: return (language == Language.TH ? "จ่ายบิล" : "Bill Payment");
                case PaymentChannel.Cash: return (language == Language.TH ? "เงินสด" : "Cash");
                case PaymentChannel.DirectDebit: return (language == Language.TH ? "" : "Direct Debit");
                case PaymentChannel.IPP: return (language == Language.TH ? "ผ่อนชำระ" : "Installment Payment");
                case PaymentChannel.Others: return (language == Language.TH ? "อื่นๆ" : "Others");
            }
            return "";
        }
    }


    public class OrderProductHomePremium : ProductHomePremium {
        public int OrderId { get; set; }
        public int PremiumId { get; set; }
    }

    public class OrderProductMotorPremium : ProductMotorPremium {
        public int OrderId { get; set; }
        public int PremiumId { get; set; }
        public bool BuyCompulsory { get; set; }
    }

    public class OrderProductPRBPremium : ProductPRB {
        public int OrderId { get; set; }
        public int PremiumId { get; set; }
    }

    public class OrderProductPAPremium : ProductPAPremium {
        public int OrderId { get; set; }
        public int PremiumId { get; set; }
    }

    public class OrderProductPHPremium : ProductPHPremium {
        public int OrderId { get; set; }
        public int PremiumId { get; set; }
    }

    public class OrderProductTAPremium : ProductTAPremium {
        public int OrderId { get; set; }
        public int PremiumId { get; set; }
    }

    public enum OrderStatus {
        Pending = 0,
        Success = 2,
        Fail = 3,
        Cancel = 4
    }

    public enum PaymentChannel {
        [Display(Name = "Credit Card/Debit Card")]
        CreditCard = 1,
        [Display(Name = "Debit Card/Credit Card")]
        DebitCard = 2,
        [Display(Name = "Bill Payment")]
        BillPayment = 3,
        [Display(Name = "Cash")]
        Cash = 4,
        [Display(Name = "Direct Debit")]
        DirectDebit = 5,
        [Display(Name = "IPP")]
        IPP = 6,
        [Display(Name = "Others")]
        Others = 7,
    }

    public enum PaymentStatus {
        Pending = 0,
        Retry = 1,
        Success = 2,
        Fail = 3,
        Reject = 4,
        CanceledByUser = 5,
    }

    public enum PartnerAPIStatus {
        Pending = 0,
        Retry = 1,
        Success = 2,
        Fail = 3
    }

    public enum BackendAPIStatus {
        Pending = 0,
        Retry = 1,
        Success = 2,
        Fail = 3
    }


    public class OrderApplication : Order {

        public ProductSubCategoryKey ProductSubCategory { get; set; }

        //Application
        public string InsuranceCode { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public decimal SumInsured { get; set; }
        public decimal NetPremium { get; set; }
        public decimal Duty { get; set; }
        public decimal Tax { get; set; }
        public decimal Premium { get; set; }
        public decimal DisplayPremium { get; set; }
        public DateTime ActiveDate { get; set; }
        public DateTime ExpireDate { get; set; }
        public bool EPolicy { get; set; }
        public int CustomerType { get; set; }
        public string BuyerName { get; set; }
        public string BuyerTelephone { get; set; }
        public string BuyerMobilePhone { get; set; }
        public string BuyerEmail { get; set; }
        public string BuyerLineId { get; set; }
        public int AddressBranchType { get; set; }
        public int AddressBranchNo { get; set; }
        public string AddressNo { get; set; }
        public string AddressMoo { get; set; }
        public string AddressVillage { get; set; }
        public string AddressFloor { get; set; }
        public string AddressSoi { get; set; }
        public string AddressRoad { get; set; }
        public string AddressDistrict { get; set; }
        public string AddressSubDistrict { get; set; }
        public string AddressProvince { get; set; }
        public string AddressPostalCode { get; set; }
        public int Compulsory { get; set; }
        public string AttachedFile { get; set; }
        public string PaymentResponse { get; set; }

        public string ProductSubCategoryTitle { get; set; }
    }
}
