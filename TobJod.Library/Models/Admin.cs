﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace TobJod.Models {
    public class Admin {
        public string Id { get; set; }
        [Display(Name = "EmployeeId"), MaxLength(8), Required]
        [RegularExpression("^[0-9-]+$", ErrorMessage = "Only digit and - is allowed")]
        [Remote("ValidateEmployeeId", "Admin", HttpMethod = "POST", ErrorMessage = "EmployeeID already exists.")]
        public string EmployeeId { get; set; }
        [Display(Name = "Group"), Required]
        public int AdminGroupId { get; set; }
        [Display(Name = "Name"), MaxLength(250), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string Name { get; set; }
        public Status Status { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateAdminId { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateAdminId { get; set; }

        public string GroupName { get; set; }
        public string CreateAdmin { get; set; }
        public string UpdateAdmin { get; set; }
        public List<AdminPermission> Permissions { get; set; }
    }

    public class AdminGroup {
        public int Id { get; set; }
        [Display(Name = "Name"), MaxLength(250), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string Title { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateAdminId { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateAdminId { get; set; }

        public string CreateAdmin { get; set; }
        public string UpdateAdmin { get; set; }
        public List<AdminPermission> Permissions { get; set; }
    }

    public class AdminPermission {
        public string AdminId { get; set; }
        public int AdminGroupId { get; set; }
        public string Module { get; set; }
        public bool Can_View { get; set; }
        public bool Can_Add { get; set; }
        public bool Can_Edit { get; set; }
        public bool Can_Delete { get; set; }
        public bool Can_Approve { get; set; }
        public bool Can_Assign { get; set; }
        public bool Can_Export { get; set; }
    }
    
    public enum AdminAction {
        View,
        Add,
        Edit,
        Delete,
        Approve,
        Assign,
        Export,
        Disable,
        Duplicate
    }
}