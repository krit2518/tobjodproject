﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TobJod.Models {

    public class OrderClaim {
        public int Id { get; set; }
        public int OrderId { get; set; }
        [Display(Name = "Claim Id"), MaxLength(50), Required]
        [RegularExpression("^[^<>~\\u005E:;%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; % & ' \" {{ }} [ ]) should not be entered")]
        public string ClaimId { get; set; }
        [Display(Name = "ชื่อผู้เอาประกันภัย"), MaxLength(250), Required]
        [RegularExpression("^[^<>~\\u005E:;%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; % & ' \" {{ }} [ ]) should not be entered")]
        public string Name { get; set; }
        [Display(Name = "Cause Of Loss")]
        public string CauseOfLoss { get; set; }
        public string Notification { get; set; }
        [Display(Name = "วันที่เกิดเหตุ")]
        public DateTime EventDate { get; set; }
        [Display(Name = "วันที่แจ้งเหตุ")]
        public DateTime NotifyDate { get; set; }
        [Display(Name = "ค่าเสียหายจริง")]
        public decimal CustomerClaim { get; set; }
        [Display(Name = "ค่าเสียหายที่ประกันพิจารณา")]
        public decimal CompanyClaim { get; set; }
        public decimal Deductible { get; set; }
        [Display(Name = "ค่าสินไหมที่ประกันจ่าย")]
        public decimal TotalClaim { get; set; }

        [Required]
        public OrderClaimStatus Status { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateAdminId { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateAdminId { get; set; }

        [Display(Name = "Policy No"), MaxLength(50), Required]
        public string PolicyNo { get; set; }
        public string Company { get; set; }
        public string CreateAdmin { get; set; }
        public string UpdateAdmin { get; set; }
    }

    public enum OrderClaimStatus {
        Open = 1,
        Follow = 2,
        Closed = 3
    }

}