﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TobJod.Models {
    public enum Language {
        TH = 1,
        EN = 2
    }
}
