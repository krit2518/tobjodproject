﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TobJod.Models {
    public class Company {
        public int Id { get; set; }
        [Display(Name = "รหัสบริษัทประกันภัย"), DisplayFormat(ConvertEmptyStringToNull = false), MaxLength(10)]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string Code { get; set; }
        [Display(Name = "คำนำหน้าชื่อบริษัทประกันภัย (TH)"), DisplayFormat(ConvertEmptyStringToNull = false), MaxLength(15)]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string PrefixTH { get; set; }
        [Display(Name = "คำนำหน้าชื่อบริษัทประกันภัย (EN)"), DisplayFormat(ConvertEmptyStringToNull = false), MaxLength(15)]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string PrefixEN { get; set; }
        [Display(Name = "ชื่อบริษัทประกันภัย (TH)"), DisplayFormat(ConvertEmptyStringToNull = false), MaxLength(250)]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string TitleTH { get; set; }
        [Display(Name = "ชื่อบริษัทประกันภัย (EN)"), DisplayFormat(ConvertEmptyStringToNull = false), MaxLength(250)]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string TitleEN { get; set; }
        [Display(Name = "รหัสบริษัทประกันภัย (Corporate)"), DisplayFormat(ConvertEmptyStringToNull = false), MaxLength(10), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string CoCode { get; set; }
        [Display(Name = "คำนำหน้าชื่อบริษัทประกันภัย (Corporate) (TH)"), DisplayFormat(ConvertEmptyStringToNull = false), MaxLength(15), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string CoPrefixTH { get; set; }
        [Display(Name = "คำนำหน้าชื่อบริษัทประกันภัย (Corporate) (EN)"), DisplayFormat(ConvertEmptyStringToNull = false), MaxLength(15), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string CoPrefixEN { get; set; }
        [Display(Name = "ชื่อบริษัทประกันภัย (Corporate) (TH)"), DisplayFormat(ConvertEmptyStringToNull = false), MaxLength(255), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string CoTitleTH { get; set; }
        [Display(Name = "ชื่อบริษัทประกันภัย (Corporate) (EN)"), DisplayFormat(ConvertEmptyStringToNull = false), MaxLength(255), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string CoTitleEN { get; set; }
        [Display(Name = "รหัสบริษัทประกันภัย (Retail)"), DisplayFormat(ConvertEmptyStringToNull = false), MaxLength(5), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string ReCode { get; set; }
        [Display(Name = "คำนำหน้าชื่อบริษัทประกันภัย (Retail) (TH)"), DisplayFormat(ConvertEmptyStringToNull = false), MaxLength(15), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string RePrefixTH { get; set; }
        [Display(Name = "คำนำหน้าชื่อบริษัทประกันภัย (Retail) (EN)"), DisplayFormat(ConvertEmptyStringToNull = false), MaxLength(15), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string RePrefixEN { get; set; }
        [Display(Name = "ชื่อบริษัทประกันภัย (Retail) (TH)"), DisplayFormat(ConvertEmptyStringToNull = false), MaxLength(50), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string ReTitleTH { get; set; }
        [Display(Name = "ชื่อบริษัทประกันภัย (Retail) (EN)"), DisplayFormat(ConvertEmptyStringToNull = false), MaxLength(50), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string ReTitleEN { get; set; }
        [Display(Name = "รหัสบริษัทประกันภัย (CRM)"), DisplayFormat(ConvertEmptyStringToNull = false), MaxLength(10), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string AlCode { get; set; }
        [Display(Name = "คำนำหน้าชื่อบริษัทประกันภัย (CRM) (TH)"), DisplayFormat(ConvertEmptyStringToNull = false), MaxLength(15), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string AlPrefixTH { get; set; }
        [Display(Name = "คำนำหน้าชื่อบริษัทประกันภัย (CRM) (EN)"), DisplayFormat(ConvertEmptyStringToNull = false), MaxLength(15), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string AlPrefixEN { get; set; }
        [Display(Name = "ชื่อบริษัทประกันภัย (CRM) (TH)"), DisplayFormat(ConvertEmptyStringToNull = false), MaxLength(255), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string AlTitleTH { get; set; }
        [Display(Name = "ชื่อบริษัทประกันภัย (CRM) (EN)"), DisplayFormat(ConvertEmptyStringToNull = false), MaxLength(255), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string AlTitleEN { get; set; }
        [Display(Name = "Mail To (use ; for multiple email)"), RegularExpression("^([\\w+-.%]+@[\\w-.]+\\.[A-Za-z]{2,4};?)+$", ErrorMessage = "Please enter Email address (for muliple addresses, separate by ';' without space)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string MailTo { get; set; }
        [Display(Name = "API Channel")]
        public CompanyAPIChannel APIChannel { get; set; }
        [Display(Name = "API URL"), RegularExpression(@"^((http|https):\/\/)([a-zA-Z0-9\~\!\@\#\$\%\^\&\*\(\)_\-\=\+\\\/\?\.\:\;\'\,]*)", ErrorMessage = "Please enter url"), DisplayFormat(ConvertEmptyStringToNull = false), MaxLength(250)]

		public string APIUrl { get; set; }
        [Display(Name = "Payment Merchant Id"), MaxLength(250), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string PaymentMerchantId { get; set; }
        [Display(Name = "Payment Secret Key"), MaxLength(250), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string PaymentSecretKey { get; set; }
        public string ImageTH { get; set; }
        public string ImageEN { get; set; }
        
        [Required]
        public Status Status { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateAdminId { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateAdminId { get; set; }

        public string CreateAdmin { get; set; }
        public string UpdateAdmin { get; set; }
    }

    public enum CompanyAPIChannel {
        Email = 1,
        WebService = 2
    }
}
