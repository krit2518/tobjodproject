﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace TobJod.Models {
    public class ConfigPage {
        public ConfigPageName Id { get; set; }
        [Display(Name = "Body (TH)"), DataType(DataType.MultilineText), AllowHtml, DisplayFormat(ConvertEmptyStringToNull = false)]
        public String BodyTH { get; set; }
        [Display(Name = "Body (EN)"), DataType(DataType.MultilineText), AllowHtml, DisplayFormat(ConvertEmptyStringToNull = false)]
        public String BodyEN { get; set; }
        [Display(Name = "Meta Title (TH)"), MaxLength(250), Required]
        //[RegularExpression("^[^<>.!@#%/:;~]+$", ErrorMessage = "Special character should not be entered")]
        public String MetaTitleTH { get; set; }
        [Display(Name = "Meta Title (EN)"), MaxLength(250), Required]
        //[RegularExpression("^[^<>.!@#%/:;~]+$", ErrorMessage = "Special character should not be entered")]
        public String MetaTitleEN { get; set; }
        [Display(Name = "Meta Keyword (TH)"), DataType(DataType.MultilineText), Required]
        [RegularExpression("^[^<>!@#%/:;~]+$", ErrorMessage = "Special character (^ < > ! @ # / : ; ~) should not be entered")]
        public String MetaKeywordTH { get; set; }
        [Display(Name = "Meta Keyword (EN)"), DataType(DataType.MultilineText), Required]
        [RegularExpression("^[^<>!@#%/:;~]+$", ErrorMessage = "Special character (^ < > ! @ # / : ; ~) should not be entered")]
        public String MetaKeywordEN { get; set; }
        [Display(Name = "Meta Description (TH)"), DataType(DataType.MultilineText), Required]
        [RegularExpression("^[^<>!@#%/:;~]+$", ErrorMessage = "Special character (^ < > ! @ # / : ; ~) should not be entered")]
        public String MetaDescriptionTH { get; set; }
        [Display(Name = "Meta Description (EN)"), DataType(DataType.MultilineText), Required]
        [RegularExpression("^[^<>!@#%/:;~]+$", ErrorMessage = "Special character (^ < > ! @ # / : ; ~) should not be entered")]
        public String MetaDescriptionEN { get; set; }
        [Display(Name = "OG Title (TH)"), MaxLength(250), Required]
        [RegularExpression("^[^<>!@#%/:;~]+$", ErrorMessage = "Special character (^ < > ! @ # / : ; ~) should not be entered")]
        public String OGTitleTH { get; set; }
        [Display(Name = "OG Title (EN)"), MaxLength(250), Required]
        [RegularExpression("^[^<>!@#%/:;~]+$", ErrorMessage = "Special character (^ < > ! @ # / : ; ~) should not be entered")]
        public String OGTitleEN { get; set; }
        [Display(Name = "OG Image (TH)"), MaxLength(250), Required]
        [RegularExpression("^[^<>!@#%/:;~]+$", ErrorMessage = "Special character (^ < > ! @ # / : ; ~) should not be entered")]
        public String OGImageTH { get; set; }
        [Display(Name = "OG Image (EN)"), MaxLength(250), Required]
        [RegularExpression("^[^<>!@#%/:;~]+$", ErrorMessage = "Special character (^ < > ! @ # / : ; ~) should not be entered")]
        public String OGImageEN { get; set; }
        [Display(Name = "OG Description (TH)"), DataType(DataType.MultilineText), Required]
        [RegularExpression("^[^<>!@#%/:;~]+$", ErrorMessage = "Special character (^ < > ! @ # / : ; ~) should not be entered")]
        public String OGDescriptionTH { get; set; }
        [Display(Name = "OG Description (EN)"), DataType(DataType.MultilineText), Required]
        [RegularExpression("^[^<>!@#%/:;~]+$", ErrorMessage = "Special character (^ < > ! @ # / : ; ~) should not be entered")]
        public String OGDescriptionEN { get; set; }
        public String Data1 { get; set; }
        public String Data2 { get; set; }
        public String Data3 { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateAdminId { get; set; }
        public ApproveStatus ApproveStatus { get; set; }
        public string ApproveAdminId { get; set; }

        public string UpdateAdmin { get; set; }
        public string ApproveAdmin { get; set; }
    }

    public enum ConfigPageName {
        Privilege = 1,
        AboutUs = 2,
        ContactUs = 3,
        ClaimProcess = 4,
        PaymentMethod = 5,
        POI = 6,

        Privilege_Draft = 901,
        AboutUs_Draft = 902,
        ContactUs_Draft = 903,
        ClaimProcess_Draft = 904,
        PaymentMethod_Draft = 905,
    }
}