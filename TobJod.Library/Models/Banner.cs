﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace TobJod.Models {
    public class Banner : BaseModel {
        public int Id { get; set; }
        [Display(Name = "Image (TH)"), MaxLength(250)]
        public string ImageTH { get; set; }
        [Display(Name = "Image (EN)"), MaxLength(250)]
        public string ImageEN { get; set; }
        [Display(Name = "Image Mobile (TH)"), MaxLength(250)]
        public string ImageMobileTH { get; set; }
        [Display(Name = "Image Mobile (EN)"), MaxLength(250)]
        public string ImageMobileEN { get; set; }
        [Display(Name = "Url (TH)"), MaxLength(250), Required]
        //[RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string UrlTH { get; set; }
        [Display(Name = "Url (EN)"), MaxLength(250), Required]
        //[RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string UrlEN { get; set; }
        [Display(Name = "Active Date"), DataType(DataType.Date), Required]
        public DateTime ActiveDate { get; set; }
        [Display(Name = "Expire Date"), DataType(DataType.Date), IsDateAfter("ActiveDate", ErrorMessage ="Expire Date must be greater than Active Date"), Required]
        public DateTime ExpireDate { get; set; }
        [Display(Name = "Order"), Required]
        public float Sequence { get; set; }
        public ApproveStatus ApproveStatus { get; set; }
        [Required]
        public Status Status { get; set; }
        [Display(Name = "Owner")]
        public string OwnerAdminId { get; set; }
        public string ApproveAdminId { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateAdminId { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateAdminId { get; set; }

        public string OwnerAdmin { get; set; }
        public string ApproveAdmin { get; set; }
        public string CreateAdmin { get; set; }
        public string UpdateAdmin { get; set; }
    }
}
