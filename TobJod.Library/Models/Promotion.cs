﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace TobJod.Models {
    public class Promotion {
        public int Id { get; set; }
        public int ParentId { get; set; }
        [Display(Name = "Code"), MaxLength(50), Required]
        public string Code { get; set; }
        [Display(Name = "Title (TH)"), MaxLength(250), Required]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string TitleTH { get; set; }
        [Display(Name = "Title (EN)"), MaxLength(250), Required]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string TitleEN { get; set; }
        [Display(Name = "Body (TH)"), DataType(DataType.MultilineText), AllowHtml, DisplayFormat(ConvertEmptyStringToNull = false), Required]
        public string BodyTH { get; set; }
        [Display(Name = "Body (EN)"), DataType(DataType.MultilineText), AllowHtml, DisplayFormat(ConvertEmptyStringToNull = false), Required]
        public string BodyEN { get; set; }
        [Display(Name = "Promotion Type"), Required]
        public PromotionType PromotionType { get; set; }
        [Display(Name = "Payment Type"), Required]
        public PromotionPaymentType PaymentType { get; set; }
        [Display(Name = "Discount Type"), Required]
        public PromotionDiscountType DiscountType { get; set; }
        [Display(Name = "Discount Amount"), Required]
        public decimal DiscountAmount { get; set; }
        [Display(Name = "Product Type"), Required]
        public PromotionProductType ProductType { get; set; }
        [Display(Name = "Active Date"), DataType(DataType.Date), Required]
        public DateTime ActiveDate { get; set; }
        [Display(Name = "Expire Date"), DataType(DataType.Date), Required]
        public DateTime ExpireDate { get; set; }
        [Display(Name = "Approve Status"), Required]
        public ApproveStatus ApproveStatus { get; set; }
        [Required]
        public Status Status { get; set; }
        [Display(Name = "Owner")]
        public string OwnerAdminId { get; set; }
        public string ApproveAdminId { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateAdminId { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateAdminId { get; set; }
        
        public string OwnerAdmin { get; set; }
        public string ApproveAdmin { get; set; }
        public string CreateAdmin { get; set; }
        public string UpdateAdmin { get; set; }

        public int ChildId { get; set; }

        [Display(Name = "Gift")]
        public List<PromotionGift> PromotionGifts { get; set; }
        [Display(Name = "Bank")]
        public List<PromotionBank> PromotionBanks { get; set; }
        [Display(Name = "Product")]
        public List<PromotionProduct> PromotionProducts { get; set; }
        [Display(Name = "Product Category")]
        public List<PromotionProductCategory> PromotionProductCategories { get; set; }
        [Display(Name = "Company")]
        public List<PromotionCompany> PromotionCompanies { get; set; }

        public Promotion() {
            PromotionGifts = new List<PromotionGift>() { new PromotionGift() };
            PromotionProducts = new List<PromotionProduct>();
            PromotionProductCategories = new List<PromotionProductCategory>();
            PromotionCompanies = new List<PromotionCompany>();
        }
    }

    public class PromotionItem: Promotion {
        public string Campaign { get; set; }
        public int ProductId { get; set; }
        public string Gift { get; set; }
        public int CampaignId { get; set; }
    }

    public class PromotionGift {
        public int Id { get; set; }
        public int PromotionId { get; set; }
        [Display(Name = "Title (TH)"), MaxLength(250), Required]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string TitleTH { get; set; }
        [Display(Name = "Title (EN)"), MaxLength(250), Required]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string TitleEN { get; set; }
    }

    public class PromotionBank {
        public int PromotionId { get; set; }
        public int BankId { get; set; }
        public PromotionDiscountType DiscountType { get; set; }
        public decimal DiscountAmount { get; set; }
        public bool IsInstallment { get; set; }
        public int InstallmentMonth { get; set; }
        
        public string Bank { get; set; }
    }

    public class PromotionProduct : Product {
        public int PromotionId { get; set; }
        public int ProductId { get; set; }

        public string Product { get; set; }
    }

    public class PromotionProductCategory : ProductCategory {
        public int PromotionId { get; set; }
        public int ProductCategoryId { get; set; }

        public string ProductCategory { get; set; }
    }
    
    public class PromotionCompany : Company {
        public int PromotionId { get; set; }
        public int CompanyId { get; set; }

        public string Company { get; set; }
    }

    public enum PromotionType {
        [Display(Name = "Discount", Order = 2)]
        Discount = 1,
        [Display(Name = "Gift", Order = 1)]
        Gift = 2
    }

    public enum PromotionPaymentType {
        [Display(Name = "All")]
        All = 1, 
        [Display(Name = "Credit Card")]
        CreditCard = 2,
        [Display(Name = "Counter Service")]
        CounterService = 3
    }

    public enum PromotionDiscountType {
        [Display(Name = "Percent")]
        Percent = 1,
        [Display(Name = "Amount")]
        Amount = 2
    }

    public enum PromotionProductType {
        [Display(Name = "All", Order = 4)]
        All = 1,
        [Display(Name = "By Product", Order = 1)]
        ByProduct = 2,
        [Display(Name = "By Category", Order = 2)]
        ByCategory = 3,
        [Display(Name = "By Company", Order = 3)]
        ByCompany = 4,
    }

}
