﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TobJod.Models {
    public class ProductCompareLog {
        public int Id { get; set; }
        public int MemberId { get; set; }
        public string SessionId { get; set; }
        public int ProductId1 { get; set; }
        public int ProductId2 { get; set; }
        public int ProductId3 { get; set; }
        public int PremiumId1 { get; set; }
        public int PremiumId2 { get; set; }
        public int PremiumId3 { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
