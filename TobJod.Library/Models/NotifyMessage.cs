﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TobJod.Models {
    public class NotifyMessage {
        public string Title { get; set; }
        public string Message { get; set; }
        public NotifyMessageType MessageType { get; set; }
    }

    public enum NotifyMessageType {
        Info,
        Success,
        Error,
        Warning 
    }
}
