﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TobJod.Models {
    public class ProductPrivilege {
        public int Id { get; set; }
        [Display(Name = "Category"), Required]
        public ProductCategoryKey CategoryId { get; set; }
        [Display(Name = "Title (TH)"), MaxLength(250), Required]
        [RegularExpression("^[^<>!@#%/:;~]+$", ErrorMessage = "Special character (^ < > ! @ # / : ; ~) should not be entered")]
        public string TitleTH { get; set; }
        [Display(Name = "Title (EN)"), MaxLength(250), Required]
        [RegularExpression("^[^<>!@#%/:;~]+$", ErrorMessage = "Special character (^ < > ! @ # / : ; ~) should not be entered")]
        public string TitleEN { get; set; }
        public int Mode { get; set; }
        [Required]
        public float Sequence { get; set; }
        [Required]
        public Status Status { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateAdminId { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateAdminId { get; set; }

        public string CreateAdmin { get; set; }
        public string UpdateAdmin { get; set; }

        public string Category { get; set; }
        public string Code { get { return ((int)CategoryId).ToString().Substring(0, 2) + Id.ToString("0000"); } }

        public int GetIdFromCode(string code) {
            return Utils.NullUtils.cvInt(code.Substring(2));
        }
    }
    
}
