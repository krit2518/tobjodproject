﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TobJod.Models {
    public class CarBrand {
        public int Id { get; set; }
        [Display(Name = "Title (TH)"), MaxLength(250), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string TitleTH { get; set; }
        [Display(Name = "Title (EN)"), MaxLength(250), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string TitleEN { get; set; }
        [Display(Name = "Code"), MaxLength(250), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string MapName { get; set; }

        [Required]
        public Status Status { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateAdminId { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateAdminId { get; set; }
        
        public string CreateAdmin { get; set; }
        public string UpdateAdmin { get; set; }
    }

    public class CarFamily {
        public int Id { get; set; }
        [Display(Name = "Brand"), Required]
        public int BrandId { get; set; }
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        [Display(Name = "Title (TH)"), MaxLength(250), Required]
        public string TitleTH { get; set; }
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        [Display(Name = "Title (EN)"), MaxLength(250), Required]
        public string TitleEN { get; set; }
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        [Display(Name = "Code"), MaxLength(250), Required]
        public string MapName { get; set; }

        [Required]
        public Status Status { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateAdminId { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateAdminId { get; set; }

        public string CreateAdmin { get; set; }
        public string UpdateAdmin { get; set; }

        public string Brand { get; set; }
    }

    public class CarModel {
        public int Id { get; set; }
        [Display(Name = "Family"), Required]
        public int FamilyId { get; set; }
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        [Display(Name = "Title (TH)"), MaxLength(250), Required]
        public string TitleTH { get; set; }
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        [Display(Name = "Title (EN)"), MaxLength(250), Required]
        public string TitleEN { get; set; }
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        [Display(Name = "Code"), MaxLength(250), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string MapName { get; set; }
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        [Display(Name = "Spec"), MaxLength(250), Required]
        public string Spec { get; set; }
        [Display(Name = "CC"), Required]
        public int CC { get; set; }
        [Display(Name = "KG"), Required]
        public int KG { get; set; }
        [Display(Name = "Seat"), Range(0, 999), Required]
        public int Seat { get; set; }
        [Display(Name = "Door"), Range(0, 999), Required]
        public int Door { get; set; }
        [Display(Name = "Year"), Range(0, 2500), Required]
        public int Year { get; set; }

        [Required]
        public Status Status { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateAdminId { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateAdminId { get; set; }
        
        public string CreateAdmin { get; set; }
        public string UpdateAdmin { get; set; }

        [Display(Name = "Brand")]
        public int BrandId { get; set; }
        public string Brand { get; set; }
        public string Family { get; set; }
    }
}
