﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TobJod.Models {
    public class Config {
        public ConfigName Id { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }

        public DateTime UpdateDate { get; set; }
        public string UpdateAdminId { get; set; }

        public string UpdateAdmin { get; set; }

    }

    public enum ConfigName {
        //EmailFrom = 80000,
        ProductEmailTo = 80001,
        ContactAddressTH = 80101,
        ContactAddressEN = 80102,
        ContactTelephoneTH = 80111,
        ContactTelephoneEN = 80112,
        ContactFaxTH = 80121,
        ContactFaxEN = 80122,
        ContactEmailTH = 80131,
        ContactEmailEN = 80132,

        EmailImageHeader = 85001,
        EmailImageFooter = 85002,

        GoogleAnalytic = 90001,

        MetaTitleTH = 10001,
        MetaTitleEN = 10002,
        MetaDescriptionTH = 10003,
        MetaDescriptionEN = 10004,
        MetaKeywordTH = 10005,
        MetaKeywordEN = 10006,
        OGTitleTH = 10007,
        OGTitleEN = 10008,
        OGDescriptionTH = 10009,
        OGDescriptionEN = 10010,
        OGImage = 10011,

        HomePromotion1ImageTH = 11011,
        HomePromotion2ImageTH = 11021,
        HomePromotion3ImageTH = 11031,
        HomePromotion4ImageTH = 11041,
        HomePromotion1UrlTH = 11012,
        HomePromotion2UrlTH = 11022,
        HomePromotion3UrlTH = 11032,
        HomePromotion4UrlTH = 11042,
        HomePromotion1ImageEN = 11013,
        HomePromotion2ImageEN = 11023,
        HomePromotion3ImageEN = 11033,
        HomePromotion4ImageEN = 11043,
        HomePromotion1UrlEN = 11014,
        HomePromotion2UrlEN = 11024,
        HomePromotion3UrlEN = 11034,
        HomePromotion4UrlEN = 11044,
        HomePromotion1Target = 11015,
        HomePromotion2Target = 11025,
        HomePromotion3Target = 11035,
        HomePromotion4Target = 11045,
        HomePromotion1ImageMobileTH = 11016,
        HomePromotion2ImageMobileTH = 11026,
        HomePromotion3ImageMobileTH = 11036,
        HomePromotion4ImageMobileTH = 11046,
        HomePromotion1ImageMobileEN = 11017,
        HomePromotion2ImageMobileEN = 11027,
        HomePromotion3ImageMobileEN = 11037,
        HomePromotion4ImageMobileEN = 11047,


        BannerDuration = 10092,
        BannerTransition = 10093,

        ProductLayout = 10100,

        ProductMotorPriceMin = 10101,
        ProductMotorPriceMax = 10102,
        ProductHomePriceMin = 10601,
        ProductHomePriceMax = 10602,
        ProductHomeYearMax = 10606,
        ProductPAPriceMin = 10301,
        ProductPAPriceMax = 10302,
        ProductPHPriceMin = 10501,
        ProductPHPriceMax = 10502,
        ProductTAPriceMin = 10401,
        ProductTAPriceMax = 10402,
        ProductTAPersonMax = 10406,

        ProductPRB110 = 19110,
        ProductPRB120 = 19120,
        ProductPRB210 = 19210,
        ProductPRB320 = 19320,

        //EnableCaptcha = 12001,

        OrderExpireWarning = 21000,
        OrderExpireNotify1 = 21001,
        OrderExpireNotify2 = 21002,
        OrderExpireNotify3 = 21003,

        PaymentExpireDay = 20001
    }
}