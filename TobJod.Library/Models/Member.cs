﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TobJod.Models {
    public class Member {
        public int Id { get; set; }
        [Display(Name = "อีเมล"), MaxLength(250), Required]
        [RegularExpression("^[^<>~\\u005E:;%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; % & ' \" {{ }} [ ]) should not be entered")]
        public string Email { get; set; }
        public string Member_Password { get; set; }
        public string LoginRefId { get; set; }
        public MemberLoginChannel LoginChannel { get; set; }
        [Display(Name="ชื่อ"), MaxLength(50), Required]
        [RegularExpression("^[^<>~\\u005E:;%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; % & ' \" {{ }} [ ]) should not be entered")]
        public string Name { get; set; }
        [Display(Name = "นามสกุล"), MaxLength(50), Required]
        [RegularExpression("^[^<>~\\u005E:;%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; % & ' \" {{ }} [ ]) should not be entered")]
        public string Surname { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Display(Name = "โทรศัพท์"), MaxLength(50)]
        [RegularExpression("^[^<>~\\u005E:;%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; % & ' \" {{ }} [ ]) should not be entered")]
        public string Telephone { get; set; }
        [Display(Name = "เพศ")]
        public Sex Sex { get; set; }
        [Display(Name = "วันเกิด")]
        public DateTime Birthday { get; set; }
        [Display(Name = "รูป")]
        public string Image { get; set; }
        [Display(Name = "สถานะสมรส")]
        public MemberMaritalStatus MaritalStatus { get; set; }
        [Display(Name = "อาชีพ")]
        public int CareerId { get; set; }
        [Display(Name = "สัญชาติ")]
        public int NationalId { get; set; }
        [Display(Name = "น้ำหนัก")]
        public int Weight { get; set; }
        [Display(Name = "ส่วนสูง")]
        public int Height { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Display(Name = "บ้านเลขที่"), MaxLength(50)]
        [RegularExpression("^[^<>~\\u005E:;%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; % & ' \" {{ }} [ ]) should not be entered")]
        public string AddressNo { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Display(Name = "หมู่"), MaxLength(50)]
        [RegularExpression("^[^<>~\\u005E:;%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; % & ' \" {{ }} [ ]) should not be entered")]
        public string AddressMoo { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Display(Name = "หมู่บ้าน/อาคาร"), MaxLength(50)]
        [RegularExpression("^[^<>~\\u005E:;%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; % & ' \" {{ }} [ ]) should not be entered")]
        public string AddressVillage { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Display(Name = "ชั้น"), MaxLength(50)]
        [RegularExpression("^[^<>~\\u005E:;%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; % & ' \" {{ }} [ ]) should not be entered")]
        public string AddressFloor { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Display(Name = "ซอย"), MaxLength(50)]
        [RegularExpression("^[^<>~\\u005E:;%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; % & ' \" {{ }} [ ]) should not be entered")]
        public string AddressSoi { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Display(Name = "ถนน"), MaxLength(50)]
        [RegularExpression("^[^<>~\\u005E:;%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; % & ' \" {{ }} [ ]) should not be entered")]
        public string AddressRoad { get; set; }
        [Display(Name = "จังหวัด")]
        public int AddressProvinceId { get; set; }
        [Display(Name = "เขต/อำเภอ")]
        public int AddressDistrictId { get; set; }
        [Display(Name = "แขวง/ตำบล")]
        public int AddressSubDistrictId { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [Display(Name = "รหัสไปรษณีย์"), MaxLength(5)]
        [RegularExpression("^[0-9]{5}$", ErrorMessage = "Only number is allowed")]
        public string AddressPostalCode { get; set; }
        public int LoginAttempt { get; set; }
        public MemberStatus Status { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }

        [Display(Name = "อาชีพ")]
        public String Career { get; set; }
        [Display(Name = "สัญชาติ")]
        public String National { get; set; }
        [Display(Name = "จังหวัด")]
        public String AddressProvince { get; set; }
        [Display(Name = "เขต/อำเภอ")]
        public String AddressDistrict { get; set; }
        [Display(Name = "แขวง/ตำบล")]
        public String AddressSubDistrict { get; set; }
    }

    public class MemberForgotPassword {
        public int Id { get; set; }
        public int MemberId { get; set; }
        public string Token { get; set; }
        public DateTime ExpireDate { get; set; }
        public Status Status { get; set; }
        public DateTime CreateDate { get; set; }
    }

    public class MemberLogin {
        public int MemberId { get; set; }
        public string IPPublic { get; set; }
        public string IPLocal { get; set; }
        public Status Status { get; set; }
        public DateTime CreateDate { get; set; }
    }

    public class MemberPassword {
        public int Id { get; set; }
        public int MemberId { get; set; }
        public string Member_Password { get; set; }
        public DateTime CreateDate { get; set; }
    }

    public enum MemberLoginChannel {
        Email = 1,
        Facebook = 2,
        Google = 3
    }

    public enum MemberStatus {
        Pending = 0,
        Inactive = 1,
        Active = 2
    }

    public enum MemberMaritalStatus {
        [Display(Name = "โสด")]
        Single = 1,
        [Display(Name = "สมรส")]
        Married = 2,
        [Display(Name = "อื่นๆ")]
        None = 0,
        //[Display(Name = "หย่าร้าง")]
        //Divorced = 3,
        //[Display(Name = "หม้าย")]
        //Widowed = 4,
    }
    /*
    

     */
}
