﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace TobJod.Models {
    public class PaymentMethod {
        public int Id { get; set; }
        [Display(Name = "Title (TH)"), MaxLength(250), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public String TitleTH { get; set; }
        [Display(Name = "Title (EN)"), MaxLength(250), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public String TitleEN { get; set; }
        [Display(Name = "Url (TH)"), MaxLength(250), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public String UrlTH { get; set; }
        [Display(Name = "Url (EN)"), MaxLength(250), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public String UrlEN { get; set; }
        [Display(Name = "Brief (TH)"), DataType(DataType.MultilineText), AllowHtml, Required]
        public String BriefTH { get; set; }
        [Display(Name = "Brief (EN)"), DataType(DataType.MultilineText), AllowHtml, Required]
        public String BriefEN { get; set; }
        [Display(Name = "Body (TH)"), DataType(DataType.MultilineText), AllowHtml, Required]
        public String BodyTH { get; set; }
        [Display(Name = "Body (EN)"), DataType(DataType.MultilineText), AllowHtml, Required]
        public String BodyEN { get; set; }
        [Display(Name = "Image (TH)"), MaxLength(250)]
        public String ImageTH { get; set; }
        [Display(Name = "Image (EN)"), MaxLength(250)]
        public String ImageEN { get; set; }
        [Display(Name = "Image Title (TH)"), MaxLength(250), Required]
        [RegularExpression("^[^<>.!@#%/:;~]+$", ErrorMessage = "Special character (^ < > . ! @ # / : ; ~) should not be entered")]
        public String ImageTitleTH { get; set; }
        [Display(Name = "Image Title (EN)"), MaxLength(250), Required]
        [RegularExpression("^[^<>.!@#%/:;~]+$", ErrorMessage = "Special character (^ < > . ! @ # / : ; ~) should not be entered")]
        public String ImageTitleEN { get; set; }
        [Display(Name = "Image Alternate (TH)"), MaxLength(250), Required]
        [RegularExpression("^[^<>.!@#%/:;~]+$", ErrorMessage = "Special character (^ < > . ! @ # / : ; ~) should not be entered")]
        public String ImageAltTH { get; set; }
        [Display(Name = "Image Alternate (EN)"), MaxLength(250), Required]
        [RegularExpression("^[^<>.!@#%/:;~]+$", ErrorMessage = "Special character (^ < > . ! @ # / : ; ~) should not be entered")]
        public String ImageAltEN { get; set; }
        [Display(Name = "Meta Title (TH)"), MaxLength(250), Required]
        [RegularExpression("^[^<>.!@#%/:;~]+$", ErrorMessage = "Special character (^ < > . ! @ # / : ; ~) should not be entered")]
        public String MetaTitleTH { get; set; }
        [Display(Name = "Meta Title (EN)"), MaxLength(250), Required]
        [RegularExpression("^[^<>.!@#%/:;~]+$", ErrorMessage = "Special character (^ < > . ! @ # / : ; ~) should not be entered")]
        public String MetaTitleEN { get; set; }
        [Display(Name = "Meta Keyword (TH)"), DataType(DataType.MultilineText), Required]
        [RegularExpression("^[^<>.!@#%/:;~]+$", ErrorMessage = "Special character (^ < > . ! @ # / : ; ~) should not be entered")]
        public String MetaKeywordTH { get; set; }
        [Display(Name = "Meta Keyword (EN)"), DataType(DataType.MultilineText), Required]
        [RegularExpression("^[^<>.!@#%/:;~]+$", ErrorMessage = "Special character (^ < > . ! @ # / : ; ~) should not be entered")]
        public String MetaKeywordEN { get; set; }
        [Display(Name = "Meta Description (TH)"), DataType(DataType.MultilineText), Required]
        [RegularExpression("^[^<>.!@#%/:;~]+$", ErrorMessage = "Special character (^ < > . ! @ # / : ; ~) should not be entered")]
        public String MetaDescriptionTH { get; set; }
        [Display(Name = "Meta Description (EN)"), DataType(DataType.MultilineText), Required]
        [RegularExpression("^[^<>.!@#%/:;~]+$", ErrorMessage = "Special character (^ < > . ! @ # / : ; ~) should not be entered")]
        public String MetaDescriptionEN { get; set; }
        [Display(Name = "OG Title (TH)"), MaxLength(250), Required]
        [RegularExpression("^[^<>.!@#%/:;~]+$", ErrorMessage = "Special character (^ < > . ! @ # / : ; ~) should not be entered")]
        public String OGTitleTH { get; set; }
        [Display(Name = "OG Title (EN)"), MaxLength(250), Required]
        [RegularExpression("^[^<>.!@#%/:;~]+$", ErrorMessage = "Special character (^ < > . ! @ # / : ; ~) should not be entered")]
        public String OGTitleEN { get; set; }
        [Display(Name = "OG Image (TH)"), MaxLength(250), Required]
        [RegularExpression("^[^<>.!@#%/:;~]+$", ErrorMessage = "Special character (^ < > . ! @ # / : ; ~) should not be entered")]
        public String OGImageTH { get; set; }
        [Display(Name = "OG Image (EN)"), MaxLength(250), Required]
        [RegularExpression("^[^<>.!@#%/:;~]+$", ErrorMessage = "Special character (^ < > . ! @ # / : ; ~) should not be entered")]
        public String OGImageEN { get; set; }
        [Display(Name = "OG Description (TH)"), DataType(DataType.MultilineText), Required]
        [RegularExpression("^[^<>.!@#%/:;~]+$", ErrorMessage = "Special character (^ < > . ! @ # / : ; ~) should not be entered")]
        public String OGDescriptionTH { get; set; }
        [Display(Name = "OG Description (EN)"), DataType(DataType.MultilineText), Required]
        [RegularExpression("^[^<>.!@#%/:;~]+$", ErrorMessage = "Special character (^ < > . ! @ # / : ; ~) should not be entered")]
        public String OGDescriptionEN { get; set; }
        public List<Tag> Tags { get; set; }
        public ApproveStatus ApproveStatus { get; set; }
        [Required]
        public Status Status { get; set; }
        [Display(Name = "Owner")]
        public string OwnerAdminId { get; set; }
        public string ApproveAdminId { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateAdminId { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateAdminId { get; set; }

        [Display(Name = "Category")]
        public NewsCategory NewsCategory { get; set; }
        [Display(Name = "Sub Category")]
        public string NewsSubCategory { get; set; }
        public string OwnerAdmin { get; set; }
        public string ApproveAdmin { get; set; }
        public string CreateAdmin { get; set; }
        public string UpdateAdmin { get; set; }
    }
    
}
