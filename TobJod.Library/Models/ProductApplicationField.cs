﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TobJod.Models {
    public class ProductApplicationField {
        public ProductApplicationFieldKey Id { get; set; }
        public ProductCategoryKey ProductCategoryId { get; set; }
        [Display(Name = "Title (TH)"), MaxLength(250), Required]
        [RegularExpression("^[^<>!@#%~]+$", ErrorMessage = "Special character (^ < > ! @ # % ~) should not be entered")]
        public string TitleTH { get; set; }
        [Display(Name = "Title (EN)"), MaxLength(250), Required]
        [RegularExpression("^[^<>!@#%~]+$", ErrorMessage = "Special character (^ < > ! @ # % ~) should not be entered")]
        public string TitleEN { get; set; }
        public ProductApplicationFieldDataType DataType { get; set; }
        public int RefId { get; set; } //MasterOptionCategory
        public ProductApplicationFieldMode Mode { get; set; }
        [Display(Name = "Step"), Range(1, 10)]
        [RegularExpression("^[0-9]+$", ErrorMessage = "Only 0-9 is allowed")]
        public int Step { get; set; }
        [Display(Name = "Page"), Range(1, 10)]
        [RegularExpression("^[0-9]+$", ErrorMessage = "Only 0-9 is allowed")]
        public int Page { get; set; }
        [Display(Name = "Order"), Range(1, 100)]
        [RegularExpression("^[0-9]+$", ErrorMessage = "Only 0-9 is allowed")]
        public int Sequence { get; set; }
        
        [Required]
        public Status Status { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateAdminId { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateAdminId { get; set; }

        public string CreateAdmin { get; set; }
        public string UpdateAdmin { get; set; }
    }

    public enum ProductApplicationFieldKey {

        #region PRB
        PRB_CarBrand = 200001,
        PRB_CarFamily = 200002,
        PRB_CarModel = 200003,
        PRB_CarRegisterYear = 200004,
        PRB_CarEngineNo = 200005,
        PRB_CarSerialNo = 200006,
        PRB_CarPlateNo = 200007,
        PRB_CarProvinceId = 200008,
        PRB_CarUsageTypeCode = 200009,
        PRB_CarUsageType = 200010,
        PRB_CarUsageCode = 200011,
        PRB_CarCC = 200012,
        PRB_CarWeight = 200013,
        PRB_CarAccessories = 200014,
        PRB_CarAccessoryValue = 200015,
        PRB_CarCCTV = 200016,
        PRB_CarGarageType = 200017,
        PRB_CustomerType = 200018,
        PRB_TitleId = 200019,
        PRB_Name = 200020,
        PRB_Surname = 200021,
        PRB_NationalId = 200022,
        PRB_IDType = 200023,
        PRB_IDCard = 200024,
        PRB_Birthday = 200025,
        PRB_Age = 200026,
        PRB_CareerId = 200027,
        PRB_AnnualIncome = 200028,
        PRB_Sex = 200029,
        PRB_MaritalStatus = 200030,
        PRB_Weight = 200031,
        PRB_Height = 200032,
        PRB_Telephone = 200033,
        PRB_MobilePhone = 200034,
        PRB_Email = 200035,
        PRB_LineId = 200036,
        PRB_Instagram = 200037,
        PRB_Facebook = 200038,
        PRB_BeneficiaryName = 200039,
        PRB_BeneficiarySurname = 200040,
        PRB_BeneficiaryIDCard = 200041,
        PRB_BeneficiaryRelationship = 200042,
        PRB_BeneficiaryMobilePhone = 200043,
        PRB_EPolicy = 200044,
        PRB_ActiveDate = 200045,
        PRB_ExpireDate = 200046,
        PRB_Bank = 200047,
        PRB_Driver_TitleId = 200048,
        PRB_Driver_Name = 200049,
        PRB_Driver_Surname = 200050,
        PRB_Driver_IDType = 200051,
        PRB_Driver_IDCard = 200052,
        PRB_Driver_RefIDCard = 200053,
        PRB_Driver_NationalId = 200054,
        PRB_Driver_Birthday = 200055,
        PRB_ContactName = 200056,
        PRB_ContactSurname = 200057,
        PRB_ContactTelephone = 200058,
        PRB_ContactMobilePhone = 200059,
        PRB_ContactEmail = 200060,
        PRB_BuyerName = 200061,
        PRB_BuyerSurname = 200062,
        PRB_BuyerTelephone = 200063,
        PRB_BuyerMobilePhone = 200064,
        PRB_BuyerEmail = 200065,
        PRB_AddressBranchNo = 200066,
        PRB_AddressNo = 200067,
        PRB_AddressMoo = 200068,
        PRB_AddressVillage = 200069,
        PRB_AddressFloor = 200070,
        PRB_AddressSoi = 200071,
        PRB_AddressRoad = 200072,
        PRB_AddressProvince = 200073,
        PRB_AddressDistrict = 200074,
        PRB_AddressSubDistrict = 200075,
        PRB_AddressPostalCode = 200076,
        PRB_BillingAddressBranchNo = 200077,
        PRB_BillingAddressNo = 200078,
        PRB_BillingAddressMoo = 200079,
        PRB_BillingAddressVillage = 200080,
        PRB_BillingAddressFloor = 200081,
        PRB_BillingAddressSoi = 200082,
        PRB_BillingAddressRoad = 200083,
        PRB_BillingAddressProvince = 200084,
        PRB_BillingAddressDistrict = 200085,
        PRB_BillingAddressSubDistrict = 200086,
        PRB_BillingAddressPostalCode = 200087,
        PRB_ShippingAddressBranchNo = 200088,
        PRB_ShippingAddressNo = 200089,
        PRB_ShippingAddressMoo = 200090,
        PRB_ShippingAddressVillage = 200091,
        PRB_ShippingAddressFloor = 200092,
        PRB_ShippingAddressSoi = 200093,
        PRB_ShippingAddressRoad = 200094,
        PRB_ShippingAddressProvince = 200095,
        PRB_ShippingAddressDistrict = 200096,
        PRB_ShippingAddressSubDistrict = 200097,
        PRB_ShippingAddressPostalCode = 200098,
        PRB_FileAttachmentIDCard = 200099,
        PRB_FileAttachmentCCTV = 200100,
        PRB_FileAttachmentCarRegistrationCopy = 200101,
        PRB_FileAttachment4 = 200102,
        PRB_FileAttachment5 = 200103,
        PRB_FileAttachment6 = 200104,
        #endregion

        #region PA
        PA_CustomerType = 300001,
        PA_TitleId = 300002,
        PA_Name = 300003,
        PA_Surname = 300004,
        PA_NationalId = 300005,
        PA_IDType = 300006,
        PA_IDCard = 300007,
        PA_Birthday = 300008,
        PA_Age = 300009,
        PA_CareerId = 300010,
        PA_AnnualIncome = 300011,
        PA_Sex = 300012,
        PA_MaritalStatus = 300013,
        PA_Weight = 300014,
        PA_Height = 300015,
        PA_Telephone = 300016,
        PA_MobilePhone = 300017,
        PA_Email = 300018,
        PA_BeneficiaryName = 300019,
        PA_BeneficiarySurname = 300020,
        PA_BeneficiaryIDType = 300021,
        PA_BeneficiaryIDCard = 300022,
        PA_BeneficiaryRelationship = 300023,
        PA_BeneficiaryMobilePhone = 300024,
        PA_EPolicy = 300025,
        PA_ActiveDate = 300026,
        PA_ExpireDate = 300027,
        PA_ContactName = 300028,
        PA_ContactSurname = 300029,
        PA_ContactTelephone = 300030,
        PA_ContactMobilePhone = 300031,
        PA_ContactEmail = 300032,
        PA_BuyerName = 300033,
        PA_BuyerSurname = 300034,
        PA_BuyerTelephone = 300035,
        PA_BuyerMobilePhone = 300036,
        PA_BuyerEmail = 300037,
        PA_AddressBranchNo = 300038,
        PA_AddressNo = 300039,
        PA_AddressMoo = 300040,
        PA_AddressVillage = 300041,
        PA_AddressFloor = 300042,
        PA_AddressSoi = 300043,
        PA_AddressRoad = 300044,
        PA_AddressProvince = 300045,
        PA_AddressDistrict = 300046,
        PA_AddressSubDistrict = 300047,
        PA_AddressPostalCode = 300048,
        PA_BillingAddressBranchNo = 300049,
        PA_BillingAddressNo = 300050,
        PA_BillingAddressMoo = 300051,
        PA_BillingAddressVillage = 300052,
        PA_BillingAddressFloor = 300053,
        PA_BillingAddressSoi = 300054,
        PA_BillingAddressRoad = 300055,
        PA_BillingAddressProvince = 300056,
        PA_BillingAddressDistrict = 300057,
        PA_BillingAddressSubDistrict = 300058,
        PA_BillingAddressPostalCode = 300059,
        PA_ShippingAddressBranchNo = 300060,
        PA_ShippingAddressNo = 300061,
        PA_ShippingAddressMoo = 300062,
        PA_ShippingAddressVillage = 300063,
        PA_ShippingAddressFloor = 300064,
        PA_ShippingAddressSoi = 300065,
        PA_ShippingAddressRoad = 300066,
        PA_ShippingAddressProvince = 300067,
        PA_ShippingAddressDistrict = 300068,
        PA_ShippingAddressSubDistrict = 300069,
        PA_ShippingAddressPostalCode = 300070,
        PA_FileAttachment1 = 300071,
        PA_FileAttachment2 = 300072,
        PA_FileAttachment3 = 300073,
        PA_FileAttachment4 = 300074,
        PA_FileAttachment5 = 300075,
        PA_FileAttachment6 = 300076,

        #endregion

        #region MOTOR
        Motor_CarBrand = 100001,
        Motor_CarFamily = 100002,
        Motor_CarModel = 100003,
        Motor_CarRegisterYear = 100004,
        Motor_CarEngineNo = 100005,
        Motor_CarSerialNo = 100006,
        Motor_CarPlateNo = 100007,
        Motor_CarProvinceId = 100008,
        Motor_CarUsageTypeCode = 100009,
        Motor_CarUsageType = 100010,
        Motor_CarCCTV = 100011,
        Motor_CarGarageType = 100012,
        Motor_CustomerType = 100013,
        Motor_TitleId = 100014,
        Motor_Name = 100015,
        Motor_Surname = 100016,
        Motor_NationalId = 100017,
        Motor_IDType = 100018,
        Motor_IDCard = 100019,
        Motor_Birthday = 100020,
        Motor_Age = 100021,
        Motor_CareerId = 100022,
        Motor_AnnualIncome = 100023,
        Motor_Sex = 100024,
        Motor_MaritalStatus = 100025,
        Motor_Weight = 100026,
        Motor_Height = 100027,
        Motor_Telephone = 100028,
        Motor_MobilePhone = 100029,
        Motor_Email = 100030,
        Motor_LineId = 100031,
        Motor_Instagram = 100032,
        Motor_Facebook = 100033,
        Motor_BeneficiaryName = 100034,
        Motor_BeneficiarySurname = 100035,
        Motor_BeneficiaryIDCard = 100036,
        Motor_BeneficiaryRelationship = 100037,
        Motor_BeneficiaryMobilePhone = 100038,
        Motor_EPolicy = 100039,
        Motor_ActiveDate = 100040,
        Motor_ExpireDate = 100041,
        Motor_Driver_TitleId = 100042,
        Motor_Driver_Name = 100043,
        Motor_Driver_Surname = 100044,
        Motor_Driver_IDType = 100045,
        Motor_Driver_IDCard = 100046,
        Motor_Driver_RefIDCard = 100047,
        Motor_Driver_NationalId = 100048,
        Motor_Driver_Birthday = 100049,
        Motor_ContactName = 100050,
        Motor_ContactSurname = 100051,
        Motor_ContactTelephone = 100052,
        Motor_ContactMobilePhone = 100053,
        Motor_ContactEmail = 100054,
        Motor_BuyerName = 100055,
        Motor_BuyerSurname = 100056,
        Motor_BuyerTelephone = 100057,
        Motor_BuyerMobilePhone = 100058,
        Motor_BuyerEmail = 100059,
        Motor_AddressBranchNo = 100060,
        Motor_AddressNo = 100061,
        Motor_AddressMoo = 100062,
        Motor_AddressVillage = 100063,
        Motor_AddressFloor = 100064,
        Motor_AddressSoi = 100065,
        Motor_AddressRoad = 100066,
        Motor_AddressProvince = 100067,
        Motor_AddressDistrict = 100068,
        Motor_AddressSubDistrict = 100069,
        Motor_AddressPostalCode = 100070,
        Motor_BillingAddressBranchNo = 100071,
        Motor_BillingAddressNo = 100072,
        Motor_BillingAddressMoo = 100073,
        Motor_BillingAddressVillage = 100074,
        Motor_BillingAddressFloor = 100075,
        Motor_BillingAddressSoi = 100076,
        Motor_BillingAddressRoad = 100077,
        Motor_BillingAddressProvince = 100078,
        Motor_BillingAddressDistrict = 100079,
        Motor_BillingAddressSubDistrict = 100080,
        Motor_BillingAddressPostalCode = 100081,
        Motor_ShippingAddressBranchNo = 100082,
        Motor_ShippingAddressNo = 100083,
        Motor_ShippingAddressMoo = 100084,
        Motor_ShippingAddressVillage = 100085,
        Motor_ShippingAddressFloor = 100086,
        Motor_ShippingAddressSoi = 100087,
        Motor_ShippingAddressRoad = 100088,
        Motor_ShippingAddressProvince = 100089,
        Motor_ShippingAddressDistrict = 100090,
        Motor_ShippingAddressSubDistrict = 100091,
        Motor_ShippingAddressPostalCode = 100092,
        Motor_FileAttachmentIDCard = 100093,
        Motor_FileAttachmentCCTV = 100094,
        Motor_FileAttachmentCarRegistrationCopy = 100095,
        Motor_FileAttachment4 = 100096,
        Motor_FileAttachment5 = 100097,
        Motor_FileAttachment6 = 100098,
        #endregion

        #region PH
        PH_CustomerType = 500001,
        PH_TitleId = 500002,
        PH_Name = 500003,
        PH_Surname = 500004,
        PH_NationalId = 500005,
        PH_IDType = 500006,
        PH_IDCard = 500007,
        PH_Birthday = 500008,
        PH_Age = 500009,
        PH_CareerId = 500010,
        PH_AnnualIncome = 500011,
        PH_Sex = 500012,
        PH_MaritalStatus = 500013,
        PH_Weight = 500014,
        PH_Height = 500015,
        PH_Telephone = 500016,
        PH_MobilePhone = 500017,
        PH_Email = 500018,
        PH_BeneficiaryName = 500019,
        PH_BeneficiarySurname = 500020,
        PH_BeneficiaryIDType = 500021,
        PH_BeneficiaryIDCard = 500022,
        PH_BeneficiaryRelationship = 500023,
        PH_BeneficiaryMobilePhone = 500024,
        PH_EPolicy = 500025,
        PH_ActiveDate = 500026,
        PH_ExpireDate = 500027,
        PH_ContactName = 500028,
        PH_ContactSurname = 500029,
        PH_ContactTelephone = 500030,
        PH_ContactMobilePhone = 500031,
        PH_ContactEmail = 500032,
        PH_BuyerName = 500033,
        PH_BuyerSurname = 500034,
        PH_BuyerTelephone = 500035,
        PH_BuyerMobilePhone = 500036,
        PH_BuyerEmail = 500037,
        PH_AddressBranchNo = 500038,
        PH_AddressNo = 500039,
        PH_AddressMoo = 500040,
        PH_AddressVillage = 500041,
        PH_AddressFloor = 500042,
        PH_AddressSoi = 500043,
        PH_AddressRoad = 500044,
        PH_AddressProvince = 500045,
        PH_AddressDistrict = 500046,
        PH_AddressSubDistrict = 500047,
        PH_AddressPostalCode = 500048,
        PH_BillingAddressBranchNo = 500049,
        PH_BillingAddressNo = 500050,
        PH_BillingAddressMoo = 500051,
        PH_BillingAddressVillage = 500052,
        PH_BillingAddressFloor = 500053,
        PH_BillingAddressSoi = 500054,
        PH_BillingAddressRoad = 500055,
        PH_BillingAddressProvince = 500056,
        PH_BillingAddressDistrict = 500057,
        PH_BillingAddressSubDistrict = 500058,
        PH_BillingAddressPostalCode = 500059,
        PH_ShippingAddressBranchNo = 500060,
        PH_ShippingAddressNo = 500061,
        PH_ShippingAddressMoo = 500062,
        PH_ShippingAddressVillage = 500063,
        PH_ShippingAddressFloor = 500064,
        PH_ShippingAddressSoi = 500065,
        PH_ShippingAddressRoad = 500066,
        PH_ShippingAddressProvince = 500067,
        PH_ShippingAddressDistrict = 500068,
        PH_ShippingAddressSubDistrict = 500069,
        PH_ShippingAddressPostalCode = 500070,
        PH_FileAttachment1 = 500071,
        PH_FileAttachment2 = 500072,
        PH_FileAttachment3 = 500073,
        PH_FileAttachment4 = 500074,
        PH_FileAttachment5 = 500075,
        PH_FileAttachment6 = 500076,
        #endregion

        #region TA
        TA_TravelType = 400001,
        TA_University = 400002,
        TA_TravelBy = 400003,
        TA_Country = 400004,
        TA_Arrival = 400005,
        TA_CustomerType = 400006,
        TA_TitleId = 400007,
        TA_Name = 400008,
        TA_Surname = 400009,
        TA_NationalId = 400010,
        TA_IDType = 400011,
        TA_IDCard = 400012,
        TA_Birthday = 400013,
        TA_Age = 400014,
        TA_CareerId = 400015,
        TA_AnnualIncome = 400016,
        TA_Sex = 400017,
        TA_MaritalStatus = 400018,
        TA_Weight = 400019,
        TA_Height = 400020,
        TA_Telephone = 400021,
        TA_MobilePhone = 400022,
        TA_Email = 400023,
        TA_BeneficiaryName = 400024,
        TA_BeneficiarySurname = 400025,
        TA_BeneficiaryIDType = 400026,
        TA_BeneficiaryIDCard = 400027,
        TA_BeneficiaryRelationship = 400028,
        TA_BeneficiaryMobilePhone = 400029,
        TA_EPolicy = 400030,
        TA_ActiveDate = 400031,
        TA_ExpireDate = 400032,
        TA_ContactName = 400033,
        TA_ContactSurname = 400034,
        TA_ContactTelephone = 400035,
        TA_ContactMobilePhone = 400036,
        TA_ContactEmail = 400037,
        TA_BuyerName = 400038,
        TA_BuyerSurname = 400039,
        TA_BuyerTelephone = 400040,
        TA_BuyerMobilePhone = 400041,
        TA_BuyerEmail = 400042,
        TA_AddressBranchNo = 400043,
        TA_AddressNo = 400044,
        TA_AddressMoo = 400045,
        TA_AddressVillage = 400046,
        TA_AddressFloor = 400047,
        TA_AddressSoi = 400048,
        TA_AddressRoad = 400049,
        TA_AddressProvince = 400050,
        TA_AddressDistrict = 400051,
        TA_AddressSubDistrict = 400052,
        TA_AddressPostalCode = 400053,
        TA_BillingAddressBranchNo = 400054,
        TA_BillingAddressNo = 400055,
        TA_BillingAddressMoo = 400056,
        TA_BillingAddressVillage = 400057,
        TA_BillingAddressFloor = 400058,
        TA_BillingAddressSoi = 400059,
        TA_BillingAddressRoad = 400060,
        TA_BillingAddressProvince = 400061,
        TA_BillingAddressDistrict = 400062,
        TA_BillingAddressSubDistrict = 400063,
        TA_BillingAddressPostalCode = 400064,
        TA_ShippingAddressBranchNo = 400065,
        TA_ShippingAddressNo = 400066,
        TA_ShippingAddressMoo = 400067,
        TA_ShippingAddressVillage = 400068,
        TA_ShippingAddressFloor = 400069,
        TA_ShippingAddressSoi = 400070,
        TA_ShippingAddressRoad = 400071,
        TA_ShippingAddressProvince = 400072,
        TA_ShippingAddressDistrict = 400073,
        TA_ShippingAddressSubDistrict = 400074,
        TA_ShippingAddressPostalCode = 400075,
        TA_FileAttachment1 = 400076,
        TA_FileAttachment2 = 400077,
        TA_FileAttachment3 = 400078,
        TA_FileAttachment4 = 400079,
        TA_FileAttachment5 = 400080,
        TA_FileAttachment6 = 400081,

        #endregion

        #region HOME
        Home_PropertyType = 600001,
        Home_LocationUsage = 600002,
        Home_BuyerStatus = 600003,
        Home_ConstructionType = 600004,
        Home_HomeFloor = 600005,
        Home_HomeRoomAmount = 600006,
        Home_SquareMeter = 600007,
        Home_ConstructionValue = 600008,
        Home_FurnitureValue = 600009,
        Home_CoverageYear = 600010,
        Home_HomeAddressBranchNo = 600011,
        Home_HomeAddressNo = 600012,
        Home_HomeAddressMoo = 600013,
        Home_HomeAddressVillage = 600014,
        Home_HomeAddressFloor = 600015,
        Home_HomeAddressSoi = 600016,
        Home_HomeAddressRoad = 600017,
        Home_HomeAddressProvince = 600018,
        Home_HomeAddressDistrict = 600019,
        Home_HomeAddressSubDistrict = 600020,
        Home_HomeAddressPostalCode = 600021,
        Home_CustomerType = 600022,
        Home_TitleId = 600023,
        Home_Name = 600024,
        Home_Surname = 600025,
        Home_NationalId = 600026,
        Home_IDType = 600027,
        Home_IDCard = 600028,
        Home_Birthday = 600029,
        Home_Age = 600030,
        Home_CareerId = 600031,
        Home_AnnualIncome = 600032,
        Home_Sex = 600033,
        Home_MaritalStatus = 600034,
        Home_Weight = 600035,
        Home_Height = 600036,
        Home_Telephone = 600037,
        Home_MobilePhone = 600038,
        Home_Email = 600039,
        Home_BeneficiaryName = 600040,
        Home_BeneficiarySurname = 600041,
        Home_BeneficiaryIDType = 600042,
        Home_BeneficiaryIDCard = 600043,
        Home_BeneficiaryRelationship = 600044,
        Home_BeneficiaryMobilePhone = 600045,
        Home_EPolicy = 600046,
        Home_ActiveDate = 600047,
        Home_ExpireDate = 600048,
        Home_ContactName = 600049,
        Home_ContactSurname = 600050,
        Home_ContactTelephone = 600051,
        Home_ContactMobilePhone = 600052,
        Home_ContactEmail = 600053,
        Home_BuyerName = 600054,
        Home_BuyerSurname = 600055,
        Home_BuyerTelephone = 600056,
        Home_BuyerMobilePhone = 600057,
        Home_BuyerEmail = 600058,
        Home_AddressBranchNo = 600059,
        Home_AddressNo = 600060,
        Home_AddressMoo = 600061,
        Home_AddressVillage = 600062,
        Home_AddressFloor = 600063,
        Home_AddressSoi = 600064,
        Home_AddressRoad = 600065,
        Home_AddressProvince = 600066,
        Home_AddressDistrict = 600067,
        Home_AddressSubDistrict = 600068,
        Home_AddressPostalCode = 600069,
        Home_BillingAddressBranchNo = 600070,
        Home_BillingAddressNo = 600071,
        Home_BillingAddressMoo = 600072,
        Home_BillingAddressVillage = 600073,
        Home_BillingAddressFloor = 600074,
        Home_BillingAddressSoi = 600075,
        Home_BillingAddressRoad = 600076,
        Home_BillingAddressProvince = 600077,
        Home_BillingAddressDistrict = 600078,
        Home_BillingAddressSubDistrict = 600079,
        Home_BillingAddressPostalCode = 600080,
        Home_ShippingAddressBranchNo = 600081,
        Home_ShippingAddressNo = 600082,
        Home_ShippingAddressMoo = 600083,
        Home_ShippingAddressVillage = 600084,
        Home_ShippingAddressFloor = 600085,
        Home_ShippingAddressSoi = 600086,
        Home_ShippingAddressRoad = 600087,
        Home_ShippingAddressProvince = 600088,
        Home_ShippingAddressDistrict = 600089,
        Home_ShippingAddressSubDistrict = 600090,
        Home_ShippingAddressPostalCode = 600091,
        Home_FileAttachment1 = 600092,
        Home_FileAttachment2 = 600093,
        Home_FileAttachment3 = 600094,
        Home_FileAttachment4 = 600095,
        Home_FileAttachment5 = 600096,
        Home_FileAttachment6 = 600097,
        #endregion


        //[Display(Name = "Motor: Name")]
        //Motor_Name = 100001,

        //[Display(Name = "Motor: Province")]
        //Motor_Province = 100010,
        //[Display(Name = "Motor: District")]
        //Motor_District = 100011,
        //[Display(Name = "Motor: SubDistrict")]
        //Motor_SubDistrict = 100012,
        //[Display(Name = "Motor: ZipCode")]
        //Motor_ZipCode = 100013,


        //[Display(Name = "PA: Name")]
        //PA_Name = 300001,
    }

    public enum ProductApplicationFieldDataType {
        String = 1,
        Int = 2,
        Decimal = 3,
        MasterOption = 4,
        Date = 5
    }

    [Flags]
    public enum ProductApplicationFieldMode {
        SequenceEditable = 1,
        StepEditable = 2
    }

    public enum ProductApplicationStep 
    {
        Start = 1,
        PolicyHolderForm = 2,
        DriverForm = 3,
        AddressForm = 4,
        AttachmentForm = 5,
        Final = 0,
        Success = 9,
        Unknown = -1,
    }

    public class ProductApplicationStage
    {
        public class Base
        {
            public const int Final = 0;
            public const int Success = 9;
            public const int Unknown = -1;
        }
        public class Motor : Base
        {
            public const int MotorForm = 1;
            public const int PolicyHolderForm = 2;
            public const int DriverForm = 3;
            public const int AddressForm = 4;
            public const int AttachmentForm = 5;
        }
        public class PRB : Motor
        {
            public const int PRBForm = 1;
        }
        public class Home : Base
        {
            public const int HomeForm = 1;
            public const int PolicyHolderForm = 2;
            public const int AddressForm = 3;
            public const int AttachmentForm = 4;
        }
        public class PA : Base
        {
            public const int PolicyHolderForm = 1;
            public const int AddressForm = 2;
            public const int AttachmentForm = 3;
        }

        public class TA : Base
        {
            public const int TAForm = 1;
            public const int PolicyHolderForm = 2;
            public const int AddressForm = 3;
            public const int AttachmentForm = 4;
        }

        public class PH : Base
        {
            public const int PolicyHolderForm = 1;
            public const int AddressForm = 2;
            public const int AttachmentForm = 3;
        }

        
    }

    public enum ProductApplicationPage
    {
        Motor = 1,
        Home = 1,
        Home_Address = 10,
        PA = 1,
        TA = 1,
        PH = 1,
        PRB = 1,
        PolicyHolderForm = 2,
        DriverForm = 3,
        AddressForm_1 = 4,
        AddressForm_2 = 5,
        AddressForm_3 = 6,
        AttachmentForm = 7,
        BuyerForm = 8,
        ContactForm = 9,
    }

    public enum MaritalStatus
    {
        [Display(Name = "โสด")]
        Single = 1,
        [Display(Name = "สมรส")]
        Married = 2,
        [Display(Name = "หย่าร้าง")]
        Divorced = 3,
        [Display(Name = "หม้าย")]
        Widowed = 4,
    }

    public enum CustomerType
    {
        [Display(Name = "นิติบุคคล")]
        Corporate = 1,
        [Display(Name = "บุคคลธรรมดา")]
        Personal = 2,
    }

    public enum UserAction
    {
        Next = 1,
        Back = 2,
        Unknown = 3,
    }

    public enum BuyerStatus
    {
        [Display(Name = "เจ้าของ")]
        Owner = 1,
        [Display(Name = "ผู้เช่า")]
        Renter = 2,
    }
}
