﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TobJod.Models {
    public class SubDistrict : BaseModel {
        public int Id { get; set; }
        [Display(Name = "District"), Required]
        public int DistrictId { get; set; }
        [Display(Name = "Code"), MaxLength(250), Required]
        public string Code { get; set; }
        [Display(Name = "Title (TH)"), MaxLength(250), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string TitleTH { get; set; }
        [Display(Name = "Title (EN)"), MaxLength(250), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string TitleEN { get; set; }
        [Display(Name = "Zip Code"), MaxLength(5), Required]
        [RegularExpression("^[0-9]{5}$", ErrorMessage = "Must be 5 digits")]
        public string ZipCode { get; set; }
        [Required]
        public Status Status { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateAdminId { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateAdminId { get; set; }

        public string CreateAdmin { get; set; }
        public string UpdateAdmin { get; set; }

        public string District { get; set; }

        [Display(Name = "Province")]
        public int ProvinceId { get; set; }
        public string Province { get; set; }
    }
}
