﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TobJod.Models {
    public class UserTestimonial {
        public int Id { get; set; }
        public string Image { get; set; }
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string NameTH { get; set; }
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string NameEN { get; set; }
        public string QuoteTH { get; set; }
        public string QuoteEN { get; set; }
        public int Sequence { get; set; }
    }
}