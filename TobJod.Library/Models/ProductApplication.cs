﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using TobJod.Utils;

namespace TobJod.Models
{

    public class ProductApplicationPolicyHolder
    {
        public int Id { get; set; }
        [Display(Name = "ApplicationId"), Required]
        public int ApplicationId { get; set; }
        [Display(Name = "Sequence"), Required]
        public int Sequence { get; set; }
        [Display(Name = "TitleId"), Required]
        public int TitleId { get; set; }
        [Display(Name = "Name"), MaxLength(50), Required]
        public string Name { get; set; }
        [Display(Name = "Surname"), MaxLength(50), Required]
        public string Surname { get; set; }
        [Display(Name = "IDType"), Required]
        public int IDType { get; set; }
        [Display(Name = "IDCard"), MaxLength(50), Required]
        public string IDCard { get; set; }
        [Display(Name = "RefIDCard"), MaxLength(50), Required]
        public string RefIDCard { get; set; }
        [Display(Name = "NationalId"), Required]
        public int NationalId { get; set; }
        [Display(Name = "Birthday"), Required]
        public DateTime Birthday { get; set; }
        [Display(Name = "Age"), Required]
        public int Age { get; set; }
        [Display(Name = "Sex"), Required]
        public int Sex { get; set; }
        [Display(Name = "MaritalStatus"), Required]
        public int MaritalStatus { get; set; }
        [Display(Name = "Weight"), Required]
        public int Weight { get; set; }
        [Display(Name = "Height"), Required]
        public int Height { get; set; }
        [Display(Name = "CareerId"), Required]
        public int CareerId { get; set; }
        [Display(Name = "Position"), MaxLength(50), Required]
        public string Position { get; set; }
        [Display(Name = "PositionDescription"), MaxLength(-1), Required]
        public string PositionDescription { get; set; }
        [Display(Name = "AnnualIncome"), Required]
        public decimal AnnualIncome { get; set; }
        [Display(Name = "Telephone"), MaxLength(50), Required]
        public string Telephone { get; set; }
        [Display(Name = "MobilePhone"), MaxLength(50), Required]
        public string MobilePhone { get; set; }
        [Display(Name = "Email"), MaxLength(250), Required]
        public string Email { get; set; }
        [Display(Name = "LineId"), MaxLength(50), Required]
        public string LineId { get; set; }
        [Display(Name = "Instagram"), MaxLength(50), Required]
        public string Instagram { get; set; }
        [Display(Name = "Facebook"), MaxLength(50), Required]
        public string Facebook { get; set; }
        [Display(Name = "BeneficiaryName"), MaxLength(50), Required]
        public string BeneficiaryName { get; set; }
        [Display(Name = "BeneficiarySurname"), MaxLength(50), Required]
        public string BeneficiarySurname { get; set; }
        [Display(Name = "BeneficiaryIDCard"), MaxLength(50), Required]
        public string BeneficiaryIDCard { get; set; }
        [Display(Name = "BeneficiaryRelationship"), MaxLength(50), Required]
        public string BeneficiaryRelationship { get; set; }
        [Display(Name = "BeneficiaryMobilePhone"), MaxLength(50), Required]
        public string BeneficiaryMobilePhone { get; set; }
        public int Bank { get; set; }
        public int BeneficiaryIDType { get; set; }
        public string Title { get; set; }
        public string Country { get; set; }
        public string Career { get; set; }

        public ProductApplicationPolicyHolder()
        {
            Id = 0;
            ApplicationId = 0;
            Sequence = 1;
            TitleId = 0;
            Name = string.Empty;
            Surname = string.Empty;
            IDType = 0;
            IDCard = string.Empty;
            RefIDCard = string.Empty;
            NationalId = 0;
            //Birthday = new DateTime(1753, 1, 1, 12, 0, 0);
            Age = 0;
            Sex = 0;
            MaritalStatus = 0;
            Weight = 0;
            Height = 0;
            CareerId = 0;
            Position = string.Empty;
            PositionDescription = string.Empty;
            AnnualIncome = 0;
            Telephone = string.Empty;
            MobilePhone = string.Empty;
            Email = string.Empty;
            LineId = string.Empty;
            Instagram = string.Empty;
            Facebook = string.Empty;
            BeneficiaryName = string.Empty;
            BeneficiarySurname = string.Empty;
            BeneficiaryIDCard = string.Empty;
            BeneficiaryIDType = 0;
            BeneficiaryRelationship = string.Empty;
            BeneficiaryMobilePhone = string.Empty;
            Bank = 0;
        }

    }

    public class ProductApplication
    {
        public int Id { get; set; }
        [Display(Name = "ProductId"), Required]
        public int ProductId { get; set; }
        [Display(Name = "PremiumId"), Required]
        public int PremiumId { get; set; }
        [Display(Name = "InsuranceCode"), MaxLength(50), Required]
        public string InsuranceCode { get; set; }
        [Display(Name = "ProductCode"), MaxLength(50), Required]
        public string ProductCode { get; set; }
        [Display(Name = "ProductName"), MaxLength(50), Required]
        public string ProductName { get; set; }
        [Display(Name = "SumInsured"), Required]
        public decimal SumInsured { get; set; }
        [Display(Name = "NetPremium"), Required]
        public decimal NetPremium { get; set; }
        [Display(Name = "Duty"), Required]
        public decimal Duty { get; set; }
        [Display(Name = "Tax"), Required]
        public decimal Tax { get; set; }
        [Display(Name = "Premium"), Required]
        public decimal Premium { get; set; }
        [Display(Name = "DisplayPremium"), Required]
        public decimal DisplayPremium { get; set; }
        [Display(Name = "ActiveDate"), Required]
        public DateTime ActiveDate { get; set; }
        [Display(Name = "ExpireDate"), Required]
        public DateTime ExpireDate { get; set; }
        [Display(Name = "EPolicy"), Required]
        public int EPolicy { get; set; }
        [Display(Name = "CustomerType"), Required]
        public int CustomerType { get; set; }
        [Display(Name = "BuyerName"), MaxLength(50), Required]
        public string BuyerName { get; set; }
        [Display(Name = "BuyerSurname"), MaxLength(50), Required]
        public string BuyerSurname { get; set; }
        [Display(Name = "BuyerTelephone"), MaxLength(50), Required]
        public string BuyerTelephone { get; set; }
        [Display(Name = "BuyerMobilePhone"), MaxLength(50), Required]
        public string BuyerMobilePhone { get; set; }
        [Display(Name = "BuyerEmail"), MaxLength(250), Required]
        public string BuyerEmail { get; set; }
        [Display(Name = "BuyerLineId"), MaxLength(50), Required]
        public string BuyerLineId { get; set; }
        [Display(Name = "ContactName"), MaxLength(50), Required]
        public string ContactName { get; set; }
        [Display(Name = "ContactSurname"), MaxLength(50), Required]
        public string ContactSurname { get; set; }
        [Display(Name = "ContactTelephone"), MaxLength(50), Required]
        public string ContactTelephone { get; set; }
        [Display(Name = "ContactMobilePhone"), MaxLength(50), Required]
        public string ContactMobilePhone { get; set; }
        [Display(Name = "ContactEmail"), MaxLength(250), Required]
        public string ContactEmail { get; set; }
        [Display(Name = "ContactLineId"), MaxLength(50), Required]
        public string ContactLineId { get; set; }
        [Display(Name = "AddressBranchType"), Required]
        public int AddressBranchType { get; set; }
        [Display(Name = "สำนักงานใหญ่/สาขา (กรณีนิติบุคคล)"), Required]
        public string AddressBranchNo { get; set; }
        [Display(Name = "เลขที่"), MaxLength(50), Required]
        public string AddressNo { get; set; }
        [Display(Name = "หมู่"), MaxLength(50), Required]
        public string AddressMoo { get; set; }
        [Display(Name = "หมู่บ้าน"), MaxLength(50), Required]

        public string AddressVillage { get; set; }
        [Display(Name = "ชั้น"), MaxLength(50), Required]
        public string AddressFloor { get; set; }
        [Display(Name = "ซอย"), MaxLength(50), Required]
        public string AddressSoi { get; set; }
        [Display(Name = "ถนน"), MaxLength(50), Required]
        public string AddressRoad { get; set; }
        [Display(Name = "เขต/ อำเภอ"), MaxLength(50), Required]
        public string AddressDistrict { get; set; }
        [Display(Name = "แขวง/ ตำบล"), MaxLength(50), Required]
        public string AddressSubDistrict { get; set; }
        [Display(Name = "จังหวัด"), MaxLength(50), Required]
        public string AddressProvince { get; set; }
        [Display(Name = "รหัสไปรษณีย์"), MaxLength(5), Required]
        public string AddressPostalCode { get; set; }
        [Display(Name = "BillingAddressBranchType"), Required]
        public int BillingAddressBranchType { get; set; }
        [Display(Name = "สำนักงานใหญ่/สาขา (กรณีนิติบุคคล)"), Required, DisplayFormat(ConvertEmptyStringToNull = false)]
        public string BillingAddressBranchNo { get; set; }
        [Display(Name = "เลขที่"), MaxLength(50), Required]
        public string BillingAddressNo { get; set; }
        [Display(Name = "หมู่"), MaxLength(50), Required]
        public string BillingAddressMoo { get; set; }
        [Display(Name = "หมู่บ้าน"), MaxLength(50), Required]
        public string BillingAddressVillage { get; set; }
        [Display(Name = "ชั้น"), MaxLength(50), Required]
        public string BillingAddressFloor { get; set; }
        [Display(Name = "ซอย"), MaxLength(50), Required]
        public string BillingAddressSoi { get; set; }
        [Display(Name = "ถนน"), MaxLength(50), Required]
        public string BillingAddressRoad { get; set; }
        [Display(Name = "เขต/ อำเภอ"), MaxLength(50), Required]
        public string BillingAddressDistrict { get; set; }
        [Display(Name = "แขวง/ ตำบล"), MaxLength(50), Required]
        public string BillingAddressSubDistrict { get; set; }
        [Display(Name = "จังหวัด"), MaxLength(50), Required]
        public string BillingAddressProvince { get; set; }
        [Display(Name = "รหัสไปรษณีย์"), MaxLength(5), Required]
        public string BillingAddressPostalCode { get; set; }
        [Display(Name = "ShippingAddressBranchType"), Required]
        public int ShippingAddressBranchType { get; set; }
        [Display(Name = "สำนักงานใหญ่/สาขา (กรณีนิติบุคคล)"), Required]
        public string ShippingAddressBranchNo { get; set; }
        [Display(Name = "เลขที่"), MaxLength(50), Required]
        public string ShippingAddressNo { get; set; }
        [Display(Name = "หมู่"), MaxLength(50), Required]
        public string ShippingAddressMoo { get; set; }
        [Display(Name = "หมู่บ้าน"), MaxLength(50), Required]
        public string ShippingAddressVillage { get; set; }
        [Display(Name = "ชั้น"), MaxLength(50), Required]
        public string ShippingAddressFloor { get; set; }
        [Display(Name = "ซอย"), MaxLength(50), Required]
        public string ShippingAddressSoi { get; set; }
        [Display(Name = "ถนน"), MaxLength(50), Required]
        public string ShippingAddressRoad { get; set; }
        [Display(Name = "เขต/ อำเภอ"), MaxLength(50), Required]
        public string ShippingAddressDistrict { get; set; }
        [Display(Name = "แขวง/ ตำบล"), MaxLength(50), Required]
        public string ShippingAddressSubDistrict { get; set; }
        [Display(Name = "จังหวัด"), MaxLength(50), Required]
        public string ShippingAddressProvince { get; set; }
        [Display(Name = "รหัสไปรษณีย์"), MaxLength(5), Required]
        public string ShippingAddressPostalCode { get; set; }
        public string Steps { get; set; }
        public string OverSteps { get; set; }
        public int Step { get; set; }
        public YesNo BillingAddressIsChecked { get; set; }
        public YesNo ShippingAddressIsChecked { get; set; }
        public int PromotionId { get; set; }
        public int CampaignId { get; set; }
        public int LeadFormId { get; set; }
        public string CouponCode { get; set; }
        public decimal TotalDiscount { get; set; }
        public Language Language { get; set; }
        public string PreviousUrl { get; set; }
        public ProductSubCategoryKey ProductSubCategoryKey { get; set; }
        public APIStatusCategory APIStatusCategory { get; set; }

        public ProductApplication()
        {
            Id = 0;
            ProductId = 0;
            PremiumId = 0;
            TotalDiscount = 0;
            InsuranceCode = string.Empty;
            ProductCode = string.Empty;
            ProductName = string.Empty;
            SumInsured = 0;
            NetPremium = 0;
            Duty = 0;
            Tax = 0;
            Premium = 0;
            DisplayPremium = 0;
            ActiveDate = new DateTime(1753, 1, 1, 12, 0, 0);
            ExpireDate = new DateTime(1753, 1, 1, 12, 0, 0);
            EPolicy = 0;
            CustomerType = 0;
            ContactName = string.Empty;
            ContactSurname = string.Empty;
            ContactTelephone = string.Empty;
            ContactMobilePhone = string.Empty;
            ContactEmail = string.Empty;
            ContactLineId = string.Empty;
            BuyerName = string.Empty;
            BuyerSurname = string.Empty;
            BuyerTelephone = string.Empty;
            BuyerMobilePhone = string.Empty;
            BuyerEmail = string.Empty;
            BuyerLineId = string.Empty;
            AddressBranchType = 0;
            AddressBranchNo = string.Empty;
            AddressNo = string.Empty;
            AddressMoo = string.Empty;
            AddressVillage = string.Empty;
            AddressFloor = string.Empty;
            AddressSoi = string.Empty;
            AddressRoad = string.Empty;
            AddressDistrict = string.Empty;
            AddressSubDistrict = string.Empty;
            AddressProvince = string.Empty;
            AddressPostalCode = string.Empty;
            BillingAddressBranchType = 0;
            BillingAddressBranchNo = string.Empty;
            BillingAddressNo = string.Empty;
            BillingAddressMoo = string.Empty;
            BillingAddressVillage = string.Empty;
            BillingAddressFloor = string.Empty;
            BillingAddressSoi = string.Empty;
            BillingAddressRoad = string.Empty;
            BillingAddressDistrict = string.Empty;
            BillingAddressSubDistrict = string.Empty;
            BillingAddressProvince = string.Empty;
            BillingAddressPostalCode = string.Empty;
            ShippingAddressBranchType = 0;
            ShippingAddressBranchNo = string.Empty;
            ShippingAddressNo = string.Empty;
            ShippingAddressMoo = string.Empty;
            ShippingAddressVillage = string.Empty;
            ShippingAddressFloor = string.Empty;
            ShippingAddressSoi = string.Empty;
            ShippingAddressRoad = string.Empty;
            ShippingAddressDistrict = string.Empty;
            ShippingAddressSubDistrict = string.Empty;
            ShippingAddressProvince = string.Empty;
            ShippingAddressPostalCode = string.Empty;
            OverSteps = string.Empty;
            Step = (int)ProductApplicationStep.Unknown;
            Steps = string.Empty;
            Language = Language.TH;
            LeadFormId = 0;
            APIStatusCategory = APIStatusCategory.Pending;
            CampaignId = 0;
            PromotionId = 0;
        }

        public List<ProductApplicationAttachment> ProductApplicationAttachment { get; set; }
        public List<ProductApplicationDriver> ProductApplicationDrivers { get; set; }
        public List<ProductApplicationPolicyHolder> ProductApplicationPolicyHolders { get; set; }
        public ProductApplicationMotor ProductApplicationMotor { get; set; }
        public ProductApplicationTA ProductApplicationTA { get; set; }
        public ProductApplicationHome ProductApplicationHome { get; set; }
        public Order Order { get; set; }
    }

    public class ProductApplicationMotor
    {
        public int ApplicationId { get; set; }
        [Display(Name = "CarModelId"), Required]
        public int CarModelId { get; set; }
        [Display(Name = "ยี่ห้อรถยนต์"), MaxLength(50), Required]
        public string CarBrand { get; set; }
        [Display(Name = "รุ่นรถยนต์"), MaxLength(50), Required]
        public string CarFamily { get; set; }
        [Display(Name = "รุ่นย่อย"), MaxLength(50), Required]
        public string CarModel { get; set; }
        [Display(Name = "ปีจดทะเบียนรถ"), Required]
        public int CarRegisterYear { get; set; }
        [Display(Name = "CarCC"), Required]
        public int CarCC { get; set; }
        [Display(Name = "CarWeight"), Required]
        public int CarWeight { get; set; }
        [Display(Name = "หมายเลขเครื่อง"), MaxLength(50), Required]
        public string CarEngineNo { get; set; }
        [Display(Name = "หมายเลขตัวถัง"), MaxLength(50), Required]
        public string CarSerialNo { get; set; }
        [Display(Name = "เลขทะเบียนรถ"), MaxLength(50), Required]
        public string CarPlateNo1 { get; set; }
        [Display(Name = "เลขทะเบียนรถ"), MaxLength(50), Required]
        public string CarPlateNo2 { get; set; }
        [Display(Name = "จังหวัดจดทะเบียน"), Required]
        public int CarProvinceId { get; set; }

        [Display(Name = "CarUsageTypeCode"), MaxLength(50), Required]
        public string CarUsageTypeCode { get; set; }
        [Display(Name = "CarUsageCode"), Required]
        public int CarUsageCode { get; set; }
        [Display(Name = "CarUsageType"), Required]
        public int CarUsageType { get; set; }
        [Display(Name = "ติดตั้ง CCTV"), Required]
        public int CarCCTV { get; set; }
        [Display(Name = "ประเภทอู่ซ่อม"), Required]
        public int CarGarageType { get; set; }
        [Display(Name = "CarAccessories"), MaxLength(-1), Required]
        public string CarAccessories { get; set; }
        [Display(Name = "CarAccessoryValue"), Required]
        public decimal CarAccessoryValue { get; set; }
        [Display(Name = "CompulsoryPremium"), Required]
        public decimal CompulsoryPremium { get; set; }
        [Display(Name = "CombinePremium"), Required]
        public decimal CombinePremium { get; set; }
        public int Compulsory { get; set; }

        public ProductApplicationMotor()
        {

            CarModelId = 0;
            CarBrand = string.Empty;
            CarFamily = string.Empty;
            CarModel = string.Empty;
            CarRegisterYear = 0;
            CarCC = 0;
            CarWeight = 0;
            CarEngineNo = string.Empty;
            CarSerialNo = string.Empty;
            CarPlateNo1 = string.Empty;
            CarPlateNo2 = string.Empty;
            CarProvinceId = 0;
            CarUsageTypeCode = string.Empty;
            CarUsageCode = 0;
            CarUsageType = 0;
            CarCCTV = 0;
            CarGarageType = 0;
            CarAccessories = string.Empty;
            CarAccessoryValue = 0;
            CompulsoryPremium = 0;
            CombinePremium = 0;
            Compulsory = 0;
        }
    }

    public class ProductApplicationHome
    {
        public int ApplicationId { get; set; }
        public int PropertyType { get; set; }
        public string PropertyTypeText { get; set; }
        public int LocationUsage { get; set; }
        public string LocationUsageText { get; set; }
        public int BuyerStatus { get; set; }
        public int ConstructionType { get; set; }
        public string ConstructionTypeText { get; set; }
        public int Floor { get; set; }
        public int RoomAmount { get; set; }
        public int SquareMeter { get; set; }
        public decimal ConstructionValue { get; set; }
        public decimal FunitureValue { get; set; }
        public int CoverageYear { get; set; }
        public string AddressBranchType { get; set; }
        [Display(Name = "สำนักงานใหญ่/สาขา (กรณีนิติบุคคล)")]
        public string AddressBranchNo { get; set; }
        [Display(Name = "เลขที่")]
        public string AddressNo { get; set; }
        [Display(Name = "หมู่")]
        public string AddressMoo { get; set; }
        [Display(Name = "หมู่บ้าน")]
        public string AddressVillage { get; set; }
        [Display(Name = "ชั้น")]
        public string AddressFloor { get; set; }
        [Display(Name = "ซอย")]
        public string AddressSoi { get; set; }
        [Display(Name = "ถนน")]
        public string AddressRoad { get; set; }
        [Display(Name = "เขต/ อำเภอ")]
        public string AddressDistrict { get; set; }
        [Display(Name = "แขวง/ ตำบล")]
        public string AddressSubDistrict { get; set; }
        [Display(Name = "จังหวัดอ")]
        public string AddressProvince { get; set; }
        [Display(Name = "รหัสไปรษณีย์")]
        public string AddressPostalCode { get; set; }
        public ProductApplicationHome()
        {
            PropertyType = 0;
            LocationUsage = 0;
            BuyerStatus = 0;
            ConstructionType = 0;
            Floor = 0;
            RoomAmount = 0;
            SquareMeter = 0;
            ConstructionValue = 0;
            FunitureValue = 0;
            CoverageYear = 0;
            AddressBranchType = string.Empty;
            AddressBranchNo = string.Empty;
            AddressNo = string.Empty;
            AddressMoo = string.Empty;
            AddressVillage = string.Empty;
            AddressFloor = string.Empty;
            AddressSoi = string.Empty;
            AddressRoad = string.Empty;
            AddressDistrict = string.Empty;
            AddressSubDistrict = string.Empty;
            AddressProvince = string.Empty;
            AddressPostalCode = string.Empty;

        }
    }

    public class ProductApplicationPA { }
    public class ProductApplicationPH { }
    public class ProductApplicationTA
    {
        public int ApplicationId { get; set; }
        public int TravelType { get; set; }
        public int TravelBy { get; set; }
        public int CoverageType { get; set; }
        public int CoverageOption { get; set; }
        public int Persons { get; set; }
        public int Day { get; set; }
        public DateTime DepartureDate { get; set; }
        public DateTime ReturnDate { get; set; }
        public string University { get; set; }
        public int Country { get; set; }
        public string CountryText { get; set; }
        public int Arrival { get; set; }
        public string ArrivalText { get; set; }
        public string TravelMethod { get; set; }
        public ProductApplicationTA()
        {
            TravelType = 0;
            TravelBy = 0;
            CoverageType = 0;
            CoverageOption = 0;
            Persons = 0;
            Day = 0;
            DepartureDate = DateUtils.SqlMinDate();
            ReturnDate = DateUtils.SqlMinDate();
            University = string.Empty;
            Country = 0;
            Arrival = 0;
        }

    }
    public class ProductApplicationDriver
    {
        public int Id { get; set; }
        [Display(Name = "ApplicationId"), Required] public int ApplicationId { get; set; }
        [Display(Name = "Sequence"), Required] public int Sequence { get; set; }
        [Display(Name = "คำนำหน้าชื่อ"), Required] public int TitleId { get; set; }
        [Display(Name = "ชื่อ"), MaxLength(50), Required] public string Name { get; set; }
        [Display(Name = "นามสกุล"), MaxLength(50), Required] public string Surname { get; set; }
        [Display(Name = "ประเภทบัตร"), Required] public int IDType { get; set; }
        [Display(Name = "เลขที่ประจำตัวประชาชน/ หมายเลข Passport"), MaxLength(50), Required] public string IDCard { get; set; }
        [Display(Name = "เลขที่ใบขับขี่"), MaxLength(50), Required] public string RefIDCard { get; set; }
        [Display(Name = "สัญชาติ"), Required] public int NationalId { get; set; }
        [Display(Name = "วันเกิด"), Required] public DateTime Birthday { get; set; }
        public string Country { get; set; }
        public string Title { get; set; }
        public string Nationality { get; set; }

        public ProductApplicationDriver()
        {
            Id = 0;
            ApplicationId = 0;
            Sequence = 0;
            TitleId = 0;
            Name = string.Empty;
            Surname = string.Empty;
            IDType = 0;
            IDCard = string.Empty;
            RefIDCard = string.Empty;
            NationalId = 0;
            Birthday = new DateTime(1753, 1, 1, 12, 0, 0);
        }
    }
    public class ProductApplicationAttachment
    {
        [Display(Name = "Id"), Required]
        public int Id { get; set; }
        [Display(Name = "ApplicationId"), Required]
        public int ApplicationId { get; set; }
        [Display(Name = "FileCategory"), Required]
        public int FileCategory { get; set; }
        [Display(Name = "FileName"), MaxLength(250), Required]
        public string FileName { get; set; }
        [Display(Name = "Status"), Required]
        public int Status { get; set; }
        [Display(Name = "CreateDate"), Required]
        public DateTime CreateDate { get; set; }
        [Display(Name = "UpdateDate"), Required]
        public DateTime UpdateDate { get; set; }
        public long FileSize { get; set; }
        public byte[] FileContent { get; set; }

        public ProductApplicationAttachment()
        {
            Id = 0;
            ApplicationId = 0;
            FileCategory = 0;
            FileName = string.Empty;
            FileSize = 0;
            Status = 0;
            CreateDate = DateUtils.SqlMinDate();
            UpdateDate = DateUtils.SqlMinDate();
        }
    }

    #region "Payment Gateway"
    public class PaymentRequest
    {

        #region CONSTANTS

        private const string NAMESPACE = "";
        private const string VERSION = "Version";
        private const string CURRENCY = "Currency";
        private const string MERCHANT_ID = "MerchantID";
        private const string PAYMENT_URL = "PaymentUrl";
        private const string RESULT_URL_1 = "ResultUrl1";
        private const string RESULT_URL_2 = "ResultUrl2";
        private const string SECRET_KEY = "Secretkey";
        private const string AMOUNT_FORMAT = "0000000000.00";

        private const char ORDER_ID_FORMAT = '0';
        private const int ORDER_ID_PADDING = 20;

        #endregion

        #region Properties

        public PaymentPlan payment_plan { get; set; }
        public string payment_url { get; set; }
        public string secret_key { get; set; }
        public string order_id2 { get { return this.FormatOrderId2(this.order_id); } }
        public string amount2 { get { return this.FormatAmount2(this.amount); } }
        public string currency2 { get { return this.Currency2(this.currency); } }
        public string version { get; set; }
        public string merchant_id { get; set; }
        public string payment_description { get; set; }
        public string order_id { get; set; }
        public string user_defined_1 { get; set; }
        public string user_defined_2 { get; set; }
        public string user_defined_3 { get; set; }
        public string user_defined_4 { get; set; }
        public string user_defined_5 { get; set; }
        public decimal amount { get; set; }
        public string currency { get; set; }
        public string promotion { get; set; }
        public string customer_email { get; set; }
        public string pay_category_id { get; set; }
        public string result_url_1 { get; set; }
        public string result_url_2 { get; set; }
        public string payment_option { get; set; }
        public string ipp_interest_type { get; set; }
        public string payment_expiry { get; set; }
        public string default_lang { get; set; }
        public string enable_store_card { get; set; }
        public string stored_card_unique_id { get; set; }
        public string request_3ds { get; set; }
        public string recurring { get; set; }
        public string order_prefix { get; set; }
        public string recurring_amount { get; set; }
        public string allow_accumulate { get; set; }
        public string max_accumulate_amount { get; set; }
        public string recurring_interval { get; set; }
        public string recurring_count { get; set; }
        public string charge_next_date { get; set; }
        public string charge_on_date { get; set; }
        public string statement_descriptor { get; set; }
        public string hash_value { get; set; }

        #endregion

        #region Constructors
        public PaymentRequest()
        {
            this.SetupConfiguration();
        }

        #endregion

        #region Public Methods

        public string CreateSignature()
        {
            return StringExtenstions.CleanString(this.version)
                + StringExtenstions.CleanString(this.merchant_id)
                + this.payment_description
                + this.order_id
                + this.currency2
                + this.amount2
                + StringExtenstions.CleanString(this.customer_email)
                + StringExtenstions.CleanString(this.result_url_1)
                + StringExtenstions.CleanString(this.result_url_2)
                + StringExtenstions.CleanString(payment_expiry) 
                + StringExtenstions.CleanString(default_lang);
        }

        public string CreateSignatureInstallmentPayment()
        {
            return this.CreateSignature()
                + this.request_3ds
                + this.payment_option
                + this.ipp_interest_type;

        }

        public string EncryptSignature()
        {
            switch (this.payment_plan)
            {
                case PaymentPlan.IPP:
                    return this.hash_hmac_sha1(this.CreateSignatureInstallmentPayment()
                , this.secret_key);
                default:
                    return this.hash_hmac_sha1(this.CreateSignature()
                , this.secret_key);
            }

        }

        #endregion

        #region Setup Methods
        private void SetupConfiguration()
        {
            string version = GetAppConfig(VERSION);
            string paymentUrl = GetAppConfig(PAYMENT_URL);
            string merchantId = GetAppConfig(MERCHANT_ID);
            //string currency = GetAppConfig(CURRENCY);
            string currency = "THB";
            string resultUrl1 = GetAppConfig(RESULT_URL_1);
            string resultUrl2 = GetAppConfig(RESULT_URL_2);
            string secretKey = GetAppConfig(SECRET_KEY);

            if (string.IsNullOrWhiteSpace(version))
            {
                throw new Exception("Please specified the version of Gateway.");
            }

            if (string.IsNullOrWhiteSpace(paymentUrl))
            {
                throw new Exception("Please specified the URL of Gateway.");
            }

            //if (string.IsNullOrWhiteSpace(secretKey))
            //{
            //    throw new Exception("Please specified the Secrete Key of Merchant.");
            //}

            //if (string.IsNullOrWhiteSpace(merchantId))
            //{
            //    throw new Exception("Please specified the ID of Merchant.");
            //}

            //if (string.IsNullOrWhiteSpace(currency))
            //{
            //    throw new Exception("Please specified the Currency of Merchant.");
            //}

            if (string.IsNullOrWhiteSpace(resultUrl1))
            {
                throw new Exception("Please specified the result URL(Frontend) of Merchant to be sent the response of the 2C2P Gateway.");
            }

            if (string.IsNullOrWhiteSpace(resultUrl2))
            {
                throw new Exception("Please specified the result URL(Backend) of Merchant to be sent the response of the 2C2P Gateway.");
            }

            this.version = version;
            this.merchant_id = NullUtils.cvString(merchantId);
            this.currency = NullUtils.cvString(currency);
            this.payment_url = NullUtils.cvString(paymentUrl);
            this.result_url_1 = NullUtils.cvString(resultUrl1);
            this.result_url_2 = NullUtils.cvString(resultUrl2);
            this.secret_key = NullUtils.cvString(secretKey);

        }

        #endregion

        #region Support Methods

        public string GetAppConfig(string key)
        {
            return WebConfigurationManager.AppSettings.Get(NAMESPACE + key);
        }

        public string Currency2(string currency)
        {
            string currenzy = "";

            EnumLoop<PaymentCurrencyCodes>.ForEach(kurrency =>
            {
                if (kurrency.ToString() == currency)
                {
                    currenzy = ((int)kurrency).ToString();
                    return;
                }
            });

            if (string.IsNullOrEmpty(currenzy))
            {
                throw new Exception("Your currency is not exists.");
            }

            return currenzy;
        }

        public string FormatOrderId2(string orderId)
        {
            return orderId.PadLeft(ORDER_ID_PADDING, ORDER_ID_FORMAT);
        }

        public string FormatAmount2(decimal amount)
        {
            return amount.ToString(AMOUNT_FORMAT).Replace(".", "");
        }

        public bool ValidateSignature()
        {
            bool isValid = true;


            return isValid;
        }

        public string hash_hmac_sha1(string message, string key)
        {
            byte[] keyByte = UTF8Encoding.UTF8.GetBytes(key);
            HMACSHA1 hmacsha1 = new HMACSHA1(keyByte);
            byte[] messageBytes = UTF8Encoding.UTF8.GetBytes(message);
            byte[] hashmessage = hmacsha1.ComputeHash(messageBytes);
            string calcHashString = ByteToString(hashmessage);

            return calcHashString.ToLower();
        }

        /*converts byte to encrypted string*/
        public string ByteToString(byte[] buff)
        {
            string sbinary = "";
            for (int i = 0; i < buff.Length; i++)
            {
                sbinary += buff[i].ToString("X2"); // hex format
            }

            return (sbinary);
        }

        #endregion

    }

    public class PaymentResponse
    {
        #region 2C2P Properties

        public string version { get; set; }
        public string request_timestamp { get; set; }
        public string merchant_id { get; set; }
        public string order_id { get; set; }
        public string payment_channel { get; set; }
        public string payment_status { get; set; }
        public string channel_response_code { get; set; }
        public string channel_response_desc { get; set; }
        public string approval_code { get; set; }
        public string eci { get; set; }
        public string transaction_datetime { get; set; }
        public string transaction_ref { get; set; }
        public string masked_pan { get; set; }
        public string paid_agent { get; set; }
        public string paid_channel { get; set; }
        public string amount { get; set; }
        public string currency { get; set; }
        public string user_defined_1 { get; set; }
        public string user_defined_2 { get; set; }
        public string user_defined_3 { get; set; }
        public string user_defined_4 { get; set; }
        public string user_defined_5 { get; set; }
        public string browser_info { get; set; }
        public string stored_card_unique_id { get; set; }
        public string backend_invoice { get; set; }
        public string recurring_unique_id { get; set; }
        public string ippPeriod { get; set; }
        public string ippInterestType { get; set; }
        public string ippInterestRate { get; set; }
        public string ippMerchantAbsorbRate { get; set; }
        public string payment_scheme { get; set; }
        public string hash_value { get; set; }
        #endregion

        #region Custom Properties
        public string back_from { get; set; }
        #endregion

    }

    static class StringExtenstions
    {
        public static string CleanString(string str)
        {
            if (string.IsNullOrEmpty(str))
                return string.Empty;
            return str.Trim();
        }

        public static double cleanDouble(string val)
        {
            double amount = 0.00;

            if (Double.TryParse(val + string.Empty, out amount))
            {
                return amount;
            }
            else
            {
                return 0.00;
            }

        }
    }

    public enum PaymentCurrencyCodes
    {
        THB = 764,
        SGD = 702,
        MYR = 458,
        USD = 840,
        IDR = 360,
        TWD = 901,
        HKD = 344,
        PHP = 608,
        MMK = 104,
        EUR = 978,
        JPY = 392,
        AUD = 36,
        BDT = 50,
        CAD = 124,
        CHF = 756,
        CNY = 156,
        DKK = 208,
        GBP = 826,
        HTG = 332,
        KRW = 410,
        NOK = 578,
        NZD = 554,
        RUB = 643,
        SEK = 752,
        VND = 704,
        YER = 886,
    }

    class EnumLoop<Key> where Key : struct, IConvertible
    {
        static readonly Key[] arr = (Key[])Enum.GetValues(typeof(Key));
        static internal void ForEach(Action<Key> act)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                act(arr[i]);
            }
        }
    }

    public enum PaymentPlan
    {
        CREDIT,
        IPP,
        RPP,
    }

    #endregion

    public enum IDTYPE
    {
        IDCard = 1,
        Passport = 2,
    }

    public enum FileCategory
    {
        IDCard = 1,
        CCTV = 2,
        RegisteredCarCopy = 3,
        File1 = 4,
        File2 = 5,
        File3 = 6,
        File4 = 7,
        File5 = 8,
        File6 = 9,
    }

    public enum APIStatusCategory {
        Pending = 0,
        Order_Success = 2,
        Motor_V_Fail = 10,
        Motor_C_Fail = 11
    }

    public static class LogUtils
    {
        private static readonly object syncObject = new object();
        private static string logPath = string.Empty;
        private static void CreateLogFile()
        {
            // not thread safe, just for test
            if (string.IsNullOrEmpty(logPath))
            {
                // Create a log file
                //DirectoryInfo pathInfo = new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + Path.DirectorySeparatorChar + "MCServerHelper");
                var errorLogPath = new DirectoryInfo(System.Configuration.ConfigurationManager.AppSettings["WebService_Log_Path"]);
                DirectoryInfo pathInfo = new DirectoryInfo(errorLogPath.FullName);

                if (!pathInfo.Exists)
                {
                    pathInfo.Create();
                }

                string logFilename = DateTime.Now.ToString("yyyyMMdd", new System.Globalization.CultureInfo("en-GB")) + "_2C2P.log";
                FileInfo logFile = new FileInfo(pathInfo.FullName + Path.DirectorySeparatorChar + logFilename);

                if (!logFile.Exists)
                {
                    var file = logFile.Create();
                    file.Close();
                }

                logPath = logFile.FullName;
            }
        }

        public static void LogException(Exception ex)
        {
            CreateLogFile();

            if (!string.IsNullOrEmpty(logPath))
            {
                using (StreamWriter writer = new StreamWriter(logPath, true))
                {
                    writer.WriteLine(DateTime.Now.ToString());
                    writer.WriteLine(ex.Message);
                    writer.WriteLine(ex.Source);
                    writer.WriteLine(ex.TargetSite);
                    writer.WriteLine(ex.StackTrace);

                    StackTrace trace = new StackTrace(ex);
                    if (trace.FrameCount > 0)
                    {
                        StackFrame frame = trace.GetFrame(0);
                        if (frame != null)
                        {
                            MethodBase method = frame.GetMethod();
                            writer.WriteLine(string.Format("{0}, Line {1}", method.Name, frame.GetFileLineNumber()));
                        }
                    }

                    writer.WriteLine();
                }

                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException);
                }
            }
        }

        public static void LogMessage(string message)
        {
            CreateLogFile();

            if (!string.IsNullOrEmpty(logPath))
            {
                using (FileStream stream = new FileStream(logPath, FileMode.Append, FileAccess.Write))
                {
                    using (StreamWriter writer = new StreamWriter(stream, Encoding.UTF8))
                    {
                        writer.WriteLine(DateTime.Now.ToString());
                        writer.WriteLine(message);
                        writer.WriteLine("");
                    }
                }
            }
        }
    }
}
