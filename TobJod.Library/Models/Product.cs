﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using TobJod.Utils;

namespace TobJod.Models {

    public class Product {
        public int Id { get; set; }
        [Display(Name = "Category"), Required]
        public ProductCategoryKey CategoryId { get; set; }
        public ProductSubCategoryKey SubCategoryId { get; set; }
        public int CompanyId { get; set; }
        public ProductSubCategoryKey ProductSubCategoryId { get; set; }
        [Display(Name = "Class"), MaxLength(2), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string Class { get; set; }
        [Display(Name = "Sub Class"), MaxLength(20), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string SubClass { get; set; }
        [Display(Name = "Product Code"), MaxLength(19), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string ProductCode { get; set; }
        [Display(Name = "Insurance Product Code"), MaxLength(20), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string InsuranceProductCode { get; set; }
        [Display(Name = "Title (TH)"), MaxLength(250), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string TitleTH { get; set; }
        [Display(Name = "Title (EN)"), MaxLength(250), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string TitleEN { get; set; }
        [Display(Name = "Active Date"), DataType(DataType.Date), Required]
        public DateTime ActiveDate { get; set; }
        [Display(Name = "Expire Date"), DataType(DataType.Date), Required]
        public DateTime ExpireDate { get; set; }
        [Display(Name = "Sale Channel")]
        public ProductSaleChannel SaleChannel { get; set; }

        [Display(Name = "ความคุ้มครอง (TH)"), DataType(DataType.MultilineText), DisplayFormat(ConvertEmptyStringToNull = false)]
        //[RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        [AllowHtml]
        public string CoverageTH { get; set; }
        [Display(Name = "ความคุ้มครอง (EN)"), DataType(DataType.MultilineText), DisplayFormat(ConvertEmptyStringToNull = false)]
        //[RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        [AllowHtml]
        public string CoverageEN { get; set; }
        [Display(Name = "ความคุ้มครองพิเศษ (TH)"), DataType(DataType.MultilineText), DisplayFormat(ConvertEmptyStringToNull = false)]
        //[RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string SpecialCoverageTH { get; set; }
        [Display(Name = "ความคุ้มครองพิเศษ (EN)"), DataType(DataType.MultilineText), DisplayFormat(ConvertEmptyStringToNull = false)]
        //[RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string SpecialCoverageEN { get; set; }
        [Display(Name = "เงื่อนไขความคุ้มครอง (TH)"), DataType(DataType.MultilineText), DisplayFormat(ConvertEmptyStringToNull = false)]
        //[RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string ConditionTH { get; set; }
        [Display(Name = "เงื่อนไขความคุ้มครอง (EN)"), DataType(DataType.MultilineText), DisplayFormat(ConvertEmptyStringToNull = false)]
        //[RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string ConditionEN { get; set; }
        [Display(Name = "ข้อยกเว้น (TH)"), DataType(DataType.MultilineText), DisplayFormat(ConvertEmptyStringToNull = false)]
        //[RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string ExceptionTH { get; set; }
        [Display(Name = "ข้อยกเว้น (EN)"), DataType(DataType.MultilineText), DisplayFormat(ConvertEmptyStringToNull = false)]
        //[RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string ExceptionEN { get; set; }
        [Display(Name = "สิทธิพิเศษ (TH)"), DataType(DataType.MultilineText), DisplayFormat(ConvertEmptyStringToNull = false)]
        //[RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string PrivilegeTH { get; set; }
        [Display(Name = "สิทธิพิเศษ (EN)"), DataType(DataType.MultilineText), DisplayFormat(ConvertEmptyStringToNull = false)]
        //[RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string PrivilegeEN { get; set; }
        [Display(Name = "Check List (TH)"), DataType(DataType.MultilineText), DisplayFormat(ConvertEmptyStringToNull = false)]
        //[RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string CheckListTH { get; set; }
        [Display(Name = "Check List (EN)"), DataType(DataType.MultilineText), DisplayFormat(ConvertEmptyStringToNull = false)]
        //[RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string CheckListEN { get; set; }
        [Display(Name = "Consent (TH)"), DataType(DataType.MultilineText), DisplayFormat(ConvertEmptyStringToNull = false)]
        //[RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string ConsentTH { get; set; }
        [Display(Name = "Consent (EN)"), DataType(DataType.MultilineText), DisplayFormat(ConvertEmptyStringToNull = false)]
        //[RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string ConsentEN { get; set; }

        [Display(Name = "Approve Status"), Required]
        public ApproveStatus ApproveStatus { get; set; }
        [Display(Name = "Approve Message"), DataType(DataType.MultilineText), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ApproveMessage { get; set; }

        [Required]
        public Status Status { get; set; }
        [Display(Name = "Owner")]
        public string OwnerAdminId { get; set; }
        public string ApproveAdminId { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateAdminId { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateAdminId { get; set; }

        public string OwnerAdmin { get; set; }
        public string ApproveAdmin { get; set; }
        public string CreateAdmin { get; set; }
        public string UpdateAdmin { get; set; }


        public string Category { get; set; }
        public string SubCategory { get; set; }
        public string Company { get; set; }
        public string CompanyCode { get; set; }

        //public ProductMotor ProductMotor { get; set; }
        public ProductPRB ProductPRB { get; set; }
        //public ProductTA ProductTA { get; set; }
        public List<int> ProductTACountry { get; set; }
        public List<ProductCareerClass> ProductCareer { get; set; }
        public List<ProductCarModel> ProductCarModel { get; set; }
        public List<ProductRegisterProvince> ProductRegisterProvince { get; set; }

        public List<ProductMotorPremium> ProductMotorPremium { get; set; }
        public List<ProductHomePremium> ProductHomePremium { get; set; }
        public List<ProductPAPremium> ProductPAPremium { get; set; }
        public List<ProductPHPremium> ProductPHPremium { get; set; }
        public List<ProductTAPremium> ProductTAPremium { get; set; }

        public string ProductCodePrefix { get { return GetProductCodePrefix(); } }

        public string GetProductCodePrefix() { 
            try { 
                if (Class != null && SubClass != null && CompanyCode != null && InsuranceProductCode != null && (CreateDate != null && CreateDate > DateTime.MinValue)) {
                    return Class.PadRight(2, '_') + (SubClass.Length > 3 ? SubClass.Substring(0, 3) : SubClass).PadRight(3, '_') + CompanyCode.PadRight(5, '_') + CreateDate.ToString("yy");
                }
            } catch { }
            return "";
        }

        public ProductSubCategoryKey GetSubCategoryIdBySubClass(string val) {
            switch(val) {
                case "1": return ProductSubCategoryKey.Motor_1;
                case "2": return ProductSubCategoryKey.Motor_2;
                case "2+": return ProductSubCategoryKey.Motor_2P;
                case "3": return ProductSubCategoryKey.Motor_3;
                case "3+": return ProductSubCategoryKey.Motor_3P;
                default: return ProductSubCategoryKey.None;
            }
        }

        public static int[] GetVehicleUsages() {
            return new int[] { 110, 120, 210, 220, 230, 320 };
        }

        public static int GetAgeFloor(DateTime birthDate, DateTime date) {
            int age = date.Year - birthDate.Year;

            if (date.Month < birthDate.Month || (date.Month == birthDate.Month && date.Day < birthDate.Day))
                age--;

            return age;
        }

        public static int GetAgeCeiling(DateTime birthDate, DateTime date) {
            double diff = ((date.Year - birthDate.Year) * 12) + date.Month - birthDate.Month; ;
            int age = (int)Math.Round(diff / 12, MidpointRounding.AwayFromZero);
            return age; 
        }
    }

    public class ProductItem : Product {
        public int Compulsory { get; set; }
        public int Usage { get; set; }
        public int PremiumId { get; set; }
        public string CompanyImage { get; set; }
        //public List<Campaign> Campaign { get; set; }
        public List<PromotionItem> Promotion { get; set; }

        public ProductItem() {
            Promotion = new List<PromotionItem>();
        }
    }
    
    public class ProductMotorPremium {
        public int Id { get; set; }
        public int ProductId { get; set; }
        [Range(0, 999999)]
        public int PlanCode { get; set; }
        //[Display(Name = "ยี่ห้อ")]
        //public int BrandId { get; set; }
        //[Display(Name = "รุ่นหลัก")]
        //public int FamilyId { get; set; }
        //[Display(Name = "รุ่น")]
        //public int ModelId { get; set; }
        [Display(Name = "ปีจดทะเบียน")]
        public int RegisterYear { get; set; }
        [Display(Name = "อายุรถเริ่มต้น")]
        public int YearMin { get; set; }
        [Display(Name = "อายุรถสิ้นสุด")]
        public int YearMax { get; set; }
        [Display(Name = "อายุผู้ขับขี่เริ่มต้น")]
        public int DriverAgeMin { get; set; }
        [Display(Name = "อายุผู้ขับขี่สิ้นสุด")]
        public int DriverAgeMax { get; set; }

        [Display(Name = "ประเภทการใช้รถ 110")]
        public bool VehicleUsage110 { get; set; }
        [Display(Name = "ประเภทการใช้รถ 120")]
        public bool VehicleUsage120 { get; set; }
        [Display(Name = "ประเภทการใช้รถ 210")]
        public bool VehicleUsage210 { get; set; }
        [Display(Name = "ประเภทการใช้รถ 220")]
        public bool VehicleUsage220 { get; set; }
        [Display(Name = "ประเภทการใช้รถ 230")]
        public bool VehicleUsage230 { get; set; }
        [Display(Name = "ประเภทการใช้รถ 320")]
        public bool VehicleUsage320 { get; set; }

        [Display(Name = "ทะเบียนทุกจังหวัด")]
        public bool RegisterProvinceAll { get; set; }

        [Display(Name = "ประเภทซ่อม")]
        public ProductMotorGarageType GarageType { get; set; }
        [Display(Name = "ตรวจสภาพรถ")]
        public bool CarInspector { get; set; }
        [Display(Name = "วิธีตรวจสภาพรถ")]
        public ProductMotorCarInspectorMethod CarInspectorMethod { get; set; }
        [Display(Name = "CCTV")]
        public ProductMotorCCTV CCTV { get; set; }


        [Display(Name = "ทุนประกันภัย"), Range(0, int.MaxValue), Required]
        public decimal SumInsured { get; set; }
        [Display(Name = "เบี้ยประกันภัยสุทธิ"), Range(0, int.MaxValue), Required]
        public decimal NetPremium { get; set; }
        [Display(Name = "อากร"), Range(0, int.MaxValue), Required]
        public decimal Duty { get; set; }
        [Display(Name = "ภาษี"), Range(0, int.MaxValue), Required]
        public decimal Vat { get; set; }
        [Display(Name = "เบี้ยประกันภัย (รวมภาษีอากร)"), Range(0, int.MaxValue), Required]
        public decimal Premium { get; set; }
        [Display(Name = "เบี้ยรวมที่แสดงบนหน้า Web Site (เบี้ยสวย)"), Range(0, int.MaxValue), Required]
        public decimal DisplayPremium { get; set; }
        [Display(Name = "เบี้ยพ.ร.บ. (รวมภาษีอากร)"), Range(0, int.MaxValue), Required]
        public decimal CompulsoryPremium { get; set; }
        [Display(Name = "เบี้ย กรณีขายรวม พ.ร.บ."), Range(0, int.MaxValue), Required]
        public decimal CombinePremium { get; set; }
        [Display(Name = "เบี้ยรวมพ.ร.บ. ที่แสดงบนหน้า Web Site (เบี้ยสวย)"), Range(0, int.MaxValue), Required]
        public decimal DisplayCombinePremium { get; set; }
        
        [Display(Name = "ความเสียหายต่อรถยนต์"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public decimal OwnDamage { get; set; }
        [Display(Name = "ความเสียหายส่วนแรก"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public decimal OwnExcess { get; set; }
        [Display(Name = "ภัยน้ำท่วม"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public decimal Flood { get; set; }

        [Display(Name = "จังหวัดที่จดทะเบียน")]
        public List<ProductMotorPremiumRegisterProvince> RegisterProvince { get; set; }

        public bool EPolicy { get; set; }

        public Status Status { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateAdminId { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateAdminId { get; set; }

        public string CreateAdmin { get; set; }
        public string UpdateAdmin { get; set; }

        //public string Brand { get; set; }
        //public string Family { get; set; }
        //public string Model { get; set; }
    }

    public class ProductPRB {
        [Range(0, 999999)]
        public int PlanCode { get; set; }
        [Display(Name = "Net Premium"), Range(0, int.MaxValue), Required]
        public decimal NetPremium { get; set; }
        [Display(Name = "Duty"), Range(0, int.MaxValue), Required]
        public decimal Duty { get; set; }
        [Display(Name = "VAT"), Range(0, int.MaxValue), Required]
        public decimal Vat { get; set; }
        [Display(Name = "Premium including taxes and stamp"), Range(0, int.MaxValue), Required]
        public decimal Premium { get; set; }
    }
    
    public class ProductHomePremium {
        public int Id { get; set; }
        public int ProductId { get; set; }
        [Range(0, 999999)]
        public int PlanCode { get; set; }
        //public bool OccupiedByOwner { get; set; }
        [Display(Name = "ประเภทอาคาร")]
        public int PropertyTypeId { get; set; }
        [Display(Name = "ลักษณะสิ่งปลูกสร้าง (ชั้นสิ่งปลูกสร้าง)")]
        public int ConstructionTypeId { get; set; }
        [Display(Name = "จำนวนปีที่คุ้มครอง")]
        public int CoverPeriod { get; set; }
        
        [Display(Name = "Sum Insured"), Range(0, int.MaxValue), Required]
        public decimal SumInsured { get; set; }
        [Display(Name = "Net Premium"), Range(0, int.MaxValue), Required]
        public decimal NetPremium { get; set; }
        [Display(Name = "Duty"), Range(0, int.MaxValue), Required]
        public decimal Duty { get; set; }
        [Display(Name = "VAT"), Range(0, int.MaxValue), Required]
        public decimal Vat { get; set; }
        [Display(Name = "Premium including taxes and stamp"), Range(0, int.MaxValue), Required]
        public decimal Premium { get; set; }
        [Display(Name = "เบี้ยรวมที่แสดงบนหน้า Web Site (เบี้ยสวย)"), Range(0, int.MaxValue), Required]
        public decimal DisplayPremium { get; set; }

        [Display(Name = "ไฟไหม้ (TH)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string FireTH { get; set; }
        [Display(Name = "ไฟไหม้ (EN)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string FireEN { get; set; }
        [Display(Name = "อุบัติเหตุเสียหาย (TH)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string AccidentalDamageTH { get; set; }
        [Display(Name = "อุบัติเหตุเสียหาย (EN)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string AccidentalDamageEN { get; set; }
        [Display(Name = "กลุ่มภัยธรรมชาติ : ภัยลมพายุ/ ภัยแผ่นดินไหว/ ภัยลูกเห็บ/ ภัยน้ำท่วม (TH)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string NaturalRiskTH { get; set; }
        [Display(Name = "กลุ่มภัยธรรมชาติ : ภัยลมพายุ/ ภัยแผ่นดินไหว/ ภัยลูกเห็บ/ ภัยน้ำท่วม (EN)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string NaturalRiskEN { get; set; }
        [Display(Name = "ค่าเช่าสำหรับการเปลี่ยนพักอาศัยชั่วคราว (TH)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string TempRentalTH { get; set; }
        [Display(Name = "ค่าเช่าสำหรับการเปลี่ยนพักอาศัยชั่วคราว (EN)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string TempRentalEN { get; set; }

        [Display(Name = "ภัยโจรกรรม (TH)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string BurglaryTH { get; set; }
        [Display(Name = "ภัยโจรกรรม (EN)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string BurglaryEN { get; set; }
        [Display(Name = "ความรับต่อบุคคลภายนอก (TH)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string PublicLiabilityTH { get; set; }
        [Display(Name = "ความรับต่อบุคคลภายนอก (EN)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string PublicLiabilityEN { get; set; }

        public bool EPolicy { get; set; }

        public Status Status { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateAdminId { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateAdminId { get; set; }

        public string CreateAdmin { get; set; }
        public string UpdateAdmin { get; set; }

        [Display(Name = "ประเภทอาคาร")]
        public string PropertyType { get; set; }
        [Display(Name = "ลักษณะสิ่งปลูกสร้าง (ชั้นสิ่งปลูกสร้าง)")]
        public string ConstructionType { get; set; }


        //public ProductHomePremium GetKey(string Id) {
        //    string[] keys = Id.Split('|');
        //    if (keys.Length != 4) return null;
        //    ProductHomePremium item = new ProductHomePremium();
        //    item.ProductId = NullUtils.cvInt(keys[0]);
        //    item.PropertyTypeId = NullUtils.cvInt(keys[1]);
        //    item.ConstructionTypeId = NullUtils.cvInt(keys[2]);
        //    item.CoverPeriod = NullUtils.cvInt(keys[3]);
        //    return item;
        //}
    }
    
    public class ProductCareerClass {
        public int ProductId { get; set; }
        [Display(Name = "อาชีพ"), Required]
        public int CareerId { get; set; }
        [Display(Name = "ชั้นอาชีพ"), Range(1, 4), Required]
        public int CareerClass { get; set; }

        [Display(Name = "อาชีพ")]
        public string Career { get; set; }
    }
    
    public class ProductPAPremium {
        public int Id { get; set; }
        public int ProductId { get; set; }
        [Range(0, 999999)]
        public int PlanCode { get; set; }
        [Display(Name = "ประเภทความคุ้มครอง")]
        public ProductPAInsuranceType InsuranceType { get; set; }
        [Display(Name = "วิธีคำนวณอายุ")]
        public ProductAgeCalculation AgeCalculation { get; set; }
        [Range(0, 99)]
        public int MinAge { get; set; }
        [Range(1, 99)]
        public int MaxAge { get; set; }
        [Display(Name = "ชั้นอาชีพ 1")]
        public bool CareerClass1 { get; set; }
        [Display(Name = "ชั้นอาชีพ 2")]
        public bool CareerClass2 { get; set; }
        [Display(Name = "ชั้นอาชีพ 3")]
        public bool CareerClass3 { get; set; }
        [Display(Name = "ชั้นอาชีพ 4")]
        public bool CareerClass4 { get; set; }

        [Display(Name = "Sum Insured"), Range(0, int.MaxValue), Required]
        public decimal SumInsured { get; set; }
        [Display(Name = "Net Premium"), Required]
        public decimal NetPremium { get; set; }
        [Display(Name = "Duty"), Required]
        public decimal Duty { get; set; }
        [Display(Name = "VAT"), Required]
        public decimal Vat { get; set; }
        [Display(Name = "Premium including taxes and stamp"), Required]
        public decimal Premium { get; set; }
        [Display(Name = "เบี้ยรวมที่แสดงบนหน้า Web Site (เบี้ยสวย)"), Range(0, int.MaxValue), Required]
        public decimal DisplayPremium { get; set; }

        [Display(Name = "สูญเสียชีวิต, สูญเสียอวัยวะ ทุพพลภาพถาวรสิ้นเชิง (TH)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string LossLifeTH { get; set; }
        [Display(Name = "สูญเสียชีวิต, สูญเสียอวัยวะ ทุพพลภาพถาวรสิ้นเชิง (EN)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string LossLifeEN { get; set; }
        [Display(Name = "การรักษาพยาบาล (TH)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string MedicalExpenseTH { get; set; }
        [Display(Name = "การรักษาพยาบาล (EN)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string MedicalExpenseEN { get; set; }

        [Display(Name = "การถูกฆาตรกรรมหรือถูกทำร้ายร่างกาย (TH)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string MurderAssaultTH { get; set; }
        [Display(Name = "การถูกฆาตรกรรมหรือถูกทำร้ายร่างกาย (EN)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string MurderAssaultEN { get; set; }
        [Display(Name = "การขับขี่หรือโดยสารรถจักรยานยนต์ (TH)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string RidingPassengerMotorcycleTH { get; set; }
        [Display(Name = "การขับขี่หรือโดยสารรถจักรยานยนต์ (EN)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string RidingPassengerMotorcycleEN { get; set; }
        [Display(Name = "ชดเชยรายได้ (TH)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string IncomeCompensationTH { get; set; }
        [Display(Name = "ชดเชยรายได้ (EN)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string IncomeCompensationEN { get; set; }
        [Display(Name = "ค่าปรงศพ (TH)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string FuneralExpenseAccidentIllnessTH { get; set; }
        [Display(Name = "ค่าปรงศพ (EN)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string FuneralExpenseAccidentIllnessEN { get; set; }

        public bool EPolicy { get; set; }

        public Status Status { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateAdminId { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateAdminId { get; set; }

        public string CreateAdmin { get; set; }
        public string UpdateAdmin { get; set; }
        
    }
    
    public class ProductPHPremium {
        public int Id { get; set; }
        public int ProductId { get; set; }
        [Range(0, 999999)]
        public int PlanCode { get; set; }
        [Display(Name = "เพศ")]
        public Sex Sex { get; set; }
        [Display(Name = "วิธีคำนวณอายุ")]
        public ProductAgeCalculation AgeCalculation { get; set; }
        public int MinAgeMonth { get; set; }
        public int MinAge { get; set; }
        public int MaxAge { get; set; }
        [Display(Name = "ชั้นอาชีพ 1")]
        public bool CareerClass1 { get; set; }
        [Display(Name = "ชั้นอาชีพ 2")]
        public bool CareerClass2 { get; set; }
        [Display(Name = "ชั้นอาชีพ 3")]
        public bool CareerClass3 { get; set; }
        [Display(Name = "ชั้นอาชีพ 4")]
        public bool CareerClass4 { get; set; }
        public int MinWeight { get; set; }
        public int MaxWeight { get; set; }
        public int MinHeight { get; set; }
        public int MaxHeight { get; set; }

        [Display(Name = "Sum Insured"), Range(0, int.MaxValue), Required]
        public decimal SumInsured { get; set; }
        [Display(Name = "Net Premium"), Range(0, int.MaxValue), Required]
        public decimal NetPremium { get; set; }
        [Display(Name = "Duty"), Range(0, int.MaxValue), Required]
        public decimal Duty { get; set; }
        [Display(Name = "VAT"), Range(0, int.MaxValue), Required]
        public decimal Vat { get; set; }
        [Display(Name = "Premium including taxes and stamp"), Range(0, int.MaxValue), Required]
        public decimal Premium { get; set; }
        [Display(Name = "เบี้ยรวมที่แสดงบนหน้า Web Site (เบี้ยสวย)"), Range(0, int.MaxValue), Required]
        public decimal DisplayPremium { get; set; }

        [Display(Name = "ค่ารักษาพยาบาลผู้ป่วยใน (TH)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string IPDTH { get; set; }
        [Display(Name = "ค่ารักษาพยาบาลผู้ป่วยใน (EN)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string IPDEN { get; set; }
        [Display(Name = "ค่าห้องและค่าอาหาร (TH)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string IPDRoomBoardTH { get; set; }
        [Display(Name = "ค่าห้องและค่าอาหาร (EN)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string IPDRoomBoardEN { get; set; }
        [Display(Name = "ค่ารักษาพยาบาลผู้ป่วยนอก (OPD) (TH)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string OutpatientBenefitsTH { get; set; }
        [Display(Name = "ค่ารักษาพยาบาลผู้ป่วยนอก (OPD) (EN)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string OutpatientBenefitsEN { get; set; }
        [Display(Name = "ค่าใช้จ่ายอันเกิดจากการผ่าตัด ค่าปรึกษาแพทย์เกี่ยวกับการผ่าตัด (TH)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string SurgicalExpenseTH { get; set; }
        [Display(Name = "ค่าใช้จ่ายอันเกิดจากการผ่าตัด ค่าปรึกษาแพทย์เกี่ยวกับการผ่าตัด (EN)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string SurgicalExpenseEN { get; set; }
        [Display(Name = "การรักษาโรคมะเร็ง (TH)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string CancerTreatmentTH { get; set; }
        [Display(Name = "การรักษาโรคมะเร็ง (EN)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string CancerTreatmentEN { get; set; }
        [Display(Name = "เปลี่ยนถ่ายไขกระดูก/อวัยวะ (TH)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string OrganTransplantTH { get; set; }
        [Display(Name = "เปลี่ยนถ่ายไขกระดูก/อวัยวะ (EN)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string OrganTransplantEN { get; set; }

        public bool EPolicy { get; set; }

        public Status Status { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateAdminId { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateAdminId { get; set; }

        public string CreateAdmin { get; set; }
        public string UpdateAdmin { get; set; }

        public string Career { get; set; }
    }
    
    public class ProductTAPremium {
        public int Id { get; set; }
        public int ProductId { get; set; }
        [Range(0, 999999)]
        public int PlanCode { get; set; }
        [Display(Name = "ประเภทการเดินทาง")]
        public ProductTATripType TripType { get; set; }
        [Display(Name = "กลุ่มประเทศ")]
        public CountryZone Zone { get; set; }
        [Display(Name = "รายชื่อประเทศที่ประกัน")]
        public List<int> IncludedCountry { get; set; }
        [Display(Name = "แบบประกันภัย")]
        public ProductTACoverageOption CoverageOption { get; set; }
        [Display(Name = "ประเภทการประกันภัย")]
        public ProductTACoverageType CoverageType { get; set; }
        [Display(Name = "จำนวนวันที่เดินทางตั้่งแต่")]
        public int MinDays { get; set; }
        [Display(Name = "จำนวนวันที่เดินทางถึง")]
        public int MaxDays { get; set; }
        [Display(Name = "จำนวนคนเดินทาง")]
        public int Persons { get; set; }

        [Display(Name = "ทุนประกันภัย"), Range(0, int.MaxValue), Required]
        public decimal SumInsured { get; set; }
        [Display(Name = "เบี้ยประกันภัยสุทธิ"), Range(0, int.MaxValue), Required]
        public decimal NetPremium { get; set; }
        [Display(Name = "อากร"), Range(0, int.MaxValue), Required]
        public decimal Duty { get; set; }
        [Display(Name = "ภาษี"), Range(0, int.MaxValue), Required]
        public decimal Vat { get; set; }
        [Display(Name = "เบี้ยประกันภัย (รวมภาษีอากร)"), Range(0, int.MaxValue), Required]
        public decimal Premium { get; set; }
        [Display(Name = "เบี้ยรวมที่แสดงบนหน้า Web Site (เบี้ยสวย)"), Range(0, int.MaxValue), Required]
        public decimal DisplayPremium { get; set; }

        [Display(Name = "เสียชีวิต,สูญเสียอวัยวะ,ทุพพลภาพถาวร (TH)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string PersonalLossLifeTH { get; set; }
        [Display(Name = "เสียชีวิต,สูญเสียอวัยวะ,ทุพพลภาพถาวร (EN)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string PersonalLossLifeEN { get; set; }
        [Display(Name = "ค่ารักษาพยาบาลจากอุบัติเหตุ (TH)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string MedicalExpenseEachAccidentTH { get; set; }
        [Display(Name = "ค่ารักษาพยาบาลจากอุบัติเหตุ (EN)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string MedicalExpenseEachAccidentEN { get; set; }
        [Display(Name = "ค่ารักษาพยาบาลในต่างประเทศ (TH)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string MedicalExpenseAccidentSicknessTH { get; set; }
        [Display(Name = "ค่ารักษาพยาบาลในต่างประเทศ (EN)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string MedicalExpenseAccidentSicknessEN { get; set; }
        [Display(Name = "การเคลื่อนย้ายฉุกเฉิน (TH)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string EmergencyMedialEvacuationTH { get; set; }
        [Display(Name = "การเคลื่อนย้ายฉุกเฉิน (EN)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string EmergencyMedialEvacuationEN { get; set; }
        [Display(Name = "การเคลื่อนย้ายกลับสู่ประเทศไทย (TH)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string RepatriationExpenseTH { get; set; }
        [Display(Name = "การเคลื่อนย้ายกลับสู่ประเทศไทย (EN)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string RepatriationExpenseEN { get; set; }
        [Display(Name = "ค่าใช้จ่ายในการส่งศพ หรือ อัฐิกลับประเทศ (TH)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string RepatriationMortalRemainsTH { get; set; }
        [Display(Name = "ค่าใช้จ่ายในการส่งศพ หรือ อัฐิกลับประเทศ (EN)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string RepatriationMortalRemainsEN { get; set; }
        [Display(Name = "การสูญหาย หรือเสียหายของกระเป๋าเดินทาง และ/หรือทรัพย์สินส่วนตัวภายในกระเป๋า (TH)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string LossDamageBaggageTH { get; set; }
        [Display(Name = "การสูญหาย หรือเสียหายของกระเป๋าเดินทาง และ/หรือทรัพย์สินส่วนตัวภายในกระเป๋า (EN)"), DisplayFormat(ConvertEmptyStringToNull = false)]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public string LossDamageBaggageEN { get; set; }

        public bool EPolicy { get; set; }

        public Status Status { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateAdminId { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateAdminId { get; set; }

        public string CreateAdmin { get; set; }
        public string UpdateAdmin { get; set; }
    }

    public class ProductCategory {
        public int Id { get; set; }
        [Display(Name = "Title (TH)"), MaxLength(250), Required]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public String TitleTH { get; set; }
        [Display(Name = "Title (EN)"), MaxLength(250), Required]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public String TitleEN { get; set; }
        [Display(Name = "Brief (TH)"), DataType(DataType.MultilineText), DisplayFormat(ConvertEmptyStringToNull = false), AllowHtml, Required]
        public String BriefTH { get; set; }
        [Display(Name = "Brief (EN)"), DataType(DataType.MultilineText), DisplayFormat(ConvertEmptyStringToNull = false), AllowHtml, Required]
        public String BriefEN { get; set; }
        [Display(Name = "Home Text 1 (TH)"), MaxLength(30), Required]
        public String HomeText1TH { get; set; }
        [Display(Name = "Home Text 1  (EN)"), MaxLength(30), Required]
        public String HomeText1EN { get; set; }
        [Display(Name = "Home Text 2 (TH)"), MaxLength(100), Required]
        public String HomeText2TH { get; set; }
        [Display(Name = "Home Text 2  (EN)"), MaxLength(100), Required]
        public String HomeText2EN { get; set; }
        [Display(Name = "Display on Home Page")]
        public bool DisplayHome { get; set; }
        [Display(Name = "Payment By Credit Card")]
        public bool PaymentCreditCard { get; set; }
        [Display(Name = "Payment By Credit Card Installment")]
        public bool PaymentCreditCardInstallment { get; set; }
        [Display(Name = "Payment By Counter Service")]
        public bool PaymentCounterService { get; set; }

        [Display(Name = "Order"), Required]
        public float Sequence { get; set; }
        [Required]
        public Status Status { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateAdminId { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateAdminId { get; set; }
        
        public string CreateAdmin { get; set; }
        public string UpdateAdmin { get; set; }


        public static ProductCategoryKey GetCategoryBySubCategory(ProductSubCategoryKey val) {
            switch (val) {
                case ProductSubCategoryKey.Motor_1:
                case ProductSubCategoryKey.Motor_2:
                case ProductSubCategoryKey.Motor_2P:
                case ProductSubCategoryKey.Motor_3:
                case ProductSubCategoryKey.Motor_3P:
                    return ProductCategoryKey.Motor;
                case ProductSubCategoryKey.Home:
                    return ProductCategoryKey.Home;
                case ProductSubCategoryKey.PA:
                    return ProductCategoryKey.PA;
                case ProductSubCategoryKey.PH:
                    return ProductCategoryKey.PH;
                case ProductSubCategoryKey.TA:
                    return ProductCategoryKey.TA;
                case ProductSubCategoryKey.PRB_Car:
                case ProductSubCategoryKey.PRB_Pickup:
                case ProductSubCategoryKey.PRB_Van:
                    return ProductCategoryKey.PRB;
            }
            return ProductCategoryKey.Motor;
        }
    }


    public class ProductSubCategory {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string TitleTH { get; set; }
        public string TitleEN { get; set; }
        public string Code { get; set; }
    }

    public class ProductSubClass {
        public int Id { get; set; }
        public ProductCategoryKey CategoryId { get; set; }
        [Display(Name = "Class"), MaxLength(3), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string Class { get; set; }
        [Display(Name = "Sub Class"), MaxLength(20), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string SubClass { get; set; }
        [Display(Name = "Sub Class Conf Group"), MaxLength(250), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string SubClassConfGroup { get; set; }
        [Display(Name = "Title (TH)"), MaxLength(250), Required]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public String TitleTH { get; set; }
        [Display(Name = "Title (EN)"), MaxLength(250), Required]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        public String TitleEN { get; set; }

        public Status Status { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateAdminId { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateAdminId { get; set; }
    }

    public class ProductCarModel : CarModel {
        public int ProductId { get; set; }
        [Display(Name = "Model"), Required]
        public int CarModelId { get; set; }
    }

    public class ProductRegisterProvince {
        [Display(Name = "Product")]
        public int ProductId { get; set; }
        [Display(Name = "Province")]
        public int RegisterProvinceId { get; set; }
        [Display(Name = "Code")]
        public string RegisterProvinceCode { get; set; }
        public bool IsUsed { get; set; }

        [Display(Name = "Province")]
        public string RegisterProvince { get; set; }
        public string Code { get; set; }
    }

    public class ProductMotorPremiumRegisterProvince {
        public int PremiumId { get; set; }
        public string RegisterProvinceCode { get; set; }
        public bool IsUsed { get; set; }
    }

    public enum ProductCategoryKey {
        [Display(Name = "ประกันรถยนต์")]
        Motor = 100,
        [Display(Name = "พ.ร.บ.")]
        PRB = 200,
        [Display(Name = "ประกันอุบัติเหตุ")]
        PA = 300,
        [Display(Name = "ประกันการเดินทาง")]
        TA = 400,
        [Display(Name = "ประกันสุขภาพ")]
        PH = 500,
        [Display(Name = "ประกันสำหรับบ้านและที่อยู่อาศัย")]
        Home = 600
    }

    public enum ProductSubCategoryKey {
        None = 0,
        [Display(Name = "ประกันรถยนต์ชั้น 1")]
        Motor_1 = 110,
        [Display(Name = "ประกันรถยนต์ชั้น 2")]
        Motor_2 = 120,
        [Display(Name = "ประกันรถยนต์ชั้น 2+")]
        Motor_2P = 121,
        [Display(Name = "ประกันรถยนต์ชั้น 3")]
        Motor_3 = 130,
        [Display(Name = "ประกันรถยนต์ชั้น 3+")]
        Motor_3P = 131,
        [Display(Name = "พ.ร.บ. เก๋ง")]
        PRB_Car = 210,
        [Display(Name = "พ.ร.บ. กระบะ")]
        PRB_Pickup = 211,
        [Display(Name = "พ.ร.บ. ตู้")]
        PRB_Van = 212,
        [Display(Name = "ประกันอุบัติเหตุ")]
        PA = 310,
        [Display(Name = "ประกันการเดินทาง")]
        TA = 410,
        [Display(Name = "ประกันสุขภาพ")]
        PH = 510,
        [Display(Name = "ประกันสำหรับบ้านและที่อยู่อาศัย")]
        Home = 610
    }

    [Flags]
    public enum ProductSaleChannel { 
        All = 3,
        Online = 1,
        Offline = 2
    }

    public enum ProductMotorCCTV {
        All = 2,
        Yes = 1,
        No = 0
    }

    public enum ProductMotorGarageType {
        [Display(Name = "ซ่อมอู่")]
        Garage = 2,
        [Display(Name = "ซ่อมห้าง")]
        Dealer = 1
    }

    public enum ProductMotorCarInspectorMethod {
        [Display(Name = "ไม่มี")]
        None = 0,
        [Display(Name = "ส่งเจ้าหน้าที่ตรวจสภาพ")]
        Person = 1,
        [Display(Name = "ส่งรูปตรวจสภาพ")]
        Attachment = 2,
    }


    public enum ProductMotorVehicleUsage {
        [Display(Name = "รถเก๋งส่วนบุคคล")]
        Normal_Personal = 110,
        [Display(Name = "รถเก๋งเพื่อการพาณิชย์")]
        Normal_Commercial = 120,
        [Display(Name = "รถโดยสารส่วนบุคคล")]
        Passenger_Personal = 210,
        [Display(Name = "รถบรรทุกเพื่อการพาณิชย์")]
        Pickup_Commercial = 320
    }

    public enum ProductPAInsuranceType {
        [Display(Name = "อบ.1")]
        PA1 = 1,
        [Display(Name = "อบ.2")]
        PA2 = 2
    }

    public enum ProductTATripType {
        [Display(Name = "เดินทางต่างประเทศ")]
        Foreign = 1,
        [Display(Name = "เดินทางในประเทศ")]
        Domestic = 2,
        [Display(Name = "เดินทางเพื่อการศึกษา")]
        Education = 3
    }

    public enum ProductTACoverageOption {
        [Display(Name = "รายเที่ยว")]
        Trip = 1,
        [Display(Name = "รายปี")]
        Year = 2,
    }
    
    public enum ProductTACoverageType {
        [Display(Name = "รายบุคคล")]
        Individual = 1,
        [Display(Name = "รายครอบครัว")]
        Family = 2,
    }

    public enum Sex {
        [Display(Name = "ชาย")]
        Male = 1,
        [Display(Name = "หญิง")]
        Female = 2
    }

    public enum ProductAgeCalculation {
        [Display(Name = "ครบปีบริบูรณ์")]
        Floor = 1,
        [Display(Name = "ปัดเศษขึ้น")]
        Ceiling = 2
    }
}
