﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TobJod.Models {
    public class AdminAuthenLog {
        public string AdminId { get; set; }
        public string SessionId { get; set; }
        public DateTime Login { get; set; }
        public DateTime Logout { get; set; }
        public string PublicIP { get; set; }
        public string LocalIP { get; set; }
        public Status Status { get; set; }
        public DateTime CreateDate { get; set; }

        public Admin Admin { get; set; }
    }
}