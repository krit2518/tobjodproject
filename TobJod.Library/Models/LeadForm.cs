﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TobJod.Models {
    public class LeadForm {
        public int Id { get; set; }
        [Display(Name = "Name"), MaxLength(50), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string Name { get; set; }
        [Display(Name = "Surname"), MaxLength(50), Required]
        [RegularExpression("^[^<>~\\u005E:;@%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; @ % & ' \" {{ }} [ ]) should not be entered")]
        public string Surname { get; set; }
        [Display(Name = "Tel"), MaxLength(10), Required]
        [RegularExpression("^[0-9]+$", ErrorMessage = "Only 0-9 is allowed")]
        public string Tel { get; set; }
        [Display(Name = "Email"), MaxLength(250), DataType(DataType.EmailAddress), Required]
        [RegularExpression("^[^<>~\\u005E:;%\\u0026\\u0027\\u0022\\u007B\\u007D\\u005B\\u005D]+$", ErrorMessage = "Special character (< > ~ ^ : ; % & ' \" {{ }} [ ]) should not be entered")]
        public string Email { get; set; }
        public string SessionId { get; set; }
        public string IPPublic { get; set; }
        public string IPPrivate { get; set; }
        public Status Status { get; set; }
        public DateTime CreateDate { get; set; }
        public OrderStatus APIStatus { get; set; }
        public string FullName { get; set; }
    }

    public class LeadFormProductApplication : LeadForm {
        public string CarSerialNo { get; set; }
        public string CarEngineNo { get; set; }
        public string CarPlateNo1 { get; set; }
        public string CarPlateNo2 { get; set; }
        public string CarProvince { get; set; }
        public string ProductCode { get; set; }
        public APIStatusCategory APIStatusCategory { get; set; }
        public string OrderCode { get; set; }
        public OrderStatus OrderStatus { get; set; }
    }
}
