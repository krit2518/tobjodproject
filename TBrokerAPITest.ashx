﻿<%@ WebHandler Language="C#" Class="TobJod.Web.TBrokerAPITest" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using TobJod;

namespace TobJod.Web {
    /// <summary>
    /// Summary description for TBrokerAPI
    /// </summary>
    public class TBrokerAPITest : IHttpHandler {

        protected HttpContext context;

        public void ProcessRequest(HttpContext context) {
            //context.Response.ContentType = "text/plain";
            // context.Response.Write("Hello World");
            //context.Response.End();
            this.context = context;
            switch (context.Request.Params["action"]) {
                case "CheckDuplicatePolicyMT":
                    CheckDuplicatePolicyMT(); break;
                case "ReservePolicyMT":
                    ReservePolicyMT(); break;
                case "SubmitPolicyMT":
                    SubmitPolicyMT(); break;
                case "SubmitPolicyMI":
                    SubmitPolicyMI(); break;
                case "GetReceivedPolicy":
                    GetReceivedPolicy(); break;
            }
        }

        private void CheckDuplicatePolicyMT() {
            using (DataSet ds = new DataSet()) {
                DataTable dt = ds.Tables.Add("table");
                dt.Columns.Add("CAR_SN_BODY");
                dt.Columns.Add("CAR_ENGINE_NO");
                dt.Columns.Add("CAR_PLAT_NO_1");
                dt.Columns.Add("CAR_PLAT_NO_2");
                dt.Columns.Add("CAR_PLAT_PROVINCE_ID");
                dt.Rows.Add("serial_no1", "engine_no1", "1กก", "0011", "01");
                //DebugDataSet(ds);

                TBrokerAPI.ServiceDigitalSoapClient api = new TBrokerAPI.ServiceDigitalSoapClient();
                DebugDataSet(api.CheckDuplicatePolicyMT(ds));
            }
        }

        private void ReservePolicyMT() {
            using (DataSet ds = new DataSet()) {
                DataTable dt = ds.Tables.Add("table");
                dt.Columns.Add("CAR_SN_BODY");
                dt.Columns.Add("CAR_ENGINE_NO");
                dt.Columns.Add("CAR_PLAT_NO_1");
                dt.Columns.Add("CAR_PLAT_NO_2");
                dt.Columns.Add("CAR_PLAT_PROVINCE_ID");
                dt.Columns.Add("EXPIRATION_DATE");
                dt.Rows.Add("serial", "engine", "11", "234", "01", DateTime.Now.AddDays(1));

                TBrokerAPI.ServiceDigitalSoapClient api = new TBrokerAPI.ServiceDigitalSoapClient();
                DebugDataSet(api.ReservePolicyMT(ds));
            }
        }

        private void SubmitPolicyMT() {
            using (DataSet ds = new DataSet()) {
                DataTable dt = ds.Tables.Add("table");
                dt.Columns.Add("ORDER_NO");
                dt.Columns.Add("CA_NO");
                dt.Columns.Add("CA_GROUP");
                dt.Columns.Add("SALE_CHANNEL");
                dt.Columns.Add("CARD_TYPE");
                dt.Columns.Add("CUST_ID_CARD");
                dt.Columns.Add("CUST_TITLE");
                dt.Columns.Add("CUST_FNAME");
                dt.Columns.Add("CUST_LNAME");
                dt.Columns.Add("CUST_BIRTHDAY");
                dt.Columns.Add("CUST_PHONE");
                dt.Columns.Add("CUST_MOBILE");
                dt.Columns.Add("CUST_EMAIL");
                dt.Columns.Add("CUST_FACEBOOK");
                dt.Columns.Add("CUST_INSTAGRAM");
                dt.Columns.Add("CUST_LINEID");
                dt.Columns.Add("CUST_OCCUPATION");
                dt.Columns.Add("PROD_FLAG");
                dt.Columns.Add("POLICY_CLASS");
                dt.Columns.Add("POLICY_SUBCLASS");
                dt.Columns.Add("POLICY_PACKAGE_CODE");
                dt.Columns.Add("POLICY_INS_CODE");
                dt.Columns.Add("POLICY_AMOUNT");
                dt.Columns.Add("POLICY_NET_AMT");
                dt.Columns.Add("POLICY_DISC_AMOUNT");
                dt.Columns.Add("POLICY_DISC_PRECEN");
                dt.Columns.Add("POLICY_REVSTAMP");
                dt.Columns.Add("POLICY_VAT");
                dt.Columns.Add("POLICY_TOTAL_AMOUNT");
                dt.Columns.Add("POLICY_NO");
                dt.Columns.Add("POLICY_BGDATE");
                dt.Columns.Add("POLICY_ENDDATE");
                dt.Columns.Add("ACT_CLASS");
                dt.Columns.Add("ACT_SUBCLASS");
                dt.Columns.Add("ACT_PACKAGE_CODE");
                dt.Columns.Add("ACT_INS_CODE");
                dt.Columns.Add("ACT_AMOUNT");
                dt.Columns.Add("ACT_NET_AMOUNT");
                dt.Columns.Add("ACT_DISC_AMOUNT");
                dt.Columns.Add("ACT_DISC_PERCENT");
                dt.Columns.Add("ACT_REVSTAMP");
                dt.Columns.Add("ACT_VAT");
                dt.Columns.Add("ACT_TOTAL_AMOUNT");
                dt.Columns.Add("ACT_POLICY_NO");
                dt.Columns.Add("ACT_BGDATE");
                dt.Columns.Add("ACT_ENDDATE");
                dt.Columns.Add("DRIV1_STATUS");
                dt.Columns.Add("DRIV1_CARD_TYPE");
                dt.Columns.Add("DRIV1_ID_CARD");
                dt.Columns.Add("DRIV1_FNAME");
                dt.Columns.Add("DRIV1_LNAME");
                dt.Columns.Add("DRIV1_BIRTHDAY");
                dt.Columns.Add("DRIV2_STATUS");
                dt.Columns.Add("DRIV2_CARD_TYPE");
                dt.Columns.Add("DRIV2_ID_CARD");
                dt.Columns.Add("DRIV2_FNAME");
                dt.Columns.Add("DRIV2_LNAME");
                dt.Columns.Add("DRIV2_BIRTHDAY");
                dt.Columns.Add("CAR_BRAND_DESC");
                dt.Columns.Add("CAR_MODEL_DESC");
                dt.Columns.Add("CAR_ENGINE_CC");
                dt.Columns.Add("CAR_YEAR");
                dt.Columns.Add("CAR_STATUS");
                dt.Columns.Add("CAR_PLAT_NO_1");
                dt.Columns.Add("CAR_PLAT_NO_2");
                dt.Columns.Add("CAR_PLAT_PROVINCE_ID");
                dt.Columns.Add("CAR_TYPE");
                dt.Columns.Add("CAR_SN_BODY");
                dt.Columns.Add("CAR_SN_ENGINE");
                dt.Columns.Add("CAR_REPAIR_TYPE");
                dt.Columns.Add("NCB");
                dt.Columns.Add("NCB_NET_AMOUNT");
                dt.Columns.Add("SEND_INSURANCE_FLAG");
                dt.Columns.Add("ADD_IDCARD_NO");
                dt.Columns.Add("ADD_IDCARD_VILLAGE");
                dt.Columns.Add("ADD_IDCARD_NO_MU");
                dt.Columns.Add("ADD_IDCARD_ALLEY");
                dt.Columns.Add("ADD_IDCARD_ROAD");
                dt.Columns.Add("ADD_IDCARD_DISTRICT");
                dt.Columns.Add("ADD_IDCARD_COUNTY");
                dt.Columns.Add("ADD_IDCARD_PROVINCE");
                dt.Columns.Add("ADD_IDCARD_ZIPCODE");
                dt.Columns.Add("ADD_CURRENT_NO_ADD");
                dt.Columns.Add("ADD_CURRENT_VILLAGE");
                dt.Columns.Add("ADD_CURRENT_NO_MU");
                dt.Columns.Add("ADD_CURRENT_ALLEY");
                dt.Columns.Add("ADD_CURRENT_ROAD");
                dt.Columns.Add("ADD_CURRENT_DISTRICT");
                dt.Columns.Add("ADD_CURRENT_COUNTY");
                dt.Columns.Add("ADD_CURRENT_PROVINCE");
                dt.Columns.Add("ADD_CURRENT_ZIPCODE");
                dt.Columns.Add("ADD_INVOICE_NAME");
                dt.Columns.Add("ADD_INVOICE_NO_ADD");
                dt.Columns.Add("ADD_INVOICE_VILLAGE");
                dt.Columns.Add("ADD_INVOICE_NO_MU");
                dt.Columns.Add("ADD_INVOICE_ALLEY");
                dt.Columns.Add("ADD_INVOICE_ROAD");
                dt.Columns.Add("ADD_INVOICE_DISTRICT");
                dt.Columns.Add("ADD_INVOICE_AMPHUR");
                dt.Columns.Add("ADD_INVOICE_PROVINCE");
                dt.Columns.Add("ADD_INVOICE_ZIPCODE");
                dt.Columns.Add("ADD_POLICY_NO");
                dt.Columns.Add("ADD_POLICY_VILLAGE");
                dt.Columns.Add("ADD_POLICY_NO_MU");
                dt.Columns.Add("ADD_POLICY_ALLEY");
                dt.Columns.Add("ADD_POLICY_ROAD");
                dt.Columns.Add("ADD_POLICY_DISTRICT");
                dt.Columns.Add("ADD_POLICY_COUNTY");
                dt.Columns.Add("ADD_POLICY_PROVINCE");
                dt.Columns.Add("ADD_POLICY_ZIPCODE");
                dt.Columns.Add("PAY_TYPE");
                dt.Columns.Add("PAY_INSTALLMENT");
                dt.Columns.Add("PAY_BANK_CODE");
                dt.Columns.Add("PAY_STATUS");
                dt.Columns.Add("PAY_MAILING_TYPE");
                dt.Columns.Add("PAY_POLICY_STATUS");
                dt.Columns.Add("PAY_GATEWAY_CODE");
                dt.Columns.Add("PAY_REF_NO");
                dt.Columns.Add("PAY_PAID_DATE");
                DataRow dr = dt.NewRow();
                dr["ORDER_NO"] = "MT012018020900001";
                dr["CA_NO"] = "ca_no";
                dr["CA_GROUP"] = "";
                dr["SALE_CHANNEL"] = "01";
                dr["CARD_TYPE"] = "01";
                dr["CUST_ID_CARD"] = "1234567891234";
                dr["CUST_TITLE"] = "คุณ";
                dr["CUST_FNAME"] = "เจน";
                dr["CUST_LNAME"] = "จิรา";
                dr["CUST_BIRTHDAY"] = "31/01/1990";
                dr["CUST_PHONE"] = "";
                dr["CUST_MOBILE"] = "0891234567";
                dr["CUST_EMAIL"] = "jan@jira.com";
                dr["CUST_FACEBOOK"] = "";
                dr["CUST_INSTAGRAM"] = "";
                dr["CUST_LINEID"] = "";
                dr["CUST_OCCUPATION"] = "";
                dr["PROD_FLAG"] = "3";
                dr["POLICY_CLASS"] = "MO";
                dr["POLICY_SUBCLASS"] = "V";
                dr["POLICY_PACKAGE_CODE"] = "V1";
                dr["POLICY_INS_CODE"] = "TNI";
                dr["POLICY_AMOUNT"] = 1000000;
                dr["POLICY_NET_AMT"] = 1000;
                dr["POLICY_DISC_AMOUNT"] = 0;
                dr["POLICY_DISC_PRECEN"] = 0;
                dr["POLICY_REVSTAMP"] = 200;
                dr["POLICY_VAT"] = 30;
                dr["POLICY_TOTAL_AMOUNT"] = 1230;
                dr["POLICY_NO"] = "";
                dr["POLICY_BGDATE"] = "02/01/2018";
                dr["POLICY_ENDDATE"] = "02/01/2019";
                dr["ACT_CLASS"] = "MO";
                dr["ACT_SUBCLASS"] = "C";
                dr["ACT_PACKAGE_CODE"] = "C";
                dr["ACT_INS_CODE"] = "TNI";
                dr["ACT_AMOUNT"] = 10000;
                dr["ACT_NET_AMOUNT"] = 100;
                dr["ACT_DISC_AMOUNT"] = 0;
                dr["ACT_DISC_PERCENT"] = 0;
                dr["ACT_REVSTAMP"] = 20;
                dr["ACT_VAT"] = 3;
                dr["ACT_TOTAL_AMOUNT"] = 123;
                dr["ACT_POLICY_NO"] = "act_policy";
                dr["ACT_BGDATE"] = "01/01/2018";
                dr["ACT_ENDDATE"] = "02/01/2019";
                dr["DRIV1_STATUS"] = "Y";
                dr["DRIV1_CARD_TYPE"] = "1";
                dr["DRIV1_ID_CARD"] = "1234567890123";
                dr["DRIV1_FNAME"] = "เจน";
                dr["DRIV1_LNAME"] = "จิรา";
                dr["DRIV1_BIRTHDAY"] = "31/10/1990";
                dr["DRIV2_STATUS"] = "N";
                dr["DRIV2_CARD_TYPE"] = "";
                dr["DRIV2_ID_CARD"] = "";
                dr["DRIV2_FNAME"] = "";
                dr["DRIV2_LNAME"] = "";
                dr["DRIV2_BIRTHDAY"] = "01/01/1900";
                dr["CAR_BRAND_DESC"] = "HONDA";
                dr["CAR_MODEL_DESC"] = "CIVIC 1.5";
                dr["CAR_ENGINE_CC"] = "1500";
                dr["CAR_YEAR"] = "2010";
                dr["CAR_STATUS"] = "N";
                dr["CAR_PLAT_NO_1"] = "1กก";
                dr["CAR_PLAT_NO_2"] = "2345";
                dr["CAR_PLAT_PROVINCE_ID"] = "01";
                dr["CAR_TYPE"] = "1.10D";
                dr["CAR_SN_BODY"] = "serial";
                dr["CAR_SN_ENGINE"] = "engine";
                dr["CAR_REPAIR_TYPE"] = "01";
                dr["NCB"] = 0;
                dr["NCB_NET_AMOUNT"] = 0;
                dr["SEND_INSURANCE_FLAG"] = "N";
                dr["ADD_IDCARD_NO"] = "";
                dr["ADD_IDCARD_VILLAGE"] = "";
                dr["ADD_IDCARD_NO_MU"] = "";
                dr["ADD_IDCARD_ALLEY"] = "";
                dr["ADD_IDCARD_ROAD"] = "";
                dr["ADD_IDCARD_DISTRICT"] = "";
                dr["ADD_IDCARD_COUNTY"] = "";
                dr["ADD_IDCARD_PROVINCE"] = "";
                dr["ADD_IDCARD_ZIPCODE"] = "";
                dr["ADD_CURRENT_NO_ADD"] = "1/2";
                dr["ADD_CURRENT_VILLAGE"] = "";
                dr["ADD_CURRENT_NO_MU"] = "";
                dr["ADD_CURRENT_ALLEY"] = "";
                dr["ADD_CURRENT_ROAD"] = "";
                dr["ADD_CURRENT_DISTRICT"] = "คันนายาว";
                dr["ADD_CURRENT_COUNTY"] = "คันนายาว";
                dr["ADD_CURRENT_PROVINCE"] = "กรุงเทพมหานคร";
                dr["ADD_CURRENT_ZIPCODE"] = "10230";
                dr["ADD_INVOICE_NAME"] = "";
                dr["ADD_INVOICE_NO_ADD"] = "";
                dr["ADD_INVOICE_VILLAGE"] = "";
                dr["ADD_INVOICE_NO_MU"] = "";
                dr["ADD_INVOICE_ALLEY"] = "";
                dr["ADD_INVOICE_ROAD"] = "";
                dr["ADD_INVOICE_DISTRICT"] = "";
                dr["ADD_INVOICE_AMPHUR"] = "";
                dr["ADD_INVOICE_PROVINCE"] = "";
                dr["ADD_INVOICE_ZIPCODE"] = "";
                dr["ADD_POLICY_NO"] = "";
                dr["ADD_POLICY_VILLAGE"] = "";
                dr["ADD_POLICY_NO_MU"] = "";
                dr["ADD_POLICY_ALLEY"] = "";
                dr["ADD_POLICY_ROAD"] = "";
                dr["ADD_POLICY_DISTRICT"] = "";
                dr["ADD_POLICY_COUNTY"] = "";
                dr["ADD_POLICY_PROVINCE"] = "";
                dr["ADD_POLICY_ZIPCODE"] = "";
                dr["PAY_TYPE"] = "7";
                dr["PAY_INSTALLMENT"] = 0;
                dr["PAY_BANK_CODE"] = "KBANK";
                dr["PAY_STATUS"] = "Y";
                dr["PAY_MAILING_TYPE"] = "1";
                dr["PAY_POLICY_STATUS"] = "1";
                dr["PAY_GATEWAY_CODE"] = "2C2P";
                dr["PAY_REF_NO"] = "pay_ref_no";
                dr["PAY_PAID_DATE"] = "09/02/2018";
                dt.Rows.Add(dr);

                TBrokerAPI.ServiceDigitalSoapClient api = new TBrokerAPI.ServiceDigitalSoapClient();
                DebugDataSet(api.SubmitPolicyMT(ds));
            }
        }

        private void SubmitPolicyMI() {
            using (DataSet ds = new DataSet()) {
                DataTable dt = ds.Tables.Add("table");
                dt.Columns.Add("ORDER_NO");
                dt.Columns.Add("CA_NO");
                dt.Columns.Add("CA_GROUP");
                dt.Columns.Add("CHANNEL");
                dt.Columns.Add("APP_DATE");
                dt.Columns.Add("CARD_TYPE");
                dt.Columns.Add("CUST_ID_CARD");
                dt.Columns.Add("CUST_TITLE");
                dt.Columns.Add("CUST_FNAME");
                dt.Columns.Add("CUST_LNAME");
                dt.Columns.Add("CUST_BIRTHDAY");
                dt.Columns.Add("CUST_PHONE");
                dt.Columns.Add("CUST_MOBILE");
                dt.Columns.Add("CUST_EMAIL");
                dt.Columns.Add("CUST_FACEBOOK");
                dt.Columns.Add("CUST_INSTAGRAM");
                dt.Columns.Add("CUST_LINEID");
                dt.Columns.Add("CUST_OCCUPATION");
                dt.Columns.Add("CUST_TYPE");
                dt.Columns.Add("DEPARTMENT_NAME");
                dt.Columns.Add("OWNER_NAME");
                dt.Columns.Add("CUST_AGE");
                dt.Columns.Add("BENEFIT_NM");
                dt.Columns.Add("RELATION_BENEFIT");
                dt.Columns.Add("INS_CODE");
                dt.Columns.Add("INS_NM");
                dt.Columns.Add("OUTLET");
                dt.Columns.Add("HUB");
                dt.Columns.Add("AGENT_CODE");
                dt.Columns.Add("AGENT_NAME");
                dt.Columns.Add("GROUPHEAD");
                dt.Columns.Add("POLICY_TYPE");
                dt.Columns.Add("INDIVIDUAL_TYPE");
                dt.Columns.Add("INS_CUST_NUM");
                dt.Columns.Add("INS_DETAIL");
                dt.Columns.Add("COUNTRY");
                dt.Columns.Add("MIN_AGE");
                dt.Columns.Add("MAX_AGE");
                dt.Columns.Add("PLAN");
                dt.Columns.Add("CLASS");
                dt.Columns.Add("SUBCLASS");
                dt.Columns.Add("POLICY_NO_OLD");
                dt.Columns.Add("EFF_DATE");
                dt.Columns.Add("EXP_DATE");
                dt.Columns.Add("SUMINSURE");
                dt.Columns.Add("PREMIUM");
                dt.Columns.Add("DUTY");
                dt.Columns.Add("TAX");
                dt.Columns.Add("NETPREMIUM");
                dt.Columns.Add("PAY_CODE");
                dt.Columns.Add("INS_NM_PAY");
                dt.Columns.Add("SEND_CODE");
                dt.Columns.Add("CIF");
                dt.Columns.Add("AO_CODE");
                dt.Columns.Add("OFF_NAME");
                dt.Columns.Add("DEP_CODE");
                dt.Columns.Add("DEPT_NM");
                dt.Columns.Add("MATCH_CHANNEL");
                dt.Columns.Add("RM");
                dt.Columns.Add("PACKAGE");
                dt.Columns.Add("RISK_EXP_CODE");
                dt.Columns.Add("BUILD_CLASS_CODE");
                dt.Columns.Add("LOCATION_ADDR");
                dt.Columns.Add("LOCATION_DISTRICT");
                dt.Columns.Add("LOCATION_AMPHUR");
                dt.Columns.Add("LOCATION_PROVINCE");
                dt.Columns.Add("LOCATION_ZIPCODE");
                dt.Columns.Add("RATE");
                dt.Columns.Add("FIRE_DISCRATE");
                dt.Columns.Add("CODEPAY_DESCRIPTION");
                dt.Columns.Add("SHIPPING_ADDR");
                dt.Columns.Add("IB_FLAG");
                dt.Columns.Add("LOAN_NO");
                dt.Columns.Add("POLICY_NO");
                dt.Columns.Add("ADD_INFO_CODE1");
                dt.Columns.Add("ADD_INFO_VALUE1");
                dt.Columns.Add("ADD_INFO_CODE2");
                dt.Columns.Add("ADD_INFO_VALUE2");
                dt.Columns.Add("ADD_INFO_CODE3");
                dt.Columns.Add("ADD_INFO_VALUE3");
                dt.Columns.Add("ADD_INFO_CODE4");
                dt.Columns.Add("ADD_INFO_VALUE4");
                dt.Columns.Add("ADD_INFO_CODE5");
                dt.Columns.Add("ADD_INFO_VALUE5");
                dt.Columns.Add("ADD_IDCARD_NO");
                dt.Columns.Add("ADD_IDCARD_VILLAGE");
                dt.Columns.Add("ADD_IDCARD_NO_MU");
                dt.Columns.Add("ADD_IDCARD_ALLEY");
                dt.Columns.Add("ADD_IDCARD_ROAD");
                dt.Columns.Add("ADD_IDCARD_DISTRICT");
                dt.Columns.Add("ADD_IDCARD_COUNTY");
                dt.Columns.Add("ADD_IDCARD_PROVINCE");
                dt.Columns.Add("ADD_IDCARD_ZIPCODE");
                dt.Columns.Add("ADD_CURRENT_NO_ADD");
                dt.Columns.Add("ADD_CURRENT_VILLAGE");
                dt.Columns.Add("ADD_CURRENT_NO_MU");
                dt.Columns.Add("ADD_CURRENT_ALLEY");
                dt.Columns.Add("ADD_CURRENT_ROAD");
                dt.Columns.Add("ADD_CURRENT_DISTRICT");
                dt.Columns.Add("ADD_CURRENT_COUNTY");
                dt.Columns.Add("ADD_CURRENT_PROVINCE");
                dt.Columns.Add("ADD_CURRENT_ZIPCODE");
                dt.Columns.Add("ADD_INVOICE_NAME");
                dt.Columns.Add("ADD_INVOICE_NO_ADD");
                dt.Columns.Add("ADD_INVOICE_VILLAGE");
                dt.Columns.Add("ADD_INVOICE_NO_MU");
                dt.Columns.Add("ADD_INVOICE_ALLEY");
                dt.Columns.Add("ADD_INVOICE_ROAD");
                dt.Columns.Add("ADD_INVOICE_DISTRICT");
                dt.Columns.Add("ADD_INVOICE_AMPHUR");
                dt.Columns.Add("ADD_INVOICE_PROVINCE");
                dt.Columns.Add("ADD_INVOICE_ZIPCODE");
                dt.Columns.Add("ADD_POLICY_NO");
                dt.Columns.Add("ADD_POLICY_VILLAGE");
                dt.Columns.Add("ADD_POLICY_NO_MU");
                dt.Columns.Add("ADD_POLICY_ALLEY");
                dt.Columns.Add("ADD_POLICY_ROAD");
                dt.Columns.Add("ADD_POLICY_DISTRICT");
                dt.Columns.Add("ADD_POLICY_COUNTY");
                dt.Columns.Add("ADD_POLICY_PROVINCE");
                dt.Columns.Add("ADD_POLICY_ZIPCODE");
                dt.Columns.Add("TAX_IS_INSURED");
                dt.Columns.Add("TAX_PERSON_TYPE");
                dt.Columns.Add("TAX_OCCUPATION");
                dt.Columns.Add("TAX_PERSON_CARD_TYPE");
                dt.Columns.Add("TAX_PERSON_CARD_ID");
                dt.Columns.Add("TAX_ID");
                dt.Columns.Add("PAY_CREDIT_TERM_TYPE");
                dt.Columns.Add("PAY_INSTALLMENT");
                dt.Columns.Add("PAY_TYPE");
                dt.Columns.Add("PAY_BANK_CODE");
                dt.Columns.Add("PAY_STATUS");
                dt.Columns.Add("PAY_MAILING_TYPE");
                dt.Columns.Add("PAY_POLI_NO");
                dt.Columns.Add("PAY_POLICY_STATUS");
                dt.Columns.Add("PAY_GATEWAY_CODE");
                dt.Columns.Add("PAY_REF_NO");
                dt.Columns.Add("PAY_PAID_DATE");
                dt.Columns.Add("AUTO_DOC_FLAG");
                dt.Columns.Add("AUTO_RV_FLAG");
                dt.Columns.Add("PAYIN_DATE");
                dt.Columns.Add("TRANS_CODE");
                DataRow dr = dt.NewRow();
                dr["ORDER_NO"] = "MI012018020700001";
                dr["CA_NO"] = "can_no";
                dr["CA_GROUP"] = "";
                dr["CHANNEL"] = "ODGT";
                dr["APP_DATE"] = "07/02/2018";
                dr["CARD_TYPE"] = "01";
                dr["CUST_ID_CARD"] = "1234567890123";
                dr["CUST_TITLE"] = "คุณ";
                dr["CUST_FNAME"] = "เจน";
                dr["CUST_LNAME"] = "จิรา";
                dr["CUST_BIRTHDAY"] = "31/12/1990";
                dr["CUST_PHONE"] = "";
                dr["CUST_MOBILE"] = "08123456789";
                dr["CUST_EMAIL"] = "jane@jira.com";
                dr["CUST_FACEBOOK"] = "";
                dr["CUST_INSTAGRAM"] = "";
                dr["CUST_LINEID"] = "";
                dr["CUST_OCCUPATION"] = "Programmer";
                dr["CUST_TYPE"] = "P";
                dr["DEPARTMENT_NAME"] = "MKT_Team";
                dr["OWNER_NAME"] = "OWNER";
                dr["CUST_AGE"] = "28";
                dr["BENEFIT_NM"] = "จัน จิรา";
                dr["RELATION_BENEFIT"] = "พี่";
                dr["INS_CODE"] = "TNI";
                dr["INS_NM"] = "ธนชาต ประกันถัย";
                dr["OUTLET"] = "OUTLET";
                dr["HUB"] = "HUB";
                dr["AGENT_CODE"] = "CODE";
                dr["AGENT_NAME"] = "";
                dr["GROUPHEAD"] = "";
                dr["POLICY_TYPE"] = "New";
                dr["INDIVIDUAL_TYPE"] = "";
                dr["INS_CUST_NUM"] = 1;
                dr["INS_DETAIL"] = "";
                dr["COUNTRY"] = "";
                dr["MIN_AGE"] = "";
                dr["MAX_AGE"] = "";
                dr["PLAN"] = "ABC";
                dr["CLASS"] = "MI";
                dr["SUBCLASS"] = "HHI";
                dr["POLICY_NO_OLD"] = "";
                dr["EFF_DATE"] = "01/02/2018";
                dr["EXP_DATE"] = "01/02/2019";
                dr["SUMINSURE"] = 1000000;
                dr["PREMIUM"] = 100;
                dr["DUTY"] = 20;
                dr["TAX"] = 3;
                dr["NETPREMIUM"] = 123;
                dr["PAY_CODE"] = "C22";
                dr["INS_NM_PAY"] = "";
                dr["SEND_CODE"] = "B10";
                dr["CIF"] = "";
                dr["AO_CODE"] = "";
                dr["OFF_NAME"] = "";
                dr["DEP_CODE"] = "";
                dr["DEPT_NM"] = "";
                dr["MATCH_CHANNEL"] = "";
                dr["RM"] = "";
                dr["PACKAGE"] = "ABC";
                dr["RISK_EXP_CODE"] = "";
                dr["BUILD_CLASS_CODE"] = "";
                dr["LOCATION_ADDR"] = "";
                dr["LOCATION_DISTRICT"] = "";
                dr["LOCATION_AMPHUR"] = "";
                dr["LOCATION_PROVINCE"] = "";
                dr["LOCATION_ZIPCODE"] = "";
                dr["RATE"] = "";
                dr["FIRE_DISCRATE"] = "";
                dr["CODEPAY_DESCRIPTION"] = "";
                dr["SHIPPING_ADDR"] = "";
                dr["LOAN_NO"] = "";
                dr["IB_FLAG"] = "";
                dr["POLICY_NO"] = "";
                dr["ADD_INFO_CODE1"] = "";
                dr["ADD_INFO_VALUE1"] = "";
                dr["ADD_INFO_CODE2"] = "";
                dr["ADD_INFO_VALUE2"] = "";
                dr["ADD_INFO_CODE3"] = "";
                dr["ADD_INFO_VALUE3"] = "";
                dr["ADD_INFO_CODE4"] = "";
                dr["ADD_INFO_VALUE4"] = "";
                dr["ADD_INFO_CODE5"] = "";
                dr["ADD_INFO_VALUE5"] = "";
                dr["ADD_IDCARD_NO"] = "";
                dr["ADD_IDCARD_VILLAGE"] = "";
                dr["ADD_IDCARD_NO_MU"] = "";
                dr["ADD_IDCARD_ALLEY"] = "";
                dr["ADD_IDCARD_ROAD"] = "";
                dr["ADD_IDCARD_DISTRICT"] = "";
                dr["ADD_IDCARD_COUNTY"] = "";
                dr["ADD_IDCARD_PROVINCE"] = "";
                dr["ADD_IDCARD_ZIPCODE"] = "";
                dr["ADD_CURRENT_NO_ADD"] = "70/122";
                dr["ADD_CURRENT_VILLAGE"] = "";
                dr["ADD_CURRENT_NO_MU"] = "";
                dr["ADD_CURRENT_ALLEY"] = "";
                dr["ADD_CURRENT_ROAD"] = "";
                dr["ADD_CURRENT_DISTRICT"] = "คันนายาว";
                dr["ADD_CURRENT_COUNTY"] = "คันนายาว";
                dr["ADD_CURRENT_PROVINCE"] = "กรุงเทพมหานคร";
                dr["ADD_CURRENT_ZIPCODE"] = "10230";
                dr["ADD_INVOICE_NAME"] = "";
                dr["ADD_INVOICE_NO_ADD"] = "";
                dr["ADD_INVOICE_VILLAGE"] = "";
                dr["ADD_INVOICE_NO_MU"] = "";
                dr["ADD_INVOICE_ALLEY"] = "";
                dr["ADD_INVOICE_ROAD"] = "";
                dr["ADD_INVOICE_DISTRICT"] = "";
                dr["ADD_INVOICE_AMPHUR"] = "";
                dr["ADD_INVOICE_PROVINCE"] = "";
                dr["ADD_INVOICE_ZIPCODE"] = "";
                dr["ADD_POLICY_NO"] = "";
                dr["ADD_POLICY_VILLAGE"] = "";
                dr["ADD_POLICY_NO_MU"] = "";
                dr["ADD_POLICY_ALLEY"] = "";
                dr["ADD_POLICY_ROAD"] = "";
                dr["ADD_POLICY_DISTRICT"] = "";
                dr["ADD_POLICY_COUNTY"] = "";
                dr["ADD_POLICY_PROVINCE"] = "";
                dr["ADD_POLICY_ZIPCODE"] = "";
                dr["TAX_IS_INSURED"] = "N";
                dr["TAX_PERSON_TYPE"] = "";
                dr["TAX_OCCUPATION"] = "";
                dr["TAX_PERSON_CARD_TYPE"] = "";
                dr["TAX_PERSON_CARD_ID"] = "";
                dr["TAX_ID"] = "";
                dr["PAY_CREDIT_TERM_TYPE"] = "ปกติ";
                dr["PAY_INSTALLMENT"] = 0;
                dr["PAY_TYPE"] = "3";
                dr["PAY_BANK_CODE"] = "KBANK";
                dr["PAY_STATUS"] = "Y";
                dr["PAY_MAILING_TYPE"] = "1";
                dr["PAY_POLI_NO"] = "1";
                dr["PAY_POLICY_STATUS"] = "1";
                dr["PAY_GATEWAY_CODE"] = "2C2P";
                dr["PAY_REF_NO"] = "REF_NO";
                dr["PAY_PAID_DATE"] = "07/02/2018";
                dr["AUTO_DOC_FLAG"] = "N";
                dr["AUTO_RV_FLAG"] = "Y";
                dr["PAYIN_DATE"] = "07/02/2018";
                dr["TRANS_CODE"] = "BLL";
                dt.Rows.Add(dr);
                //DebugDataSet(ds);

                TBrokerAPI.ServiceDigitalSoapClient api = new TBrokerAPI.ServiceDigitalSoapClient();
                DebugDataSet(api.SubmitPolicyMI(ds));
            }
        }

        private void GetReceivedPolicy() {
            TBrokerAPI.ServiceDigitalSoapClient api = new TBrokerAPI.ServiceDigitalSoapClient();
            DebugDataSet(api.GetReceivedPolicy(DateTime.Today, new string[] { "OGDT" }, new string[] { "TEAM" }));
        }
        private void DebugDataSet(DataSet ds) {
            if (ds == null) return;
            foreach (System.Data.DataTable dt in ds.Tables) {
                context.Response.Write("<table border=1>");
                context.Response.Write("<thead><tr><th>Row</th>");
                foreach (System.Data.DataColumn dc in dt.Columns) {
                    context.Response.Write("<th>" + dc.ColumnName + "</th>");
                }
                context.Response.Write("</tr></thead>");
                context.Response.Write("<tbody>");
                int rowNum = 0;
                foreach (System.Data.DataRow dr in dt.Rows) {
                    context.Response.Write("<tr><td>" + rowNum + "</td>");
                    int colNumber = 0;
                    foreach (System.Data.DataColumn dc in dt.Columns) {
                        context.Response.Write("<td>" + dr[dc].ToString() + "</td>");
                    }
                    context.Response.Write("</tr>");
                    rowNum++;
                }
                context.Response.Write("</tbody></table><br/><br/>");
            }
        }

        public bool IsReusable {
            get
            {
                return true;
            }
        }
    }
}