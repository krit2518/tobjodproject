﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Service;
using TobJod.Utils;
using static TobJod.Service.TBrokerAPIService;

namespace TobJod.Batch {
    class Program {
        private const string HR = "------------------------------------------------------------";
        private static System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo("en-GB");

        static void Main(string[] args) {
            var arg = "";
            if (args.Length > 1) arg = args[1];
            switch (args[0]) {
                case "PartnerProductMail":
                    Console.WriteLine(PartnerProductMail(arg));
                    break;
                case "TBrokerRetry":
                    Console.WriteLine(TBrokerRetry());
                    break;
                case "TBrokerGetPolicyNo":
                    Console.WriteLine(TBrokerGetPolicyNo());
                    break;
            }
        }

        private static string PartnerProductMail(string arg) {           
            StringBuilder sb = new StringBuilder();
            DateTime batchFrom = new DateTime(1990, 1, 1);
            DateTime batchDate = DateTime.Now;

            sb.AppendLine("PartnerProductMail: Start @ " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
            //DateTime d;
            //if (!string.IsNullOrEmpty(arg) && DateTime.TryParseExact(arg, "yyyyMMdd", ci, System.Globalization.DateTimeStyles.None, out d)) {
            //    batchDate = d;
            //}

            CompanyRepository companyRepo = new CompanyRepository();
            var companies = companyRepo.ListCompany_Active();

            OrderRepository repo = new OrderRepository();
            ActionResultStatus status = new ActionResultStatus();

            foreach (var company in companies) {
                int category = 0;
                if (company.APIChannel == CompanyAPIChannel.Email) {
                    List<Order> list = new List<Order>();
                    List<Order> listPending = repo.ListOrderByPartnerAPIStatus(batchFrom, batchDate, company.Id, category, PartnerAPIStatus.Pending);
                    List<Order> listFail = repo.ListOrderByPartnerAPIStatus(batchFrom, batchDate, company.Id, category, PartnerAPIStatus.Fail);
                    if (listPending != null) list.AddRange(listPending);
                    if (listFail != null) list.AddRange(listFail);
                    var listId = list.Select(x => x.Id).ToList();
                    listId = listId.Distinct().ToList();

                    sb.AppendLine("     Company: " + company.Code + ", (" + company.MailTo + ") " + listId.Count + " records.");
                    sb.AppendLine("     Order Id: [" + string.Join(", ", listId) + "]");

                    try {
                        var result = new PartnerAPIService().GenerateExcel(listId, true);
                        status = result.Item1;

                        if (status.Success) {
                            foreach (var excel in result.Item2) {
                                if (excel.Value != null) {
                                    try {
                                        var email = company.MailTo;

                                        string body = "New Product Application";

                                        System.IO.Stream stream = new System.IO.MemoryStream(excel.Value);
                                        System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(stream, "application_" + excel.Key.ToString() + ".xlsx");
                                        status = MailService.SendMailAttachment(System.Configuration.ConfigurationManager.AppSettings["Mail_From_Email"], System.Configuration.ConfigurationManager.AppSettings["Mail_From_Name"], email, "Product Application", body, "", "", new List<System.Net.Mail.Attachment>() { attachment });

                                        //sb.AppendLine("PartnerProductMail: " + company.Code + ", " + company.MailTo + " " + (status.Success ? "Success" : "Fail: " + status.ErrorMessage));
                                        sb.AppendLine("     Category " + excel.Key);
                                        sb.AppendLine("     Status: " + (status.Success ? "Success" : "Fail: " + status.ErrorMessage));

                                        //status.Success = true;
                                    } catch (Exception ex) {
                                        sb.AppendLine("     Status: Error, " + ex.ToString());
                                        status.Success = false;
                                    }
                                }
                            }

                            if (status.Success) {
                                repo.UpdateOrderStatus_PartnerAPI(listId, PartnerAPIStatus.Success, "");
                            } else {
                                repo.UpdateOrderStatus_PartnerAPI(listId, PartnerAPIStatus.Fail, "");
                            }
                        } else {
                            sb.AppendLine("     Status: Error, Excel Error, " + status.ErrorMessage);
                        }
                    } catch (Exception ex) {
                        sb.AppendLine("     Status: Error, " + ex.ToString());
                        status.Success = false;
                    }
                }
            }
            //sb.AppendLine("     " + (status.Success ? "Success" : "Fail"));
            sb.AppendLine("PartnerProductMail: End @ " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));


            FileUtils.SaveTextAppend(sb.ToString(), System.Configuration.ConfigurationManager.AppSettings["WebService_Log_Path"] + DateTime.Now.ToString("yyyyMMdd", ci) + "_Batch_PartnerProductMail.log");

            return sb.ToString();
        }

        private static string TBrokerRetry() {
            StringBuilder sb = new StringBuilder();
            DateTime batchFrom = new DateTime(1990, 1, 1);
            DateTime batchDate = DateTime.Now;

            DateTime now = DateTime.Now;

            sb.AppendLine("TBrokerRetry: Start @ " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));

            OrderRepository repo = new OrderRepository();
            List<Order> list = new List<Order>();
            List<Order> listPending;
            List<Order> listFail;
            List<int> listId;
            ProductCategoryKey[] categories = new ProductCategoryKey[] { ProductCategoryKey.Motor, ProductCategoryKey.PRB, ProductCategoryKey.Home, ProductCategoryKey.PA, ProductCategoryKey.PH, ProductCategoryKey.TA };

            foreach (var category in categories) {
                list = new List<Order>();
                listPending = new List<Order>();
                listFail = new List<Order>();
                listId = new List<int>();

                listPending = repo.ListOrderByBackendAPIStatus(batchFrom, batchDate, (int)category, BackendAPIStatus.Pending);
                listFail = repo.ListOrderByBackendAPIStatus(batchFrom, batchDate, (int)category, BackendAPIStatus.Fail);
                if (listPending != null) list.AddRange(listPending);
                if (listFail != null) list.AddRange(listFail);
                listId = list.Select(x => x.Id).ToList();
                listId = listId.Distinct().ToList();
                var api = new TBrokerAPIService();

                sb.AppendLine("-- [" + category.ToString() + "] Start @" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " -- ");

                int success = 0; int fail = 0;
                foreach (var id in listId) {
                    var result = api.SubmitPolicy(new List<int>() { id });
                    var code = result.Keys.First();
                    var item = result[code];
                    if (item.ErrorMessage == null) item.ErrorMessage = "";
                    repo.UpdateOrderStatus_BackendAPI(code, (int)(item.Success ? BackendAPIStatus.Success : BackendAPIStatus.Fail), item.ErrorMessage);

                    Order order = new Order();
                    order.Id = id;
                    order.BackendAPIStatus = (item.Success ? BackendAPIStatus.Success : BackendAPIStatus.Fail);
                    order.UpdateDate = DateTime.Now;
                    if (!string.IsNullOrEmpty(item.Data)) order.CANo = item.Data;
                    repo.UpdateOrderStatus_BackendAPI(order, NullUtils.cvString(item.ErrorMessage));

                    if (item.Success) {
                        success += 1;
                    } else {
                        fail += 1;
                    }
                    //sb.AppendLine("---- Order : (" + item.Key + ") Success:" + item.Value.Success.ToString() + " -- ");
                }
                sb.AppendLine("-- [" + category.ToString() + "] Success: " + success + " records. -- ");
                sb.AppendLine("-- [" + category.ToString() + "] Fail: " + fail + " records. -- ");

                sb.AppendLine("-- [" + category.ToString() + "] End @" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " -- ");
            }
            
            sb.AppendLine("TBrokerRetry: End @ " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));

            FileUtils.SaveTextAppend(sb.ToString(), System.Configuration.ConfigurationManager.AppSettings["WebService_Log_Path"] + DateTime.Now.ToString("yyyyMMdd", ci) + "_Batch_TBrokerRetry.log");

            return sb.ToString();
        }

        private static string TBrokerGetPolicyNo() {
            StringBuilder sb = new StringBuilder();
            DateTime batchDate = DateTime.Today;

            DateTime now = DateTime.Now;

            sb.AppendLine("TBrokerGetPolicyNo: Start @ " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));

            TBrokerAPI.ServiceDigitalSoapClient client = new TBrokerAPI.ServiceDigitalSoapClient();
            var inspector = new LogEndpointBehavior(System.Configuration.ConfigurationManager.AppSettings["WebService_Log_Path"], "GetReceivedPolicy", DateTime.Now);
            client.Endpoint.EndpointBehaviors.Add(inspector);
            var data = client.GetReceivedPolicy(batchDate, new string[] { "01" }, new string[] { "" });

            if (data != null && data.Tables.Count > 0 && data.Tables[0].Rows.Count > 0) {
                List<Order> list = new List<Order>();
                List<Order> listCompulsory = new List<Order>();
                foreach (DataRow row in data.Tables[0].Rows) {
                    if (string.Equals(row["CLASS"], "MO") && string.Equals(row["SUBCLASS"], "C")) {
                        listCompulsory.Add(new Order() { OrderCode = row["ORDER_NO"].ToString(), PolicyNo = row["POLICY_NO"].ToString(), UpdateDate = now, PolicyReceiveDate = ValueUtils.GetDateTimeDefaultFormat(row["RECEIVED_POLDATE"].ToString()) });
                        //sb.AppendLine("--- [" + listCompulsory.Last().OrderCode + "] " + listCompulsory.Last().PolicyNo);
                    } else {
                        list.Add(new Order() { OrderCode = row["ORDER_NO"].ToString(), PolicyNo = row["POLICY_NO"].ToString(), UpdateDate = now, PolicyReceiveDate = ValueUtils.GetDateTimeDefaultFormat(row["RECEIVED_POLDATE"].ToString()) });
                        //sb.AppendLine("--- [" + listCompulsory.Last().OrderCode + "] " + listCompulsory.Last().PolicyNo);
                    }
                }

                OrderRepository repo = new OrderRepository();
                sb.AppendLine("--- UPDATE M  " + repo.UpdateOrder_PolicyNo(list) + " records.");

                int countC = 0; int countMC = 0;
                if (listCompulsory.Count > 0) {
                    foreach (Order order in listCompulsory) {
                        int category = repo.GetOrderProductCategoryByOrderCode_Admin(order.OrderCode);
                        //if (category == (int)ProductCategoryKey.PRB) {
                        //    countC += repo.UpdateOrder_PolicyNo(new List<Order>() { order });
                        //} else {
                            order.PolicyNoAdd = order.PolicyNo;
                            countMC += repo.UpdateOrder_PolicyNoAdd(new List<Order>() { order });
                        //}
                    }
                    sb.AppendLine("--- UPDATE C   " + countC + " records.");
                    sb.AppendLine("--- UPDATE M+C " + countMC + " records.");
                }
            } else {
                sb.Append("No Data");
            }

            sb.AppendLine("TBrokerGetPolicyNo: End @ " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));

            FileUtils.SaveTextAppend(sb.ToString(), System.Configuration.ConfigurationManager.AppSettings["WebService_Log_Path"] + DateTime.Now.ToString("yyyyMMdd", ci) + "_Batch_TBrokerGetPolicyNo.log");

            return sb.ToString();
        }
    }
}
