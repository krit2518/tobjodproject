﻿var url_path = '';
$(function () {
    $('#px-site-nav').pxNav();

    SetupDataTable();
    SetupModal();
    SetupNotification();
    SetupValidator();
});
function SetupDataTable() {
    if ($.fn.dataTable) {
        $.extend(true, $.fn.dataTable.defaults, {
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: {
                error: function (xhr, error, thrown) {
                    if (error == 'parsererror') {
                        window.location = url_authen;
                    }
                }
            }
        });
    }
    $('#dataTable').confirmation({
        rootSelector: '#dataTable',
        container: 'body',
        selector: '[data-toggle=confirmation]',
        btnOkClass: 'btn-xs btn-danger',
        btnCancelClass: 'btn-xs btn-default',
        btnOkIcon: 'fa fa-check',
        btnCancelIcon: 'fa fa-times',
        singleton: true
    });
}
function SetupModal() {
    $('.modal').on('shown.bs.modal', function () {
        $('[autofocus]', this).focus();
    });
    $('.modal').on('hidden.bs.modal', function () {
        $(this).removeData('bs.modal');
    });
}

function SetupNotification() {
    //$('#navbar-messages').perfectScrollbar();
}

function SetupValidator() {
    $('.date-picker').on('keydown paste', function (e) { e.preventDefault(); return false; });
    //$(':text[data-val-required], select[data-val-required], textarea[data-val-required]').parent().addClass('required');
    //$(':checkbox[data-val-required], :radio[data-val-required]').parent().parent().addClass('required');
    $('[data-val-required]').each(function (i, e) {
        $(e).closest('.form-group').find('label.control-label').not('.not-required').addClass('required');
    });
    $('.required-radio').each(function (i, e) {
        $(e).closest('.form-group').find('label.control-label').addClass('required');
    });
}

function dateDiffInDays(a, b) {
    try {
        var _MS_PER_DAY = 1000 * 60 * 60 * 24;
        // Discard the time and time-zone information.
        var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
        var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

        return Math.floor((utc2 - utc1) / _MS_PER_DAY);
    } catch (e) {}
    return 0;
}

function checkUpdateImage(file, fileSize) {
    var filename = file.value;
    var reg = /^([a-zA-Z0-9(.) _-]+)$/;
    if (!reg.test(file.files[0].name)) {
        alert('File name should contain only a-z, 0-9 _ - . ()');
        file.value = '';
        return;
    }
    var ext = filename.substr(filename.lastIndexOf('.') + 1).toLowerCase();
    if (ext != "jpg" && ext != "jpeg" && ext != "png" && ext != "gif") {
        alert('File type not allowed (only jpg, png, gif).');
        file.value = '';
        return;
    }
    if (file.files[0].size > fileSize) {
        alert('Image file size is over limit.');
        file.value = '';
        return;
    }

    var $img = $(file).parent().find('.img-preview img');
    var reader = new FileReader();
    reader.onload = function (e) {
        $img.attr('src', e.target.result);
    }
    reader.readAsDataURL(file.files[0]);
}

function checkUpdateFile(file, fileSize) {
    var filename = file.value;
    var reg = /^([a-zA-Z0-9(.) _-]+)$/;
    if (!reg.test(file.files[0].name)) {
        alert('File name should contain only a-z, 0-9 _ - . ()');
        file.value = '';
        return;
    }
    if (file.files[0].size > fileSize) {
        alert('Image file size is over limit.');
        file.value = '';
        return;
    }
}
$(function () {
    if (typeof CKEDITOR != 'undefined') {
        $('form').on('reset', function (e) {
            if ($(CKEDITOR.instances).length) {
                for (var key in CKEDITOR.instances) {
                    var instance = CKEDITOR.instances[key];
                    if ($(instance.element.$).closest('form').attr('name') == $(e.target).attr('name')) {
                        instance.setData(instance.element.$.defaultValue);
                    }
                }
            }
        });
        
    }
});

$('body').on('reset', 'form', function (e) {
    $('.img-preview img').each(function (i, e) {
        if ($(e).attr('src').indexOf('data:image') == 0) $(e).attr('src', '');
    });
    $('input[type=file]', $(this)).each(function (i, e) {
        document.getElementById($(e).attr('id')).value = '';
    });
    resetValidation();
});
function resetValidation() {
    $('.field-validation-valid').html('');
}