﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Emedia.Entities.FileBrowser;
using Newtonsoft.Json;

public partial class assets_ckeditor_filebrowser_browser : System.Web.UI.Page {

	protected string CurrentFolder;
	protected FileBrowserType FileType;

	protected void Page_Load(object sender, EventArgs e) {
		CurrentFolder = Emedia.Common.Utilities.HttpUtils.GetPathRelative(System.Configuration.ConfigurationManager.AppSettings["Path_Upload_Editor"] + Request.QueryString["f"]);
		string filetype = Request.QueryString["type"];
		if (string.IsNullOrEmpty(filetype)) { filetype = FileBrowserType.all.ToString(); }
		FileType = Emedia.Common.Utilities.EnumUtils.StringToEnum<FileBrowserType>(filetype);
		string CurrentPath = Server.MapPath(CurrentFolder);
		BindList(CurrentPath);		
	}

	protected void BindList(string path) {
		FileController fc = new FileController();
		FolderController foc = new FolderController(path);
		FileBrowser fb = new FileBrowser();
		fb.Folders = ConvertFolderSeparator(foc.GetAllFolders(path));
		fb.CurrentFolder = path;
		fb.Files = fc.GetFileList(path, FileType);

		rptFolder.DataSource = fb.Folders;
		rptFolder.DataBind();
		rptFolder.Visible = (rptFolder.Items.Count > 0);
		rptFile.DataSource = fb.Files;
		rptFile.DataBind();
		rptFile.Visible = (rptFile.Items.Count > 0);
	}

	private IEnumerable<UserFolder> ConvertFolderSeparator(IEnumerable<UserFolder> folders) {
		foreach (UserFolder folder in folders) {
			folder.Path = folder.Path.Replace('\\', '|');
		}
		return folders;
	}

	protected string ToJson(object obj) {
		return JsonConvert.SerializeObject(obj);
	}
}