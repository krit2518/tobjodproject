﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="browser.aspx.cs" Inherits="assets_ckeditor_filebrowser_browser" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Browser</title>
	<link rel="stylesheet" type="text/css" href="../../jquery-ui/jquery-ui.min.css" />
	<link rel="stylesheet" type="text/css" href="qtfile/qtfile.css" />
</head>
<body>
	<form id="frm" runat="server">
	<div>
		<div id="qtfile" class="ui-helper-reset ui-widget qtfile" title="admin">
			<div class="file-preview-overlay"></div>
			<h3 class="ui-helper-reset ui-widget-header ui-corner-top qtfile-header">File Browser</h3>
			<div class="ui-widget-content">
				<div class="menu-bar">
					<span id="buttonCreateFolder" class="button folder-create" title="Create new folder under current folder"><span class="icon">&nbsp;</span>New</span> 
					<span id="buttonRenameFolder" class="button folder-rename" title="Rename current folder"><span class="icon">&nbsp;</span>Rename</span> 
					<span id="buttonDeleteFolder" class="button folder-delete" title="Delete current folder"><span class="icon">&nbsp;</span>Delete</span> 
					<span id="buttonUploadFile" class="button file-upload" title="Upload file to current folder"><span class="icon">&nbsp;</span>Upload</span> 
					<span id="buttonRefreshFileList" class="button file-refresh" title="Refresh files of current folder"><span class="icon">&nbsp;</span></span>
				</div>
				<div class="ui-helper-clearfix qtfile-content">
					<div class="folder-wraper">
						<div class="ui-state-default folder-header"><span>Folders</span><span id="buttonRefreshFolderList" class="button folder-refresh" title="Refresh files of current folder"><span class="icon">&nbsp;</span></span></div>
						<div id="folderListPanel" class="folder-panel">
<asp:Repeater ID="rptFolder" runat="server">
	<HeaderTemplate>
							<ul>
	</HeaderTemplate>
	<ItemTemplate>
								<li<%#(String.Equals(((Emedia.Entities.FileBrowser.UserFolder)Container.DataItem).Path, CurrentFolder) ? " class=\"current-folder\"" : "")%>><%#ToJson(((Emedia.Entities.FileBrowser.UserFolder)Container.DataItem).ToJsonObject())%></li>
	</ItemTemplate>
	<FooterTemplate>
							</ul>
	</FooterTemplate>
</asp:Repeater>
						</div>
					</div>
					<div class="file-wraper">
						<div class="ui-state-default ui-helper-clearfix file-header">
							<span class="ui-state-default file-name-header">Name</span><span class="ui-state-default file-size-header">Size</span>
							<span class="ui-state-default file-actions-header">Actions</span>
						</div>
						<div id="fileListPanel" class="ui-helper-clearfix ui-widget-content file-panel">
<asp:Repeater ID="rptFile" runat="server">
	<HeaderTemplate>
							<ul class="file-list">
	</HeaderTemplate>
	<ItemTemplate>
								<li><%#ToJson(((Emedia.Entities.FileBrowser.UserFileInfo)Container.DataItem).ToJsonObject())%></li>
	</ItemTemplate>
	<FooterTemplate>
							</ul>
	</FooterTemplate>
</asp:Repeater>
						</div>
					</div>
					
				</div>
			</div>
			<div class="ui-helper-clearfix ui-state-default ui-corner-bottom qtfile-footer">
				<span id="statusMessage" class="status-message">&nbsp;</span>
			</div>
					<div class="ui-dialog ui-widget-content ui-corner-top file-preview">
						<div class="ui-dialog-titlebar ui-state-default ui-corner-all ui-helper-clearfix"><span class="ui-dialog-title">File details</span> <span class="ui-dialog-titlebar-close ui-corner-all file-preview-close"><span class="ui-icon ui-icon-closethick">close</span> </span></div>
						<div class="ui-dialog-content ui-widget-content">
							<p><span class="file-preview-field-name">Name</span> <span class="file-preview-name">&nbsp;</span></p>
							<p><span class="file-preview-field-name">Size</span> <span class="file-preview-size">&nbsp;</span></p>
							<p><span class="file-preview-field-name">Content type</span> <span class="file-preview-content-type">&nbsp;</span></p>
							<p><span class="file-preview-field-name">Modified</span> <span class="file-preview-modified">&nbsp;</span></p>
							<p class="file-preview-image"><span class="file-preview-image-loading">&nbsp;</span></p>
						</div>
					</div>
		</div>
	</div>
	</form>
	<script type="text/javascript" src="../../jquery-1.12.4.min.js"></script>
	<script type="text/javascript" src="../../jquery-ui/jquery-ui.min.js"></script>
	<script type="text/javascript" src="qtfile/jquery.qtfile.min.js"></script>
	<script type="text/javascript">
		$(function () {
			$('#qtfile').qtfile({ basePath: '<%=Emedia.Global.BaseUrl%>assets/vendor/ckeditor/filebrowser/browser.ashx', initfolder: '<%=CurrentFolder%>', fileType: '<%=FileType%>', FileClick: function (path) {
				if (window.parent.CKEDITOR != undefined) {
					var dialog = window.parent.CKEDITOR.dialog.getCurrent();
					switch (dialog.getName()) {
						case 'image':
							dialog.setValueOf('info', 'txtUrl', path);
							dialog.selectPage('info');
							break;
						case 'link':
							dialog.setValueOf('info', 'url', path);
							dialog.selectPage('info');
							break;
						case 'flash':
							dialog.setValueOf('info', 'src', path);
							dialog.selectPage('info');
							break;
						default:
							break;
					}
				}
			}
			});
		});
		
	</script>
</body>
</html>
