﻿<%@ WebHandler Language="C#" Class="browser_ashx" %>

using System;
using System.Web;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using Emedia.Entities.FileBrowser;
using Newtonsoft.Json;

public class browser_ashx : IHttpHandler {
	
	public bool IsReusable {
		get {
			return false;
		}
	}

	private HttpContext Context;
	private string uploadPath, initPath;
	protected FileBrowserType FileType;
	
	public void ProcessRequest (HttpContext context) {
		Context = context;
		uploadPath = context.Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["Path_Upload_Editor"]);
		initPath = context.Server.MapPath(context.Request.Params["initfolder"]);
		FileType = FileBrowserType.image;
		try { FileType = Emedia.Common.Utilities.EnumUtils.StringToEnum<FileBrowserType>(context.Request.Params["fileType"]); } catch { }
		if (!initPath.StartsWith(uploadPath)) { initPath = uploadPath; }
		switch (context.Request.QueryString["action"]) {
			case "uploadimage": UploadImage(); break;
			case "filelist": FileList(); break;
			case "filemove": FileMove(); break;
			case "filedelete": FileDelete(); break;
			case "download": FileDownload(); break;
			case "fileupload": FileUpload(); break;
			case "folderlist": FolderList(); break;
			case "foldercreate": FolderCreate(); break;
			case "folderdelete": FolderDelete(); break;
		}
	}

	private void UploadImage() {
		string filename;
		string path = initPath;
		string pathRel = "";
		Emedia.Common.Utilities.FileUtils.CreateFolder(path);
		foreach (string file in Context.Request.Files) {
			HttpPostedFile post = Context.Request.Files[file];
			filename = Path.GetFileName(post.FileName);
			if (post.ContentLength == 0) { Context.Response.Write(""); }
			post.SaveAs(path + filename);
			Context.Response.Write("<script type=\"text/javascript\">window.parent.CKEDITOR.tools.callFunction(1, '" + pathRel + filename + "', '');</script>");
		}
		Context.Response.Write("");
	}

	private void FileList() {
		string path = Context.Request.QueryString["path"];
		FileBrowser fb = new FileBrowser();
		FileController fc = new FileController();
		IEnumerable<UserFileInfo> files = fc.GetFileList(initPath + path.Replace('|', '\\'), FileType);
		if (files == null) { Context.Response.Write(""); }
		List<object> objects = new List<object>();
		foreach (UserFileInfo file in files) {
			objects.Add(file.ToJsonObject());
		}
		Context.Response.Write(ToJson(objects));
	}

	private void FileMove() {
		string srcPath = Context.Request.QueryString["srcPath"];
		string destPath = Context.Request.QueryString["destPath"];
		FileActionResult rs = FileActionResult.Error;
		FileController fCtrl = new FileController();
		rs = fCtrl.Move(initPath + srcPath.Replace('|', '\\'), initPath + destPath.Replace('|', '\\'), false);
		Context.Response.Write(ToResult(rs));
	}

	private void FileDelete() {
		string path = Context.Request.QueryString["path"];
		FileActionResult rs = FileActionResult.Error;
		if (Emedia.Common.Utilities.FileUtils.DeleteFile(initPath + path.Replace('|', '\\'))) {
			rs = FileActionResult.Succeed;
		}
		Context.Response.Write(ToResult(rs));
	}

	private void FileDownload() {
		string path = Context.Request.QueryString["path"];
		FileController fc = new FileController();
		UserFile file = fc.Retrieve(initPath + "\\" + path.Replace('|', '\\'));
		if (file != null) {
			Context.Response.Clear();
			Context.Response.ContentType = file.ContentType; 
			Context.Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
			Context.Response.Flush();
			Context.Response.WriteFile(file.Path);
		} else {
			Context.Response.Write(ToResult(FileActionResult.NotFound));
		}
	}
	
	private void FileUpload() {
		string path = Context.Request.Form["path"];
		FileActionResult rs = FileActionResult.Error;
		FileController fc = new FileController();
		RootFolder rf = new RootFolder(initPath);
		HttpPostedFile file;
		foreach (string mf in Context.Request.Files) {
			file = Context.Request.Files[mf];
			if (FileType == FileBrowserType.image && !fc.GetMime(file.FileName).StartsWith("image")) {
				rs = FileActionResult.FileTypeNotAllowed;
			} else 
			if (file.ContentLength > int.Parse(System.Configuration.ConfigurationManager.AppSettings["Editor_Upload_Max_File_Size"])) {
				rs = FileActionResult.FileSizeLimitExceed;
			} else {
				string filePath = initPath + path.Replace('|', '\\') + "\\";
				string fileName = Path.GetFileName(file.FileName);
				rs = fc.Save(filePath, file, false);
				if (rs == FileActionResult.Succeed) {
					Context.Response.Write(ToJson(UserFileInfo.FromPostedFile(file).ToJsonObject()));
					return;
				}
			}
		}
		Context.Response.Write(ToResult(rs));
	}

	private void FolderList() {
		string path = Context.Request.QueryString["path"];
		FolderController foCtrl = new FolderController(initPath);
		FileController fCtrl = new FileController();

		List<Object> list = new List<object>();
		foreach (UserFolder folder in foCtrl.GetAllFolders(path)) {
			folder.Path = folder.Path.Replace('\\', '|');
			list.Add(folder.ToJsonObject());
		}
		Context.Response.Write(ToJson(list));
	}

	private void FolderCreate() {
		string path = Context.Request.QueryString["path"];
		FileActionResult rs = new FileActionResult();
		FolderController foCtrl = new FolderController(initPath);
		if (foCtrl.Create(initPath + path.Replace('|', '\\'))) {
			rs = FileActionResult.Succeed;
		} else {
			rs = FileActionResult.Error;
		}
		Context.Response.Write(ToResult(rs));
	}

	private void FolderMove() {
		string srcPath = Context.Request.QueryString["srcPath"];
		string destPath = Context.Request.QueryString["destPath"];
		FolderController foCtrl = new FolderController(initPath);
		FileActionResult rs = foCtrl.Move(initPath + srcPath.Replace('|', '\\') + "\\", initPath + destPath.Replace('|', '\\') + "\\");
		Context.Response.Write(ToResult(rs));
	}

	private void FolderDelete() {
		string path = Context.Request.QueryString["path"];
		FileActionResult rs = new FileActionResult();
		FolderController foCtrol = new FolderController(initPath);
		if (foCtrol.Delete(initPath + path.Replace('|', '\\'))) {
			rs = FileActionResult.Succeed;
		} else {
			rs = FileActionResult.Error;
		}
		Context.Response.Write(ToResult(rs));
	}

	private IEnumerable<UserFolder> ConvertFolderSeparator(IEnumerable<UserFolder> folders) {
		foreach (UserFolder folder in folders) {
			folder.Path = folder.Path.Replace('\\', '|');
		}
		return folders;
	}

	protected string ToJson(object obj) {
		return JsonConvert.SerializeObject(obj);
	}

	protected string ToResult(FileActionResult result) {
		UserResponse resp = new UserResponse();
		resp.succeed = (result == FileActionResult.Succeed);
		resp.status = result.ToString();
		return ToJson(resp);
	}
	
}