/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	// The toolbar groups arrangement, optimized for two toolbar rows.
	//config.toolbarGroups = [
	//	{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
	//	{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
	//	{ name: 'links' },
	//	{ name: 'insert' },
	//	{ name: 'forms' },
	//	{ name: 'tools' },
	//	{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
	//	{ name: 'others' },
	//	'/',
	//	{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
	//	{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
	//	{ name: 'styles' },
	//	{ name: 'colors' }
	//];
    // config.toolbar = 'Basic';
    /*
    config.toolbar_Basic = [
        { name: 'links', items: ['Link', 'Unlink'] },
        { name: 'insert', items: ['Image', 'Table'] },
        { name: 'basicstyles', items: ['Bold', 'Italic'] },
        { name: 'list', items: ['BulletedList'] },
        { name: 'paragraph', items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
        { name: 'tools', items: ['Maximize'] },
        { name: 'source', items: ['Source'] } //, 'Preview'
    ];
    */
    /*
    config.toolbar_Basic = [
            { name: 'document', items : [ 'Source','-','Save','NewPage','DocProps','Preview','Print','-','Templates' ] },
            { name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
            { name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
            { name: 'forms', items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 
                'HiddenField' ] },
            '/',
            { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
            { name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv',
            '-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
            { name: 'links', items : [ 'Link','Unlink','Anchor' ] },
            { name: 'insert', items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ] },
            '/',
            { name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
            { name: 'colors', items : [ 'TextColor','BGColor' ] },
            { name: 'tools', items : [ 'Maximize', 'ShowBlocks','-','About' ] }
    ];
    */
if( false ){ 
    config.toolbar_Basic = [
        { name: 'document', items: [ 'Source', ] },
        { name: 'clipboard', items: [ 'Undo', 'Redo' ] },
        { name: 'editing', items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
        { name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
        '/',
        { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat' ] },
        { name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
        { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
        { name: 'insert', items: [ 'Image', 'Table', 'HorizontalRule', 'PageBreak'] },
        '/',
        { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
        { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
        { name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
        { name: 'about', items: [ 'About' ] }
    ];
}
else {
    config.toolbar = [
        { name: 'document', items: [ 'Source', ] },
        { name: 'clipboard', items: [ 'Undo', 'Redo' ] },
        { name: 'editing', items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
        { name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
        '/',
        { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat' ] },
        { name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
        { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
        { name: 'insert', items: [ 'Image', 'Table', 'HorizontalRule', 'PageBreak'] },
        '/',
        { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
        { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
        { name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
        { name: 'about', items: [ 'About' ] }
    ];
}
    

	// Remove some buttons provided by the standard plugins, which are
	// not needed in the Standard(s) toolbar.
    config.toolbar.removeButtons = 'Anchor,SpecialChar,Underline,Strike,Cut,Copy,Cut,Paste,PasteFromWord,Scayt';

	// Set the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';

	// Simplify the dialog windows.
    config.removeDialogTabs = 'image:advanced;link:advanced';

    //config.allowedContent = 'img form input param pre flash br a td p span font em strong table tr th td style  script iframe u s li ul div[*]{*}(*)';
    config.allowedContent = true;


    config.extraPlugins = 'justify,preview';
    config.image_previewText = ' ';
    config.contentsCss = ['/assets/vendor/bootstrap.min.css'];
};


CKEDITOR.on('dialogDefinition', function (ev) {
    var dialogName = ev.data.name;
    var dialogDefinition = ev.data.definition;
    var minWidth = 700;
    var minHeight = 335;

    if (dialogName == 'image') {
        dialogDefinition.minWidth = minWidth;
        dialogDefinition.minHeight = minHeight;
        dialogDefinition.removeContents('advanced');

        var infoTabImg = dialogDefinition.getContents('info');
        infoTabImg.get('txtWidth').width = "80px";
        infoTabImg.get('txtHeight').width = "80px";
        infoTabImg.get('txtHSpace').hidden = true;
        infoTabImg.get('txtVSpace').hidden = true;
        infoTabImg.get('txtBorder').hidden = true;
        infoTabImg.get('browse').hidden = false;
        infoTabImg.get('browse').onClick = function () { ev.data.definition.dialog.selectPage('Upload'); };
        var uploadTabImg = dialogDefinition.getContents('Upload');
        uploadTabImg.hidden = false;
        uploadTabImg.add({
            type: 'html',
            id: 'htmlBrowse',
            html: '<div style="width: 100%; height: 320px;"><iframe src="../assets/vendor/ckeditor/filebrowser/browser.aspx?f=' + uploadurl + '&type=image" frameborder="0" name="iframeBrowse" id="iframeBrowse" style="width:100%; height: 320px;margin:0;padding:0;"></iframe></div>',
            minHeight: 300
        });
        uploadTabImg.remove('upload');
        uploadTabImg.remove('uploadButton');
        var linkTab = dialogDefinition.getContents('Link');
        linkTab.remove('browse');
    }
    if (dialogName == 'link') {
        dialogDefinition.minWidth = minWidth;
        dialogDefinition.minHeight = minHeight;
        dialogDefinition.removeContents('advanced');

        var infoTabLink = dialogDefinition.getContents('info');
        infoTabLink.get('browse').hidden = false;
        infoTabLink.get('browse').onClick = function () { ev.data.definition.dialog.selectPage('upload'); };
        var uploadTabLink = dialogDefinition.getContents('upload');
        uploadTabLink.hidden = false;
        uploadTabLink.add({
            type: 'html',
            id: 'htmlBrowse',
            html: '<div style="width: 100%; height: 320px;"><iframe src="../assets/vendor/ckeditor/filebrowser/browser.aspx?f=' + uploadurl + '&type=all" frameborder="0" name="iframeBrowse" id="iframeBrowse" style="width:100%; height: 320px;margin:0;padding:0;"></iframe></div>',
            minHeight: 300
        });
        uploadTabLink.remove('upload');
        uploadTabLink.remove('uploadButton');
    }
});

//CKEDITOR.on('instanceReady', function (ev) {

//    // Ends self closing tags the HTML4 way, like <br>.
//    ev.editor.dataProcessor.htmlFilter.addRules({
//        elements: {
//            $: function (element) {
//                // Output dimensions of images as width and height
//                if (element.name == 'img') {
//                    var style = element.attributes.style;

//                    if (style) {
//                        // Get the width from the style.
//                        var match = /(?:^|\s)width\s*:\s*(\d+(\%?))(px)?/i.exec(style),
//                            width = match && match[1];

//                        // Get the height from the style.
//                        match = /(?:^|\s)height\s*:\s*(\d+(\%?))(px)?/i.exec(style);
//                        var height = match && match[1];

//                        // Get the float from the style.
//                        match = /(?:^|\s)float\s*:\s*(\w+)/i.exec(style);
//                        var float = match && match[1];

//                        if (width) {
//                            element.attributes.style = element.attributes.style.replace(/(?:^|\s)width\s*:\s*(\d+(\%?))(px)?;?/i, '');
//                            element.attributes.width = width;
//                        }

//                        if (height) {
//                            element.attributes.style = element.attributes.style.replace(/(?:^|\s)height\s*:\s*(\d+(\%?))(px)?;?/i, '');
//                            element.attributes.height = height;
//                        }
//                        if (float) {
//                            element.attributes.style = element.attributes.style.replace(/(?:^|\s)float\s*:\s*(\w+)/i, '');
//                            element.attributes.align = float;
//                        }

//                    }
//                }

//                if (!element.attributes.style) delete element.attributes.style;

//                return element;
//            }
//        }
//    });
//});