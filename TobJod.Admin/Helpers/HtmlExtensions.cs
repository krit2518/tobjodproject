﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace TobJod.Admin {
    public static class HtmlExtensions {

        public static MvcHtmlString CheckboxLabel(this HtmlHelper helper, string name, bool isChecked, string labelText, object htmlAttributes) {
            var label = new TagBuilder("label");

            var builder = new TagBuilder("input");
            builder.MergeAttribute("id", name);
            builder.MergeAttribute("name", name);
            builder.MergeAttribute("type", "checkbox");
            if (isChecked) builder.MergeAttribute("checked", "checked");

            label.MergeAttributes(new RouteValueDictionary(htmlAttributes));
            label.InnerHtml = builder.ToString(TagRenderMode.SelfClosing) + labelText;

            // Render tag
            return MvcHtmlString.Create(label.ToString());
        }

        public static MvcHtmlString FileImageFor<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression) {
            return helper.FileFor(expression, null, true);
        }

        public static MvcHtmlString FileFor<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression) {
            return helper.FileFor(expression, null, false);
        }

        public static MvcHtmlString FileFor<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes, bool isImage = false) {
            var builder = new TagBuilder("input");

            var id = helper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(ExpressionHelper.GetExpressionText(expression));
            builder.GenerateId(id);
            builder.MergeAttribute("name", id);
            builder.MergeAttribute("type", "file");
            if (isImage) {
                builder.MergeAttribute("onchange", "checkUpdateImage(this, " + System.Configuration.ConfigurationManager.AppSettings["Editor_Upload_Max_File_Size"] + ")");
            } else {
                builder.MergeAttribute("onchange", "checkUpdateFile(this, " + System.Configuration.ConfigurationManager.AppSettings["Editor_Upload_Max_File_Size"] + ")");
            }
            if (isImage) builder.MergeAttribute("accept", "image/jpeg, image/png, image/gif");
            //builder.MergeAttribute("class", "custom-file-input form-control");

            builder.MergeAttributes(new RouteValueDictionary(htmlAttributes));

            // Render tag
            return MvcHtmlString.Create(builder.ToString(TagRenderMode.SelfClosing));
        }
        
        public static MvcHtmlString If(this MvcHtmlString value, bool evaluation, MvcHtmlString falseValue = default(MvcHtmlString)) {
            return evaluation ? value : falseValue;
        }
        
        //public static MvcHtmlString HasPermission(this MvcHtmlString value, VetApp.Models.MenuFunctionId menuFunction, MvcHtmlString falseValue = default(MvcHtmlString)) {
        //    VetApp.Helpers.UserPremission UserPermission = new VetApp.Helpers.UserPremission();
        //    return UserPermission.HasPermissions(menuFunction) ? value : falseValue;
        //}

        //public static MvcHtmlString RadioButtonListFor<TModel, TProperty>(
        //    this HtmlHelper<TModel> htmlHelper,
        //    System.Linq.Expressions.Expression<Func<TModel, TProperty>> expression,
        //    IEnumerable<SelectListItem> listOfValues,
        //    string selectedItemValue = "") {
        //    var metaData = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
        //    var sb = new StringBuilder();

        //    if (listOfValues != null) {
        //        if (string.IsNullOrEmpty(selectedItemValue) && ((listOfValues is System.Web.Mvc.SelectList) && !string.IsNullOrEmpty(NullUtils.cvString(((System.Web.Mvc.SelectList)listOfValues).SelectedValue)))) { selectedItemValue = NullUtils.cvString(((System.Web.Mvc.SelectList)listOfValues).SelectedValue); }
        //        foreach (SelectListItem item in listOfValues) {
        //            var id = string.Format("{0}_{1}", metaData.PropertyName, item.Value);
        //            //var id = string.Format("{1}", metaData.PropertyName, item.Value);

        //            object htmlAttrib = new { id = id };
        //            if (string.Equals(selectedItemValue, item.Value)) { htmlAttrib = new { id = id, @checked = "checked" }; }

        //            var label = htmlHelper.Label(id, HttpUtility.HtmlEncode(item.Text), new { @for = id });
        //            var radio = htmlHelper.RadioButtonFor(expression, item.Value, htmlAttrib).ToHtmlString();

        //            //sb.AppendFormat("<div class=\"RadioButton\">{0}{1}</div>", radio, label);
        //            sb.AppendFormat("{0}{1}", radio, label);
        //        }
        //    }

        //    return MvcHtmlString.Create(sb.ToString());
        //}


    }
}