﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace TobJod.Admin {
    public static class EnumExtensions {
        public static string GetDisplayName(this Enum enumValue) {
            var enumType = enumValue.GetType();
            var enumMemberName = Enum.GetName(enumType, enumValue);
            try {
                return enumType
                    .GetEnumMemberAttribute<DisplayAttribute>(enumMemberName)
                    ?.GetName() // Potentially localized
                    ?? enumMemberName;
            } catch { }
            return enumType.Name;
        }

        public static string DisplayNameOrDefault(this Enum value) =>
        value.GetEnumMemberAttribute<DisplayAttribute>()?.GetName();

        static TAttribute GetEnumMemberAttribute<TAttribute>(this Enum value) where TAttribute : Attribute =>
            value.GetType().GetEnumMemberAttribute<TAttribute>(value.ToString());

        static TAttribute GetEnumMemberAttribute<TAttribute>(this Type enumType, string enumMemberName) where TAttribute : Attribute =>
            enumType.GetMember(enumMemberName).Single().GetCustomAttribute<TAttribute>();



        public static Dictionary<int, string> EnumDictionary<T>() {
            if (!typeof(T).IsEnum)
                throw new ArgumentException("Type must be an enum");
            return Enum.GetValues(typeof(T))
                .Cast<T>()
                .ToDictionary(t => (int)(object)t, t => t.ToString());
        }

        public static Dictionary<int, string> EnumDictionaryDisplay<T>() {
            if (!typeof(T).IsEnum)
                throw new ArgumentException("Type must be an enum");
            return Enum.GetValues(typeof(T))
                .Cast<T>()
                .ToDictionary(t => (int)(object)t, t => t.ToString());
        }
    }

}