﻿using System.Web;
using System.Web.Optimization;

namespace TobJod.Admin {
    public class BundleConfig {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles) {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            //bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
            //          "~/Scripts/bootstrap.js",
            //          "~/Scripts/respond.js"));

            //bundles.Add(new StyleBundle("~/Content/css").Include(
            //          "~/Content/bootstrap.css",
            //          "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/assets/css").Include(
                      "~/assets/dist/css/bootstrap.min.css",
                      "~/assets/dist/css/pixeladmin.min.css",
                      "~/assets/dist/css/themes/silver.min.css",
                      "~/assets/dist/css/themes/tobjod.min.css",
                      "~/assets/css/global.css"
                ));

            bundles.Add(new StyleBundle("~/assets/vendor/font-awesome").Include(
                      "~/assets/vendor/font-awesome/css/font-awesome.min.css"
                ));

            bundles.Add(new ScriptBundle("~/assets/js").Include(
                      "~/assets/vendor/jquery-1.12.4.min.js",
                      "~/assets/dist/js/bootstrap.min.js",
                      "~/assets/dist/js/pixeladmin.js",
                      "~/assets/vendor/bootstrap-confirmation.min.js",
                      "~/assets/js/init.js"
                ));
            bundles.Add(new ScriptBundle("~/assets/js-ie8").Include(
                      "~/assets/js/polyfill.js",
                      "~/assets/vendor/es5.js",
                      //"~/assets/vendor/es5-shim.min.js",
                      //"~/assets/vendor/es5-sham.js",
                      //"~/assets/vendor/minifill.js",
                      "~/assets/vendor/jquery-1.12.4.min.js",
                      "~/assets/dist/js/bootstrap.min.js",
                      "~/assets/dist/js/pixeladmin-ie8.js",
                      "~/assets/vendor/bootstrap-confirmation.min.js",
                      "~/assets/js/init.js"
                ));
            //bundles.Add(new ScriptBundle("~/assets/vendor/ckeditor").Include(
            //          "~/assets/vendor/ckeditor/ckeditor.js",
            //          "~/assets/vendor/ckeditor/adapters/jquery.js"
            //    ));
        }
    }
}
