﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class BaseController : Controller {
        protected TobJod.Models.Admin Admin { get; set; }
        protected AdminMenu Menu { get; set; }
        protected string Module { get; set; }
        protected string Action { get; set; }
        protected int PageSize { get; set; }
        public string PathPhy { get; set; }
        public string PathRel { get; set; }
        private string path;
        public string Path
        {
            get { return path; }
            set
            {
                path = value;
                PathPhy = HttpUtils.GetPathPhysical(Path);
                PathRel = HttpUtils.GetPathRelative(Path);
                ViewBag.PathRel = PathRel;
            }
        }

        protected string FORMAT_MONEY = "#,###,##0.00";
        protected CultureInfo cultureGB;


        //protected Dictionary<ConfigName, string> Configs { get; set; }
        //private User currentUser;

        public BaseController() {
            PageSize = 10;
            cultureGB = new CultureInfo("en-GB");


            //ConfigRepository configRepo = new ConfigRepository();
            //Configs = configRepo.List();
        }

        protected override void Initialize(System.Web.Routing.RequestContext requestContext) {
            base.Initialize(requestContext);


        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext) {
            //TempData.Remove("Notify");
            var admin = GetAdmin();
            if (admin == null) return;
            Admin = admin.Item1;
            ViewBag.Admin = Admin;
            ViewBag.Menu = admin.Item2;

            Module = HttpContext.Request.RequestContext.RouteData.Values["controller"].ToString();
            Action = HttpContext.Request.RequestContext.RouteData.Values["action"].ToString();
            Menu = ((List<AdminMenu>)ViewBag.Menu).Where(x => x.Module == Module).FirstOrDefault();
            ViewBag.CurrentAction = Action;
            ViewBag.CurrentMenu = (Menu != null ? Menu : new AdminMenu());

            if (!string.Equals(Module, "Home")) {
                CheckPermission(GetAdminPermissionType(Action));
            }

        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext) {
            //base.OnActionExecuted(filterContext);
            if (ViewBag.Notify == null && TempData.ContainsKey("Notify")) {
                ViewBag.Notify = TempData["Notify"];
            }
        }

        protected override void OnException(ExceptionContext filterContext) {
            //base.OnException(filterContext);
            string body = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " " + Module + " : " + Action + " : " + filterContext.Exception.ToString();
            FileUtils.SaveText(body,
                string.Format(System.Configuration.ConfigurationManager.AppSettings["Error_Log_Path"], DateTime.Today.ToString("yyyyMMdd")));
            //Response.Redirect("~/home/error");
        }


        private Tuple<TobJod.Models.Admin, List<AdminMenu>> GetAdmin() {
            if (!HttpContext.User.Identity.IsAuthenticated) {
                Response.Redirect("~/Authen", true);
                return null;
            }
           // string username = HttpContext.User.Identity.Name;
            string username = "admin"; ///////TODO: Remove Dummy
            AdminRepository repo = new AdminRepository();
            TobJod.Models.Admin admin = repo.GetAdminByEmployeeId(username);
            var menu = repo.GetAdminMenu(admin.Id);
            return Tuple.Create(admin, menu);
        }

        protected void CheckPermission(AdminAction Action) {
            CheckPermission(Action, this.Module);
        }


        protected void CheckPermission(AdminAction Action, string Module) {
            bool hasPermission = HasPermission(Action, Module);
            if (!hasPermission) {
                Response.Redirect("~/");
            }
        }

        protected bool HasPermission(AdminAction Action, string Module) {
            AdminPermission permission = Admin.Permissions.Where(x => x.Module == Module).FirstOrDefault();
            if (permission == null) return false;
            switch (Action) {
                case AdminAction.View: return permission.Can_View;
                case AdminAction.Add: return permission.Can_Add;
                case AdminAction.Edit: return permission.Can_Edit;
                case AdminAction.Delete: return permission.Can_Delete;
                case AdminAction.Approve: return permission.Can_Approve;
                case AdminAction.Assign: return permission.Can_Assign;
                case AdminAction.Export: return permission.Can_Export;
                case AdminAction.Disable: return permission.Can_Delete;
            }
            return false;
        }

        private AdminAction GetAdminPermissionType(string Action) {
            switch (Action) {
                case "Index": return AdminAction.View;
                case "Create": return AdminAction.Add;
                case "Edit": return AdminAction.Edit;
                case "Delete": return AdminAction.Delete;
                case "Approve": return AdminAction.Approve;
                case "Assign": return AdminAction.Assign;
                case "Export": return AdminAction.Export;
                case "Disable": return AdminAction.Delete;
            }
            return AdminAction.View;
        }

        protected void Notify(NotifyMessageType messageType, string message) {
            Notify(messageType, "", message);
        }

        protected void Notify(NotifyMessageType messageType, string title, string message) {
            NotifyMessage notify = new NotifyMessage() { Title = title, Message = message, MessageType = messageType };
            TempData["Notify"] = notify;
            ViewBag.NotifyMessage = notify;
        }


        public void Notify_Add_Success() { Notify(NotifyMessageType.Success, "Record added", ""); }

        public void Notify_Add_Fail() { Notify(NotifyMessageType.Error, "Add error", "Cannot add record. Please try again later."); }

        public void Notify_Update_Success() { Notify(NotifyMessageType.Success, "Record updated", ""); }

        public void Notify_Update_Fail() { Notify(NotifyMessageType.Error, "Update error", "Cannot update record. Please try again later."); }

        public void Notify_Delete_Success() { Notify(NotifyMessageType.Success, "Record deleted", ""); }

        public void Notify_Delete_Fail() { Notify(NotifyMessageType.Error, "Delete error", "Cannot delete record. Please try again later."); }


        protected string HtmlImage(string path, string image) {
            if (string.IsNullOrEmpty(image)) return "";
            return "<img src=\"" + path + image + "\" class=\"img-responsive\" style=\"max-height: 100px;\" />";
        }

        protected string HtmlActionButton(AdminAction type, int id) {
            return HtmlActionButton(type, id, String.Empty);
        }

        protected string HtmlActionButton(AdminAction type, int id, string queryString, bool disable = false) {
            switch (type) {
                case AdminAction.Edit: return HtmlActionButtonBuilder(Menu.Can_Edit, Url.Action("Edit") + "?id=" + id.ToString() + queryString, "btn btn-xs btn-warning btn-action", "Edit", "fa fa-pencil", "", disable);
                case AdminAction.Delete: return HtmlActionButtonBuilder(Menu.Can_Delete, Url.Action("Delete") + "?id=" + id.ToString() + queryString, "btn btn-xs btn-danger btn-action", "Delete", "fa fa-trash", " data-toggle=\"confirmation\"", disable);
                case AdminAction.Approve: return HtmlActionButtonBuilder(Menu.Can_Approve, Url.Action("Approve") + "?id=" + id.ToString() + queryString, "btn btn-xs btn-success btn-action", "Approve", "fa fa-check-square", "", disable);
                case AdminAction.Assign:
                    return HtmlActionButtonBuilder(Menu.Can_Assign, Url.Action("Assign") + "?id=" + id.ToString() + queryString, "btn btn-xs btn-info btn-action", "Assign", "fa fa-user", "", disable);
                case AdminAction.View: return HtmlActionButtonBuilder(Menu.Can_View, Url.Action("View") + "?id=" + id.ToString() + queryString, "btn btn-xs btn-default btn-action", "View", "fa fa-eye", "", disable);
                case AdminAction.Export: return HtmlActionButtonBuilder(Menu.Can_Export, Url.Action("Export") + "?id=" + id.ToString() + queryString, "btn btn-xs btn-default btn-action", "Export", "fa fa-download", "", disable);
                case AdminAction.Disable: return HtmlActionButtonBuilder(Menu.Can_Edit, Url.Action("Disable") + "?id=" + id.ToString() + queryString, "btn btn-xs btn-danger btn-action", "Disable", "fa fa-eye-slash", " data-toggle=\"confirmation\"", disable);
                case AdminAction.Duplicate: return HtmlActionButtonBuilder(Menu.Can_Add, Url.Action("Duplicate") + "?id=" + id.ToString() + queryString, "btn btn-xs btn-warning btn-action", "Duplicate", "fa fa-copy", "", disable);
                default: return "";
            }
        }

        protected string HtmlActionButton(AdminAction type, string id) {
            return HtmlActionButton(type, id, String.Empty);
        }

        protected string HtmlActionButton(AdminAction type, string id, string queryString, bool disable = false) {
            switch (type) {
                case AdminAction.Edit: return HtmlActionButtonBuilder(Menu.Can_Edit, Url.Action("Edit") + "?id=" + id.ToString() + queryString, "btn btn-xs btn-warning btn-action", "Edit", "fa fa-pencil", "", disable);
                case AdminAction.Delete: return HtmlActionButtonBuilder(Menu.Can_Delete, Url.Action("Delete") + "?id=" + id.ToString() + queryString, "btn btn-xs btn-danger btn-action btn-delete", "Delete", "fa fa-trash", " data-toggle=\"confirmation\"", disable);
                case AdminAction.Approve: return HtmlActionButtonBuilder(Menu.Can_Approve, Url.Action("Approve") + "?id=" + id.ToString() + queryString, "btn btn-xs btn-success btn-action", "Approve", "fa fa-check-square", "", disable);
                case AdminAction.Assign:
                    return HtmlActionButtonBuilder(Menu.Can_Assign, Url.Action("Assign") + "?id=" + id.ToString() + queryString, "btn btn-xs btn-info btn-action", "Assign", "fa fa-user", "", disable);
                case AdminAction.Disable:
                    return HtmlActionButtonBuilder(Menu.Can_Delete, Url.Action("Disable") + "?id=" + id.ToString() + queryString, "btn btn-xs btn-danger btn-action", "Disable", "fa fa-eye-slash", " data-toggle=\"confirmation\"", disable);
                default: return "";
            }
        }

        protected string HtmlActionCustomButton(AdminAction type, string id, string action, string queryString = "", bool disable = false, string modalTarget = "") {
            switch (type) {
                case AdminAction.Edit: return HtmlActionButtonBuilder(Menu.Can_Edit, Url.Action(action) + "?id=" + id.ToString() + queryString, "btn btn-xs btn-warning btn-action", "Edit", "fa fa-pencil", (string.IsNullOrEmpty(modalTarget) ? "" : " data-toggle=\"modal\" data-target=\"" + modalTarget + "\" "), disable);
                case AdminAction.Delete: return HtmlActionButtonBuilder(Menu.Can_Delete, "#", "btn btn-xs btn-danger btn-action btn-delete", "Delete", "fa fa-trash", " data-toggle=\"confirmation\" data-url=\"" + Url.Action(action) + "?id=" + id.ToString() + queryString + "\"", disable);
                case AdminAction.Approve: return HtmlActionButtonBuilder(Menu.Can_Approve, Url.Action(action) + "?id=" + id.ToString() + queryString, "btn btn-xs btn-success btn-action", "Approve", "fa fa-check-square", "", disable);
                case AdminAction.Assign:
                    return HtmlActionButtonBuilder(Menu.Can_Assign, Url.Action(action) + "?id=" + id.ToString() + queryString, "btn btn-xs btn-info btn-action", "Assign", "fa fa-user", "", disable);
                case AdminAction.View: return HtmlActionButtonBuilder(Menu.Can_View, Url.Action(action) + "?id=" + id.ToString() + queryString, "btn btn-xs btn-default btn-action", "Edit", "fa fa-eye", (string.IsNullOrEmpty(modalTarget) ? "" : " data-toggle=\"modal\" data-target=\"" + modalTarget + "\" "), disable);
                case AdminAction.Disable:
                    return HtmlActionButtonBuilder(Menu.Can_Delete, Url.Action("Disable") + "?id=" + id.ToString() + queryString, "btn btn-xs btn-danger btn-action", "Disable", "fa fa-eye-slash", " data-toggle=\"confirmation\"", disable);
                default: return "";
            }
        }

        protected string HtmlActionButtonBuilder(bool hasPermission, string link, string buttonStyle, string title, string icon, string attribute, bool disable) {
            if (!hasPermission) return "";
            if (!disable) {
                return string.Format("<a href=\"{0}\" class=\"{1}\" title=\"{2}\"{3}><i class=\"{4}\"></i></a> ", link, buttonStyle, title, attribute, icon);
            } else {
                return string.Format("<span class=\"{1} btn-outline\" title=\"{2}\"{3} disabled=\"disabled\"><i class=\"{4}\"></i></span> ", link, buttonStyle, title, attribute, icon);
            }
        }

        protected DataTableParameter GetDataTableParameter() {
            DataTableParameter item = new DataTableParameter();
            item.draw = NullUtils.cvInt(Request.Unvalidated.Form["draw"]);
            item.start = NullUtils.cvInt(Request.Unvalidated.Form["start"], 0);
            item.length = NullUtils.cvInt(Request.Unvalidated.Form["length"], 10);
            item.sortColumn = NullUtils.cvInt(Request.Unvalidated.Form["order[0][column]"]); ;
            item.sortDirection = Request.Unvalidated.Form["order[0][dir]"];
            item.searchValue = Request.Unvalidated.Form["search[value]"];
            item.hasSearch = (!string.IsNullOrEmpty(item.searchValue));

            return item;
        }

        protected bool IsImage(HttpPostedFileBase file) {
            //if (file.ContentType.Contains("image")) {
            //    return true;
            //}
            string[] formats = new string[] { ".jpg", ".png", ".gif", ".jpeg" };
            return formats.Any(item => file.FileName.EndsWith(item, StringComparison.OrdinalIgnoreCase));
        }

        protected void ExportFile(string fileName, string sheetName, DataTable dt, string[] intColumn, string[] doubleColumn, string[] dateColumn, string[] dateTimeColumn = null) {
            using (ExcelPackage pck = new ExcelPackage()) {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add(sheetName);

                //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1
                ws.Cells["A1"].LoadFromDataTable(dt, true);

                //Format the header for column 1-3
                using (ExcelRange rng = ws.Cells[1, 1, 1, dt.Columns.Count]) {
                    rng.Style.Font.Bold = true;
                    rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                    rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));  //Set color to dark blue
                    rng.Style.Font.Color.SetColor(Color.White);
                }

                //Example how to Format Column 1 as numeric 
                if (intColumn != null) {
                    foreach (var item in intColumn) {
                        int c = dt.Columns[item].Ordinal + 1;
                        using (ExcelRange col = ws.Cells[2, c, 2 + dt.Rows.Count, c]) {
                            col.Style.Numberformat.Format = "#,##0";
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        }
                    }
                }
                if (doubleColumn != null) {
                    foreach (var item in doubleColumn) {
                        int c = dt.Columns[item].Ordinal + 1;
                        using (ExcelRange col = ws.Cells[2, c, 2 + dt.Rows.Count, c]) {
                            col.Style.Numberformat.Format = "#,##0.00";
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        }
                    }
                }
                if (dateColumn != null) {
                    foreach (var item in dateColumn) {
                        int c = dt.Columns[item].Ordinal + 1;
                        using (ExcelRange col = ws.Cells[2, c, 2 + dt.Rows.Count, c]) {
                            col.Style.Numberformat.Format = "dd/mm/yyyy";
                        }
                    }
                }
                if (dateTimeColumn != null) {
                    foreach (var item in dateTimeColumn) {
                        int c = dt.Columns[item].Ordinal + 1;
                        using (ExcelRange col = ws.Cells[2, c, 2 + dt.Rows.Count, c]) {
                            col.Style.Numberformat.Format = "dd/mm/yyyy HH:mm";
                        }
                    }
                }

                ws.Cells.AutoFitColumns();

                //Write it back to the client
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=" + fileName);
                Response.BinaryWrite(pck.GetAsByteArray());
            }
        }

        protected void ExportFileMultipleSheet(string fileName, string[] sheetNames, DataTable[] dts, string[] intColumn, string[] doubleColumn, string[] dateColumn, string[] dateTimeColumn = null) {
            using (ExcelPackage pck = new ExcelPackage()) {
                for (int i = 0; i < sheetNames.Length; i++) {
                    string sheetName = sheetNames[i];
                    DataTable dt = dts[i];
                    if (dt.Rows.Count == 0) continue;
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add(sheetName);

                    //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1
                    ws.Cells["A1"].LoadFromDataTable(dt, true);

                    //Format the header for column 1-3
                    using (ExcelRange rng = ws.Cells[1, 1, 1, dt.Columns.Count]) {
                        rng.Style.Font.Bold = true;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                        rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));  //Set color to dark blue
                        rng.Style.Font.Color.SetColor(Color.White);
                    }

                    //Example how to Format Column 1 as numeric 
                    if (intColumn != null) {
                        foreach (var item in intColumn) {
                            int c = dt.Columns[item].Ordinal + 1;
                            using (ExcelRange col = ws.Cells[2, c, 2 + dt.Rows.Count, c]) {
                                col.Style.Numberformat.Format = "#,##0";
                                col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            }
                        }
                    }
                    if (doubleColumn != null) {
                        foreach (var item in doubleColumn) {
                            int c = dt.Columns[item].Ordinal + 1;
                            using (ExcelRange col = ws.Cells[2, c, 2 + dt.Rows.Count, c]) {
                                col.Style.Numberformat.Format = "#,##0.00";
                                col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            }
                        }
                    }
                    if (dateColumn != null) {
                        foreach (var item in dateColumn) {
                            int c = dt.Columns[item].Ordinal + 1;
                            using (ExcelRange col = ws.Cells[2, c, 2 + dt.Rows.Count, c]) {
                                col.Style.Numberformat.Format = "dd/mm/yyyy";
                            }
                        }
                    }
                    if (dateTimeColumn != null) {
                        foreach (var item in dateTimeColumn) {
                            int c = dt.Columns[item].Ordinal + 1;
                            using (ExcelRange col = ws.Cells[2, c, 2 + dt.Rows.Count, c]) {
                                col.Style.Numberformat.Format = "dd/mm/yyyy HH:mm";
                            }
                        }
                    }

                    ws.Cells.AutoFitColumns();
                }

                //Write it back to the client
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=" + fileName);
                Response.BinaryWrite(pck.GetAsByteArray());
            }
        }

        protected string GetExcelColumnName(int columnNumber) {
            int dividend = columnNumber;
            string columnName = String.Empty;
            int modulo;

            while (dividend > 0) {
                modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                dividend = (int)((dividend - modulo) / 26);
            }

            return columnName;
        }

        protected string ReplaceTextEditorPath(string val) {
            return val.Replace(HttpUtils.GetPathRelative("~"), "{{url}}");
        }

        protected string ReplaceTextEditorPathDisplay(string val) {
            //return string.Format(val, HttpUtils.GetPathRelative("~"));
            return val.Replace("{{url}}", HttpUtils.GetPathRelative("~"));
        }

        protected bool ValidateStringLength(string val, int length, string label, ref StringBuilder sb) {
            if (val.Length > length) {
                sb.AppendLine(string.Format("- {0} length exceed {1} characters<br/>", label, length));
                return false;
            }
            return true;
        }

        protected int ValidateNumber(object val, string label, ref StringBuilder sb) {
            int v;
            if (!int.TryParse(NullUtils.cvString(val), out v)) {
                sb.AppendLine("- " + label + " is not number.<br/>");
            }
            return v;
        }

        protected string ValidateModelMessage(object model) {
            ModelState.Clear();
            var result = TryValidateModel(model);
            if (result) return string.Empty;
            var errors = ModelState
            .Where(x => x.Value.Errors.Count > 0)
            .Select(x => new { x.Key, x.Value.Errors })
            .ToArray();
            StringBuilder sb = new StringBuilder();
            foreach (var item in errors) {
                foreach (var itemE in item.Errors.Distinct()) {
                    sb.AppendLine("- " + (!itemE.ErrorMessage.StartsWith("The field") ? "The field " + item.Key + ": " : "") + itemE.ErrorMessage + "<br/>");
                }
            }
            return sb.ToString();
        }

        protected bool DataRowHasData(DataRow value) {
            return !(value == null || value.ItemArray.All(i => i is DBNull));
        }
        
    }
}