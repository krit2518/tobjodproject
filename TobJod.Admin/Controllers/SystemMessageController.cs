﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TobJod.Admin.Models;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class SystemMessageController : BaseController {
        SystemMessageRepository repo;

        public SystemMessageController() {
            repo = new SystemMessageRepository();
        }
        
        // GET: SystemMessage
        public ActionResult Index() {
            return View();
        }

        // GET: SystemMessage/Edit/5
        public ActionResult Edit(int id) {
            SystemMessage item = repo.GetSystemMessage_Admin(id);
            BindForm();
            return View("Form", item);
        }

        // POST: SystemMessage/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, SystemMessage item) {
            int result = 0;
            try {
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = Admin.Id; // = item.OwnerAdminId

                result = repo.UpdateSystemMessage(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            BindForm();
            return View("Form", item);
        }
        
        public ActionResult DataTable() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();
            
            if (param.hasSearch) {
                whereC.Add(" (Code LIKE @searchValue OR BodyTH LIKE @searchValue OR BodyEN LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "Id", "Category", "Code", "SubCode", "BodyTH", "UpdateDate" };
            DataTableData<SystemMessage> data = repo.ListSystemMessage_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Id.ToString(),
                        x.Category.GetDisplayName(),
                        x.Code.ToString(),
                        x.SubCode.ToString(),
                        x.BodyTH.ToString(),
                        x.UpdateDate.ToString("dd/MM/yyyy HH:mm"),
                        (
                            HtmlActionButton(AdminAction.Edit, x.Id)
                        )
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        private void BindForm() {
        }
    }
}
