﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TobJod.Admin.Models;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class OrderController : BaseController {
        OrderRepository repo;

        public OrderController() {
            repo = new OrderRepository();
        }

        // GET: Order
        public ActionResult Index() {
            int[] status = ValueUtils.GetIntSplit(Request.QueryString["Status"], new char[] { ',' }, true);
            if (status.Length == 0) status = new int[] { 0, 1, 2, 3, 4 }; 
            ViewBag.Status = status;
            OrderViewModel model = new OrderViewModel();

            //try { model.ProductSubCategoryId = (ProductSubCategoryKey)NullUtils.cvInt(Request.QueryString["ProductSubCategoryId"]); } catch { }
            DateTime dateFrom = ValueUtils.GetDate(Request.Unvalidated.QueryString["DateFrom"]);
            DateTime dateTo = ValueUtils.GetDate(Request.Unvalidated.QueryString["DateTo"]);
            if (dateFrom > DateUtils.SqlMinDate()) ViewBag.DateFrom = dateFrom.ToString("dd/MM/yyyy", cultureGB);
            if (dateTo > DateUtils.SqlMinDate()) ViewBag.DateTo = dateTo.ToString("dd/MM/yyyy", cultureGB);
            if (Request.QueryString.AllKeys.Contains("export")) {
                Export();
            }
            return View(model);
        }

        // GET: Order/Edit/5
        public ActionResult Edit(int id) {
            var item = repo.GetOrder_Admin(id);
            item.Category = ProductCategory.GetCategoryBySubCategory(item.ProductApplication.ProductSubCategoryKey);
            BindForm(item);
            OrderViewModel o = OrderViewModel.Map(item);
            return View("Form", o);
        }

        // POST: Order/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, OrderViewModel model)
        {
            var item = repo.GetOrder_Admin(id);
            item.Category = ProductCategory.GetCategoryBySubCategory(item.ProductApplication.ProductSubCategoryKey);

            try {
                if (item != null && item.ProductApplication != null)
                {
                    var application = item.ProductApplication;
                    var attachments = new List<ProductApplicationAttachment>();
                    if (item.Category == ProductCategoryKey.Motor || item.Category == ProductCategoryKey.PRB)
                    {
                        UploadAttachmentFile(application.Id, application.ProductId, model.IDCardFile, FileCategory.IDCard, application, attachments);
                        UploadAttachmentFile(application.Id, application.ProductId, model.CCTVFile, FileCategory.CCTV, application, attachments);
                        UploadAttachmentFile(application.Id, application.ProductId, model.CarRegistrationCopyFile, FileCategory.RegisteredCarCopy, application, attachments);
                        UploadAttachmentFile(application.Id, application.ProductId, model.File4, FileCategory.File4, application, attachments);
                        UploadAttachmentFile(application.Id, application.ProductId, model.File5, FileCategory.File5, application, attachments);
                        UploadAttachmentFile(application.Id, application.ProductId, model.File6, FileCategory.File6, application, attachments);
                    } else {
                        UploadAttachmentFile(application.Id, application.ProductId, model.File1, FileCategory.File1, application, attachments);
                        UploadAttachmentFile(application.Id, application.ProductId, model.File2, FileCategory.File2, application, attachments);
                        UploadAttachmentFile(application.Id, application.ProductId, model.File3, FileCategory.File3, application, attachments);
                        UploadAttachmentFile(application.Id, application.ProductId, model.File4, FileCategory.File4, application, attachments);
                        UploadAttachmentFile(application.Id, application.ProductId, model.File5, FileCategory.File5, application, attachments);
                        UploadAttachmentFile(application.Id, application.ProductId, model.File6, FileCategory.File6, application, attachments);
                    }

                    if (attachments.Count > 0)
                    {
                        ProductApplicationRepository app = new ProductApplicationRepository();
                        app.AddProductApplicationAttachments(attachments);
                        Notify_Update_Success();
                    }
                    return RedirectToAction("Index");
                }
            }
            catch { }
            Notify_Update_Fail();

            BindForm(item);
            return View("Form", item);
        }

        private void BindForm(Order item)
        {
            ProductRepository productRepository = new ProductRepository();
            PromotionRepository promotionRepository = new PromotionRepository();

            var app = item.ProductApplication;
            var promotionId = app.PromotionId;
            var promotion = promotionRepository.GetPromotion_Admin(promotionId);
            item.Promotion = promotion;

            if (item.Category == ProductCategoryKey.Motor)
            {
                BindMotorForm(item);
                BindDriverForm(item);
            }
            if (item.Category == ProductCategoryKey.PRB)
            {
                BindMotorForm(item);
                BindPRBForm(item);
            }
            if (item.Category == ProductCategoryKey.TA)
            {
                BindTAForm(item);
            }
            if(item.Category == ProductCategoryKey.Home)
            {
                BindHomeForm(item);
            }

            BindPolicyHolder(item);
            BindAddressForm(item);

        }

        private void BindAddressForm(Order item)
        {
            if (item.ProductApplication != null && item.ProductApplication != null)
            {
                var app = item.ProductApplication;
                ProvinceRepository provinceRepo = new ProvinceRepository();
                DistrictRepository districtRepo = new DistrictRepository();
                SubDistrictRepository subDistrictRepo = new SubDistrictRepository();

                //var provinces = provinceRepo.ListProvince_SelectList_Admin(Language.TH);
                //var districts = districtRepo.ListDistrict_SelectList_Admin(NullUtils.cvInt(app.AddressProvince), Language.TH);
                //var subDistricts = subDistrictRepo.ListSubDistrict_SelectList_Admin(NullUtils.cvInt(app.AddressDistrict), Language.TH);
                //var billing_districts = districtRepo.ListDistrict_SelectList_Admin(NullUtils.cvInt(app.BillingAddressProvince), Language.TH);
                //var billing_subDistricts = subDistrictRepo.ListSubDistrict_SelectList_Admin(NullUtils.cvInt(app.BillingAddressDistrict), Language.TH);
                //var shipping_districts = districtRepo.ListDistrict_SelectList_Admin(NullUtils.cvInt(app.ShippingAddressProvince), Language.TH);
                //var shipping_subDistricts = subDistrictRepo.ListSubDistrict_SelectList_Admin(NullUtils.cvInt(app.ShippingAddressDistrict), Language.TH);

                var province = provinceRepo.GetProvince_Admin(NullUtils.cvInt(app.AddressProvince));//  provinces.Where(o => o.Value == app.AddressProvince).FirstOrDefault();
                var district = districtRepo.GetDistrict_Admin(NullUtils.cvInt(app.AddressDistrict));// districts.Where(o => o.Value == app.AddressDistrict.Trim()).FirstOrDefault();
                var subDistrict = subDistrictRepo.GetSubDistrict_Admin(NullUtils.cvInt(app.AddressSubDistrict)); //  subDistricts.Where(o => o.Value.ToString() == app.AddressSubDistrict.ToString().Trim()).FirstOrDefault();

                var billing_province = provinceRepo.GetProvince_Admin(NullUtils.cvInt(app.BillingAddressProvince));//provinces.Where(o => o.Value == app.BillingAddressProvince).FirstOrDefault();
                var billing_district = districtRepo.GetDistrict_Admin(NullUtils.cvInt(app.BillingAddressDistrict));// billing_districts.Where(o => o.Value == app.BillingAddressDistrict.Trim()).FirstOrDefault();
                var billing_subDistrict = subDistrictRepo.GetSubDistrict_Admin(NullUtils.cvInt(app.BillingAddressSubDistrict)); //billing_subDistricts.Where(o => o.Value.ToString() == app.BillingAddressSubDistrict.ToString().Trim()).FirstOrDefault();

                var shipping_province = provinceRepo.GetProvince_Admin(NullUtils.cvInt(app.ShippingAddressProvince));//provinces.Where(o => o.Value == app.ShippingAddressProvince).FirstOrDefault();
                var shipping_district = districtRepo.GetDistrict_Admin(NullUtils.cvInt(app.ShippingAddressDistrict));// districts.Where(o => o.Value == app.ShippingAddressDistrict.Trim()).FirstOrDefault();
                var shipping_subDistrict = subDistrictRepo.GetSubDistrict_Admin(NullUtils.cvInt(app.ShippingAddressSubDistrict)); //subDistricts.Where(o => o.Value.ToString() == app.ShippingAddressSubDistrict.ToString().Trim()).FirstOrDefault();

                app.AddressProvince = province != null ? province.TitleTH : "";
                app.AddressDistrict = district != null ? district.TitleTH : "";
                app.AddressSubDistrict = subDistrict != null ? subDistrict.TitleTH : "";
                app.AddressPostalCode = subDistrict != null ? subDistrict.ZipCode : "";

                app.BillingAddressProvince = billing_province != null ? billing_province.TitleTH : "";
                app.BillingAddressDistrict = billing_district != null ? billing_district.TitleTH : "";
                app.BillingAddressSubDistrict = billing_subDistrict != null ? billing_subDistrict.TitleTH : "";
                app.BillingAddressPostalCode = billing_subDistrict != null ? billing_subDistrict.ZipCode : "";

                app.ShippingAddressProvince = shipping_province != null ? shipping_province.TitleTH : "";
                app.ShippingAddressDistrict = shipping_district != null ? shipping_district.TitleTH : "";
                app.ShippingAddressSubDistrict = shipping_subDistrict != null ? shipping_subDistrict.TitleTH : "";
                app.ShippingAddressPostalCode = shipping_subDistrict != null ? shipping_subDistrict.ZipCode : "";
            }
        }

        private void BindPRBForm(Order item)
        {
            if(item.ProductApplication != null && item.ProductApplicationMotor != null)
            {
                var motor = item.ProductApplicationMotor;
                item.ProductApplicationMotor = motor;
            }
        }

        private void BindHomeForm(Order item)
        {
            if (item.ProductApplication != null && item.ProductApplicationHome != null)
            {
                CountryRepository countryRepository = new CountryRepository();
                MasterOptionRepository masterOptionRepository = new MasterOptionRepository();
                ProvinceRepository provinceRepo = new ProvinceRepository();
                DistrictRepository districtRepo = new DistrictRepository();
                SubDistrictRepository subDistrictRepo = new SubDistrictRepository();

                var Countries = countryRepository.ListCountry_SelectList_Active(Language.TH);
                var Salutations = masterOptionRepository.ListMasterOption_SelectList_Active(Language.TH, MasterOptionCategory.Salutation);
                var Relationships = masterOptionRepository.ListMasterOption_SelectList_Active(Language.TH, MasterOptionCategory.Relationship);
                var LocationUsages = masterOptionRepository.ListMasterOption_SelectList_Active(Language.TH, MasterOptionCategory.LocationUsage);
                var PropertyTypes = masterOptionRepository.ListMasterOption_SelectList_Active(Language.TH, MasterOptionCategory.PropertyType);
                var ConstructionTypes = masterOptionRepository.ListMasterOption_SelectList_Active(Language.TH, MasterOptionCategory.ConstructionType);

                var home = item.ProductApplicationHome;
                var locationUsage = LocationUsages.Where(o => o.Value == home.LocationUsage.ToString()).FirstOrDefault();
                var property = PropertyTypes.Where(o => o.Value == home.PropertyType.ToString()).FirstOrDefault();
                var constructionType = ConstructionTypes.Where(o => o.Value == home.ConstructionType.ToString()).FirstOrDefault();

                home.LocationUsageText = locationUsage != null ? locationUsage.Text : string.Empty;
                home.PropertyTypeText = property != null ? property.Text : string.Empty;
                home.ConstructionTypeText = constructionType != null ? constructionType.Text : string.Empty;

    
                var provinces = provinceRepo.ListProvince_SelectList_Active(Language.TH);
                var districts = districtRepo.ListDistrict_SelectList_Active(NullUtils.cvInt(home.AddressProvince), Language.TH);
                var subDistricts = subDistrictRepo.ListSubDistrict_Active(NullUtils.cvInt(home.AddressDistrict), Language.TH);
              
                var province = provinces.Where(o => o.Value == home.AddressProvince).FirstOrDefault();
                var district = districts.Where(o => o.Value == home.AddressDistrict.Trim()).FirstOrDefault();
                var subDistrict = subDistricts.Where(o => o.Id.ToString() == home.AddressSubDistrict.ToString().Trim()).FirstOrDefault();

                home.AddressProvince = province != null ? province.Text : "";
                home.AddressDistrict = district != null ? district.Text : "";
                home.AddressSubDistrict = subDistrict != null ? subDistrict.TitleTH : "";

                item.ProductApplicationHome = home;
            }
        }

        private void BindTAForm(Order item)
        {
            if(item.ProductApplication != null && item.ProductApplicationTA != null)
            {
                CountryRepository countryRepository = new CountryRepository();
                MasterOptionRepository masterOptionRepository = new MasterOptionRepository();
                var Countries = countryRepository.ListCountry_SelectList_Active(Language.TH);
                var Salutations = masterOptionRepository.ListMasterOption_SelectList_Active(Language.TH, MasterOptionCategory.Salutation);
                var Relationships = masterOptionRepository.ListMasterOption_SelectList_Active(Language.TH, MasterOptionCategory.Relationship);
                var TravelMethods = masterOptionRepository.ListMasterOption_SelectList_Active(Language.TH, MasterOptionCategory.TravelMethod);


                var ta = item.ProductApplicationTA;
                var country = Countries.Where(o => o.Value == ta.Country.ToString()).FirstOrDefault();
                var arrival = Countries.Where(o => o.Value == ta.Arrival.ToString()).FirstOrDefault();
                var travelMethod = TravelMethods.Where(o => o.Value == ta.TravelBy.ToString()).FirstOrDefault();

                ta.CountryText = country != null ? country.Text : string.Empty;
                ta.ArrivalText = arrival != null ? arrival.Text : (ta.Arrival == Country.SchengenId) ? "Schengen" : string.Empty;
                ta.TravelMethod = travelMethod != null ? travelMethod.Text : string.Empty;

                item.ProductApplicationTA = ta;
            }
            
        }

        private void BindPolicyHolder(Order item)
        {
            if (item.ProductApplicationPolicyHolders != null && item.ProductApplicationPolicyHolders.Count > 0)
            {
                CountryRepository countryRepository = new CountryRepository();
                MasterOptionRepository masterOptionRepository = new MasterOptionRepository();
                var Countries = countryRepository.ListCountry_SelectList_Active(Language.TH);
                var Careers = masterOptionRepository.ListMasterOption_SelectList_Active(Language.TH, MasterOptionCategory.Career);
                var Salutations = masterOptionRepository.ListMasterOption_SelectList_Active(Language.TH, MasterOptionCategory.Salutation);
                var Relationships = masterOptionRepository.ListMasterOption_SelectList_Active(Language.TH, MasterOptionCategory.Relationship);
                foreach (var policyHolder in item.ProductApplicationPolicyHolders)
                {
                    var country = Countries.Where(o => o.Value == policyHolder.NationalId.ToString()).FirstOrDefault();
                    var career = Careers.Where(o => o.Value == policyHolder.CareerId.ToString()).FirstOrDefault();
                    var salutation = Salutations.Where(o => o.Value == policyHolder.TitleId.ToString()).FirstOrDefault();
                    var relationShip = Relationships.Where(o => o.Value == policyHolder.BeneficiaryRelationship.ToString().Trim()).FirstOrDefault();

                    if (career != null) { policyHolder.Career = career.Text; }
                    if (country != null) { policyHolder.Country = country.Text; }
                    if (salutation != null) { policyHolder.Title = salutation.Text; }
                    if (relationShip != null) { policyHolder.BeneficiaryRelationship = relationShip.Text; }
                }
            }
        }

        private void BindDriverForm(Order item)
        {
            CountryRepository countryRepository = new CountryRepository();
            MasterOptionRepository masterOptionRepository = new MasterOptionRepository();
            var Countries = countryRepository.ListCountry_SelectList_Active(Language.TH);
            var Salutations = masterOptionRepository.ListMasterOption_SelectList_Active(Language.TH, MasterOptionCategory.Salutation);
            if (item.ProductApplicationDrivers != null && item.ProductApplicationDrivers.Count > 0)
            {
                foreach(var driver in item.ProductApplicationDrivers)
                {
                    var nationality = Countries.Where(o => o.Value == driver.NationalId.ToString()).FirstOrDefault();
                    var salutation = Salutations.Where(o => o.Value == driver.TitleId.ToString()).FirstOrDefault();

                    if (nationality != null) { driver.Nationality = nationality.Text; }
                    if (salutation != null) { driver.Title = salutation.Text; }
                }
            }
        }

        private void BindMotorForm(Order item)
        {
            ProvinceRepository provinceRepo = new ProvinceRepository();
            MasterOptionRepository masterRepo = new MasterOptionRepository();
            CarRepository carRepo = new CarRepository();
            ProductApplicationMotor motor = item.ProductApplicationMotor;
          
            var carBrands = carRepo.ListCarBrand_SelectList(Language.TH);
            var carFamily = carRepo.ListCarFamilyByBrand_NameValue(Language.TH, NullUtils.cvInt(motor.CarBrand));
            var carModel = carRepo.ListCarModelByBrand_NameValue(Language.TH, NullUtils.cvInt(motor.CarBrand));
            var carProvinces = masterRepo.ListMasterOption_SelectList_Active(Language.TH, MasterOptionCategory.MotorProvince);

            var carBrandObj = carBrands.FirstOrDefault(brand => brand.Value == NullUtils.cvString(motor.CarBrand));
            var carFamilyObj = carFamily.FirstOrDefault(family => family.Value == NullUtils.cvString(motor.CarFamily));
            var carModelObj = carModel.FirstOrDefault(m => m.Value == NullUtils.cvString(motor.CarModelId));
            var carProvinceObj = carProvinces.FirstOrDefault(p => p.Value == NullUtils.cvString(motor.CarProvinceId));

            ViewBag.CarBrand = carBrandObj != null ? carBrandObj.Text : string.Empty;
            ViewBag.CarFamily = carFamilyObj != null ? carFamilyObj.Name : string.Empty;
            ViewBag.CarModel = carModelObj != null ? carModelObj.Name : string.Empty;
            ViewBag.CarProvince = carProvinceObj != null ? carProvinceObj.Text : string.Empty;
        }

        // POST: Order/Edit/5
        //[HttpPost]
        //public ActionResult Edit(int id, Order item) {
        //    int result = 0;
        //    try {
        //        item.UpdateDate = DateTime.Now;

        //        result = repo.UpdateMasterOption(item);
        //        if (result > 0) {
        //            Notify_Update_Success();
        //            return RedirectToAction("Index", "MasterOption", new { category = (int)item.Category });
        //        }
        //    } catch { }
        //    Notify_Update_Fail();
        //    return View("Form", item);
        //}

        // GET: Order/Cancel/5
        public ActionResult Cancel(int id) {
            var result = repo.UpdateOrderStatus(id, (int)OrderStatus.Cancel, Admin.Id);
            if (result > 0) {
                Notify_Update_Success();
            } else {
                Notify_Update_Fail();
            }
            return RedirectToAction("Edit", new { id = id });
        }

        // GET: Order/Edit/5
        public ActionResult EditOrderStatus(int id,int status)
        {
            var result = 0;
            try { result = repo.UpdateOrderStatus(id, status, Admin.Id); } catch { }
            if (result > 0)
            {
                Notify_Update_Success();
            }
            else
            {
                Notify_Update_Fail();
            }
            return RedirectToAction("Edit", new { id = id });
        }

        // GET: Order/Export
        public void Export() {
            string where = string.Empty;
            List<string> whereC = new List<string>();

            DateTime dateFrom = ValueUtils.GetDate(Request.Unvalidated.QueryString["DateFrom"]);
            DateTime dateTo = ValueUtils.GetDate(Request.Unvalidated.QueryString["DateTo"]);

            if (NullUtils.cvInt(Request.Unvalidated.Form["category"]) > 0)
            {
                whereC.Add(string.Format(" (SubCategoryId={0}) ", NullUtils.cvInt(Request.Unvalidated.Form["category"])));
            }

            if (dateFrom > DateUtils.SqlMinDate()) {
                whereC.Add(" (CreateDate >= '" + dateFrom.ToString("yyyy-MM-dd", new CultureInfo("en-GB")) + "')");
            }
            if (dateTo > DateUtils.SqlMinDate()) {
                whereC.Add(" (CreateDate <= '" + dateTo.ToString("yyyy-MM-dd", new CultureInfo("en-GB")) + " 23:59:59')");
            }
            int[] status = ValueUtils.GetIntSplit(Request.QueryString["Status"], new char[] { ',' }, true);
            if (status.Length == 0) status = new int[] { 999 };
            whereC.Add(" Status IN (" + string.Join(",", status) + ")");

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            var dt = repo.ListOrder_Export(where, null);
            //var appRepo = new ProductApplicationRepository();
            //System.Data.DataSet appFormData = null;
            //var idList = new List<int>();
            //if (dt != null && dt.Rows.Count > 0)
            //{
            //    try
            //    {
            //        idList = dt.AsEnumerable().Select(o => o.Field<int>("ProductApplicationId")).ToList<int>();
            //        appFormData = appRepo.ListProductApplicationForms(idList);
            //    }
            //    catch (Exception ex) { }
            //}

            //ExportFile(dt, appFormData);
            ExportFile("Order.xlsx", "Order", dt, null, null, null, new string[] { "CreateDate", "UpdateDate", "PaymentDate" });
        }

        //protected void ExportFile(string fileName, string sheetName, DataTable dt, string[] intColumn, string[] doubleColumn, string[] dateColumn, string[] dateTimeColumn = null)
        protected void ExportFile(DataTable dt, System.Data.DataSet appFormData)
        {
            CountryRepository countryRepo = new CountryRepository();
            ProvinceRepository provinceRepo = new ProvinceRepository();
            MasterOptionRepository masterRepo = new MasterOptionRepository();

            var dtAppForm = appFormData.Tables["Table"];
            var dtPolicyHolder = appFormData.Tables["Table1"];
            var dtAttachment = appFormData.Tables["Table2"];
            var dtDriver = appFormData.Tables["Table3"];
            var dtMotor = appFormData.Tables["Table4"];
            var dtTA = appFormData.Tables["Table5"];
            var dtHome = appFormData.Tables["Table6"];
            var dtOrder = appFormData.Tables["Table7"];

            var Countries = countryRepo.ListCountry_SelectList_Active(Language.TH);
            var Relationships = masterRepo.ListMasterOption_SelectList_Active(Language.TH, MasterOptionCategory.Relationship);
            var Salutations = masterRepo.ListMasterOption_SelectList_Active(Language.TH, MasterOptionCategory.Salutation);
            var Provinces = provinceRepo.ListProvince_SelectList_Active(Language.TH);

            #region Support Methods
            Func<string, string> GetTitle = (titleId) =>
            {
                var result = Salutations.FirstOrDefault(o => o.Value == titleId);
                return result == null ? "" : result.Text;
            };
            Func<string, string> GetNationality = (nationalId) =>
            {
                var result = Countries.FirstOrDefault(o => o.Value == nationalId);
                return result == null ? "" : result.Text;
            };
            Func<int, string> GetIDCardType = (cardTypeId) =>
            {
                return ((int)TobJod.Models.IDTYPE.IDCard == cardTypeId ? "เลขที่ประจำตัวประชาชน" : "หมายเลข Passport");
            };
            #endregion

            using (ExcelPackage pck = new ExcelPackage())
            {
                ExcelWorksheet wsOrder = pck.Workbook.Worksheets.Add("Order");
                ExcelWorksheet wsMotor = pck.Workbook.Worksheets.Add("Motor");
                ExcelWorksheet wsPRB = pck.Workbook.Worksheets.Add("PRB");
                ExcelWorksheet wsPA = pck.Workbook.Worksheets.Add("PA");
                ExcelWorksheet wsPH = pck.Workbook.Worksheets.Add("PH");
                ExcelWorksheet wsTA = pck.Workbook.Worksheets.Add("TA");
                ExcelWorksheet wsHome = pck.Workbook.Worksheets.Add("Home");

                for(int i=0; i < pck.Workbook.Worksheets.Count; i++)
                {
                    using (ExcelRange rng = pck.Workbook.Worksheets[i+1].Cells[1, 1, 1, dt.Columns.Count])
                    {
                        rng.Style.Font.Bold = true;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                        rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));  //Set color to dark blue
                        rng.Style.Font.Color.SetColor(Color.White);
                    }
                }

                var lastRow = 2; var lastCell = 0;

                if (dt != null && dt.Rows.Count > 0)
                {
                    wsMotor.Cells[lastRow, 1].Value = "ยี่ห้อรถยนต์";
                    wsMotor.Cells[lastRow, 2].Value = "รุ่นรถยนต์";
                    wsMotor.Cells[lastRow, 3].Value = "รุ่นย่อย";
                    wsMotor.Cells[lastRow, 4].Value = "ปีจดทะเบียนรถ";
                    wsMotor.Cells[lastRow, 5].Value = "หมายเลขเครื่อง";
                    wsMotor.Cells[lastRow, 6].Value = "หมายเลขตัวถัง";
                    wsMotor.Cells[lastRow, 7].Value = "เลขทะเบียนรถ";
                    wsMotor.Cells[lastRow, 8].Value = "จังหวัดจดทะเบียน";
                    wsMotor.Cells[lastRow, 9].Value = "ติดตั้ง CCTV";
                    wsMotor.Cells[lastRow, 10].Value = "ประเภทอู่ซ่อม";

                    wsMotor.Cells[lastRow, 11].Value = "คำนำหน้าชื่อ";
                    wsMotor.Cells[lastRow, 12].Value = "ชื่อ";
                    wsMotor.Cells[lastRow, 13].Value = "นามสกุล";
                    wsMotor.Cells[lastRow, 14].Value = "สัญชาติ";
                    wsMotor.Cells[lastRow, 15].Value = "ประเภทบัตร";
                    wsMotor.Cells[lastRow, 16].Value = "เลขที่ประจำตัวประชาชน/ หมายเลข Passport";
                    wsMotor.Cells[lastRow, 17].Value = "เลขที่ใบขับขี่";
                    wsMotor.Cells[lastRow, 18].Value = "วันเกิด";

                    if (appFormData != null && (dtMotor != null) && dtMotor.Rows.Count > 0)
                    {
                        foreach(DataRow motor in dtMotor.Rows) {
                            var carInfo = GetCarInfo(motor);
                            wsMotor.Cells[lastRow, 1].Value = carInfo.Item1 != null ? NullUtils.cvString(carInfo.Item1.Text) : "";
                            wsMotor.Cells[lastRow, 2].Value = carInfo.Item2 != null ? NullUtils.cvString(carInfo.Item2.Name) : "";
                            wsMotor.Cells[lastRow, 3].Value = carInfo.Item3 != null ? NullUtils.cvString(carInfo.Item3.Name) : "";
                            wsMotor.Cells[lastRow, 4].Value = NullUtils.cvString(motor["CarRegisterYear"]) == "0" ? "" : motor["CarRegisterYear"].ToString();
                            wsMotor.Cells[lastRow, 5].Value = NullUtils.cvString(motor["CarEngineNo"]);
                            wsMotor.Cells[lastRow, 6].Value = NullUtils.cvString(motor["CarSerialNo"]);
                            wsMotor.Cells[lastRow, 7].Value = NullUtils.cvString(motor["CarPlateNo1"]) + NullUtils.cvString(motor["CarPlateNo2"]);
                            wsMotor.Cells[lastRow, 8].Value = carInfo.Item4 != null ? NullUtils.cvString(carInfo.Item4.Text) : "";
                            wsMotor.Cells[lastRow, 9].Value = (NullUtils.cvInt(motor["CarCCTV"]) == (int)TobJod.Models.ProductMotorCCTV.Yes ? "ติดตั้ง" : "ไม่ติดตั้ง");
                            wsMotor.Cells[lastRow, 10].Value = (NullUtils.cvInt(motor["CarGarageType"]) == (int)TobJod.Models.AllYesNo.Yes ? "ซ่อมศูนย์" : "ซ่อมอู่");
                            
                            if(dtDriver != null && dtDriver.Rows.Count > 0)
                            {
                                var driver = dtDriver.Select("ApplicationId=" + NullUtils.cvInt(motor["ApplicationId"])).FirstOrDefault();
                                if(driver != null)
                                {
                                    wsMotor.Cells[lastRow, 11].Value = GetTitle(NullUtils.cvString(driver["TitleId"]));
                                    wsMotor.Cells[lastRow, 12].Value = NullUtils.cvString(driver["Name"]); //"ชื่อ"
                                    wsMotor.Cells[lastRow, 13].Value = NullUtils.cvString(driver["Surname"]); //"นามสกุล";
                                    wsMotor.Cells[lastRow, 14].Value = GetNationality(NullUtils.cvString(driver["NationalId"])); //"สัญชาติ";
                                    wsMotor.Cells[lastRow, 15].Value = GetIDCardType(NullUtils.cvInt(driver["IDType"])); //"ประเภทบัตร";
                                    wsMotor.Cells[lastRow, 16].Value = NullUtils.cvString(driver["IDCard"]); //"เลขที่ประจำตัวประชาชน/ หมายเลข Passport";
                                    wsMotor.Cells[lastRow, 17].Value = NullUtils.cvString(driver["RefIDCard"]); //"เลขที่ใบขับขี่";
                                    wsMotor.Cells[lastRow, 18].Value = NullUtils.cvDateTime(driver["Birthday"]).ToString("dd/MM/yyyy", cultureGB); //"วันเกิด";
                                }
                            }

                            lastRow = 2; lastCell = 18;
                            var lastDimension = BindAddressSheet(lastRow, lastCell, wsMotor, dtAppForm);
                            lastRow = lastDimension.Item1; lastCell = lastDimension.Item2;
                        }

                    }

                }

                //Write it back to the client
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=" + "Order.xlsx");
                Response.BinaryWrite(pck.GetAsByteArray());
            }
        }

        private Tuple<int,int> BindAddressSheet(int lastRow, int lastCell,ExcelWorksheet ws, DataTable dtAppForm)
        {

            Func<int, int, string, ExcelRange> MergeCell = (r, c, caption) => {
                var cell = ws.Cells[r - 1, c, r - 1, c + 10];
                cell.Value = caption;
                cell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                cell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                cell.Merge = true;
                return cell;
            };

            MergeCell(lastRow, lastCell, "ที่อยู่ปัจจุบัน");
            ws.Cells[lastRow, ++lastCell].Value = "เลขที่";
            ws.Cells[lastRow, ++lastCell].Value = "หมู่";
            ws.Cells[lastRow, ++lastCell].Value = "หมู่บ้าน";
            ws.Cells[lastRow, ++lastCell].Value = "ชั้น";
            ws.Cells[lastRow, ++lastCell].Value = "ซอย";
            ws.Cells[lastRow, ++lastCell].Value = "ถนน";
            ws.Cells[lastRow, ++lastCell].Value = "แขวง/ ตำบล";
            ws.Cells[lastRow, ++lastCell].Value = "เขต/ อำเภอ";
            ws.Cells[lastRow, ++lastCell].Value = "จังหวัด";
            ws.Cells[lastRow, ++lastCell].Value = "รหัสไปรษณีย์";

            MergeCell(lastRow, ++lastCell, "ที่อยู่ในการออกใบกำกับภาษี");
            ws.Cells[lastRow, ++lastCell].Value = "เลขที่";
            ws.Cells[lastRow, ++lastCell].Value = "หมู่";
            ws.Cells[lastRow, ++lastCell].Value = "หมู่บ้าน";
            ws.Cells[lastRow, ++lastCell].Value = "ชั้น";
            ws.Cells[lastRow, ++lastCell].Value = "ซอย";
            ws.Cells[lastRow, ++lastCell].Value = "ถนน";
            ws.Cells[lastRow, ++lastCell].Value = "แขวง/ ตำบล";
            ws.Cells[lastRow, ++lastCell].Value = "เขต/ อำเภอ";
            ws.Cells[lastRow, ++lastCell].Value = "จังหวัด";
            ws.Cells[lastRow, ++lastCell].Value = "รหัสไปรษณีย์";

            MergeCell(lastRow, ++lastCell, "ที่อยู่ในการจัดส่งเอกสาร");
            ws.Cells[lastRow, ++lastCell].Value = "เลขที่";
            ws.Cells[lastRow, ++lastCell].Value = "หมู่";
            ws.Cells[lastRow, ++lastCell].Value = "หมู่บ้าน";
            ws.Cells[lastRow, ++lastCell].Value = "ชั้น";
            ws.Cells[lastRow, ++lastCell].Value = "ซอย";
            ws.Cells[lastRow, ++lastCell].Value = "ถนน";
            ws.Cells[lastRow, ++lastCell].Value = "แขวง/ ตำบล";
            ws.Cells[lastRow, ++lastCell].Value = "เขต/ อำเภอ";
            ws.Cells[lastRow, ++lastCell].Value = "จังหวัด";
            ws.Cells[lastRow, ++lastCell].Value = "รหัสไปรษณีย์";

            return new Tuple<int, int>(lastRow, lastCell);
        }

        private Tuple<SelectListItem, NameValue, NameValue, SelectListItem> GetCarInfo(DataRow motor)
        {
            MasterOptionRepository masterRepo = new MasterOptionRepository();
            CarRepository carRepo = new CarRepository();

            var carBrands = carRepo.ListCarBrand_SelectList(Language.TH);
            var carFamily = carRepo.ListCarFamilyByBrand_NameValue(Language.TH, NullUtils.cvInt(motor["CarBrand"]));
            var carModel = carRepo.ListCarModelByBrand_NameValue(Language.TH, NullUtils.cvInt(motor["CarBrand"]));
            var carProvinces = masterRepo.ListMasterOption_SelectList_Active(Language.TH, MasterOptionCategory.MotorProvince);

            var carBrandObj = carBrands.FirstOrDefault(brand => brand.Value == NullUtils.cvString(motor["CarBrand"]));
            var carFamilyObj = carFamily.FirstOrDefault(family => family.Value == NullUtils.cvString(motor["CarFamily"]));
            var carModelObj = carModel.FirstOrDefault(m => m.Value == NullUtils.cvString(motor["CarModelId"]));
            var carProvinceObj = carProvinces.FirstOrDefault(p => p.Value == NullUtils.cvString(motor["CarProvinceId"]));

            return new Tuple<SelectListItem, NameValue, NameValue, SelectListItem>(carBrandObj,carFamilyObj,carModelObj,carProvinceObj);
        }


        // GET: Order/DataTable
        public ActionResult DataTable() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();

            DateTime dateFrom = ValueUtils.GetDate(Request.Unvalidated.Form["DateFrom"]);
            DateTime dateTo = ValueUtils.GetDate(Request.Unvalidated.Form["DateTo"]);

            if (NullUtils.cvInt(Request.Unvalidated.Form["category"]) > 0)
            {
                whereC.Add(" (SubCategoryId=@category) ");
                whereParams.category = NullUtils.cvInt(Request.Unvalidated.Form["category"]);
            }

            if (dateFrom > DateUtils.SqlMinDate()) {
                whereC.Add(" (CreateDate >= @DateFrom)");
                whereParams.DateFrom = dateFrom;
            }

            if (dateTo > DateUtils.SqlMinDate()) {
                whereC.Add(" (CreateDate <= @DateTo)");
                whereParams.DateTo = DateUtils.ExtendToEndofDate(dateTo);
            }

            int[] status = ValueUtils.GetIntSplit(Request.Unvalidated.Form["Status"], new char[] { ',' }, true);
            if (status.Length == 0) status = new int[] { 999 };
            whereC.Add(" Status IN @status");
            whereParams.status = status;

            if (param.hasSearch) {
                whereC.Add(" (BuyerName LIKE @searchValue OR BuyerEmail LIKE @searchValue OR BuyerMobilePhone LIKE @searchValue OR ProductCode LIKE @searchValue OR ProductName LIKE @searchValue OR ProductSubCategoryTitle LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }
            
            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "OrderCode", "BuyerName", "BuyerEmail", "BuyerMobilePhone", "ProductCode", "ProductName", "ProductSubCategoryTitle", "CreateDate", "PaymentId", "PaymentDate", "PaymentInstallment", "PaymentTotal", "PaymentResponse","PartnerAPIStatus","BackendAPIStatus", "Status", "Id", "Compulsory", "AttachedFile" };
            DataTableData<OrderApplication> data = repo.ListOrderApplication_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.OrderCode.ToString(),
                        NullUtils.cvString(x.BuyerName).ToString(),
                        NullUtils.cvString(x.BuyerEmail).ToString(),
                        NullUtils.cvString(x.BuyerMobilePhone).ToString(),
                        NullUtils.cvString(x.ProductCode).ToString(),
                        NullUtils.cvString(x.ProductName).ToString(),
                        x.ProductSubCategoryTitle,
                        (x.Compulsory == 1 ? "Y" : ""),
                        x.CreateDate.ToString("dd/MM/yyyy HH:mm"),
                        x.PaymentId.ToString(),
                        x.PaymentDate != TobJod.Utils.DateUtils.SqlMinDate() ? x.PaymentDate.ToString("dd/MM/yyyy HH:mm") : string.Empty,
                        x.PaymentInstallment.ToString(),
                        x.PaymentTotal.ToString("n2"),
                        NullUtils.cvString(x.PaymentResponse),
                        ("<div class=\"btn btn-" + GetStatusClass((int)x.PartnerAPIStatus) + "\">" + x.PartnerAPIStatus.ToString() + "</a>"),
                        ("<div class=\"btn btn-" + GetStatusClass((int)x.BackendAPIStatus) + "\">" + x.BackendAPIStatus.ToString() + "</a>"),
                        ("<div class=\"btn btn-" + (string.Equals(x.AttachedFile, "Yes") ? "success" : "danger") + "\">" + x.AttachedFile + "</a>"),
                       ("<div class=\"btn btn-" + GetStatusClass((int)x.Status)+ "\">" + x.Status.ToString() + "</a>"),
                        (
                            HtmlActionButton(AdminAction.Edit, x.Id)
                        )
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        protected string GetStatusClass(int status) {
            switch (status) {
                case 0: return "info"; //Pending
                case 1: return "info"; //Retry
                case 2: return "success"; //Success
                case 3: return "danger"; //Fail
                case 4: return "danger"; //Reject
                case 5: return "default"; //Cancel
                default: return "info";
            }
        }

        public ActionResult DeleteFile(string fileId, string applicationId, string productId, string appForm)
        {
            int result = 0;
            if ((NullUtils.cvInt(fileId) > 0 && NullUtils.cvInt(applicationId) > 0 && NullUtils.cvInt(productId) > 0) && appForm + "" != string.Empty)
            {
                ProductApplicationRepository repo = new ProductApplicationRepository();
                var application = repo.GetProductApplicationForm(NullUtils.cvInt(applicationId));
                if (application != null && application.ProductApplicationAttachment != null)
                {
                    var o = application.ProductApplicationAttachment.Where(x => x.Id == NullUtils.cvInt(fileId)).FirstOrDefault();
                    if (o != null)
                    {
                        result = repo.DeleteProductApplicationAttachment(NullUtils.cvInt(fileId));
                        if (result > 0)
                        {
                            string url = string.Format(Url.Action(appForm, "Order") + "?ApplicationId={0}&ProductId={1}", applicationId, productId);
                            return Redirect(url);
                        }
                    }
                }
            }
            return Redirect(Url.Content("~"));
        }

        public FileResult Download(string id)
        {
            var fileId = TobJod.Utils.NullUtils.cvInt(id);
            if (fileId > 0)
            {
                ProductApplicationRepository repo = new ProductApplicationRepository();
                var file = repo.GetProductApplicationAttachment(fileId);
                if(file != null)
                {
                    var contentType = MimeMapping.GetMimeMapping(file.FileName);
                    return File(file.FileContent, contentType, file.FileName);
                }
            }
            return null;
        }
        private void UploadAttachmentFile(int applicationId, int productId, HttpPostedFileBase file, FileCategory fileCategory, ProductApplication app, List<ProductApplicationAttachment> attachments)
        {
            if (file != null && file.ContentLength > 0)
            {
                var fileName = string.Format("{0}_{1}_{2}", applicationId, (int)fileCategory, file.FileName);
                var fileSize = file.InputStream.Length;
                byte[] bytes;

                using (System.IO.BinaryReader br = new System.IO.BinaryReader(file.InputStream))
                {
                    bytes = br.ReadBytes(file.ContentLength);
                }

                ProductApplicationAttachment attachment = new ProductApplicationAttachment();
                attachment.ApplicationId = applicationId;
                attachment.FileSize = fileSize;
                attachment.FileContent = bytes;
                attachment.FileCategory = (int)fileCategory;
                attachment.FileName = fileName;
                attachment.CreateDate = DateTime.Now;
                attachments.Add(attachment);
            }
            else
            {
                if (app != null && app.ProductApplicationAttachment != null && app.ProductApplicationAttachment.Count > 0)
                {
                    var item = app.ProductApplicationAttachment.Where(o => o.ApplicationId == applicationId && o.FileCategory == (int)fileCategory).FirstOrDefault();
                    if (item != null)
                    {
                        item.UpdateDate = DateTime.Now;
                        attachments.Add(item);
                    }
                }
            }
        }
    }
}
