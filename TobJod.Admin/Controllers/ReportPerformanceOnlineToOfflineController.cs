﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TobJod.Admin.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class ReportPerformanceOnlineToOfflineController : BaseController {

        // GET: ReportPerformanceOnlineToOffline
        public ActionResult Index(ReportPerformanceOnlineToOfflineViewModel model) {
            ReportRepository repo = new ReportRepository();
            model.DateFrom = ValueUtils.GetDate(Request.Params["DateFrom"]);
            model.DateTo = ValueUtils.GetDate(Request.Params["DateTo"]);
            if (model.DateTo == DateUtils.SqlMinDate()) model.DateTo = DateTime.Today.AddDays(-1);
            if (model.DateFrom == DateUtils.SqlMinDate()) model.DateFrom = DateUtils.GetFirstDayOfMonth(model.DateTo);

            //31 DAYS
            var x = model.DateTo.Subtract(model.DateFrom);
            if (x.Days <= 31) {
                if (string.Equals(Request.Params["export"], "1")) {
                    var dt = repo.ListPerformanceExport(model.DateFrom, model.DateTo, (int)model.Source);
                    Export(dt, model.DateFrom, model.DateTo);
                } else if (Request.QueryString.Count > 0) {
                    model.DataTable = repo.ListPerformance(model.DateFrom, model.DateTo, (int)model.Source); ;
                }
            } else {
                Notify(TobJod.Models.NotifyMessageType.Warning, "Date must be <= 31 days.");
            }

            return View(model);
        }

        private void Export(DataTable dt, DateTime dateFrom, DateTime dateTo) {
            string sheetName = "PerformanceOnlineToOffline";
            string fileName = "PerformanceOnlineToOffline.xlsx";
            Color bgColor = Color.FromArgb(255, 251, 213, 182);

            //Dictionary<string, int> data = new Dictionary<string, int>();
            Dictionary<string, int> dataTotal = new Dictionary<string, int>();
            foreach (DataRow row in dt.Rows) {
                //data[row["CreateDate"] + "|" + row["LogDate"] + "|" + row["LeadStatus"]] = NullUtils.cvInt(row["Total"]);
                if (!dataTotal.ContainsKey(row["CreateDate"].ToString())) dataTotal[row["CreateDate"].ToString()] = 0;
                dataTotal[row["CreateDate"].ToString()] += NullUtils.cvInt(row["Total"]);
            }

            using (ExcelPackage pck = new ExcelPackage()) {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add(sheetName);

                ws.Cells["A2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells["A2"].Style.Fill.BackgroundColor.SetColor(bgColor);
                ws.Cells["A2"].Style.Font.Bold = true;

                ws.Cells["A2"].Value = "Date";


                DateTime dateHeader = dateFrom;
                int column = 2;
                while (dateHeader <= (dateTo)) {
                    ws.Cells[1, column].Value = dateHeader.ToString("dd/MM/yyyy", cultureGB);
                    ws.Cells[1, column, 1, column + 7].Merge = true;
                    ws.Cells[1, column, 1, column + 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    ws.Cells[1, column, 1, column + 7].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[1, column, 1, column + 7].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 242, 176, 135));
                    ws.Cells[2, column].Value = "Total"; column++;
                    ws.Cells[2, column].Value = "Pending"; column++;
                    ws.Cells[2, column].Value = "Allocated"; column++;
                    ws.Cells[2, column].Value = "Follow Up"; column++;
                    ws.Cells[2, column].Value = "Yes"; column++;
                    ws.Cells[2, column].Value = "No"; column++;
                    ws.Cells[2, column].Value = "Spam"; column++;
                    ws.Cells[2, column].Value = "Closed"; column++;
                    dateHeader = dateHeader.AddDays(1);
                }

                int i = 3;
                DateTime date = dateFrom;
                DateTime endDate = dateTo.AddDays(365); //new DateTime(dateTo.Year + 1, dateTo.Month, dateTo.Day).AddDays(-1);
                if (endDate > DateTime.Today) endDate = DateTime.Today;

                while (date <= endDate) {
                    ws.Cells["A" + i.ToString()].Value = date.ToString("dd/MM/yyyy", cultureGB);
                    DateTime dateColumn = dateFrom;
                    int col = 2;
                    while (dateColumn <= dateTo) {
                        if (date >= dateColumn) {
                            DateTime dateC = dateColumn;

                            ws.Cells[i, col].Value = (dataTotal.ContainsKey(dateColumn.ToString()) ? dataTotal[dateColumn.ToString()] : 0); //Total
                            col++;
                            //ws.Cells[i, col].Value = dt.AsEnumerable().Where(x => x.Field<DateTime>("CreateDate") == dateColumn && x.Field<DateTime>("LogDate") <= date && (x.Field<int>("LeadStatus") == 0)).Sum(x => x.Field<int>("Total"));
                            col++;
                            ws.Cells[i, col].Value = dt.AsEnumerable().Where(x => x.Field<DateTime>("CreateDate") == dateColumn && x.Field<DateTime>("LogDate") <= date && x.Field<int>("LeadStatus") == 1).Sum(x => x.Field<int>("Total"));
                            col++;
                            ws.Cells[i, col].Value = dt.AsEnumerable().Where(x => x.Field<DateTime>("CreateDate") == dateColumn && x.Field<DateTime>("LogDate") <= date && (x.Field<int>("LeadStatus") == 2)).Sum(x => x.Field<int>("Total"));
                            col++;
                            ws.Cells[i, col].Value = dt.AsEnumerable().Where(x => x.Field<DateTime>("CreateDate") == dateColumn && x.Field<DateTime>("LogDate") <= date && x.Field<int>("LeadStatus") == 3).Sum(x => x.Field<int>("Total"));
                            col++;
                            ws.Cells[i, col].Value = dt.AsEnumerable().Where(x => x.Field<DateTime>("CreateDate") == dateColumn && x.Field<DateTime>("LogDate") <= date && x.Field<int>("LeadStatus") == 4).Sum(x => x.Field<int>("Total"));
                            col++;
                            ws.Cells[i, col].Value = dt.AsEnumerable().Where(x => x.Field<DateTime>("CreateDate") == dateColumn && x.Field<DateTime>("LogDate") <= date && x.Field<int>("LeadStatus") == 5).Sum(x => x.Field<int>("Total"));
                            col++;
                            ws.Cells[i, col].Value = dt.AsEnumerable().Where(x => x.Field<DateTime>("CreateDate") == dateColumn && x.Field<DateTime>("LogDate") <= date && x.Field<int>("LeadStatus") == 9).Sum(x => x.Field<int>("Total"));
                            col++;

                            ws.Cells[i, col - 7].Value = (NullUtils.cvInt(ws.Cells[i, (col - 8)].Value) - (NullUtils.cvInt(ws.Cells[i, (col - 6)].Value) + NullUtils.cvInt(ws.Cells[i, (col - 5)].Value) + NullUtils.cvInt(ws.Cells[i, (col - 4)].Value) + NullUtils.cvInt(ws.Cells[i, (col - 3)].Value) + NullUtils.cvInt(ws.Cells[i, (col - 2)].Value) + NullUtils.cvInt(ws.Cells[i, (col - 1)].Value)));
                        } else {
                            col += 7;
                        }
                        dateColumn = dateColumn.AddDays(1);
                    }

                    date = date.AddDays(1);
                    i++;
                }
                
                int c = dt.Columns[2].Ordinal + 1;
                using (ExcelRange col = ws.Cells[2, c, 2 + dt.Rows.Count, c]) {
                    col.Style.Numberformat.Format = "#,##0";
                    col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                }

                //Format the header
                using (ExcelRange rng = ws.Cells[2, 1, 2, (column - 1)]) { // "A1:" + GetExcelColumnName(column) + (i - 1)]) {
                    rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    rng.Style.Fill.BackgroundColor.SetColor(bgColor);
                }

                //Format the border
                using (ExcelRange rng = ws.Cells[1, 1, (i - 1), (column - 1)]) { // "A1:" + GetExcelColumnName(column) + (i - 1)]) {
                    rng.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                }

                ws.Cells.AutoFitColumns();

                //Write it back to the client
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=" + fileName);
                Response.BinaryWrite(pck.GetAsByteArray());
                Response.End();
            }

        }
    }
}