﻿using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using TobJod.Admin.Models;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class BankController : BaseController {
        BankRepository repo;

        public BankController() {
            repo = new BankRepository();
        }

        // GET: Bank
        public ActionResult Index() {
            return View();
        }

        // GET: Bank/Create
        public ActionResult Create() {
            Bank item = new Bank();
            item.Status = Status.Active;
            BindForm();
            return View("Form", item);
        }

        // POST: Bank/Create
        [HttpPost]
        public ActionResult Create(Bank item) {
            try {
                item.CreateDate = item.UpdateDate = DateTime.Now;
                item.CreateAdminId = item.UpdateAdminId = Admin.Id;

                repo.AddBank(item);
                Notify_Add_Success();
                return RedirectToAction("Index");
            } catch { }
            Notify_Add_Fail();
            BindForm();
            return View("Form", item);
        }

        // GET: Bank/Edit/5
        public ActionResult Edit(int id) {
            Bank item = repo.GetBank_Admin(id);
            BindForm();
            return View("Form", item);
        }

        // POST: Bank/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Bank item) {
            int result = 0;
            try {
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = Admin.Id;

                result = repo.UpdateBank(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            BindForm();
            return View("Form", item);
        }

        // GET: Bank/Import
        public ActionResult Import() {
            CheckPermission(AdminAction.Add);
            return View();
        }

        // POST: Bank/Import/5
        [HttpPost]
        public ActionResult Import(HttpPostedFileBase File) {
            CheckPermission(AdminAction.Add);
            List<Bank> items = new List<Bank>();
            StringBuilder sb = new StringBuilder();
            try {
                string path = System.IO.Path.GetTempPath();
                string fileName = UploadUtils.SaveFile(File, path);
                DataSet ds;
                using (var stream = System.IO.File.Open(path + fileName, FileMode.Open, FileAccess.Read)) {
                    using (var reader = ExcelReaderFactory.CreateReader(stream)) {
                        ds = reader.AsDataSet();
                    }
                }

                var t_banks = repo.ListBank_Dictionary_Admin();
                var banks = new Dictionary<string, int>(t_banks, StringComparer.OrdinalIgnoreCase);

                StringBuilder sbRow;
                using (ds) {
                    DataTable dt = ds.Tables[0];
                    DateTime now = DateTime.Now;
                    bool isFirst = true;
                    int i = 1;
                    foreach (DataRow dr in dt.Rows) {
                        if (isFirst) { isFirst = false; continue; }

                        Bank bank = new Bank() { Code = NullUtils.cvString(dr[0]), PaymentGatewayCode = NullUtils.cvString(dr[1]), TitleTH = NullUtils.cvString(dr[2]), TitleEN = NullUtils.cvString(dr[3]), Status = Status.Active, CreateDate = now, CreateAdminId = Admin.Id, UpdateDate = now, UpdateAdminId = Admin.Id };
                        sbRow = new StringBuilder();

                        sbRow.Append(ValidateModelMessage(bank));

                        if (sbRow.Length > 0) {
                            sb.Append("<tr><th>Row " + (i+1) + "</th><td> " + sbRow + "</td></tr>");
                        } else {
                            items.Add(bank);
                        }
                        
                        i++;
                    }
                }

                if (sb.Length > 0) {
                    ViewBag.Message = "INVALID DATA";
                    ViewBag.ErrorMessage = sb.ToString();
                    Notify_Add_Fail();
                    return View();
                } else {
                    int result = repo.AddBank(items);
                    if (result > 0) {
                        Notify_Add_Success();
                        return RedirectToAction("Index");
                    }
                }
            } catch { }
            Notify_Add_Fail();
            return View();
        }

        // GET: Bank/Delete/5
        public ActionResult Delete(int id) {
            var result = repo.DeleteBank(id, Admin.Id);
            if (result > 0) {
                Notify_Delete_Success();
            } else {
                Notify_Delete_Fail();
            }
            return RedirectToAction("Index");
        }

        // GET: Bank/DataTable
        public ActionResult DataTable() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();

            if (param.hasSearch) {
                whereC.Add(" (TitleTH LIKE @searchValue OR TitleEN LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "Id", "TitleTH", "Code", "PaymentGatewayCode", "UpdateDate", "Status" };
            DataTableData<Bank> data = repo.ListBank_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Id.ToString(),
                        x.TitleTH.ToString(),
                        x.Code.ToString(),
                        x.PaymentGatewayCode.ToString(),
                        x.UpdateDate.ToString("dd/MM/yyyy HH:mm"),
                        x.Status.ToString(),
                        (HtmlActionButton(AdminAction.Edit, x.Id) + HtmlActionButton(AdminAction.Delete, x.Id))
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        private void BindForm() {

        }
    }
}
