﻿using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class CarFamilyController : BaseController {
        CarRepository repo;

        public CarFamilyController() {
            repo = new CarRepository();
        }
        
        // GET: CarFamily
        public ActionResult Index() {
            return View();
        }

        // GET: CarFamily/Create
        public ActionResult Create() {
            CarFamily item = new CarFamily();
            item.Status = Status.Active;
            BindForm();
            return View("Form", item);
        }

        // POST: CarFamily/Create
        [HttpPost]
        public ActionResult Create(CarFamily item) {
            try {
                item.CreateDate = item.UpdateDate = DateTime.Now;
                item.CreateAdminId = item.UpdateAdminId = Admin.Id;

                repo.AddCarFamily(item);
                Notify_Add_Success();
                return RedirectToAction("Index");
            } catch { }
            Notify_Add_Fail();
            BindForm();
            return View("Form", item);
        }

        // GET: CarFamily/Edit/5
        public ActionResult Edit(int id) {
            CarFamily item = repo.GetCarFamily_Admin(id);
            BindForm();
            return View("Form", item);
        }

        // POST: CarFamily/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, CarFamily item) {
            int result = 0;
            try {
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = Admin.Id;

                result = repo.UpdateCarFamily(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            BindForm();
            return View("Form", item);
        }

        // GET: CarBrand/Import
        public ActionResult Import() {
            CheckPermission(AdminAction.Add);
            return View();
        }

        // POST: CarFamily/Import/5
        [HttpPost]
        public ActionResult Import(HttpPostedFileBase File) {
            CheckPermission(AdminAction.Add);
            List<CarFamily> items = new List<CarFamily>();
            StringBuilder sb = new StringBuilder();
            try {
                string path = System.IO.Path.GetTempPath();
                string fileName = UploadUtils.SaveFile(File, path);
                DataSet ds;
                using (var stream = System.IO.File.Open(path + fileName, FileMode.Open, FileAccess.Read)) {
                    using (var reader = ExcelReaderFactory.CreateReader(stream)) {
                        ds = reader.AsDataSet();
                    }
                }

                var brands = repo.ListCarBrand_Dictionary_Admin();

                StringBuilder sbRow;
                using (ds) {
                    DataTable dt = ds.Tables[0];
                    DateTime now = DateTime.Now;
                    bool isFirst = true;
                    int i = 1;
                    foreach (DataRow dr in dt.Rows) {
                        if (isFirst) { isFirst = false; continue; }
                        
                        CarFamily r = new CarFamily() { BrandId = 0, TitleTH = NullUtils.cvString(dr[2]), TitleEN = NullUtils.cvString(dr[3]), MapName = NullUtils.cvString(dr[1]), Status = Status.Active, CreateDate = now, CreateAdminId = Admin.Id, UpdateDate = now, UpdateAdminId = Admin.Id };

                        sbRow = new StringBuilder();

                        sbRow.Append(ValidateModelMessage(r));

                        var brand = NullUtils.cvString(dr[0]);
                        if (brands.ContainsKey(brand)) {
                            r.BrandId = brands[brand];
                        } else {
                            sbRow.AppendLine("- Brand \"" + brand + "\" Not found.<br/>");
                        }

                        if (sbRow.Length > 0) {
                            sb.Append("<tr><th>Row " + (i + 1) + "</th><td> " + sbRow + "</td></tr>");
                        } else {
                            items.Add(r);
                        }

                        i++;
                    }
                }

                if (sb.Length > 0) {
                    ViewBag.Message = "INVALID DATA";
                    ViewBag.ErrorMessage = sb.ToString();
                    Notify_Add_Fail();
                    return View();
                } else {
                    int result = repo.AddCarFamily(items);
                    if (result > 0) {
                        Notify_Add_Success();
                        return RedirectToAction("Index");
                    }
                }
            } catch { }
            Notify_Add_Fail();
            return View();
        }

        // GET: CarFamily/Delete/5
        public ActionResult Delete(int id) {
            var item = repo.ListCarModel_SelectList_Admin(id);
            if (item.Count == 0) {
                var result = repo.DeleteCarFamily(id, Admin.Id);
                if (result > 0) {
                    Notify_Delete_Success();
                } else {
                    Notify_Delete_Fail();
                }
            } else {
                Notify(NotifyMessageType.Error, "Delete Error", "Record is in used.");
            }
            return RedirectToAction("Index");
        }

        // GET: CarFamily/DataTable
        public ActionResult DataTable() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();
           
            if (param.hasSearch) {
                whereC.Add(" (TitleTH LIKE @searchValue OR TitleEN LIKE @searchValue OR Brand LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "Id", "Brand", "TitleTH", "UpdateDate", "Status" };
            DataTableData<CarFamily> data = repo.ListCarFamily_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Id.ToString(),
                        x.Brand.ToString(),
                        x.TitleTH.ToString(),
                        x.UpdateDate.ToString("dd/MM/yyyy HH:mm"),
                        x.Status.ToString(),
                        (HtmlActionButton(AdminAction.Edit, x.Id) + HtmlActionButton(AdminAction.Delete, x.Id))
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        private void BindForm() {
            ViewBag.BrandList = repo.ListCarBrand_SelectList_Admin();
        }

    }
}
