﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class DayOffController : BaseController {
        DayOffRepository repo;

        public DayOffController() {
            repo = new DayOffRepository();
        }
        
        // GET: DayOff
        public ActionResult Index() {
            var d = repo.ListDayOff_MinMax();
            List<SelectListItem> years = new List<SelectListItem>();
            int year = NullUtils.cvInt(Request.QueryString["Year"]);
            for (int i = d.Item2.Year; i >= d.Item1.Year; i--) {
                years.Add(new SelectListItem() { Text = i.ToString(), Value = i.ToString(), Selected = (i == year) });
            }
            ViewBag.YearList = years;
            return View();
        }

        // GET: DayOff/Create
        public ActionResult Create() {
            DayOff item = new DayOff();
            item.Status = Status.Active;
            BindForm();
            return View("Form", item);
        }

        // POST: DayOff/Create
        [HttpPost]
        public ActionResult Create(DayOff item) {
            try {
                item.CreateDate = item.UpdateDate = DateTime.Now;
                item.CreateAdminId = item.UpdateAdminId = Admin.Id;
                
                repo.AddDayOff(item);
                Notify_Add_Success();
                return RedirectToAction("Index");
            } catch { }
            Notify_Add_Fail();
            BindForm();
            return View("Form", item);
        }

        // GET: DayOff/Edit/5
        public ActionResult Edit(int id) {
            DayOff item = repo.GetDayOff_Admin(id);
            BindForm();
            return View("Form", item);
        }

        // POST: DayOff/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, DayOff item) {
            int result = 0;
            try {
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = Admin.Id;
                
                result = repo.UpdateDayOff(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            BindForm();
            return View("Form", item);
        }

        // GET: DayOff/Delete/5
        public ActionResult Delete(int id) {
            var result = repo.DeleteDayOff(id, Admin.Id);
            if (result > 0) {
                Notify_Delete_Success();
            } else {
                Notify_Delete_Fail();
            }
            return RedirectToAction("Index");
        }

        // GET: DayOff/DataTable
        public ActionResult DataTable() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();

            int year = NullUtils.cvInt(Request.Form["Year"]);

            if (year > 0) {
                whereC.Add(" (YEAR(EventDate) = @year) ");
                whereParams.year = year;
            }

            if (param.hasSearch) {
                whereC.Add(" (TitleTH LIKE @searchValue OR TitleEN LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "Id", "EventDate", "TitleTH", "UpdateDate", "Status" };
            DataTableData<DayOff> data = repo.ListDayOff_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Id.ToString(),
                        x.EventDate.ToString("dd/MM/yyyy"),
                        x.TitleTH.ToString(),
                        x.UpdateDate.ToString("dd/MM/yyyy HH:mm"),
                        x.Status.ToString(),
                        (HtmlActionButton(AdminAction.Edit, x.Id) + HtmlActionButton(AdminAction.Delete, x.Id))
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        private void BindForm() {
        }

    }
}
