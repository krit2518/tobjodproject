﻿using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using TobJod.Admin.Models;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class PoiListController : BaseController {
        PoiRepository repo;

        public PoiListController() {
            repo = new PoiRepository();
        }

        // GET: PoiList
        public ActionResult Index() {
            return View();
        }

        // GET: PoiList/Create
        public ActionResult Create() {
            Poi item = new Poi();
            item.Status = Status.Active;
            BindForm();
            return View("Form", item);
        }

        // POST: PoiList/Create
        [HttpPost]
        public ActionResult Create(Poi item) {
            try {
                item.CreateDate = item.UpdateDate = DateTime.Now;
                item.CreateAdminId = item.UpdateAdminId = Admin.Id;

                repo.AddPoi(item);
                Notify_Add_Success();
                return RedirectToAction("Index");
            } catch { }
            Notify_Add_Fail();
            BindForm();
            return View("Form", item);
        }

        // GET: PoiList/Edit/5
        public ActionResult Edit(int id) {
            Poi item = repo.GetPoi_Admin(id);
            BindForm();
            return View("Form", item);
        }

        // POST: PoiList/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Poi item) {
            int result = 0;
            try {
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = Admin.Id; // = item.OwnerAdminId

                result = repo.UpdatePoi(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            BindForm();
            return View("Form", item);
        }

        // GET: PoiList/Import
        public ActionResult Import() {
            CheckPermission(AdminAction.Add);
            return View();
        }

        // POST: PoiList/Import/5
        [HttpPost]
        public ActionResult Import(HttpPostedFileBase File) {
            CheckPermission(AdminAction.Add);
            List<Poi> items = new List<Poi>(); 
            try {
                string path = System.IO.Path.GetTempPath();
                string fileName = UploadUtils.SaveFile(File, path);
                DataSet ds;
                using (var stream = System.IO.File.Open(path + fileName, FileMode.Open, FileAccess.Read)) {
                    using (var reader = ExcelReaderFactory.CreateReader(stream)) {
                        ds = reader.AsDataSet();
                    }
                }

                StringBuilder sbError = new StringBuilder();
                int[] categories = { 1, 2, 3 };

                using (ds) {
                    DataTable dt = ds.Tables[0];
                    DateTime now = DateTime.Now;
                    bool isFirst = true;
                    Poi poi;
                    int i = 1;
                    foreach (DataRow dr in dt.Rows) {
                        if (isFirst) { isFirst = false; continue; }
                        poi = new Poi() { TitleTH = NullUtils.cvString(dr[1]).Trim(Environment.NewLine.ToCharArray()), TitleEN = NullUtils.cvString(dr[2]).Trim(Environment.NewLine.ToCharArray()), Latitude = NullUtils.cvDouble(dr[3]), Longitude = NullUtils.cvDouble(dr[4]), Status = Status.Active, CreateDate = now, CreateAdminId = Admin.Id, UpdateDate = now, UpdateAdminId = Admin.Id };
                        if (categories.Contains(NullUtils.cvInt(dr[0])) && poi.Latitude != 0 && poi.Longitude != 0) {
                            poi.Category = (PoiCategory)NullUtils.cvInt(dr[0]);
                            items.Add(poi);
                        } else {
                            sbError.AppendLine("<tr><th>Row " + (i+1) + "</th><td>");
                            if (!categories.Contains(NullUtils.cvInt(dr[0]))) {
                                sbError.AppendLine("- Category Incorrect. [" + NullUtils.cvString(dr[0]) + "]<br />");
                            }
                            if (poi.Latitude == 0) {
                                sbError.AppendLine("- Latitude Incorrect. [" + NullUtils.cvString(dr[3]) + "]<br />");
                            }
                            if (poi.Longitude == 0) {
                                sbError.AppendLine("- Longitude Incorrect. [" + NullUtils.cvString(dr[4]) + "]<br />");
                            }
                            sbError.AppendLine("</td></tr>");
                        }
                        i++;
                    }
                }
                
                if (sbError.Length > 0) {
                    ViewBag.ErrorMessage = sbError.ToString();
                } else {
                    int result = repo.AddPoi(items);
                    if (result > 0) {
                        Notify_Add_Success();
                        return RedirectToAction("Index");
                    }
                }
            } catch { }
            Notify_Add_Fail();
            return View();
        }

        // GET: PoiList/Delete/5
        public ActionResult Delete(int id) {
            var item = repo.GetPoi_Admin(id);
            var result = repo.DeletePoi(id, Admin.Id);
            if (result > 0) {
                Notify_Delete_Success();
            } else {
                Notify_Delete_Fail();
            }
            return RedirectToAction("Index");
        }

        public ActionResult DataTable() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();
            
            if (param.hasSearch) {
                whereC.Add(" (TitleTH LIKE @searchValue OR TitleEN LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "Id", "Category", "TitleTH", "UpdateDate", "Status" };
            DataTableData<Poi> data = repo.ListPoi_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Id.ToString(),
                        x.Category.ToString(),
                        x.TitleTH,
                        x.UpdateDate.ToString("dd/MM/yyyy HH:mm"),
                        x.Status.ToString(),
                        (
                            HtmlActionButton(AdminAction.Edit, x.Id) +
                            HtmlActionButton(AdminAction.Delete, x.Id)
                        )
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        private void BindForm() {
        }
    }
}
