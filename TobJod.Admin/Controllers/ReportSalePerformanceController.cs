﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using TobJod.Admin.Models;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class ReportSalePerformanceController : BaseController {
        ReportRepository repo;

        public ReportSalePerformanceController() {
            repo = new ReportRepository();
        }

        // GET: ReportSalePerformance
        public ActionResult Index(ReportSalePerformanceViewModel model) {
            model.Category = NullUtils.cvInt(Request.Unvalidated.QueryString["Category"]);
            model.Company = NullUtils.cvInt(Request.Unvalidated.QueryString["Company"]);
            model.DateFrom = ValueUtils.GetDate(Request.Params["DateFrom"]);
            model.DateTo = ValueUtils.GetDate(Request.Params["DateTo"]);
            if (model.DateTo == DateUtils.SqlMinDate()) model.DateTo = DateTime.Today.AddDays(-1);
            if (model.DateFrom == DateUtils.SqlMinDate()) model.DateFrom = DateUtils.GetFirstDayOfMonth(model.DateTo);

            if (Request.QueryString.AllKeys.Contains("export")) {
                Export(model);
                return null;
            } else {
                ProductCategoryRepository catRepo = new ProductCategoryRepository();
                model.Categories = catRepo.ListProductCategory_SelectList_Admin();
                model.Categories.Insert(0, new SelectListItem() { Text = "All", Value = "0" });
                CompanyRepository companyRepo = new CompanyRepository();
                model.Companies = companyRepo.ListCompany_SelectList_Admin();
                model.Companies.Insert(0, new SelectListItem() { Text = "All", Value = "0" });

                var dt = repo.ListSalePerformance(model.DateFrom, model.DateTo, model.Category, model.Company);
                
                model.DataTable = dt;
            }
            return View(model);
        }
        
        // GET: ReportSalePerformance/Export
        public void Export(ReportSalePerformanceViewModel model) {
            var dt = repo.ListSalePerformance(model.DateFrom, model.DateTo, model.Category, model.Company);

            string[] intColumn = { "Total" };
            string[] doubleColumn = { "Percentage" };
            string sheetName = "Sale_Performance";
            string fileName = "SalePerformance.xlsx";

            using (ExcelPackage pck = new ExcelPackage()) {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add(sheetName);

                ws.Cells["A1"].LoadFromDataTable(dt, true);

                int sumRow = 2 + dt.Rows.Count;

                ws.Cells[sumRow, 1].Value = "Total";
                ws.Cells[sumRow, 1, sumRow, 5].Merge = true;
                ws.Cells[sumRow, 1, sumRow, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                ws.Cells[sumRow, 6].Formula = "SUM(F2:F" + (dt.Rows.Count + 1) + ")";
                ws.Cells[sumRow, 7].Formula = "SUM(G2:G" + (dt.Rows.Count + 1) + ")";

                for (int i = 2; i < dt.Rows.Count + 2; i++) {
                    ws.Cells[i, 7].Formula = String.Format("(F{0}/F{1})*100", i, sumRow);
                }
                
                //Format the header for column 1-3
                using (ExcelRange rng = ws.Cells[1, 1, 1, dt.Columns.Count]) {
                    rng.Style.Font.Bold = true;
                    rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                    rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(79, 129, 189));  //Set color to dark blue
                    rng.Style.Font.Color.SetColor(Color.White);
                }

                //Format the header for column 1-3
                using (ExcelRange rng = ws.Cells[sumRow, 1, sumRow, dt.Columns.Count]) {
                    rng.Style.Font.Bold = true;
                    rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                    rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(180, 180, 180));  //Set color to dark blue
                    rng.Style.Font.Color.SetColor(Color.White);
                }

                //Example how to Format Column 1 as numeric 
                if (intColumn != null) {
                    foreach (var item in intColumn) {
                        int c = dt.Columns[item].Ordinal + 1;
                        using (ExcelRange col = ws.Cells[2, c, 2 + dt.Rows.Count, c]) {
                            col.Style.Numberformat.Format = "#,##0";
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        }
                    }
                }
                if (doubleColumn != null) {
                    foreach (var item in doubleColumn) {
                        int c = dt.Columns[item].Ordinal + 1;
                        using (ExcelRange col = ws.Cells[2, c, 2 + dt.Rows.Count, c]) {
                            col.Style.Numberformat.Format = "#,##0.00";
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        }
                    }
                }

                ws.Cells.AutoFitColumns();

                //Write it back to the client
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=" + fileName);
                Response.BinaryWrite(pck.GetAsByteArray());
            }
            //ExportFile("SalePerformance.xlsx", "SalePerformance",  dt, null, null, null, null);
        }
        
    }
}
