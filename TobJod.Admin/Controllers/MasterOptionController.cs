﻿using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class MasterOptionController : BaseController {
        MasterOptionRepository repo;

        public MasterOptionController() {
            repo = new MasterOptionRepository();
        }

        // GET: MasterOption
        public ActionResult Index() {
            MasterOption item = new MasterOption();
            try { item.Category = (MasterOptionCategory)NullUtils.cvInt(Request.Params["category"]); } catch { }
            return View(item);
        }

        // GET: MasterOption/Create
        public ActionResult Create() {
            MasterOption item = new MasterOption();
            item.Status = Status.Active;
            return View("Form", item);
        }

        // POST: MasterOption/Create
        [HttpPost]
        public ActionResult Create(MasterOption item) {
            var search = repo.GetMasterOption_Admin(item);
            if (search != null) {
                Notify(NotifyMessageType.Error, "Add error", "Cannot add record. Duplicate data.");
                return View("Form", item);
            }
            try {
                item.RefId = 0;
                item.Mode = 0;
                item.CreateDate = item.UpdateDate = DateTime.Now;
                item.CreateAdminId = item.UpdateAdminId = Admin.Id;

                repo.AddMasterOption(item);
                Notify_Add_Success();
                return RedirectToAction("Index", "MasterOption", new { category = (int)item.Category });
            } catch { }
            Notify_Add_Fail();
            return View("Form", item);
        }

        // GET: MasterOption/Edit/5
        public ActionResult Edit(int id) {
            MasterOption item = repo.GetMasterOption_Admin(id);
            return View("Form", item);
        }

        // POST: MasterOption/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, MasterOption item) {
            if (item.Category != MasterOptionCategory.PropertyType) {
                var search = repo.GetMasterOption_Admin(item);
                if (search != null) {
                    Notify(NotifyMessageType.Error, "Update error", "Cannot update record. Duplicate data.");
                    return View("Form", item);
                }
            }
            int result = 0;
            try {
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = Admin.Id;

                result = repo.UpdateMasterOption(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index", "MasterOption", new { category = (int)item.Category });
                }
            } catch { }
            Notify_Update_Fail();
            return View("Form", item);
        }

        // GET: MasterOption/Import
        public ActionResult Import() {
            CheckPermission(AdminAction.Add);
            return View();
        }

        // POST: MasterOption/Import/5
        [HttpPost]
        public ActionResult Import(HttpPostedFileBase File, MasterOptionCategory category) {
            CheckPermission(AdminAction.Add);
            List<MasterOption> items = new List<MasterOption>();
            StringBuilder sb = new StringBuilder();
            try {
                string path = System.IO.Path.GetTempPath();
                string fileName = UploadUtils.SaveFile(File, path);
                DataSet ds;
                using (var stream = System.IO.File.Open(path + fileName, FileMode.Open, FileAccess.Read)) {
                    using (var reader = ExcelReaderFactory.CreateReader(stream)) {
                        ds = reader.AsDataSet();
                    }
                }

                StringBuilder sbRow;
                using (ds) {
                    DataTable dt = ds.Tables[0];
                    DateTime now = DateTime.Now;
                    bool isFirst = true;
                    int i = 1;
                    foreach (DataRow dr in dt.Rows) {
                        if (isFirst) { isFirst = false; continue; }
                        MasterOption r;
                        sbRow = new StringBuilder();

                        if (category == MasterOptionCategory.DriverAge) {
                            r = new MasterOption() { Category = category, Code = NullUtils.cvString(dr[0]), TitleTH = NullUtils.cvString(dr[1]), TitleEN = NullUtils.cvString(dr[2]), Min = ValidateNumber(dr[3], "Min", ref sbRow), Max = ValidateNumber(dr[4], "Max", ref sbRow), RefId = 0, Mode = 0, Status = Status.Active, CreateDate = now, CreateAdminId = Admin.Id, UpdateDate = now, UpdateAdminId = Admin.Id };
                            if (r.Max < r.Min) {
                                sbRow.AppendLine("- Max < Min<br/>");
                            }
                        } else {
                            r = new MasterOption() { Category = category, Code = NullUtils.cvString(dr[0]), TitleTH = NullUtils.cvString(dr[1]), TitleEN = NullUtils.cvString(dr[2]), Min = 0, Max = 0, RefId = 0, Mode = 0, Status = Status.Active, CreateDate = now, CreateAdminId = Admin.Id, UpdateDate = now, UpdateAdminId = Admin.Id };
                        }

                        sbRow.Append(ValidateModelMessage(r));

                        if (sbRow.Length > 0) {
                            sb.Append("<tr><th>Row " + (i + 1) + "</th><td> " + sbRow + "</td></tr>");
                        } else {
                            items.Add(r);
                        }

                        i++;
                    }
                }
                
                if (sb.Length > 0) {
                    ViewBag.Message = "INVALID DATA";
                    ViewBag.ErrorMessage = sb.ToString();
                    Notify_Add_Fail();
                    return View();
                } else {
                    int result = repo.AddMasterOption(items, category == MasterOptionCategory.PropertyType);
                    if (result > 0) {
                        Notify_Add_Success();
                        return RedirectToAction("Index");
                    }
                }
            } catch { }
            Notify_Add_Fail();
            return View();
        }

        // GET: MasterOption/Delete/5
        public ActionResult Delete(int id) {
            var item = repo.GetMasterOption_Admin(id);
            bool canDelete = true;
            if (item.Category == MasterOptionCategory.FAQCategory) {
                FaqRepository faqRepo = new FaqRepository();
                var items = faqRepo.ListFaqByCategory_Admin(id);
                canDelete = items.Count == 0;
            }
            if (canDelete) {
                var result = repo.DeleteMasterOption(id, Admin.Id);
                if (result > 0) {
                    Notify_Delete_Success();
                } else {
                    Notify_Delete_Fail();
                }
            } else {
                Notify(NotifyMessageType.Error, "Delete Error", "Record is in used.");
            }
            return RedirectToAction("Index", "MasterOption", new { category = Request.QueryString["category"] });
        }

        // GET: MasterOption/DataTable
        public ActionResult DataTable() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();

            //if (NullUtils.cvInt(Request.QueryString["category"]) > 0) {
            whereC.Add(" (category=@category) ");
            whereParams.category = NullUtils.cvInt(Request.Unvalidated.Form["category"]);
            //}

            if (param.hasSearch) {
                whereC.Add(" (TitleTH LIKE @searchValue OR TitleEN LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "Id", "Sequence", "Category", "TitleTH", "UpdateDate", "Status" };
            DataTableData<MasterOption> data = repo.ListMasterOption_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Id.ToString(),
                        x.Sequence.ToString(),
                        x.Category.GetDisplayName(),
                        x.TitleTH.ToString(),
                        x.UpdateDate.ToString("dd/MM/yyyy HH:mm"),
                        x.Status.ToString(),
                        (HtmlActionButton(AdminAction.Edit, x.Id, "&category=" + ((int)x.Category)) + HtmlActionButton(AdminAction.Delete, x.Id, "&category=" + ((int)x.Category)))
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}
