﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TobJod.Admin.Models;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class CompanyController : BaseController {
        CompanyRepository repo;

        public CompanyController() {
            Path = System.Configuration.ConfigurationManager.AppSettings["Path_Company"];
            repo = new CompanyRepository();
        }
        
        // GET: Company
        public ActionResult Index() {
            return View();
        }

        // GET: Company/Create
        public ActionResult Create() {
            CompanyViewModel item = new CompanyViewModel();
            item.Status = Status.Active;
            BindForm();
            return View("Form", item);
        }

        // POST: Company/Create
        [HttpPost]
        public ActionResult Create(CompanyViewModel item) {
            try {
                item.CreateDate = item.UpdateDate = DateTime.Now;
                item.CreateAdminId = item.UpdateAdminId = Admin.Id;
                
                item.ImageTH = UploadUtils.SaveFile(item.ImageTHFile, PathPhy);
                item.ImageEN = UploadUtils.SaveFile(item.ImageENFile, PathPhy);

                repo.AddCompany(item);
                Notify_Add_Success();
                return RedirectToAction("Index");
            } catch { }
            Notify_Add_Fail();
            BindForm();
            return View("Form", item);
        }

        // GET: Company/Edit/5
        public ActionResult Edit(int id) {
            Company model = repo.GetCompany_Admin(id);
            CompanyViewModel item = CompanyViewModel.Map(model);
            BindForm();
            return View("Form", item);
        }

        // POST: Company/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, CompanyViewModel item) {
            int result = 0;
            try {
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = Admin.Id;
                
                item.ImageTH = UploadUtils.SaveAndReplaceFile(item.ImageTHFile, PathPhy, item.ImageTH);
                item.ImageEN = UploadUtils.SaveAndReplaceFile(item.ImageENFile, PathPhy, item.ImageEN);

                result = repo.UpdateCompany(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            BindForm();
            return View("Form", item);
        }

        // GET: Company/Delete/5
        public ActionResult Delete(int id) {
            var result = repo.DeleteCompany(id, Admin.Id);
            if (result > 0) {
                Notify_Delete_Success();
            } else {
                Notify_Delete_Fail();
            }
            return RedirectToAction("Index");
        }

        // GET: Company/DataTable
        public ActionResult DataTable() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();
           
            if (param.hasSearch) {
                whereC.Add(" (CoCode LIKE @searchValue OR CoTitleTH LIKE @searchValue OR CoTitleEN LIKE @searchValue OR ReCode LIKE @searchValue OR ReTitleTH LIKE @searchValue OR ReTitleEN LIKE @searchValue OR AlCode LIKE @searchValue OR AlTitleTH LIKE @searchValue OR AlTitleEN LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "Id", "ReCode", "RePrefixTH", "ReTitleTH", "UpdateDate", "Status" };
            DataTableData<Company> data = repo.ListCompany_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Id.ToString(),
                        x.ReCode.ToString(),
                        x.RePrefixTH.ToString(),
                        x.ReTitleTH.ToString(),
                        x.UpdateDate.ToString("dd/MM/yyyy HH:mm"),
                        x.Status.ToString(),
                        (HtmlActionButton(AdminAction.Edit, x.Id) + HtmlActionButton(AdminAction.Delete, x.Id))
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        private void BindForm() {
            //List<SelectListItem> items = new List<SelectListItem>();
            //for (int i = 1; i <= 3; i++) {
            //    items.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });
            //}
            //ViewBag.ZoneList = items;
        }

    }
}
