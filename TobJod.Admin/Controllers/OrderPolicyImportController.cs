﻿using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class OrderPolicyImportController : BaseController {
        OrderRepository repo;

        public OrderPolicyImportController() {
            repo = new OrderRepository();
        }

        // GET: OrderPolicyImport
        public ActionResult Index() {
            return View();
        }

        // POST: OrderPolicyImport/Index/5
        [HttpPost]
        public ActionResult Index(HttpPostedFileBase File) {
            List<Order> list = new List<Order>();
            List<Order> listCompulsory = new List<Order>();
            StringBuilder sb = new StringBuilder();
            int count = 0;
            try {
                string path = System.IO.Path.GetTempPath();
                string fileName = UploadUtils.SaveFile(File, path);
                DataSet ds;
                using (var stream = System.IO.File.Open(path + fileName, FileMode.Open, FileAccess.Read)) {
                    using (var reader = ExcelReaderFactory.CreateReader(stream)) {
                        ds = reader.AsDataSet();
                    }
                }

                using (ds) {
                    DataTable dt = ds.Tables[0];
                    DateTime now = DateTime.Now;
                    bool isFirst = true;
                    int i = 1;
                    StringBuilder sbRow;
                    Order order;
                    foreach (DataRow dr in dt.Rows) {
                        if (isFirst) { isFirst = false; continue; }

                        sbRow = new StringBuilder();
                        order = new Order() { OrderCode = NullUtils.cvString(dr[1]), PolicyNo = NullUtils.cvString(dr[7]), UpdateDate = now, PolicyReceiveDate = GetExcelDate(dr[6].ToString()) };

                        if (order.OrderCode.Length == 0 && order.PolicyNo.Length == 0 && order.PolicyReceiveDate.Year < 2018) continue;

                        if (order.OrderCode.Length == 0) sbRow.AppendLine("- เลขที่รับแจ้ง is invalid<br/>");
                        if (order.PolicyNo.Length == 0) sbRow.AppendLine("- เลขที่กรมธรรม์ is invalid<br/>");
                        if (order.PolicyReceiveDate.Year < 2018) sbRow.AppendLine("- วันที่รับกรมธรรม์ is invalid<br/>");
                        
                        if (sbRow.Length > 0) {
                            sb.Append("<tr><th>Row " + (i + 1) + "</th><td> " + sbRow + "</td></tr>");
                        } else {
                            if (string.Equals(dr[3], "MO") && string.Equals(dr[4], "C")) {
                                listCompulsory.Add(order);
                            } else {
                                list.Add(order);
                            }

                            OrderRepository repo = new OrderRepository();
                            repo.UpdateOrder_PolicyNo(list);

                            if (listCompulsory.Count > 0) {
                                foreach (Order o in listCompulsory) {
                                    int category = repo.GetOrderProductCategoryByOrderCode_Admin(o.OrderCode);
                                    if (category == (int)ProductCategoryKey.PRB) {
                                        count += repo.UpdateOrder_PolicyNo(new List<Order>() { o });
                                    } else {
                                        count += repo.UpdateOrder_PolicyNoAdd(new List<Order>() { o });
                                    }
                                }
                            }
                        }


                        i++;
                    }
                }

                if (sb.Length > 0) {
                    ViewBag.Message = "INVALID DATA";
                    ViewBag.ErrorMessage = sb.ToString();
                    Notify_Add_Fail();
                    return View();
                } else {
                    Notify_Add_Success();
                    //ViewBag.ErrorMessage = "Total: " + count.ToString("n") + " record(s).";
                    //return RedirectToAction("Index");
                    return View();
                }
            } catch (Exception ex) {
                ViewBag.ErrorMessage = ex.ToString();
            }
            Notify_Add_Fail();
            return View();
        }

        private DateTime GetExcelDate(string val) {
            DateTime d = ValueUtils.GetDateTime(val, "dd/MM/yyyy HH:mm:ss");
            if (d.Year < 1900) {
                string dt = val;
                if (val.IndexOf(" ") > 0) {
                    dt = dt.Substring(0, val.IndexOf(" ")).Trim();
                    var dd = dt.Split('/');
                    try { d = new DateTime(NullUtils.cvInt(dd[2]), NullUtils.cvInt(dd[1]), NullUtils.cvInt(dd[0])); } catch { }
                }
            }

            return d;
        }

    }
}
