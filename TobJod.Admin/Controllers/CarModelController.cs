﻿using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class CarModelController : BaseController {
        CarRepository repo;

        public CarModelController() {
            repo = new CarRepository();
        }
        
        // GET: CarModel
        public ActionResult Index() {
            return View();
        }

        // GET: CarModel/Create
        public ActionResult Create() {
            CarModel item = new CarModel();
            item.Status = Status.Active;
            BindForm(0);
            return View("Form", item);
        }

        // POST: CarModel/Create
        [HttpPost]
        public ActionResult Create(CarModel item) {
            try {
                item.CreateDate = item.UpdateDate = DateTime.Now;
                item.CreateAdminId = item.UpdateAdminId = Admin.Id;

                repo.AddCarModel(item);
                Notify_Add_Success();
                return RedirectToAction("Index");
            } catch { }
            Notify_Add_Fail();
            BindForm(item.FamilyId);
            return View("Form", item);
        }

        // GET: CarModel/Edit/5
        public ActionResult Edit(int id) {
            CarModel item = repo.GetCarModel_Admin(id);
            BindForm(item.FamilyId);
            return View("Form", item);
        }

        // POST: CarModel/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, CarModel item) {
            int result = 0;
            try {
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = Admin.Id;

                result = repo.UpdateCarModel(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            BindForm(item.FamilyId);
            return View("Form", item);
        }

        // GET: CarBrand/Import
        public ActionResult Import() {
            CheckPermission(AdminAction.Add);
            return View();
        }

        // POST: CarModel/Import/5
        [HttpPost]
        public ActionResult Import(HttpPostedFileBase File) {
            CheckPermission(AdminAction.Add);
            List<CarModel> items = new List<CarModel>();
            StringBuilder sb = new StringBuilder();
            try {
                string path = System.IO.Path.GetTempPath();
                string fileName = UploadUtils.SaveFile(File, path);
                DataSet ds;
                using (var stream = System.IO.File.Open(path + fileName, FileMode.Open, FileAccess.Read)) {
                    using (var reader = ExcelReaderFactory.CreateReader(stream)) {
                        ds = reader.AsDataSet();
                    }
                }

                var t_brands = repo.ListCarBrand_Dictionary_Admin();
                var t_families = repo.ListCarFamily_Dictionary_Admin();
                var brands = new Dictionary<string, int>(t_brands, StringComparer.OrdinalIgnoreCase);
                var families = new Dictionary<string, int>(t_families, StringComparer.OrdinalIgnoreCase);

                StringBuilder sbRow;
                using (ds) {
                    DataTable dt = ds.Tables[0];
                    DateTime now = DateTime.Now;
                    bool isFirst = true;
                    int i = 1;
                    foreach (DataRow dr in dt.Rows) {
                        if (isFirst) { isFirst = false; continue; }

                        sbRow = new StringBuilder();

                        var r = new CarModel() { BrandId = 0, FamilyId = 0, TitleTH = NullUtils.cvString(dr[3]), TitleEN = NullUtils.cvString(dr[3]), MapName = NullUtils.cvString(dr[0]), Spec = NullUtils.cvString(dr[3]), CC = ValidateNumber(dr[4], "CC", ref sbRow), KG = ValidateNumber(dr[5], "KB", ref sbRow), Seat = ValidateNumber(dr[6], "Seat", ref sbRow), Door = ValidateNumber(dr[7], "Door", ref sbRow), Year = ValidateNumber(dr[8], "Year", ref sbRow), Status = Status.Active, CreateDate = now, CreateAdminId = Admin.Id, UpdateDate = now, UpdateAdminId = Admin.Id };


                        sbRow.Append(ValidateModelMessage(r));

                        var brand = NullUtils.cvString(dr[1]);
                        var family = NullUtils.cvString(dr[2]);
                        if (brands.ContainsKey(brand)) {
                            r.BrandId = brands[brand];

                            if (families.ContainsKey(r.BrandId + "|" + family)) {
                                r.FamilyId = families[r.BrandId + "|" + family];
                            } else {
                                CarFamily fam = new CarFamily();
                                fam.BrandId = r.BrandId;
                                fam.MapName = fam.TitleTH = fam.TitleEN = family;
                                fam.Status = Status.Active;
                                fam.CreateDate = fam.UpdateDate = now;
                                fam.CreateAdminId = fam.UpdateAdminId = Admin.Id;

                                if (repo.AddCarFamily(fam) > 0) {
                                    t_families = repo.ListCarFamily_Dictionary_Admin();
                                    families = new Dictionary<string, int>(t_families, StringComparer.OrdinalIgnoreCase);

                                    r.FamilyId = families[r.BrandId + "|" + family];
                                } else {
                                    sbRow.AppendLine("- Family \"" + family + "\" Not found.<br/>");
                                }
                            }

                        } else {
                            sbRow.AppendLine("- Brand \"" + brand + "\" Not found.<br/>");
                        }

                        if (sbRow.Length > 0) {
                            sb.Append("<tr><th>Row " + (i + 1) + "</th><td> " + sbRow + "</td></tr>");
                        } else {
                            items.Add(r);
                        }

                        i++;
                    }
                }

                if (sb.Length > 0) {
                    ViewBag.Message = "INVALID DATA";
                    ViewBag.ErrorMessage = sb.ToString();
                    Notify_Add_Fail();
                    return View();
                } else {
                    int result = repo.AddCarModel(items);
                    if (result > 0) {
                        Notify_Add_Success();
                        return RedirectToAction("Index");
                    }
                }
            } catch { }
            Notify_Add_Fail();
            return View();
        }

        // GET: CarModel/Delete/5
        public ActionResult Delete(int id) {
            var result = repo.DeleteCarModel(id, Admin.Id);
            if (result > 0) {
                Notify_Delete_Success();
            } else {
                Notify_Delete_Fail();
            }
            return RedirectToAction("Index");
        }

        // GET: CarModel/DataTable
        public ActionResult DataTable() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();
           
            if (param.hasSearch) {
                whereC.Add(" (TitleTH LIKE @searchValue OR TitleEN LIKE @searchValue OR Brand LIKE @searchValue OR Family LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "Id", "Brand", "Family", "TitleTH", "UpdateDate", "Status" };
            DataTableData<CarModel> data = repo.ListCarModel_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Id.ToString(),
                        x.Brand.ToString(),
                        x.Family.ToString(),
                        x.TitleTH.ToString(),
                        x.UpdateDate.ToString("dd/MM/yyyy HH:mm"),
                        x.Status.ToString(),
                        (HtmlActionButton(AdminAction.Edit, x.Id) + HtmlActionButton(AdminAction.Delete, x.Id))
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        // GET: CarModel/SelectList_Family
        public ActionResult SelectList_Family(int brandId) {
            var data = repo.ListCarFamily_SelectList_Admin(brandId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        private void BindForm(int familyId) {
            ViewBag.BrandList = repo.ListCarBrand_SelectList_Admin();
            CarFamily family = null;
            if (familyId > 0) {
                family = repo.GetCarFamily_Admin(familyId);
            }
            if (family != null) {
                ViewBag.FamilyList = repo.ListCarFamily_SelectList_Admin(family.BrandId);
            } else {
                ViewBag.FamilyList = new List<SelectListItem>();
            }
        }

    }
}
