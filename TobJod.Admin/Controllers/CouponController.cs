﻿using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class CouponController : BaseController {
        CouponRepository repo;

        public CouponController() {
            repo = new CouponRepository();
        }
        
        // GET: Coupon
        public ActionResult Index() {
            return View();
        }

        // GET: Coupon/Create
        public ActionResult Create() {
            Coupon item = new Coupon();
            item.Status = Status.Active;
            item.CouponProducts = new List<CouponProduct>();
            item.CouponProductCategories = new List<CouponProductCategory>();
            item.CouponCompanies = new List<CouponCompany>();
            BindForm();
            return View("Form", item);
        }

        // POST: Coupon/Create
        [HttpPost]
        public ActionResult Create(Coupon item) {
            try {
                item.CreateDate = item.UpdateDate = DateTime.Now;
                item.CreateAdminId = item.UpdateAdminId = Admin.Id;

                if (item.ProductType == CouponProductType.ByProduct && !string.IsNullOrEmpty(Request.Form["product_id"])) {
                    string[] products = Request.Form["product_id"].Split(',');
                    item.CouponProducts = new List<CouponProduct>();
                    foreach (var p in products) {
                        item.CouponProducts.Add(new CouponProduct() { ProductId = int.Parse(p) });
                    }
                }

                if (item.ProductType == CouponProductType.ByCategory) {
                    var values = Request.Form["CouponProductCategories"].Split(',');
                    item.CouponProductCategories = new List<CouponProductCategory>();
                    foreach (var it in values) {
                        if (!string.Equals(it, "false")) {
                            item.CouponProductCategories.Add(new CouponProductCategory() { ProductCategoryId = int.Parse(it) });
                        }
                    }
                } else {
                    item.CouponProductCategories = null;
                }
                if (item.ProductType == CouponProductType.ByCompany) {
                    var values = Request.Form["CouponCompanies"].Split(',');
                    item.CouponCompanies = new List<CouponCompany>();
                    foreach (var it in values) {
                        if (!string.Equals(it, "false")) {
                            item.CouponCompanies.Add(new CouponCompany() { CompanyId = int.Parse(it) });
                        }
                    }
                } else {
                    item.CouponCompanies = null;
                }

                int id = repo.AddCoupon(item);

                if (id > 0) {
                    List<CouponCode> couponCodes = new List<CouponCode>();
                    var count = 0;
                    if (item.CouponType == CouponType.OneTime) {
                        List<string> codes = new List<string>();
                        int length = (10 - item.Code.Length);
                        if (length < item.Total.ToString().Length) length = item.Total.ToString().Length;
                        Random random = new Random();

                        while (count < item.Total) {
                            couponCodes.AddRange(GenerateCode(random, length, item.Total, item.Id, item.Code, ref codes));
                            count += repo.AddCouponCode(couponCodes);

                        }
                    } else if (item.CouponType == CouponType.Mass) {
                        couponCodes.Add(new CouponCode() { CouponId = item.Id, Code = item.Code, Used = false });
                        count += repo.AddCouponCode(couponCodes);
                    }
                }


                Notify_Add_Success();
                return RedirectToAction("Index");
            } catch { }
            Notify_Add_Fail();
            BindForm();
            return View("Form", item);
        }

        // GET: Coupon/Edit/5
        public ActionResult Edit(int id) {
            Coupon item = repo.GetCoupon_Admin(id);
            BindForm();
            return View("Form", item);
        }

        // POST: Coupon/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Coupon item) {
            int result = 0;
            try {
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = Admin.Id;

                result = repo.UpdateCoupon(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            BindForm();
            return View("Form", item);
        }
        
        // GET: Coupon/Delete/5
        public ActionResult Delete(int id) {
            var result = repo.DeleteCoupon(id, Admin.Id);
            if (result > 0) {
                Notify_Delete_Success();
            } else {
                Notify_Delete_Fail();
            }
            return RedirectToAction("Index");
        }

        // GET: Coupon/Code
        public ActionResult Code(int id) {
            if (string.Equals(Request.Params["export"], "1")) {
                Code_Export(id);
            }
            var item = repo.GetCoupon_Admin(id);
            return View("Code", item);
        }
        
        public void Code_Export(int id) {
            var item = repo.GetCoupon_Admin(id);
            DataTable dt;
            if (item.CouponType == CouponType.Mass) {
                dt = repo.GetCouponCodeMass_AdminExport(id);
            } else {
                dt = repo.GetCouponCodeOnce_AdminExport(id);
            }
            ExportFile("CouponCode.xlsx", "Code", dt, null, null, null, new String[] { "Use_Date" });
        }

        // GET: Coupon/DataTable
        public ActionResult DataTable() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();
           
            if (param.hasSearch) {
                whereC.Add(" (TitleTH LIKE @searchValue OR TitleEN LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "Id", "TitleTH", "CouponType", "ActiveDate", "ExpireDate", "Total", "UpdateDate", "Status", "TotalUsed" };
            DataTableData<Coupon> data = repo.ListCoupon_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Id.ToString(),
                        x.TitleTH.ToString(),
                        x.CouponType.GetDisplayName(),
                        x.ActiveDate.ToString("dd/MM/yyyy"),
                        x.ExpireDate.ToString("dd/MM/yyyy"),
                        (x.TotalUsed.ToString("#,##0") + "/" + x.Total.ToString("#,##0")),
                        x.UpdateDate.ToString("dd/MM/yyyy HH:mm"),
                        x.Status.ToString(),
                        (HtmlActionButtonBuilder(true, Url.Action("Code") + "?id=" + x.Id, "btn btn-xs btn-info btn-action", "Code", "fa fa-th-list", "", false) + HtmlActionButton(AdminAction.Edit, x.Id) + HtmlActionButton(AdminAction.Delete, x.Id))
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        // GET: Coupon/DataTable_Code
        public ActionResult DataTable_Code() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();

            int couponId = NullUtils.cvInt(Request.Params["coupon_id"]);
            var item = repo.GetCoupon_Admin(couponId);

            whereC.Add(" (CouponId=@CouponId) ");
            whereParams.CouponId = couponId;

            if (param.hasSearch) {
                whereC.Add(" (CouponCode LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "CouponCode", "CreateDate" };
            DataTableData<CouponCodeLog> data;
            if (item.CouponType == CouponType.OneTime) {
                data = repo.ListCouponCodeOnce_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            } else {
                data = repo.ListCouponCodeMass_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            }
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.CouponCode.ToString(),
                        (x.CreateDate == DateTime.MinValue ? "" : x.CreateDate.ToString("dd/MM/yyyy HH:mm:ss")),
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        // GET: Promotion/DataTableProduct
        public ActionResult DataTableProduct() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();

            if (param.hasSearch) {
                whereC.Add(" (TitleTH LIKE @searchValue OR TitleEN LIKE @searchValue OR Category LIKE @searchValue OR SubCategory LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            whereC.Add(" (Status=" + (int)Status.Active + " AND ApproveStatus=" + (int)ApproveStatus.Approve + ") ");

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            ProductRepository productRepo = new ProductRepository();

            string[] columns = new string[] { "Id", "Category", "SubCategory", "TitleTH", "Status" };
            DataTableData<Product> data = productRepo.ListProduct_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Id.ToString(),
                        (x.Category == null ? "พ.ร.บ." : x.Category.ToString()),
                        x.SubCategory.ToString(),
                        x.TitleTH.ToString(),
                        x.Status.ToString(),
                        "<button type=\"button\" class=\"btn btn-xs btn-success\" title=\"Select\" data-id=\"" + x.Id.ToString() + "\" data-title=\"" + x.TitleTH.ToString() + "\"><i class=\"fa fa-check\"></i></a>"
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        private void BindForm() {
            ProductCategoryRepository productCategoryRepo = new ProductCategoryRepository();
            List<SelectListItem> items = productCategoryRepo.ListProductCategory_SelectList_Admin();
            ViewBag.ProductCategoryList = items;
            CompanyRepository companyRepo = new CompanyRepository();
            List<SelectListItem> companies = companyRepo.ListCompany_SelectList_Admin();
            ViewBag.CompanyList = companies;
        }

        private List<CouponCode> GenerateCode(Random random, int length, int total, int id, string prefix, ref List<string> codes) {
            List<CouponCode> couponCodes = new List<CouponCode>();
            while (couponCodes.Count < total) {
                string code = GenerateRandom(random, length);
                if (!codes.Contains(code)) {
                    codes.Add(code);
                    couponCodes.Add(new CouponCode() { CouponId = id, Code = prefix + code, Used = false });
                }
            }
            return couponCodes;
        }

        private String GenerateRandom(Random random, int length) {
            String characters = "123456789ABCDEFGHIJKLMNPQRSTUVWXYZ123456789";
            int iLength = characters.Length;
            char c;
            String sRandomResult = "";
            for (int i = 0; i < length; i++) {
                c = characters[random.Next(iLength)];
                sRandomResult += c.ToString();
            }
            return (sRandomResult);
        }
    }
}
