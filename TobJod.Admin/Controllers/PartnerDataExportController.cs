﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TobJod.Admin.Models;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Service;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class PartnerDataExportController : BaseController {
        ReportRepository repo;

        public PartnerDataExportController() {
            repo = new ReportRepository();
        }

        // GET: PartnerDataExport
        public ActionResult Index(PartnerDataExportViewModel model) {
            model.Status = NullUtils.cvInt(Request.Unvalidated.QueryString["Status"]);
            model.Company = NullUtils.cvInt(Request.Unvalidated.QueryString["Company"]);
            model.DateFrom = ValueUtils.GetDateTime(Request.Unvalidated.QueryString["DateFrom"] + " " + NullUtils.cvInt(Request.Unvalidated.QueryString["DateFromH"]).ToString("00") + ":" + NullUtils.cvInt(Request.Unvalidated.QueryString["DateFromM"]).ToString("00"));
            model.DateTo = ValueUtils.GetDateTime(Request.Unvalidated.QueryString["DateTo"] + " " + NullUtils.cvInt(Request.Unvalidated.QueryString["DateToH"]).ToString("00") + ":" + NullUtils.cvInt(Request.Unvalidated.QueryString["DateToM"]).ToString("00"));
            if (model.DateFrom == DateUtils.SqlMinDate()) model.DateFrom = DateTime.Today;
            if (model.DateTo == DateUtils.SqlMinDate()) model.DateTo = model.DateFrom;

            model.StatusList = new List<SelectListItem>();
            model.StatusList.Insert(0, new SelectListItem() { Text = "All", Value = "-1" });
            var status = EnumExtensions.EnumDictionary<PartnerAPIStatus>();
            foreach (var item in status) {
                model.StatusList.Add(new SelectListItem() { Text = item.Value, Value = item.Key.ToString() });
            }
            CompanyRepository companyRepo = new CompanyRepository();
            model.Companies = companyRepo.ListCompany_SelectList_Admin();
            //model.Companies.Insert(0, new SelectListItem() { Text = "All", Value = "0" });

            var dt = repo.ListPartnerData(model.DateFrom, model.DateTo, model.Company, model.Status);

            model.DataTable = dt;

            return View(model);
        }

        // GET: PartnerDataExport/Export
        public void Export(PartnerDataExportViewModel model) {
            model.Status = NullUtils.cvInt(Request.Unvalidated.QueryString["Status"]);
            model.Company = NullUtils.cvInt(Request.Unvalidated.QueryString["Company"]);
            model.DateFrom = ValueUtils.GetDateTime(Request.Unvalidated.QueryString["DateFrom"] + " " + NullUtils.cvInt(Request.Unvalidated.QueryString["DateFromH"]).ToString("00") + ":" + NullUtils.cvInt(Request.Unvalidated.QueryString["DateFromM"]).ToString("00"));
            model.DateTo = ValueUtils.GetDateTime(Request.Unvalidated.QueryString["DateTo"] + " " + NullUtils.cvInt(Request.Unvalidated.QueryString["DateToH"]).ToString("00") + ":" + NullUtils.cvInt(Request.Unvalidated.QueryString["DateToM"]).ToString("00"));

            List<int> dt = repo.ListPartnerDataExport(model.DateFrom, model.DateTo, model.Company, model.Category, model.Status);
            if (model.Category == 100) {
                dt.AddRange(repo.ListPartnerDataExport(model.DateFrom, model.DateTo, model.Company, 200, model.Status));
            }
            var result = new PartnerAPIService().GenerateExcel(dt, true);
            if (result.Item1.Success && result.Item2 != null) {
                var excel = result.Item2.FirstOrDefault();
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=application_" + excel.Key.ToString() + ".xlsx");
                Response.BinaryWrite(excel.Value);
                Response.End();
            }
        }
        
    }
}
