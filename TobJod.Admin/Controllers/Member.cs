﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TobJod.Admin.Controllers {
    public class Member {
        public int Id { get; set; }
        public string Email { get; set; }
        public string EncryptedKey { get; set; } //Do not use password***

    }
}