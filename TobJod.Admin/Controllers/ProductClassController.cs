﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class ProductClassController : BaseController {
        ProductSubClassRepository repo;

        public ProductClassController() {
            repo = new ProductSubClassRepository();
        }

        // GET: ProductSubClass
        public ActionResult Index() {
            return View();
        }

        // GET: ProductSubClass/Create
        public ActionResult Create() {
            ProductSubClass item = new ProductSubClass();
            item.Status = Status.Active;
            BindForm();
            return View("Form", item);
        }

        // POST: ProductSubClass/Create
        [HttpPost]
        public ActionResult Create(ProductSubClass item) {
            try {
                item.CreateDate = item.UpdateDate = DateTime.Now;
                item.CreateAdminId = item.UpdateAdminId = Admin.Id;
                
                repo.AddProductSubClass(item);
                Notify_Add_Success();
                return RedirectToAction("Index");
            } catch { }
            Notify_Add_Fail();
            BindForm();
            return View("Form", item);
        }

        // GET: ProductSubClass/Edit/5
        public ActionResult Edit(int id) {
            ProductSubClass item = repo.GetProductSubClass_Admin(id);
            BindForm();
            return View("Form", item);
        }

        // POST: ProductSubClass/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, ProductSubClass item) {
            int result = 0;
            try {
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = Admin.Id;
                
                result = repo.UpdateProductSubClass(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            BindForm();
            return View("Form", item);
        }

        // GET: ProductSubClass/Delete/5
        public ActionResult Delete(int id) {
            var result = repo.DeleteProductSubClass(id, Admin.Id);
            if (result > 0) {
                Notify_Delete_Success();
            } else {
                Notify_Delete_Fail();
            }
            return RedirectToAction("Index");
        }

        // GET: ProductSubClass/DataTable
        public ActionResult DataTable() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();

            if (param.hasSearch) {
                whereC.Add(" (Class LIKE @searchValue OR SubClass LIKE @SearchValue OR TitleTH LIKE @searchValue OR TitleEN LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "Id", "CategoryId", "Class", "SubClass", "TitleTH", "UpdateDate", "Status" };
            DataTableData<ProductSubClass> data = repo.ListProductSubClass_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Id.ToString(),
                        x.CategoryId.ToString(),
                        x.Class.ToString(),
                        x.SubClass.ToString(),
                        x.TitleTH.ToString(),
                        x.UpdateDate.ToString("dd/MM/yyyy HH:mm"),
                        x.Status.ToString(),
                        (HtmlActionButton(AdminAction.Edit, x.Id) + HtmlActionButton(AdminAction.Delete, x.Id))
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        private void BindForm() {
        }

    }
}
