﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TobJod.Admin.Models;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class ClaimProcessController : BaseController {
        ConfigPageRepository repo;
        ConfigPageName id = ConfigPageName.ClaimProcess;
        ConfigPageName draft_id = ConfigPageName.ClaimProcess_Draft;

        public ClaimProcessController() {
            Path = System.Configuration.ConfigurationManager.AppSettings["Path_ConfigPage"];
            repo = new ConfigPageRepository();
        }
        
        // GET: ConfigPage
        public ActionResult Index() {
            ConfigPage model = repo.GetConfigPage_Admin(draft_id);
            ConfigPageViewModel item = ConfigPageViewModel.Map(model);
            item.CurrentPage = repo.GetConfigPage_Admin(id);
            item.CurrentPage.BodyTH = ReplaceTextEditorPathDisplay(item.CurrentPage.BodyTH);
            item.CurrentPage.BodyEN = ReplaceTextEditorPathDisplay(item.CurrentPage.BodyEN);
            item.OGImageTHSame = (string.IsNullOrEmpty(item.OGImageTH));
            item.OGImageENSame = (string.IsNullOrEmpty(item.OGImageEN));
            item.BodyTH = ReplaceTextEditorPathDisplay(item.BodyTH);
            item.BodyEN = ReplaceTextEditorPathDisplay(item.BodyEN);
            return View("Index", item);
        }

        // GET: ConfigPage/Edit
        public ActionResult Edit() {
            ConfigPage model = repo.GetConfigPage_Admin(draft_id);
            ConfigPageViewModel item = ConfigPageViewModel.Map(model);
            item.OGImageTHSame = (string.IsNullOrEmpty(item.OGImageTH));
            item.OGImageENSame = (string.IsNullOrEmpty(item.OGImageEN));
            item.BodyTH = ReplaceTextEditorPathDisplay(item.BodyTH);
            item.BodyEN = ReplaceTextEditorPathDisplay(item.BodyEN);
            return View("Form", item);
        }

        // POST: ConfigPage/Edit/5
        [HttpPost]
        public ActionResult Edit(ConfigPageViewModel item) {
            int result = 0;
            try {
                item.Id = draft_id;
                item.BodyTH = ReplaceTextEditorPath(item.BodyTH);
                item.BodyEN = ReplaceTextEditorPath(item.BodyEN);
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = Admin.Id;
                item.ApproveStatus = ApproveStatus.Pending;
                item.OGImageTH = item.OGImageTHSame ? "" : UploadUtils.SaveAndReplaceFile(item.OGImageTHFile, PathPhy, item.OGImageTH);
                item.OGImageEN = item.OGImageENSame ? "" : UploadUtils.SaveAndReplaceFile(item.OGImageENFile, PathPhy, item.OGImageEN);
                item.Data1 = item.Data2 = item.Data3 = "";
                item.ApproveStatus = ApproveStatus.Pending;
                item.ApproveAdminId = "";

                result = repo.UpdateConfigPage(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            return View("Form", item);
        }

        // GET: ConfigPage/Views
        public ActionResult Views() {
            ConfigPage model = repo.GetConfigPage_Admin(draft_id);
            ConfigPageViewModel item = ConfigPageViewModel.Map(model);
            item.OGImageTHSame = (string.IsNullOrEmpty(item.OGImageTH));
            item.OGImageENSame = (string.IsNullOrEmpty(item.OGImageEN));
            item.BodyTH = ReplaceTextEditorPathDisplay(item.BodyTH);
            item.BodyEN = ReplaceTextEditorPathDisplay(item.BodyEN);
            return View("View", item);
        }

        //// GET: ConfigPage/Approve
        //public ActionResult Approve() {
        //    ConfigPage model = repo.GetConfigPage_Admin(draft_id);
        //    ConfigPageViewModel item = ConfigPageViewModel.Map(model);
        //    item.OGImageTHSame = (string.IsNullOrEmpty(item.OGImageTH));
        //    item.OGImageENSame = (string.IsNullOrEmpty(item.OGImageEN));
        //    item.BodyTH = ReplaceTextEditorPathDisplay(item.BodyTH);
        //    item.BodyEN = ReplaceTextEditorPathDisplay(item.BodyEN);
        //    return View("View", item);
        //}

        // POST: ConfigPage/Approve
        [HttpPost]
        public ActionResult Approve(ConfigPageViewModel item) {
            int result = 0;
            try {
                item = ConfigPageViewModel.Map(repo.GetConfigPage_Admin(draft_id));
                item.ApproveStatus = (ApproveStatus)Enum.Parse(typeof(ApproveStatus), Request.Form["Approve"].ToString());
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = item.ApproveAdminId = Admin.Id; // = item.OwnerAdminId

                result = repo.UpdateConfigPageApproveStatus(item, (int)draft_id);
                if (result > 0) {
                    if (item.ApproveStatus == ApproveStatus.Approve) {
                        repo.UpdateConfigPageCopy((int)draft_id, (int)id, Admin.Id);
                    }
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            return View("View", item);
        }
    }
}
