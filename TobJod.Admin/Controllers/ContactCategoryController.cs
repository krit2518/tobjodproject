﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class ContactCategoryController : BaseController {
        ContactCategoryRepository repo;

        public ContactCategoryController() {
            repo = new ContactCategoryRepository();
        }
        
        // GET: ContactCategory
        public ActionResult Index() {
            //ContactCategory item = new ContactCategory();
            //item.Category = NullUtils.cvInt(Request.QueryString["category"]);
            return View();
        }

        // GET: ContactCategory/Create
        public ActionResult Create() {
            ContactCategory item = new ContactCategory();
            item.Status = Status.Active;
            return View("Form", item);
        }

        // POST: ContactCategory/Create
        [HttpPost]
        public ActionResult Create(ContactCategory item) {
            try {
                item.CreateDate = item.UpdateDate = DateTime.Now;
                item.CreateAdminId = item.UpdateAdminId = Admin.Id;

                repo.AddContactCategory(item);
                Notify_Add_Success();
                return RedirectToAction("Index");
            } catch { }
            Notify_Add_Fail();
            return View("Form", item);
        }

        // GET: ContactCategory/Edit/5
        public ActionResult Edit(int id) {
            ContactCategory item = repo.GetContactCategory_Admin(id);
            return View("Form", item);
        }

        // POST: ContactCategory/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, ContactCategory item) {
            int result = 0;
            try {
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = Admin.Id;

                result = repo.UpdateContactCategory(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            return View("Form", item);
        }

        // GET: ContactCategory/Delete/5
        public ActionResult Delete(int id) {
            ContactSubCategoryRepository subRepo = new ContactSubCategoryRepository();
            var items = subRepo.ListContactSubCategory_SelectList_Admin(id);
            if (items.Count == 0) {
                var result = repo.DeleteContactCategory(id, Admin.Id);
                if (result > 0) {
                    Notify_Delete_Success();
                } else {
                    Notify_Delete_Fail();
                }
            } else {
                Notify(NotifyMessageType.Error, "Delete Error", "Record is in used.");
            }
            return RedirectToAction("Index");
        }

        // GET: ContactCategory/DataTable
        public ActionResult DataTable() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();
            
            if (param.hasSearch) {
                whereC.Add(" (TitleTH LIKE @searchValue OR TitleEN LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "Id", "Sequence", "TitleTH", "UpdateDate", "Status" };
            DataTableData<ContactCategory> data = repo.ListContactCategory_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Id.ToString(),
                        x.Sequence.ToString(),
                        x.TitleTH.ToString(),
                        x.UpdateDate.ToString("dd/MM/yyyy HH:mm"),
                        x.Status.ToString(),
                        (HtmlActionButton(AdminAction.Edit, x.Id) + HtmlActionButton(AdminAction.Delete, x.Id))
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}
