﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TobJod.Admin.Models;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class BannerController : BaseController {
        BannerRepository repo;

        public BannerController() {
            Path = System.Configuration.ConfigurationManager.AppSettings["Path_Banner"];
            repo = new BannerRepository();
        }

        
        // GET: Banner
        public ActionResult Index() {
            DateTime dateFrom = ValueUtils.GetDate(Request.Unvalidated.QueryString["DateFrom"]);
            DateTime dateTo = ValueUtils.GetDate(Request.Unvalidated.QueryString["DateTo"]);
            if (dateFrom > DateUtils.SqlMinDate()) ViewBag.DateFrom = dateFrom.ToString("dd/MM/yyyy", cultureGB);
            if (dateTo > DateUtils.SqlMinDate()) ViewBag.DateTo = dateTo.ToString("dd/MM/yyyy", cultureGB);
            return View();
        }

        // GET: Banner/Create
        public ActionResult Create() {
            BannerViewModel item = new BannerViewModel();
            item.Status = Status.Active;
            return View("Form", item);
        }

        // POST: Banner/Create
        [HttpPost]
        public ActionResult Create(BannerViewModel item) {
            if (UploadUtils.IsImage(item.ImageTHFile, item.ImageENFile, item.ImageMobileTHFile, item.ImageMobileENFile)) {
                try {
                    item.CreateDate = item.UpdateDate = DateTime.Now;
                    item.CreateAdminId = item.UpdateAdminId = item.OwnerAdminId = Admin.Id;
                    item.ApproveAdminId = "";
                    item.ApproveStatus = ApproveStatus.Pending;
                    item.ImageTH = UploadUtils.SaveFile(item.ImageTHFile, PathPhy);
                    item.ImageEN = UploadUtils.SaveFile(item.ImageENFile, PathPhy);
                    item.ImageMobileTH = UploadUtils.SaveFile(item.ImageMobileTHFile, PathPhy);
                    item.ImageMobileEN = UploadUtils.SaveFile(item.ImageMobileENFile, PathPhy);

                    repo.AddBanner(item);
                    Notify_Add_Success();
                    return RedirectToAction("Index");
                } catch { }
            }
            Notify_Add_Fail();
            return View("Form", item);
        }

        // GET: Banner/Edit/5
        public ActionResult Edit(int id) {
            Banner model = repo.GetBanner_Admin(id);
            BannerViewModel item = BannerViewModel.Map(model);
            return View("Form", item);
        }

        // POST: Banner/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, BannerViewModel item) {
            int result = 0;
            if (UploadUtils.IsImage(item.ImageTHFile, item.ImageENFile, item.ImageMobileTHFile, item.ImageMobileENFile)) {
                try {

                    item.UpdateDate = DateTime.Now;
                    item.UpdateAdminId = Admin.Id; // = item.OwnerAdminId
                    item.ImageTH = UploadUtils.SaveAndReplaceFile(item.ImageTHFile, PathPhy, item.ImageTH);
                    item.ImageEN = UploadUtils.SaveAndReplaceFile(item.ImageENFile, PathPhy, item.ImageEN);
                    item.ImageMobileTH = UploadUtils.SaveAndReplaceFile(item.ImageMobileTHFile, PathPhy, item.ImageMobileTH);
                    item.ImageMobileEN = UploadUtils.SaveAndReplaceFile(item.ImageMobileENFile, PathPhy, item.ImageMobileEN);

                    result = repo.UpdateBanner(item);
                    if (result > 0) {
                        Notify_Update_Success();
                        return RedirectToAction("Index");
                    }
                } catch { }
            }
            Notify_Update_Fail();
            return View("Form", item);
        }

        // GET: Banner/View/5
        public ActionResult View(int id) {
            Banner model = repo.GetBanner_Admin(id);
            BannerViewModel item = BannerViewModel.Map(model);
            return View("View", item);
        }

        // GET: Banner/Approve/5
        public ActionResult Approve(int id) {
            Banner x = repo.GetBanner_Admin(id);
            BannerViewModel item = BannerViewModel.Map(x);
            return View("View", item);
        }

        // POST: Banner/Approve/5
        [HttpPost]
        public ActionResult Approve(int id, BannerViewModel item) {
            int result = 0;
            try {
                item.ApproveStatus = (ApproveStatus)Enum.Parse(typeof(ApproveStatus), Request.Form["Approve"].ToString());
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = item.ApproveAdminId = Admin.Id; // = item.OwnerAdminId

                result = repo.UpdateBannerApproveStatus(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            return View("View", item);
        }

        // GET: Banner/Assign/5
        public ActionResult Assign(int id) {
            Banner model = repo.GetBanner_Admin(id);
            AdminRepository adminRepo = new AdminRepository();
            ViewBag.OwnerAdminList = adminRepo.ListAdminByPermission(Menu.Module, AdminAction.Edit).Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() }).ToList();
            BannerViewModel item = BannerViewModel.Map(model);
            return View("View", item);
        }

        // POST: Banner/Assign/5
        [HttpPost]
        public ActionResult Assign(int id, BannerViewModel item) {
            int result = 0;
            try {
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = Admin.Id;

                result = repo.UpdateBannerOwner(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            return View("View", item);
        }

        // GET: Banner/Disable/5
        public ActionResult Disable(int id) {
            Banner item = new Banner();
            item.Id = id;
            item.Status = Status.Inactive;
            item.UpdateDate = DateTime.Now;
            item.UpdateAdminId = Admin.Id;
            var result = repo.UpdateBannerStatus(item);
            if (result > 0) {
                Notify_Update_Success();
            } else {
                Notify_Update_Fail();
            }
            return RedirectToAction("Index");
        }

        // GET: Banner/Delete/5
        public ActionResult Delete(int id) {
            var item = repo.GetBanner_Admin(id);
            var result = repo.DeleteBanner(id, Admin.Id);
            if (result > 0) {
                FileUtils.DeleteFiles(PathPhy, item.ImageEN, item.ImageTH, item.ImageMobileEN, item.ImageMobileTH);
                Notify_Delete_Success();
            } else {
                Notify_Delete_Fail();
            }
            return RedirectToAction("Index");
        }

        public ActionResult DataTable() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();

            if (!Menu.Can_Approve) { //see only own content if not approver/assigner
                whereC.Add(" (OwnerAdminId=@ownerAdminId) ");
                whereParams.ownerAdminId = Admin.Id;
            }

            DateTime dateFrom = ValueUtils.GetDate(Request.Unvalidated.Form["DateFrom"]);
            DateTime dateTo = ValueUtils.GetDate(Request.Unvalidated.Form["DateTo"]);
            if (dateFrom > DateUtils.SqlMinDate()) {
                whereC.Add(" (ActiveDate >= @DateFrom)");
                whereParams.DateFrom = dateFrom;
            }

            if (dateTo > DateUtils.SqlMinDate()) {
                whereC.Add(" (ActiveDate <= @DateTo)");
                whereParams.DateTo = dateTo;
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "Id", "Sequence", "ImageTH", "ActiveDate", "ExpireDate", "UpdateDate", "Status", "ApproveStatus" };
            DataTableData<Banner> data = repo.ListBanner_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Id.ToString(),
                        x.Sequence.ToString(),
                        HtmlImage(PathRel, x.ImageTH),
                        x.ActiveDate.ToString("dd/MM/yyyy"),
                        x.ExpireDate.ToString("dd/MM/yyyy"),
                        x.UpdateDate.ToString("dd/MM/yyyy HH:mm"),
                        x.Status.ToString(),
                        x.ApproveStatus.ToString(),
                        (
                            HtmlActionButton(AdminAction.View, x.Id) + 
                            HtmlActionButton(AdminAction.Edit, x.Id, "", (x.ApproveStatus == ApproveStatus.Approve)) +
                            (x.ApproveStatus == ApproveStatus.Approve ? HtmlActionButton(AdminAction.Disable, x.Id, "", (x.Status == Status.Inactive)) : HtmlActionButton(AdminAction.Delete, x.Id)) +
                            HtmlActionButton(AdminAction.Approve, x.Id, "", (x.ApproveStatus == ApproveStatus.Approve)) +
                            HtmlActionButton(AdminAction.Assign, x.Id, "", (x.ApproveStatus == ApproveStatus.Approve))
                        )
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

    }
}
