﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class PromotionController : BaseController {
        PromotionRepository repo;

        public PromotionController() {
            repo = new PromotionRepository();
        }
        
        // GET: Promotion
        public ActionResult Index() {
            DateTime dateFrom = ValueUtils.GetDate(Request.QueryString["DateFrom"]);
            DateTime dateTo = ValueUtils.GetDate(Request.QueryString["DateTo"]);
            if (dateFrom > DateUtils.SqlMinDate()) ViewBag.DateFrom = dateFrom.ToString("dd/MM/yyyy", cultureGB);
            if (dateTo > DateUtils.SqlMinDate()) ViewBag.DateTo = dateTo.ToString("dd/MM/yyyy", cultureGB);
            return View();
        }

        // GET: Promotion/Create
        public ActionResult Create() {
            Promotion item = new Promotion();
            //item.PromotionType = PromotionType.Gift;
            item.PromotionGifts = new List<PromotionGift>() { new PromotionGift() };
            item.PromotionProducts = new List<PromotionProduct>();
            item.PromotionProductCategories = new List<PromotionProductCategory>();
            item.PromotionCompanies = new List<PromotionCompany>();
            item.Status = Status.Active;
            BindForm();
            return View("Form", item);
        }

        // POST: Promotion/Create
        [HttpPost]
        public ActionResult Create(Promotion item) {
            try {
                item.BodyTH = ReplaceTextEditorPath(item.BodyTH);
                item.BodyEN = ReplaceTextEditorPath(item.BodyEN);
                item.CreateDate = item.UpdateDate = DateTime.Now;
                item.CreateAdminId = item.UpdateAdminId = item.OwnerAdminId = Admin.Id;
                item.ApproveAdminId = "";
                item.ApproveStatus = ApproveStatus.Pending;
                
                item.Code = "";

                if (item.PromotionType == PromotionType.Gift) {
                    item.DiscountType = 0; // PromotionDiscountType.Amount;
                    item.DiscountAmount = 0;
                //} else if (item.PromotionType == PromotionType.Discount) {
                //    item.PromotionGifts = new List<PromotionGift>();
                }
                
                if (item.ProductType == PromotionProductType.ByProduct && !string.IsNullOrEmpty(Request.Form["product_id"])) {
                    string[] products = Request.Form["product_id"].Split(',');
                    item.PromotionProducts = new List<PromotionProduct>();
                    foreach (var p in products) {
                        item.PromotionProducts.Add(new PromotionProduct() { ProductId = int.Parse(p) });
                    }
                }
                
                if (item.ProductType == PromotionProductType.ByCategory) {
                    var values = Request.Form["PromotionProductCategories"].Split(',');
                    item.PromotionProductCategories = new List<PromotionProductCategory>();
                    foreach (var it in values) {
                        if (!string.Equals(it, "false")) {
                            item.PromotionProductCategories.Add(new PromotionProductCategory() { ProductCategoryId = int.Parse(it) });
                        }
                    }
                } else {
                    item.PromotionProductCategories = null;
                }
                if (item.ProductType == PromotionProductType.ByCompany) {
                    var values = Request.Form["PromotionCompanies"].Split(',');
                    item.PromotionCompanies = new List<PromotionCompany>();
                    foreach (var it in values) {
                        if (!string.Equals(it, "false")) {
                            item.PromotionCompanies.Add(new PromotionCompany() { CompanyId = int.Parse(it) });
                        }
                    }
                } else {
                    item.PromotionCompanies = null;
                }

                int id = repo.AddPromotion(item);
                Notify_Add_Success();
                return RedirectToAction("Index");
            } catch { }
            Notify_Add_Fail();
            BindForm();
            return View("Form", item);
        }

        // GET: Promotion/Edit/5
        public ActionResult Edit(int id) {
            Promotion item = repo.GetPromotion_Admin(id);
            item.BodyTH = ReplaceTextEditorPathDisplay(item.BodyTH);
            item.BodyEN = ReplaceTextEditorPathDisplay(item.BodyEN);
            BindForm();
            return View("Form", item);
        }

        // POST: Promotion/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Promotion item) {
            int result = 0;
            try {
                item.BodyTH = ReplaceTextEditorPath(item.BodyTH);
                item.BodyEN = ReplaceTextEditorPath(item.BodyEN);
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = Admin.Id;

                //if (item.PromotionType == PromotionType.Gift) {
                //    string[] gTH = Request.Form["PromotionGifts.TitleTH"].Split(',');
                //    string[] gEN = Request.Form["PromotionGifts.TitleEN"].Split(',');
                //    if (gTH.Length > 0) {
                //        for (int i = 0; i < gTH.Length; i++) {
                //            item.PromotionGifts.Add(new PromotionGift() { TitleTH = gTH[i], TitleEN = gEN[i] });
                //        }
                //    }
                //}

                if (item.PromotionType == PromotionType.Gift) {
                    item.DiscountType = 0;// PromotionDiscountType.Amount;
                    item.DiscountAmount = 0;
                //} else if (item.PromotionType == PromotionType.Discount) {
                //    item.PromotionGifts = new List<PromotionGift>();
                }


                if (item.ProductType == PromotionProductType.ByProduct && !string.IsNullOrEmpty(Request.Form["product_id"])) {
                    string[] products = Request.Form["product_id"].Split(',');
                    item.PromotionProducts = new List<PromotionProduct>();
                    foreach (var it in products) {
                        item.PromotionProducts.Add(new PromotionProduct() { ProductId = int.Parse(it) });
                    }
                }

                if (item.ProductType == PromotionProductType.ByCategory) {
                    var values = Request.Form["PromotionProductCategories"].Split(',');
                    item.PromotionProductCategories = new List<PromotionProductCategory>();
                    foreach (var it in values) {
                        if (!string.Equals(it, "false")) {
                            item.PromotionProductCategories.Add(new PromotionProductCategory() { ProductCategoryId = int.Parse(it) });
                        }
                    }
                } else {
                    item.PromotionProductCategories = null;
                }
                if (item.ProductType == PromotionProductType.ByCompany) {
                    var values = Request.Form["PromotionCompanies"].Split(',');
                    item.PromotionCompanies = new List<PromotionCompany>();
                    foreach (var it in values) {
                        if (!string.Equals(it, "false")) {
                            item.PromotionCompanies.Add(new PromotionCompany() { CompanyId = int.Parse(it) });
                        }
                    }
                } else {
                    item.PromotionCompanies = null;
                }

                result = repo.UpdatePromotion(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            BindForm();
            return View("Form", item);
        }

        // GET: Promotion/Duplicate/5
        public ActionResult Duplicate(int id) {
            Promotion item = repo.GetPromotion_Admin(id);
            item.BodyTH = ReplaceTextEditorPathDisplay(item.BodyTH);
            item.BodyEN = ReplaceTextEditorPathDisplay(item.BodyEN);
            item.ParentId = id;
            BindForm();
            return View("Form", item);
        }

        // POST: Promotion/Duplicate
        [HttpPost]
        public ActionResult Duplicate(Promotion item) {
            try {
                item.BodyTH = ReplaceTextEditorPath(item.BodyTH);
                item.BodyEN = ReplaceTextEditorPath(item.BodyEN);
                item.CreateDate = item.UpdateDate = DateTime.Now;
                item.CreateAdminId = item.UpdateAdminId = item.OwnerAdminId = Admin.Id;
                item.ApproveAdminId = "";
                item.ApproveStatus = ApproveStatus.Pending;

                item.Code = "";

                if (item.PromotionType == PromotionType.Gift) {
                    item.DiscountType = 0;// PromotionDiscountType.Amount;
                    item.DiscountAmount = 0;
                //} else if (item.PromotionType == PromotionType.Discount) {
                //    item.PromotionGifts = new List<PromotionGift>();
                }

                if (item.ProductType == PromotionProductType.ByProduct && !string.IsNullOrEmpty(Request.Form["product_id"])) {
                    string[] products = Request.Form["product_id"].Split(',');
                    item.PromotionProducts = new List<PromotionProduct>();
                    foreach (var p in products) {
                        item.PromotionProducts.Add(new PromotionProduct() { ProductId = int.Parse(p) });
                    }
                }

                if (item.ProductType == PromotionProductType.ByCategory) {
                    var values = Request.Form["PromotionProductCategories"].Split(',');
                    item.PromotionProductCategories = new List<PromotionProductCategory>();
                    foreach (var it in values) {
                        if (!string.Equals(it, "false")) {
                            item.PromotionProductCategories.Add(new PromotionProductCategory() { ProductCategoryId = int.Parse(it) });
                        }
                    }
                } else {
                    item.PromotionProductCategories = null;
                }
                if (item.ProductType == PromotionProductType.ByCompany) {
                    var values = Request.Form["PromotionCompanies"].Split(',');
                    item.PromotionCompanies = new List<PromotionCompany>();
                    foreach (var it in values) {
                        if (!string.Equals(it, "false")) {
                            item.PromotionCompanies.Add(new PromotionCompany() { CompanyId = int.Parse(it) });
                        }
                    }
                } else {
                    item.PromotionCompanies = null;
                }

                int id = repo.AddPromotion(item);
                Notify_Add_Success();
                return RedirectToAction("Index");
            } catch { }
            Notify_Add_Fail();
            BindForm();
            return View("Form", item);
        }

        // GET: Promotion/View/5
        public ActionResult View(int id) {
            Promotion item = repo.GetPromotion_Admin(id);
            item.BodyTH = ReplaceTextEditorPathDisplay(item.BodyTH);
            item.BodyEN = ReplaceTextEditorPathDisplay(item.BodyEN);
            BindForm();
            return View("View", item);
        }

        // GET: Promotion/Approve/5
        public ActionResult Approve(int id) {
            Promotion item = repo.GetPromotion_Admin(id);
            item.BodyTH = ReplaceTextEditorPathDisplay(item.BodyTH);
            item.BodyEN = ReplaceTextEditorPathDisplay(item.BodyEN);
            BindForm();
            return View("View", item);
        }

        // POST: Promotion/Approve/5
        [HttpPost]
        public ActionResult Approve(int id, Promotion item) {
            int result = 0;
            try {
                item.ApproveStatus = (ApproveStatus)Enum.Parse(typeof(ApproveStatus), Request.Form["Approve"].ToString());
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = item.ApproveAdminId = Admin.Id; // = item.OwnerAdminId

                result = repo.UpdatePromotionApproveStatus(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            BindForm();
            return View("View", item);
        }

        // GET: Promotion/Assign/5
        public ActionResult Assign(int id) {
            Promotion item = repo.GetPromotion_Admin(id);
            item.BodyTH = ReplaceTextEditorPathDisplay(item.BodyTH);
            item.BodyEN = ReplaceTextEditorPathDisplay(item.BodyEN);
            AdminRepository adminRepo = new AdminRepository();
            ViewBag.OwnerAdminList = adminRepo.ListAdminByPermission(Menu.Module, AdminAction.Edit).Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() }).ToList();
            BindForm();
            return View("View", item);
        }

        // POST: Promotion/Assign/5
        [HttpPost]
        public ActionResult Assign(int id, Promotion item) {
            int result = 0;
            try {
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = Admin.Id;

                result = repo.UpdatePromotionOwner(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            BindForm();
            return View("View", item);
        }

        // GET: Promotion/Disable/5
        public ActionResult Disable(int id) {
            Promotion item = new Promotion();
            item.Id = id;
            item.Status = Status.Inactive;
            item.UpdateDate = DateTime.Now;
            item.UpdateAdminId = Admin.Id;
            var result = repo.UpdatePromotionStatus(item);
            if (result > 0) {
                Notify_Update_Success();
            } else {
                Notify_Update_Fail();
            }
            return RedirectToAction("Index");
        }

        // GET: Promotion/Delete/5
        public ActionResult Delete(int id) {
            var result = repo.DeletePromotion(id, Admin.Id);
            if (result > 0) {
                Notify_Delete_Success();
            } else {
                Notify_Delete_Fail();
            }
            return RedirectToAction("Index");
        }

        // GET: Promotion/DataTable
        public ActionResult DataTable() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();
           
            if (!Menu.Can_Approve && !Menu.Can_Assign) { //see only own content if not approver/assigner
                whereC.Add(" (OwnerAdminId=@ownerAdminId) ");
                whereParams.ownerAdminId = Admin.Id;
            }

            if (param.hasSearch) {
                whereC.Add(" (TitleTH LIKE @searchValue OR TitleEN LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }
            DateTime dateFrom = ValueUtils.GetDate(Request.Unvalidated.Form["DateFrom"]);
            DateTime dateTo = ValueUtils.GetDate(Request.Unvalidated.Form["DateTo"]);
            if (dateFrom > DateUtils.SqlMinDate()) {
                whereC.Add(" (ActiveDate >= @DateFrom)");
                whereParams.DateFrom = dateFrom;
            }

            if (dateTo > DateUtils.SqlMinDate()) {
                whereC.Add(" (ActiveDate <= @DateTo)");
                whereParams.DateTo = dateTo;
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "Id", "TitleTH", "ActiveDate", "ExpireDate", "UpdateDate", "Status", "ApproveStatus", "ChildId" };
            DataTableData<Promotion> data = repo.ListPromotion_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Id.ToString(),
                        x.TitleTH.ToString(),
                        x.ActiveDate.ToString("dd/MM/yyyy"),
                        x.ExpireDate.ToString("dd/MM/yyyy"),
                        x.UpdateDate.ToString("dd/MM/yyyy HH:mm"),
                        x.Status.ToString(),
                        x.ApproveStatus.ToString(),
                        (
                            HtmlActionButton(AdminAction.View, x.Id) + 
                            HtmlActionButton(AdminAction.Edit, x.Id, "", (x.ApproveStatus == ApproveStatus.Approve)) +
                            HtmlActionButton(AdminAction.Duplicate, x.Id, "", x.ChildId > 0) +
                            (x.ApproveStatus == ApproveStatus.Approve ? HtmlActionButton(AdminAction.Disable, x.Id, "", (x.Status == Status.Inactive)) : HtmlActionButton(AdminAction.Delete, x.Id)) +
                            HtmlActionButton(AdminAction.Approve, x.Id, "", (x.ApproveStatus == ApproveStatus.Approve)) +
                            HtmlActionButton(AdminAction.Assign, x.Id, "", (x.ApproveStatus == ApproveStatus.Approve))
                        )
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }
        
        // GET: Promotion/DataTableProduct
        public ActionResult DataTableProduct() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();
            
            if (param.hasSearch) {
                whereC.Add(" (TitleTH LIKE @searchValue OR TitleEN LIKE @searchValue OR Category LIKE @searchValue OR SubCategory LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            whereC.Add(" (Status=" + (int)Status.Active + " AND ApproveStatus=" + (int)ApproveStatus.Approve + ") ");

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            ProductRepository productRepo = new ProductRepository();

            string[] columns = new string[] { "Id", "Category", "SubCategory", "TitleTH", "Status" };
            DataTableData<Product> data = productRepo.ListProduct_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Id.ToString(),
                        (x.Category == null ? "พ.ร.บ." : x.Category.ToString()),
                        x.SubCategory.ToString(),
                        x.TitleTH.ToString(),
                        x.Status.ToString(),
                        "<button type=\"button\" class=\"btn btn-xs btn-success\" title=\"Select\" data-id=\"" + x.Id.ToString() + "\" data-title=\"" + x.TitleTH.ToString() + "\"><i class=\"fa fa-check\"></i></a>"
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }
        
        private void BindForm() {
            ProductCategoryRepository productCategoryRepo = new ProductCategoryRepository();
            List<SelectListItem> items = productCategoryRepo.ListProductCategory_SelectList_Admin();
            ViewBag.ProductCategoryList = items;
            CompanyRepository companyRepo = new CompanyRepository();
            List<SelectListItem> companies = companyRepo.ListCompany_SelectList_Admin();
            ViewBag.CompanyList = companies;
        }

    }
}
