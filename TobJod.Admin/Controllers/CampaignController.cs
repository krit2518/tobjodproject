﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TobJod.Admin.Models;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class CampaignController : BaseController {
        CampaignRepository repo;

        public CampaignController() {
            Path = System.Configuration.ConfigurationManager.AppSettings["Path_Campaign"];
            repo = new CampaignRepository();
        }
        
        // GET: Campaign
        public ActionResult Index() {
            DateTime dateFrom = ValueUtils.GetDate(Request.QueryString["DateFrom"]);
            DateTime dateTo = ValueUtils.GetDate(Request.QueryString["DateTo"]);
            if (dateFrom > DateUtils.SqlMinDate()) ViewBag.DateFrom = dateFrom.ToString("dd/MM/yyyy", cultureGB);
            if (dateTo > DateUtils.SqlMinDate()) ViewBag.DateTo = dateTo.ToString("dd/MM/yyyy", cultureGB);
            return View();
        }

        // GET: Campaign/Create
        public ActionResult Create() {
            CampaignViewModel item = new CampaignViewModel();
            item.CampaignPromotions = new List<CampaignPromotion>();
            item.Status = Status.Active;
            BindForm();
            return View("Form", item);
        }

        // POST: Campaign/Create
        [HttpPost]
        public ActionResult Create(CampaignViewModel item) {
            try {
                item.BodyTH = ReplaceTextEditorPath(item.BodyTH);
                item.BodyEN = ReplaceTextEditorPath(item.BodyEN);
                item.CreateDate = item.UpdateDate = DateTime.Now;
                item.CreateAdminId = item.UpdateAdminId = item.OwnerAdminId = Admin.Id;
                item.ApproveAdminId = "";
                item.ApproveStatus = ApproveStatus.Pending;
                item.ImageTH = UploadUtils.SaveFile(item.ImageTHFile, PathPhy);
                item.ImageEN = UploadUtils.SaveFile(item.ImageENFile, PathPhy);
                if (item.PromotionAll != true) item.PromotionAll = false;

                item.Code = "";
                
                item.CampaignPromotions = new List<CampaignPromotion>();
                if (!item.PromotionAll) {
                    string[] promotions = Request.Form["promotion_id"].Split(',');
                    if (promotions.Length > 0) {
                        foreach (var it in promotions) {
                            item.CampaignPromotions.Add(new CampaignPromotion() { PromotionId = int.Parse(it) });
                        }
                    }
                }

                int id = repo.AddCampaign(item);
                Notify_Add_Success();
                return RedirectToAction("Index");
            } catch { }
            Notify_Add_Fail();
            item.CampaignPromotions = new List<CampaignPromotion>();
            BindForm();
            return View("Form", item);
        }

        // GET: Campaign/Edit/5
        public ActionResult Edit(int id) {
            Campaign model = repo.GetCampaign_Admin(id);
            CampaignViewModel item = CampaignViewModel.Map(model);
            item.BodyTH = ReplaceTextEditorPathDisplay(item.BodyTH);
            item.BodyEN = ReplaceTextEditorPathDisplay(item.BodyEN);
            BindForm();
            return View("Form", item);
        }

        // POST: Campaign/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, CampaignViewModel item) {
            int result = 0;
            try {
                item.BodyTH = ReplaceTextEditorPath(item.BodyTH);
                item.BodyEN = ReplaceTextEditorPath(item.BodyEN);
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = Admin.Id;
                item.ImageTH = UploadUtils.SaveAndReplaceFile(item.ImageTHFile, PathPhy, item.ImageTH);
                item.ImageEN = UploadUtils.SaveAndReplaceFile(item.ImageENFile, PathPhy, item.ImageEN);
                if (item.PromotionAll != true) item.PromotionAll = false;
                
                item.CampaignPromotions = new List<CampaignPromotion>();
                if (!item.PromotionAll) {
                    string[] promotions = Request.Form["promotion_id"].Split(',');
                    if (promotions.Length > 0) {
                        foreach (var it in promotions) {
                            item.CampaignPromotions.Add(new CampaignPromotion() { PromotionId = int.Parse(it) });
                        }
                    }
                }
                
                result = repo.UpdateCampaign(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            BindForm();
            return View("Form", item);
        }

        // GET: Campaign/Duplicate/5
        public ActionResult Duplicate(int id) {
            Campaign model = repo.GetCampaign_Admin(id);
            CampaignViewModel item = CampaignViewModel.Map(model);
            item.BodyTH = ReplaceTextEditorPathDisplay(item.BodyTH);
            item.BodyEN = ReplaceTextEditorPathDisplay(item.BodyEN);
            item.ParentId = id;
            BindForm();
            return View("Form", item);
        }

        // POST: Campaign/Duplicate
        [HttpPost]
        public ActionResult Duplicate(CampaignViewModel item) {
            try {
                item.BodyTH = ReplaceTextEditorPath(item.BodyTH);
                item.BodyEN = ReplaceTextEditorPath(item.BodyEN);
                item.CreateDate = item.UpdateDate = DateTime.Now;
                item.CreateAdminId = item.UpdateAdminId = item.OwnerAdminId = Admin.Id;
                item.ApproveAdminId = "";
                item.ApproveStatus = ApproveStatus.Pending;
                item.ImageTH = UploadUtils.SaveOrDuplicateFile(item.ImageTHFile, PathPhy, item.ImageTH);
                item.ImageEN = UploadUtils.SaveOrDuplicateFile(item.ImageENFile, PathPhy, item.ImageEN);
                if (item.PromotionAll != true) item.PromotionAll = false;

                item.Code = "";

                item.CampaignPromotions = new List<CampaignPromotion>();
                if (!item.PromotionAll) {
                    string[] promotions = Request.Form["promotion_id"].Split(',');
                    if (promotions.Length > 0) {
                        foreach (var it in promotions) {
                            item.CampaignPromotions.Add(new CampaignPromotion() { PromotionId = int.Parse(it) });
                        }
                    }
                }

                int id = repo.AddCampaign(item);
                Notify_Add_Success();
                return RedirectToAction("Index");
            } catch { }
            Notify_Add_Fail();
            item.CampaignPromotions = new List<CampaignPromotion>();
            BindForm();
            return View("Form", item);
        }

        // GET: Campaign/View/5
        public ActionResult View(int id) {
            Campaign model = repo.GetCampaign_Admin(id);
            CampaignViewModel item = CampaignViewModel.Map(model);
            item.BodyTH = ReplaceTextEditorPathDisplay(item.BodyTH);
            item.BodyEN = ReplaceTextEditorPathDisplay(item.BodyEN);
            BindForm();
            return View("View", item);
        }

        // GET: Campaign/Approve/5
        public ActionResult Approve(int id) {
            Campaign model = repo.GetCampaign_Admin(id);
            CampaignViewModel item = CampaignViewModel.Map(model);
            item.BodyTH = ReplaceTextEditorPathDisplay(item.BodyTH);
            item.BodyEN = ReplaceTextEditorPathDisplay(item.BodyEN);
            BindForm();
            return View("View", item);
        }

        // POST: Campaign/Approve/5
        [HttpPost]
        public ActionResult Approve(int id, CampaignViewModel item) {
            int result = 0;
            try {
                item.ApproveStatus = (ApproveStatus)Enum.Parse(typeof(ApproveStatus), Request.Form["Approve"].ToString());
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = item.ApproveAdminId = Admin.Id; // = item.OwnerAdminId

                result = repo.UpdateCampaignApproveStatus(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            BindForm();
            return View("View", item);
        }

        // GET: Campaign/Assign/5
        public ActionResult Assign(int id) {
            Campaign model = repo.GetCampaign_Admin(id);
            AdminRepository adminRepo = new AdminRepository();
            ViewBag.OwnerAdminList = adminRepo.ListAdminByPermission(Menu.Module, AdminAction.Edit).Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() }).ToList();
            CampaignViewModel item = CampaignViewModel.Map(model);
            item.BodyTH = ReplaceTextEditorPathDisplay(item.BodyTH);
            item.BodyEN = ReplaceTextEditorPathDisplay(item.BodyEN);
            BindForm();
            return View("View", item);
        }

        // POST: Campaign/Assign/5
        [HttpPost]
        public ActionResult Assign(int id, CampaignViewModel item) {
            int result = 0;
            try {
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = Admin.Id;

                result = repo.UpdateCampaignOwner(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            BindForm();
            return View("View", item);
        }

        // GET: Campaign/Disable/5
        public ActionResult Disable(int id) {
            Campaign item = new Campaign();
            item.Id = id;
            item.Status = Status.Inactive;
            item.UpdateDate = DateTime.Now;
            item.UpdateAdminId = Admin.Id;
            var result = repo.UpdateCampaignStatus(item);
            if (result > 0) {
                Notify_Update_Success();
            } else {
                Notify_Update_Fail();
            }
            return RedirectToAction("Index");
        }

        // GET: Campaign/Delete/5
        public ActionResult Delete(int id) {
            var result = repo.DeleteCampaign(id, Admin.Id);
            if (result > 0) {
                Notify_Delete_Success();
            } else {
                Notify_Delete_Fail();
            }
            return RedirectToAction("Index");
        }

        // GET: Campaign/Export
        public void Export() {
            var dt = repo.ListCampaign_Export();
            ExportFile("Campaign.xlsx", "Campaign", dt, null, null, new string[] { "ActiveDate", "ExpireDate", "Promotion_ActiveDate", "Promotion_ExpireDate" });
        }

        // GET: Campaign/DataTable
        public ActionResult DataTable() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();
           
            if (!Menu.Can_Approve && !Menu.Can_Assign) { //see only own content if not approver/assigner
                whereC.Add(" (OwnerAdminId=@ownerAdminId) ");
                whereParams.ownerAdminId = Admin.Id;
            }
            DateTime dateFrom = ValueUtils.GetDate(Request.Unvalidated.Form["DateFrom"]);
            DateTime dateTo = ValueUtils.GetDate(Request.Unvalidated.Form["DateTo"]);
            if (dateFrom > DateUtils.SqlMinDate()) {
                whereC.Add(" (ActiveDate >= @DateFrom)");
                whereParams.DateFrom = dateFrom;
            }

            if (dateTo > DateUtils.SqlMinDate()) {
                whereC.Add(" (ActiveDate <= @DateTo)");
                whereParams.DateTo = dateTo;
            }

            if (param.hasSearch) {
                whereC.Add(" (TitleTH LIKE @searchValue OR TitleEN LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "Id", "TitleTH", "ActiveDate", "ExpireDate", "UpdateDate", "Status", "ApproveStatus", "ChildId" };
            DataTableData<Campaign> data = repo.ListCampaign_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Id.ToString(),
                        x.TitleTH.ToString(),
                        x.ActiveDate.ToString("dd/MM/yyyy"),
                        x.ExpireDate.ToString("dd/MM/yyyy"),
                        x.UpdateDate.ToString("dd/MM/yyyy HH:mm"),
                        x.Status.ToString(),
                        x.ApproveStatus.ToString(),
                        (
                            HtmlActionButton(AdminAction.View, x.Id) + 
                            HtmlActionButton(AdminAction.Edit, x.Id, "", (x.ApproveStatus == ApproveStatus.Approve)) +
                            HtmlActionButton(AdminAction.Duplicate, x.Id, "", x.ChildId > 0) +
                            (x.ApproveStatus == ApproveStatus.Approve ? HtmlActionButton(AdminAction.Disable, x.Id, "", (x.Status == Status.Inactive)) : HtmlActionButton(AdminAction.Delete, x.Id)) +
                            HtmlActionButton(AdminAction.Approve, x.Id, "", (x.ApproveStatus == ApproveStatus.Approve)) +
                            HtmlActionButton(AdminAction.Assign, x.Id, "", (x.ApproveStatus == ApproveStatus.Approve))
                        )
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }
        
        // GET: Campaign/DataTablePromotion
        public ActionResult DataTablePromotion() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();
            
            if (param.hasSearch) {
                whereC.Add(" (TitleTH LIKE @searchValue OR TitleEN LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }
            DateTime dateFrom = ValueUtils.GetDate(Request.Unvalidated.Form["DateFrom"]);
            DateTime dateTo = ValueUtils.GetDate(Request.Unvalidated.Form["DateTo"]);
            if (dateFrom > DateUtils.SqlMinDate()) {
                whereC.Add(" (ActiveDate >= @DateFrom)");
                whereParams.DateFrom = dateFrom;
            }

            if (dateTo > DateUtils.SqlMinDate()) {
                whereC.Add(" (ActiveDate <= @DateTo)");
                whereParams.DateTo = dateTo;
            }

            whereC.Add(" (Status=" + (int)Status.Active + " AND ApproveStatus=" + (int)ApproveStatus.Approve + ") ");

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            PromotionRepository promotionRepo = new PromotionRepository();

            string[] columns = new string[] { "Id", "TitleTH", "ActiveDate", "ExpireDate", "Status" };
            DataTableData<Promotion> data = promotionRepo.ListPromotion_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Id.ToString(),
                        x.TitleTH.ToString(),
                        x.ActiveDate.ToString("dd/MM/yyyy"),
                        x.ExpireDate.ToString("dd/MM/yyyy"),
                        x.Status.ToString(),
                        "<button type=\"button\" class=\"btn btn-xs btn-success\" title=\"Select\" data-id=\"" + x.Id.ToString() + "\" data-title=\"" + x.TitleTH.ToString() + "\"><i class=\"fa fa-check\"></i></a>"
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }
        
        private void BindForm() {
        }

    }
}
