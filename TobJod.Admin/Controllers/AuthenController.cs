﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {

    public class AuthenController : Controller {

        // GET: Authen
        public ActionResult Index() {
            ViewBag.Message = "Please Login again.";
            string empId = Request.Form["emp_id"];
            if (!string.IsNullOrEmpty(empId)) {
                AdminRepository repo = new AdminRepository();
                var user = repo.GetAdminByEmployeeId(empId);
                if (user != null) {
                    AdminAuthenLogRepository logRepo = new AdminAuthenLogRepository();
                    Session["Login"] = DateTime.Now;
                    logRepo.AddAdminAuthenLog(new TobJod.Models.AdminAuthenLog() { AdminId = user.EmployeeId, SessionId = Session.SessionID, Login = DateTime.Now, PublicIP = HttpUtils.GetPublicIP(), LocalIP = HttpUtils.GetPrivateIP(), Status = TobJod.Models.Status.Active, CreateDate = DateTime.Now });
                    FormsAuthentication.SetAuthCookie(user.EmployeeId, false);
                    return RedirectToAction("Index", "Home");
                } else {
                    ViewBag.Message = "Invalid Login";
                }
            }
            return View();
        }

        public void Logout() {
            AdminAuthenLogRepository logRepo = new AdminAuthenLogRepository();
            logRepo.UpdateAdminAuthenLogLogout(new TobJod.Models.AdminAuthenLog() { AdminId = HttpContext.User.Identity.Name, SessionId = Session.SessionID, Logout = DateTime.Now, Status = TobJod.Models.Status.Inactive });
            FormsAuthentication.SignOut();
            //return RedirectToAction("Index");
            Response.Redirect(System.Configuration.ConfigurationManager.AppSettings["Url_Authen"]);
        }

    }
}