﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TobJod.Admin.Models;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class HomePromotionController : BaseController {
        ConfigRepository repo;

        public HomePromotionController() {
            Path = System.Configuration.ConfigurationManager.AppSettings["Path_Config"];
            repo = new ConfigRepository();
        }

        // GET: HomePromotion
        public ActionResult Index() {
            if (!HasPermission(AdminAction.Edit, this.Module) && HasPermission(AdminAction.View, this.Module)) {
                return RedirectToAction("Views");
            }
            return RedirectToAction("Edit");
        }

        // GET: HomePromotion/Edit
        public ActionResult Edit() {
            var items = repo.ListConfigs_Admin();
            HomePromotionViewModel item = new HomePromotionViewModel(items);
            return View("Form", item);
        }

        // POST: HomePromotion/Edit/5
        [HttpPost]
        public ActionResult Edit(HomePromotionViewModel item) {
            int result = 0;
            if (UploadUtils.IsImage(item.HomePromotion1ImageTHFile, item.HomePromotion2ImageTHFile, item.HomePromotion3ImageTHFile, item.HomePromotion4ImageTHFile, item.HomePromotion1ImageENFile, item.HomePromotion2ImageENFile, item.HomePromotion3ImageENFile, item.HomePromotion4ImageENFile, item.HomePromotion1ImageMobileTHFile, item.HomePromotion2ImageMobileTHFile, item.HomePromotion3ImageMobileTHFile, item.HomePromotion4ImageMobileTHFile, item.HomePromotion1ImageMobileENFile, item.HomePromotion2ImageMobileENFile, item.HomePromotion3ImageMobileENFile, item.HomePromotion4ImageMobileENFile)) {
                try {
                    item.HomePromotion1ImageTH = UploadUtils.SaveAndReplaceFile(item.HomePromotion1ImageTHFile, PathPhy, item.HomePromotion1ImageTH);
                    item.HomePromotion2ImageTH = UploadUtils.SaveAndReplaceFile(item.HomePromotion2ImageTHFile, PathPhy, item.HomePromotion2ImageTH);
                    item.HomePromotion3ImageTH = UploadUtils.SaveAndReplaceFile(item.HomePromotion3ImageTHFile, PathPhy, item.HomePromotion3ImageTH);
                    item.HomePromotion4ImageTH = UploadUtils.SaveAndReplaceFile(item.HomePromotion4ImageTHFile, PathPhy, item.HomePromotion4ImageTH);
                    item.HomePromotion1ImageEN = UploadUtils.SaveAndReplaceFile(item.HomePromotion1ImageENFile, PathPhy, item.HomePromotion1ImageEN);
                    item.HomePromotion2ImageEN = UploadUtils.SaveAndReplaceFile(item.HomePromotion2ImageENFile, PathPhy, item.HomePromotion2ImageEN);
                    item.HomePromotion3ImageEN = UploadUtils.SaveAndReplaceFile(item.HomePromotion3ImageENFile, PathPhy, item.HomePromotion3ImageEN);
                    item.HomePromotion4ImageEN = UploadUtils.SaveAndReplaceFile(item.HomePromotion4ImageENFile, PathPhy, item.HomePromotion4ImageEN);

                    item.HomePromotion1ImageMobileTH = UploadUtils.SaveAndReplaceFile(item.HomePromotion1ImageMobileTHFile, PathPhy, item.HomePromotion1ImageMobileTH);
                    item.HomePromotion2ImageMobileTH = UploadUtils.SaveAndReplaceFile(item.HomePromotion2ImageMobileTHFile, PathPhy, item.HomePromotion2ImageMobileTH);
                    item.HomePromotion3ImageMobileTH = UploadUtils.SaveAndReplaceFile(item.HomePromotion3ImageMobileTHFile, PathPhy, item.HomePromotion3ImageMobileTH);
                    item.HomePromotion4ImageMobileTH = UploadUtils.SaveAndReplaceFile(item.HomePromotion4ImageMobileTHFile, PathPhy, item.HomePromotion4ImageMobileTH);
                    item.HomePromotion1ImageMobileEN = UploadUtils.SaveAndReplaceFile(item.HomePromotion1ImageMobileENFile, PathPhy, item.HomePromotion1ImageMobileEN);
                    item.HomePromotion2ImageMobileEN = UploadUtils.SaveAndReplaceFile(item.HomePromotion2ImageMobileENFile, PathPhy, item.HomePromotion2ImageMobileEN);
                    item.HomePromotion3ImageMobileEN = UploadUtils.SaveAndReplaceFile(item.HomePromotion3ImageMobileENFile, PathPhy, item.HomePromotion3ImageMobileEN);
                    item.HomePromotion4ImageMobileEN = UploadUtils.SaveAndReplaceFile(item.HomePromotion4ImageMobileENFile, PathPhy, item.HomePromotion4ImageMobileEN);

                    result = repo.UpdateConfigs(item.ToList(), DateTime.Now, Admin.Id);
                    if (result > 0) {
                        Notify_Update_Success();
                        return RedirectToAction("Edit");
                    }
                } catch { }
            }
            Notify_Update_Fail();
            return View("Form", item);
        }

        // GET: HomePromotion/Views
        public ActionResult Views() {
            var items = repo.ListConfigs_Admin();
            HomePromotionViewModel item = new HomePromotionViewModel(items);
            return View("View", item);
        }

    }
}
