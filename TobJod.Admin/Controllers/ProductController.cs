﻿using ExcelDataReader;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using TobJod.Admin.Helpers;
using TobJod.Admin.Models;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class ProductController : BaseController {
        ProductRepository repo;
        private int category;

        public ProductController() {
            repo = new ProductRepository();
        }
        
        // GET: Product
        public ActionResult Index() {
            Product item = new Product();
            CompanyRepository coRepo = new CompanyRepository();
            ViewBag.CompanyList = coRepo.ListCompany_SelectList_Admin();
            try { item.ProductSubCategoryId = (ProductSubCategoryKey)NullUtils.cvInt(Request.QueryString["ProductSubCategoryId"]); } catch { }
            try { item.Status = (Status)NullUtils.cvInt(Request.QueryString["Status"], -1); } catch { }
            try { item.ApproveStatus = (ApproveStatus)NullUtils.cvInt(Request.QueryString["ApproveStatus"], -1); } catch { }
            item.CompanyId = NullUtils.cvInt(Request.QueryString["CompanyId"]);
            if (string.Equals(Request.QueryString["export"], "1")) {
                string where = string.Empty;
                dynamic whereParams = new System.Dynamic.ExpandoObject();
                List<string> whereC = new List<string>();
                
                if (NullUtils.cvInt(Request.Unvalidated.QueryString["ProductSubCategoryId"]) > 0) {
                    whereC.Add(" (SubCategoryId=@category) ");
                    whereParams.category = NullUtils.cvInt(Request.Unvalidated.QueryString["ProductSubCategoryId"]);
                }

                if (NullUtils.cvInt(Request.Unvalidated.QueryString["CompanyId"]) > 0) {
                    whereC.Add(" (CompanyId=@company) ");
                    whereParams.company = NullUtils.cvInt(Request.Unvalidated.QueryString["CompanyId"]);
                }

                if (NullUtils.cvInt(Request.Unvalidated.QueryString["Status"]) > -1) {
                    whereC.Add(" (Status=@status) ");
                    whereParams.status = NullUtils.cvInt(Request.Unvalidated.QueryString["Status"]);
                }

                if (NullUtils.cvInt(Request.Unvalidated.QueryString["ApproveStatus"]) > -1) {
                    whereC.Add(" (ApproveStatus=@approvestatus) ");
                    whereParams.approvestatus = NullUtils.cvInt(Request.Unvalidated.QueryString["ApproveStatus"]);
                }
                
                if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

                ExportBulk(repo.ListProduct_Admin(where, whereParams), ProductCategory.GetCategoryBySubCategory(item.ProductSubCategoryId));
            }
            return View(item);
        }

        // GET: Product/Create
        public ActionResult Create() {
            Product item = new Product();
            item.Status = Status.Active;
            item.ProductCode = "[Auto]";
            category = NullUtils.cvInt(Request.QueryString["category"]);
            ProductCategoryKey c;
            if (Enum.TryParse<ProductCategoryKey>(category.ToString(), out c)) { item.CategoryId = c; }
            BindForm();
            return View("Form", item);
        }

        // POST: Product/Create
        [HttpPost]
        public ActionResult Create(Product item) {
            int result = 0;
            try {
                var subClass = repo.ListProductSubClass(item.CategoryId);
                if (!subClass.Contains(item.Class + "|" + item.SubClass)) {
                    Notify(NotifyMessageType.Error, "Add error", "Invalid Class/Subclass.");
                    category = NullUtils.cvInt(Request.QueryString["category"]);
                    item.ProductCode = "[Auto]";
                    category = (int)item.CategoryId;
                    BindForm();
                    return View("Form", item);
                }

                item.CoverageTH = GetCoverage("CoverageTH");
                item.CoverageEN = GetCoverage("CoverageEN");
                item.CreateDate = item.UpdateDate = DateTime.Now;
                item.CreateAdminId = item.UpdateAdminId = Admin.Id;
                item.ApproveAdminId = item.OwnerAdminId = "";

                CompanyRepository companyRepo = new CompanyRepository();
                item.CompanyCode = companyRepo.GetCompany_Admin(item.CompanyId).Code;
                
                result = repo.AddProduct(item);
                if (result > 0) {
                    Notify_Add_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Add_Fail();
            category = NullUtils.cvInt(Request.QueryString["category"]);
            BindForm();
            return View("Form", item);
        }

        // GET: Product/View/5
        public ActionResult View(int id) {
            Product item = repo.GetProduct_Admin(id);
            category = (int)item.CategoryId;
            BindForm();
            return View("View", item);
        }

        // GET: Product/Edit/5
        public ActionResult Edit(int id) {
            Product item = repo.GetProduct_Admin(id);
            category = (int)item.CategoryId;
            BindForm();
            if (item.ApproveStatus == ApproveStatus.Approve || (item.ApproveStatus == ApproveStatus.Pending && !String.IsNullOrEmpty(item.ApproveAdminId))) {
                return View("FormExpire", item);
            } else {
                return View("Form", item);
            }
        }

        // POST: Product/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Product item) {
            Product model = repo.GetProduct_Admin(id);
            int result = 0;
            try {
                if (model.ApproveStatus == ApproveStatus.Approve || !String.IsNullOrEmpty(model.ApproveAdminId)) {
                    item.ApproveStatus = ApproveStatus.Pending;
                    item.UpdateDate = DateTime.Now;
                    item.UpdateAdminId = Admin.Id;

                    result = repo.UpdateProductExpire(item);
                } else {
                    item.CoverageTH = GetCoverage("CoverageTH");
                    item.CoverageEN = GetCoverage("CoverageEN");
                    item.UpdateDate = DateTime.Now;
                    item.UpdateAdminId = Admin.Id;

                    result = repo.UpdateProduct(item);
                }
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            category = (int)item.CategoryId;
            BindForm();
            return View("Form", item);
        }

        // GET: Product/Import
        public ActionResult Import() {
            BindForm();
            return View("Import");
        }

        // POST: Product/Import/5
        [HttpPost]
        public ActionResult Import(ProductImportViewModel item) {
            List<Product> items;
            ActionResultStatus result = new ActionResultStatus();
            try {
                string path = System.IO.Path.GetTempPath();
                if (!item.ImportFile.FileName.ToLower().EndsWith(".xlsx")) throw new Exception("Invalide file type");
                string fileName = UploadUtils.SaveFile(item.ImportFile, path);
                DataSet ds;
                using (var stream = System.IO.File.Open(path + fileName, FileMode.Open, FileAccess.Read)) {
                    using (var reader = ExcelReaderFactory.CreateReader(stream)) {
                        ds = reader.AsDataSet();
                    }
                }

                using (ds) {
                    switch (item.CategoryId) {
                        case ProductCategoryKey.Motor: var xM = ImportMotor(ds); result = xM.Item1; items = xM.Item2; break;
                        case ProductCategoryKey.Home: var xH = ImportHome(ds); result = xH.Item1; items = xH.Item2; break;
                        case ProductCategoryKey.PA: var xPA = ImportPA(ds); result = xPA.Item1; items = xPA.Item2; break;
                        case ProductCategoryKey.PH: var xPH = ImportPH(ds); result = xPH.Item1; items = xPH.Item2; break;
                        case ProductCategoryKey.TA: var xTA = ImportTA(ds); result = xTA.Item1; items = xTA.Item2; break;
                    }
                }

                //item.UpdateDate = DateTime.Now;
                //item.UpdateAdminId = Admin.Id;

                //result = repo.UpdateProduct(item);
                if (result.Success) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            return View("Import", item);
        }

        // GET: Product/Approve/5
        public ActionResult Approve(int id) {
            Product item = repo.GetProduct_Admin(id);
            category = (int)item.CategoryId;
            BindForm();
            return View("View", item);
        }

        // POST: Product/Approve/5
        [HttpPost]
        public ActionResult Approve(int id, Product item) {
            int result = 0;
            try {
                item.ApproveStatus = (ApproveStatus)Enum.Parse(typeof(ApproveStatus), Request.Form["Approve"].ToString());                
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = item.ApproveAdminId = Admin.Id; // = item.OwnerAdminId

                result = repo.UpdateProductApproveStatus(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            category = (int)item.CategoryId;
            BindForm();
            return View("View", item);
        }

        // GET: Product/Assign/5
        public ActionResult Assign(int id) {
            Product item = repo.GetProduct_Admin(id);
            category = (int)item.CategoryId;
            AdminRepository adminRepo = new AdminRepository();
            ViewBag.OwnerAdminList = adminRepo.ListAdminByPermission(Menu.Module, AdminAction.Edit).Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() }).ToList();
            BindForm();
            return View("View", item);
        }

        // POST: Product/Assign/5
        [HttpPost]
        public ActionResult Assign(int id, Product item) {
            int result = 0;
            try {
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = Admin.Id;

                result = repo.UpdateProductOwner(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            category = (int)item.CategoryId;
            BindForm();
            return View("View", item);
        }
        
        // GET: Product/Disable/5
        public ActionResult Disable(int id) {
            Product item = new Product();
            item.Id = id;
            item.Status = Status.Inactive;
            item.UpdateDate = DateTime.Now;
            item.UpdateAdminId = Admin.Id;
            var result = repo.UpdateProductStatus(item);
            if (result > 0) {
                Notify_Update_Success();
            } else {
                Notify_Update_Fail();
            }
            return RedirectToAction("Index");
        }

        // GET: Product/Delete/5
        public ActionResult Delete(int id) {
            var result = repo.DeleteProduct(id, Admin.Id);
            if (result > 0) {
                Notify_Delete_Success();
            } else {
                Notify_Delete_Fail();
            }
            return RedirectToAction("Index");
        }

        // GET: Product/CreateMotorPremium/5
        public ActionResult CreateMotorPremium(string id) {
            CheckPermission(AdminAction.Add);
            ProductMotorPremium item = new ProductMotorPremium();
            item.ProductId = NullUtils.cvInt(Request.QueryString["productId"]);
            ViewBag.ProvinceList = repo.GetProductMotorProvinceCodeByProductId_Admin(item.ProductId, 0);
            return View("ProductMotor", item);
        }

        // POST: Product/CreateMotorPremium/5
        [HttpPost]
        public ActionResult CreateMotorPremium(string id, ProductMotorPremium item) {
            CheckPermission(AdminAction.Add);
            int result = 0; string message = "";
            if (CheckPremium(item.NetPremium, item.Duty, item.Vat, item.Premium)) {
                try {
                    item.RegisterProvince = new List<ProductMotorPremiumRegisterProvince>();
                    if (!item.RegisterProvinceAll) {
                        foreach (var r in Request.Form.AllKeys.Where(x => x.StartsWith("RegisterProvince[")).ToList()) {
                            item.RegisterProvince.Add(new ProductMotorPremiumRegisterProvince() { PremiumId = item.Id, RegisterProvinceCode = Request.Form[r], IsUsed = true });
                        }
                    }
                    item.Status = Status.Active;
                    item.CreateDate = item.UpdateDate = DateTime.Now;
                    item.CreateAdminId = item.UpdateAdminId = Admin.Id;
                    result = repo.AddProductMotorPremium(item);
                } catch { }
            } else { message = "Premium <> (NetPremium + Duty + Vat)."; }
            return Json(new { success = (result > 0), message = message });
        }

        // GET: Product/EditMotorPremium/5
        public ActionResult EditMotorPremium(string id) {
            CheckPermission(AdminAction.Edit);
            ProductMotorPremium item = repo.GetProductMotorPremium_Admin(id);
            ViewBag.ProvinceList = repo.GetProductMotorProvinceCodeByProductId_Admin(item.ProductId, NullUtils.cvInt(id));
            return View("ProductMotor", item);
        }

        // POST: Product/EditMotorPremium/5
        [HttpPost]
        public ActionResult EditMotorPremium(string id, ProductMotorPremium item) {
            CheckPermission(AdminAction.Edit);
            int result = 0; string message = "";
            if (CheckPremium(item.NetPremium, item.Duty, item.Vat, item.Premium)) {
                try {
                    item.RegisterProvince = new List<ProductMotorPremiumRegisterProvince>();
                    if (!item.RegisterProvinceAll) {
                        foreach (var r in Request.Form.AllKeys.Where(x => x.StartsWith("RegisterProvince[")).ToList()) {
                            item.RegisterProvince.Add(new ProductMotorPremiumRegisterProvince() { PremiumId = item.Id, RegisterProvinceCode = Request.Form[r], IsUsed = true });
                        }
                    }
                    item.UpdateDate = DateTime.Now;
                    item.UpdateAdminId = Admin.Id;
                    result = repo.UpdateProductMotorPremium(item);
                } catch { }
            } else { message = "Premium <> (NetPremium + Duty + Vat)."; }
            return Json(new { success = (result > 0), message = message });
        }

        // GET: Product/CreateHomePremium/5
        public ActionResult CreateHomePremium(string id) {
            CheckPermission(AdminAction.Add);
            ProductHomePremium item = new ProductHomePremium();
            item.ProductId = NullUtils.cvInt(Request.QueryString["productId"]);
            MasterOptionRepository masterOptionRepo = new MasterOptionRepository();
            ViewBag.PropertyTypeList = masterOptionRepo.ListMasterOption_SelectList_Admin(MasterOptionCategory.PropertyType);
            ViewBag.ConstructionTypeList = masterOptionRepo.ListMasterOption_SelectList_Admin(MasterOptionCategory.ConstructionType);
            return View("ProductHome", item);
        }

        // POST: Product/CreateHomePremium/5
        [HttpPost]
        public ActionResult CreateHomePremium(string id, ProductHomePremium item) {
            CheckPermission(AdminAction.Add);
            int result = 0; string message = "";
            if (CheckPremium(item.NetPremium, item.Duty, item.Vat, item.Premium)) {
                try {
                    item.Status = Status.Active;
                    item.CreateDate = item.UpdateDate = DateTime.Now;
                    item.CreateAdminId = item.UpdateAdminId = Admin.Id;
                    result = repo.AddProductHomePremium(item);
                } catch { }
            } else { message = "Premium <> (NetPremium + Duty + Vat)."; }
            return Json(new { success = (result > 0), message = message });
        }

        // GET: Product/EditHomePremium/5
        public ActionResult EditHomePremium(string id) {
            CheckPermission(AdminAction.Edit);
            ProductHomePremium item = repo.GetProductHomePremium_Admin(id);
            MasterOptionRepository masterOptionRepo = new MasterOptionRepository();
            ViewBag.PropertyTypeList = masterOptionRepo.ListMasterOption_SelectList_Admin(MasterOptionCategory.PropertyType);
            ViewBag.ConstructionTypeList = masterOptionRepo.ListMasterOption_SelectList_Admin(MasterOptionCategory.ConstructionType);
            return View("ProductHome", item);
        }

        // POST: Product/EditHomePremium/5
        [HttpPost]
        public ActionResult EditHomePremium(string id, ProductHomePremium item) {
            CheckPermission(AdminAction.Edit);
            int result = 0; string message = "";
            if (CheckPremium(item.NetPremium, item.Duty, item.Vat, item.Premium)) {
                try {
                    item.UpdateDate = DateTime.Now;
                    item.UpdateAdminId = Admin.Id;
                    result = repo.UpdateProductHomePremium(item);
                } catch { }
            } else { message = "Premium <> (NetPremium + Duty + Vat)."; }
            return Json(new { success = (result > 0), message = message });
        }

        // GET: Product/CreatePAPremium/5
        public ActionResult CreatePAPremium(string id) {
            CheckPermission(AdminAction.Add);
            ProductPAPremium item = new ProductPAPremium();
            item.ProductId = NullUtils.cvInt(Request.QueryString["productId"]);
            MasterOptionRepository masterOptionRepo = new MasterOptionRepository();
            ViewBag.CareerList = masterOptionRepo.ListMasterOption_SelectList_Admin(MasterOptionCategory.Career);
            return View("ProductPA", item);
        }

        // POST: Product/CreatePAPremium/5
        [HttpPost]
        public ActionResult CreatePAPremium(string id, ProductPAPremium item) {
            CheckPermission(AdminAction.Add);
            int result = 0; string message = "";
            if (CheckPremium(item.NetPremium, item.Duty, item.Vat, item.Premium)) {
                try {
                    item.Status = Status.Active;
                    item.CreateDate = item.UpdateDate = DateTime.Now;
                    item.CreateAdminId = item.UpdateAdminId = Admin.Id;
                    result = repo.AddProductPAPremium(item);
                } catch { }
            } else { message = "Premium <> (NetPremium + Duty + Vat)."; }
            return Json(new { success = (result > 0), message = message });
        }

        // GET: Product/EditPAPremium/5
        public ActionResult EditPAPremium(string id) {
            CheckPermission(AdminAction.Edit);
            ProductPAPremium item = repo.GetProductPAPremium_Admin(id);
            //MasterOptionRepository masterOptionRepo = new MasterOptionRepository();
            //ViewBag.CareerList = masterOptionRepo.ListMasterOption_SelectList_Admin(MasterOptionCategory.Career).Where(x => x.Value == item.CareerId.ToString()).ToList();
            return View("ProductPA", item);
        }

        // POST: Product/EditPAPremium/5
        [HttpPost]
        public ActionResult EditPAPremium(string id, ProductPAPremium item) {
            CheckPermission(AdminAction.Edit);
            int result = 0; string message = "";
            if (CheckPremium(item.NetPremium, item.Duty, item.Vat, item.Premium)) {
                try {
                    item.UpdateDate = DateTime.Now;
                    item.UpdateAdminId = Admin.Id;
                    result = repo.UpdateProductPAPremium(item);
                } catch { }
            } else { message = "Premium <> (NetPremium + Duty + Vat)."; }
            return Json(new { success = (result > 0), message = message });
        }

        // GET: Product/CreatePHPremium/5
        public ActionResult CreatePHPremium(string id) {
            CheckPermission(AdminAction.Add);
            ProductPHPremium item = new ProductPHPremium();
            item.ProductId = NullUtils.cvInt(Request.QueryString["productId"]);
            MasterOptionRepository masterOptionRepo = new MasterOptionRepository();
            ViewBag.CareerList = masterOptionRepo.ListMasterOption_SelectList_Admin(MasterOptionCategory.Career);
            return View("ProductPH", item);
        }

        // POST: Product/CreatePHPremium/5
        [HttpPost]
        public ActionResult CreatePHPremium(string id, ProductPHPremium item) {
            CheckPermission(AdminAction.Add);
            int result = 0; string message = "";
            if (CheckPremium(item.NetPremium, item.Duty, item.Vat, item.Premium)) {
                try {
                    item.Status = Status.Active;
                    item.CreateDate = item.UpdateDate = DateTime.Now;
                    item.CreateAdminId = item.UpdateAdminId = Admin.Id;
                    result = repo.AddProductPHPremium(item);
                } catch { }
            } else { message = "Premium <> (NetPremium + Duty + Vat)."; }
            return Json(new { success = (result > 0), message = message });
        }

        // GET: Product/EditPHPremium/5
        public ActionResult EditPHPremium(string id) {
            CheckPermission(AdminAction.Edit);
            ProductPHPremium item = repo.GetProductPHPremium_Admin(id);
            //MasterOptionRepository masterOptionRepo = new MasterOptionRepository();
            //ViewBag.CareerList = masterOptionRepo.ListMasterOption_SelectList_Admin(MasterOptionCategory.Career);
            return View("ProductPH", item);
        }

        // POST: Product/EditPHPremium/5
        [HttpPost]
        public ActionResult EditPHPremium(string id, ProductPHPremium item) {
            CheckPermission(AdminAction.Edit);
            int result = 0; string message = "";
            if (CheckPremium(item.NetPremium, item.Duty, item.Vat, item.Premium)) {
                try {
                    item.UpdateDate = DateTime.Now;
                    item.UpdateAdminId = Admin.Id;
                    result = repo.UpdateProductPHPremium(item);
                } catch { }
            } else { message = "Premium <> (NetPremium + Duty + Vat)."; }
            return Json(new { success = (result > 0), message = message });
        }

        // GET: Product/CreateTAPremium/5
        public ActionResult CreateTAPremium(string id) {
            CheckPermission(AdminAction.Add);
            ProductTAPremium item = new ProductTAPremium();
            item.ProductId = NullUtils.cvInt(Request.QueryString["productId"]);
            return View("ProductTA", item);
        }

        // POST: Product/CreateTAPremium/5
        [HttpPost]
        public ActionResult CreateTAPremium(string id, ProductTAPremium item) {
            CheckPermission(AdminAction.Add);
            int result = 0; string message = "";
            if (CheckPremium(item.NetPremium, item.Duty, item.Vat, item.Premium)) {
                try {
                    item.Status = Status.Active;
                    item.CreateDate = item.UpdateDate = DateTime.Now;
                    item.CreateAdminId = item.UpdateAdminId = Admin.Id;
                    result = repo.AddProductTAPremium(item);
                } catch { }
            } else { message = "Premium <> (NetPremium + Duty + Vat)."; }
            return Json(new { success = (result > 0), message = message });
        }

        // GET: Product/EditTAPremium/5
        public ActionResult EditTAPremium(string id) {
            CheckPermission(AdminAction.Edit);
            ProductTAPremium item = repo.GetProductTAPremium_Admin(id);
            return View("ProductTA", item);
        }

        // POST: Product/EditPAPremium/5
        [HttpPost]
        public ActionResult EditTAPremium(string id, ProductTAPremium item) {
            CheckPermission(AdminAction.Edit);
            int result = 0; string message = "";
            if (CheckPremium(item.NetPremium, item.Duty, item.Vat, item.Premium)) {
                try {
                    item.UpdateDate = DateTime.Now;
                    item.UpdateAdminId = Admin.Id;
                    result = repo.UpdateProductTAPremium(item);
                } catch { }
            } else { message = "Premium <> (NetPremium + Duty + Vat)."; }
            return Json(new { success = (result > 0), message = message });
        }

        // GET: Product/CreateMotorCar/5
        public ActionResult CreateMotorCar(string id) {
            CheckPermission(AdminAction.Add);
            ProductCarModel item = new ProductCarModel();
            item.ProductId = NullUtils.cvInt(Request.QueryString["productId"]);
            CarRepository carRepo = new CarRepository();
            ViewBag.BrandList = carRepo.ListCarBrand_SelectList_Admin();
            return View("ProductMotorCar", item);
        }

        // POST: Product/CreateMotorCar/5
        [HttpPost]
        public ActionResult CreateMotorCar(string id, ProductCarModel item) {
            CheckPermission(AdminAction.Add);
            int result = 0;
            try {
                result = repo.AddProductMotorCar(item);
            } catch {
            }
            return Json(new { success = (result > 0), message = "Data already exists." });
        }

        // GET: Product/CreateMotorProvince/5
        public ActionResult CreateMotorProvince(string id) {
            CheckPermission(AdminAction.Add);
            ProductRegisterProvince item = new ProductRegisterProvince();
            item.ProductId = NullUtils.cvInt(Request.QueryString["productId"]);
            MasterOptionRepository masterOptionRepo = new MasterOptionRepository();
            ViewBag.ProvinceList = masterOptionRepo.ListMasterOption_SelectList_Admin(MasterOptionCategory.MotorProvince);
            return View("ProductMotorProvince", item);
        }

        // POST: Product/CreateMotorProvince/5
        [HttpPost]
        public ActionResult CreateMotorProvince(string id, ProductRegisterProvince item) {
            CheckPermission(AdminAction.Add);
            int result = 0;
            try {
                result = repo.AddProductMotorProvince(item);
            } catch {
                result = repo.UpdateProductMotorProvince(item);
            }
            return Json(new { success = (result > 0) });
        }

        // GET: Product/EditMotorProvince/5
        public ActionResult EditMotorProvince(string id) {
            CheckPermission(AdminAction.Edit);
            ProductRegisterProvince item = repo.GetProductMotorProvince_Admin(id);
            return View("ProductMotorProvince", item);
        }

        // POST: Product/EditMotorProvince/5
        [HttpPost]
        public ActionResult EditMotorProvince(string id, ProductRegisterProvince item) {
            CheckPermission(AdminAction.Edit);
            int result = 0;
            try {
                result = repo.UpdateProductMotorProvince(item);
            } catch { }
            return Json(new { success = (result > 0) });
        }

        // GET: Product/CreatePACareer/5
        public ActionResult CreatePACareer(string id) {
            CheckPermission(AdminAction.Add);
            ProductCareerClass item = new ProductCareerClass();
            item.ProductId = NullUtils.cvInt(Request.QueryString["productId"]);
            MasterOptionRepository masterOptionRepo = new MasterOptionRepository();
            ViewBag.CareerList = masterOptionRepo.ListMasterOption_SelectList_Admin(MasterOptionCategory.Career);
            return View("ProductCareer", item);
        }

        // POST: Product/CreatePACareer/5
        [HttpPost]
        public ActionResult CreatePACareer(string id, ProductCareerClass item) {
            CheckPermission(AdminAction.Add);
            int result = 0;
            try {
                result = repo.AddProductPACareer(item);
            } catch {
                result = repo.UpdateProductPACareer(item);
            }
            return Json(new { success = (result > 0) });
        }

        // GET: Product/EditPACareer/5
        public ActionResult EditPACareer(string id) {
            CheckPermission(AdminAction.Edit);
            ProductCareerClass item = repo.GetProductPACareer_Admin(id);
            return View("ProductCareer", item);
        }

        // POST: Product/EditPACareer/5
        [HttpPost]
        public ActionResult EditPACareer(string id, ProductCareerClass item) {
            CheckPermission(AdminAction.Edit);
            int result = 0;
            try {
                result = repo.UpdateProductPACareer(item);
            } catch { }
            return Json(new { success = (result > 0) });
        }

        // GET: Product/CreatePHCareer/5
        public ActionResult CreatePHCareer(string id) {
            CheckPermission(AdminAction.Add);
            ProductCareerClass item = new ProductCareerClass();
            item.ProductId = NullUtils.cvInt(Request.QueryString["productId"]);
            MasterOptionRepository masterOptionRepo = new MasterOptionRepository();
            ViewBag.CareerList = masterOptionRepo.ListMasterOption_SelectList_Admin(MasterOptionCategory.Career);
            return View("ProductCareer", item);
        }

        // POST: Product/CreatePHCareer/5
        [HttpPost]
        public ActionResult CreatePHCareer(string id, ProductCareerClass item) {
            CheckPermission(AdminAction.Add);
            int result = 0;
            try {
                result = repo.AddProductPHCareer(item);
            } catch {
                result = repo.UpdateProductPHCareer(item);
            }
            return Json(new { success = (result > 0) });
        }

        // GET: Product/EditPHCareer/5
        public ActionResult EditPHCareer(string id) {
            CheckPermission(AdminAction.Edit);
            ProductCareerClass item = repo.GetProductPHCareer_Admin(id);
            return View("ProductCareer", item);
        }

        // POST: Product/EditPHCareer/5
        [HttpPost]
        public ActionResult EditPHCareer(string id, ProductCareerClass item) {
            CheckPermission(AdminAction.Edit);
            int result = 0;
            try {
                result = repo.UpdateProductPHCareer(item);
            } catch { }
            return Json(new { success = (result > 0) });
        }

        // GET: Product/Export/5
        public ActionResult Export(int id = 0) {
            ProductCategoryKey category = ProductCategoryKey.Motor;
            try { category = (ProductCategoryKey)NullUtils.cvInt(Request.Params["category"]); } catch { }
            Product item = null;
            if (id > 0) item = repo.GetProduct_Admin(id);
            if (item != null) {
                category = item.CategoryId;
            } else {
                item = new Product();
            }
            ExportBulk(new List<Product>() { item }, category);
            
            return RedirectToAction("Index");
        }

        private void ExportBulk(List<Product> list, ProductCategoryKey category) {
            using (ExcelPackage pck = new ExcelPackage(new FileInfo(Server.MapPath("~/assets/downloads/product/template_" + category.ToString().ToLower() + ".xlsx")), false)) {
                ExcelWorksheet wsProductTH = pck.Workbook.Worksheets["Product (TH)"];
                ExcelWorksheet wsProductEN = pck.Workbook.Worksheets["Product (EN)"];
                ExcelWorksheet wsCoverageTH = pck.Workbook.Worksheets["Coverage (TH)"];
                ExcelWorksheet wsCoverageEN = pck.Workbook.Worksheets["Coverage (EN)"];
                ExcelWorksheet wsCheckListTH = pck.Workbook.Worksheets["CheckList (TH)"];
                ExcelWorksheet wsCheckListEN = pck.Workbook.Worksheets["CheckList (EN)"];
                ExcelWorksheet wsPrivilegeTH = pck.Workbook.Worksheets["Privilege (TH)"];
                ExcelWorksheet wsPrivilegeEN = pck.Workbook.Worksheets["Privilege (EN)"];

                int column = 0;
                int lastRow = 0; 
                int maxColumn = 0; int maxRow = 7;
                foreach (var itemx in list) {
                    itemx.CategoryId = category;
                    var item = repo.GetProduct_Admin(itemx.Id);
                    if (item == null) { item = itemx; item.CategoryId = category; }
                    if (item != null) {
                        int colCoverage = (column * 2);
                        wsCoverageTH.Cells[2, 3 + colCoverage].Value = wsCoverageEN.Cells[2, 3 + colCoverage].Value = wsCheckListTH.Cells[2, 3 + column].Value = wsCheckListEN.Cells[2, 3 + column].Value = wsPrivilegeTH.Cells[2, 3 + column].Value = wsPrivilegeEN.Cells[2, 3 + column].Value = item.InsuranceProductCode;
                        wsCoverageTH.Cells[3, 3 + colCoverage].Value = wsCoverageEN.Cells[3, 3 + colCoverage].Value = wsCheckListTH.Cells[3, 3 + column].Value = wsCheckListEN.Cells[3, 3 + column].Value = wsPrivilegeTH.Cells[3, 3 + column].Value = wsPrivilegeEN.Cells[3, 3 + column].Value = item.TitleTH;
                        var coverageTH = SplitTableToRow(item.CoverageTH, 30);
                        for (int i = 0; i < coverageTH.Item1.Length; i++) wsCoverageTH.Cells[(5 + i), 3 + colCoverage].Value = coverageTH.Item1[i];
                        for (int i = 0; i < coverageTH.Item2.Length; i++) wsCoverageTH.Cells[(5 + i), 4 + colCoverage].Value = coverageTH.Item2[i];
                        var coverageEN = SplitTableToRow(item.CoverageEN, 30);
                        for (int i = 0; i < coverageEN.Item1.Length; i++) wsCoverageEN.Cells[(5 + i), 3 + colCoverage].Value = coverageEN.Item1[i];
                        for (int i = 0; i < coverageEN.Item2.Length; i++) wsCoverageEN.Cells[(5 + i), 4 + colCoverage].Value = coverageEN.Item2[i];
                        var specialCoverageTH = SplitStringToRow(item.SpecialCoverageTH, 30);
                        for (int i = 0; i < specialCoverageTH.Length; i++) wsCoverageTH.Cells[(36 + i), 3 + colCoverage].Value = specialCoverageTH[i];
                        var specialCoverageEN = SplitStringToRow(item.SpecialCoverageEN, 30);
                        for (int i = 0; i < specialCoverageEN.Length; i++) wsCoverageEN.Cells[(36 + i), 3 + colCoverage].Value = specialCoverageEN[i];
                        var conditionTH = SplitStringToRow(item.ConditionTH, 30);
                        for (int i = 0; i < conditionTH.Length; i++) wsCoverageTH.Cells[(67 + i), 3 + colCoverage].Value = conditionTH[i];
                        var conditionEN = SplitStringToRow(item.ConditionEN, 30);
                        for (int i = 0; i < conditionEN.Length; i++) wsCoverageEN.Cells[(67 + i), 3 + colCoverage].Value = conditionEN[i];
                        var exceptionTH = SplitStringToRow(item.ExceptionTH, 30);
                        for (int i = 0; i < exceptionTH.Length; i++) wsCoverageTH.Cells[(98 + i), 3 + colCoverage].Value = exceptionTH[i];
                        var exceptionEN = SplitStringToRow(item.ExceptionEN, 30);
                        for (int i = 0; i < exceptionEN.Length; i++) wsCoverageEN.Cells[(98 + i), 3 + colCoverage].Value = exceptionEN[i];
                        var consentTH = SplitStringToRow(item.ConsentTH, 30);
                        for (int i = 0; i < consentTH.Length; i++) wsCoverageTH.Cells[(129 + i), 3 + colCoverage].Value = consentTH[i];
                        var consentEN = SplitStringToRow(item.ConsentEN, 30);
                        for (int i = 0; i < consentEN.Length; i++) wsCoverageEN.Cells[(129 + i), 3 + colCoverage].Value = consentEN[i]; 

                        var checkListTH = SplitStringToRow(item.CheckListTH, 30);
                        for (int i = 0; i < checkListTH.Length; i++) wsCheckListTH.Cells[(5 + i), 3 + column].Value = checkListTH[i];
                        var checkListEN = SplitStringToRow(item.CheckListEN, 30);
                        for (int i = 0; i < checkListEN.Length; i++) wsCheckListEN.Cells[(5 + i), 3 + column].Value = checkListEN[i];

                        var privilegeTH = SplitStringToRow(item.PrivilegeTH, 30);
                        for (int i = 0; i < privilegeTH.Length; i++) wsPrivilegeTH.Cells[(5 + i), 3 + column].Value = privilegeTH[i];
                        var privilegeEN = SplitStringToRow(item.PrivilegeEN, 30);
                        for (int i = 0; i < privilegeEN.Length; i++) wsPrivilegeEN.Cells[(5 + i), 3 + column].Value = privilegeEN[i];
                    }

                    if (category == ProductCategoryKey.Motor) {
                        maxColumn = 34;

                        ExcelWorksheet wsCarModel = pck.Workbook.Worksheets["Car_Model"];
                        if (item != null) {
                            wsCarModel.Cells[2, 11 + column].Value = item.InsuranceProductCode;
                            wsCarModel.Cells[3, 11 + column].Value = item.TitleTH;
                        }
                        var cars = repo.GetProductMotorCarModelAll_Admin(item.Id);
                        for (int i = 0; i < cars.Count(); i++) {
                            if (column == 0) {
                                wsCarModel.Cells[i + 5, 2].Value = cars[i].Id;
                                wsCarModel.Cells[i + 5, 3].Value = cars[i].Brand;
                                wsCarModel.Cells[i + 5, 4].Value = cars[i].Family;
                                wsCarModel.Cells[i + 5, 5].Value = cars[i].Spec;
                                wsCarModel.Cells[i + 5, 6].Value = cars[i].CC;
                                wsCarModel.Cells[i + 5, 7].Value = cars[i].KG;
                                wsCarModel.Cells[i + 5, 8].Value = cars[i].Seat;
                                wsCarModel.Cells[i + 5, 9].Value = cars[i].Door;
                                wsCarModel.Cells[i + 5, 10].Value = cars[i].TitleTH;
                            }
                            if (item.Id > 0) {
                                wsCarModel.Cells[i + 5, 11 + column].Value = cars[i].Status == Status.Active ? "Y" : "N";
                            }
                        }
                        wsCarModel.Cells["B4:K4"].AutoFilter = true;
                        //for (int i = 2; i < 11; i++) {
                        //    using (ExcelRange rng = wsCarModel.Cells[6, i, cars.Count() + 4, i]) {
                        //        rng.StyleID = wsCarModel.Cells[5, i].StyleID;
                        //    }
                        //}

                        ExcelWorksheet wsProvince = pck.Workbook.Worksheets["Register_Province"];
                        if (item != null) {
                            wsProvince.Cells[2, 4 + column].Value = item.InsuranceProductCode;
                            wsProvince.Cells[3, 4 + column].Value = item.TitleTH;
                        }
                        var provinces = repo.GetProductMotorRegisterProvinceAll_Admin(item.Id);
                        var provinceList = new List<string>();
                        for (int i = 0; i < provinces.Count(); i++) {
                            wsProvince.Cells[i + 5, 2].Value = provinces[i].RegisterProvinceId;
                            wsProvince.Cells[i + 5, 3].Value = provinces[i].RegisterProvince;
                            wsProvince.Cells[i + 5, 4 + column].Value = provinces[i].RegisterProvinceCode;
                            if (provinces[i].IsUsed && !provinceList.Contains(provinces[i].RegisterProvinceCode)) provinceList.Add(provinces[i].RegisterProvinceCode);
                        }

                        var subCategoryCode = repo.ListProductSubCategory_IdCode();
                        
                        if (item != null && item.ProductMotorPremium != null) {
                            maxRow = item.ProductMotorPremium.Count() + 5;
                            int row = Math.Max(lastRow, 6);
                            foreach (var premium in item.ProductMotorPremium) {
                                var provinceCode = repo.GetProductMotorPremiumRegisterProvince(premium.Id).Select(x => x.RegisterProvinceCode).ToArray();

                                wsProductTH.Cells[row, 2].Value = row - 5;
                                wsProductTH.Cells[row, 3].Value = item.ProductCode;
                                wsProductTH.Cells[row, 4].Value = item.CompanyCode;
                                wsProductTH.Cells[row, 5].Value = item.Class;
                                wsProductTH.Cells[row, 6].Value = item.SubClass;
                                wsProductTH.Cells[row, 7].Value = item.InsuranceProductCode;
                                wsProductTH.Cells[row, 8].Value = item.TitleTH;
                                wsProductTH.Cells[row, 9].Value = premium.PlanCode;
                                wsProductTH.Cells[row, 10].Value = item.ActiveDate.ToString("dd/MM/yyyy", cultureGB);
                                wsProductTH.Cells[row, 11].Value = item.ExpireDate.ToString("dd/MM/yyyy", cultureGB);
                                wsProductTH.Cells[row, 12].Value = (item.SaleChannel == ProductSaleChannel.All ? "A" : (item.SaleChannel == ProductSaleChannel.Online ? "Y" : (item.SaleChannel == ProductSaleChannel.Offline ? "N" : "")));
                                wsProductTH.Cells[row, 13].Value = premium.EPolicy ? "Y" : "N";
                                wsProductTH.Cells[row, 14].Value = premium.SumInsured;
                                wsProductTH.Cells[row, 15].Value = premium.NetPremium;
                                wsProductTH.Cells[row, 16].Value = premium.Duty;
                                wsProductTH.Cells[row, 17].Value = premium.Vat;
                                wsProductTH.Cells[row, 18].Value = premium.Premium;
                                wsProductTH.Cells[row, 19].Value = premium.DisplayPremium;
                                wsProductTH.Cells[row, 20].Value = premium.CompulsoryPremium;
                                wsProductTH.Cells[row, 21].Value = premium.CombinePremium;
                                wsProductTH.Cells[row, 22].Value = premium.DisplayCombinePremium;
                                wsProductTH.Cells[row, 23].Value = subCategoryCode[(int)item.SubCategoryId];

                                wsProductTH.Cells[row, 24].Value = (premium.RegisterYear > 0 ? premium.RegisterYear.ToString() : (premium.YearMin > 0 && premium.YearMax > 0 ? premium.YearMin + " - " + premium.YearMax : ""));
                                wsProductTH.Cells[row, 25].Value = (premium.CCTV == ProductMotorCCTV.All ? "All" : (premium.CCTV == ProductMotorCCTV.Yes ? "Y" : "N"));

                                wsProductTH.Cells[row, 26].Value = ((premium.VehicleUsage110 ? "110," : "") + (premium.VehicleUsage120 ? "120," : "") + (premium.VehicleUsage210 ? "210," : "") + (premium.VehicleUsage220 ? "220," : "") + (premium.VehicleUsage230 ? "230," : "") + (premium.VehicleUsage320 ? "320," : "")).Trim(',');
                                wsProductTH.Cells[row, 27].Value = (premium.RegisterProvinceAll ? "All" : String.Join(",", provinceCode));
                                wsProductTH.Cells[row, 28].Value = (premium.GarageType == ProductMotorGarageType.Dealer ? "D" : "G");
                                wsProductTH.Cells[row, 29].Value = (premium.DriverAgeMin > 0 && premium.DriverAgeMax > 0 ? premium.DriverAgeMin + "-" + premium.DriverAgeMax : "");
                                wsProductTH.Cells[row, 30].Value = (premium.CarInspector ? "Y" : "N");
                                wsProductTH.Cells[row, 31].Value = (premium.CarInspectorMethod == ProductMotorCarInspectorMethod.Person ? "1" : premium.CarInspectorMethod == ProductMotorCarInspectorMethod.Attachment ? "2" : "");
                                wsProductTH.Cells[row, 32].Value = premium.OwnDamage;
                                wsProductTH.Cells[row, 33].Value = premium.OwnExcess;
                                wsProductTH.Cells[row, 34].Value = premium.Flood;

                                wsProductEN.Cells[row, 2].Value = row - 5;
                                wsProductEN.Cells[row, 3].Value = item.ProductCode;
                                wsProductEN.Cells[row, 4].Value = item.CompanyCode;
                                wsProductEN.Cells[row, 5].Value = item.Class;
                                wsProductEN.Cells[row, 6].Value = item.SubClass;
                                wsProductEN.Cells[row, 7].Value = item.InsuranceProductCode;
                                wsProductEN.Cells[row, 8].Value = item.TitleEN;
                                wsProductEN.Cells[row, 9].Value = premium.PlanCode;
                                wsProductEN.Cells[row, 10].Value = item.ActiveDate.ToString("dd/MM/yyyy", cultureGB);
                                wsProductEN.Cells[row, 11].Value = item.ExpireDate.ToString("dd/MM/yyyy", cultureGB);
                                wsProductEN.Cells[row, 12].Value = (item.SaleChannel == ProductSaleChannel.All ? "A" : (item.SaleChannel == ProductSaleChannel.Online ? "Y" : (item.SaleChannel == ProductSaleChannel.Offline ? "N" : "")));
                                wsProductEN.Cells[row, 13].Value = premium.EPolicy ? "Y" : "N";
                                wsProductEN.Cells[row, 14].Value = premium.SumInsured;
                                wsProductEN.Cells[row, 15].Value = premium.NetPremium;
                                wsProductEN.Cells[row, 16].Value = premium.Duty;
                                wsProductEN.Cells[row, 17].Value = premium.Vat;
                                wsProductEN.Cells[row, 18].Value = premium.Premium;
                                wsProductEN.Cells[row, 19].Value = premium.DisplayPremium;
                                wsProductEN.Cells[row, 20].Value = premium.CompulsoryPremium;
                                wsProductEN.Cells[row, 21].Value = premium.CombinePremium;
                                wsProductEN.Cells[row, 22].Value = premium.DisplayCombinePremium;
                                wsProductEN.Cells[row, 23].Value = subCategoryCode[(int)item.SubCategoryId];

                                wsProductEN.Cells[row, 24].Value = (premium.RegisterYear > 0 ? premium.RegisterYear.ToString() : (premium.YearMin > 0 && premium.YearMax > 0 ? premium.YearMin + " - " + premium.YearMax : ""));
                                wsProductEN.Cells[row, 25].Value = (premium.CCTV == ProductMotorCCTV.All ? "All" : (premium.CCTV == ProductMotorCCTV.Yes ? "Y" : "N"));

                                wsProductEN.Cells[row, 26].Value = ((premium.VehicleUsage110 ? "110," : "") + (premium.VehicleUsage120 ? "120," : "") + (premium.VehicleUsage210 ? "210," : "") + (premium.VehicleUsage220 ? "220," : "") + (premium.VehicleUsage230 ? "230," : "") + (premium.VehicleUsage320 ? "320," : "")).Trim(',');
                                wsProductEN.Cells[row, 27].Value = (premium.RegisterProvinceAll ? "All" : String.Join(",", provinceCode));
                                wsProductEN.Cells[row, 28].Value = (premium.GarageType == ProductMotorGarageType.Dealer ? "D" : "G");
                                wsProductEN.Cells[row, 29].Value = (premium.DriverAgeMin > 0 && premium.DriverAgeMax > 0 ? premium.DriverAgeMin + "-" + premium.DriverAgeMax : "");
                                wsProductEN.Cells[row, 30].Value = (premium.CarInspector ? "Y" : "N");
                                wsProductEN.Cells[row, 31].Value = (premium.CarInspectorMethod == ProductMotorCarInspectorMethod.Person ? "1" : premium.CarInspectorMethod == ProductMotorCarInspectorMethod.Attachment ? "2" : "");
                                wsProductEN.Cells[row, 32].Value = premium.OwnDamage;
                                wsProductEN.Cells[row, 33].Value = premium.OwnExcess;
                                wsProductEN.Cells[row, 34].Value = premium.Flood;

                                row++;
                            }
                            lastRow = row;
                        }
                    } else if (category == ProductCategoryKey.Home) {
                        maxColumn = 28;

                        MasterOptionRepository masterRepo = new MasterOptionRepository();
                        var constructionType = masterRepo.ListMasterOption_DictionaryIdTitleTH_Admin(MasterOptionCategory.ConstructionType);
                        var propertyType = masterRepo.ListMasterOption_DictionaryIdTitleTH_Admin(MasterOptionCategory.PropertyType);
                        List<string> options = new List<string>();
                        foreach (var option in masterRepo.ListMasterOption_Admin(MasterOptionCategory.PropertyType).OrderBy(x => x.Code)) {
                            options.Add("[" + option.TitleTH + "]");
                            
                        }
                        wsProductTH.Cells[5, 19].Value += string.Join(",", options);
                        wsProductEN.Cells[5, 19].Value = wsProductTH.Cells[5, 19].Value;
                        options = new List<string>();
                        foreach (var option in masterRepo.ListMasterOption_Admin(MasterOptionCategory.ConstructionType).OrderBy(x => x.Code)) {
                            options.Add("[" + option.TitleTH + "]");
                        }
                        wsProductTH.Cells[5, 20].Value += string.Join(",", options);
                        wsProductEN.Cells[5, 20].Value = wsProductTH.Cells[5, 20].Value;

                        if (item != null && item.ProductHomePremium != null) {
                            maxRow = item.ProductHomePremium.Count() + 5;
                            int row = Math.Max(lastRow, 6);
                            foreach (var premium in item.ProductHomePremium) {
                                wsProductTH.Cells[row, 2].Value = row - 5;
                                wsProductTH.Cells[row, 3].Value = item.ProductCode;
                                wsProductTH.Cells[row, 4].Value = item.CompanyCode;
                                wsProductTH.Cells[row, 5].Value = item.Class;
                                wsProductTH.Cells[row, 6].Value = item.SubClass;
                                wsProductTH.Cells[row, 7].Value = item.InsuranceProductCode;
                                wsProductTH.Cells[row, 8].Value = item.TitleTH;
                                wsProductTH.Cells[row, 9].Value = premium.PlanCode;
                                wsProductTH.Cells[row, 10].Value = item.ActiveDate.ToString("dd/MM/yyyy", cultureGB);
                                wsProductTH.Cells[row, 11].Value = item.ExpireDate.ToString("dd/MM/yyyy", cultureGB);
                                wsProductTH.Cells[row, 12].Value = (item.SaleChannel == ProductSaleChannel.All ? "A" : (item.SaleChannel == ProductSaleChannel.Online ? "Y" : (item.SaleChannel == ProductSaleChannel.Offline ? "N" : "")));
                                wsProductTH.Cells[row, 13].Value = premium.EPolicy ? "Y" : "N";
                                wsProductTH.Cells[row, 14].Value = premium.SumInsured;
                                wsProductTH.Cells[row, 15].Value = premium.NetPremium;
                                wsProductTH.Cells[row, 16].Value = premium.Duty;
                                wsProductTH.Cells[row, 17].Value = premium.Vat;
                                wsProductTH.Cells[row, 18].Value = premium.Premium;
                                wsProductTH.Cells[row, 19].Value = premium.DisplayPremium;
                                wsProductTH.Cells[row, 20].Value = propertyType[premium.PropertyTypeId];
                                wsProductTH.Cells[row, 21].Value = constructionType[premium.ConstructionTypeId];
                                wsProductTH.Cells[row, 22].Value = premium.CoverPeriod;
                                wsProductTH.Cells[row, 23].Value = premium.FireTH;
                                wsProductTH.Cells[row, 24].Value = premium.AccidentalDamageTH;
                                wsProductTH.Cells[row, 25].Value = premium.NaturalRiskTH;
                                wsProductTH.Cells[row, 26].Value = premium.TempRentalTH;
                                wsProductTH.Cells[row, 27].Value = premium.BurglaryTH;
                                wsProductTH.Cells[row, 28].Value = premium.PublicLiabilityTH;

                                wsProductEN.Cells[row, 2].Value = row - 5;
                                wsProductEN.Cells[row, 3].Value = item.ProductCode;
                                wsProductEN.Cells[row, 4].Value = item.CompanyCode;
                                wsProductEN.Cells[row, 5].Value = item.Class;
                                wsProductEN.Cells[row, 6].Value = item.SubClass;
                                wsProductEN.Cells[row, 7].Value = item.InsuranceProductCode;
                                wsProductEN.Cells[row, 8].Value = item.TitleEN;
                                wsProductEN.Cells[row, 9].Value = premium.PlanCode;
                                wsProductEN.Cells[row, 10].Value = item.ActiveDate.ToString("dd/MM/yyyy", cultureGB);
                                wsProductEN.Cells[row, 11].Value = item.ExpireDate.ToString("dd/MM/yyyy", cultureGB);
                                wsProductEN.Cells[row, 12].Value = (item.SaleChannel == ProductSaleChannel.All ? "A" : (item.SaleChannel == ProductSaleChannel.Online ? "Y" : (item.SaleChannel == ProductSaleChannel.Offline ? "N" : "")));
                                wsProductEN.Cells[row, 13].Value = premium.EPolicy ? "Y" : "N";
                                wsProductEN.Cells[row, 14].Value = premium.SumInsured;
                                wsProductEN.Cells[row, 15].Value = premium.NetPremium;
                                wsProductEN.Cells[row, 16].Value = premium.Duty;
                                wsProductEN.Cells[row, 17].Value = premium.Vat;
                                wsProductEN.Cells[row, 18].Value = premium.Premium;
                                wsProductEN.Cells[row, 19].Value = premium.DisplayPremium;
                                wsProductEN.Cells[row, 20].Value = propertyType[premium.PropertyTypeId];
                                wsProductEN.Cells[row, 21].Value = constructionType[premium.ConstructionTypeId];
                                wsProductEN.Cells[row, 22].Value = premium.CoverPeriod;
                                wsProductEN.Cells[row, 23].Value = premium.FireEN;
                                wsProductEN.Cells[row, 24].Value = premium.AccidentalDamageEN;
                                wsProductEN.Cells[row, 25].Value = premium.NaturalRiskEN;
                                wsProductEN.Cells[row, 26].Value = premium.TempRentalEN;
                                wsProductEN.Cells[row, 27].Value = premium.BurglaryEN;
                                wsProductEN.Cells[row, 28].Value = premium.PublicLiabilityEN;
                                row++;
                            }
                            lastRow = row;
                        }
                    } else if (category == ProductCategoryKey.PA) {
                        maxColumn = 29;

                        ExcelWorksheet wsCareer = pck.Workbook.Worksheets["Career"];
                        var careers = repo.GetProductPACareerAll_Admin(item.Id);
                        for (int i = 0; i < careers.Count(); i++) {
                            wsCareer.Cells[i + 5, 2].Value = careers[i].Career;
                            wsCareer.Cells[i + 5, 3 + column].Value = ((int)careers[i].CareerClass == 0 ? "" : careers[i].CareerClass.ToString());
                        }
                        using (ExcelRange rng = wsCareer.Cells[6, 2, careers.Count() + 4, 2]) {
                            rng.StyleID = wsCareer.Cells[5, 2].StyleID;
                        }
                        using (ExcelRange rng = wsCareer.Cells[6, 3, careers.Count() + 4, 3]) {
                            rng.StyleID = wsCareer.Cells[5, 3].StyleID;
                        }


                        if (item != null && item.ProductPAPremium != null) {
                            maxRow = item.ProductPAPremium.Count() + 5;
                            int row = Math.Max(lastRow, 6);
                            wsCareer.Cells[2, 3].Value = item.InsuranceProductCode;
                            wsCareer.Cells[3, 3].Value = item.TitleTH;
                            foreach (var premium in item.ProductPAPremium) {
                                wsProductTH.Cells[row, 2].Value = row - 5;
                                wsProductTH.Cells[row, 3].Value = item.ProductCode;
                                wsProductTH.Cells[row, 4].Value = item.CompanyCode;
                                wsProductTH.Cells[row, 5].Value = item.Class;
                                wsProductTH.Cells[row, 6].Value = item.SubClass;
                                wsProductTH.Cells[row, 7].Value = item.InsuranceProductCode;
                                wsProductTH.Cells[row, 8].Value = item.TitleTH;
                                wsProductTH.Cells[row, 9].Value = premium.PlanCode;
                                wsProductTH.Cells[row, 10].Value = item.ActiveDate.ToString("dd/MM/yyyy", cultureGB);
                                wsProductTH.Cells[row, 11].Value = item.ExpireDate.ToString("dd/MM/yyyy", cultureGB);
                                wsProductTH.Cells[row, 12].Value = (item.SaleChannel == ProductSaleChannel.All ? "A" : (item.SaleChannel == ProductSaleChannel.Online ? "Y" : (item.SaleChannel == ProductSaleChannel.Offline ? "N" : "")));
                                wsProductTH.Cells[row, 13].Value = premium.EPolicy ? "Y" : "N";
                                wsProductTH.Cells[row, 14].Value = premium.SumInsured;
                                wsProductTH.Cells[row, 15].Value = premium.NetPremium;
                                wsProductTH.Cells[row, 16].Value = premium.Duty;
                                wsProductTH.Cells[row, 17].Value = premium.Vat;
                                wsProductTH.Cells[row, 18].Value = premium.Premium;
                                wsProductTH.Cells[row, 19].Value = premium.DisplayPremium;
                                wsProductTH.Cells[row, 20].Value = ((premium.CareerClass1 ? "1," : "") + (premium.CareerClass2 ? "2," : "") + (premium.CareerClass3 ? "3," : "") + (premium.CareerClass4 ? "4," : "")).Trim(',');
                                wsProductTH.Cells[row, 21].Value = (premium.MinAge + "-" + premium.MaxAge);
                                wsProductTH.Cells[row, 22].Value = (premium.AgeCalculation == ProductAgeCalculation.Ceiling ? "ปัด" : "ครบ");
                                wsProductTH.Cells[row, 23].Value = premium.InsuranceType.GetDisplayName();
                                wsProductTH.Cells[row, 24].Value = premium.LossLifeTH;
                                wsProductTH.Cells[row, 25].Value = premium.MedicalExpenseTH;
                                wsProductTH.Cells[row, 26].Value = premium.IncomeCompensationTH;
                                wsProductTH.Cells[row, 27].Value = premium.MurderAssaultTH;
                                wsProductTH.Cells[row, 28].Value = premium.RidingPassengerMotorcycleTH;
                                wsProductTH.Cells[row, 29].Value = premium.FuneralExpenseAccidentIllnessTH;

                                wsProductEN.Cells[row, 2].Value = row - 5;
                                wsProductEN.Cells[row, 3].Value = item.ProductCode;
                                wsProductEN.Cells[row, 4].Value = item.CompanyCode;
                                wsProductEN.Cells[row, 5].Value = item.Class;
                                wsProductEN.Cells[row, 6].Value = item.SubClass;
                                wsProductEN.Cells[row, 7].Value = item.InsuranceProductCode;
                                wsProductEN.Cells[row, 8].Value = item.TitleEN;
                                wsProductEN.Cells[row, 9].Value = premium.PlanCode;
                                wsProductEN.Cells[row, 10].Value = item.ActiveDate.ToString("dd/MM/yyyy", cultureGB);
                                wsProductEN.Cells[row, 12].Value = item.ExpireDate.ToString("dd/MM/yyyy", cultureGB);
                                wsProductEN.Cells[row, 12].Value = (item.SaleChannel == ProductSaleChannel.All ? "A" : (item.SaleChannel == ProductSaleChannel.Online ? "Y" : (item.SaleChannel == ProductSaleChannel.Offline ? "N" : "")));
                                wsProductEN.Cells[row, 13].Value = premium.EPolicy ? "Y" : "N";
                                wsProductEN.Cells[row, 14].Value = premium.SumInsured;
                                wsProductEN.Cells[row, 15].Value = premium.NetPremium;
                                wsProductEN.Cells[row, 16].Value = premium.Duty;
                                wsProductEN.Cells[row, 17].Value = premium.Vat;
                                wsProductEN.Cells[row, 18].Value = premium.Premium;
                                wsProductEN.Cells[row, 19].Value = premium.DisplayPremium;
                                wsProductEN.Cells[row, 20].Value = ((premium.CareerClass1 ? "1," : "") + (premium.CareerClass2 ? "2," : "") + (premium.CareerClass3 ? "3," : "") + (premium.CareerClass4 ? "4," : "")).Trim(',');
                                wsProductEN.Cells[row, 21].Value = (premium.MinAge + "-" + premium.MaxAge);
                                wsProductEN.Cells[row, 22].Value = (premium.AgeCalculation == ProductAgeCalculation.Ceiling ? "ปัด" : "ครบ");
                                wsProductEN.Cells[row, 23].Value = premium.InsuranceType.GetDisplayName();
                                wsProductEN.Cells[row, 24].Value = premium.LossLifeEN;
                                wsProductEN.Cells[row, 25].Value = premium.MedicalExpenseEN;
                                wsProductEN.Cells[row, 26].Value = premium.IncomeCompensationEN;
                                wsProductEN.Cells[row, 27].Value = premium.MurderAssaultEN;
                                wsProductEN.Cells[row, 28].Value = premium.RidingPassengerMotorcycleEN;
                                wsProductEN.Cells[row, 29].Value = premium.FuneralExpenseAccidentIllnessEN;
                                row++;
                            }
                            lastRow = row;
                        }
                    } else if (category == ProductCategoryKey.PH) {
                        maxColumn = 38;

                        ExcelWorksheet wsCareer = pck.Workbook.Worksheets["Career"];
                        var careers = repo.GetProductPHCareerAll_Admin(item.Id);
                        for (int i = 0; i < careers.Count(); i++) {
                            wsCareer.Cells[i + 5, 2].Value = careers[i].Career;
                            wsCareer.Cells[i + 5, 3 + column].Value = ((int)careers[i].CareerClass == 0 ? "" : careers[i].CareerClass.ToString());
                        }
                        using (ExcelRange rng = wsCareer.Cells[6, 2, careers.Count() + 4, 2]) {
                            rng.StyleID = wsCareer.Cells[5, 2].StyleID;
                        }
                        using (ExcelRange rng = wsCareer.Cells[6, 3, careers.Count() + 4, 3]) {
                            rng.StyleID = wsCareer.Cells[5, 3].StyleID;
                        }

                        if (item != null && item.ProductPHPremium != null) {
                            maxRow = item.ProductPHPremium.Count() + 5;
                            int row = 6;
                            wsCareer.Cells[2, 3].Value = item.InsuranceProductCode;
                            wsCareer.Cells[3, 3].Value = item.TitleTH;
                            foreach (var premium in item.ProductPHPremium) {
                                wsProductTH.Cells[row, 2].Value = row - 5;
                                wsProductTH.Cells[row, 3].Value = item.ProductCode;
                                wsProductTH.Cells[row, 4].Value = item.CompanyCode;
                                wsProductTH.Cells[row, 5].Value = item.Class;
                                wsProductTH.Cells[row, 6].Value = item.SubClass;
                                wsProductTH.Cells[row, 7].Value = item.InsuranceProductCode;
                                wsProductTH.Cells[row, 8].Value = item.TitleTH;
                                wsProductTH.Cells[row, 9].Value = premium.PlanCode;
                                wsProductTH.Cells[row, 10].Value = item.ActiveDate.ToString("dd/MM/yyyy", cultureGB);
                                wsProductTH.Cells[row, 11].Value = item.ExpireDate.ToString("dd/MM/yyyy", cultureGB);
                                wsProductTH.Cells[row, 12].Value = (item.SaleChannel == ProductSaleChannel.All ? "A" : (item.SaleChannel == ProductSaleChannel.Online ? "Y" : (item.SaleChannel == ProductSaleChannel.Offline ? "N" : "")));
                                wsProductTH.Cells[row, 13].Value = premium.EPolicy ? "Y" : "N";
                                wsProductTH.Cells[row, 14].Value = premium.SumInsured;
                                wsProductTH.Cells[row, 15].Value = premium.NetPremium;
                                wsProductTH.Cells[row, 16].Value = premium.Duty;
                                wsProductTH.Cells[row, 17].Value = premium.Vat;
                                wsProductTH.Cells[row, 18].Value = premium.Premium;
                                wsProductTH.Cells[row, 19].Value = premium.DisplayPremium;
                                wsProductTH.Cells[row, 20].Value = ((premium.CareerClass1 ? "1," : "") + (premium.CareerClass2 ? "2," : "") + (premium.CareerClass3 ? "3," : "") + (premium.CareerClass4 ? "4," : "")).Trim(',');
                                wsProductTH.Cells[row, 21].Value = ((premium.MinAge > 0 ? premium.MinAge.ToString() : "0." + premium.MinAgeMonth.ToString()) + "-" + premium.MaxAge);
                                wsProductTH.Cells[row, 22].Value = (premium.AgeCalculation == ProductAgeCalculation.Ceiling ? "ปัด" : "ครบ");
                                wsProductTH.Cells[row, 23].Value = (premium.Sex == Sex.Female ? "หญิง" : "ชาย");
                                wsProductTH.Cells[row, 24].Value = (premium.MinWeight + "-" + premium.MaxWeight);
                                wsProductTH.Cells[row, 25].Value = (premium.MinHeight + "-" + premium.MaxHeight);

                                wsProductTH.Cells[row, 26].Value = premium.IPDTH;
                                wsProductTH.Cells[row, 27].Value = premium.OutpatientBenefitsTH;
                                wsProductTH.Cells[row, 28].Value = premium.IPDRoomBoardTH;
                                wsProductTH.Cells[row, 29].Value = premium.SurgicalExpenseTH;
                                wsProductTH.Cells[row, 30].Value = premium.CancerTreatmentTH;
                                wsProductTH.Cells[row, 31].Value = premium.OrganTransplantTH;

                                wsProductEN.Cells[row, 2].Value = row - 5;
                                wsProductEN.Cells[row, 3].Value = item.ProductCode;
                                wsProductEN.Cells[row, 4].Value = item.CompanyCode;
                                wsProductEN.Cells[row, 5].Value = item.Class;
                                wsProductEN.Cells[row, 6].Value = item.SubClass;
                                wsProductEN.Cells[row, 7].Value = item.InsuranceProductCode;
                                wsProductEN.Cells[row, 8].Value = item.TitleEN;
                                wsProductEN.Cells[row, 9].Value = premium.PlanCode;
                                wsProductEN.Cells[row, 10].Value = item.ActiveDate.ToString("dd/MM/yyyy", cultureGB);
                                wsProductEN.Cells[row, 11].Value = item.ExpireDate.ToString("dd/MM/yyyy", cultureGB);
                                wsProductEN.Cells[row, 12].Value = (item.SaleChannel == ProductSaleChannel.All ? "A" : (item.SaleChannel == ProductSaleChannel.Online ? "Y" : (item.SaleChannel == ProductSaleChannel.Offline ? "N" : "")));
                                wsProductEN.Cells[row, 13].Value = premium.EPolicy ? "Y" : "N";
                                wsProductEN.Cells[row, 14].Value = premium.SumInsured;
                                wsProductEN.Cells[row, 15].Value = premium.NetPremium;
                                wsProductEN.Cells[row, 16].Value = premium.Duty;
                                wsProductEN.Cells[row, 17].Value = premium.Vat;
                                wsProductEN.Cells[row, 18].Value = premium.Premium;
                                wsProductEN.Cells[row, 19].Value = premium.DisplayPremium;
                                wsProductEN.Cells[row, 20].Value = ((premium.CareerClass1 ? "1," : "") + (premium.CareerClass2 ? "2," : "") + (premium.CareerClass3 ? "3," : "") + (premium.CareerClass4 ? "4," : "")).Trim(',');
                                wsProductEN.Cells[row, 21].Value = ((premium.MinAge > 0 ? premium.MinAge.ToString() : "0." + premium.MinAgeMonth.ToString()) + "-" + premium.MaxAge);
                                wsProductEN.Cells[row, 22].Value = (premium.AgeCalculation == ProductAgeCalculation.Ceiling ? "ปัด" : "ครบ");
                                wsProductEN.Cells[row, 23].Value = (premium.Sex == Sex.Female ? "หญิง" : "ชาย");
                                wsProductEN.Cells[row, 24].Value = (premium.MinWeight + "-" + premium.MaxWeight);
                                wsProductEN.Cells[row, 25].Value = (premium.MinHeight + "-" + premium.MaxHeight);

                                wsProductEN.Cells[row, 26].Value = premium.IPDEN;
                                wsProductEN.Cells[row, 27].Value = premium.OutpatientBenefitsEN;
                                wsProductEN.Cells[row, 28].Value = premium.IPDRoomBoardEN;
                                wsProductEN.Cells[row, 29].Value = premium.SurgicalExpenseEN;
                                wsProductEN.Cells[row, 30].Value = premium.CancerTreatmentEN;
                                wsProductEN.Cells[row, 31].Value = premium.OrganTransplantEN;
                                row++;
                            }
                        }

                    } else if (category == ProductCategoryKey.TA) {
                        maxColumn = 32;

                        ExcelWorksheet wsCountry = pck.Workbook.Worksheets["Country"];
                        var countries = repo.GetProductTACountryAll_Admin(item.Id);
                        for (int i = 0; i < countries.Count(); i++) {
                            wsCountry.Cells[i + 5, 2].Value = countries[i].TitleEN;
                            wsCountry.Cells[i + 5, 3 + column].Value = countries[i].Code;
                        }
                        using (ExcelRange rng = wsCountry.Cells[6, 2, countries.Count() + 4, 2]) {
                            rng.StyleID = wsCountry.Cells[5, 2].StyleID;
                        }
                        using (ExcelRange rng = wsCountry.Cells[6, 3, countries.Count() + 4, 3]) {
                            rng.StyleID = wsCountry.Cells[5, 3].StyleID;
                        }

                        if (item != null && item.ProductTAPremium != null) {
                            maxRow = item.ProductTAPremium.Count() + 5;
                            int row = Math.Max(lastRow, 6);
                            wsCountry.Cells[2, 3].Value = item.InsuranceProductCode;
                            wsCountry.Cells[3, 3].Value = item.TitleTH;
                            foreach (var premium in item.ProductTAPremium) {
                                wsProductTH.Cells[row, 2].Value = row - 5;
                                wsProductTH.Cells[row, 3].Value = item.ProductCode;
                                wsProductTH.Cells[row, 4].Value = item.CompanyCode;
                                wsProductTH.Cells[row, 5].Value = item.Class;
                                wsProductTH.Cells[row, 6].Value = item.SubClass;
                                wsProductTH.Cells[row, 7].Value = item.InsuranceProductCode;
                                wsProductTH.Cells[row, 8].Value = item.TitleTH;
                                wsProductTH.Cells[row, 9].Value = premium.PlanCode;
                                wsProductTH.Cells[row, 10].Value = item.ActiveDate.ToString("dd/MM/yyyy", cultureGB);
                                wsProductTH.Cells[row, 11].Value = item.ExpireDate.ToString("dd/MM/yyyy", cultureGB);
                                wsProductTH.Cells[row, 12].Value = (item.SaleChannel == ProductSaleChannel.All ? "A" : (item.SaleChannel == ProductSaleChannel.Online ? "Y" : (item.SaleChannel == ProductSaleChannel.Offline ? "N" : "")));
                                wsProductTH.Cells[row, 13].Value = premium.EPolicy ? "Y" : "N";
                                wsProductTH.Cells[row, 14].Value = premium.SumInsured;
                                wsProductTH.Cells[row, 15].Value = premium.NetPremium;
                                wsProductTH.Cells[row, 16].Value = premium.Duty;
                                wsProductTH.Cells[row, 17].Value = premium.Vat;
                                wsProductTH.Cells[row, 18].Value = premium.Premium;
                                wsProductTH.Cells[row, 19].Value = premium.DisplayPremium;
                                wsProductTH.Cells[row, 20].Value = premium.TripType.GetDisplayName();
                                wsProductTH.Cells[row, 21].Value = premium.Zone.GetDisplayName();
                                wsProductTH.Cells[row, 22].Value = premium.CoverageOption.GetDisplayName();
                                wsProductTH.Cells[row, 23].Value = premium.CoverageType.GetDisplayName();
                                wsProductTH.Cells[row, 24].Value = (premium.MinDays + "-" + premium.MaxDays);
                                wsProductTH.Cells[row, 25].Value = premium.Persons;
                                wsProductTH.Cells[row, 26].Value = premium.PersonalLossLifeTH;
                                wsProductTH.Cells[row, 27].Value = premium.MedicalExpenseEachAccidentTH;
                                wsProductTH.Cells[row, 28].Value = premium.MedicalExpenseAccidentSicknessTH;
                                wsProductTH.Cells[row, 29].Value = premium.EmergencyMedialEvacuationTH;
                                wsProductTH.Cells[row, 30].Value = premium.RepatriationExpenseTH;
                                wsProductTH.Cells[row, 31].Value = premium.RepatriationMortalRemainsTH;
                                wsProductTH.Cells[row, 32].Value = premium.LossDamageBaggageTH;

                                wsProductEN.Cells[row, 2].Value = row - 5;
                                wsProductEN.Cells[row, 3].Value = item.ProductCode;
                                wsProductEN.Cells[row, 4].Value = item.CompanyCode;
                                wsProductEN.Cells[row, 5].Value = item.Class;
                                wsProductEN.Cells[row, 6].Value = item.SubClass;
                                wsProductEN.Cells[row, 7].Value = item.InsuranceProductCode;
                                wsProductEN.Cells[row, 8].Value = item.TitleEN;
                                wsProductEN.Cells[row, 9].Value = premium.PlanCode;
                                wsProductEN.Cells[row, 10].Value = item.ActiveDate.ToString("dd/MM/yyyy", cultureGB);
                                wsProductEN.Cells[row, 11].Value = item.ExpireDate.ToString("dd/MM/yyyy", cultureGB);
                                wsProductEN.Cells[row, 12].Value = (item.SaleChannel == ProductSaleChannel.All ? "A" : (item.SaleChannel == ProductSaleChannel.Online ? "Y" : (item.SaleChannel == ProductSaleChannel.Offline ? "N" : "")));
                                wsProductEN.Cells[row, 13].Value = premium.EPolicy ? "Y" : "N";
                                wsProductEN.Cells[row, 14].Value = premium.SumInsured;
                                wsProductEN.Cells[row, 15].Value = premium.NetPremium;
                                wsProductEN.Cells[row, 16].Value = premium.Duty;
                                wsProductEN.Cells[row, 17].Value = premium.Vat;
                                wsProductEN.Cells[row, 18].Value = premium.Premium;
                                wsProductEN.Cells[row, 19].Value = premium.DisplayPremium;
                                wsProductEN.Cells[row, 20].Value = premium.TripType.GetDisplayName();
                                wsProductEN.Cells[row, 21].Value = premium.Zone.GetDisplayName();
                                wsProductEN.Cells[row, 22].Value = premium.CoverageOption.GetDisplayName();
                                wsProductEN.Cells[row, 23].Value = premium.CoverageType.GetDisplayName();
                                wsProductEN.Cells[row, 24].Value = (premium.MinDays + "-" + premium.MaxDays);
                                wsProductEN.Cells[row, 25].Value = premium.Persons;
                                wsProductEN.Cells[row, 26].Value = premium.PersonalLossLifeEN;
                                wsProductEN.Cells[row, 27].Value = premium.MedicalExpenseEachAccidentEN;
                                wsProductEN.Cells[row, 28].Value = premium.MedicalExpenseAccidentSicknessEN;
                                wsProductEN.Cells[row, 29].Value = premium.EmergencyMedialEvacuationEN;
                                wsProductEN.Cells[row, 30].Value = premium.RepatriationExpenseEN;
                                wsProductEN.Cells[row, 31].Value = premium.RepatriationMortalRemainsEN;
                                wsProductEN.Cells[row, 32].Value = premium.LossDamageBaggageEN;
                                row++;
                            }
                            lastRow = row;
                        }
                    } else if (category == ProductCategoryKey.PRB) {
                        maxColumn = 15;

                        if (item != null && item.ProductPRB != null) {
                            maxRow = 1 + 5;
                            int row = Math.Max(lastRow, 6);
                            var premium = item.ProductPRB;
                            wsProductTH.Cells[row, 2].Value = row - 5;
                            wsProductTH.Cells[row, 3].Value = item.ProductCode;
                            wsProductTH.Cells[row, 4].Value = item.Class;
                            wsProductTH.Cells[row, 5].Value = item.SubClass;
                            wsProductTH.Cells[row, 6].Value = item.CompanyCode;
                            wsProductTH.Cells[row, 7].Value = item.InsuranceProductCode;
                            wsProductTH.Cells[row, 8].Value = item.TitleTH;
                            wsProductTH.Cells[row, 9].Value = premium.PlanCode;
                            wsProductTH.Cells[row, 10].Value = item.ActiveDate.ToString("dd/MM/yyyy", cultureGB);
                            wsProductTH.Cells[row, 11].Value = item.ExpireDate.ToString("dd/MM/yyyy", cultureGB);
                            wsProductTH.Cells[row, 12].Value = (item.SaleChannel == ProductSaleChannel.All ? "A" : (item.SaleChannel == ProductSaleChannel.Online ? "Y" : (item.SaleChannel == ProductSaleChannel.Offline ? "N" : "")));
                            wsProductTH.Cells[row, 13].Value = premium.NetPremium;
                            wsProductTH.Cells[row, 14].Value = premium.Duty;
                            wsProductTH.Cells[row, 15].Value = premium.Vat;
                            wsProductTH.Cells[row, 16].Value = premium.Premium;

                            wsProductEN.Cells[row, 2].Value = row - 5;
                            wsProductEN.Cells[row, 3].Value = item.ProductCode;
                            wsProductEN.Cells[row, 4].Value = item.Class;
                            wsProductEN.Cells[row, 5].Value = item.SubClass;
                            wsProductEN.Cells[row, 6].Value = item.CompanyCode;
                            wsProductEN.Cells[row, 7].Value = item.InsuranceProductCode;
                            wsProductEN.Cells[row, 8].Value = item.TitleEN;
                            wsProductEN.Cells[row, 9].Value = premium.PlanCode;
                            wsProductEN.Cells[row, 10].Value = item.ActiveDate.ToString("dd/MM/yyyy", cultureGB);
                            wsProductEN.Cells[row, 11].Value = item.ExpireDate.ToString("dd/MM/yyyy", cultureGB);
                            wsProductEN.Cells[row, 12].Value = (item.SaleChannel == ProductSaleChannel.All ? "A" : (item.SaleChannel == ProductSaleChannel.Online ? "Y" : (item.SaleChannel == ProductSaleChannel.Offline ? "N" : "")));
                            wsProductEN.Cells[row, 13].Value = premium.NetPremium;
                            wsProductEN.Cells[row, 14].Value = premium.Duty;
                            wsProductEN.Cells[row, 15].Value = premium.Vat;
                            wsProductEN.Cells[row, 16].Value = premium.Premium;
                            row++;
                            lastRow = row;
                        }
                    }
                    column++;
                }

                if (lastRow < 1) lastRow = 6;
                //Format the border
                using (ExcelRange rng = wsProductTH.Cells[1, 1, lastRow, maxColumn]) {
                    rng.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Top.Color.SetColor(Color.LightGray);
                    rng.Style.Border.Bottom.Color.SetColor(Color.LightGray);
                    rng.Style.Border.Left.Color.SetColor(Color.LightGray);
                    rng.Style.Border.Right.Color.SetColor(Color.LightGray);
                }
                using (ExcelRange rng = wsProductEN.Cells[1, 1, lastRow, maxColumn]) {
                    rng.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Top.Color.SetColor(Color.LightGray);
                    rng.Style.Border.Bottom.Color.SetColor(Color.LightGray);
                    rng.Style.Border.Left.Color.SetColor(Color.LightGray);
                    rng.Style.Border.Right.Color.SetColor(Color.LightGray);
                }
                using (ExcelRange rng = wsCoverageTH.Cells[1, 1, 158, (column * 2) + 2]) {
                    rng.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Top.Color.SetColor(Color.LightGray);
                    rng.Style.Border.Bottom.Color.SetColor(Color.LightGray);
                    rng.Style.Border.Left.Color.SetColor(Color.LightGray);
                    rng.Style.Border.Right.Color.SetColor(Color.LightGray);
                }
                using (ExcelRange rng = wsCoverageEN.Cells[1, 1, 158, (column * 2) + 2]) {
                    rng.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Top.Color.SetColor(Color.LightGray);
                    rng.Style.Border.Bottom.Color.SetColor(Color.LightGray);
                    rng.Style.Border.Left.Color.SetColor(Color.LightGray);
                    rng.Style.Border.Right.Color.SetColor(Color.LightGray);
                }
                using (ExcelRange rng = wsPrivilegeTH.Cells[1, 1, 34, (column) + 2]) {
                    rng.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Top.Color.SetColor(Color.LightGray);
                    rng.Style.Border.Bottom.Color.SetColor(Color.LightGray);
                    rng.Style.Border.Left.Color.SetColor(Color.LightGray);
                    rng.Style.Border.Right.Color.SetColor(Color.LightGray);
                }
                using (ExcelRange rng = wsPrivilegeEN.Cells[1, 1, 34, (column) + 2]) {
                    rng.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    rng.Style.Border.Top.Color.SetColor(Color.LightGray);
                    rng.Style.Border.Bottom.Color.SetColor(Color.LightGray);
                    rng.Style.Border.Left.Color.SetColor(Color.LightGray);
                    rng.Style.Border.Right.Color.SetColor(Color.LightGray);
                }
                //Write it back to the client
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=template_" + category.ToString().ToLower() + ".xlsx");
                Response.BinaryWrite(pck.GetAsByteArray());
                Response.End();
            }

        }

        // GET: Product/DeleteMotorPremium/5
        public ActionResult DeleteMotorPremium(string id) {
            CheckPermission(AdminAction.Delete);
            var result = repo.DeleteProductMotorPremium(id, Admin.Id);
            return Json(new { success = (result > 0) }, JsonRequestBehavior.AllowGet);
        }

        // GET: Product/DeleteHomePremium/5
        public ActionResult DeleteHomePremium(string id) {
            CheckPermission(AdminAction.Delete);
            var result = repo.DeleteProductHomePremium(id, Admin.Id);
            return Json(new { success = (result > 0) }, JsonRequestBehavior.AllowGet);
        }

        // GET: Product/DeletePAPremium/5
        public ActionResult DeletePAPremium(string id) {
            CheckPermission(AdminAction.Delete);
            var result = repo.DeleteProductPAPremium(id, Admin.Id);
            return Json(new { success = (result > 0) }, JsonRequestBehavior.AllowGet);
        }

        // GET: Product/DeletePHPremium/5
        public ActionResult DeletePHPremium(string id) {
            CheckPermission(AdminAction.Delete);
            var result = repo.DeleteProductPHPremium(id, Admin.Id);
            return Json(new { success = (result > 0) }, JsonRequestBehavior.AllowGet);
        }

        // GET: Product/DeleteTAPremium/5
        public ActionResult DeleteTAPremium(string id) {
            CheckPermission(AdminAction.Delete);
            var result = repo.DeleteProductTAPremium(id, Admin.Id);
            return Json(new { success = (result > 0) }, JsonRequestBehavior.AllowGet);
        }

        // GET: Product/DeleteMotorCar/5
        public ActionResult DeleteMotorCar(string id) {
            CheckPermission(AdminAction.Delete);
            var result = repo.DeleteProductMotorCar(id, Admin.Id);
            return Json(new { success = (result > 0) }, JsonRequestBehavior.AllowGet);
        }

        // GET: Product/DeletePACareer/5
        public ActionResult DeletePACareer(string id) {
            CheckPermission(AdminAction.Delete);
            var result = repo.DeleteProductPACareer(id, Admin.Id);
            return Json(new { success = (result > 0) }, JsonRequestBehavior.AllowGet);
        }

        // GET: Product/DeletePHCareer/5
        public ActionResult DeletePHCareer(string id) {
            CheckPermission(AdminAction.Delete);
            var result = repo.DeleteProductPHCareer(id, Admin.Id);
            return Json(new { success = (result > 0) }, JsonRequestBehavior.AllowGet);
        }

        #region DataTable

        // GET: Product/DataTable
        public ActionResult DataTable() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();
           
            //if (!Menu.Can_Approve && !Menu.Can_Assign) { //see only own content if not approver/assigner
            //    whereC.Add(" (OwnerAdminId=@ownerAdminId) ");
            //    whereParams.ownerAdminId = Admin.Id;
            //}

            if (NullUtils.cvInt(Request.Unvalidated.Form["category"]) > 0) {
                whereC.Add(" (SubCategoryId=@category) ");
                whereParams.category = NullUtils.cvInt(Request.Unvalidated.Form["category"]);
            }

            if (NullUtils.cvInt(Request.Unvalidated.Form["company"]) > 0) {
                whereC.Add(" (CompanyId=@company) ");
                whereParams.company = NullUtils.cvInt(Request.Unvalidated.Form["company"]);
            }

            if (NullUtils.cvInt(Request.Unvalidated.Form["status"]) > -1) {
                whereC.Add(" (Status=@status) ");
                whereParams.status = NullUtils.cvInt(Request.Unvalidated.Form["status"]);
            }

            if (NullUtils.cvInt(Request.Unvalidated.Form["approvestatus"]) > -1) {
                whereC.Add(" (ApproveStatus=@approvestatus) ");
                whereParams.approvestatus = NullUtils.cvInt(Request.Unvalidated.Form["approvestatus"]);
            }

            if (param.hasSearch) {
                whereC.Add(" (TitleTH LIKE @searchValue OR TitleEN LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "Id", "ProductCode", "SubCategory", "TitleTH", "ActiveDate", "ExpireDate", "UpdateDate", "Status", "ApproveStatus", "SubCategoryId" };
            DataTableData<Product> data = repo.ListProduct_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Id.ToString(),
                        x.ProductCode.ToString(),
                        x.SubCategory.ToString(),
                        x.TitleTH.ToString(),
                        x.ActiveDate.ToString("dd/MM/yyyy"),
                        x.ExpireDate.ToString("dd/MM/yyyy"),
                        x.UpdateDate.ToString("dd/MM/yyyy HH:mm"),
                        x.Status.ToString(),
                        x.ApproveStatus.ToString(),
                        (
                            HtmlActionButton(AdminAction.View, x.Id, "") +
                            HtmlActionButton(AdminAction.Edit, x.Id, "", (x.Status != Status.Active)) + //, (x.ApproveStatus == ApproveStatus.Approve)
                            (x.ApproveStatus == ApproveStatus.Approve ? HtmlActionButton(AdminAction.Disable, x.Id, "", (x.Status == Status.Inactive)) : HtmlActionButton(AdminAction.Delete, x.Id)) +
                            HtmlActionButton(AdminAction.Approve, x.Id, "", (x.ApproveStatus == ApproveStatus.Approve)) +
                            HtmlActionButton(AdminAction.Assign, x.Id, "", (x.ApproveStatus == ApproveStatus.Approve))+
                            HtmlActionButton(AdminAction.Export, x.Id, "", (x.SubCategoryId == ProductSubCategoryKey.PRB_Car || x.SubCategoryId == ProductSubCategoryKey.PRB_Pickup || x.SubCategoryId == ProductSubCategoryKey.PRB_Van))
                        )
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        // GET: Product/DataTable_Motor_Premium
        public ActionResult DataTable_Motor_Premium() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();
            
            whereC.Add(" (ProductId=@ProductId) ");
            whereParams.ProductId = Request.QueryString["product_id"];

            if (param.hasSearch) {
                whereC.Add(" (Brand LIKE @searchValue OR Model LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "RegisterYear", "YearMin", "YearMax", "SumInsured", "Premium", "Id" };
            DataTableData<ProductMotorPremium> data = repo.ListProductMotorPremium_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;

            if (string.Equals(Request.Params["view"], "1")) {
                data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.RegisterYear.ToString(),
                        x.YearMin.ToString(),
                        x.YearMax.ToString(),
                        x.SumInsured.ToString(FORMAT_MONEY),
                        x.Premium.ToString(FORMAT_MONEY),
                        (
                            HtmlActionCustomButton(AdminAction.View, x.Id.ToString(), "EditMotorPremium", "&view=1", false, "#modal")
                        )
                    }).ToList();
            } else { 
                data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.RegisterYear.ToString(),
                        x.YearMin.ToString(),
                        x.YearMax.ToString(),
                        x.SumInsured.ToString(FORMAT_MONEY),
                        x.Premium.ToString(FORMAT_MONEY),
                        (
                            HtmlActionCustomButton(AdminAction.Edit, x.Id.ToString(), "EditMotorPremium", "", false, "#modal") +
                            HtmlActionCustomButton(AdminAction.Delete, x.Id.ToString(), "DeleteMotorPremium")
                        )
                    }).ToList();
            }

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        // GET: Product/DataTable_Home_Premium
        public ActionResult DataTable_Home_Premium() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();

            whereC.Add(" (ProductId=@ProductId) ");
            whereParams.ProductId = Request.QueryString["product_id"];

            if (param.hasSearch) {
                whereC.Add(" (PropertyType LIKE @searchValue OR ConstructionType LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "PropertyType", "ConstructionType", "CoverPeriod", "SumInsured", "Premium", "Id" };
            DataTableData<ProductHomePremium> data = repo.ListProductHomePremium_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            if (string.Equals(Request.Params["view"], "1")) {
                data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.PropertyType.ToString(),
                        x.ConstructionType.ToString(),
                        x.CoverPeriod.ToString(),
                        x.SumInsured.ToString(FORMAT_MONEY),
                        x.Premium.ToString(FORMAT_MONEY),
                        (
                            HtmlActionCustomButton(AdminAction.View, x.Id.ToString(), "EditHomePremium", "&view=1", false, "#modal")
                        )
                    }).ToList();
            } else { 
                data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.PropertyType.ToString(),
                        x.ConstructionType.ToString(),
                        x.CoverPeriod.ToString(),
                        x.SumInsured.ToString(FORMAT_MONEY),
                        x.Premium.ToString(FORMAT_MONEY),
                        (
                            HtmlActionCustomButton(AdminAction.Edit, x.Id.ToString(), "EditHomePremium", "", false, "#modal") +
                            HtmlActionCustomButton(AdminAction.Delete, x.Id.ToString(), "DeleteHomePremium")
                        )
                    }).ToList();
            }

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        // GET: Product/DataTable_PA_Premium
        public ActionResult DataTable_PA_Premium() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();

            whereC.Add(" (ProductId=@ProductId) ");
            whereParams.ProductId = Request.QueryString["product_id"];

            if (param.hasSearch) {
                whereC.Add(" (MinAge LIKE @searchValue OR MaxAge LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "MinAge", "MaxAge", "InsuranceType", "Premium", "Id" };
            DataTableData<ProductPAPremium> data = repo.ListProductPAPremium_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            if (string.Equals(Request.Params["view"], "1")) {
                data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.MinAge.ToString(),
                        x.MaxAge.ToString(),
                        x.InsuranceType.GetDisplayName(),
                        x.Premium.ToString(FORMAT_MONEY),
                        (
                            HtmlActionCustomButton(AdminAction.View, x.Id.ToString(), "EditPAPremium", "&view=1", false, "#modal")
                        )
                    }).ToList();
            } else { 
                data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.MinAge.ToString(),
                        x.MaxAge.ToString(),
                        x.InsuranceType.GetDisplayName(),
                        x.Premium.ToString(FORMAT_MONEY),
                        (
                            HtmlActionCustomButton(AdminAction.Edit, x.Id.ToString(), "EditPAPremium", "", false, "#modal") +
                            HtmlActionCustomButton(AdminAction.Delete, x.Id.ToString(), "DeletePAPremium")
                        )
                    }).ToList();
            }

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        // GET: Product/DataTable_PH_Premium
        public ActionResult DataTable_PH_Premium() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();

            whereC.Add(" (ProductId=@ProductId) ");
            whereParams.ProductId = Request.QueryString["product_id"];

            if (param.hasSearch) {
                whereC.Add(" (MinAge LIKE @searchValue OR MaxAge LIKE @searchValue OR (CAST(MinAge AS VARCHAR(3)) + '.' + CAST(MinAgeMonth AS VARCHAR(3))) LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "Sex", "MinAge", "MaxAge", "SumInsured", "Premium", "Id", "MinAgeMonth" };
            DataTableData<ProductPHPremium> data = repo.ListProductPHPremium_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            if (string.Equals(Request.Params["view"], "1")) {
                data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Sex.ToString(),
                        (x.MinAgeMonth > 0 ? "0." + x.MinAgeMonth.ToString() : x.MinAge.ToString()),
                        x.MaxAge.ToString(),
                        x.SumInsured.ToString(FORMAT_MONEY),
                        x.Premium.ToString(FORMAT_MONEY),
                        (
                            HtmlActionCustomButton(AdminAction.View, x.Id.ToString(), "EditPHPremium", "&view=1", false, "#modal")
                        )
                    }).ToList();
            } else { 
                data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Sex.ToString(),
                        (x.MinAgeMonth > 0 ? "0." + x.MinAgeMonth.ToString() : x.MinAge.ToString()),
                        x.MaxAge.ToString(),
                        x.SumInsured.ToString(FORMAT_MONEY),
                        x.Premium.ToString(FORMAT_MONEY),
                        (
                            HtmlActionCustomButton(AdminAction.Edit, x.Id.ToString(), "EditPHPremium", "", false, "#modal") +
                            HtmlActionCustomButton(AdminAction.Delete, x.Id.ToString(), "DeletePHPremium")
                        )
                    }).ToList();
            }

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        // GET: Product/DataTable_TA_Premium
        public ActionResult DataTable_TA_Premium() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();

            whereC.Add(" (ProductId=@ProductId) ");
            whereParams.ProductId = Request.QueryString["product_id"];

            if (param.hasSearch) {
                whereC.Add(" (MinDays LIKE @searchValue OR MaxDays LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "MinDays", "MaxDays", "Persons", "SumInsured", "Premium", "Id" };
            DataTableData<ProductTAPremium> data = repo.ListProductTAPremium_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            if (string.Equals(Request.Params["view"], "1")) {
                data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.MinDays.ToString(),
                        x.MaxDays.ToString(),
                        x.Persons.ToString(),
                        x.SumInsured.ToString(FORMAT_MONEY),
                        x.Premium.ToString(FORMAT_MONEY),
                        (
                            HtmlActionCustomButton(AdminAction.View, x.Id.ToString(), "EditTAPremium", "&view=1", false, "#modal") 
                        )
                    }).ToList();
            } else { 
                data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.MinDays.ToString(),
                        x.MaxDays.ToString(),
                        x.Persons.ToString(),
                        x.SumInsured.ToString(FORMAT_MONEY),
                        x.Premium.ToString(FORMAT_MONEY),
                        (
                            HtmlActionCustomButton(AdminAction.Edit, x.Id.ToString(), "EditTAPremium", "", false, "#modal") +
                            HtmlActionCustomButton(AdminAction.Delete, x.Id.ToString(), "DeleteTAPremium")
                        )
                    }).ToList();
            }

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        // GET: Product/DataTable_Motor_Car
        public ActionResult DataTable_Motor_Car() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();

            whereC.Add(" (ProductId=@ProductId) ");
            whereParams.ProductId = Request.QueryString["product_id"];

            if (param.hasSearch) {
                whereC.Add(" (TitleTH LIKE @searchValue OR Brand LIKE @searchValue OR Family LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "Brand", "Family", "TitleTH", "CarModelId", "ProductId" };
            DataTableData<ProductCarModel> data = repo.ListProductMotorCar_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            if (string.Equals(Request.Params["view"], "1")) {
                data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Brand.ToString(),
                        x.Family.ToString(),
                        x.TitleTH.ToString(),
                    }).ToList();
            } else {
                data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Brand.ToString(),
                        x.Family.ToString(),
                        NullUtils.cvString(x.TitleTH).ToString(),
                        (
                            HtmlActionCustomButton(AdminAction.Delete, x.ProductId + "|" + x.CarModelId, "DeleteMotorCar")
                        )
                    }).ToList();
            }

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        // GET: Product/DataTable_Motor_Province
        public ActionResult DataTable_Motor_Province() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();

            whereC.Add(" (ProductId=@ProductId) ");
            whereParams.ProductId = Request.QueryString["product_id"];

            if (param.hasSearch) {
                whereC.Add(" (RegisterProvince LIKE @searchValue OR RegisterProvinceCode LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "RegisterProvince", "RegisterProvinceCode", "RegisterProvinceId", "ProductId" };
            DataTableData<ProductRegisterProvince> data = repo.ListProductMotorProvince_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            if (string.Equals(Request.Params["view"], "1")) {
                data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.RegisterProvince.ToString(),
                        x.RegisterProvinceCode.ToString(),
                    }).ToList();
            } else {
                data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.RegisterProvince.ToString(),
                        x.RegisterProvinceCode.ToString(),
                        (
                            HtmlActionCustomButton(AdminAction.Edit, x.ProductId + "|" + x.RegisterProvinceId, "EditMotorProvince", "", false, "#modal") +
                            HtmlActionCustomButton(AdminAction.Delete, x.ProductId + "|" + x.RegisterProvinceId, "DeleteMotorProvince")
                        )
                    }).ToList();
            }

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        // GET: Product/DataTable_PA_Career
        public ActionResult DataTable_PA_Career() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();

            whereC.Add(" (ProductId=@ProductId) ");
            whereParams.ProductId = Request.QueryString["product_id"];

            if (param.hasSearch) {
                whereC.Add(" (Career LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "Career", "CareerClass", "ProductId", "CareerId" };
            DataTableData<ProductCareerClass> data = repo.ListProductPACareer_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            if (string.Equals(Request.Params["view"], "1")) {
                data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Career.ToString(),
                        x.CareerClass.ToString()
                    }).ToList();
            } else { 
                data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Career.ToString(),
                        x.CareerClass.ToString(),
                        (
                            HtmlActionCustomButton(AdminAction.Edit, x.ProductId + "|" + x.CareerId, "EditPACareer", "", false, "#modal") +
                            HtmlActionCustomButton(AdminAction.Delete, x.ProductId + "|" + x.CareerId, "DeletePACareer")
                        )
                    }).ToList();
            }

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        // GET: Product/DataTable_PH_Career
        public ActionResult DataTable_PH_Career() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();

            whereC.Add(" (ProductId=@ProductId) ");
            whereParams.ProductId = Request.QueryString["product_id"];

            if (param.hasSearch) {
                whereC.Add(" (Career LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "Career", "CareerClass", "ProductId", "CareerId" };
            DataTableData<ProductCareerClass> data = repo.ListProductPHCareer_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            if (string.Equals(Request.Params["view"], "1")) {
                data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Career.ToString(),
                        x.CareerClass.ToString()
                    }).ToList();
            } else { 
                data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Career.ToString(),
                        x.CareerClass.ToString(),
                        (
                            HtmlActionCustomButton(AdminAction.Edit, x.ProductId + "|" + x.CareerId, "EditPHCareer", "", false, "#modal") +
                            HtmlActionCustomButton(AdminAction.Delete, x.ProductId + "|" + x.CareerId, "DeletePHCareer")
                        )
                    }).ToList();
            }

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        #endregion

        // GET: Product/SelectList_Family
        public ActionResult SelectList_CarFamily(int brandId) {
            CarRepository carRepo = new CarRepository();
            var data = carRepo.ListCarFamily_SelectList_Admin(brandId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        // GET: Product/SelectList_CarModel
        public ActionResult SelectList_CarModel(int familyId) {
            CarRepository carRepo = new CarRepository();
            var data = carRepo.ListCarModel_SelectList_Admin(familyId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        private void BindForm() {
            ProductCategoryRepository categoryRepo = new ProductCategoryRepository();
            ViewBag.SubCategoryList = categoryRepo.ListProductSubCategoryByCategory_SelectList_Admin(category);
            CompanyRepository companyRepo = new CompanyRepository();
            ViewBag.CompanyList = companyRepo.ListCompany_SelectList_Admin();
            ViewBag.GarageTypeList = new List<SelectListItem>() { new SelectListItem() { Text = "อู่ห้างฯ", Value = "true" }, new SelectListItem() { Text = "ทั่วไป", Value = "false" } };
            CountryRepository countryRepo = new CountryRepository();
            ViewBag.CountryList = countryRepo.ListCountry_SelectList_Admin(Language.EN);
        }

        #region Import

        private Tuple<ActionResultStatus, List<Product>> ImportMotor(DataSet ds) {
            Dictionary<string, Product> items = new Dictionary<string, Product>();
            ActionResultStatus result = new ActionResultStatus() { Success = true, ErrorMessage = string.Empty };
            StringBuilder sbError = new StringBuilder();
            try {
                Product item;
                ProductMotorPremium premium;
                Dictionary<string, Product> coveragePrivilege = GetCoveragePrivilege(ds);
                var companies = GetCompanies();

                var vehicleUsage = new string[] { "110", "120", "210", "220", "230", "320" };

                var subClass = repo.ListProductSubClass(ProductCategoryKey.Motor);

                Language sheetLanguage = Language.TH;
                DataTable dtProductTH = ds.Tables["Product (TH)"];
                DataTable dtProductEN = ds.Tables["Product (EN)"];

                //CarModel
                DataTable dtCarModel = ds.Tables["Car_Model"];
                Dictionary<int, string> columns_m = new Dictionary<int, string>();
                Dictionary<string, List<ProductCarModel>> productCars = new Dictionary<string, List<ProductCarModel>>();
                for (int c = 10; c < dtCarModel.Rows[1].ItemArray.Length; c++) {
                    string productCode = NullUtils.cvString(dtCarModel.Rows[1].ItemArray[c]);
                    if (!string.IsNullOrEmpty(productCode.Trim())) {
                        columns_m.Add(c, productCode);
                        productCars.Add(productCode, new List<ProductCarModel>());
                    }
                }

                var j = 0;
                foreach (DataRow drP in dtCarModel.Rows) {
                    j++;
                    if (j < 5) continue;
                    foreach (var col in columns_m.Keys) {
                        int id = NullUtils.cvInt(drP[1]);
                        if (id > 0) {
                            string pCode = NullUtils.cvString(drP[col]);
                            if (string.Equals(pCode, "Y")) {
                                productCars[columns_m[col]].Add(new ProductCarModel() { CarModelId = id });
                            }
                        }
                    }
                }

                //RegisterProvince
                DataTable dtRegisterProvince = ds.Tables["Register_Province"];
                Dictionary<int, string> columns = new Dictionary<int, string>();
                Dictionary<string, List<ProductRegisterProvince>> productProvinces = new Dictionary<string, List<ProductRegisterProvince>>();
                for (int c = 3; c < dtRegisterProvince.Rows[1].ItemArray.Length; c++) {
                    string productCode = NullUtils.cvString(dtRegisterProvince.Rows[1].ItemArray[c]);
                    if (!string.IsNullOrEmpty(productCode.Trim())) {
                        columns.Add(c, productCode);
                        productProvinces.Add(productCode, new List<ProductRegisterProvince>());
                    }
                }

                j = 0;
                foreach (DataRow drP in dtRegisterProvince.Rows) {
                    j++;
                    if (j < 5) continue;
                    foreach (var col in columns.Keys) {
                        int id = NullUtils.cvInt(drP[1]);
                        if (id > 0) {
                            string pCode = NullUtils.cvString(drP[col]);
                            productProvinces[columns[col]].Add(new ProductRegisterProvince() { RegisterProvinceId = id, RegisterProvinceCode = pCode, IsUsed = false });
                        }
                    }
                }

                var subCategoryCode = repo.ListProductSubCategory_CodeId();

                Func<DataRow, Tuple<Product, string>> importRow = dr => {
                    item = new Product();
                    StringBuilder sb = new StringBuilder();
                    try {
                        string code = NullUtils.cvString(dr[6]);
                        if (coveragePrivilege.ContainsKey(code)) {
                            Product model = coveragePrivilege[code];
                            item.CoverageTH = NullUtils.cvString(model.CoverageTH);
                            item.CoverageEN = NullUtils.cvString(model.CoverageEN);
                            item.SpecialCoverageTH = NullUtils.cvString(model.SpecialCoverageTH);
                            item.SpecialCoverageEN = NullUtils.cvString(model.SpecialCoverageEN);
                            item.ConditionTH = NullUtils.cvString(model.ConditionTH);
                            item.ConditionEN = NullUtils.cvString(model.ConditionEN);
                            item.ExceptionTH = NullUtils.cvString(model.ExceptionTH);
                            item.ExceptionEN = NullUtils.cvString(model.ExceptionEN);
                            item.PrivilegeTH = NullUtils.cvString(model.PrivilegeTH);
                            item.PrivilegeEN = NullUtils.cvString(model.PrivilegeEN);
                            item.CheckListTH = NullUtils.cvString(model.CheckListTH);
                            item.CheckListEN = NullUtils.cvString(model.CheckListEN);
                            item.ConsentTH = NullUtils.cvString(model.ConsentTH);
                            item.ConsentEN = NullUtils.cvString(model.ConsentEN);
                        } else {
                            item.CoverageTH = item.CoverageEN = "";
                            item.SpecialCoverageTH = item.SpecialCoverageEN = "";
                            item.ConditionTH = item.ConditionEN = "";
                            item.ExceptionTH = item.ExceptionEN = "";
                            item.PrivilegeTH = item.PrivilegeEN = "";
                            item.CheckListTH = item.CheckListEN = "";
                            item.ConsentTH = item.ConsentEN = "";
                        }
                        item.SaleChannel = ProductSaleChannel.All;
                        item.ProductCode = NullUtils.cvString(dr[2]);

                        item.CompanyCode = NullUtils.cvString(dr[3]);
                        if (companies.ContainsKey(dr[3].ToString())) { item.CompanyId = companies[dr[3].ToString()]; } else { sb.AppendLine("- Insurance Code Incorrect. [" + dr[3].ToString() + "]<br/>"); }

                        item.Class = NullUtils.cvString(dr[4]);
                        item.SubClass = NullUtils.cvString(dr[5]);

                        if (!subClass.Contains(item.Class + "|" + item.SubClass)) {
                            if (!subClass.Contains(item.Class)) {
                                sb.AppendLine("- Class Incorrect. [" + dr[4].ToString() + "]<br/>");
                            } else {
                                sb.AppendLine("- Sub Class Incorrect. [" + dr[5].ToString() + "]<br/>");
                            }
                        }

                        item.CategoryId = ProductCategoryKey.Motor;
                        if (subCategoryCode.ContainsKey(NullUtils.cvString(dr[22]))) {
                            item.SubCategoryId = (ProductSubCategoryKey)subCategoryCode[NullUtils.cvString(dr[22])];
                        } else {
                            sb.AppendLine("- Class of Insurance Incorrect. [" + dr[21].ToString() + "]<br/>");
                        }
                        
                        item.InsuranceProductCode = NullUtils.cvString(dr[6]);
                        if (item.InsuranceProductCode.Length > 20) sb.AppendLine("- Insurance Product Code Incorrect. [" + dr[6].ToString() + "]<br/>");
                        if (sheetLanguage == Language.TH) {
                            item.TitleTH = NullUtils.cvString(dr[7]);
                        } else {
                            item.TitleEN = NullUtils.cvString(dr[7]);
                        }
                        item.ActiveDate = GetDate(dr[9]);
                        if (item.ActiveDate == SqlMinDate()) sb.AppendLine("- Active Date Incorrect. [" + NullUtils.cvString(dr[9]) + "]<br/>");
                        item.ExpireDate = GetDate(dr[10]);
                        if (item.ExpireDate == SqlMinDate()) sb.AppendLine("- Expire Date Incorrect. [" + NullUtils.cvString(dr[10]) + "]<br/>");
                        if (item.ExpireDate <= item.ActiveDate) sb.AppendLine("- Expire Date Incorrect. [" + NullUtils.cvString(dr[10]) + " >= " + NullUtils.cvString(dr[9]) + "]<br/>");
                        switch (NullUtils.cvString(dr[11]).ToUpper()) {
                            case "A": item.SaleChannel = ProductSaleChannel.All; break;
                            case "Y": item.SaleChannel = ProductSaleChannel.Online; break;
                            case "N": item.SaleChannel = ProductSaleChannel.Offline; break;
                            default: sb.AppendLine("- Sale Channel Incorrect. [" + NullUtils.cvString(dr[11]) + "]<br/>"); break;
                        }
                        item.CreateDate = item.UpdateDate = DateTime.Now;
                        item.CreateAdminId = item.UpdateAdminId = Admin.Id;
                        item.Status = Status.Active;
                        
                        premium = new ProductMotorPremium();
                        premium.PlanCode = GetInt(dr[8], "Plan Code", ref sb);
                        if (premium.PlanCode > 999999) {
                            sb.AppendLine("- Plan Code Incorrect. [" + NullUtils.cvString(dr[8]) + "]<br/>");
                        }
                        premium.EPolicy = GetYesNo(NullUtils.cvString(dr[12]), "E-Policy", ref sb);
                        premium.SumInsured = GetDecimal(dr[13], premium.GetDisplayName(x => x.SumInsured), ref sb);
                        premium.NetPremium = GetDecimal(dr[14], premium.GetDisplayName(x => x.NetPremium), ref sb);
                        premium.Duty = GetDecimal(dr[15], premium.GetDisplayName(x => x.Duty), ref sb);
                        premium.Vat = GetDecimal(dr[16], premium.GetDisplayName(x => x.Vat), ref sb);
                        premium.Premium = GetDecimal(dr[17], premium.GetDisplayName(x => x.Premium), ref sb);
                        premium.DisplayPremium = GetDecimal(dr[18], premium.GetDisplayName(x => x.DisplayPremium), ref sb);
                        premium.CompulsoryPremium = GetDecimal(dr[19], premium.GetDisplayName(x => x.CompulsoryPremium), ref sb);
                        premium.CombinePremium = GetDecimal(dr[20], premium.GetDisplayName(x => x.CombinePremium), ref sb);
                        premium.DisplayCombinePremium = GetDecimal(dr[21], premium.GetDisplayName(x => x.DisplayCombinePremium), ref sb);
                        CheckPremium(premium.NetPremium, premium.Duty, premium.Vat, premium.Premium, ref sb);
                        
                        var registerYear = NullUtils.cvString(dr[23]);
                        if (registerYear.Length == 0 || (registerYear.Length == 4 && string.Equals(registerYear, NullUtils.cvInt(dr[23]).ToString()))) {
                            premium.RegisterYear = NullUtils.cvInt(dr[23]);
                        } else if (registerYear.Contains("-")) {
                            var years = GetYearMinMax(NullUtils.cvString(dr[23]), "Register Year", ref sb);
                            premium.YearMin = years.Item1;
                            premium.YearMax = years.Item2;
                        }

                        if (premium.RegisterYear == 0 && (premium.YearMin == 0 && premium.YearMax == 0)) {
                            sb.AppendLine("- Register Year Incorrect. [" + NullUtils.cvString(dr[23]) + "]<br/>");
                        }

                        switch (NullUtils.cvString(dr[24]).ToUpper()) {
                            case "ALL": premium.CCTV = ProductMotorCCTV.All; break;
                            case "Y": premium.CCTV = ProductMotorCCTV.Yes; break;
                            case "N": premium.CCTV = ProductMotorCCTV.No; break;
                            default: sb.AppendLine("- CCTV Incorrect. [" + NullUtils.cvString(dr[24]) + "]<br/>"); break;
                        }

                        if (string.Equals("ALL", NullUtils.cvString(dr[25]).ToUpper())) {
                            premium.VehicleUsage110 = premium.VehicleUsage120 = premium.VehicleUsage210 = premium.VehicleUsage220 = premium.VehicleUsage230 = premium.VehicleUsage320 = true;
                        } else {
                            var vehicleUsages = GetDataSplit(dr[25]);
                            var err = vehicleUsages.Where(x => vehicleUsage.Contains(x) == false);
                            if (err.Count() > 0) sb.AppendLine("- Vehicle Usage Incorrect. [" + NullUtils.cvString(dr[25]) + "]<br/>");
                            premium.VehicleUsage110 = vehicleUsages.Contains("110");
                            premium.VehicleUsage120 = vehicleUsages.Contains("120");
                            premium.VehicleUsage210 = vehicleUsages.Contains("210");
                            premium.VehicleUsage220 = vehicleUsages.Contains("220");
                            premium.VehicleUsage230 = vehicleUsages.Contains("230");
                            premium.VehicleUsage320 = vehicleUsages.Contains("320");
                        }

                        premium.RegisterProvince = new List<ProductMotorPremiumRegisterProvince>();
                        if (string.Equals("ALL", NullUtils.cvString(dr[26]).ToUpper())) {
                            premium.RegisterProvinceAll = true;
                        } else {
                            premium.RegisterProvinceAll = false;
                            var provinces = GetDataSplit(dr[26]);
                            foreach (var p in provinces) {
                                premium.RegisterProvince.Add(new ProductMotorPremiumRegisterProvince() { RegisterProvinceCode = p, IsUsed = true });
                            }
                        }

                        switch (NullUtils.cvString(dr[27]).ToUpper()) {
                            case "D": premium.GarageType = ProductMotorGarageType.Dealer; break;
                            case "G": premium.GarageType = ProductMotorGarageType.Garage; break;
                            default: sb.AppendLine("- Garage Type Incorrect. [" + NullUtils.cvString(dr[27]) + "]<br/>"); break;
                        }

                        if (String.IsNullOrEmpty(NullUtils.cvString(dr[28]))) {
                            premium.DriverAgeMin = 0;
                            premium.DriverAgeMax = 0;
                        } else {
                            var driverAge = GetYearMinMax(NullUtils.cvString(dr[28]), "อายุผู้ขับขี่", ref sb);
                            premium.DriverAgeMin = driverAge.Item1;
                            premium.DriverAgeMax = driverAge.Item2;
                        }

                        premium.CarInspector = GetYesNo(NullUtils.cvString(dr[29]), premium.GetDisplayName(x => x.CarInspector), ref sb);

                        switch (NullUtils.cvString(dr[30]).ToUpper()) {
                            case "": premium.CarInspectorMethod = ProductMotorCarInspectorMethod.None; break;
                            case "1": premium.CarInspectorMethod = ProductMotorCarInspectorMethod.Person; break;
                            case "2": premium.CarInspectorMethod = ProductMotorCarInspectorMethod.Attachment; break;
                            default: sb.AppendLine("- Car Inspector Method Incorrect. [" + NullUtils.cvString(dr[30]) + "]<br/>"); break;
                        }


                        premium.OwnDamage = GetDecimal(dr[31], premium.GetDisplayName(x => x.OwnDamage), ref sb);
                        premium.OwnExcess = GetDecimal(dr[32], premium.GetDisplayName(x => x.OwnExcess), ref sb);
                        premium.Flood = GetDecimal(dr[33], premium.GetDisplayName(x => x.Flood), ref sb);

                        //RegisterProvince
                        if (productProvinces.ContainsKey(code)) {
                            item.ProductRegisterProvince = productProvinces[code];
                        }

                        if (productCars.ContainsKey(code)) {
                            item.ProductCarModel = productCars[code];
                        } else {
                            sb.AppendLine("- Car_Model ไม่พบ Code Product [" + item.InsuranceProductCode + "]<br/>");
                        }

                        item.ProductMotorPremium = new List<ProductMotorPremium> {
                            premium
                        };
                    } catch (Exception ex) {
                        sb.AppendLine("- Invalid data " + ex.ToString());
                    }
                    return Tuple.Create(item, sb.ToString());
                };

                var i = 0;
                foreach (DataRow dr in dtProductTH.Rows) {
                    i++;
                    if (i < 6) continue;
                    sheetLanguage = Language.TH;
                    var row = importRow(dr);
                    if (row.Item1.InsuranceProductCode.Length == 0) continue;
                    Product product = row.Item1;
                    string error = row.Item2;
                    sheetLanguage = Language.EN;
                    var rowEN = importRow(dtProductEN.Rows[i - 1]);
                    Product productEN = rowEN.Item1;
                    int productIndex = product.ProductMotorPremium.Count - 1;
                    if (product.InsuranceProductCode != productEN.InsuranceProductCode ||
                        product.ProductMotorPremium[productIndex].RegisterYear != productEN.ProductMotorPremium[productIndex].RegisterYear ||
                        product.ProductMotorPremium[productIndex].YearMin != productEN.ProductMotorPremium[productIndex].YearMin ||
                        product.ProductMotorPremium[productIndex].YearMax != productEN.ProductMotorPremium[productIndex].YearMax ||
                        product.ProductMotorPremium[productIndex].CarInspector != productEN.ProductMotorPremium[productIndex].CarInspector ||
                        product.ProductMotorPremium[productIndex].CarInspectorMethod != productEN.ProductMotorPremium[productIndex].CarInspectorMethod ||
                        product.ProductMotorPremium[productIndex].SumInsured != productEN.ProductMotorPremium[productIndex].SumInsured ||
                        product.ProductMotorPremium[productIndex].Premium != productEN.ProductMotorPremium[productIndex].Premium ||
                        product.ProductMotorPremium[productIndex].DisplayPremium != productEN.ProductMotorPremium[productIndex].DisplayPremium ||
                        product.ProductMotorPremium[productIndex].DisplayCombinePremium != productEN.ProductMotorPremium[productIndex].DisplayCombinePremium ||
                        product.ProductMotorPremium[productIndex].CompulsoryPremium != productEN.ProductMotorPremium[productIndex].CompulsoryPremium ||
                        product.ProductMotorPremium[productIndex].CombinePremium != productEN.ProductMotorPremium[productIndex].CombinePremium ||
                        product.ProductMotorPremium[productIndex].DriverAgeMin != productEN.ProductMotorPremium[productIndex].DriverAgeMin ||
                        product.ProductMotorPremium[productIndex].DriverAgeMax != productEN.ProductMotorPremium[productIndex].DriverAgeMax ||
                        product.ProductMotorPremium[productIndex].CCTV != productEN.ProductMotorPremium[productIndex].CCTV ||
                        product.ProductMotorPremium[productIndex].OwnExcess != productEN.ProductMotorPremium[productIndex].OwnExcess ||
                        product.ProductMotorPremium[productIndex].OwnDamage != productEN.ProductMotorPremium[productIndex].OwnDamage ||
                        product.ProductMotorPremium[productIndex].Flood != productEN.ProductMotorPremium[productIndex].Flood ||
                        product.ProductMotorPremium[productIndex].PlanCode != productEN.ProductMotorPremium[productIndex].PlanCode) {
                        result.Success = false;
                        sbError.AppendLine("<tr><th>Row " + i.ToString() + "</th><td>ข้อมูล Product (TH) และ Product (EN) ไม่ตรงกัน</td>");
                    } else {
                        product.TitleEN = productEN.TitleEN;
                        product.ApproveAdminId = product.OwnerAdminId = "";
                        product.ApproveStatus = ApproveStatus.Pending;

                        if (!items.ContainsKey(product.InsuranceProductCode)) {
                            items.Add(product.InsuranceProductCode, product);
                        } else {
                            items[product.InsuranceProductCode].ProductMotorPremium.Add(product.ProductMotorPremium[0]);
                        }
                    }
                    if (error.Length > 0) {
                        result.Success = false;
                        sbError.AppendLine("<tr><th>Row " + i.ToString() + "</th><td>" + error.ToString() + "</td>");
                    }
                }
                
            } catch (Exception ex) {
                result.Success = false;
                sbError.AppendLine("<tr><th>Summary</th><td>"+ ex.ToString() + "</td>");
            }
            if (result.Success) {
                result.Success = repo.AddProducts(items.Values.ToList()) > 0;
            } else {
                ViewBag.ErrorMessage = sbError.ToString();
            }

            return Tuple.Create(result, items.Values.ToList());
        }

        private Tuple<ActionResultStatus, List<Product>> ImportHome(DataSet ds) {
            Dictionary<string, Product> items = new Dictionary<string, Product>();
            ActionResultStatus result = new ActionResultStatus() { Success = true, ErrorMessage = string.Empty };
            StringBuilder sbError = new StringBuilder();
            try {
                Product item;
                ProductHomePremium premium;
                Dictionary<string, Product> coveragePrivilege = GetCoveragePrivilege(ds);
                var companies = GetCompanies();
                MasterOptionRepository masterOptionRepo = new MasterOptionRepository();
                var t_propertyType = masterOptionRepo.ListMasterOption_DictionaryTitleTH_Admin(MasterOptionCategory.PropertyType);
                var t_constructionType = masterOptionRepo.ListMasterOption_DictionaryTitleTH_Admin(MasterOptionCategory.ConstructionType);
                var propertyTypes = new Dictionary<string, int>(t_propertyType, StringComparer.OrdinalIgnoreCase);
                var constructionTypes = new Dictionary<string, int>(t_constructionType, StringComparer.OrdinalIgnoreCase);
                var subClass = repo.ListProductSubClass(ProductCategoryKey.Home);

                ConfigRepository configRepo = new ConfigRepository();
                var coverMax = NullUtils.cvInt(configRepo.GetConfig(ConfigName.ProductHomeYearMax).Body);

                Language sheetLanguage = Language.TH;
                DataTable dtProductTH = ds.Tables["Product (TH)"];
                DataTable dtProductEN = ds.Tables["Product (EN)"];
                
                Func<DataRow, Tuple<Product, string>> importRow = dr => {
                    item = new Product();
                    StringBuilder sb = new StringBuilder();
                    try {
                        string code = NullUtils.cvString(dr[6]);
                        if (coveragePrivilege.ContainsKey(code)) {
                            Product model = coveragePrivilege[code];
                            item.CoverageTH = NullUtils.cvString(model.CoverageTH);
                            item.CoverageEN = NullUtils.cvString(model.CoverageEN);
                            item.SpecialCoverageTH = NullUtils.cvString(model.SpecialCoverageTH);
                            item.SpecialCoverageEN = NullUtils.cvString(model.SpecialCoverageEN);
                            item.ConditionTH = NullUtils.cvString(model.ConditionTH);
                            item.ConditionEN = NullUtils.cvString(model.ConditionEN);
                            item.ExceptionTH = NullUtils.cvString(model.ExceptionTH);
                            item.ExceptionEN = NullUtils.cvString(model.ExceptionEN);
                            item.PrivilegeTH = NullUtils.cvString(model.PrivilegeTH);
                            item.PrivilegeEN = NullUtils.cvString(model.PrivilegeEN);
                            item.CheckListTH = NullUtils.cvString(model.CheckListTH);
                            item.CheckListEN = NullUtils.cvString(model.CheckListEN);
                            item.ConsentTH = NullUtils.cvString(model.ConsentTH);
                            item.ConsentEN = NullUtils.cvString(model.ConsentEN);
                        } else {
                            item.CoverageTH = item.CoverageEN = "";
                            item.SpecialCoverageTH = item.SpecialCoverageEN = "";
                            item.ConditionTH = item.ConditionEN = "";
                            item.ExceptionTH = item.ExceptionEN = "";
                            item.PrivilegeTH = item.PrivilegeEN = "";
                            item.CheckListTH = item.CheckListEN = "";
                            item.ConsentTH = item.ConsentEN = "";
                        }
                        item.SaleChannel = ProductSaleChannel.All;
                        item.CategoryId = ProductCategoryKey.Home;
                        item.SubCategoryId = ProductSubCategoryKey.Home;
                        item.ProductCode = NullUtils.cvString(dr[2]);

                        item.CompanyCode = NullUtils.cvString(dr[3]);
                        if (companies.ContainsKey(dr[3].ToString())) { item.CompanyId = companies[dr[3].ToString()]; } else { sb.AppendLine("- Insurance Code Incorrect. [" + dr[3].ToString() + "]<br/>"); }

                        item.Class = NullUtils.cvString(dr[4]);
                        item.SubClass = NullUtils.cvString(dr[5]);

                        if (!subClass.Contains(item.Class + "|" + item.SubClass)) {
                            if (!subClass.Contains(item.Class)) {
                                sb.AppendLine("- Class Incorrect. [" + dr[4].ToString() + "]<br/>");
                            } else {
                                sb.AppendLine("- Sub Class Incorrect. [" + dr[5].ToString() + "]<br/>");
                            }
                        }

                        item.InsuranceProductCode = NullUtils.cvString(dr[6]);
                        if (item.InsuranceProductCode.Length > 20) sb.AppendLine("- Insurance Product Code Incorrect. [" + dr[6].ToString() + "]<br/>");
                        if (sheetLanguage == Language.TH) {
                            item.TitleTH = NullUtils.cvString(dr[7]);
                        } else {
                            item.TitleEN = NullUtils.cvString(dr[7]);
                        }
                       
                        item.ActiveDate = GetDate(dr[9]);
                        if (item.ActiveDate == SqlMinDate()) sb.AppendLine("- Active Date Incorrect. [" + NullUtils.cvString(dr[9]) + "]<br/>");
                        item.ExpireDate = GetDate(dr[10]);
                        if (item.ExpireDate == SqlMinDate()) sb.AppendLine("- Expire Date Incorrect. [" + NullUtils.cvString(dr[10]) + "]<br/>");
                        if (item.ExpireDate <= item.ActiveDate) sb.AppendLine("- Expire Date Incorrect. [" + NullUtils.cvString(dr[10]) + " >= " + NullUtils.cvString(dr[9]) + "]<br/>");
                        switch (NullUtils.cvString(dr[11]).ToUpper()) {
                            case "A": item.SaleChannel = ProductSaleChannel.All; break;
                            case "Y": item.SaleChannel = ProductSaleChannel.Online; break;
                            case "N": item.SaleChannel = ProductSaleChannel.Offline; break;
                            default: sb.AppendLine("- Sale Channel Incorrect. [" + NullUtils.cvString(dr[11]) + "]<br/>"); break;
                        }
                        item.CreateDate = item.UpdateDate = DateTime.Now;
                        item.CreateAdminId = item.UpdateAdminId = Admin.Id;
                        item.Status = Status.Active;

                        premium = new ProductHomePremium();
                        premium.PlanCode = GetInt(NullUtils.cvString(dr[8]), "Plan Code", ref sb);
                        if (premium.PlanCode > 999999) {
                            sb.AppendLine("- Plan Code Incorrect. [" + NullUtils.cvString(dr[8]) + "]<br/>");
                        }
                        premium.EPolicy = GetYesNo(NullUtils.cvString(dr[12]), "E-Policy", ref sb);
                        premium.SumInsured = GetDecimal(dr[13], premium.GetDisplayName(x => x.SumInsured), ref sb);
                        premium.NetPremium = GetDecimal(dr[14], premium.GetDisplayName(x => x.NetPremium), ref sb);
                        premium.Duty = GetDecimal(dr[15], premium.GetDisplayName(x => x.Duty), ref sb);
                        premium.Vat = GetDecimal(dr[16], premium.GetDisplayName(x => x.Vat), ref sb);
                        premium.Premium = GetDecimal(dr[17], premium.GetDisplayName(x => x.Premium), ref sb);
                        premium.DisplayPremium = GetDecimal(dr[18], premium.GetDisplayName(x => x.DisplayPremium), ref sb);
                        CheckPremium(premium.NetPremium, premium.Duty, premium.Vat, premium.Premium, ref sb);

                        if (propertyTypes.ContainsKey(dr[19].ToString())) {
                            premium.PropertyTypeId = (int)propertyTypes[dr[19].ToString()];
                        } else {
                            sb.AppendLine("- Property Type Incorrect. [" + NullUtils.cvString(dr[19]) + "]<br/>");
                        }
                        if (constructionTypes.ContainsKey(dr[20].ToString())) {
                            premium.ConstructionTypeId = (int)constructionTypes[dr[20].ToString()];
                        } else {
                            sb.AppendLine("- Construction Type Incorrect. [" + NullUtils.cvString(dr[20]) + "]<br/>");
                        }
                        premium.CoverPeriod = NullUtils.cvInt(dr[21]); 
                        if (premium.CoverPeriod < 1 || premium.CoverPeriod > coverMax) sb.AppendLine("- Cover Period Incorrect. [" + NullUtils.cvString(dr[21]) + "]<br/>");

                        if (sheetLanguage == Language.TH) {
                            premium.FireTH = GetString(dr[22], premium.GetDisplayName(x => x.FireTH), ref sb);
                            premium.AccidentalDamageTH = GetString(dr[23], premium.GetDisplayName(x => x.AccidentalDamageTH), ref sb);
                            premium.NaturalRiskTH = GetString(dr[24], premium.GetDisplayName(x => x.NaturalRiskTH), ref sb);
                            premium.TempRentalTH = GetString(dr[25], premium.GetDisplayName(x => x.TempRentalTH), ref sb);

                            premium.BurglaryTH = GetString(dr[26], premium.GetDisplayName(x => x.BurglaryTH), ref sb);
                            premium.PublicLiabilityTH = GetString(dr[27], premium.GetDisplayName(x => x.PublicLiabilityTH), ref sb);
                        } else {
                            premium.FireEN = GetString(dr[22], premium.GetDisplayName(x => x.FireEN), ref sb);
                            premium.AccidentalDamageEN = GetString(dr[23], premium.GetDisplayName(x => x.AccidentalDamageEN), ref sb);
                            premium.NaturalRiskEN = GetString(dr[24], premium.GetDisplayName(x => x.NaturalRiskEN), ref sb);
                            premium.TempRentalEN = GetString(dr[25], premium.GetDisplayName(x => x.TempRentalEN), ref sb);
                            premium.BurglaryEN = GetString(dr[26], premium.GetDisplayName(x => x.BurglaryEN), ref sb);
                            premium.PublicLiabilityEN = GetString(dr[27], premium.GetDisplayName(x => x.PublicLiabilityEN), ref sb);
                        }
                        

                        //items[item.InsuranceProductCode].ProductHomePremium.Add(premium);
                        item.ProductHomePremium = new List<ProductHomePremium> {
                            premium
                        };
                    } catch (Exception ex) {
                        sb.AppendLine("- Invalid data " + ex.ToString());
                    }
                    return Tuple.Create(item, sb.ToString());
                };

                var i = 0;
                foreach (DataRow dr in dtProductTH.Rows) {
                    i++;
                    if (i < 6) continue;
                    sheetLanguage = Language.TH;
                    var row = importRow(dr);
                    if (row.Item1.InsuranceProductCode.Length == 0) continue;
                    Product product = row.Item1;
                    string error = row.Item2;
                    sheetLanguage = Language.EN;
                    var rowEN = importRow(dtProductEN.Rows[i - 1]);
                    Product productEN = rowEN.Item1;
                    int productIndex = product.ProductHomePremium.Count - 1;
                    if (product.InsuranceProductCode != productEN.InsuranceProductCode || 
                        product.ProductHomePremium[productIndex].ProductId != productEN.ProductHomePremium[productIndex].ProductId ||
                        product.ProductHomePremium[productIndex].PropertyTypeId != productEN.ProductHomePremium[productIndex].PropertyTypeId ||
                        product.ProductHomePremium[productIndex].ConstructionTypeId != productEN.ProductHomePremium[productIndex].ConstructionTypeId ||
                        product.ProductHomePremium[productIndex].CoverPeriod != productEN.ProductHomePremium[productIndex].CoverPeriod ||
                        product.ProductHomePremium[productIndex].PlanCode != productEN.ProductHomePremium[productIndex].PlanCode) {
                        result.Success = false;
                        sbError.AppendLine("<tr><th>Row " + i.ToString() + "</th><td>ข้อมูล Product (TH) และ Product (EN) ไม่ตรงกัน</td>");
                    } else {
                        product.TitleEN = productEN.TitleEN;
                        product.ProductHomePremium[productIndex].FireEN = productEN.ProductHomePremium[productIndex].FireEN;
                        product.ProductHomePremium[productIndex].AccidentalDamageEN = productEN.ProductHomePremium[productIndex].AccidentalDamageEN;
                        product.ProductHomePremium[productIndex].NaturalRiskEN = productEN.ProductHomePremium[productIndex].NaturalRiskEN;
                        product.ProductHomePremium[productIndex].TempRentalEN = productEN.ProductHomePremium[productIndex].TempRentalEN;
                        product.ProductHomePremium[productIndex].BurglaryEN = productEN.ProductHomePremium[productIndex].BurglaryEN;
                        product.ProductHomePremium[productIndex].PublicLiabilityEN = productEN.ProductHomePremium[productIndex].PublicLiabilityEN;
                        product.ApproveAdminId = product.OwnerAdminId = "";
                        product.ApproveStatus = ApproveStatus.Pending;

                        if (!items.ContainsKey(product.InsuranceProductCode)) {
                            items.Add(product.InsuranceProductCode, product);
                        } else {
                            items[product.InsuranceProductCode].ProductHomePremium.Add(product.ProductHomePremium[0]);
                        }
                    }
                    if (error.Length > 0) {
                        result.Success = false;
                        sbError.AppendLine("<tr><th>Row " + i.ToString() + "</th><td>" + error.ToString() + "</td>");
                    }
                }
            } catch (Exception ex) {
                result.Success = false;
                sbError.AppendLine("<tr><th>Summary</th><td>" + ex.ToString() + "</td>");
            }
            if (result.Success) {
                result.Success = repo.AddProducts(items.Values.ToList()) > 0;
            } else {
                ViewBag.ErrorMessage = sbError.ToString();
            }

            return Tuple.Create(result, items.Values.ToList());
        }

        private Tuple<ActionResultStatus, List<Product>> ImportPA(DataSet ds) {
            Dictionary<string, Product> items = new Dictionary<string, Product>();
            ActionResultStatus result = new ActionResultStatus() { Success = true, ErrorMessage = string.Empty };
            StringBuilder sbError = new StringBuilder();
            try {
                Product item;
                ProductPAPremium premium;
                var companies = GetCompanies();
                //var privileges = GetPrivileges(ProductCategoryKey.PA);
                MasterOptionRepository masterOptionRepo = new MasterOptionRepository();
                var t_career = masterOptionRepo.ListMasterOption_DictionaryTitleTH_Admin(MasterOptionCategory.Career);
                var careers = new Dictionary<string, int>(t_career, StringComparer.OrdinalIgnoreCase);
                Dictionary<string, Product> coveragePrivilege = GetCoveragePrivilege(ds);
                
                Language sheetLanguage = Language.TH;
                DataTable dtProductTH = ds.Tables["Product (TH)"];
                DataTable dtProductEN = ds.Tables["Product (EN)"];

                var ageCalculation = new Dictionary<string, ProductAgeCalculation>();
                ageCalculation.Add("ปัด", ProductAgeCalculation.Ceiling);
                ageCalculation.Add("ครบ", ProductAgeCalculation.Floor);

                var insuranceTypes = new Dictionary<string, ProductPAInsuranceType>();
                insuranceTypes.Add(ProductPAInsuranceType.PA1.GetDisplayName(), ProductPAInsuranceType.PA1);
                insuranceTypes.Add(ProductPAInsuranceType.PA2.GetDisplayName(), ProductPAInsuranceType.PA2);

                var subClass = repo.ListProductSubClass(ProductCategoryKey.PA);

                //Career
                DataTable dtCareer = ds.Tables["Career"];
                Dictionary<int, string> columns = new Dictionary<int, string>();
                Dictionary<string, List<ProductCareerClass>> productCareers = new Dictionary<string, List<ProductCareerClass>>();
                for (int c = 2; c < dtCareer.Rows[1].ItemArray.Length; c++) {
                    string code = NullUtils.cvString(dtCareer.Rows[1].ItemArray[c]);
                    if (!string.IsNullOrEmpty(code.Trim())) {
                        columns.Add(c, code);
                        productCareers.Add(code, new List<ProductCareerClass>());
                    }
                }

                var j = 0;
                foreach (DataRow dr in dtCareer.Rows) {
                    j++;
                    if (j < 5) continue;
                    int careerId = 0;
                    string career = NullUtils.cvString(dr[1]);
                    if (careers.ContainsKey(career)) {
                        careerId = careers[career];
                        foreach (var col in columns.Keys) {
                            int clss = NullUtils.cvInt(dr[col]);
                            if (clss > 0) {
                                productCareers[columns[col]].Add(new ProductCareerClass() { CareerId = careerId, CareerClass = clss });
                            }
                        }
                    }
                }

                Func<DataRow, Tuple<Product, string>> importRow = dr => {
                    item = new Product();
                    StringBuilder sb = new StringBuilder();
                    try {
                        string code = NullUtils.cvString(dr[6]);
                        if (coveragePrivilege.ContainsKey(code)) {
                            Product model = coveragePrivilege[code];
                            item.CoverageTH = NullUtils.cvString(model.CoverageTH);
                            item.CoverageEN = NullUtils.cvString(model.CoverageEN);
                            item.SpecialCoverageTH = NullUtils.cvString(model.SpecialCoverageTH);
                            item.SpecialCoverageEN = NullUtils.cvString(model.SpecialCoverageEN);
                            item.ConditionTH = NullUtils.cvString(model.ConditionTH);
                            item.ConditionEN = NullUtils.cvString(model.ConditionEN);
                            item.ExceptionTH = NullUtils.cvString(model.ExceptionTH);
                            item.ExceptionEN = NullUtils.cvString(model.ExceptionEN);
                            item.PrivilegeTH = NullUtils.cvString(model.PrivilegeTH);
                            item.PrivilegeEN = NullUtils.cvString(model.PrivilegeEN);
                            item.CheckListTH = NullUtils.cvString(model.CheckListTH);
                            item.CheckListEN = NullUtils.cvString(model.CheckListEN);
                            item.ConsentTH = NullUtils.cvString(model.ConsentTH);
                            item.ConsentEN = NullUtils.cvString(model.ConsentEN);
                        } else {
                            item.CoverageTH = item.CoverageEN = "";
                            item.SpecialCoverageTH = item.SpecialCoverageEN = "";
                            item.ConditionTH = item.ConditionEN = "";
                            item.ExceptionTH = item.ExceptionEN = "";
                            item.PrivilegeTH = item.PrivilegeEN = "";
                            item.CheckListTH = item.CheckListEN = "";
                            item.ConsentTH = item.ConsentEN = "";
                        }
                        item.SaleChannel = ProductSaleChannel.All;
                        item.ProductCode = NullUtils.cvString(dr[2]);

                        item.CompanyCode = NullUtils.cvString(dr[3]);
                        if (companies.ContainsKey(dr[3].ToString())) { item.CompanyId = companies[dr[3].ToString()]; } else { sb.AppendLine("- Insurance Code Incorrect. [" + dr[3].ToString() + "]<br/>"); }

                        item.Class = NullUtils.cvString(dr[4]);
                        item.SubClass = NullUtils.cvString(dr[5]);

                        item.CategoryId = ProductCategoryKey.PA;
                        item.SubCategoryId = ProductSubCategoryKey.PA;

                        if (!subClass.Contains(item.Class + "|" + item.SubClass)) {
                            if (!subClass.Contains(item.Class)) {
                                sb.AppendLine("- Class Incorrect. [" + dr[4].ToString() + "]<br/>");
                            } else {
                                sb.AppendLine("- Sub Class Incorrect. [" + dr[5].ToString() + "]<br/>");
                            }
                        }

                        item.InsuranceProductCode = NullUtils.cvString(dr[6]);
                        if (item.InsuranceProductCode.Length > 20) sb.AppendLine("- Insurance Product Code Incorrect. [" + dr[6].ToString() + "]<br/>");
                        if (sheetLanguage == Language.TH) {
                            item.TitleTH = NullUtils.cvString(dr[7]);
                        } else {
                            item.TitleEN = NullUtils.cvString(dr[7]);
                        }
                        item.ActiveDate = GetDate(dr[9]);
                        if (item.ActiveDate == SqlMinDate()) sb.AppendLine("- Active Date Incorrect. [" + NullUtils.cvString(dr[9]) + "]<br/>");
                        item.ExpireDate = GetDate(dr[10]);
                        if (item.ExpireDate == SqlMinDate()) sb.AppendLine("- Expire Date Incorrect. [" + NullUtils.cvString(dr[10]) + "]<br/>");
                        if (item.ExpireDate <= item.ActiveDate) sb.AppendLine("- Expire Date Incorrect. [" + NullUtils.cvString(dr[10]) + " >= " + NullUtils.cvString(dr[9]) + "]<br/>");
                        switch (NullUtils.cvString(dr[11]).ToUpper()) {
                            case "A": item.SaleChannel = ProductSaleChannel.All; break;
                            case "Y": item.SaleChannel = ProductSaleChannel.Online; break;
                            case "N": item.SaleChannel = ProductSaleChannel.Offline; break;
                            default: sb.AppendLine("- Sale Channel Incorrect. [" + NullUtils.cvString(dr[11]) + "]<br/>"); break;
                        }
                        item.CreateDate = item.UpdateDate = DateTime.Now;
                        item.CreateAdminId = item.UpdateAdminId = Admin.Id;
                        item.Status = Status.Active;

                        premium = new ProductPAPremium();
                        premium.PlanCode = GetInt(dr[8], "Plan Code", ref sb);
                        if (premium.PlanCode > 999999) {
                            sb.AppendLine("- Plan Code Incorrect. [" + NullUtils.cvString(dr[8]) + "]<br/>");
                        }
                        premium.EPolicy = GetYesNo(NullUtils.cvString(dr[12]), "E-Policy", ref sb);
                        premium.SumInsured = GetDecimal(dr[13], premium.GetDisplayName(x => x.SumInsured), ref sb);
                        premium.NetPremium = GetDecimal(dr[14], premium.GetDisplayName(x => x.NetPremium), ref sb);
                        premium.Duty = GetDecimal(dr[15], premium.GetDisplayName(x => x.Duty), ref sb);
                        premium.Vat = GetDecimal(dr[16], premium.GetDisplayName(x => x.Vat), ref sb);
                        premium.Premium = GetDecimal(dr[17], premium.GetDisplayName(x => x.Premium), ref sb);
                        premium.DisplayPremium = GetDecimal(dr[18], premium.GetDisplayName(x => x.DisplayPremium), ref sb);
                        CheckPremium(premium.NetPremium, premium.Duty, premium.Vat, premium.Premium, ref sb);

                        var careerClass = GetDataSplit(dr[19]);
                        premium.CareerClass1 = careerClass.Contains("1");
                        premium.CareerClass2 = careerClass.Contains("2");
                        premium.CareerClass3 = careerClass.Contains("3");
                        premium.CareerClass4 = careerClass.Contains("4");
                        if (premium.CareerClass1 == false && premium.CareerClass2 == false && premium.CareerClass3 == false && premium.CareerClass4 == false) sb.AppendLine("- ชั้นอาชีพ Incorrect. [" + NullUtils.cvString(dr[19]) + "]<br/>");
                        var ages = GetDataRange(dr[20], "Age", ref sb);
                        premium.MinAge = ages.Item1;
                        premium.MaxAge = ages.Item2;
                        if (premium.MinAge > 99) sb.AppendLine("- Min Age > 99. [" + NullUtils.cvString(dr[20]) + "]<br/>");
                        if (premium.MaxAge > 99) sb.AppendLine("- Max Age > 99. [" + NullUtils.cvString(dr[20]) + "]<br/>");
                        if (ageCalculation.ContainsKey(NullUtils.cvString(dr[21]))) {
                            premium.AgeCalculation = ageCalculation[NullUtils.cvString(dr[21])];
                        } else { sb.AppendLine("- " + premium.AgeCalculation.GetDisplayName() + " Incorrect. [" + NullUtils.cvString(dr[21]) + "]<br/>"); }
                        if (insuranceTypes.ContainsKey(NullUtils.cvString(dr[22]))) {
                            premium.InsuranceType = insuranceTypes[NullUtils.cvString(dr[22])];
                        } else { sb.AppendLine("- " + premium.InsuranceType.GetDisplayName() + " Incorrect. [" + NullUtils.cvString(dr[22]) + "]<br/>"); }


                        if (sheetLanguage == Language.TH) {
                            premium.LossLifeTH = GetString(dr[23], premium.GetDisplayName(x => x.LossLifeTH), ref sb);
                            premium.MedicalExpenseTH = GetString(dr[24], premium.GetDisplayName(x => x.MedicalExpenseTH), ref sb);
                            premium.IncomeCompensationTH = GetString(dr[25], premium.GetDisplayName(x => x.IncomeCompensationTH), ref sb);
                            premium.MurderAssaultTH = GetString(dr[26], premium.GetDisplayName(x => x.MurderAssaultTH), ref sb);
                            premium.RidingPassengerMotorcycleTH = GetString(dr[27], premium.GetDisplayName(x => x.RidingPassengerMotorcycleTH), ref sb);
                            premium.FuneralExpenseAccidentIllnessTH = GetString(dr[28], premium.GetDisplayName(x => x.FuneralExpenseAccidentIllnessTH), ref sb);
                        } else {
                            premium.LossLifeEN = GetString(dr[23], premium.GetDisplayName(x => x.LossLifeEN), ref sb);
                            premium.MedicalExpenseEN = GetString(dr[24], premium.GetDisplayName(x => x.MedicalExpenseEN), ref sb);
                            premium.IncomeCompensationEN = GetString(dr[25], premium.GetDisplayName(x => x.IncomeCompensationEN), ref sb);
                            premium.MurderAssaultEN = GetString(dr[26], premium.GetDisplayName(x => x.MurderAssaultEN), ref sb);
                            premium.RidingPassengerMotorcycleEN = GetString(dr[27], premium.GetDisplayName(x => x.RidingPassengerMotorcycleEN), ref sb);
                            premium.FuneralExpenseAccidentIllnessEN = GetString(dr[28], premium.GetDisplayName(x => x.FuneralExpenseAccidentIllnessEN), ref sb);
                        }

                        item.ProductPAPremium = new List<ProductPAPremium> {
                            premium
                        };
                    } catch (Exception ex) {
                        sb.AppendLine("- Invalid data " + ex.ToString());
                    }
                    return Tuple.Create(item, sb.ToString());
                };

                var i = 0;
                foreach (DataRow dr in dtProductTH.Rows) {
                    i++;
                    if (i < 6) continue;
                    sheetLanguage = Language.TH;
                    var row = importRow(dr);
                    if (row.Item1.InsuranceProductCode.Length == 0) continue;
                    Product product = row.Item1;
                    string error = row.Item2;
                    sheetLanguage = Language.EN;
                    var rowEN = importRow(dtProductEN.Rows[i - 1]);
                    Product productEN = rowEN.Item1;
                    int productIndex = product.ProductPAPremium.Count - 1;
                    if (product.InsuranceProductCode != productEN.InsuranceProductCode ||
                        product.ProductPAPremium[productIndex].InsuranceType != productEN.ProductPAPremium[productIndex].InsuranceType ||
                        product.ProductPAPremium[productIndex].AgeCalculation != productEN.ProductPAPremium[productIndex].AgeCalculation ||
                        product.ProductPAPremium[productIndex].MinAge != productEN.ProductPAPremium[productIndex].MinAge ||
                        product.ProductPAPremium[productIndex].MaxAge != productEN.ProductPAPremium[productIndex].MaxAge ||
                        product.ProductPAPremium[productIndex].CareerClass1 != productEN.ProductPAPremium[productIndex].CareerClass1 ||
                        product.ProductPAPremium[productIndex].CareerClass2 != productEN.ProductPAPremium[productIndex].CareerClass2 ||
                        product.ProductPAPremium[productIndex].CareerClass3 != productEN.ProductPAPremium[productIndex].CareerClass3 ||
                        product.ProductPAPremium[productIndex].CareerClass4 != productEN.ProductPAPremium[productIndex].CareerClass4 ||
                        product.ProductPAPremium[productIndex].PlanCode != productEN.ProductPAPremium[productIndex].PlanCode) {
                        result.Success = false;
                        sbError.AppendLine("<tr><th>Row " + i.ToString() + "</th><td>ข้อมูล Product (TH) และ Product (EN) ไม่ตรงกัน</td>");
                    } else {
                        product.TitleEN = productEN.TitleEN;

                        product.ProductPAPremium[productIndex].LossLifeEN = productEN.ProductPAPremium[productIndex].LossLifeEN;
                        product.ProductPAPremium[productIndex].MedicalExpenseEN = productEN.ProductPAPremium[productIndex].MedicalExpenseEN;
                        product.ProductPAPremium[productIndex].IncomeCompensationEN = productEN.ProductPAPremium[productIndex].IncomeCompensationEN;
                        product.ProductPAPremium[productIndex].MurderAssaultEN = productEN.ProductPAPremium[productIndex].MurderAssaultEN;
                        product.ProductPAPremium[productIndex].RidingPassengerMotorcycleEN = productEN.ProductPAPremium[productIndex].RidingPassengerMotorcycleEN;
                        product.ProductPAPremium[productIndex].FuneralExpenseAccidentIllnessEN = productEN.ProductPAPremium[productIndex].RidingPassengerMotorcycleEN;

                        product.ApproveAdminId = product.OwnerAdminId = "";
                        product.ApproveStatus = ApproveStatus.Pending;

                        if (!items.ContainsKey(product.InsuranceProductCode)) {
                            if (productCareers.ContainsKey(product.InsuranceProductCode)) {
                                product.ProductCareer = productCareers[product.InsuranceProductCode];
                            } else {
                                error += ("- Career ไม่พบ Code Product [" + product.InsuranceProductCode + "]<br/>");
                            }
                            items.Add(product.InsuranceProductCode, product);
                        } else {
                            items[product.InsuranceProductCode].ProductPAPremium.Add(product.ProductPAPremium[0]);
                        }
                    }
                    if (error.Length > 0) {
                        result.Success = false;
                        sbError.AppendLine("<tr><th>Row " + i.ToString() + "</th><td>" + error.ToString() + "</td>");
                    }
                }
            } catch (Exception ex) {
                result.Success = false;
                sbError.AppendLine("<tr><th>Summary</th><td>" + ex.ToString() + "</td>");
            }
            if (result.Success) {
                if (repo.AddProducts(items.Values.ToList()) == 0) {
                    result.Success = false;
                }
            } else {
                ViewBag.ErrorMessage = sbError.ToString();
            }

            return Tuple.Create(result, items.Values.ToList());
        }

        private Tuple<ActionResultStatus, List<Product>> ImportPH(DataSet ds) {
            Dictionary<string, Product> items = new Dictionary<string, Product>();
            ActionResultStatus result = new ActionResultStatus() { Success = true, ErrorMessage = string.Empty };
            StringBuilder sbError = new StringBuilder();
            try {
                Product item;
                ProductPHPremium premium;
                var companies = GetCompanies();
                //var privileges = GetPrivileges(ProductCategoryKey.PH);
                MasterOptionRepository masterOptionRepo = new MasterOptionRepository();
                var t_career = masterOptionRepo.ListMasterOption_DictionaryTitleTH_Admin(MasterOptionCategory.Career);
                var careers = new Dictionary<string, int>(t_career, StringComparer.OrdinalIgnoreCase);
                Dictionary<string, Product> coveragePrivilege = GetCoveragePrivilege(ds);

                Language sheetLanguage = Language.TH;
                DataTable dtProductTH = ds.Tables["Product (TH)"];
                DataTable dtProductEN = ds.Tables["Product (EN)"];

                var ageCalculation = new Dictionary<string, ProductAgeCalculation>();
                ageCalculation.Add("ปัด", ProductAgeCalculation.Ceiling);
                ageCalculation.Add("ครบ", ProductAgeCalculation.Floor);

                var subClass = repo.ListProductSubClass(ProductCategoryKey.PH);

                //Career
                DataTable dtCareer = ds.Tables["Career"];
                Dictionary<int, string> columns = new Dictionary<int, string>();
                Dictionary<string, List<ProductCareerClass>> productCareers = new Dictionary<string, List<ProductCareerClass>>();
                for (int c = 2; c < dtCareer.Rows[1].ItemArray.Length; c++) {
                    string code = NullUtils.cvString(dtCareer.Rows[1].ItemArray[c]);
                    if (!string.IsNullOrEmpty(code.Trim())) {
                        columns.Add(c, code);
                        productCareers.Add(code, new List<ProductCareerClass>());
                    }
                }

                var j = 0;
                foreach (DataRow dr in dtCareer.Rows) {
                    j++;
                    if (j < 5) continue;
                    int careerId = 0;
                    string career = NullUtils.cvString(dr[1]);
                    if (careers.ContainsKey(career)) {
                        careerId = careers[career];
                        foreach (var col in columns.Keys) {
                            int clss = NullUtils.cvInt(dr[col]);
                            if (clss > 0) {
                                productCareers[columns[col]].Add(new ProductCareerClass() { CareerId = careerId, CareerClass = clss });
                            }
                        }
                    }
                }

                Func<DataRow, Tuple<Product, string>> importRow = dr => {
                    item = new Product();
                    StringBuilder sb = new StringBuilder();
                    try {
                        string code = NullUtils.cvString(dr[6]);
                        if (coveragePrivilege.ContainsKey(code)) {
                            Product model = coveragePrivilege[code];
                            item.CoverageTH = NullUtils.cvString(model.CoverageTH);
                            item.CoverageEN = NullUtils.cvString(model.CoverageEN);
                            item.SpecialCoverageTH = NullUtils.cvString(model.SpecialCoverageTH);
                            item.SpecialCoverageEN = NullUtils.cvString(model.SpecialCoverageEN);
                            item.ConditionTH = NullUtils.cvString(model.ConditionTH);
                            item.ConditionEN = NullUtils.cvString(model.ConditionEN);
                            item.ExceptionTH = NullUtils.cvString(model.ExceptionTH);
                            item.ExceptionEN = NullUtils.cvString(model.ExceptionEN);
                            item.PrivilegeTH = NullUtils.cvString(model.PrivilegeTH);
                            item.PrivilegeEN = NullUtils.cvString(model.PrivilegeEN);
                            item.CheckListTH = NullUtils.cvString(model.CheckListTH);
                            item.CheckListEN = NullUtils.cvString(model.CheckListEN);
                            item.ConsentTH = NullUtils.cvString(model.ConsentTH);
                            item.ConsentEN = NullUtils.cvString(model.ConsentEN);
                        } else {
                            item.CoverageTH = item.CoverageEN = "";
                            item.SpecialCoverageTH = item.SpecialCoverageEN = "";
                            item.ConditionTH = item.ConditionEN = "";
                            item.ExceptionTH = item.ExceptionEN = "";
                            item.PrivilegeTH = item.PrivilegeEN = "";
                            item.CheckListTH = item.CheckListEN = "";
                            item.ConsentTH = item.ConsentEN = "";
                        }
                        item.SaleChannel = ProductSaleChannel.All;
                        item.ProductCode = NullUtils.cvString(dr[2]);

                        item.CompanyCode = NullUtils.cvString(dr[3]);
                        if (companies.ContainsKey(dr[3].ToString())) { item.CompanyId = companies[dr[3].ToString()]; } else { sb.AppendLine("- Insurance Code Incorrect. [" + dr[3].ToString() + "]<br/>"); }

                        item.Class = NullUtils.cvString(dr[4]);
                        item.SubClass = NullUtils.cvString(dr[5]);

                        item.CategoryId = ProductCategoryKey.PH;
                        item.SubCategoryId = ProductSubCategoryKey.PH;

                        if (!subClass.Contains(item.Class + "|" + item.SubClass)) {
                            if (!subClass.Contains(item.Class)) {
                                sb.AppendLine("- Class Incorrect. [" + dr[4].ToString() + "]<br/>");
                            } else {
                                sb.AppendLine("- Sub Class Incorrect. [" + dr[5].ToString() + "]<br/>");
                            }
                        }

                        item.InsuranceProductCode = NullUtils.cvString(dr[6]);
                        if (item.InsuranceProductCode.Length > 20) sb.AppendLine("- Insurance Product Code Incorrect. [" + dr[6].ToString() + "]<br/>");
                        if (sheetLanguage == Language.TH) {
                            item.TitleTH = NullUtils.cvString(dr[7]);
                        } else {
                            item.TitleEN = NullUtils.cvString(dr[7]);
                        }
                        item.ActiveDate = GetDate(dr[9]);
                        if (item.ActiveDate == SqlMinDate()) sb.AppendLine("- Active Date Incorrect. [" + NullUtils.cvString(dr[9]) + "]<br/>");
                        item.ExpireDate = GetDate(dr[10]);
                        if (item.ExpireDate == SqlMinDate()) sb.AppendLine("- Expire Date Incorrect. [" + NullUtils.cvString(dr[10]) + "]<br/>");
                        if (item.ExpireDate <= item.ActiveDate) sb.AppendLine("- Expire Date Incorrect. [" + NullUtils.cvString(dr[10]) + " >= " + NullUtils.cvString(dr[9]) + "]<br/>");
                        switch (NullUtils.cvString(dr[11]).ToUpper()) {
                            case "A": item.SaleChannel = ProductSaleChannel.All; break;
                            case "Y": item.SaleChannel = ProductSaleChannel.Online; break;
                            case "N": item.SaleChannel = ProductSaleChannel.Offline; break;
                            default: sb.AppendLine("- Sale Channel Incorrect. [" + NullUtils.cvString(dr[11]) + "]<br/>"); break;
                        }
                        item.CreateDate = item.UpdateDate = DateTime.Now;
                        item.CreateAdminId = item.UpdateAdminId = Admin.Id;
                        item.Status = Status.Active;

                        premium = new ProductPHPremium();
                        premium.PlanCode = GetInt(dr[8], "Plan Code", ref sb);
                        if (premium.PlanCode > 999999) {
                            sb.AppendLine("- Plan Code Incorrect. [" + NullUtils.cvString(dr[8]) + "]<br/>");
                        }
                        premium.EPolicy = GetYesNo(NullUtils.cvString(dr[12]), "E-Policy", ref sb);
                        premium.SumInsured = GetDecimal(dr[13], premium.GetDisplayName(x => x.SumInsured), ref sb);
                        premium.NetPremium = GetDecimal(dr[14], premium.GetDisplayName(x => x.NetPremium), ref sb);
                        premium.Duty = GetDecimal(dr[15], premium.GetDisplayName(x => x.Duty), ref sb);
                        premium.Vat = GetDecimal(dr[16], premium.GetDisplayName(x => x.Vat), ref sb);
                        premium.Premium = GetDecimal(dr[17], premium.GetDisplayName(x => x.Premium), ref sb);
                        premium.DisplayPremium = GetDecimal(dr[18], premium.GetDisplayName(x => x.DisplayPremium), ref sb);
                        CheckPremium(premium.NetPremium, premium.Duty, premium.Vat, premium.Premium, ref sb);

                        var careerClass = GetDataSplit(dr[19]);
                        premium.CareerClass1 = careerClass.Contains("1");
                        premium.CareerClass2 = careerClass.Contains("2");
                        premium.CareerClass3 = careerClass.Contains("3");
                        premium.CareerClass4 = careerClass.Contains("4");
                        if (premium.CareerClass1 == false && premium.CareerClass2 == false && premium.CareerClass3 == false && premium.CareerClass4 == false) sb.AppendLine("- ชั้นอาชีพ Incorrect. [" + NullUtils.cvString(dr[19]) + "]<br/>");
                        var ages = GetDataRangeDecimal(dr[20], "Age", ref sb);
                        premium.MinAge = (int)Math.Floor(ages.Item1);
                        premium.MaxAge = (int)Math.Floor(ages.Item2);
                        if (premium.MinAge == 0 && premium.MaxAge > 0) {
                            var agesMonth = ValueUtils.GetStringSplit(dr[19], new char[] { '-' });
                            if (agesMonth.Length > 0) {
                                var m = NullUtils.cvDec(agesMonth[0].Trim());
                                if (m > 0) premium.MinAgeMonth = NullUtils.cvInt(agesMonth[0].Trim().Replace("0.", ""));
                            }
                        }
                        if (premium.MinAge > 99) sb.AppendLine("- Min Age > 99. [" + NullUtils.cvString(dr[20]) + "]<br/>");
                        if (premium.MaxAge > 99) sb.AppendLine("- Max Age > 99. [" + NullUtils.cvString(dr[20]) + "]<br/>");
                        if (ageCalculation.ContainsKey(NullUtils.cvString(dr[21]))) {
                            premium.AgeCalculation = ageCalculation[NullUtils.cvString(dr[21])];
                        } else { sb.AppendLine("- " + premium.AgeCalculation.GetDisplayName() + " Incorrect. [" + NullUtils.cvString(dr[21]) + "]<br/>"); }
                        
                        if (string.Equals(dr[22], "ชาย")) { premium.Sex = Sex.Male;
                        } else if (string.Equals(dr[22], "หญิง")) { premium.Sex = Sex.Female; 
                        } else { sb.AppendLine("- Sex Incorrect. [" + NullUtils.cvString(dr[22]) + "]<br/>"); }
                        var weight = GetDataRange(dr[23], "Weight", ref sb, true);
                        premium.MinWeight = weight.Item1;
                        premium.MaxWeight = weight.Item2;
                        if (premium.MinWeight > 100 || premium.MaxWeight > 100) sb.AppendLine("- Weight Must be <=100. [" + NullUtils.cvString(dr[23]) + "]<br/>");
                        var height = GetDataRange(dr[24], "Height", ref sb, true);
                        premium.MinHeight = height.Item1;
                        premium.MaxHeight = height.Item2;
                        if (premium.MinHeight > 200 || premium.MaxHeight > 200) sb.AppendLine("- Height Must be <=200. [" + NullUtils.cvString(dr[24]) + "]<br/>");

                        if (sheetLanguage == Language.TH) {
                            premium.IPDTH = GetString(dr[24], premium.GetDisplayName(x => x.IPDTH), ref sb);
                            premium.OutpatientBenefitsTH = GetString(dr[25], premium.GetDisplayName(x => x.OutpatientBenefitsTH), ref sb);
                            premium.IPDRoomBoardTH = GetString(dr[26], premium.GetDisplayName(x => x.IPDRoomBoardTH), ref sb);
                            premium.SurgicalExpenseTH = GetString(dr[27], premium.GetDisplayName(x => x.SurgicalExpenseTH), ref sb);
                            premium.CancerTreatmentTH = GetString(dr[28], premium.GetDisplayName(x => x.CancerTreatmentTH), ref sb);
                            premium.OrganTransplantTH = GetString(dr[29], premium.GetDisplayName(x => x.OrganTransplantTH), ref sb);
                        } else {
                            premium.IPDEN = GetString(dr[24], premium.GetDisplayName(x => x.IPDEN), ref sb);
                            premium.OutpatientBenefitsEN = GetString(dr[25], premium.GetDisplayName(x => x.OutpatientBenefitsEN), ref sb);
                            premium.IPDRoomBoardEN = GetString(dr[26], premium.GetDisplayName(x => x.IPDRoomBoardEN), ref sb);
                            premium.SurgicalExpenseEN = GetString(dr[27], premium.GetDisplayName(x => x.SurgicalExpenseEN), ref sb);
                            premium.CancerTreatmentEN = GetString(dr[28], premium.GetDisplayName(x => x.CancerTreatmentEN), ref sb);
                            premium.OrganTransplantEN = GetString(dr[29], premium.GetDisplayName(x => x.OrganTransplantEN), ref sb);
                        }

                        item.ProductPHPremium = new List<ProductPHPremium> {
                            premium
                        };
                    } catch (Exception ex) {
                        sb.AppendLine("- Invalid data " + ex.ToString());
                    }
                    return Tuple.Create(item, sb.ToString());
                };

                var i = 0;
                foreach (DataRow dr in dtProductTH.Rows) {
                    i++;
                    if (i < 6) continue;
                    sheetLanguage = Language.TH;
                    var row = importRow(dr);
                    if (row.Item1.InsuranceProductCode.Length == 0) continue;
                    Product product = row.Item1;
                    string error = row.Item2;
                    sheetLanguage = Language.EN;
                    var rowEN = importRow(dtProductEN.Rows[i - 1]);
                    Product productEN = rowEN.Item1;
                    int productIndex = product.ProductPHPremium.Count - 1;
                    if (product.InsuranceProductCode != productEN.InsuranceProductCode ||
                        product.ProductPHPremium[productIndex].Sex != productEN.ProductPHPremium[productIndex].Sex ||
                        product.ProductPHPremium[productIndex].AgeCalculation != productEN.ProductPHPremium[productIndex].AgeCalculation ||
                        product.ProductPHPremium[productIndex].MinAge != productEN.ProductPHPremium[productIndex].MinAge ||
                        product.ProductPHPremium[productIndex].MaxAge != productEN.ProductPHPremium[productIndex].MaxAge ||
                        product.ProductPHPremium[productIndex].MinWeight != productEN.ProductPHPremium[productIndex].MinWeight ||
                        product.ProductPHPremium[productIndex].MaxWeight != productEN.ProductPHPremium[productIndex].MaxWeight ||
                        product.ProductPHPremium[productIndex].MinHeight != productEN.ProductPHPremium[productIndex].MinHeight ||
                        product.ProductPHPremium[productIndex].MaxHeight != productEN.ProductPHPremium[productIndex].MaxHeight ||
                        product.ProductPHPremium[productIndex].CareerClass1 != productEN.ProductPHPremium[productIndex].CareerClass1 ||
                        product.ProductPHPremium[productIndex].CareerClass2 != productEN.ProductPHPremium[productIndex].CareerClass2 ||
                        product.ProductPHPremium[productIndex].CareerClass3 != productEN.ProductPHPremium[productIndex].CareerClass3 ||
                        product.ProductPHPremium[productIndex].CareerClass4 != productEN.ProductPHPremium[productIndex].CareerClass4 ||
                        product.ProductPHPremium[productIndex].PlanCode != productEN.ProductPHPremium[productIndex].PlanCode) {
                        result.Success = false;
                        sbError.AppendLine("<tr><th>Row " + i.ToString() + "</th><td>ข้อมูล Product (TH) และ Product (EN) ไม่ตรงกัน</td>");
                    } else {
                        product.TitleEN = productEN.TitleEN;

                        product.ProductPHPremium[productIndex].IPDEN = productEN.ProductPHPremium[productIndex].IPDEN;
                        product.ProductPHPremium[productIndex].OutpatientBenefitsEN = productEN.ProductPHPremium[productIndex].OutpatientBenefitsEN;
                        product.ProductPHPremium[productIndex].IPDRoomBoardEN = productEN.ProductPHPremium[productIndex].IPDRoomBoardEN;
                        product.ProductPHPremium[productIndex].SurgicalExpenseEN = productEN.ProductPHPremium[productIndex].SurgicalExpenseEN;
                        product.ProductPHPremium[productIndex].CancerTreatmentEN = productEN.ProductPHPremium[productIndex].CancerTreatmentEN;
                        product.ProductPHPremium[productIndex].OrganTransplantEN = productEN.ProductPHPremium[productIndex].OrganTransplantEN;

                        product.ApproveAdminId = product.OwnerAdminId = "";
                        product.ApproveStatus = ApproveStatus.Pending;

                        if (!items.ContainsKey(product.InsuranceProductCode)) {
                            if (productCareers.ContainsKey(product.InsuranceProductCode)) {
                                product.ProductCareer = productCareers[product.InsuranceProductCode];
                            } else {
                                error += ("- Career ไม่พบ Code Product [" + product.InsuranceProductCode + "]<br/>");
                            }
                            items.Add(product.InsuranceProductCode, product);
                        } else {
                            items[product.InsuranceProductCode].ProductPHPremium.Add(product.ProductPHPremium[0]);
                        }
                    }
                    if (error.Length > 0) {
                        result.Success = false;
                        sbError.AppendLine("<tr><th>Row " + i.ToString() + "</th><td>" + error.ToString() + "</td></tr>");
                    }
                }
            } catch (Exception ex) {
                result.Success = false;
                sbError.AppendLine("<tr><th>Summary</th><td>" + ex.ToString() + "</td>");
            }
            if (result.Success) {
                if (repo.AddProducts(items.Values.ToList()) == 0) {
                    result.Success = false;
                }
            } else {
                ViewBag.ErrorMessage = sbError.ToString();
            }

            return Tuple.Create(result, items.Values.ToList());
        }

        private Tuple<ActionResultStatus, List<Product>> ImportTA(DataSet ds) {
            Dictionary<string, Product> items = new Dictionary<string, Product>();
            ActionResultStatus result = new ActionResultStatus() { Success = true, ErrorMessage = string.Empty };
            StringBuilder sbError = new StringBuilder();
            try {
                Product item;
                ProductTAPremium premium;
                Dictionary<string, Product> coveragePrivilege = GetCoveragePrivilege(ds);
                var companies = GetCompanies();
                CountryRepository countryRepo = new CountryRepository();
                var t_countries = countryRepo.ListCountry_Dictionary_Admin();
                var countries = new Dictionary<string, int>(t_countries, StringComparer.OrdinalIgnoreCase);

                Language sheetLanguage = Language.TH;
                DataTable dtProductTH = ds.Tables["Product (TH)"];
                DataTable dtProductEN = ds.Tables["Product (EN)"];

                var tripTypes = new Dictionary<string, ProductTATripType>();
                tripTypes.Add(ProductTATripType.Foreign.GetDisplayName(), ProductTATripType.Foreign);
                tripTypes.Add(ProductTATripType.Domestic.GetDisplayName(), ProductTATripType.Domestic);
                tripTypes.Add(ProductTATripType.Education.GetDisplayName(), ProductTATripType.Education);

                var zones = new Dictionary<string, CountryZone>();
                zones.Add(CountryZone.All.GetDisplayName().ToLower(), CountryZone.All);
                zones.Add(CountryZone.Thailand.GetDisplayName().ToLower(), CountryZone.Thailand);
                zones.Add(CountryZone.Worldwide.GetDisplayName().ToLower(), CountryZone.Worldwide);
                zones.Add(CountryZone.Asia.GetDisplayName().ToLower(), CountryZone.Asia);
                zones.Add(CountryZone.Schengen.GetDisplayName().ToLower(), CountryZone.Schengen);

                var coverageOptions = new Dictionary<string, ProductTACoverageOption>();
                coverageOptions.Add(ProductTACoverageOption.Trip.GetDisplayName(), ProductTACoverageOption.Trip);
                coverageOptions.Add(ProductTACoverageOption.Year.GetDisplayName(), ProductTACoverageOption.Year);

                var coverageTypes = new Dictionary<string, ProductTACoverageType>();
                coverageTypes.Add(ProductTACoverageType.Individual.GetDisplayName(), ProductTACoverageType.Individual);
                coverageTypes.Add(ProductTACoverageType.Family.GetDisplayName(), ProductTACoverageType.Family);

                var subClass = repo.ListProductSubClass(ProductCategoryKey.TA);

                //Country
                DataTable dtCountry = ds.Tables["Country"];
                Dictionary<int, string> columns = new Dictionary<int, string>();
                Dictionary<string, List<int>> productCountries = new Dictionary<string, List<int>>();
                for (int c = 2; c < dtCountry.Rows[1].ItemArray.Length; c++) {
                    string code = NullUtils.cvString(dtCountry.Rows[1].ItemArray[c]);
                    if (!string.IsNullOrEmpty(code.Trim())) {
                        columns.Add(c, code);
                        productCountries.Add(code, new List<int>());
                    }
                }

                var j = 0;
                foreach (DataRow dr in dtCountry.Rows) {
                    j++;
                    if (j < 5) continue;
                    int countryId = 0;
                    string country = NullUtils.cvString(dr[1]);
                    if (countries.ContainsKey(country)) {
                        countryId = countries[country];

                        foreach (var col in columns.Keys) {
                            if (string.Equals(dr[col], "Y")) {
                                productCountries[columns[col]].Add(countryId);
                            }
                        }
                    }
                }
                
                Func<DataRow, Tuple<Product, string>> importRow = dr => {
                    item = new Product();
                    StringBuilder sb = new StringBuilder();
                    try {
                        string code = NullUtils.cvString(dr[6]);
                        if (coveragePrivilege.ContainsKey(code)) {
                            Product model = coveragePrivilege[code];
                            item.CoverageTH = NullUtils.cvString(model.CoverageTH);
                            item.CoverageEN = NullUtils.cvString(model.CoverageEN);
                            item.SpecialCoverageTH = NullUtils.cvString(model.SpecialCoverageTH);
                            item.SpecialCoverageEN = NullUtils.cvString(model.SpecialCoverageEN);
                            item.ConditionTH = NullUtils.cvString(model.ConditionTH);
                            item.ConditionEN = NullUtils.cvString(model.ConditionEN);
                            item.ExceptionTH = NullUtils.cvString(model.ExceptionTH);
                            item.ExceptionEN = NullUtils.cvString(model.ExceptionEN);
                            item.PrivilegeTH = NullUtils.cvString(model.PrivilegeTH);
                            item.PrivilegeEN = NullUtils.cvString(model.PrivilegeEN);
                            item.CheckListTH = NullUtils.cvString(model.CheckListTH);
                            item.CheckListEN = NullUtils.cvString(model.CheckListEN);
                            item.ConsentTH = NullUtils.cvString(model.ConsentTH);
                            item.ConsentEN = NullUtils.cvString(model.ConsentEN);
                        } else {
                            item.CoverageTH = item.CoverageEN = "";
                            item.SpecialCoverageTH = item.SpecialCoverageEN = "";
                            item.ConditionTH = item.ConditionEN = "";
                            item.ExceptionTH = item.ExceptionEN = "";
                            item.PrivilegeTH = item.PrivilegeEN = "";
                            item.CheckListTH = item.CheckListEN = "";
                            item.ConsentTH = item.ConsentEN = "";
                        }
                        item.SaleChannel = ProductSaleChannel.All;
                        item.ProductCode = NullUtils.cvString(dr[2]);

                        item.CompanyCode = NullUtils.cvString(dr[3]);
                        if (companies.ContainsKey(dr[3].ToString())) { item.CompanyId = companies[dr[3].ToString()]; } else { sb.AppendLine("- Insurance Code Incorrect. [" + dr[3].ToString() + "]<br/>"); }

                        item.Class = NullUtils.cvString(dr[4]);
                        item.SubClass = NullUtils.cvString(dr[5]);

                        item.CategoryId = ProductCategoryKey.TA;
                        item.SubCategoryId = ProductSubCategoryKey.TA;

                        if (!subClass.Contains(item.Class + "|" + item.SubClass)) {
                            if (!subClass.Contains(item.Class)) {
                                sb.AppendLine("- Class Incorrect. [" + dr[4].ToString() + "]<br/>");
                            } else {
                                sb.AppendLine("- Sub Class Incorrect. [" + dr[5].ToString() + "]<br/>");
                            }
                        }

                        item.InsuranceProductCode = NullUtils.cvString(dr[6]);
                        if (item.InsuranceProductCode.Length > 20) sb.AppendLine("- Insurance Product Code Incorrect. [" + dr[6].ToString() + "]<br/>");
                        if (sheetLanguage == Language.TH) {
                            item.TitleTH = NullUtils.cvString(dr[7]);
                        } else {
                            item.TitleEN = NullUtils.cvString(dr[7]);
                        }
                        item.ActiveDate = GetDate(dr[9]);
                        if (item.ActiveDate == SqlMinDate()) sb.AppendLine("- Active Date Incorrect. [" + NullUtils.cvString(dr[9]) + "]<br/>");
                        item.ExpireDate = GetDate(dr[10]);
                        if (item.ExpireDate == SqlMinDate()) sb.AppendLine("- Expire Date Incorrect. [" + NullUtils.cvString(dr[10]) + "]<br/>");
                        if (item.ExpireDate <= item.ActiveDate) sb.AppendLine("- Expire Date Incorrect. [" + NullUtils.cvString(dr[10]) + " >= " + NullUtils.cvString(dr[9]) + "]<br/>");
                        switch (NullUtils.cvString(dr[11]).ToUpper()) {
                            case "A": item.SaleChannel = ProductSaleChannel.All; break;
                            case "Y": item.SaleChannel = ProductSaleChannel.Online; break;
                            case "N": item.SaleChannel = ProductSaleChannel.Offline; break;
                            default: sb.AppendLine("- Sale Channel Incorrect. [" + NullUtils.cvString(dr[11]) + "]<br/>"); break;
                        }
                        item.CreateDate = item.UpdateDate = DateTime.Now;
                        item.CreateAdminId = item.UpdateAdminId = Admin.Id;
                        item.Status = Status.Active;
                        
                        premium = new ProductTAPremium();
                        premium.PlanCode = GetInt(dr[8], "Plan Code", ref sb);
                        if (premium.PlanCode > 999999) {
                            sb.AppendLine("- Plan Code Incorrect. [" + NullUtils.cvString(dr[8]) + "]<br/>");
                        }
                        premium.EPolicy = GetYesNo(NullUtils.cvString(dr[12]), "E-Policy", ref sb);
                        premium.SumInsured = GetDecimal(dr[13], premium.GetDisplayName(x => x.SumInsured), ref sb);
                        premium.NetPremium = GetDecimal(dr[14], premium.GetDisplayName(x => x.NetPremium), ref sb);
                        premium.Duty = GetDecimal(dr[15], premium.GetDisplayName(x => x.Duty), ref sb);
                        premium.Vat = GetDecimal(dr[16], premium.GetDisplayName(x => x.Vat), ref sb);
                        premium.Premium = GetDecimal(dr[17], premium.GetDisplayName(x => x.Premium), ref sb);
                        premium.DisplayPremium = GetDecimal(dr[18], premium.GetDisplayName(x => x.DisplayPremium), ref sb);
                        CheckPremium(premium.NetPremium, premium.Duty, premium.Vat, premium.Premium, ref sb);

                        if (tripTypes.ContainsKey(NullUtils.cvString(dr[19]))) {
                            premium.TripType = tripTypes[NullUtils.cvString(dr[19])]; 
                        } else { sb.AppendLine("- Trip Type Incorrect. [" + NullUtils.cvString(dr[19]) + "]<br/>"); }
                        if (zones.ContainsKey(NullUtils.cvString(dr[20]).ToLower())) {
                            premium.Zone = zones[NullUtils.cvString(dr[20]).ToLower()];
                        } else { sb.AppendLine("- Zone Incorrect. [" + NullUtils.cvString(dr[20]) + "]<br/>"); }
                        if (coverageOptions.ContainsKey(NullUtils.cvString(dr[21]))) {
                            premium.CoverageOption = coverageOptions[NullUtils.cvString(dr[21])];
                        } else { sb.AppendLine("- Coverage Options Incorrect. [" + NullUtils.cvString(dr[21]) + "]<br/>"); }
                        if (coverageTypes.ContainsKey(NullUtils.cvString(dr[22]))) {
                            premium.CoverageType = coverageTypes[NullUtils.cvString(dr[22])];
                        } else { sb.AppendLine("- Coverage Type Incorrect. [" + NullUtils.cvString(dr[22]) + "]<br/>"); }

                        var days = GetDataRange(dr[23], premium.GetDisplayName(x => x.MinDays) , ref sb);
                        premium.MinDays = days.Item1;
                        premium.MaxDays = days.Item2;
                        premium.Persons = GetInt(dr[24], premium.GetDisplayName(x => x.Persons), ref sb);

                        if (sheetLanguage == Language.TH) {
                            premium.PersonalLossLifeTH = GetString(dr[25], premium.GetDisplayName(x => x.PersonalLossLifeTH), ref sb);
                            premium.MedicalExpenseEachAccidentTH = GetString(dr[26], premium.GetDisplayName(x => x.MedicalExpenseEachAccidentTH), ref sb);
                            premium.MedicalExpenseAccidentSicknessTH = GetString(dr[27], premium.GetDisplayName(x => x.MedicalExpenseAccidentSicknessTH), ref sb);
                            premium.EmergencyMedialEvacuationTH = GetString(dr[28], premium.GetDisplayName(x => x.EmergencyMedialEvacuationTH), ref sb);
                            premium.RepatriationExpenseTH = GetString(dr[29], premium.GetDisplayName(x => x.RepatriationExpenseTH), ref sb);
                            premium.RepatriationMortalRemainsTH = GetString(dr[30], premium.GetDisplayName(x => x.RepatriationMortalRemainsTH), ref sb);
                            premium.LossDamageBaggageTH = GetString(dr[31], premium.GetDisplayName(x => x.LossDamageBaggageTH), ref sb);
                        } else {
                            premium.PersonalLossLifeEN = GetString(dr[25], premium.GetDisplayName(x => x.PersonalLossLifeEN), ref sb);
                            premium.MedicalExpenseEachAccidentEN = GetString(dr[26], premium.GetDisplayName(x => x.MedicalExpenseEachAccidentEN), ref sb);
                            premium.MedicalExpenseAccidentSicknessEN = GetString(dr[27], premium.GetDisplayName(x => x.MedicalExpenseAccidentSicknessEN), ref sb);
                            premium.EmergencyMedialEvacuationEN = GetString(dr[28], premium.GetDisplayName(x => x.EmergencyMedialEvacuationEN), ref sb);
                            premium.RepatriationExpenseEN = GetString(dr[29], premium.GetDisplayName(x => x.RepatriationExpenseEN), ref sb);
                            premium.RepatriationMortalRemainsEN = GetString(dr[30], premium.GetDisplayName(x => x.RepatriationMortalRemainsEN), ref sb);
                            premium.LossDamageBaggageEN = GetString(dr[31], premium.GetDisplayName(x => x.LossDamageBaggageEN), ref sb);
                        }

                        item.ProductTAPremium = new List<ProductTAPremium> {
                            premium
                        };
                    } catch (Exception ex) {
                        sb.AppendLine("- Invalid data " + ex.ToString());
                    }
                    return Tuple.Create(item, sb.ToString());
                };
                
                var i = 0;
                foreach (DataRow dr in dtProductTH.Rows) {
                    i++;
                    if (i < 6) continue;
                    sheetLanguage = Language.TH;
                    var row = importRow(dr);
                    if (row.Item1.InsuranceProductCode.Length == 0) continue;
                    Product product = row.Item1;
                    string error = row.Item2;
                    sheetLanguage = Language.EN;
                    var rowEN = importRow(dtProductEN.Rows[i - 1]);
                    Product productEN = rowEN.Item1;
                    int productIndex = product.ProductTAPremium.Count - 1;
                    if (product.InsuranceProductCode != productEN.InsuranceProductCode ||
                        product.ProductTAPremium[productIndex].TripType != productEN.ProductTAPremium[productIndex].TripType ||
                        product.ProductTAPremium[productIndex].Zone != productEN.ProductTAPremium[productIndex].Zone ||
                        product.ProductTAPremium[productIndex].CoverageOption != productEN.ProductTAPremium[productIndex].CoverageOption ||
                        product.ProductTAPremium[productIndex].CoverageType != productEN.ProductTAPremium[productIndex].CoverageType ||
                        product.ProductTAPremium[productIndex].MinDays != productEN.ProductTAPremium[productIndex].MinDays ||
                        product.ProductTAPremium[productIndex].MaxDays != productEN.ProductTAPremium[productIndex].MaxDays ||
                        product.ProductTAPremium[productIndex].PlanCode != productEN.ProductTAPremium[productIndex].PlanCode) {
                        result.Success = false;
                        sbError.AppendLine("<tr><th>Row " + i.ToString() + "</th><td>ข้อมูล Product (TH) และ Product (EN) ไม่ตรงกัน</td>");
                    } else {
                        product.TitleEN = productEN.TitleEN;

                        product.ProductTAPremium[productIndex].PersonalLossLifeEN = productEN.ProductTAPremium[productIndex].PersonalLossLifeEN;
                        product.ProductTAPremium[productIndex].MedicalExpenseEachAccidentEN = productEN.ProductTAPremium[productIndex].MedicalExpenseEachAccidentEN;
                        product.ProductTAPremium[productIndex].MedicalExpenseAccidentSicknessEN = productEN.ProductTAPremium[productIndex].MedicalExpenseAccidentSicknessEN;
                        product.ProductTAPremium[productIndex].EmergencyMedialEvacuationEN = productEN.ProductTAPremium[productIndex].EmergencyMedialEvacuationEN;
                        product.ProductTAPremium[productIndex].RepatriationExpenseEN = productEN.ProductTAPremium[productIndex].RepatriationExpenseEN;
                        product.ProductTAPremium[productIndex].RepatriationMortalRemainsEN = productEN.ProductTAPremium[productIndex].RepatriationMortalRemainsEN;
                        product.ProductTAPremium[productIndex].LossDamageBaggageEN = productEN.ProductTAPremium[productIndex].LossDamageBaggageEN;

                        product.ApproveAdminId = product.OwnerAdminId = "";
                        product.ApproveStatus = ApproveStatus.Pending;

                        if (!items.ContainsKey(product.InsuranceProductCode)) {
                            if (productCountries.ContainsKey(product.InsuranceProductCode)) {
                                product.ProductTACountry = productCountries[product.InsuranceProductCode];
                            }
                            items.Add(product.InsuranceProductCode, product);
                        } else {
                            items[product.InsuranceProductCode].ProductTAPremium.Add(product.ProductTAPremium[0]);
                        }
                    }
                    if (error.Length > 0) {
                        result.Success = false;
                        sbError.AppendLine("<tr><th>Row " + i.ToString() + "</th><td>" + error.ToString() + "</td>");
                    }
                }
            } catch (Exception ex) {
                result.Success = false;
                sbError.AppendLine("<tr><th>Summary</th><td>" + ex.ToString() + "</td>");
            }
            if (result.Success) {
                result.Success = repo.AddProducts(items.Values.ToList()) > 0;
            } else {
                ViewBag.ErrorMessage = sbError.ToString();
            }

            return Tuple.Create(result, items.Values.ToList());
        }

        #endregion

        #region Function 

        private bool GetYesNo(string value, string label, ref StringBuilder sb) {
            if (string.Equals(value, "Y", StringComparison.InvariantCultureIgnoreCase)) return true;
            if (string.Equals(value, "N", StringComparison.InvariantCultureIgnoreCase)) return false;
            sb.AppendLine("- " + label + " Incorrect. [" + value + "].<br/>");
            return false;
        }

        private Tuple<int, int> GetYearMinMax(string value, string label, ref StringBuilder sb) {
            int year1 = 0, year2 = 0;
            value = value.Replace(" ", "");
            string[] year = value.Split('-');
            //if (year.Length > 0) {
            //    year1 = NullUtils.cvInt(year[0]);
            //    year2 = year1;
            //}
            //if (year.Length > 1) {
            //    var y = NullUtils.cvInt(year[1]);
            //    if (y > 0) year2 = y;
            //}
            if (year.Length == 2) {
                year1 = NullUtils.cvInt(year[0]);
                year2 = NullUtils.cvInt(year[1]);
                if (string.Equals((year1.ToString() + "-" + year2.ToString()), value) && year2 >= year1) return Tuple.Create(year1, year2);
            }
            sb.AppendLine("- " + label + " Incorrect. [" + value + "].<br/>");
            return Tuple.Create(0, 0);
        }

        private Dictionary<string, int> GetCompanies() {
            CompanyRepository companyRepo = new CompanyRepository();
            var items = companyRepo.ListCompany_Dictionary_Admin();
            return new Dictionary<string, int>(items, StringComparer.OrdinalIgnoreCase);
        }

        private Dictionary<string, Product> GetCoveragePrivilege(DataSet ds) {
            DataTable dtCoverageTH = ds.Tables["Coverage (TH)"];
            DataTable dtCoverageEN = ds.Tables["Coverage (EN)"];
            DataTable dtCheckListTH = ds.Tables["CheckList (TH)"];
            DataTable dtCheckListEN = ds.Tables["CheckList (EN)"];
            DataTable dtPrivilegeTH = ds.Tables["Privilege (TH)"];
            DataTable dtPrivilegeEN = ds.Tables["Privilege (EN)"];
            Dictionary<string, Product> items = new Dictionary<string, Product>();
            try {
                Language language = Language.TH;
                foreach (var dt in new[] { dtCoverageTH, dtCoverageEN }) {
                    if (dt != null) {
                        int column = dt.Columns.Count;
                        for (int i = 2; i < column - 1; i++) {
                            string code = dt.Rows[1][i]?.ToString();
                            if (string.IsNullOrEmpty(code)) continue;
                            
                            if (!items.ContainsKey(code)) items.Add(code, new Product() { InsuranceProductCode = code });
                            if (language == Language.TH) {
                                items[code].CoverageTH = GetStringTableRows(dt, i, 5, 34);
                                items[code].SpecialCoverageTH = GetStringRows(dt, i, 36, 65);
                                items[code].ConditionTH = GetStringRows(dt, i, 67, 96);
                                items[code].ExceptionTH = GetStringRows(dt, i, 98, 127);
                                items[code].ConsentTH = GetStringRows(dt, i, 129, 158);
                            } else {
                                items[code].CoverageEN = GetStringTableRows(dt, i, 5, 34);
                                items[code].SpecialCoverageEN = GetStringRows(dt, i, 36, 65);
                                items[code].ConditionEN = GetStringRows(dt, i, 67, 96);
                                items[code].ExceptionEN = GetStringRows(dt, i, 98, 127);
                                items[code].ConsentEN = GetStringRows(dt, i, 129, 158);
                            }
                        }

                    }
                    language = Language.EN;
                }
                language = Language.TH;
                foreach (var dt in new[] { dtCheckListTH, dtCheckListEN }) {
                    if (dt != null) {
                        int column = dt.Columns.Count;
                        for (int i = 2; i < column; i++) {
                            string code = dt.Rows[1][i]?.ToString();
                            if (string.IsNullOrEmpty(code)) continue;

                            if (!items.ContainsKey(code)) items.Add(code, new Product() { InsuranceProductCode = code });
                            if (language == Language.TH) {
                                items[code].CheckListTH = GetStringRows(dt, i, 5, 34);
                            } else {
                                items[code].CheckListEN = GetStringRows(dt, i, 5, 34);
                            }
                        }
                    }
                    language = Language.EN;
                }
                language = Language.TH;
                foreach (var dt in new[] { dtPrivilegeTH, dtPrivilegeEN }) {
                    if (dt != null) {
                        int column = dt.Columns.Count;
                        for (int i = 2; i < column; i++) {
                            string code = dt.Rows[1][i]?.ToString();
                            if (string.IsNullOrEmpty(code)) continue;
                            
                            if (!items.ContainsKey(code)) items.Add(code, new Product() { InsuranceProductCode = code });
                            if (language == Language.TH) {
                                items[code].PrivilegeTH = GetStringRows(dt, i, 5, 34);
                            } else {
                                items[code].PrivilegeEN = GetStringRows(dt, i, 5, 34);
                            }
                        }
                    }
                    language = Language.EN;
                }
                return items;
            } catch { }
            return null;
        }

        private string[] GetDataSplit(object val) {
            return NullUtils.cvString(val).Replace(" ", "").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
        }
        
        private Tuple<int, int> GetDataRange(object val, string label, ref StringBuilder sb, bool allowZero = false) {
            string s = NullUtils.cvString(val);
            var x = NullUtils.cvString(s).Replace(" ", "").Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
            int i1 = 0, i2 = 0;
            if (x.Length == 1) {
                i1 = i2 = NullUtils.cvInt(x[0]);
                if (i1 > 0) {
                    return Tuple.Create(i1, i2);
                } else {
                    sb.AppendLine("- " + label + " Incorrect. [" + s + "].<br/>");
                }
            } else if (x.Length == 2) {
                i1 = NullUtils.cvInt(x[0]);
                i2 = NullUtils.cvInt(x[1]);
                if ((allowZero || i1 > 0) && i2 > 0 && i2 >= i1) {
                    return Tuple.Create(i1, i2);
                } else {
                    sb.AppendLine("- " + label + " Incorrect. [" + s + "].<br/>");
                }
            }
            return Tuple.Create(i1, i2);
        }

        private Tuple<decimal, decimal> GetDataRangeDecimal(object val, string label, ref StringBuilder sb) {
            string s = NullUtils.cvString(val);
            var x = NullUtils.cvString(s).Replace(" ", "").Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
            decimal i1 = 0, i2 = 0;
            if (x.Length == 1) {
                i1 = i2 = NullUtils.cvDec(x[0]);
                if (i1 > 0) {
                    return Tuple.Create(i1, i2);
                } else {
                    sb.AppendLine("- " + label + " Incorrect. [" + s + "].<br/>");
                }
            } else if (x.Length == 2) {
                i1 = NullUtils.cvDec(x[0]);
                i2 = NullUtils.cvDec(x[1]);
                if (i1 > 0 && i2 > 0 && i2 >= i1) {
                    return Tuple.Create(i1, i2);
                } else {
                    sb.AppendLine("- " + label + " Incorrect. [" + s + "].<br/>");
                }
            }
            return Tuple.Create(i1, i2);
        }

        private DateTime GetDate(object val, string format = "d/M/yyyy", bool isThaiCalendar = false) {
            DateTime date;
            System.Globalization.CultureInfo ci;
            if (isThaiCalendar) {
                ci = new System.Globalization.CultureInfo("th-TH");
            } else {
                ci = new System.Globalization.CultureInfo("en-GB");
            }
            string v = NullUtils.cvString(val);
            if (DateTime.TryParseExact(v, format, ci, System.Globalization.DateTimeStyles.None, out date)) {
                return date;
            } else {
                return SqlMinDate();
            }
        }

        private int GetInt(object val, string label, ref StringBuilder sb) {
            string s = NullUtils.cvString(val);
            if (string.IsNullOrWhiteSpace(s) || string.Equals(s, "-")) return 0;
            int result;
            if (int.TryParse(s, out result)) {
                return result;
            } else {
                sb.AppendLine("- " + label + " Incorrect. [" + s + "].<br/>");
            }
            return 0;
        }

        private Decimal GetDecimal(object val, string label, ref StringBuilder sb) {
            string s = NullUtils.cvString(val);
            if (string.IsNullOrWhiteSpace(s) || string.Equals(s, "-")) return 0;
            Decimal dec;
            if (decimal.TryParse(s, out dec)) {
                return dec;
            } else {
                sb.AppendLine("- " + label + " Incorrect. [" + s + "].<br/>");
            }
            return 0;
        }

        private String GetString(object val, string label, ref StringBuilder sb, int maxLength = 250) {
            string s = NullUtils.cvString(val);
            if (s.Length > maxLength) {
                sb.AppendLine("- " + label + " Incorrect. [" + s + "].<br/>");
            }
            return s.Trim();
        }

        private void CheckPremium(decimal netPremium, decimal duty, decimal vat, decimal premium, ref StringBuilder sb) {
            if (premium != (netPremium + vat + duty)) {
                sb.AppendLine("- Premium is Incorrect. [" + premium + " <> (" + netPremium + " + " + duty + " + " + vat + ")].<br/>");
            }
        }

        private bool CheckPremium(decimal netPremium, decimal duty, decimal vat, decimal premium) {
            return (premium == (netPremium + vat + duty));
        }

        private List<int> GetCountries(Dictionary<string, int> countries, string[] vals, string label, ref StringBuilder sb) {
            List<int> items = new List<int>();
            foreach (var item in vals) {
                if (countries.ContainsKey(item)) {
                    items.Add(countries[item]); // new Country() { Id = countries[item] });
                } else {
                    sb.AppendLine("- " + label + " Incorrect. [" + item + "].<br/>");
                }
            }
            return items;
        }
        
        private DateTime SqlMinDate() {
            return new DateTime(1753, 1, 1);
        }

        private String GetStringRows(DataTable dt, int column, int minRow, int maxRow) {
            if ((minRow > dt.Rows.Count + 1) || (column > dt.Columns.Count + 1)) return "";
            maxRow = Math.Min(maxRow, dt.Rows.Count - 1);
            StringBuilder sb = new StringBuilder();

            for (int i = minRow - 1; i < maxRow - 1; i++) {
                var val = dt.Rows[i][column];
                if (val != null && !string.IsNullOrEmpty(val.ToString().Trim())) {
                    sb.AppendLine(val.ToString().Trim());
                }
            }
            return sb.ToString();
        }

        private String GetStringTableRows(DataTable dt, int column, int minRow, int maxRow) {
            if ((minRow > dt.Rows.Count + 1) || (column > dt.Columns.Count + 1)) return "";
            maxRow = Math.Min(maxRow, dt.Rows.Count - 1);
            StringBuilder sb = new StringBuilder();

            for (int i = minRow - 1; i < maxRow - 1; i++) {
                var val = dt.Rows[i][column];
                var td = dt.Rows[i][column + 1];
                if (val != null && !string.IsNullOrEmpty(val.ToString().Trim())) {
                    sb.Append("<tr><td>" + val.ToString().Trim() + "</td><td>" + td.ToString().Trim() + "</td></tr>");
                }
            }
            return sb.ToString();
        }

        private String[] SplitStringToRow(string value, int maxRow) {
            List<string> vals = new List<string>();
            if (value == null) return vals.ToArray();
            var tmp = value.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
            if (tmp.Count() <= maxRow) return tmp;
            for (int i = 0; i < maxRow; i++) {
                vals.Add(tmp[i]);
            }
            for (int i = maxRow; i < tmp.Count(); i++) {
                vals[maxRow - 1] += tmp[i];
            }
            return vals.ToArray();
        }

        private Tuple<String[], String[]> SplitTableToRow(string value, int maxRow) {
            List<String> th = new List<string>();
            List<String> td = new List<string>();
            if (value == null) return Tuple.Create(th.ToArray(), td.ToArray());
            var doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(value);


            if (doc.DocumentNode.SelectNodes("//tr") != null) {
                foreach (HtmlAgilityPack.HtmlNode row in doc.DocumentNode.SelectNodes("//tr")) {
                    int i = 0;
                    string tht = "";
                    string tdt = "";
                    foreach (HtmlAgilityPack.HtmlNode col in row.SelectNodes("td")) {
                        if (i == 0) {
                            tht = col.InnerText;
                        } else {
                            tdt = col.InnerText;
                        }
                        i++;
                    }
                    th.Add(tht);
                    td.Add(tdt);
                }
            }
            return Tuple.Create(th.ToArray(), td.ToArray());
        }

        private String GetCoverage(string prefix) {
            StringBuilder sb = new StringBuilder();
            for (int i = 1; i <= 30; i++) {
                sb.Append("<tr><td>" + Request.Form[prefix + "_" + i + "_th"] + "</td><td>" + Request.Form[prefix + "_" + i + "_td"] + "</td></tr>");
            }
            return sb.ToString();
        }

        #endregion
    }
}
