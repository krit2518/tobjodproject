﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TobJod.Admin.Models;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class ProductCategoryController : BaseController {
        ProductCategoryRepository repo;

        public ProductCategoryController() {
            repo = new ProductCategoryRepository();
        }
        
        // GET: ProductCategory
        public ActionResult Index() {
            return View();
        }

        // GET: ProductCategory/Edit/5
        public ActionResult Edit(int id) {
            ProductCategory item = repo.GetProductCategory_Admin(id);
            item.BriefTH = item.BriefTH;
            item.BriefEN = item.BriefEN;
            BindForm();
            return View("Form", item);
        }

        // POST: ProductCategory/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, ProductCategory item) {
            int result = 0;
            try {
                item.BriefTH = item.BriefTH;
                item.BriefEN = item.BriefEN;
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = Admin.Id; // = item.OwnerAdminId

                result = repo.UpdateProductCategory(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            BindForm();
            return View("Form", item);
        }
        
        public ActionResult DataTable() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();
            
            if (param.hasSearch) {
                whereC.Add(" (TitleTH LIKE @searchValue OR TitleEN LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "Id", "TitleTH", "DisplayHome", "Sequence", "UpdateDate", "Status"};
            DataTableData<ProductCategory> data = repo.ListProductCategory_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Id.ToString(),
                        x.TitleTH,
                        (x.DisplayHome ? "Y" : "").ToString(),
                        x.Sequence.ToString(),
                        x.UpdateDate.ToString("dd/MM/yyyy HH:mm"),
                        x.Status.ToString(),
                        (
                            HtmlActionButton(AdminAction.Edit, x.Id) + 
                            HtmlActionButton(AdminAction.Delete, x.Id) 
                        )
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        private void BindForm() {
        }
    }
}
