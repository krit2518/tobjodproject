﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class SubscribeController : BaseController {
        SubscribeRepository repo;

        public SubscribeController() {
            repo = new SubscribeRepository();
        }
        
        // GET: Subscribe
        public ActionResult Index() {
            //Subscribe item = new Subscribe();
            //item.Category = NullUtils.cvInt(Request.QueryString["category"]);
            return View();
        }
        
        // GET: Subscribe/Delete/5
        public ActionResult Delete(int id) {
            var result = repo.DeleteSubscribe(id, Admin.Id);
            if (result > 0) {
                Notify_Delete_Success();
            } else {
                Notify_Delete_Fail();
            }
            return RedirectToAction("Index");
        }

        // GET: Subscribe/Export
        public void Export() {
            var dt = repo.ListSubscribe_Export("", null);
            ExportFile("Subscribe.xlsx", "Subscribe", dt, null, null, new string[] { "CreateDate" });
        }

        // GET: Subscribe/DataTable
        public ActionResult DataTable() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();
            
            if (param.hasSearch) {
                whereC.Add(" (Email LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "Id", "Email", "CreateDate" };
            DataTableData<Subscribe> data = repo.ListSubscribe_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Id.ToString(),
                        x.Email.ToString(),
                        x.CreateDate.ToString("dd/MM/yyyy HH:mm"),
                        (HtmlActionButton(AdminAction.Edit, x.Id) + HtmlActionButton(AdminAction.Delete, x.Id))
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}
