﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class ContactSubCategoryController : BaseController {
        ContactSubCategoryRepository repo;

        public ContactSubCategoryController() {
            repo = new ContactSubCategoryRepository();
        }
        
        // GET: ContactSubCategory
        public ActionResult Index() {
            return View();
        }

        // GET: ContactSubCategory/Create
        public ActionResult Create() {
            ContactSubCategory item = new ContactSubCategory();
            item.Status = Status.Active;
            BindForm();
            return View("Form", item);
        }

        // POST: ContactSubCategory/Create
        [HttpPost]
        public ActionResult Create(ContactSubCategory item) {
            try {
                item.CreateDate = item.UpdateDate = DateTime.Now;
                item.CreateAdminId = item.UpdateAdminId = Admin.Id;
                //item.MailTo = item.MailTo.Replace("\r\n", ";");

                repo.AddContactSubCategory(item);
                Notify_Add_Success();
                return RedirectToAction("Index");
            } catch { }
            Notify_Add_Fail();
            BindForm();
            return View("Form", item);
        }

        // GET: ContactSubCategory/Edit/5
        public ActionResult Edit(int id) {
            ContactSubCategory item = repo.GetContactSubCategory_Admin(id);
            BindForm();
            return View("Form", item);
        }

        // POST: ContactSubCategory/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, ContactSubCategory item) {
            int result = 0;
            try {
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = Admin.Id;
                //item.MailTo = item.MailTo.Replace("\r\n", ";");

                result = repo.UpdateContactSubCategory(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            BindForm();
            return View("Form", item);
        }

        // GET: ContactSubCategory/Delete/5
        public ActionResult Delete(int id) {
            LeadRepository leadRepo = new LeadRepository();
            var items = leadRepo.CountLeadBySubject_Admin(id);
            if (items == 0) {
                var result = repo.DeleteContactSubCategory(id, Admin.Id);
                if (result > 0) {
                    Notify_Delete_Success();
                } else {
                    Notify_Delete_Fail();
                }
            } else {
                Notify(NotifyMessageType.Error, "Delete Error", "Record is in used.");
            }
            return RedirectToAction("Index");
        }

        // GET: ContactSubCategory/DataTable
        public ActionResult DataTable() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();

            //if (NullUtils.cvInt(Request.QueryString["category"]) > 0) {
            //whereC.Add(" (categoryId=@category) ");
            //whereParams.category = NullUtils.cvInt(Request.QueryString["category"]);
            //}

            if (param.hasSearch) {
                whereC.Add(" (TitleTH LIKE @searchValue OR TitleEN LIKE @searchValue OR Category LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "Id", "Sequence", "Category", "TitleTH", "MailTo", "UpdateDate", "Status" };
            DataTableData<ContactSubCategory> data = repo.ListContactSubCategory_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Id.ToString(),
                        x.Sequence.ToString(),
                        x.Category.ToString(),
                        x.TitleTH.ToString(),
                        x.MailTo.ToString().Replace(";","<br/>"),
                        x.UpdateDate.ToString("dd/MM/yyyy HH:mm"),
                        x.Status.ToString(),
                        (HtmlActionButton(AdminAction.Edit, x.Id) + HtmlActionButton(AdminAction.Delete, x.Id))
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        private void BindForm() {
            ContactCategoryRepository categoryRepo = new ContactCategoryRepository();
            ViewBag.CategoryList = categoryRepo.ListContactCategory_SelectList_Admin();
        }
    }

}
