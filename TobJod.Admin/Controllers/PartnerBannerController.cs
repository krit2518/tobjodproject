﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TobJod.Admin.Models;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class PartnerBannerController : BaseController {
        PartnerBannerRepository repo;

        public PartnerBannerController() {
            Path = System.Configuration.ConfigurationManager.AppSettings["Path_PartnerBanner"];
            repo = new PartnerBannerRepository();
        }

        
        // GET: PartnerBanner
        public ActionResult Index() {
            return View();
        }

        // GET: PartnerBanner/Create
        public ActionResult Create() {
            PartnerBannerViewModel item = new PartnerBannerViewModel();
            item.Status = Status.Active;
            return View("Form", item);
        }

        // POST: PartnerBanner/Create
        [HttpPost]
        public ActionResult Create(PartnerBannerViewModel item) {
            if (UploadUtils.IsImage(item.ImageTHFile, item.ImageENFile)) {
                try {
                    item.CreateDate = item.UpdateDate = DateTime.Now;
                    item.CreateAdminId = item.UpdateAdminId = Admin.Id;
                    item.ImageTH = UploadUtils.SaveFile(item.ImageTHFile, PathPhy);
                    item.ImageEN = UploadUtils.SaveFile(item.ImageENFile, PathPhy);

                    repo.AddPartnerBanner(item);
                    Notify_Add_Success();
                    return RedirectToAction("Index");
                } catch { }
            }
            Notify_Add_Fail();
            return View("Form", item);
        }

        // GET: PartnerBanner/Edit/5
        public ActionResult Edit(int id) {
            PartnerBanner model = repo.GetPartnerBanner_Admin(id);
            PartnerBannerViewModel item = PartnerBannerViewModel.Map(model);
            return View("Form", item);
        }

        // POST: PartnerBanner/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, PartnerBannerViewModel item) {
            int result = 0;
            if (UploadUtils.IsImage(item.ImageTHFile, item.ImageENFile)) {
                try {
                    item.UpdateDate = DateTime.Now;
                    item.UpdateAdminId = Admin.Id; // = item.OwnerAdminId
                    item.ImageTH = UploadUtils.SaveAndReplaceFile(item.ImageTHFile, PathPhy, item.ImageTH);
                    item.ImageEN = UploadUtils.SaveAndReplaceFile(item.ImageENFile, PathPhy, item.ImageEN);

                    result = repo.UpdatePartnerBanner(item);
                    if (result > 0) {
                        Notify_Update_Success();
                        return RedirectToAction("Index");
                    }
                } catch { }
            }
            Notify_Update_Fail();
            return View("Form", item);
        }

        // GET: PartnerBanner/View/5
        public ActionResult View(int id) {
            PartnerBanner model = repo.GetPartnerBanner_Admin(id);
            PartnerBannerViewModel item = PartnerBannerViewModel.Map(model);
            return View("View", item);
        }

        // GET: PartnerBanner/Disable/5
        public ActionResult Disable(int id) {
            PartnerBanner item = new PartnerBanner();
            item.Id = id;
            item.Status = Status.Inactive;
            item.UpdateDate = DateTime.Now;
            item.UpdateAdminId = Admin.Id;
            var result = repo.UpdatePartnerBannerStatus(item);
            if (result > 0) {
                Notify_Update_Success();
            } else {
                Notify_Update_Fail();
            }
            return RedirectToAction("Index");
        }

        // GET: PartnerBanner/Delete/5
        public ActionResult Delete(int id) {
            var item = repo.GetPartnerBanner_Admin(id);
            var result = repo.DeletePartnerBanner(id, Admin.Id);
            if (result > 0) {
                FileUtils.DeleteFiles(PathPhy, item.ImageEN, item.ImageTH);
                Notify_Delete_Success();
            } else {
                Notify_Delete_Fail();
            }
            return RedirectToAction("Index");
        }

        public ActionResult DataTable() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();

            if (param.hasSearch) {
                whereC.Add(" (TitleTH LIKE @searchValue OR TitleEN LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "Id", "Sequence", "TitleTH", "ImageTH", "UpdateDate", "Status" };
            DataTableData<PartnerBanner> data = repo.ListPartnerBanner_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Id.ToString(),
                        x.Sequence.ToString(),
                        x.TitleTH.ToString(),
                        HtmlImage(PathRel, x.ImageTH),
                        x.UpdateDate.ToString("dd/MM/yyyy HH:mm"),
                        x.Status.ToString(),
                        (
                            HtmlActionButton(AdminAction.View, x.Id) + 
                            HtmlActionButton(AdminAction.Edit, x.Id) +
                            HtmlActionButton(AdminAction.Delete, x.Id) 
                        )
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

    }
}
