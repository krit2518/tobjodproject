﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using TobJod.Admin.Models;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class TransactionLogController : BaseController {
        TransactionLogRepository repo;

        public TransactionLogController() {
            repo = new TransactionLogRepository();
        }

        // GET: AuthenLog
        public ActionResult Index(TransactionLogViewModel model) {
            model.DateFrom = ValueUtils.GetDate(Request.QueryString["DateFrom"]);
            model.DateTo = ValueUtils.GetDate(Request.QueryString["DateTo"]);
            if (model.DateFrom == DateUtils.SqlMinDate()) model.DateFrom = DateUtils.GetFirstDayOfMonth(DateTime.Now);
            if (model.DateTo == DateUtils.SqlMinDate()) model.DateTo = DateTime.Now;

            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Text = "Admin", Value = "Admin" });
            items.Add(new SelectListItem() { Text = "Admin Group", Value = "AdminGroup" });
            items.Add(new SelectListItem() { Text = "Bank", Value = "Bank" });
            items.Add(new SelectListItem() { Text = "Banner", Value = "Banner" });
            items.Add(new SelectListItem() { Text = "Campaign", Value = "Campaign" });
            items.Add(new SelectListItem() { Text = "Car Brand", Value = "CarBrand" });
            items.Add(new SelectListItem() { Text = "Car Family", Value = "CarFamily" });
            items.Add(new SelectListItem() { Text = "Car Model", Value = "CarModel" });
            items.Add(new SelectListItem() { Text = "Company", Value = "Company" });
            items.Add(new SelectListItem() { Text = "Config", Value = "Config" });
            items.Add(new SelectListItem() { Text = "Config Page", Value = "ConfigPage" });
            items.Add(new SelectListItem() { Text = "Contact Category", Value = "ContactCategory" });
            items.Add(new SelectListItem() { Text = "Contact Sub Category", Value = "ContactSubCategory" });
            items.Add(new SelectListItem() { Text = "Coupon", Value = "Coupon" });
            items.Add(new SelectListItem() { Text = "Country", Value = "Country" });
            items.Add(new SelectListItem() { Text = "Day Off", Value = "DayOff" });
            items.Add(new SelectListItem() { Text = "District", Value = "District" });
            items.Add(new SelectListItem() { Text = "FAQ", Value = "Faq" });
            items.Add(new SelectListItem() { Text = "Lead", Value = "Lead" });
            items.Add(new SelectListItem() { Text = "Master Option", Value = "MasterOption" });
            items.Add(new SelectListItem() { Text = "News", Value = "News" });
            items.Add(new SelectListItem() { Text = "News Sub Category", Value = "NewsSubCategory" });
            items.Add(new SelectListItem() { Text = "Partner Banner", Value = "PartnerBanner" });
            items.Add(new SelectListItem() { Text = "Payment Method", Value = "PaymentMethod" });
            items.Add(new SelectListItem() { Text = "POI", Value = "Poi" });
            items.Add(new SelectListItem() { Text = "Popup", Value = "Popup" });
            items.Add(new SelectListItem() { Text = "Privilege", Value = "Privilege" });
            items.Add(new SelectListItem() { Text = "Product", Value = "Product" });
            items.Add(new SelectListItem() { Text = "Product Application", Value = "ProductApplication" });
            items.Add(new SelectListItem() { Text = "Product Category", Value = "ProductCategory" });
            items.Add(new SelectListItem() { Text = "Product Class", Value = "ProductClass" });
            items.Add(new SelectListItem() { Text = "Promotion", Value = "Promotion" });
            items.Add(new SelectListItem() { Text = "Province", Value = "Province" });
            items.Add(new SelectListItem() { Text = "Sub District", Value = "SubDistrict" });
            items.Add(new SelectListItem() { Text = "Subscribe", Value = "Subscribe" });
            items.Add(new SelectListItem() { Text = "System Message", Value = "SystemMessage" });
            items.Add(new SelectListItem() { Text = "Tag", Value = "Tag" });

            var x = GetTableColumns(model.Category);
            model.Columns = x.Columns;

            ViewBag.CategoryList = items;// new SelectList(items);
            return View(model);
        }

        // GET: AuthenLog/DataTable
        public ActionResult DataTable() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            //dynamic whereParams = new System.Dynamic.ExpandoObject();
            NameValueCollection whereParams = new NameValueCollection();
            List<string> whereC = new List<string>();

            var item = GetTableColumns(Request.Unvalidated.Form["category"]);

            if (param.hasSearch) {
                string[] ignore = { "ShadowDate", "ActiveDate", "ExpireDate", "Status", "Approve" };
                List<string> searchCol = new List<string>();
                foreach (var col in item.TableColumn) {
                    if (!ignore.Contains(col)) {
                        searchCol.Add(" " + col + " LIKE @searchValue ");
                    }
                }
                whereC.Add("(" + string.Join(" OR ", searchCol) + ")");
                whereParams.Add("@searchValue", "%" + param.searchValue + "%");
            }

            var dateFrom = ValueUtils.GetDate(Request.Unvalidated.Form["DateFrom"]);
            var dateTo = ValueUtils.GetDate(Request.Unvalidated.Form["DateTo"]);
            if (dateFrom == DateUtils.SqlMinDate()) dateFrom = DateUtils.GetFirstDayOfMonth(DateTime.Now);
            if (dateTo == DateUtils.SqlMinDate()) dateTo = DateTime.Now;

            whereC.Add(" (ShadowDate BETWEEN @DateFrom AND @DateTo) ");
            whereParams.Add("@DateFrom", dateFrom.ToString("yyyy-MM-dd HH:mm:ss"));
            whereParams.Add("@DateTo", DateUtils.ExtendToEndofDate(dateTo).ToString("yyyy-MM-dd HH:mm:ss"));

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            DataTableData<DataRow> data = repo.ListTransactionLog_DataTable(item.Table, item.TableColumn, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = FormatColumns(data.dataRaw, item); //.Select(x => x.ItemArray.Select(c => c.ToString()).ToArray()).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        private List<string[]> FormatColumns(List<DataRow> items, TransactionLogViewModel model) {
            int statusColumn = -1;
            int approveStatusColumn = -1;
            int eventDate = -1;
            int activeDate = -1;
            int expireDate = -1;
            if (model.TableColumn.Contains("Status")) statusColumn = Array.FindIndex(model.TableColumn, x => x == "Status");
            if (model.TableColumn.Contains("ApproveStatus")) approveStatusColumn = Array.FindIndex(model.TableColumn, x => x == "ApproveStatus");
            if (model.TableColumn.Contains("EventDate")) eventDate = Array.FindIndex(model.TableColumn, x => x == "EventDate");
            if (model.TableColumn.Contains("ActiveDate")) activeDate = Array.FindIndex(model.TableColumn, x => x == "ActiveDate");
            if (model.TableColumn.Contains("ExpireDate")) expireDate = Array.FindIndex(model.TableColumn, x => x == "ExpireDate");
            List<string[]> vals = new List<string[]>();
            foreach (var item in items) {
                string[] val = item.ItemArray.Select(c => c.ToString()).ToArray();
                if (statusColumn >= 0) {
                    switch (NullUtils.cvInt(val[statusColumn])) {
                        case (int)Status.Active: val[statusColumn] = Status.Active.ToString(); break;
                        case (int)Status.Inactive: val[statusColumn] = Status.Inactive.ToString(); break;
                    }
                }

                if (approveStatusColumn >= 0) {
                    switch (NullUtils.cvInt(val[approveStatusColumn])) {
                        case (int)ApproveStatus.Pending: val[approveStatusColumn] = ApproveStatus.Pending.ToString(); break;
                        case (int)ApproveStatus.Approve: val[approveStatusColumn] = ApproveStatus.Approve.ToString(); break;
                        case (int)ApproveStatus.Reject: val[approveStatusColumn] = ApproveStatus.Reject.ToString(); break;
                    }
                }

                if (eventDate >= 0) {
                    val[eventDate] = ValueUtils.GetDate(val[eventDate], "dd/MM/yyyy HH:mm:ss").ToString("dd/MM/yyyy");
                }

                if (activeDate >= 0) {
                    val[activeDate] = ValueUtils.GetDate(val[activeDate], "dd/MM/yyyy HH:mm:ss").ToString("dd/MM/yyyy");
                }

                if (expireDate >= 0) {
                    val[expireDate] = ValueUtils.GetDate(val[expireDate], "dd/MM/yyyy HH:mm:ss").ToString("dd/MM/yyyy");
                }

                vals.Add(val);
            }
            return vals;
        }

        private TransactionLogViewModel GetTableColumns(string table) {
            TransactionLogViewModel model = new TransactionLogViewModel();
            switch (table) {
                case "Admin": return model = new TransactionLogViewModel() { Table = "(SELECT t.*, g.Title GroupTitle FROM Admin_Shadow t LEFT JOIN AdminGroup g ON t.AdminGroupId=g.Id) t", TableColumn = new string[] { "ShadowDate", "Action", "AdminId", "EmployeeId", "GroupTitle", "Name", "Status" }, Columns = { "Action Date", "Action", "By", "Employee Id", "Group", "Name", "Status" } };
                case "AdminGroup": return model = new TransactionLogViewModel() { Table = "AdminGroup_Shadow", TableColumn = new string[] { "ShadowDate", "Action", "AdminId", "Id", "Title" }, Columns = { "Action Date", "Action", "By", "Id", "Title" } };
                case "Bank": return model = new TransactionLogViewModel() { Table = "Bank_Shadow", TableColumn = new string[] { "ShadowDate", "Action", "AdminId", "Id", "Code", "PaymentGatewayCode", "TitleTH", "TitleEN", "Status" }, Columns = { "Action Date", "Action", "By", "Id", "Code", "Payment Gateway Code", "TitleTH", "TitleEN", "Status" } };
                case "Banner": return model = new TransactionLogViewModel() { Table = "Banner_Shadow", TableColumn = new string[] { "ShadowDate", "Action", "AdminId", "Id", "ImageTH", "ImageEN", "UrlTH", "UrlEN", "ActiveDate", "ExpireDate", "Status", "ApproveStatus", "ApproveAdminId", "OwnerAdminId" }, Columns = { "Action Date", "Action", "By", "Id", "ImageTH", "ImageEN", "UrlTH", "UrlEN", "ActiveDate", "ExpireDate", "Status", "Approve", "Approver", "Owner" } };
                case "Campaign": return model = new TransactionLogViewModel() { Table = "Campaign_Shadow", TableColumn = new string[] { "ShadowDate", "Action", "AdminId", "Id", "TitleTH", "TitleEN", "BodyTH", "BodyEN", "ImageTH", "ImageEN", "ImageAltTH", "ImageAltEN", "PromotionAll", "ActiveDate", "ExpireDate", "Status", "ApproveStatus", "ApproveAdminId", "OwnerAdminId" }, Columns = { "Action Date", "Action", "By", "Id", "TitleTH", "TitleEN", "BodyTH", "BodyEN", "ImageTH", "ImageEN", "ImageAltTH", "ImageAltEN", "PromotionAll", "ActiveDate", "ExpireDate", "Status", "Approve", "Approver", "Owner" } };
                case "CarBrand": return model = new TransactionLogViewModel() { Table = "CarBrand_Shadow", TableColumn = new string[] { "ShadowDate", "Action", "AdminId", "Id", "TitleTH", "TitleEN", "MapName", "Status" }, Columns = { "Action Date", "Action", "By", "Id", "TitleTH", "TitleEN", "Map Name", "Status" } };
                case "CarFamily": return model = new TransactionLogViewModel() { Table = "(SELECT t.*, b.TitleTH Brand FROM CarFamily_Shadow t LEFT JOIN CarBrand b ON t.BrandId=b.Id) t", TableColumn = new string[] { "ShadowDate", "Action", "AdminId", "Id", "TitleTH", "TitleEN", "MapName", "Status" }, Columns = { "Action Date", "Action", "By", "Id", "TitleTH", "TitleEN", "Map Name", "Status" } };
                case "CarModel": return model = new TransactionLogViewModel() { Table = "(SELECT t.*, f.TitleTH Family, b.TitleTH Brand FROM CarModel_Shadow t LEFT JOIN CarFamily f ON t.FamilyId=f.Id LEFT JOIN CarBrand b ON f.BrandId=b.Id) t", TableColumn = new string[] { "ShadowDate", "Action", "AdminId", "Id", "Brand", "Family", "TitleTH", "TitleEN", "MapName", "Spec", "CC", "KG", "Seat", "Door", "Year", "Status" }, Columns = { "Action Date", "Action", "By", "Id", "Brand", "Family", "TitleTH", "TitleEN", "Map Name", "Spec", "CC", "KG", "Seat", "Door", "Year", "Status" } };
                case "Company": return model = new TransactionLogViewModel() { Table = "Company_Shadow", TableColumn = new string[] { "ShadowDate", "Action", "AdminId", "Id", "Code", "PrefixTH", "PrefixEN", "TitleTH", "TitleEN", "CoCode", "CoPrefixTH", "CoPrefixEN", "CoTitleTH", "CoTitleEN", "ReCode", "RePrefixTH", "RePrefixEN", "ReTitleTH", "ReTitleEN", "AlCode", "AlPrefixTH", "AlPrefixEN", "AlTitleTH", "AlTitleEN", "ImageTH", "ImageEN", "Status" }, Columns = { "Action Date", "Action", "By", "Id", "Code", "PrefixTH", "PrefixEN", "TitleTH", "TitleEN", "CoCode", "CoPrefixTH", "CoPrefixEN", "CoTitleTH", "CoTitleEN", "ReCode", "RePrefixTH", "RePrefixEN", "ReTitleTH", "ReTitleEN", "AlCode", "AlPrefixTH", "AlPrefixEN", "AlTitleTH", "AlTitleEN", "ImageTH", "ImageEN", "Status" } };
                case "Config": return model = new TransactionLogViewModel() { Table = "Config_Shadow", TableColumn = new string[] { "ShadowDate", "Action", "AdminId", "Id", "Title", "Body" }, Columns = { "Action Date", "Action", "By", "Id", "Title", "Body" } };
                case "ConfigPage": return model = new TransactionLogViewModel() { Table = "ConfigPage_Shadow", TableColumn = new string[] { "ShadowDate", "Action", "AdminId", "Id", "TitleTH", "TitleEN", "BodyTH", "BodyEN", "MetaTitleTH", "MetaTitleEN", "MetaKeywordTH", "MetaKeywordEN", "MetaDescriptionTH", "MetaDescriptionEN", "OGTitleTH", "OGTitleEN", "OGImageTH", "OGImageEN", "OGDescriptionTH", "OGDescriptionEN" }, Columns = { "Action Date", "Action", "By", "Id", "TitleTH", "TitleEN", "BodyTH", "BodyEN", "MetaTitleTH", "MetaTitleEN", "MetaKeywordTH", "MetaKeywordEN", "MetaDescriptionTH", "MetaDescriptionEN", "OGTitleTH", "OGTitleEN", "OGImageTH", "OGImageEN", "OGDescriptionTH", "OGDescriptionEN" } };
                case "ContactCategory": return model = new TransactionLogViewModel() { Table = "ContactCategory_Shadow", TableColumn = new string[] { "ShadowDate", "Action", "AdminId", "Id", "TitleTH", "TitleEN", "Sequence", "Status" }, Columns = { "Action Date", "Action", "By", "Id", "TitleTH", "TitleEN", "Sequence", "Status" } };
                case "ContactSubCategory": return model = new TransactionLogViewModel() { Table = "(SELECT t.*, c.TitleTH Category FROM ContactSubCategory_Shadow t LEFT JOIN ContactCategory c ON t.CategoryId=c.Id) t", TableColumn = new string[] { "ShadowDate", "Action", "AdminId", "Id", "Category", "TitleTH", "TitleEN", "MailTo", "Sequence", "Status" }, Columns = { "Action Date", "Action", "By", "Id", "Category", "TitleTH", "TitleEN", "MailTo", "Sequence", "Status" } };
                case "Country": return model = new TransactionLogViewModel() { Table = "(SELECT ShadowDate, Action, AdminId, Id, Code, " + sqlCaseEnum("Zone", EnumExtensions.EnumDictionary<CountryZone>()) + " Zone, TitleTH, TitleEN, Status FROM Country_Shadow) t", TableColumn = new string[] { "ShadowDate", "Action", "AdminId", "Id", "Code", "Zone", "TitleTH", "TitleEN", "Status" }, Columns = { "Action Date", "Action", "By", "Id", "Code", "Zone", "TitleTH", "TitleEN", "Status" } };
                case "Coupon": return model = new TransactionLogViewModel() { Table = "(SELECT ShadowDate, Action, AdminId, Id, " + sqlCaseEnum("CouponType", EnumExtensions.EnumDictionary<CouponType>()) + " Type, TitleTH, TitleEN, Code, Total, " + sqlCaseEnum("DiscountType", EnumExtensions.EnumDictionary<CouponDiscountType>()) + " DiscountType, DiscountAmount, " + sqlCaseEnum("ProductType", EnumExtensions.EnumDictionary<CouponProductType>()) + " ProductType, ActiveDate, ExpireDate, Status FROM Coupon_Shadow) t", TableColumn = new string[] { "ShadowDate", "Action", "AdminId", "Id", "Code", "Type", "TitleTH", "TitleEN", "Total", "DiscountType", "DiscountAmount", "ProductType", "ActiveDate", "ExpireDate", "Status" }, Columns = { "Action Date", "Action", "By", "Id", "Code", "Type", "TitleTH", "TitleEN", "Total", "Discount Type", "Discount Amount", "Product Type", "Active Date", "Expire Date", "Status" } };
                case "DayOff": return model = new TransactionLogViewModel() { Table = "DayOff_Shadow", TableColumn = new string[] { "ShadowDate", "Action", "AdminId", "Id", "EventDate", "TitleTH", "TitleEN", "Status" }, Columns = { "Action Date", "Action", "By", "Id", "EventDate", "TitleTH", "TitleEN", "Status" } };
                case "District": return model = new TransactionLogViewModel() { Table = "(SELECT t.*, p.TitleTH Province FROM District_Shadow t LEFT JOIN Province p ON t.ProvinceId=p.Id) t", TableColumn = new string[] { "ShadowDate", "Action", "AdminId", "Id", "Province", "TitleTH", "TitleEN", "Code", "Status" }, Columns = { "Action Date", "Action", "By", "Id", "Province", "TitleTH", "TitleEN", "Code", "Status" } };
                case "Faq": return model = new TransactionLogViewModel() { Table = "(SELECT t.*, m.TitleTH Category FROM Faq_Shadow t LEFT JOIN MasterOption m ON t.CategoryId=m.Id) t", TableColumn = new string[] { "ShadowDate", "Action", "AdminId", "Id", "Category", "QuestionTH", "QuestionEN", "AnswerTH", "AnswerEN", "Pin", "Sequence", "Status" }, Columns = { "Action Date", "Action", "By", "Id", "Category", "QuestionTH", "QuestionEN", "AnswerTH", "AnswerEN", "Pin", "Sequence", "Status" } };
                case "Lead": return model = new TransactionLogViewModel() { Table = "(SELECT ShadowDate, Action, AdminId, t.Id, " + sqlCaseEnum("Source", EnumExtensions.EnumDictionary<LeadSource>()) + " Source, ProductId, PremiumId, Name, Surname, Tel, Email, CallBackDate, c.TitleTH Subject, Message, Remark, " + sqlCaseEnum("LeadStatus", EnumExtensions.EnumDictionary<LeadStatus>()) + " LeadStatus, OwnerAdminId, AssignAdminId FROM Lead_Shadow t LEFT JOIN ContactSubCategory c ON t.SubjectId=c.Id) t", TableColumn = new string[] { "ShadowDate", "Action", "AdminId", "Id", "Source", "ProductId", "PremiumId", "Name", "Surname", "Tel", "Email", "CallBackDate", "Subject", "Message", "Remark", "LeadStatus", "OwnerAdminId", "AssignAdminId" }, Columns = { "Action Date", "Action", "By", "Id", "Source", "ProductId", "PremiumId", "Name", "Surname", "Tel", "Email", "CallBackDate", "Subject", "Message", "Remark", "LeadStatus", "Owner", "Assigner" } };
                case "MasterOption": return model = new TransactionLogViewModel() { Table = "(SELECT ShadowDate, Action, AdminId, Id, " + sqlCaseEnum("Category", EnumExtensions.EnumDictionary<MasterOptionCategory>()) + " Category, TitleTH, TitleEN, Code, Sequence, Status FROM MasterOption_Shadow) t", TableColumn = new string[] { "ShadowDate", "Action", "AdminId", "Id", "Category", "TitleTH", "TitleEN", "Code", "Min", "Max", "Sequence", "Status" }, Columns = { "Action Date", "Action", "By", "Id", "Category", "TitleTH", "TitleEN", "Code", "Min", "Max", "Sequence", "Status" } };
                case "News": return model = new TransactionLogViewModel() { Table = "(SELECT t.*, s.TitleTH SubCategory FROM News_Shadow t LEFT JOIN NewsSubCategory s ON t.NewsSubCategoryId=s.Id) t", TableColumn = new string[] { "ShadowDate", "Action", "AdminId", "Id", "SubCategory", "TitleTH", "TitleEN", "UrlPrefix", "UrlTH", "UrlEN", "BriefTH", "BriefEN", "BodyTH", "BodyEN", "ImageTH", "ImageEN", "ImageTitleTH", "ImageTitleEN", "ImageAltTH", "ImageAltEN", "MetaTitleTH", "MetaTitleEN", "MetaKeywordTH", "MetaKeywordEN", "MetaDescriptionTH", "MetaDescriptionEN", "OGTitleTH", "OGTitleEN", "OGImageTH", "OGImageEN", "OGDescriptionTH", "OGDescriptionEN", "Status", "ApproveStatus", "ApproveAdminId", "OwnerAdminId" }, Columns = { "Action Date", "Action", "By", "Id", "SubCategory", "TitleTH", "TitleEN", "UrlPrefix", "UrlTH", "UrlEN", "BriefTH", "BriefEN", "BodyTH", "BodyEN", "ImageTH", "ImageEN", "ImageTitleTH", "ImageTitleEN", "ImageAltTH", "ImageAltEN", "MetaTitleTH", "MetaTitleEN", "MetaKeywordTH", "MetaKeywordEN", "MetaDescriptionTH", "MetaDescriptionEN", "OGTitleTH", "OGTitleEN", "OGImageTH", "OGImageEN", "OGDescriptionTH", "OGDescriptionEN", "Status", "Approve", "Approver", "Owner" } };
                case "NewsSubCategory": return model = new TransactionLogViewModel() { Table = "NewsSubCategory_Shadow", TableColumn = new string[] { "ShadowDate", "Action", "AdminId", "Id", "NewsCategory", "TitleTH", "TitleEN", "Status" }, Columns = { "Action Date", "Action", "By", "Id", "NewsCategory", "TitleTH", "TitleEN", "Status" } };
                case "PartnerBanner": return model = new TransactionLogViewModel() { Table = "PartnerBanner_Shadow", TableColumn = new string[] { "ShadowDate", "Action", "AdminId", "Id", "ImageTH", "ImageEN", "Status" }, Columns = { "Action Date", "Action", "By", "Id", "ImageTH", "ImageEN", "Status" } };
                case "PaymentMethod": return model = new TransactionLogViewModel() { Table = "PaymentMethod_Shadow", TableColumn = new string[] { "ShadowDate", "Action", "AdminId", "Id", "TitleTH", "TitleEN", "UrlTH", "UrlEN", "BriefTH", "BriefEN", "BodyTH", "BodyEN", "ImageTH", "ImageEN", "ImageTitleTH", "ImageTitleEN", "ImageAltTH", "ImageAltEN", "MetaTitleTH", "MetaTitleEN", "MetaKeywordTH", "MetaKeywordEN", "MetaDescriptionTH", "MetaDescriptionEN", "OGTitleTH", "OGTitleEN", "OGImageTH", "OGImageEN", "OGDescriptionTH", "OGDescriptionEN", "Status", "ApproveStatus", "ApproveAdminId", "OwnerAdminId" }, Columns = { "Action Date", "Action", "By", "Id", "TitleTH", "TitleEN", "UrlTH", "UrlEN", "BriefTH", "BriefEN", "BodyTH", "BodyEN", "ImageTH", "ImageEN", "ImageTitleTH", "ImageTitleEN", "ImageAltTH", "ImageAltEN", "MetaTitleTH", "MetaTitleEN", "MetaKeywordTH", "MetaKeywordEN", "MetaDescriptionTH", "MetaDescriptionEN", "OGTitleTH", "OGTitleEN", "OGImageTH", "OGImageEN", "OGDescriptionTH", "OGDescriptionEN", "Status", "Approve", "Approver", "Owner" } };
                case "Poi": return model = new TransactionLogViewModel() { Table = "(SELECT ShadowDate, Action, AdminId, Id, " + sqlCaseEnum("Category", EnumExtensions.EnumDictionary<PoiCategory>()) + " Category, TitleTH, TitleEN, Latitude, Longitude, Status FROM Poi_Shadow) t", TableColumn = new string[] { "ShadowDate", "Action", "AdminId", "Id", "Category", "TitleTH", "TitleEN", "Latitude", "Longitude", "Status" }, Columns = { "Action Date", "Action", "By", "Id", "Category", "TitleTH", "TitleEN", "Latitude", "Longitude", "Status" } };
                case "Popup": return model = new TransactionLogViewModel() { Table = "Popup_Shadow", TableColumn = new string[] { "ShadowDate", "Action", "AdminId", "Id", "ImageTH", "ImageEN", "UrlTH", "UrlEN", "ActiveDate", "ExpireDate", "Status", "ApproveStatus", "ApproveAdminId", "OwnerAdminId" }, Columns = { "Action Date", "Action", "By", "Id", "ImageTH", "ImageEN", "UrlTH", "UrlEN", "ActiveDate", "ExpireDate", "Status", "Approve", "Approver", "Owner" } };
                case "Privilege": return model = new TransactionLogViewModel() { Table = "Privilege_Shadow", TableColumn = new string[] { "ShadowDate", "Action", "AdminId", "Id", "TitleTH", "TitleEN", "UrlTH", "UrlEN", "BriefTH", "BriefEN", "BodyTH", "BodyEN", "ImageTH", "ImageEN", "ImageTitleTH", "ImageTitleEN", "ImageAltTH", "ImageAltEN", "MetaTitleTH", "MetaTitleEN", "MetaKeywordTH", "MetaKeywordEN", "MetaDescriptionTH", "MetaDescriptionEN", "OGTitleTH", "OGTitleEN", "OGImageTH", "OGImageEN", "OGDescriptionTH", "OGDescriptionEN", "Status", "ApproveStatus", "ApproveAdminId", "OwnerAdminId" }, Columns = { "Action Date", "Action", "By", "Id", "TitleTH", "TitleEN", "UrlTH", "UrlEN", "BriefTH", "BriefEN", "BodyTH", "BodyEN", "ImageTH", "ImageEN", "ImageTitleTH", "ImageTitleEN", "ImageAltTH", "ImageAltEN", "MetaTitleTH", "MetaTitleEN", "MetaKeywordTH", "MetaKeywordEN", "MetaDescriptionTH", "MetaDescriptionEN", "OGTitleTH", "OGTitleEN", "OGImageTH", "OGImageEN", "OGDescriptionTH", "OGDescriptionEN", "Status", "Approve", "Approver", "Owner" } };
                case "Product": return model = new TransactionLogViewModel() { Table = "(SELECT t.*, s.TitleTH SubCategory, c.TitleTH Company FROM Product_Shadow t LEFT JOIN ProductSubCategory s ON t.SubCategoryId=s.Id LEFT JOIN Company c ON t.CompanyId=c.Id) t", TableColumn = new string[] { "ShadowDate", "Action", "AdminId", "Id", "SubCategory", "Company", "Class", "SubClass", "ProductCode", "InsuranceProductCode", "TitleTH", "TitleEN", "ActiveDate", "ExpireDate", "CoverageTH", "CoverageEN", "ConditionTH", "ConditionEN", "ExceptionTH", "ExceptionEN", "PrivilegeTH", "PrivilegeEN", "Status", "ApproveStatus", "ApproveAdminId", "OwnerAdminId" }, Columns = { "Action Date", "Action", "By", "Id", "SubCategory", "Company", "Class", "SubClass", "ProductCode", "InsuranceProductCode", "TitleTH", "TitleEN", "ActiveDate", "ExpireDate", "CoverageTH", "CoverageEN", "ConditionTH", "ConditionEN", "ExceptionTH", "ExceptionEN", "PrivilegeTH", "PrivilegeEN", "Status", "Approve", "Approver", "Owner" } };
                case "ProductApplication": return model = new TransactionLogViewModel() { Table = "(SELECT ShadowDate, Action, AdminId, Id, " + sqlCaseEnum("Id", EnumExtensions.EnumDictionary<ProductApplicationFieldKey>()) + " Field, TitleTH, TitleEN, Step, Sequence FROM ProductApplicationField_Shadow) t", TableColumn = new string[] { "ShadowDate", "Action", "AdminId", "Id", "Field", "TitleTH", "TitleEN", "Step", "Sequence" }, Columns = { "Action Date", "Action", "By", "Id", "Title", "TitleTH", "TitleEN", "Step", "Sequence" } };
                case "ProductCategory": return model = new TransactionLogViewModel() { Table = "ProductCategory_Shadow", TableColumn = new string[] { "ShadowDate", "Action", "AdminId", "Id", "TitleTH", "TitleEN", "HomeText1TH", "HomeText1EN", "HomeText2TH", "HomeText2EN", "DisplayHome" }, Columns = { "Action Date", "Action", "By", "Id", "TitleTH", "TitleEN", "HomeText1TH", "HomeText1EN", "HomeText2TH", "HomeText2EN", "DisplayHome" } };
                case "ProductClass": return model = new TransactionLogViewModel() { Table = "(SELECT ShadowDate, Action, AdminId, Id, " + sqlCaseEnum("CategoryId", EnumExtensions.EnumDictionary<ProductCategoryKey>()) + " Category, TitleTH, TitleEN, Class, SubClass, SubClassConfGroup, Status FROM ProductSubClass_Shadow) t", TableColumn = new string[] { "ShadowDate", "Action", "AdminId", "Id", "TitleTH", "TitleEN", "Category", "Class", "SubClass", "SubClassConfGroup", "Status" }, Columns = { "Action Date", "Action", "By", "Id", "TitleTH", "TitleEN", "Category", "Class", "Sub Class", "Sub Class Conf Group", "Status" } };
                case "Promotion": return model = new TransactionLogViewModel() { Table = "Promotion_Shadow", TableColumn = new string[] { "ShadowDate", "Action", "AdminId", "Id", "TitleTH", "TitleEN", "BodyTH", "BodyEN", "PromotionType", "ProductType", "ActiveDate", "ExpireDate", "ApproveStatus", "Status", "ApproveAdminId", "OwnerAdminId" }, Columns = { "Action Date", "Action", "By", "Id", "TitleTH", "TitleEN", "BodyTH", "BodyEN", "PromotionType", "ProductType", "ActiveDate", "ExpireDate", "Approve", "Status", "Approver", "Owner" } };
                case "Province": return model = new TransactionLogViewModel() { Table = "Province_Shadow", TableColumn = new string[] { "ShadowDate", "Action", "AdminId", "Id", "TitleTH", "TitleEN", "Code", "Status" }, Columns = { "Action Date", "Action", "By", "Id", "TitleTH", "TitleEN", "Code", "Status" } };
                case "Subscribe": return model = new TransactionLogViewModel() { Table = "Subscribe_Shadow", TableColumn = new string[] { "ShadowDate", "Action", "AdminId", "Id", "Email" }, Columns = { "Action Date", "Action", "By", "Id", "Email" } };
                case "SubDistrict": return model = new TransactionLogViewModel() { Table = "(SELECT t.*, d.TitleTH District, p.TitleTH Province FROM SubDistrict_Shadow t LEFT JOIN District d ON t.DistrictId=d.Id LEFT JOIN Province p ON d.ProvinceId=p.Id) t", TableColumn = new string[] { "ShadowDate", "Action", "AdminId", "Id", "Province", "District", "TitleTH", "TitleEN", "Code", "ZipCode", "Status" }, Columns = { "Action Date", "Action", "By", "Id", "Province", "District", "TitleTH", "TitleEN", "Code", "Zip Code", "Status" } };
                case "SystemMessage": return model = new TransactionLogViewModel() { Table = "(SELECT ShadowDate, Action, AdminId, Id, " + sqlCaseEnum("Category", EnumExtensions.EnumDictionary<SystemMessageCategory>()) + " Category, Code, SubCode, BodyTH, BodyEN FROM SystemMessage_Shadow) t", TableColumn = new string[] { "ShadowDate", "Action", "AdminId", "Id", "Category", "Code", "SubCode", "BodyTH", "BodyEN" }, Columns = { "Action Date", "Action", "By", "Id", "Category", "Code", "Sub Code", "BodyTH", "BodyEN" } };
                case "Tag": return model = new TransactionLogViewModel() { Table = "Tag_Shadow", TableColumn = new string[] { "ShadowDate", "Action", "AdminId", "Id", "Title" }, Columns = { "Action Date", "Action", "By", "Id", "Title" } };
            }
            return model;
        }

        private string sqlCaseEnum(string column, Dictionary<int, string> dictionary) {
            StringBuilder sb = new StringBuilder("(CASE ");
            foreach (var item in dictionary) {
                sb.Append(" WHEN " + column + "=" + item.Key + " THEN '" + item.Value + "' ");
            }
            return sb.ToString() + " ELSE '' END )";
        }
    }
}
