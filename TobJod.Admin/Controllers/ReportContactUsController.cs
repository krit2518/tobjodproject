﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TobJod.Admin.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class ReportContactUsController : BaseController {
        // GET: ReportContactUs
        public ActionResult Index(ReportContactUsViewModel model) {
            ReportRepository repo = new ReportRepository();
            model.DateFrom = ValueUtils.GetDate(Request.Params["DateFrom"]);
            model.DateTo = ValueUtils.GetDate(Request.Params["DateTo"]);
            if (model.DateTo == DateUtils.SqlMinDate()) model.DateTo = DateTime.Today.AddDays(-1);
            if (model.DateFrom == DateUtils.SqlMinDate()) model.DateFrom = DateUtils.GetFirstDayOfMonth(model.DateTo);
            model.Category = NullUtils.cvInt(Request.Params["Category"]);

            ContactCategoryRepository catRepo = new ContactCategoryRepository();
            model.Categories = catRepo.ListContactCategory_SelectList_Admin();
            model.Categories.Insert(0, new SelectListItem() { Text = "All", Value = "0" });

            //31 DAYS
            var x = model.DateTo.Subtract(model.DateFrom);
            if (x.Days <= 31) {
                if (Request.QueryString.Count > 0) {
                    var dt = repo.ListContactUs(model.DateFrom, model.DateTo, model.Category);
                    model.DataTable = dt;

                    if (string.Equals(Request.Params["export"], "1")) {
                        Export(dt);
                    }
                }
            } else {
                Notify(TobJod.Models.NotifyMessageType.Warning, "Date must be <= 31 days.");
            }

            return View(model);
        }

        private void Export(DataTable dt) {
            string sheetName = "ContactUs";
            string fileName = "ContactUs.xlsx";
            int prevCategory = 0;
            int categoryRow = 0;
            int categoryCount = 0, totalCount = 0;
            Color bgColor = Color.FromArgb(255, 251, 213, 182);
            Color bgTotalColor = Color.FromArgb(255, 255, 243, 233);

            using (ExcelPackage pck = new ExcelPackage()) {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add(sheetName);

                //ws.Cells["A1"].Value = "";
                ws.Cells["B1"].Value = "จำนวนลูกค้าที่กรอกข้อมูลใน Contact Form  ";
                ws.Cells["A2"].Value = "ทุกหัวข้อ";
                ws.Cells["A2:B2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells["A2:B2"].Style.Fill.BackgroundColor.SetColor(bgColor);
                ws.Cells["A2:B2"].Style.Font.Bold = true;

                if (dt.Rows.Count > 0) {
                    int i = 3;
                    foreach (DataRow row in dt.Rows) {
                        int category = NullUtils.cvInt(row["CategoryId"]);

                        if (prevCategory != category) {
                            if (categoryRow > 0) ws.Cells["B" + categoryRow.ToString()].Value = categoryCount;
                            categoryRow = i; i++;
                            ws.Cells["A" + categoryRow.ToString()].Value = row["Category"];
                            ws.Cells["A" + categoryRow.ToString()].Style.Font.Bold = true;
                            ws.Cells["A" + categoryRow.ToString() + ":B" + categoryRow.ToString()].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            ws.Cells["A" + categoryRow.ToString() + ":B" + categoryRow.ToString()].Style.Fill.BackgroundColor.SetColor(bgTotalColor);
                            categoryCount = 0;
                        }

                        int count = NullUtils.cvInt(row["total"]);
                        categoryCount += count;
                        totalCount += count;

                        ws.Cells["A" + i.ToString()].Value = "    - " + row["Subject"];
                        ws.Cells["B" + i.ToString()].Value = row["Total"];

                        prevCategory = category;
                        i++;
                    }
                    ws.Cells["B" + categoryRow.ToString()].Value = categoryCount;
                    ws.Cells["B2"].Value = totalCount;

                    //Format the header
                    using (ExcelRange rng = ws.Cells["A1:" + GetExcelColumnName(2) + "1"]) {
                        rng.Style.Font.Bold = true;
                        rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                        rng.Style.Fill.BackgroundColor.SetColor(bgColor);  //Set color to dark blue
                        rng.Style.Font.Color.SetColor(Color.Black);
                    }

                    //Example how to Format Column 1 as numeric 
                    int c = dt.Columns[2].Ordinal + 1;
                    using (ExcelRange col = ws.Cells[2, c, 2 + dt.Rows.Count, c]) {
                        col.Style.Numberformat.Format = "#,##0";
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    }


                    //Format the header for column 1-3
                    using (ExcelRange rng = ws.Cells["A1:" + GetExcelColumnName(2) + (i - 1)]) {
                        rng.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        rng.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        rng.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        rng.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    }

                    ws.Cells.AutoFitColumns();
                }

                //Write it back to the client
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;  filename=" + fileName);
                Response.BinaryWrite(pck.GetAsByteArray());
                Response.End();
            }
        }
    }
}