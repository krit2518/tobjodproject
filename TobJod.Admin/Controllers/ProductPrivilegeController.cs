﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class ProductPrivilegeController : BaseController {
        ProductPrivilegeRepository repo;

        public ProductPrivilegeController() {
            repo = new ProductPrivilegeRepository();
        }
        
        // GET: ProductPrivilege
        public ActionResult Index() {
            //ProductPrivilege item = new ProductPrivilege();
            //item.Category = NullUtils.cvInt(Request.QueryString["category"]);
            return View();
        }

        // GET: ProductPrivilege/Create
        public ActionResult Create() {
            ProductPrivilege item = new ProductPrivilege();
            item.Status = Status.Active;
            return View("Form", item);
        }

        // POST: ProductPrivilege/Create
        [HttpPost]
        public ActionResult Create(ProductPrivilege item) {
            try {
                item.Mode = 0;
                item.CreateDate = item.UpdateDate = DateTime.Now;
                item.CreateAdminId = item.UpdateAdminId = Admin.Id;

                repo.AddProductPrivilege(item);
                Notify_Add_Success();
                return RedirectToAction("Index");
            } catch { }
            Notify_Add_Fail();
            return View("Form", item);
        }

        // GET: ProductPrivilege/Edit/5
        public ActionResult Edit(int id) {
            ProductPrivilege item = repo.GetProductPrivilege_Admin(id);
            return View("Form", item);
        }

        // POST: ProductPrivilege/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, ProductPrivilege item) {
            int result = 0;
            try {
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = Admin.Id;

                result = repo.UpdateProductPrivilege(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            return View("Form", item);
        }

        // GET: ProductPrivilege/Delete/5
        public ActionResult Delete(int id) {
            var result = repo.DeleteProductPrivilege(id, Admin.Id);
            if (result > 0) {
                Notify_Delete_Success();
            } else {
                Notify_Delete_Fail();
            }
            return RedirectToAction("Index");
        }

        // GET: ProductPrivilege/DataTable
        public ActionResult DataTable() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();

            //if (NullUtils.cvInt(Request.QueryString["category"]) > 0) {
            whereC.Add(" (categoryId=@category) ");
            whereParams.category = NullUtils.cvInt(Request.QueryString["category"]);
            //}

            if (param.hasSearch) {
                whereC.Add(" (TitleTH LIKE @searchValue OR TitleEN LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "Id", "Sequence", "CategoryId", "TitleTH", "UpdateDate", "Status" };
            DataTableData<ProductPrivilege> data = repo.ListProductPrivilege_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Id.ToString(),
                        x.Sequence.ToString(),
                        x.CategoryId.GetDisplayName(),
                        x.TitleTH.ToString(),
                        x.UpdateDate.ToString("dd/MM/yyyy HH:mm"),
                        x.Status.ToString(),
                        (HtmlActionButton(AdminAction.Edit, x.Id) + HtmlActionButton(AdminAction.Delete, x.Id))
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}
