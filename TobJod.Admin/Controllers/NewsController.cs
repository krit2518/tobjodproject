﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TobJod.Admin.Models;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class NewsController : BaseController {
        NewsRepository repo;

        public NewsController() {
            Path = System.Configuration.ConfigurationManager.AppSettings["Path_News"];
            repo = new NewsRepository();
        }
        
        // GET: News
        public ActionResult Index() {
            DateTime dateFrom = ValueUtils.GetDate(Request.QueryString["DateFrom"]);
            DateTime dateTo = ValueUtils.GetDate(Request.QueryString["DateTo"]);
            if (dateFrom > DateUtils.SqlMinDate()) ViewBag.DateFrom = dateFrom.ToString("dd/MM/yyyy", cultureGB);
            if (dateTo > DateUtils.SqlMinDate()) ViewBag.DateTo = dateTo.ToString("dd/MM/yyyy", cultureGB);

            return View();
        }

        // GET: News/Create
        public ActionResult Create() {
            NewsViewModel item = new NewsViewModel();
            item.UrlPrefix = DateTime.Now.Year.ToString();
            item.Status = Status.Active;
            item.OGImageTHSame = item.OGImageENSame = true;
            BindForm();
            return View("Form", item);
        }

        // POST: News/Create
        [HttpPost]
        public ActionResult Create(NewsViewModel item) {
            if (UploadUtils.IsImage(item.ImageTHFile, item.ImageENFile, item.OGImageTHFile, item.OGImageENFile)) {
                try {
                    item.BriefTH = ReplaceTextEditorPath(item.BriefTH);
                    item.BriefEN = ReplaceTextEditorPath(item.BriefEN);
                    item.BodyTH = ReplaceTextEditorPath(item.BodyTH);
                    item.BodyEN = ReplaceTextEditorPath(item.BodyEN);
                    item.CreateDate = item.UpdateDate = DateTime.Now;
                    item.CreateAdminId = item.UpdateAdminId = item.OwnerAdminId = Admin.Id;
                    item.ApproveAdminId = "";
                    item.ApproveStatus = ApproveStatus.Pending;
                    item.ImageTH = UploadUtils.SaveFile(item.ImageTHFile, PathPhy);
                    item.ImageEN = UploadUtils.SaveFile(item.ImageENFile, PathPhy);
                    item.OGImageTH = item.OGImageTHSame ? "" : UploadUtils.SaveFile(item.OGImageTHFile, PathPhy);
                    item.OGImageEN = item.OGImageENSame ? "" : UploadUtils.SaveFile(item.OGImageENFile, PathPhy);

                    repo.AddNews(item);
                    Notify_Add_Success();
                    return RedirectToAction("Index");
                } catch { }
            }
            Notify_Add_Fail();
            BindForm();
            return View("Form", item);
        }

        // GET: News/Edit/5
        public ActionResult Edit(int id) {
            News model = repo.GetNews_Admin(id);
            NewsViewModel item = NewsViewModel.Map(model);
            item.OGImageTHSame = (string.IsNullOrEmpty(item.OGImageTH));
            item.OGImageENSame = (string.IsNullOrEmpty(item.OGImageEN));
            item.BriefTH = ReplaceTextEditorPathDisplay(item.BriefTH);
            item.BriefEN = ReplaceTextEditorPathDisplay(item.BriefEN);
            item.BodyTH = ReplaceTextEditorPathDisplay(item.BodyTH);
            item.BodyEN = ReplaceTextEditorPathDisplay(item.BodyEN);
            BindForm();
            return View("Form", item);
        }

        // POST: News/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, NewsViewModel item) {
            int result = 0;
            if (UploadUtils.IsImage(item.ImageTHFile, item.ImageENFile, item.OGImageTHFile, item.OGImageENFile)) {
                try {
                    item.BriefTH = ReplaceTextEditorPath(item.BriefTH);
                    item.BriefEN = ReplaceTextEditorPath(item.BriefEN);
                    item.BodyTH = ReplaceTextEditorPath(item.BodyTH);
                    item.BodyEN = ReplaceTextEditorPath(item.BodyEN);
                    item.UpdateDate = DateTime.Now;
                    item.UpdateAdminId = Admin.Id; // = item.OwnerAdminId
                    item.ImageTH = UploadUtils.SaveAndReplaceFile(item.ImageTHFile, PathPhy, item.ImageTH);
                    item.ImageEN = UploadUtils.SaveAndReplaceFile(item.ImageENFile, PathPhy, item.ImageEN);
                    item.OGImageTH = item.OGImageTHSame ? "" : UploadUtils.SaveAndReplaceFile(item.OGImageTHFile, PathPhy, item.OGImageTH);
                    item.OGImageEN = item.OGImageENSame ? "" : UploadUtils.SaveAndReplaceFile(item.OGImageENFile, PathPhy, item.OGImageEN);

                    result = repo.UpdateNews(item);
                    if (result > 0) {
                        Notify_Update_Success();
                        return RedirectToAction("Index");
                    }
                } catch { }
            }
            Notify_Update_Fail();
            BindForm();
            return View("Form", item);
        }

        // GET: News/View/5
        public ActionResult View(int id) {
            News x = repo.GetNews_Admin(id);
            NewsViewModel item = NewsViewModel.Map(x);
            item.BriefTH = ReplaceTextEditorPathDisplay(item.BriefTH);
            item.BriefEN = ReplaceTextEditorPathDisplay(item.BriefEN);
            item.BodyTH = ReplaceTextEditorPathDisplay(item.BodyTH);
            item.BodyEN = ReplaceTextEditorPathDisplay(item.BodyEN);
            BindForm();
            return View("View", item);
        }

        // GET: News/Approve/5
        public ActionResult Approve(int id) {
            News x = repo.GetNews_Admin(id);
            NewsViewModel item = NewsViewModel.Map(x);
            item.BriefTH = ReplaceTextEditorPathDisplay(item.BriefTH);
            item.BriefEN = ReplaceTextEditorPathDisplay(item.BriefEN);
            item.BodyTH = ReplaceTextEditorPathDisplay(item.BodyTH);
            item.BodyEN = ReplaceTextEditorPathDisplay(item.BodyEN);
            BindForm();
            return View("View", item);
        }

        // POST: News/Approve/5
        [HttpPost]
        public ActionResult Approve(int id, NewsViewModel item) {
            int result = 0;
            try {
                item.ApproveStatus = (ApproveStatus)Enum.Parse(typeof(ApproveStatus), Request.Form["Approve"].ToString());
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = item.ApproveAdminId = Admin.Id; // = item.OwnerAdminId

                result = repo.UpdateNewsApproveStatus(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            BindForm();
            return View("View", item);
        }

        // GET: News/Assign/5
        public ActionResult Assign(int id) {
            News model = repo.GetNews_Admin(id);
            AdminRepository adminRepo = new AdminRepository();
            ViewBag.OwnerAdminList = adminRepo.ListAdminByPermission(Menu.Module, AdminAction.Edit).Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() }).ToList();
            NewsViewModel item = NewsViewModel.Map(model);
            item.BriefTH = ReplaceTextEditorPathDisplay(item.BriefTH);
            item.BriefEN = ReplaceTextEditorPathDisplay(item.BriefEN);
            item.BodyTH = ReplaceTextEditorPathDisplay(item.BodyTH);
            item.BodyEN = ReplaceTextEditorPathDisplay(item.BodyEN);
            BindForm();
            return View("View", item);
        }

        // POST: News/Assign/5
        [HttpPost]
        public ActionResult Assign(int id, NewsViewModel item) {
            int result = 0;
            try {
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = Admin.Id;

                result = repo.UpdateNewsOwner(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            BindForm();
            return View("View", item);
        }

        // GET: News/Gallery/5
        public ActionResult Gallery(int id) {
            CheckPermission(AdminAction.Edit);
            News news = repo.GetNews_Admin(id);
            NewsImageViewModel item = new NewsImageViewModel();
            item.News = news;
            return View("Gallery", item);
        }

        // POST: News/Gallery/5
        [HttpPost]
        public ActionResult Gallery(int id, NewsImageViewModel item) {
            CheckPermission(AdminAction.Edit);
            int result = 0;
            try {
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = Admin.Id; 
                item.Image = UploadUtils.SaveAndReplaceFile(item.ImageFile, PathPhy, item.Image);
                item.NewsId = item.News.Id;

                if (item.Id > 0) {
                    result = repo.UpdateNewsImage(item);
                } else {
                    item.CreateDate = item.UpdateDate;
                    item.CreateAdminId = item.UpdateAdminId;
                    result = repo.AddNewsImage(item);
                }
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Gallery", new { id = item.NewsId });
                }
            } catch { }
            News news = repo.GetNews_Admin(item.News.Id);
            item.News = news;
            Notify_Update_Fail();
            return View("Gallery", item);
        }

        // GET: News/GalleryDelete/5
        public ActionResult GalleryDelete(int id) {
            var item = repo.GetNewsImage_Admin(id);
            var result = repo.DeleteNewsImage(id, Admin.Id);
            if (result > 0) {
                FileUtils.DeleteFiles(PathPhy, item.Image);
                Notify_Delete_Success();
            } else {
                Notify_Delete_Fail();
            }
            return RedirectToAction("Gallery", new { id = item.NewsId });
        }

        // GET: News/Disable/5
        public ActionResult Disable(int id) {
            News item = new News();
            item.Id = id;
            item.Status = Status.Inactive;
            item.UpdateDate = DateTime.Now;
            item.UpdateAdminId = Admin.Id;
            var result = repo.UpdateNewsStatus(item);
            if (result > 0) {
                Notify_Update_Success();
            } else {
                Notify_Update_Fail();
            }
            return RedirectToAction("Index");
        }

        // GET: News/Delete/5
        public ActionResult Delete(int id) {
            var item = repo.GetNews_Admin(id);
            var result = repo.DeleteNews(id, Admin.Id);
            if (result > 0) {
                FileUtils.DeleteFiles(PathPhy, item.ImageEN, item.ImageTH, item.OGImageTH, item.OGImageEN);
                foreach (var image in item.Gallery) {
                    FileUtils.DeleteFiles(PathPhy, image.Image);
                }
                Notify_Delete_Success();
            } else {
                Notify_Delete_Fail();
            }
            return RedirectToAction("Index");
        }

        public ActionResult DataTable() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();

            if (!Menu.Can_Approve && !Menu.Can_Assign) { //see only own content if not approver/assigner
                whereC.Add(" (OwnerAdminId=@ownerAdminId) ");
                whereParams.ownerAdminId = Admin.Id;
            }

            if (param.hasSearch) {
                whereC.Add(" (TitleTH LIKE @searchValue OR TitleEN LIKE @searchValue OR NewsCategoryTitle LIKE @searchValue OR NewsSubCategory LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            DateTime dateFrom = ValueUtils.GetDate(Request.Unvalidated.Form["DateFrom"]);
            DateTime dateTo = ValueUtils.GetDate(Request.Unvalidated.Form["DateTo"]);
            if (dateFrom > DateUtils.SqlMinDate()) {
                whereC.Add(" (CreateDate >= @DateFrom)");
                whereParams.DateFrom = dateFrom;
            }

            if (dateTo > DateUtils.SqlMinDate()) {
                whereC.Add(" (CreateDate <= @DateTo)");
                whereParams.DateTo = DateUtils.ExtendToEndofDate(dateTo);
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "Id", "NewsCategory", "NewsSubCategory", "TitleTH", "CreateDate", "UpdateDate", "Status", "ApproveStatus" };
            DataTableData<News> data = repo.ListNews_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Id.ToString(),
                        x.NewsCategory.ToString(),
                        x.NewsSubCategory,
                        x.TitleTH,
                        x.CreateDate.ToString("dd/MM/yyyy HH:mm"),
                        x.UpdateDate.ToString("dd/MM/yyyy HH:mm"),
                        x.Status.ToString(),
                        x.ApproveStatus.ToString(),
                        (
                            HtmlActionButton(AdminAction.View, x.Id) +
                            HtmlActionButton(AdminAction.Edit, x.Id, "", (x.ApproveStatus == ApproveStatus.Approve)) +
                            HtmlActionButtonBuilder(Menu.Can_Edit, Url.Action("Gallery") + "?id=" + x.Id.ToString(), "btn btn-xs btn-warning btn-action", "Gallery", "fa fa-image", "", (x.ApproveStatus == ApproveStatus.Approve)) +
                            (x.ApproveStatus == ApproveStatus.Approve ? HtmlActionButton(AdminAction.Disable, x.Id, "", (x.Status == Status.Inactive)) : HtmlActionButton(AdminAction.Delete, x.Id)) +
                            HtmlActionButton(AdminAction.Approve, x.Id, "", (x.ApproveStatus == ApproveStatus.Approve)) +
                            HtmlActionButton(AdminAction.Assign, x.Id, "", (x.ApproveStatus == ApproveStatus.Approve))
                        )
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ValidateUrlTH(string urlTH, string urlPrefix, int id) {
            return Json((repo.GetNewsByUrl_Admin(urlTH, "", urlPrefix, id) == null), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ValidateUrlEN(string urlEN, string urlPrefix, int id) {
            return Json((repo.GetNewsByUrl_Admin("", urlEN, urlPrefix, id) == null), JsonRequestBehavior.AllowGet);
        }

        private void BindForm() {
            NewsSubCategoryRepository catRepo = new NewsSubCategoryRepository();
            var sub = catRepo.ListNewsSubCategory_SelectList_Admin();
            var selectList = new SelectList(sub, "Id", "TitleTH", "NewsCategory", "", null);
            ViewBag.SubCategoryList = selectList;
            TagRepository tagRepo = new TagRepository();
            ViewBag.TagList = tagRepo.ListTag_SelectList_Admin();
        }
    }
}
