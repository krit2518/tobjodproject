﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TobJod.Models;
using TobJod.Repositories;

namespace TobJod.Admin.Controllers {
    public class NewsSubCategoryController : BaseController {
        NewsSubCategoryRepository repo;

        public NewsSubCategoryController() {
            repo = new NewsSubCategoryRepository();
        }
        
        // GET: NewsSubCategory
        public ActionResult Index() {
            //NewsSubCategory item = new NewsSubCategory();
            //item.Category = NullUtils.cvInt(Request.QueryString["category"]);
            return View();
        }

        private void BindForm() {
            List<SelectListItem> items = new List<SelectListItem>();
            for (int i = 1; i <= 3; i++) {
                items.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });
            }
            ViewBag.ClassList = items;
        }

        // GET: NewsSubCategory/Create
        public ActionResult Create() {
            NewsSubCategory item = new NewsSubCategory();
            item.Status = Status.Active;
            BindForm();
            return View("Form", item);
        }

        // POST: NewsSubCategory/Create
        [HttpPost]
        public ActionResult Create(NewsSubCategory item) {
            try {
                item.CreateDate = item.UpdateDate = DateTime.Now;
                item.CreateAdminId = item.UpdateAdminId = Admin.Id;
                
                repo.AddNewsSubCategory(item);
                Notify_Add_Success();
                return RedirectToAction("Index");
            } catch { }
            Notify_Add_Fail();
            BindForm();
            return View("Form", item);
        }

        // GET: NewsSubCategory/Edit/5
        public ActionResult Edit(int id) {
            NewsSubCategory item = repo.GetNewsSubCategory_Admin(id);
            BindForm();
            return View("Form", item);
        }

        // POST: NewsSubCategory/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, NewsSubCategory item) {
            int result = 0;
            try {
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = Admin.Id;

                result = repo.UpdateNewsSubCategory(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            BindForm();
            return View("Form", item);
        }

        // GET: NewsSubCategory/Delete/5
        public ActionResult Delete(int id) {
            NewsRepository newsRepo = new NewsRepository();
            var items = newsRepo.ListNewsBySubCategory_Admin(id);
            if (items.Count == 0) {
                var result = repo.DeleteNewsSubCategory(id, Admin.Id);
                if (result > 0) {
                    Notify_Delete_Success();
                } else {
                    Notify_Delete_Fail();
                }
            } else {
                Notify(NotifyMessageType.Error, "Delete Error", "Record is in used.");
            }
            return RedirectToAction("Index");
        }

        // GET: NewsSubCategory/DataTable
        public ActionResult DataTable() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();
           
            if (param.hasSearch) {
                whereC.Add(" (TitleTH LIKE @searchValue OR TitleEN LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "Id", "NewsCategory", "TitleTH", "UpdateDate", "Status" };
            DataTableData<NewsSubCategory> data = repo.ListNewsSubCategory_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Id.ToString(),
                        x.NewsCategory.ToString(),
                        x.TitleTH.ToString(),
                        x.UpdateDate.ToString("dd/MM/yyyy HH:mm"),
                        x.Status.ToString(),
                        (HtmlActionButton(AdminAction.Edit, x.Id) + HtmlActionButton(AdminAction.Delete, x.Id))
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}
