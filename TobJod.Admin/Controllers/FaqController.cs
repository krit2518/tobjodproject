﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TobJod.Models;
using TobJod.Repositories;

namespace TobJod.Admin.Controllers {
    public class FaqController : BaseController {
        FaqRepository repo;

        public FaqController() {
            repo = new FaqRepository();
        }
        
        // GET: Faq
        public ActionResult Index() {
            //Faq item = new Faq();
            //item.Category = NullUtils.cvInt(Request.QueryString["category"]);
            return View();
        }

        // GET: Faq/Create
        public ActionResult Create() {
            Faq item = new Faq();
            item.Status = Status.Active;
            BindForm();
            return View("Form", item);
        }

        // POST: Faq/Create
        [HttpPost]
        public ActionResult Create(Faq item) {
            try {
                item.AnswerTH = ReplaceTextEditorPath(item.AnswerTH);
                item.AnswerEN = ReplaceTextEditorPath(item.AnswerEN);
                item.CreateDate = item.UpdateDate = DateTime.Now;
                item.CreateAdminId = item.UpdateAdminId = Admin.Id;
                
                repo.AddFaq(item);
                Notify_Add_Success();
                return RedirectToAction("Index");
            } catch { }
            Notify_Add_Fail();
            BindForm();
            return View("Form", item);
        }

        // GET: Faq/Edit/5
        public ActionResult Edit(int id) {
            Faq item = repo.GetFaq_Admin(id);
            item.AnswerTH = ReplaceTextEditorPathDisplay(item.AnswerTH);
            item.AnswerEN = ReplaceTextEditorPathDisplay(item.AnswerEN);
            BindForm();
            return View("Form", item);
        }

        // POST: Faq/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Faq item) {
            int result = 0;
            try {
                item.AnswerTH = ReplaceTextEditorPath(item.AnswerTH);
                item.AnswerEN = ReplaceTextEditorPath(item.AnswerEN);
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = Admin.Id;
                
                result = repo.UpdateFaq(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            BindForm();
            return View("Form", item);
        }

        // GET: Faq/Delete/5
        public ActionResult Delete(int id) {
            var result = repo.DeleteFaq(id, Admin.Id);
            if (result > 0) {
                Notify_Delete_Success();
            } else {
                Notify_Delete_Fail();
            }
            return RedirectToAction("Index");
        }

        // GET: Faq/DataTable
        public ActionResult DataTable() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();
           
            if (param.hasSearch) {
                whereC.Add(" (QuestionTH LIKE @searchValue OR QuestionEN LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "Id", "Sequence", "Pin", "QuestionTH", "Category", "UpdateDate", "Status" };
            DataTableData<Faq> data = repo.ListFaq_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Id.ToString(),
                        x.Sequence.ToString(),
                        (x.Pin ? "Y" : ""),
                        x.QuestionTH.ToString(),
                        x.Category.ToString(),
                        x.UpdateDate.ToString("dd/MM/yyyy HH:mm"),
                        x.Status.ToString(),
                        (HtmlActionButton(AdminAction.Edit, x.Id) + HtmlActionButton(AdminAction.Delete, x.Id))
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        private void BindForm() {
            MasterOptionRepository masterRepo = new MasterOptionRepository();
            List<SelectListItem> items = masterRepo.ListMasterOption_SelectList_Admin(MasterOptionCategory.FAQCategory);
            ViewBag.CategoryList = items;
        }

    }
}
