﻿using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class SubDistrictController : BaseController {
        SubDistrictRepository repo;

        public SubDistrictController() {
            repo = new SubDistrictRepository();
        }
        
        // GET: SubDistrict
        public ActionResult Index() {
            return View();
        }

        // GET: SubDistrict/Create
        public ActionResult Create() {
            SubDistrict item = new SubDistrict();
            item.Status = Status.Active;
            BindForm(0);
            return View("Form", item);
        }

        // POST: SubDistrict/Create
        [HttpPost]
        public ActionResult Create(SubDistrict item) {
            try {
                item.CreateDate = item.UpdateDate = DateTime.Now;
                item.CreateAdminId = item.UpdateAdminId = Admin.Id;

                repo.AddSubDistrict(item);
                Notify_Add_Success();
                return RedirectToAction("Index");
            } catch { }
            Notify_Add_Fail();
            BindForm(item.DistrictId);
            return View("Form", item);
        }

        // GET: SubDistrict/Edit/5
        public ActionResult Edit(int id) {
            SubDistrict item = repo.GetSubDistrict_Admin(id);
            BindForm(item.DistrictId);
            return View("Form", item);
        }

        // POST: SubDistrict/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, SubDistrict item) {
            int result = 0;
            try {
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = Admin.Id;

                result = repo.UpdateSubDistrict(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            BindForm(item.DistrictId);
            return View("Form", item);
        }

        // GET: SubDistrict/Import
        public ActionResult Import() {
            return View();
        }

        // POST: SubDistrict/Import/5
        [HttpPost]
        public ActionResult Import(HttpPostedFileBase File) {
            List<SubDistrict> items = new List<SubDistrict>();
            StringBuilder sb = new StringBuilder();
            try {
                string path = System.IO.Path.GetTempPath();
                string fileName = UploadUtils.SaveFile(File, path);
                DataSet ds;
                using (var stream = System.IO.File.Open(path + fileName, FileMode.Open, FileAccess.Read)) {
                    using (var reader = ExcelReaderFactory.CreateReader(stream)) {
                        ds = reader.AsDataSet();
                    }
                }

                DistrictRepository pRepo = new DistrictRepository();
                var districtCode = pRepo.ListDistrictCode_Dictionary_Admin();

                StringBuilder sbRow;
                using (ds) {
                    DataTable dt = ds.Tables[0];
                    DateTime now = DateTime.Now;
                    bool isFirst = true;
                    int i = 1;
                    foreach (DataRow dr in dt.Rows) {
                        if (isFirst) { isFirst = false; continue; }

                        var r = new SubDistrict() { DistrictId = 0, Code = NullUtils.cvString(dr[1]), TitleTH = NullUtils.cvString(dr[2]), TitleEN = NullUtils.cvString(dr[3]), ZipCode = NullUtils.cvString(dr[4]), Status = Status.Active, CreateDate = now, CreateAdminId = Admin.Id, UpdateDate = now, UpdateAdminId = Admin.Id };
                        if (DataRowHasData(dr)) {
                            sbRow = new StringBuilder();

                            sbRow.Append(ValidateModelMessage(r));

                            var district = NullUtils.cvString(dr[0]);
                            if (districtCode.ContainsKey(district)) {
                                r.DistrictId = districtCode[district];
                            } else {
                                sbRow.AppendLine("- District \"" + district + "\" Not found.<br/>");
                            }

                            if (sbRow.Length > 0) {
                                sb.Append("<tr><th>Row " + (i + 1) + "</th><td> " + sbRow + "</td></tr>");
                            } else {
                                items.Add(r);
                            }

                        }
                        i++;
                    }
                }

                if (sb.Length > 0) {
                    ViewBag.Message = "INVALID DATA";
                    ViewBag.ErrorMessage = sb.ToString();
                    Notify_Add_Fail();
                    return View();
                } else {
                    int result = repo.AddSubDistrict(items);
                    if (result > 0) {
                        Notify_Add_Success();
                        return RedirectToAction("Index");
                    }
                }
            } catch { }
            Notify_Add_Fail();
            return View();
        }

        // GET: SubDistrict/Delete/5
        public ActionResult Delete(int id) {
            var result = repo.DeleteSubDistrict(id, Admin.Id);
            if (result > 0) {
                Notify_Delete_Success();
            } else {
                Notify_Delete_Fail();
            }
            return RedirectToAction("Index");
        }

        // GET: SubDistrict/DataTable
        public ActionResult DataTable() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();
           
            if (param.hasSearch) {
                whereC.Add(" (ProvinceTH LIKE @searchValue OR ProvinceEN LIKE @searchValue OR DistrictTH LIKE @searchValue OR DistrictEN LIKE @searchValue OR ZipCode LIKE @searchValue OR TitleTH LIKE @searchValue OR TitleEN LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "Id", "Province", "District", "Code", "TitleTH", "ZipCode", "UpdateDate", "Status" };
            DataTableData<SubDistrict> data = repo.ListSubDistrict_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Id.ToString(),
                        x.Province.ToString(),
                        x.District.ToString(),
                        x.Code.ToString(),
                        x.TitleTH.ToString(),
                        x.ZipCode.ToString(),
                        x.UpdateDate.ToString("dd/MM/yyyy HH:mm"),
                        x.Status.ToString(),
                        (HtmlActionButton(AdminAction.Edit, x.Id) + HtmlActionButton(AdminAction.Delete, x.Id))
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        // GET: SubDistrict/SelectList_District
        public ActionResult SelectList_District(int provinceId) {
            DistrictRepository carRepo = new DistrictRepository();
            var data = carRepo.ListDistrict_SelectList_Admin(provinceId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        private void BindForm(int districtId) {
            ProvinceRepository provinceRepo = new ProvinceRepository();
            List<SelectListItem> items = provinceRepo.ListProvince_SelectList_Admin();
            ViewBag.ProvinceList = items;
            District district = null;
            DistrictRepository districtRepo = new DistrictRepository();
            if (districtId > 0) {
                district = districtRepo.GetDistrict_Admin(districtId);
            }
            if (district != null) {
                ViewBag.DistrictList = districtRepo.ListDistrict_SelectList_Admin(district.ProvinceId);
            } else {
                ViewBag.DistrictList = new List<SelectListItem>();
            }
        }

    }
}
