﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class AdminController : BaseController {
        AdminRepository repo;

        public AdminController() {
            repo = new AdminRepository();
        }

        // GET: Admin
        public ActionResult Index() {
            return View();
        }

        // GET: Admin/Create
        public ActionResult Create() {
            TobJod.Models.Admin item = new TobJod.Models.Admin();
            BindForm();
            return View("Form", item);
        }

        // POST: Admin/Create
        [HttpPost]
        public ActionResult Create(TobJod.Models.Admin item) {
            try {
                item.Id = item.EmployeeId;
                item.Status = Status.Active;
                item.CreateDate = item.UpdateDate = DateTime.Now;
                item.CreateAdminId = item.UpdateAdminId = Admin.Id;

                repo.AddAdmin(item);
                Notify_Add_Success();
                return RedirectToAction("Index");
            } catch { }
            Notify_Add_Fail();
            BindForm();
            return View("Form", item);
        }

        // GET: Admin/Edit/5
        public ActionResult Edit(string id) {
            TobJod.Models.Admin item = repo.GetAdmin(id);
            BindForm();
            return View("Form", item);
        }

        // POST: Admin/Edit/5
        [HttpPost]
        public ActionResult Edit(string id, TobJod.Models.Admin item) {
            int result = 0;
            try {
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = Admin.Id;

                result = repo.UpdateAdmin(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            BindForm();
            return View("Form", item);
        }

        // GET: Admin/Group/5
        public ActionResult Group(string id) {
            CheckPermission(AdminAction.Edit);
            BindForm();
            return View("Group");
        }

        // POST: Admin/Group/5
        [HttpPost]
        public ActionResult Group() {
            CheckPermission(AdminAction.Edit);
            var idList = Request.Form["emp"];
            //int result = 0;
            try {
                //    item.UpdateDate = DateTime.Now;
                //    item.UpdateAdminId = Admin.Id;

                //    result = repo.UpdateAdmin(item);
                var result = repo.UpdateAdminGroup(ValueUtils.GetStringSplit(idList, new char[] { ',' }), NullUtils.cvInt(Request.Form["group"]), Admin.Id);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            BindForm();
            return View("Group");
        }

        // GET: Admin/Delete/5
        public ActionResult Delete(string id) {
            //var result = repo.DeleteAdmin(id, Admin.Id);
            var result = repo.UpdateAdminStatus(id, Status.Inactive, Admin.Id);
            if (result > 0) {
                Notify_Delete_Success();
            } else {
                Notify_Delete_Fail();
            }
            return RedirectToAction("Index");
        }

        // GET: Admin/DataTable
        public ActionResult DataTable() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();

            whereC.Add(" Status=@Status");
            whereParams.Status = Status.Active;

            if (param.hasSearch) {
                whereC.Add(" (EmployeeId LIKE @searchValue OR Name LIKE @searchValue OR GroupName LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "EmployeeId", "Name", "GroupName", "UpdateDate", "Id" };
            DataTableData<TobJod.Models.Admin> data = repo.ListAdmin_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.EmployeeId,
                        x.Name,
                        x.GroupName,
                        x.UpdateDate.ToString("dd/MM/yyyy HH:mm"),
                        (HtmlActionButton(AdminAction.Edit, x.Id) + HtmlActionButton(AdminAction.Delete, x.Id))
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        // GET: Admin/DataTable
        public ActionResult DataTableGroup() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();

            if (param.hasSearch) {
                whereC.Add(" (EmployeeId LIKE @searchValue OR Name LIKE @searchValue OR GroupName LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "Id", "EmployeeId", "Name", "GroupName", "UpdateDate" };
            DataTableData<TobJod.Models.Admin> data = repo.ListAdmin_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Id.ToString(),
                        x.EmployeeId,
                        x.Name,
                        x.GroupName,
                        x.UpdateDate.ToString("dd/MM/yyyy HH:mm"),
                        "<button type=\"button\" class=\"btn btn-success btn-add-user\" data-id=\"" + x.Id.ToString() + "\" data-emp=\"" + x.EmployeeId + "\" data-name=\"" + x.Name + "\">Select</button>",
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ValidateEmployeeId(string EmployeeId) {
            var item = repo.GetAdminByEmployeeId(EmployeeId);
            return Json(item == null);
        }

        private void BindForm() {
            AdminGroupRepository groupRepo = new AdminGroupRepository();
            ViewBag.GroupList = groupRepo.ListAdminGroup_Admin().Select(x => new SelectListItem { Text = x.Title, Value = x.Id.ToString() }).ToList();
        }

    }
}
