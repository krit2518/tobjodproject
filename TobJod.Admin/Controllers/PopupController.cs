﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TobJod.Admin.Models;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class PopupController : BaseController {
        PopupRepository repo;

        public PopupController() {
            Path = System.Configuration.ConfigurationManager.AppSettings["Path_Popup"];
            repo = new PopupRepository();
        }

        
        // GET: Popup
        public ActionResult Index() {
            DateTime dateFrom = ValueUtils.GetDate(Request.QueryString["DateFrom"]);
            DateTime dateTo = ValueUtils.GetDate(Request.QueryString["DateTo"]);
            if (dateFrom > DateUtils.SqlMinDate()) ViewBag.DateFrom = dateFrom.ToString("dd/MM/yyyy", cultureGB);
            if (dateTo > DateUtils.SqlMinDate()) ViewBag.DateTo = dateTo.ToString("dd/MM/yyyy", cultureGB);
            return View();
        }

        // GET: Popup/Create
        public ActionResult Create() {
            PopupViewModel item = new PopupViewModel();
            item.Status = Status.Active;
            return View("Form", item);
        }

        // POST: Popup/Create
        [HttpPost]
        public ActionResult Create(PopupViewModel item) {
            if (UploadUtils.IsImage(item.ImageTHFile, item.ImageENFile)) {
                try {
                    item.CreateDate = item.UpdateDate = DateTime.Now;
                    item.CreateAdminId = item.UpdateAdminId = item.OwnerAdminId = Admin.Id;
                    item.ApproveAdminId = "";
                    item.ApproveStatus = ApproveStatus.Pending;
                    item.ImageTH = UploadUtils.SaveFile(item.ImageTHFile, PathPhy);
                    item.ImageEN = UploadUtils.SaveFile(item.ImageENFile, PathPhy);

                    repo.AddPopup(item);
                    Notify_Add_Success();
                    return RedirectToAction("Index");
                } catch { }
            }
            Notify_Add_Fail();
            return View("Form", item);
        }

        // GET: Popup/Edit/5
        public ActionResult Edit(int id) {
            Popup model = repo.GetPopup_Admin(id);
            PopupViewModel item = PopupViewModel.Map(model);
            return View("Form", item);
        }

        // POST: Popup/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, PopupViewModel item) {
            int result = 0;
            if (UploadUtils.IsImage(item.ImageTHFile, item.ImageENFile)) {
                try {
                    item.UpdateDate = DateTime.Now;
                    item.UpdateAdminId = Admin.Id; // = item.OwnerAdminId
                    item.ImageTH = UploadUtils.SaveAndReplaceFile(item.ImageTHFile, PathPhy, item.ImageTH);
                    item.ImageEN = UploadUtils.SaveAndReplaceFile(item.ImageENFile, PathPhy, item.ImageEN);

                    result = repo.UpdatePopup(item);
                    if (result > 0) {
                        Notify_Update_Success();
                        return RedirectToAction("Index");
                    }
                } catch { }
            }
            Notify_Update_Fail();
            return View("Form", item);
        }

        // GET: Popup/View/5
        public ActionResult View(int id) {
            Popup model = repo.GetPopup_Admin(id);
            PopupViewModel item = PopupViewModel.Map(model);
            return View("View", item);
        }

        // GET: Popup/Approve/5
        public ActionResult Approve(int id) {
            Popup x = repo.GetPopup_Admin(id);
            PopupViewModel item = PopupViewModel.Map(x);
            return View("View", item);
        }

        // POST: Popup/Approve/5
        [HttpPost]
        public ActionResult Approve(int id, PopupViewModel item) {
            int result = 0;
            try {
                item.ApproveStatus = (ApproveStatus)Enum.Parse(typeof(ApproveStatus), Request.Form["Approve"].ToString());
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = item.ApproveAdminId = Admin.Id; // = item.OwnerAdminId

                result = repo.UpdatePopupApproveStatus(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            return View("View", item);
        }

        // GET: Popup/Assign/5
        public ActionResult Assign(int id) {
            Popup model = repo.GetPopup_Admin(id);
            AdminRepository adminRepo = new AdminRepository();
            ViewBag.OwnerAdminList = adminRepo.ListAdminByPermission(Menu.Module, AdminAction.Edit).Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() }).ToList();
            PopupViewModel item = PopupViewModel.Map(model);
            return View("View", item);
        }

        // POST: Popup/Assign/5
        [HttpPost]
        public ActionResult Assign(int id, PopupViewModel item) {
            int result = 0;
            try {
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = Admin.Id;

                result = repo.UpdatePopupOwner(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            return View("View", item);
        }

        // GET: Popup/Disable/5
        public ActionResult Disable(int id) {
            Popup item = new Popup();
            item.Id = id;
            item.Status = Status.Inactive;
            item.UpdateDate = DateTime.Now;
            item.UpdateAdminId = Admin.Id;
            var result = repo.UpdatePopupStatus(item);
            if (result > 0) {
                Notify_Update_Success();
            } else {
                Notify_Update_Fail();
            }
            return RedirectToAction("Index");
        }

        // GET: Popup/Delete/5
        public ActionResult Delete(int id) {
            var item = repo.GetPopup_Admin(id);
            var result = repo.DeletePopup(id, Admin.Id);
            if (result > 0) {
                FileUtils.DeleteFiles(PathPhy, item.ImageEN, item.ImageTH);
                Notify_Delete_Success();
            } else {
                Notify_Delete_Fail();
            }
            return RedirectToAction("Index");
        }

        public ActionResult DataTable() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();

            DateTime dateFrom = ValueUtils.GetDate(Request.Unvalidated.Form["DateFrom"]);
            DateTime dateTo = ValueUtils.GetDate(Request.Unvalidated.Form["DateTo"]);
            if (dateFrom > DateUtils.SqlMinDate()) {
                whereC.Add(" (ActiveDate >= @DateFrom)");
                whereParams.DateFrom = dateFrom;
            }

            if (dateTo > DateUtils.SqlMinDate()) {
                whereC.Add(" (ActiveDate <= @DateTo)");
                whereParams.DateTo = dateTo;
            }

            if (!Menu.Can_Approve && !Menu.Can_Assign) { //see only own content if not approver/assigner
                whereC.Add(" (OwnerAdminId=@ownerAdminId) ");
                whereParams.ownerAdminId = Admin.Id;
            }
            
            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "Id", "ImageTH", "ActiveDate", "ExpireDate", "UpdateDate", "Status", "ApproveStatus" };
            DataTableData<Popup> data = repo.ListPopup_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Id.ToString(),
                        HtmlImage(PathRel, x.ImageTH),
                        x.ActiveDate.ToString("dd/MM/yyyy"),
                        x.ExpireDate.ToString("dd/MM/yyyy"),
                        x.UpdateDate.ToString("dd/MM/yyyy HH:mm"),
                        x.Status.ToString(),
                        x.ApproveStatus.ToString(),
                        (
                            HtmlActionButton(AdminAction.View, x.Id) + 
                            HtmlActionButton(AdminAction.Edit, x.Id, "", (x.ApproveStatus == ApproveStatus.Approve)) +
                            (x.ApproveStatus == ApproveStatus.Approve ? HtmlActionButton(AdminAction.Disable, x.Id, "", (x.Status == Status.Inactive)) : HtmlActionButton(AdminAction.Delete, x.Id)) +
                            HtmlActionButton(AdminAction.Approve, x.Id, "", (x.ApproveStatus == ApproveStatus.Approve)) +
                            HtmlActionButton(AdminAction.Assign, x.Id, "", (x.ApproveStatus == ApproveStatus.Approve))
                        )
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}
