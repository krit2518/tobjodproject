﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TobJod.Admin.Models;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class ConfigController : BaseController {
        ConfigRepository repo;

        public ConfigController() {
            Path = System.Configuration.ConfigurationManager.AppSettings["Path_Config"];
            repo = new ConfigRepository();
        }
        
        // GET: ConfigPage
        public ActionResult Index() {
            if (!HasPermission(AdminAction.Edit, this.Module) && HasPermission(AdminAction.View, this.Module)) {
                return RedirectToAction("Views");
            }
            return RedirectToAction("Edit");
        }

        // GET: ConfigPage/Edit
        public ActionResult Edit() {
            var items = repo.ListConfigs_Admin();
            ConfigViewModel item = new ConfigViewModel(items);
            return View("Form", item);
        }

        // POST: ConfigPage/Edit/5
        [HttpPost]
        public ActionResult Edit(ConfigViewModel item) {
            int result = 0;
            if (UploadUtils.IsImage(item.OGImageFile, item.EmailImageHeaderFile, item.EmailImageFooterFile)) {
                //try {
                item.OGImage = UploadUtils.SaveAndReplaceFile(item.OGImageFile, PathPhy, item.OGImage);
                item.EmailImageHeader = UploadUtils.SaveAndReplaceFile(item.EmailImageHeaderFile, PathPhy, item.EmailImageHeader);
                item.EmailImageFooter = UploadUtils.SaveAndReplaceFile(item.EmailImageFooterFile, PathPhy, item.EmailImageFooter);

                result = repo.UpdateConfigs(item.ToList(), DateTime.Now, Admin.Id);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Edit");
                }
                //} catch { }
            }
            Notify_Update_Fail();
            return View("Form", item);
        }

        // GET: ConfigPage/Views
        public ActionResult Views() {
            var items = repo.ListConfigs_Admin();
            ConfigViewModel item = new ConfigViewModel(items);
            return View("View", item);
        }

    }
}
