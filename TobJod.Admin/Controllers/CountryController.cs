﻿using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class CountryController : BaseController {
        CountryRepository repo;

        public CountryController() {
            repo = new CountryRepository();
        }
        
        // GET: Country
        public ActionResult Index() {
            return View();
        }

        // GET: Country/Create
        public ActionResult Create() {
            Country item = new Country();
            item.Status = Status.Active;
            BindForm();
            return View("Form", item);
        }

        // POST: Country/Create
        [HttpPost]
        public ActionResult Create(Country item) {
            try {
                item.CreateDate = item.UpdateDate = DateTime.Now;
                item.CreateAdminId = item.UpdateAdminId = Admin.Id;

                item.Code = "";

                repo.AddCountry(item);
                Notify_Add_Success();
                return RedirectToAction("Index");
            } catch { }
            Notify_Add_Fail();
            BindForm();
            return View("Form", item);
        }

        // GET: Country/Edit/5
        public ActionResult Edit(int id) {
            Country item = repo.GetCountry_Admin(id);
            BindForm();
            return View("Form", item);
        }

        // POST: Country/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Country item) {
            int result = 0;
            try {
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = Admin.Id;

                item.Code = "";

                result = repo.UpdateCountry(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            BindForm();
            return View("Form", item);
        }

        // GET: Country/Import
        public ActionResult Import() {
            return View();
        }

        // POST: Country/Import/5
        [HttpPost]
        public ActionResult Import(HttpPostedFileBase File) {
            List<Country> items = new List<Country>();
            StringBuilder sb = new StringBuilder();
            try {
                string path = System.IO.Path.GetTempPath();
                string fileName = UploadUtils.SaveFile(File, path);
                DataSet ds;
                using (var stream = System.IO.File.Open(path + fileName, FileMode.Open, FileAccess.Read)) {
                    using (var reader = ExcelReaderFactory.CreateReader(stream)) {
                        ds = reader.AsDataSet();
                    }
                }

                StringBuilder sbRow;
                using (ds) {
                    DataTable dt = ds.Tables[0];
                    DateTime now = DateTime.Now;
                    bool isFirst = true;
                    int i = 1;
                    foreach (DataRow dr in dt.Rows) {
                        if (isFirst) { isFirst = false; continue; }
                        var r = new Country() { Zone = (CountryZone)NullUtils.cvInt(dr[0]), TitleTH = NullUtils.cvString(dr[1]), TitleEN = NullUtils.cvString(dr[2]), Status = Status.Active, CreateDate = now, CreateAdminId = Admin.Id, UpdateDate = now, UpdateAdminId = Admin.Id };
                        sbRow = new StringBuilder();

                        sbRow.Append(ValidateModelMessage(r));

                        if (sbRow.Length > 0) {
                            sb.Append("<tr><th>Row " + (i + 1) + "</th><td> " + sbRow + "</td></tr>");
                        } else {
                            items.Add(r);
                        }

                        i++;
                    }
                }

                if (sb.Length > 0) {
                    ViewBag.Message = "INVALID DATA";
                    ViewBag.ErrorMessage = sb.ToString();
                    Notify_Add_Fail();
                    return View();
                } else {
                    int result = repo.AddCountry(items);
                    if (result > 0) {
                        Notify_Add_Success();
                        return RedirectToAction("Index");
                    }
                }
            } catch { }
            Notify_Add_Fail();
            return View();
        }

        // GET: Country/Delete/5
        public ActionResult Delete(int id) {
            var result = repo.DeleteCountry(id, Admin.Id);
            if (result > 0) {
                Notify_Delete_Success();
            } else {
                Notify_Delete_Fail();
            }
            return RedirectToAction("Index");
        }

        // GET: Country/DataTable
        public ActionResult DataTable() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();
           
            if (param.hasSearch) {
                whereC.Add(" (TitleTH LIKE @searchValue OR TitleEN LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "Id", "Zone", "TitleTH", "UpdateDate", "Status" };
            DataTableData<Country> data = repo.ListCountry_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Id.ToString(),
                        x.Zone.ToString(),
                        x.TitleTH.ToString(),
                        x.UpdateDate.ToString("dd/MM/yyyy HH:mm"),
                        x.Status.ToString(),
                        (HtmlActionButton(AdminAction.Edit, x.Id) + HtmlActionButton(AdminAction.Delete, x.Id))
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        private void BindForm() {
            //List<SelectListItem> items = new List<SelectListItem>();
            //for (int i = 1; i <= 3; i++) {
            //    items.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });
            //}
            //ViewBag.ZoneList = items;
        }

    }
}
