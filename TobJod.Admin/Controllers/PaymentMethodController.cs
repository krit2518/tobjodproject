﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TobJod.Admin.Models;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class PaymentMethodController : BaseController {
        PaymentMethodRepository repo;
        ConfigPageRepository configRepo;
        ConfigPageName id = ConfigPageName.PaymentMethod;
        ConfigPageName draft_id = ConfigPageName.PaymentMethod_Draft;

        public PaymentMethodController() {
            Path = System.Configuration.ConfigurationManager.AppSettings["Path_PaymentMethod"];
            repo = new PaymentMethodRepository();
            configRepo = new ConfigPageRepository();
        }

        // GET: PaymentMethod
        public ActionResult Index() {
            DateTime dateFrom = ValueUtils.GetDate(Request.QueryString["DateFrom"]);
            DateTime dateTo = ValueUtils.GetDate(Request.QueryString["DateTo"]);
            if (dateFrom > DateUtils.SqlMinDate()) ViewBag.DateFrom = dateFrom.ToString("dd/MM/yyyy", cultureGB);
            if (dateTo > DateUtils.SqlMinDate()) ViewBag.DateTo = dateTo.ToString("dd/MM/yyyy", cultureGB);

            ConfigPage model = configRepo.GetConfigPage_Admin(draft_id);
            ConfigPageViewModel item = ConfigPageViewModel.Map(model);
            item.CurrentPage = configRepo.GetConfigPage_Admin(id);
            item.CurrentPage.BodyTH = ReplaceTextEditorPathDisplay(item.CurrentPage.BodyTH);
            item.CurrentPage.BodyEN = ReplaceTextEditorPathDisplay(item.CurrentPage.BodyEN);
            item.OGImageTHSame = (string.IsNullOrEmpty(item.OGImageTH));
            item.OGImageENSame = (string.IsNullOrEmpty(item.OGImageEN));
            item.BodyTH = ReplaceTextEditorPathDisplay(item.BodyTH);
            item.BodyEN = ReplaceTextEditorPathDisplay(item.BodyEN);

            return View("Index", item);
        }

        // GET: ConfigPage/EditPage
        public ActionResult EditPage() {
            ConfigPage model = configRepo.GetConfigPage_Admin(draft_id);
            ConfigPageViewModel item = ConfigPageViewModel.Map(model);
            item.OGImageTHSame = (string.IsNullOrEmpty(item.OGImageTH));
            item.OGImageENSame = (string.IsNullOrEmpty(item.OGImageEN));
            item.BodyTH = ReplaceTextEditorPathDisplay(item.BodyTH);
            item.BodyEN = ReplaceTextEditorPathDisplay(item.BodyEN);
            return View("FormPage", item);
        }

        // POST: ConfigPage/EditPage/5
        [HttpPost]
        public ActionResult EditPage(ConfigPageViewModel item) {
            int result = 0;
            if (UploadUtils.IsImage(item.OGImageTHFile, item.OGImageENFile)) {
                try {
                    item.Id = draft_id;
                    item.BodyTH = ReplaceTextEditorPath(item.BodyTH);
                    item.BodyEN = ReplaceTextEditorPath(item.BodyEN);
                    item.UpdateDate = DateTime.Now;
                    item.UpdateAdminId = Admin.Id;
                    item.ApproveStatus = ApproveStatus.Pending;
                    item.OGImageTH = item.OGImageTHSame ? "" : UploadUtils.SaveAndReplaceFile(item.OGImageTHFile, PathPhy, item.OGImageTH);
                    item.OGImageEN = item.OGImageENSame ? "" : UploadUtils.SaveAndReplaceFile(item.OGImageENFile, PathPhy, item.OGImageEN);
                    item.Data1 = item.Data2 = item.Data3 = "";
                    item.ApproveStatus = ApproveStatus.Pending;
                    item.ApproveAdminId = "";

                    result = configRepo.UpdateConfigPage(item);
                    if (result > 0) {
                        Notify_Update_Success();
                        return RedirectToAction("Index");
                    }
                } catch { }
            }
            Notify_Update_Fail();
            return View("FormPage", item);
        }

        // POST: ConfigPage/ApprovePage
        [HttpPost]
        public ActionResult ApprovePage(ConfigPageViewModel item) {
            int result = 0;
            try {
                item = ConfigPageViewModel.Map(configRepo.GetConfigPage_Admin(draft_id));
                item.ApproveStatus = (ApproveStatus)Enum.Parse(typeof(ApproveStatus), Request.Form["Approve"].ToString());
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = item.ApproveAdminId = Admin.Id; // = item.OwnerAdminId

                result = configRepo.UpdateConfigPageApproveStatus(item, (int)draft_id);
                if (result > 0) {
                    if (item.ApproveStatus == ApproveStatus.Approve) {
                        configRepo.UpdateConfigPageCopy((int)draft_id, (int)id, Admin.Id);
                    }
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            return RedirectToAction("Index");
        }

        // GET: PaymentMethod/Create
        public ActionResult Create() {
            PaymentMethodViewModel item = new PaymentMethodViewModel();
            item.Status = Status.Active;
            item.OGImageTHSame = item.OGImageENSame = true;
            BindForm();
            return View("Form", item);
        }

        // POST: PaymentMethod/Create
        [HttpPost]
        public ActionResult Create(PaymentMethodViewModel item) {
            try {
                item.BriefTH = ReplaceTextEditorPath(item.BriefTH);
                item.BriefEN = ReplaceTextEditorPath(item.BriefEN);
                item.BodyTH = ReplaceTextEditorPath(item.BodyTH);
                item.BodyEN = ReplaceTextEditorPath(item.BodyEN);
                item.CreateDate = item.UpdateDate = DateTime.Now;
                item.CreateAdminId = item.UpdateAdminId = item.OwnerAdminId = Admin.Id;
                item.ApproveAdminId = "";
                item.ApproveStatus = ApproveStatus.Pending;
                item.ImageTH = UploadUtils.SaveFile(item.ImageTHFile, PathPhy);
                item.ImageEN = UploadUtils.SaveFile(item.ImageENFile, PathPhy);
                item.OGImageTH = item.OGImageTHSame ? "" : UploadUtils.SaveFile(item.OGImageTHFile, PathPhy);
                item.OGImageEN = item.OGImageENSame ? "" : UploadUtils.SaveFile(item.OGImageENFile, PathPhy);

                repo.AddPaymentMethod(item);
                Notify_Add_Success();
                return RedirectToAction("Index");
            } catch { }
            Notify_Add_Fail();
            BindForm();
            return View("Form", item);
        }

        // GET: PaymentMethod/Edit/5
        public ActionResult Edit(int id) {
            PaymentMethod model = repo.GetPaymentMethod_Admin(id);
            PaymentMethodViewModel item = PaymentMethodViewModel.Map(model);
            item.OGImageTHSame = (string.IsNullOrEmpty(item.OGImageTH));
            item.OGImageENSame = (string.IsNullOrEmpty(item.OGImageEN));
            item.BriefTH = ReplaceTextEditorPathDisplay(item.BriefTH);
            item.BriefEN = ReplaceTextEditorPathDisplay(item.BriefEN);
            item.BodyTH = ReplaceTextEditorPathDisplay(item.BodyTH);
            item.BodyEN = ReplaceTextEditorPathDisplay(item.BodyEN);
            BindForm();
            return View("Form", item);
        }

        // POST: PaymentMethod/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, PaymentMethodViewModel item) {
            int result = 0;
            try {
                item.BriefTH = ReplaceTextEditorPath(item.BriefTH);
                item.BriefEN = ReplaceTextEditorPath(item.BriefEN);
                item.BodyTH = ReplaceTextEditorPath(item.BodyTH);
                item.BodyEN = ReplaceTextEditorPath(item.BodyEN);
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = Admin.Id; // = item.OwnerAdminId
                item.ImageTH = UploadUtils.SaveAndReplaceFile(item.ImageTHFile, PathPhy, item.ImageTH);
                item.ImageEN = UploadUtils.SaveAndReplaceFile(item.ImageENFile, PathPhy, item.ImageEN);
                item.OGImageTH = item.OGImageTHSame ? "" : UploadUtils.SaveAndReplaceFile(item.OGImageTHFile, PathPhy, item.OGImageTH);
                item.OGImageEN = item.OGImageENSame ? "" : UploadUtils.SaveAndReplaceFile(item.OGImageENFile, PathPhy, item.OGImageEN);

                result = repo.UpdatePaymentMethod(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            BindForm();
            return View("Form", item);
        }

        // GET: PaymentMethod/View/5
        public ActionResult View(int id) {
            PaymentMethod x = repo.GetPaymentMethod_Admin(id);
            PaymentMethodViewModel item = PaymentMethodViewModel.Map(x);
            item.BriefTH = ReplaceTextEditorPathDisplay(item.BriefTH);
            item.BriefEN = ReplaceTextEditorPathDisplay(item.BriefEN);
            item.BodyTH = ReplaceTextEditorPathDisplay(item.BodyTH);
            item.BodyEN = ReplaceTextEditorPathDisplay(item.BodyEN);
            BindForm();
            return View("View", item);
        }

        // GET: PaymentMethod/Approve/5
        public ActionResult Approve(int id) {
            PaymentMethod x = repo.GetPaymentMethod_Admin(id);
            PaymentMethodViewModel item = PaymentMethodViewModel.Map(x);
            item.BriefTH = ReplaceTextEditorPathDisplay(item.BriefTH);
            item.BriefEN = ReplaceTextEditorPathDisplay(item.BriefEN);
            item.BodyTH = ReplaceTextEditorPathDisplay(item.BodyTH);
            item.BodyEN = ReplaceTextEditorPathDisplay(item.BodyEN);
            BindForm();
            return View("View", item);
        }

        // POST: PaymentMethod/Approve/5
        [HttpPost]
        public ActionResult Approve(int id, PaymentMethodViewModel item) {
            int result = 0;
            try {
                item.ApproveStatus = (ApproveStatus)Enum.Parse(typeof(ApproveStatus), Request.Form["Approve"].ToString());
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = item.ApproveAdminId = Admin.Id; // = item.OwnerAdminId

                result = repo.UpdatePaymentMethodApproveStatus(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            BindForm();
            return View("View", item);
        }

        // GET: PaymentMethod/Assign/5
        public ActionResult Assign(int id) {
            PaymentMethod model = repo.GetPaymentMethod_Admin(id);
            AdminRepository adminRepo = new AdminRepository();
            ViewBag.OwnerAdminList = adminRepo.ListAdminByPermission(Menu.Module, AdminAction.Edit).Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() }).ToList();
            PaymentMethodViewModel item = PaymentMethodViewModel.Map(model);
            item.BriefTH = ReplaceTextEditorPathDisplay(item.BriefTH);
            item.BriefEN = ReplaceTextEditorPathDisplay(item.BriefEN);
            item.BodyTH = ReplaceTextEditorPathDisplay(item.BodyTH);
            item.BodyEN = ReplaceTextEditorPathDisplay(item.BodyEN);
            BindForm();
            return View("View", item);
        }

        // POST: PaymentMethod/Assign/5
        [HttpPost]
        public ActionResult Assign(int id, PaymentMethodViewModel item) {
            int result = 0;
            try {
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = Admin.Id;

                result = repo.UpdatePaymentMethodOwner(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            BindForm();
            return View("View", item);
        }

        // GET: PaymentMethod/Disable/5
        public ActionResult Disable(int id) {
            PaymentMethod item = new PaymentMethod();
            item.Id = id;
            item.Status = Status.Inactive;
            item.UpdateDate = DateTime.Now;
            item.UpdateAdminId = Admin.Id;
            var result = repo.UpdatePaymentMethodStatus(item);
            if (result > 0) {
                Notify_Update_Success();
            } else {
                Notify_Update_Fail();
            }
            return RedirectToAction("Index");
        }

        // GET: PaymentMethod/Delete/5
        public ActionResult Delete(int id) {
            var item = repo.GetPaymentMethod_Admin(id);
            var result = repo.DeletePaymentMethod(id, Admin.Id);
            if (result > 0) {
                FileUtils.DeleteFiles(PathPhy, item.ImageEN, item.ImageTH, item.OGImageTH, item.OGImageEN);
                Notify_Delete_Success();
            } else {
                Notify_Delete_Fail();
            }
            return RedirectToAction("Index");
        }

        public ActionResult DataTable() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();

            if (!Menu.Can_Approve && !Menu.Can_Assign) { //see only own content if not approver/assigner
                whereC.Add(" (OwnerAdminId=@ownerAdminId) ");
                whereParams.ownerAdminId = Admin.Id;
            }

            if (param.hasSearch) {
                whereC.Add(" (TitleTH LIKE @searchValue OR TitleEN LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            DateTime dateFrom = ValueUtils.GetDate(Request.Unvalidated.Form["DateFrom"]);
            DateTime dateTo = ValueUtils.GetDate(Request.Unvalidated.Form["DateTo"]);
            if (dateFrom > DateUtils.SqlMinDate()) {
                whereC.Add(" (CreateDate >= @DateFrom)");
                whereParams.DateFrom = dateFrom;
            }

            if (dateTo > DateUtils.SqlMinDate()) {
                whereC.Add(" (CreateDate <= @DateTo)");
                whereParams.DateTo = DateUtils.ExtendToEndofDate(dateTo);
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "Id", "TitleTH", "CreateDate", "UpdateDate", "Status", "ApproveStatus" };
            DataTableData<PaymentMethod> data = repo.ListPaymentMethod_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Id.ToString(),
                        x.TitleTH,
                        x.CreateDate.ToString("dd/MM/yyyy HH:mm"),
                        x.UpdateDate.ToString("dd/MM/yyyy HH:mm"),
                        x.Status.ToString(),
                        x.ApproveStatus.ToString(),
                        (
                            HtmlActionButton(AdminAction.View, x.Id) +
                            HtmlActionButton(AdminAction.Edit, x.Id, "", (x.ApproveStatus == ApproveStatus.Approve)) +
                            (x.ApproveStatus == ApproveStatus.Approve ? HtmlActionButton(AdminAction.Disable, x.Id, "", (x.Status == Status.Inactive)) : HtmlActionButton(AdminAction.Delete, x.Id)) +
                            HtmlActionButton(AdminAction.Approve, x.Id, "", (x.ApproveStatus == ApproveStatus.Approve)) +
                            HtmlActionButton(AdminAction.Assign, x.Id, "", (x.ApproveStatus == ApproveStatus.Approve))
                        )
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ValidateUrlTH(string urlTH, string urlPrefix, int id) {
            return Json((repo.GetPaymentMethodByUrl_Admin(urlTH, "", id) == null), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ValidateUrlEN(string urlEN, string urlPrefix, int id) {
            return Json((repo.GetPaymentMethodByUrl_Admin("", urlEN, id) == null), JsonRequestBehavior.AllowGet);
        }

        private void BindForm() {
        }
    }
}
