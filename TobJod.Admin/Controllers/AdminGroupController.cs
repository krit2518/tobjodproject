﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class AdminGroupController : BaseController {
        AdminGroupRepository repo;

        public AdminGroupController() {
            repo = new AdminGroupRepository();
        }

        // GET: Admin
        public ActionResult Index() {
            return View();
        }

        private void BindForm(int adminId) {
            ViewBag.AdminMenu = repo.GetAdminGroupMenu(adminId);
        }

        private List<AdminPermission> GetPermissions() {
            AdminRepository adminRepo = new AdminRepository();
            List<AdminMenu> modules = adminRepo.ListMenu();
            List<AdminPermission> items = new List<AdminPermission>();
            AdminPermission permission;
            foreach (var item in modules) {
                permission = new AdminPermission();
                permission.Module = item.Module;
                string key = "Permission[" + item.Module + "]";
                if (Request.Form.AllKeys.Contains(key + "[View]")) { permission.Can_View = true; }
                if (Request.Form.AllKeys.Contains(key + "[Add]")) { permission.Can_Add = true; }
                if (Request.Form.AllKeys.Contains(key + "[Edit]")) { permission.Can_Edit = true; }
                if (Request.Form.AllKeys.Contains(key + "[Delete]")) { permission.Can_Delete = true; }
                if (Request.Form.AllKeys.Contains(key + "[Approve]")) { permission.Can_Approve = true; }
                if (Request.Form.AllKeys.Contains(key + "[Assign]")) { permission.Can_Assign = true; }
                if (Request.Form.AllKeys.Contains(key + "[Export]")) { permission.Can_Export = true; }
                items.Add(permission);
            }
            return items;
        }

        // GET: Admin/Create
        public ActionResult Create() {
            AdminGroup item = new AdminGroup();
            BindForm(0);
            return View("Form", item);
        }

        // POST: Admin/Create
        [HttpPost]
        public ActionResult Create(AdminGroup item) {
            try {
                item.CreateDate = item.UpdateDate = DateTime.Now;
                item.CreateAdminId = item.UpdateAdminId = Admin.Id;
                item.Permissions = GetPermissions();

                repo.AddAdminGroup(item);
                Notify_Add_Success();
                return RedirectToAction("Index");
            } catch { }
            Notify_Add_Fail();
            BindForm(0);
            return View("Form", item);
        }

        // GET: Admin/Edit/5
        public ActionResult Edit(int id) {
            AdminGroup item = repo.GetAdminGroup_Admin(id);
            BindForm(id);
            return View("Form", item);
        }

        // POST: Admin/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, AdminGroup item) {
            int result = 0;
            try {
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = Admin.Id;
                item.Permissions = GetPermissions();

                result = repo.UpdateAdminGroup(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            BindForm(id);
            return View("Form", item);
        }

        // GET: Admin/Delete/5
        public ActionResult Delete(int id) {
            AdminRepository adminRepo = new AdminRepository();
            var count = adminRepo.CountAdminByGroup(id);
            if (count == 0) {
                var result = repo.DeleteAdminGroup(id, Admin.Id);
                if (result > 0) {
                    Notify_Delete_Success();
                } else {
                    Notify_Delete_Fail();
                }
            } else {
                Notify(NotifyMessageType.Error, "Delete Error", "Record is in used.");
            }
            return RedirectToAction("Index");
        }

        // GET: Admin/DataTable
        public ActionResult DataTable() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();
            
            if (param.hasSearch) {
                whereC.Add(" (Title LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "Id", "Title", "UpdateDate" };
            DataTableData<AdminGroup> data = repo.ListAdminGroup_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Id.ToString(),
                        x.Title,
                        x.UpdateDate.ToString("dd/MM/yyyy HH:mm"),
                        (HtmlActionButton(AdminAction.Edit, x.Id) + HtmlActionButton(AdminAction.Delete, x.Id))
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}
