﻿using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class DistrictController : BaseController {
        DistrictRepository repo;

        public DistrictController() {
            repo = new DistrictRepository();
        }
        
        // GET: District
        public ActionResult Index() {
            return View();
        }

        // GET: District/Create
        public ActionResult Create() {
            District item = new District();
            item.Status = Status.Active;
            BindForm();
            return View("Form", item);
        }

        // POST: District/Create
        [HttpPost]
        public ActionResult Create(District item) {
            try {
                item.CreateDate = item.UpdateDate = DateTime.Now;
                item.CreateAdminId = item.UpdateAdminId = Admin.Id;

                repo.AddDistrict(item);
                Notify_Add_Success();
                return RedirectToAction("Index");
            } catch { }
            Notify_Add_Fail();
            BindForm();
            return View("Form", item);
        }

        // GET: District/Edit/5
        public ActionResult Edit(int id) {
            District item = repo.GetDistrict_Admin(id);
            BindForm();
            return View("Form", item);
        }

        // POST: District/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, District item) {
            int result = 0;
            try {
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = Admin.Id;

                result = repo.UpdateDistrict(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            BindForm();
            return View("Form", item);
        }

        // GET: District/Import
        public ActionResult Import() {
            return View();
        }

        // POST: District/Import/5
        [HttpPost]
        public ActionResult Import(HttpPostedFileBase File) {
            List<District> items = new List<District>();
            StringBuilder sb = new StringBuilder();
            try {
                string path = System.IO.Path.GetTempPath();
                string fileName = UploadUtils.SaveFile(File, path);
                DataSet ds;
                using (var stream = System.IO.File.Open(path + fileName, FileMode.Open, FileAccess.Read)) {
                    using (var reader = ExcelReaderFactory.CreateReader(stream)) {
                        ds = reader.AsDataSet();
                    }
                }

                ProvinceRepository pRepo = new ProvinceRepository();
                var provinceCode = pRepo.ListProvinceCode_Dictionary_Admin();

                StringBuilder sbRow;
                using (ds) {
                    DataTable dt = ds.Tables[0];
                    DateTime now = DateTime.Now;
                    bool isFirst = true;
                    int i = 1;
                    foreach (DataRow dr in dt.Rows) {
                        if (isFirst) { isFirst = false; continue; }

                        var r = new District() { ProvinceId = 0, Code = NullUtils.cvString(dr[1]), TitleTH = NullUtils.cvString(dr[2]), TitleEN = NullUtils.cvString(dr[3]), Status = Status.Active, CreateDate = now, CreateAdminId = Admin.Id, UpdateDate = now, UpdateAdminId = Admin.Id };

                        if (DataRowHasData(dr)) {
                            sbRow = new StringBuilder();

                            sbRow.Append(ValidateModelMessage(r));

                            var province = NullUtils.cvString(dr[0]);
                            if (provinceCode.ContainsKey(province)) {
                                r.ProvinceId = provinceCode[province];
                            } else {
                                sbRow.AppendLine("- Province \"" + province + "\" Not found.<br/>");
                            }

                            if (sbRow.Length > 0) {
                                sb.Append("<tr><th>Row " + (i + 1) + "</th><td> " + sbRow + "</td></tr>");
                            } else {
                                items.Add(r);
                            }
                        }
                        i++;
                    }
                }

                if (sb.Length > 0) {
                    ViewBag.Message = "INVALID DATA";
                    ViewBag.ErrorMessage = sb.ToString();
                    Notify_Add_Fail();
                    return View();
                } else {
                    int result = repo.AddDistrict(items);
                    if (result > 0) {
                        Notify_Add_Success();
                        return RedirectToAction("Index");
                    }
                }
            } catch { }
            Notify_Add_Fail();
            return View();
        }

        // GET: District/Delete/5
        public ActionResult Delete(int id) {
            SubDistrictRepository subRepo = new SubDistrictRepository();
            var items = subRepo.ListSubDistrict_SelectList_Admin(id);
            if (items.Count == 0) {
                var result = repo.DeleteDistrict(id, Admin.Id);
                if (result > 0) {
                    Notify_Delete_Success();
                } else {
                    Notify_Delete_Fail();
                }
            } else {
                Notify(NotifyMessageType.Error, "Delete Error", "Record is in used.");
            }
            return RedirectToAction("Index");
        }

        // GET: District/DataTable
        public ActionResult DataTable() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();
           
            if (param.hasSearch) {
                whereC.Add(" (ProvinceTH LIKE @searchValue OR ProvinceEN LIKE @searchValue OR TitleTH LIKE @searchValue OR TitleEN LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "Id", "Province", "Code", "TitleTH", "UpdateDate", "Status" };
            DataTableData<District> data = repo.ListDistrict_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Id.ToString(),
                        x.Province.ToString(),
                        x.Code.ToString(),
                        x.TitleTH.ToString(),
                        x.UpdateDate.ToString("dd/MM/yyyy HH:mm"),
                        x.Status.ToString(),
                        (HtmlActionButton(AdminAction.Edit, x.Id) + HtmlActionButton(AdminAction.Delete, x.Id))
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        private void BindForm() {
            ProvinceRepository provinceRepo = new ProvinceRepository();
            List<SelectListItem> items = provinceRepo.ListProvince_SelectList_Admin();
            ViewBag.ProvinceList = items;
        }

    }
}
