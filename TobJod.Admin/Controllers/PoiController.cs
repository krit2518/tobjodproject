﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TobJod.Admin.Models;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class PoiController : BaseController {
        ConfigPageRepository repo;
        ConfigPageName id = ConfigPageName.POI;

        public PoiController() {
            repo = new ConfigPageRepository();
        }
        
        // GET: ConfigPage
        public ActionResult Index() {
            return RedirectToAction("Edit");
        }

        // GET: ConfigPage/Edit
        public ActionResult Edit() {
            ConfigPage model = repo.GetConfigPage_Admin(id);
            ConfigPageViewModel item = ConfigPageViewModel.Map(model);
            item.OGImageTHSame = (string.IsNullOrEmpty(item.OGImageTH));
            item.OGImageENSame = (string.IsNullOrEmpty(item.OGImageEN));
            return View("Form", item);
        }

        // POST: ConfigPage/Edit/5
        [HttpPost]
        public ActionResult Edit(ConfigPageViewModel item) {
            int result = 0;
            try {
                item.Id = id;
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = Admin.Id;
                item.OGImageTH = item.OGImageTHSame ? "" : UploadUtils.SaveAndReplaceFile(item.OGImageTHFile, PathPhy, item.OGImageTH);
                item.OGImageEN = item.OGImageENSame ? "" : UploadUtils.SaveAndReplaceFile(item.OGImageENFile, PathPhy, item.OGImageEN);
                item.Data1 = item.Data2 = item.Data3 = "";

                result = repo.UpdateConfigPage(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            return View("Form", item);
        }
        
    }
}
