﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class LeadFormController : BaseController {
        LeadFormRepository repo;

        public LeadFormController() {
            repo = new LeadFormRepository();
        }

        // GET: LeadForm
        public ActionResult Index() {
            DateTime dateFrom = ValueUtils.GetDate(Request.Unvalidated.QueryString["DateFrom"]);
            DateTime dateTo = ValueUtils.GetDate(Request.Unvalidated.QueryString["DateTo"]);
            if (dateFrom > DateUtils.SqlMinDate()) ViewBag.DateFrom = dateFrom.ToString("dd/MM/yyyy", cultureGB);
            if (dateTo > DateUtils.SqlMinDate()) ViewBag.DateTo = dateTo.ToString("dd/MM/yyyy", cultureGB);
            if (Request.QueryString.AllKeys.Contains("export")) {
                Export();
            }
            return View();
        }

        // GET: LeadForm/Delete/5
        public ActionResult Delete(int id) {
            var result = repo.DeleteLeadForm(id, Admin.Id);
            if (result > 0) {
                Notify_Delete_Success();
            } else {
                Notify_Delete_Fail();
            }
            return RedirectToAction("Index");
        }

        // GET: LeadForm/Export
        public void Export() {
            string where = string.Empty;
            List<string> whereC = new List<string>();

            DateTime dateFrom = ValueUtils.GetDate(Request.Unvalidated.QueryString ["DateFrom"]);
            DateTime dateTo = ValueUtils.GetDate(Request.Unvalidated.QueryString["DateTo"]);
            if (dateFrom > DateUtils.SqlMinDate()) {
                whereC.Add(" (CreateDate >= '" + dateFrom.ToString("yyyy-MM-dd", new CultureInfo("en-GB")) + "')");
            }
            if (dateTo > DateUtils.SqlMinDate()) {
                whereC.Add(" (CreateDate <= '" + dateTo.ToString("yyyy-MM-dd", new CultureInfo("en-GB")) + " 23:59:59')");
            }
            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            var dt = repo.ListLeadForm_Export(where, null);
            ExportFile("LeadForm.xlsx", "LeadForm",  dt, null, null, null, new string[] { "CreateDate" });
        }

        // GET: LeadForm/DataTable
        public ActionResult DataTable() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();

            DateTime dateFrom = ValueUtils.GetDate(Request.Unvalidated.Form["DateFrom"]);
            DateTime dateTo = ValueUtils.GetDate(Request.Unvalidated.Form["DateTo"]);
            if (dateFrom > DateUtils.SqlMinDate()) {
                whereC.Add(" (CreateDate >= @DateFrom)");
                whereParams.DateFrom = dateFrom;
            }

            if (dateTo > DateUtils.SqlMinDate()) {
                dateTo = ValueUtils.ExtendToEndOfDate(dateTo);
                whereC.Add(" (CreateDate <= @DateTo)");
                whereParams.DateTo = dateTo;
            }

            if (param.hasSearch) {
                whereC.Add(" ((Name + ' ' + Surname) LIKE @searchValue OR Tel LIKE @searchValue OR Email LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }
            
            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "Id", "CreateDate", "Name", "Tel", "Email", "Surname" , "CarSerialNo", "CarEngineNo", "CarPlateNo1", "CarPlateNo2", "CarProvince", "ProductCode", "APIStatusCategory", "OrderCode", "OrderStatus" };
            DataTableData<LeadFormProductApplication> data = repo.ListLeadForm_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Id.ToString(),
                        x.CreateDate.ToString("dd/MM/yyyy HH:mm"),
                        (x.Name.ToString() + " " + x.Surname.ToString()),
                        x.Tel.ToString(),
                        x.Email.ToString(),
                        NullUtils.cvString(x.CarEngineNo),
                        NullUtils.cvString(x.CarSerialNo),
                        NullUtils.cvString(x.CarPlateNo1) + NullUtils.cvString(x.CarPlateNo2),
                        NullUtils.cvString(x.CarProvince),
                        NullUtils.cvString(x.ProductCode),
                        NullUtils.cvString(x.OrderCode),
                        x.APIStatusCategory == APIStatusCategory.Motor_C_Fail ? "Call Back (C)" : (x.APIStatusCategory == APIStatusCategory.Motor_V_Fail ? "Call Back (V)": ( x.OrderStatus == OrderStatus.Success ? "Order Success": "")),
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }
        
    }
}
