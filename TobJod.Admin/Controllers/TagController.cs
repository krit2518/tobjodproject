﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class TagController : BaseController {
        TagRepository repo;

        public TagController() {
            repo = new TagRepository();
        }
        
        // GET: Tag
        public ActionResult Index() {
            //Tag item = new Tag();
            //item.Category = NullUtils.cvInt(Request.QueryString["category"]);
            return View();
        }

        // GET: Tag/Create
        public ActionResult Create() {
            Tag item = new Tag();
            return View("Form", item);
        }

        // POST: Tag/Create
        [HttpPost]
        public ActionResult Create(Tag item) {
            try {
                item.CreateDate = item.UpdateDate = DateTime.Now;
                item.CreateAdminId = item.UpdateAdminId = Admin.Id;

                repo.AddTag(item);
                Notify_Add_Success();
                return RedirectToAction("Index");
            } catch { }
            Notify_Add_Fail();
            return View("Form", item);
        }

        // GET: Tag/Edit/5
        public ActionResult Edit(int id) {
            Tag item = repo.GetTag_Admin(id);
            return View("Form", item);
        }

        // POST: Tag/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Tag item) {
            int result = 0;
            try {
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = Admin.Id;

                result = repo.UpdateTag(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            return View("Form", item);
        }

        // GET: Tag/Delete/5
        public ActionResult Delete(int id) {
            var result = repo.DeleteTag(id, Admin.Id);
            if (result > 0) {
                Notify_Delete_Success();
            } else {
                Notify_Delete_Fail();
            }
            return RedirectToAction("Index");
        }

        // GET: Tag/Export
        public void Export() {
            var dt = repo.ListTag_Export("", null);
            ExportFile("Tag.xlsx", "Tag", dt, null, null, new string[] { "CreateDate", "UpdateDate" });
        }

        // GET: Tag/DataTable
        public ActionResult DataTable() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();
            
            if (param.hasSearch) {
                whereC.Add(" (Title LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "Id", "Title", "UpdateDate" };
            DataTableData<Tag> data = repo.ListTag_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Id.ToString(),
                        x.Title.ToString(),
                        x.UpdateDate.ToString("dd/MM/yyyy HH:mm"),
                        (HtmlActionButton(AdminAction.Edit, x.Id) + HtmlActionButton(AdminAction.Delete, x.Id))
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}
