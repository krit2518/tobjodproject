﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class AuthenLogController : BaseController {
        AdminAuthenLogRepository repo;

        public AuthenLogController() {
            repo = new AdminAuthenLogRepository();
        }

        // GET: AuthenLog
        public ActionResult Index() {
            return View();
        }

        // GET: AuthenLog/DataTable
        public ActionResult DataTable() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();

            if (param.hasSearch) {
                whereC.Add(" (AdminId LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "AdminId", "LogIn", "LogOut", "PublicIP", "LocalIP" };
            DataTableData<AdminAuthenLog> data = repo.ListAdminAuthenLog_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.AdminId.ToString(),
                        x.Login.ToString("dd/MM/yyyy HH:mm"),
                        (x.Logout == DateTime.MinValue ? "" : x.Logout.ToString("dd/MM/yyyy HH:mm")),
                        x.PublicIP.ToString(),
                        x.LocalIP.ToString()
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}
