﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TobJod.Admin.Models;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class ProductApplicationFieldController : BaseController {
        ProductApplicationFieldRepository repo;

        public ProductApplicationFieldController() {
            repo = new ProductApplicationFieldRepository();
        }

        // GET: ProductApplicationField
        public ActionResult Index() {
            return View();
        }
        
        // GET: ProductApplicationField/Edit/5
        public ActionResult Edit(int id) {
            ProductApplicationField item = repo.GetProductApplicationField_Admin(id);
            BindForm();
            return View("Form", item);
        }

        // POST: ProductApplicationField/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, ProductApplicationField item) {
            int result = 0;
            try {
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = Admin.Id;

                result = repo.UpdateProductApplicationField(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            Notify_Update_Fail();
            BindForm();
            return View("Form", item);
        }
        
        // GET: ProductApplicationField/DataTable
        public ActionResult DataTable() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();

            if (!string.IsNullOrEmpty(Request.Form["category"])) {
                whereC.Add(" (ProductCategoryId=@ProductCategoryId) ");
                whereParams.ProductCategoryId = NullUtils.cvInt(Request.Form["category"]);
            }

            if (param.hasSearch) {
                whereC.Add(" (TitleTH LIKE @searchValue OR TitleEN LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            whereC.Add(" (Status=2) ");

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "ProductCategoryId", "TitleTH", "Step", "Sequence", "UpdateDate", "Id" };
            DataTableData<ProductApplicationField> data = repo.ListProductApplicationField_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.ProductCategoryId.ToString(),
                        x.Id.ToString(),
                        x.TitleTH.ToString(),
                        x.Step.ToString(),
                        x.Sequence.ToString(),
                        x.UpdateDate.ToString("dd/MM/yyyy HH:mm"),
                        (HtmlActionButton(AdminAction.Edit, (int)x.Id))
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        private void BindForm() {

        }
    }
}
