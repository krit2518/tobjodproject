﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web.Mvc;
using TobJod.Models;
using TobJod.Repositories;
using TobJod.Utils;

namespace TobJod.Admin.Controllers {
    public class LeadController : BaseController {
        LeadRepository repo;

        public LeadController() {
            repo = new LeadRepository();
        }

        // GET: Lead
        public ActionResult Index() {
            //Lead item = new Lead();
            //item.Category = NullUtils.cvInt(Request.QueryString["category"]);
            if (Request.Params["status"] != null) ViewBag.Status = ValueUtils.GetIntSplit(Request.Params["status"], new char[] { ',' }, true);
            if (Request.Params["source"] != null) ViewBag.Source = ValueUtils.GetIntSplit(Request.Params["source"], new char[] { ',' });
            DateTime dateFrom = ValueUtils.GetDate(Request.Unvalidated.QueryString["DateFrom"]);
            DateTime dateTo = ValueUtils.GetDate(Request.Unvalidated.QueryString["DateTo"]);
            if (dateFrom > DateUtils.SqlMinDate()) ViewBag.DateFrom = dateFrom.ToString("dd/MM/yyyy", cultureGB);
            if (dateTo > DateUtils.SqlMinDate()) ViewBag.DateTo = dateTo.ToString("dd/MM/yyyy", cultureGB);
            ViewBag.Category = NullUtils.cvInt(Request.QueryString["Category"]);
            if (Request.QueryString.AllKeys.Contains("export")) {
                Export();
            }
            return View();
        }

        // GET: Lead/Create
        public ActionResult Create() {
            Lead item = new Lead();
            item.Status = Status.Active;
            return View("Form", item);
        }

        // POST: Lead/Create
        [HttpPost]
        public ActionResult Create(Lead item) {
            try {
                item.CreateDate = item.UpdateDate = DateTime.Now;
                item.CreateAdminId = item.UpdateAdminId = Admin.Id;

                repo.AddLead(item);
                Notify_Add_Success();
                return RedirectToAction("Index");
            } catch { }
            Notify_Add_Fail();
            return View("Form", item);
        }

        // GET: Lead/Edit/5
        public ActionResult Edit(int id) {
            Lead item = repo.GetLead_Admin(id);
            item.LeadCriteria = GetLeadCriterialDetail(item.LeadCriteria, item.PremiumId);
            if (item.Remark == null) item.Remark = "";
            if (item.LeadStatus == LeadStatus.Pending) {
                item.LeadStatus = LeadStatus.Allocated;
                item.OwnerAdminId = Admin.Id;
                repo.UpdateLeadOwnerStatus(item);
            }
            if (item.FollowUpDate == DateUtils.SqlMinDate()) item.FollowUpDate = null;
            return View("Form", item);
        }

        // POST: Lead/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Lead item) {
            int result = 0;
            try {
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = Admin.Id;

                if (item.FollowUpDate == null) item.FollowUpDate = DateUtils.SqlMinDate();

                result = repo.UpdateLeadStatusLog(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            item.LeadCriteria = GetLeadCriterialDetail(item.LeadCriteria, item.PremiumId);
            Notify_Update_Fail();
            return View("Form", item);
        }

        // GET: Lead/Assign/5
        public ActionResult Assign(int id) {
            Lead item = repo.GetLead_Admin(id);
            item.LeadCriteria = GetLeadCriterialDetail(item.LeadCriteria, item.PremiumId);
            AdminRepository adminRepo = new AdminRepository();
            ViewBag.OwnerAdminList = adminRepo.ListAdminByPermission(Menu.Module, AdminAction.Edit).Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() }).ToList();
            return View("View", item);
        }

        // POST: Lead/Assign/5
        [HttpPost]
        public ActionResult Assign(int id, Lead item) {
            int result = 0;
            try {
                item.UpdateDate = DateTime.Now;
                item.UpdateAdminId = Admin.Id;
                item.AssignAdminId = Admin.Id;

                result = repo.UpdateLeadOwner(item);
                if (result > 0) {
                    Notify_Update_Success();
                    return RedirectToAction("Index");
                }
            } catch { }
            item.LeadCriteria = GetLeadCriterialDetail(item.LeadCriteria, item.PremiumId);
            AdminRepository adminRepo = new AdminRepository();
            ViewBag.OwnerAdminList = adminRepo.ListAdminByPermission(Menu.Module, AdminAction.Edit).Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() }).ToList();
            Notify_Update_Fail();
            return View("View", item);
        }

        // GET: Lead/View/5
        public ActionResult View(int id) {
            Lead item = repo.GetLead_Admin(id);
            item.LeadCriteria = GetLeadCriterialDetail(item.LeadCriteria, item.PremiumId);
            return View("View", item);
        }

        // GET: Lead/Delete/5
        public ActionResult Delete(int id) {
            var result = repo.DeleteLead(id, Admin.Id);
            if (result > 0) {
                Notify_Delete_Success();
            } else {
                Notify_Delete_Fail();
            }
            return RedirectToAction("Index");
        }

        // GET: Lead/Export
        public void Export() {
            NameValueCollection param = new NameValueCollection();
            List<string> whereC = new List<string>();

            var status = ValueUtils.GetIntSplit(Request.Params["status"], new char[] { ',' }, true);
            whereC.Add(" (LeadStatus IN (" + string.Join(",", status) + ")) ");

            var source = ValueUtils.GetIntSplit(Request.Params["source"], new char[] { ',' });
            whereC.Add(" (Source IN (" + string.Join(",", source) + ")) ");

            int Category = NullUtils.cvInt(Request.Params["Category"]);
            if (Category > 0) {
                whereC.Add(" p.SubCategoryId=@Category ");
                param.Add("Category", Category.ToString());
            }

            DateTime dateFrom = ValueUtils.GetDate(Request.Params["DateFrom"]);
            DateTime dateTo = ValueUtils.GetDate(Request.Params["DateTo"]);

            if (dateFrom > DateUtils.SqlMinDate()) {
                whereC.Add(" (t.CreateDate >= @DateFrom)");
                param.Add("DateFrom", dateFrom.ToString("yyyy-MM-dd", cultureGB));
            }

            if (dateTo > DateUtils.SqlMinDate()) {
                dateTo = ValueUtils.ExtendToEndOfDate(dateTo);
                whereC.Add(" (t.CreateDate <= @DateTo)");
                param.Add("DateTo", dateFrom.ToString("yyyy-MM-dd" + " 23:59:59", cultureGB));
            }

            if (source.Length == 0) return;

            string where = string.Empty;
            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            var dt = repo.ListLead_Export(where + " AND Source=" + (int)LeadSource.ContactUs, param);
            var dtForm = repo.ListLead_Export(where + " AND Source=" + (int)LeadSource.Product, param);
            ExportFileMultipleSheet("Lead.xlsx", new string[] { "Contact Us", "Contact Form" }, new System.Data.DataTable[] { dt, dtForm }, null, null, null, new string[] { "CreateDate", "CallBackDate" });
        }

        // GET: Lead/DataTable
        public ActionResult DataTable() {
            DataTableParameter param = GetDataTableParameter();

            string where = string.Empty;
            dynamic whereParams = new System.Dynamic.ExpandoObject();
            List<string> whereC = new List<string>();

            //if (!Menu.Can_Assign) { //see only own content if not approver/assigner
            //    whereC.Add(" (OwnerAdminId=@ownerAdminId OR OwnerAdminId='') ");
            //    whereParams.ownerAdminId = Admin.Id;
            //}
            DateTime dateFrom = ValueUtils.GetDate(Request.Unvalidated.Form["DateFrom"]);
            DateTime dateTo = ValueUtils.GetDate(Request.Unvalidated.Form["DateTo"]);
            if (dateFrom > DateUtils.SqlMinDate()) {
                whereC.Add(" (t.CreateDate >= @DateFrom)");
                whereParams.DateFrom = dateFrom;
            }

            if (dateTo > DateUtils.SqlMinDate()) {
                dateTo = ValueUtils.ExtendToEndOfDate(dateTo);
                whereC.Add(" (t.CreateDate <= @DateTo)");
                whereParams.DateTo = dateTo;
            }

            if (param.hasSearch) {
                whereC.Add(" (Name LIKE @searchValue OR Surname LIKE @searchValue OR Tel LIKE @searchValue OR Email LIKE @searchValue OR Product LIKE @searchValue) ");
                whereParams.searchValue = "%" + param.searchValue + "%";
            }

            var status = ValueUtils.GetIntSplit(Request.Unvalidated.Form["status"], new char[] { ',' }, true);
            whereC.Add(" (LeadStatus IN @LeadStatus) ");
            whereParams.LeadStatus = status;

            var source = ValueUtils.GetIntSplit(Request.Unvalidated.Form["source"], new char[] { ',' });
            whereC.Add(" (Source IN @Source) ");
            whereParams.Source = source;

            int Category = NullUtils.cvInt(Request.Params["Category"]);
            if (Category > 0) {
                whereC.Add(" ProductSubCategoryId=@Category ");
                whereParams.Category = Category.ToString();
            }

            if (whereC.Count > 0) where = "WHERE " + string.Join(" AND ", whereC);

            string[] columns = new string[] { "Id", "CreateDate", "CallBackDate", "FollowUpDate", "Name", "Surname", "Tel", "Email", "SubjectCategory", "Subject", "ProductCategory", "Product", "Message", "LeadStatus", "Remark", "OwnerAdmin", "Source" };
            DataTableData<Lead> data = repo.ListLead_DataTable(columns, where, whereParams, param.sortColumn, param.sortDirection, param.start, param.length);
            data.draw = param.draw;
            data.data = data.dataRaw.Select((x) =>
                    new string[] {
                        x.Id.ToString(),
                        x.CreateDate.ToString("dd/MM/yyyy HH:mm"),
                        x.CallBackDate.ToString("dd/MM/yyyy HH:mm") + " - " + x.CallBackDate.AddHours(2).ToString("HH:mm"),
                        (x.FollowUpDate.Value.Year > 2000 ? x.FollowUpDate.Value.ToString("dd/MM/yyyy") : ""),
                        x.Name.ToString(),
                        x.Surname.ToString(),
                        x.Tel.ToString(),
                        x.Email.ToString(),
                        x.SubjectCategory.ToString(),
                        x.Subject.ToString(),
                        x.ProductCategory.ToString(),
                        x.Product.ToString(),
                        x.Message.ToString(),
                        x.LeadStatus.GetDisplayName(),
                        x.Remark.ToString(),
                        x.OwnerAdmin.ToString(),
                        x.Source.GetDisplayName(),
                        (HtmlActionButton(AdminAction.Edit, x.Id) + HtmlActionButton(AdminAction.View, x.Id) + HtmlActionButton(AdminAction.Assign, x.Id))
                    }).ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        private LeadCriteria GetLeadCriterialDetail(LeadCriteria item, int premiumId) {
            if (item == null) return null;
            item.Category = ProductCategory.GetCategoryBySubCategory(item.SubCategoryId);
            CareerRepository careerRepo = new CareerRepository();
            ProductRepository productRepo = new ProductRepository();
            //ProvinceRepository provinceRepo = new ProvinceRepository();
            MasterOptionRepository masterRepo = new MasterOptionRepository();
            switch (item.Category) {
                case ProductCategoryKey.Motor:
                    CarRepository carRepo = new CarRepository();
                    item.MotorBrandIdTitle = carRepo.GetCarBrand_Admin(item.MotorBrandId)?.TitleTH;
                    item.MotorFamilyIdTitle = carRepo.GetCarFamily_Admin(item.MotorFamilyId)?.TitleTH;
                    item.MotorModelIdTitle = carRepo.GetCarModel_Admin(item.MotorModelId)?.TitleTH;
                    //try { item.MotorUsageTitle = ((ProductMotorVehicleUsage)item.MotorUsage).ToString(); } catch { }
                    AllYesNoFlag cctv = NullUtils.cvEnum<AllYesNoFlag>(item.MotorCCTV, AllYesNoFlag.None);
                    item.MotorCCTVTitle = (cctv.HasFlag(AllYesNoFlag.All) ? "ทั้งหมด" : (cctv.HasFlag(AllYesNoFlag.Yes) ? "มี" : (cctv.HasFlag(AllYesNoFlag.No) ? "ไม่มี" : "")));  //(item.MotorCCTV ? "Y" : "N");
                    item.MotorPRBTitle = (item.MotorPRB ? "Y" : "N");
                    item.MotorAccessoryTitle = (item.MotorAccessory ? "Y" : "N");
                    AllYesNoFlag garage = NullUtils.cvEnum<AllYesNoFlag>(item.MotorGarage, AllYesNoFlag.None);
                    item.MotorGarageTitle = (cctv.HasFlag(AllYesNoFlag.All) ? "ทั้งหมด" : (cctv.HasFlag(AllYesNoFlag.Yes) ? "อู่ห้าง" : (cctv.HasFlag(AllYesNoFlag.No) ? "อู่ทั่วไป" : "")));
                    item.MotorRegisterProvinceIdTitle = masterRepo.GetMasterOption_Admin(item.MotorRegisterProvinceId)?.TitleTH;
                    try { item.PremiumPrice = productRepo.GetProductMotorPremium_Admin(premiumId.ToString()).Premium; } catch { }

                    AllYesNoFlag driverAge = NullUtils.cvEnum<AllYesNoFlag>(item.MotorDriverAge, AllYesNoFlag.None);
                    item.MotorDriverAgeTitle = (driverAge.HasFlag(AllYesNoFlag.All) ? "ทั้งหมด" : (driverAge.HasFlag(AllYesNoFlag.No) ? "ไม่ระบุ" : (driverAge.HasFlag(AllYesNoFlag.Yes) ? "ระบุ" : "")));  //(item.MotorCCTV ? "Y" : "N");
                    if (driverAge.HasFlag(AllYesNoFlag.Yes) && item.MotorDriverAgeId > 0) { item.MotorDriverAgeIdTitle = masterRepo.GetMasterOption_Admin(item.MotorDriverAgeId)?.TitleTH; }
                    break;
                case ProductCategoryKey.Home:
                    item.HousePropertyTypeTitle = masterRepo.GetMasterOption_Admin(item.HousePropertyType)?.TitleTH;
                    item.HouseConstructionTypeTitle = masterRepo.GetMasterOption_Admin(item.HouseConstructionType)?.TitleTH;
                    try { item.PremiumPrice = productRepo.GetProductHomePremium_Admin(premiumId.ToString()).Premium; } catch { }
                    break;
                case ProductCategoryKey.PA:
                    item.CareerIdTitle = careerRepo.GetCareer_Admin(item.CareerId)?.TitleTH;
                    try { item.PremiumPrice = productRepo.GetProductPAPremium_Admin(premiumId.ToString()).Premium; } catch { }
                    break;
                case ProductCategoryKey.PH:
                    item.CareerIdTitle = careerRepo.GetCareer_Admin(item.CareerId)?.TitleTH;
                    try { item.SexTitle = ((Sex)item.Sex).DisplayNameOrDefault(); } catch { }
                    try { item.PremiumPrice = productRepo.GetProductPHPremium_Admin(premiumId.ToString()).Premium; } catch { }
                    break;
                case ProductCategoryKey.TA:
                    CountryRepository countryRepo = new CountryRepository();
                    try { item.TATripTypeTitle = ((ProductTATripType)item.TATripType).GetDisplayName(); } catch { }
                    try { item.TACoverageOptionTitle = ((ProductTACoverageOption)item.TACoverageOption).GetDisplayName(); } catch { }
                    try { item.TACoverageTypeTitle = ((ProductTACoverageType)item.TACoverageType).GetDisplayName(); } catch { }
                    try { item.TAZoneTitle = ((CountryZone)item.TAZone).GetDisplayName(); } catch { }
                    item.TACountryIdTitle = countryRepo.GetCountry_Admin(item.TACountryId)?.TitleTH;
                    try { item.PremiumPrice = productRepo.GetProductTAPremium_Admin(premiumId.ToString()).Premium; } catch { }
                    break;
                case ProductCategoryKey.PRB:
                    try {
                        var prb = productRepo.GetProductPRB_Active(Language.TH, premiumId);
                        if (prb != null) {
                            item.CompanyTitle = prb.Company;
                            item.PremiumPrice = prb.ProductPRB.Premium;
                        }
                    } catch { }
                    break;
            }

            return item;
        }

    }
}
