﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TobJod.Models;

namespace TobJod.Admin.Models {
    public class PartnerDataExportViewModel : ReportViewModel {
        [Display(Name = "Status")]
        public int Status { get; set; }
        public List<SelectListItem> StatusList { get; set; }
        [Display(Name = "Company")]
        public int Company { get; set; }
        public int Category { get; set; }
        public List<SelectListItem> Companies { get; set; }
    }
}