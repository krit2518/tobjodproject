﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using TobJod.Models;

namespace TobJod.Admin.Models {
    public class CampaignViewModel : Campaign {
        [Display(Name = "Image (TH) [jpg,png,gif. Size<=2MB]")]
        [JsonIgnore]
        public HttpPostedFileBase ImageTHFile { get; set; }
        [Display(Name = "Image (EN) [jpg,png,gif. Size<=2MB]")]
        [JsonIgnore]
        public HttpPostedFileBase ImageENFile { get; set; }

        public static CampaignViewModel Map(Campaign baseObject) {
            var serializedParent = JsonConvert.SerializeObject(baseObject);
            return JsonConvert.DeserializeObject<CampaignViewModel>(serializedParent);
        }
        
    }
}