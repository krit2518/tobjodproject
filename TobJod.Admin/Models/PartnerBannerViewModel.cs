﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TobJod.Models;

namespace TobJod.Admin.Models {
    public class PartnerBannerViewModel : PartnerBanner {
        [Display(Name = "Image (TH) 250x80 [jpg,png,gif. Size<=2MB]")] //250x80
        [JsonIgnore]
        public HttpPostedFileBase ImageTHFile { get; set; }
        [Display(Name = "Image (EN) 250x80 [jpg,png,gif. Size<=2MB]")]
        [JsonIgnore]
        public HttpPostedFileBase ImageENFile { get; set; }

        public static PartnerBannerViewModel Map(PartnerBanner baseObject) {
            var serializedParent = JsonConvert.SerializeObject(baseObject);
            return JsonConvert.DeserializeObject<PartnerBannerViewModel>(serializedParent);
        }
        
    }
}