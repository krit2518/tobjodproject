﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TobJod.Models;

namespace TobJod.Admin.Models {
    public class NewsImageViewModel : NewsImage {
        [Display(Name = "Image [jpg,png,gif. Size<=2MB]")]
        public HttpPostedFileBase ImageFile { get; set; }
        
        public News News { get; set; }
    }
}