﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TobJod.Models;

namespace TobJod.Admin.Models {
    public class CompanyViewModel : Company {
        [Display(Name = "Image (TH) 338x338 [jpg,png,gif. Size<=2MB]")]
        [JsonIgnore]
        public HttpPostedFileBase ImageTHFile { get; set; }
        [Display(Name = "Image (EN) 338x338 [jpg,png,gif. Size<=2MB]")]
        [JsonIgnore]
        public HttpPostedFileBase ImageENFile { get; set; }

        public static CompanyViewModel Map(Company baseObject) {
            var serializedParent = JsonConvert.SerializeObject(baseObject);
            return JsonConvert.DeserializeObject<CompanyViewModel>(serializedParent);
        }
        
    }
}