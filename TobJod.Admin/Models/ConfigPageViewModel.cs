﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TobJod.Models;

namespace TobJod.Admin.Models {
    public class ConfigPageViewModel : ConfigPage {
        [JsonIgnore]
        public HttpPostedFileBase OGImageTHFile { get; set; }
        [JsonIgnore]
        public HttpPostedFileBase OGImageENFile { get; set; }
        [Display(Name = "OG Image (TH) [jpg,png,gif. Size<=2MB]")]
        public bool OGImageTHSame { get; set; }
        [Display(Name = "OG Image (EN) [jpg,png,gif. Size<=2MB]")]
        public bool OGImageENSame { get; set; }

        [JsonIgnore]
        public ConfigPage CurrentPage { get; set; }

        public static ConfigPageViewModel Map(ConfigPage baseObject) {
            var serializedParent = JsonConvert.SerializeObject(baseObject);
            return JsonConvert.DeserializeObject<ConfigPageViewModel>(serializedParent);
        }
    }
}