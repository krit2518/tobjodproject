﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TobJod.Models;

namespace TobJod.Admin.Models {
    public class PaymentMethodViewModel : PaymentMethod {
        [Display(Name = "Url (TH)"), MaxLength(250), Required]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        [Remote("ValidateUrlTH", "PaymentMethod", AdditionalFields = "Id", HttpMethod = "POST", ErrorMessage = "URL already exists.")]
        public new string UrlTH { get; set; }
        [Display(Name = "Url (EN)"), MaxLength(250), Required]
        [RegularExpression("^[^<>;~]+$", ErrorMessage = "Special character (^ < > ~ ;) should not be entered")]
        [Remote("ValidateUrlEN", "PaymentMethod", AdditionalFields = "Id", HttpMethod = "POST", ErrorMessage = "URL already exists.")]
        public new string UrlEN { get; set; }

        [Display(Name = "Image (TH) [jpg,png,gif. Size<=2MB]")]
        public HttpPostedFileBase ImageTHFile { get; set; }
        [Display(Name = "Image (EN) [jpg,png,gif. Size<=2MB]")]
        public HttpPostedFileBase ImageENFile { get; set; }
        [Display(Name = "OG Image (TH) [jpg,png,gif. Size<=2MB]")]
        public HttpPostedFileBase OGImageTHFile { get; set; }
        [Display(Name = "OG Image (EN) [jpg,png,gif. Size<=2MB]")]
        public HttpPostedFileBase OGImageENFile { get; set; }
        [Display(Name = "OG Image (TH) [jpg,png,gif. Size<=2MB]")]
        public bool OGImageTHSame { get; set; }
        [Display(Name = "OG Image (EN) [jpg,png,gif. Size<=2MB]")]
        public bool OGImageENSame { get; set; }

        public static PaymentMethodViewModel Map(PaymentMethod baseObject) {
            var serializedParent = JsonConvert.SerializeObject(baseObject);
            return JsonConvert.DeserializeObject<PaymentMethodViewModel>(serializedParent);
        }
        
    }
}