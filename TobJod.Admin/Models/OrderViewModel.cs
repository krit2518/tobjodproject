﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TobJod.Admin.Models
{
    public class OrderViewModel : TobJod.Models.Order
    {
        public HttpPostedFileBase IDCardFile { get; set; }
        public HttpPostedFileBase CCTVFile { get; set; }
        public HttpPostedFileBase CarRegistrationCopyFile { get; set; }
        public HttpPostedFileBase File1 { get; set; }
        public HttpPostedFileBase File2 { get; set; }
        public HttpPostedFileBase File3 { get; set; }
        public HttpPostedFileBase File4 { get; set; }
        public HttpPostedFileBase File5 { get; set; }
        public HttpPostedFileBase File6 { get; set; }

        //public TobJod.Models.ProductSubCategoryKey ProductSubCategoryId { get; set; }
        public static OrderViewModel Map(TobJod.Models.Order baseObject)
        {
            var serializedParent = JsonConvert.SerializeObject(baseObject);
            return JsonConvert.DeserializeObject<OrderViewModel>(serializedParent);
        }
    }
}