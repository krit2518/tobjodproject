﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TobJod.Models;

namespace TobJod.Admin.Models {
    public class PopupViewModel : Popup {
        [Display(Name = "Image (TH) [jpg,png,gif. Size<=2MB]")]
        public HttpPostedFileBase ImageTHFile { get; set; }
        [Display(Name = "Image (EN) [jpg,png,gif. Size<=2MB]")]
        public HttpPostedFileBase ImageENFile { get; set; }

        public static PopupViewModel Map(Popup baseObject) {
            var serializedParent = JsonConvert.SerializeObject(baseObject);
            return JsonConvert.DeserializeObject<PopupViewModel>(serializedParent);
        }
        
    }
}