﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;
using TobJod.Models;

namespace TobJod.Admin.Models {
    public class TransactionLogViewModel {
        [Display(Name = "Category"), Required]
        public string Category { get; set; }
        [Display(Name = "From"), DataType(DataType.Date)]
        public DateTime DateFrom { get; set; }
        [Display(Name = "To"), DataType(DataType.Date)]
        public DateTime DateTo { get; set; }
        public DataTable DataTable { get; set; }

        public List<string> Columns { get; set; }
        public string[] TableColumn { get; set; }
        public string Table { get; set; }

        public TransactionLogViewModel() {
            Columns = new List<string>();
            TableColumn = new string[] { };
        }
    }
}