﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;
using TobJod.Models;

namespace TobJod.Admin.Models {
    public class ReportProductCompareViewModel : ReportViewModel {
        [Display(Name = "Category")]
        public ProductSubCategoryKey Category { get; set; }
    }
}