﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TobJod.Models;
using TobJod.Utils;

namespace TobJod.Admin.Models {
    public class ConfigViewModel {
        [Range(1000, 1000000, ErrorMessage = "Please enter a number between 1000 and 1000000."), Required(ErrorMessage = "Please enter  a number between 1000 and 1000000.")]
        public int BannerDuration { get; set; }
        [Required]
        public string BannerTransition { get; set; }

        [Range(0, 100000, ErrorMessage = "Please enter a number between 0 and 100000."), Required(ErrorMessage = "Please enter  a number between 0 and 100000.")]
        public int ProductMotorPriceMin { get; set; }
        [Range(0, 100000, ErrorMessage = "Please enter a number between 0 and 100000."), Required(ErrorMessage = "Please enter  a number between 0 and 100000.")]
        public int ProductMotorPriceMax { get; set; }

        //[Range(0, 100000), Required]
        //public int ProductHomePriceMin { get; set; }
        //[Range(0, 100000), Required]
        //public int ProductHomePriceMax { get; set; }
        [Range(0, 1000, ErrorMessage = "Please enter a number between 0 and 1000."), Required(ErrorMessage = "Please enter  a number between 0 and 1000.")]
        public int ProductHomeYearMax { get; set; }

        [Range(0, 100000, ErrorMessage = "Please enter a number between 0 and 100000."), Required(ErrorMessage = "Please enter  a number between 0 and 100000.")]
        public int ProductPAPriceMin { get; set; }
        [Range(0, 100000, ErrorMessage = "Please enter a number between 0 and 100000."), Required(ErrorMessage = "Please enter  a number between 0 and 100000.")]
        public int ProductPAPriceMax { get; set; }

        [Range(0, 100000, ErrorMessage = "Please enter a number between 0 and 100000."), Required(ErrorMessage = "Please enter  a number between 0 and 100000.")]
        public int ProductPHPriceMin { get; set; }
        [Range(0, 100000, ErrorMessage = "Please enter a number between 0 and 100000."), Required(ErrorMessage = "Please enter  a number between 0 and 100000.")]
        public int ProductPHPriceMax { get; set; }

        [Range(0, 100000, ErrorMessage = "Please enter a number between 0 and 100000."), Required(ErrorMessage = "Please enter  a number between 0 and 100000.")]
        public int ProductTAPriceMin { get; set; }
        [Range(0, 100000, ErrorMessage = "Please enter a number between 0 and 100000."), Required(ErrorMessage = "Please enter  a number between 0 and 100000.")]
        public int ProductTAPriceMax { get; set; }
        [Range(0, 1000, ErrorMessage = "Please enter a number between 0 and 1000."), Required(ErrorMessage = "Please enter  a number between 0 and 1000.")]
        public int ProductTAPersonMax { get; set; }

        [RegularExpression("^([\\w+-.%]+@[\\w-.]+\\.[A-Za-z]{2,4};?)+$", ErrorMessage = "Please enter Email address (for muliple addresses, separate by ';' without space)"), Required]
        public string ProductEmailTo { get; set; }
        [DataType(DataType.MultilineText)]
        public string ContactAddressTH { get; set; }
        [DataType(DataType.MultilineText)]
        public string ContactAddressEN { get; set; }
        public string ContactTelephoneTH { get; set; }
        public string ContactTelephoneEN { get; set; }
        public string ContactFaxTH { get; set; }
        public string ContactFaxEN { get; set; }
        public string ContactEmailTH { get; set; }
        public string ContactEmailEN { get; set; }

        [Range(0, 1000, ErrorMessage = "Please enter a number between 0 and 1000."), Required(ErrorMessage = "Please enter  a number between 0 and 1000.")]
        public int PaymentExpireDay { get; set; }

        [Required]
        public int ProductLayout { get; set; }

        public string OGImage { get; set; }

        public string EmailImageHeader { get; set; }
        public string EmailImageFooter { get; set; }

        //public bool EnableChaptcha { get; set; }

        public HttpPostedFileBase OGImageFile { get; set; }
        public HttpPostedFileBase EmailImageHeaderFile { get; set; }
        public HttpPostedFileBase EmailImageFooterFile { get; set; }

        //public Dictionary<ConfigName, Config> Configs { get; set; }

        public ConfigViewModel() { }

        public ConfigViewModel(Dictionary<ConfigName, Config> items) {
            //Configs = items;
            OGImage = NullUtils.cvString(items[ConfigName.OGImage].Body);

            BannerDuration = NullUtils.cvInt(items[ConfigName.BannerDuration].Body);
            BannerTransition = NullUtils.cvString(items[ConfigName.BannerTransition].Body);

            ProductLayout = NullUtils.cvInt(items[ConfigName.ProductLayout].Body);
            ProductMotorPriceMin = NullUtils.cvInt(items[ConfigName.ProductMotorPriceMin].Body);
            ProductMotorPriceMax = NullUtils.cvInt(items[ConfigName.ProductMotorPriceMax].Body);
            //ProductHomePriceMin = NullUtils.cvInt(items[ConfigName.ProductHomePriceMin].Body);
            //ProductHomePriceMax = NullUtils.cvInt(items[ConfigName.ProductHomePriceMax].Body);
            ProductPAPriceMin = NullUtils.cvInt(items[ConfigName.ProductPAPriceMin].Body);
            ProductPAPriceMax = NullUtils.cvInt(items[ConfigName.ProductPAPriceMax].Body);
            ProductPHPriceMin = NullUtils.cvInt(items[ConfigName.ProductPHPriceMin].Body);
            ProductPHPriceMax = NullUtils.cvInt(items[ConfigName.ProductPHPriceMax].Body);
            ProductTAPriceMin = NullUtils.cvInt(items[ConfigName.ProductTAPriceMin].Body);
            ProductTAPriceMax = NullUtils.cvInt(items[ConfigName.ProductTAPriceMax].Body);
            ProductTAPersonMax = NullUtils.cvInt(items[ConfigName.ProductTAPersonMax].Body);
            ProductHomeYearMax = NullUtils.cvInt(items[ConfigName.ProductHomeYearMax].Body);

            ProductEmailTo = NullUtils.cvString(items[ConfigName.ProductEmailTo].Body);

            ContactAddressTH = NullUtils.cvString(items[ConfigName.ContactAddressTH].Body);
            ContactAddressEN = NullUtils.cvString(items[ConfigName.ContactAddressEN].Body);
            ContactTelephoneTH = NullUtils.cvString(items[ConfigName.ContactTelephoneTH].Body);
            ContactTelephoneEN = NullUtils.cvString(items[ConfigName.ContactTelephoneEN].Body);
            ContactFaxTH = NullUtils.cvString(items[ConfigName.ContactFaxTH].Body);
            ContactFaxEN = NullUtils.cvString(items[ConfigName.ContactFaxEN].Body);
            ContactEmailTH = NullUtils.cvString(items[ConfigName.ContactEmailTH].Body);
            ContactEmailEN = NullUtils.cvString(items[ConfigName.ContactEmailEN].Body);

            EmailImageHeader = NullUtils.cvString(items[ConfigName.EmailImageHeader].Body);
            EmailImageFooter = NullUtils.cvString(items[ConfigName.EmailImageFooter].Body);

            PaymentExpireDay = NullUtils.cvInt(items[ConfigName.PaymentExpireDay].Body);
            

        }

        public List<Config> ToList() {
            List<Config> items = new List<Config>();
            items.Add(new Config() { Id = ConfigName.OGImage, Body = OGImage.ToString() });

            items.Add(new Config() { Id = ConfigName.BannerDuration, Body = BannerDuration.ToString() });
            items.Add(new Config() { Id = ConfigName.BannerTransition, Body = BannerTransition.ToString() });

            items.Add(new Config() { Id = ConfigName.ProductLayout, Body = ProductLayout.ToString() });
            items.Add(new Config() { Id = ConfigName.ProductMotorPriceMin, Body = ProductMotorPriceMin.ToString()});
            items.Add(new Config() { Id = ConfigName.ProductMotorPriceMax, Body = ProductMotorPriceMax.ToString()});
            //items.Add(new Config() { Id = ConfigName.ProductHomePriceMin, Body = ProductHomePriceMin.ToString() });
            //items.Add(new Config() { Id = ConfigName.ProductHomePriceMax, Body = ProductHomePriceMax.ToString() });
            items.Add(new Config() { Id = ConfigName.ProductPAPriceMin, Body = ProductPAPriceMin.ToString() });
            items.Add(new Config() { Id = ConfigName.ProductPAPriceMax, Body = ProductPAPriceMax.ToString() });
            items.Add(new Config() { Id = ConfigName.ProductPHPriceMin, Body = ProductPHPriceMin.ToString() });
            items.Add(new Config() { Id = ConfigName.ProductPHPriceMax, Body = ProductPHPriceMax.ToString() });
            items.Add(new Config() { Id = ConfigName.ProductTAPriceMin, Body = ProductTAPriceMin.ToString() });
            items.Add(new Config() { Id = ConfigName.ProductTAPriceMax, Body = ProductTAPriceMax.ToString() });
            items.Add(new Config() { Id = ConfigName.ProductTAPersonMax, Body = ProductTAPersonMax.ToString() });
            items.Add(new Config() { Id = ConfigName.ProductHomeYearMax, Body = ProductHomeYearMax.ToString() });

            items.Add(new Config() { Id = ConfigName.ProductEmailTo, Body = ProductEmailTo.ToString() });
            items.Add(new Config() { Id = ConfigName.ContactAddressTH, Body = ContactAddressTH.ToString() });
            items.Add(new Config() { Id = ConfigName.ContactAddressEN, Body = ContactAddressEN.ToString() });
            items.Add(new Config() { Id = ConfigName.ContactTelephoneTH, Body = ContactTelephoneTH.ToString() });
            items.Add(new Config() { Id = ConfigName.ContactTelephoneEN, Body = ContactTelephoneEN.ToString() });
            items.Add(new Config() { Id = ConfigName.ContactFaxTH, Body = ContactFaxTH.ToString() });
            items.Add(new Config() { Id = ConfigName.ContactFaxEN, Body = ContactFaxEN.ToString() });
            items.Add(new Config() { Id = ConfigName.ContactEmailTH, Body = ContactEmailTH.ToString() });
            items.Add(new Config() { Id = ConfigName.ContactEmailEN, Body = ContactEmailEN.ToString() });

            items.Add(new Config() { Id = ConfigName.EmailImageHeader, Body = EmailImageHeader.ToString() });
            items.Add(new Config() { Id = ConfigName.EmailImageFooter, Body = EmailImageFooter.ToString() });

            items.Add(new Config() { Id = ConfigName.PaymentExpireDay, Body = PaymentExpireDay.ToString() });

            return items;
        }
        
    }
}