﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;
using TobJod.Models;

namespace TobJod.Admin.Models {
    public class ReportViewModel {
        [Display(Name = "From"), DataType(DataType.Date)]
        public DateTime DateFrom { get; set; }
        [Display(Name = "To"), DataType(DataType.Date)]
        public DateTime DateTo { get; set; }
        public DataTable DataTable { get; set; }
    }
}