﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TobJod.Models;
using TobJod.Utils;

namespace TobJod.Admin.Models {
    public class HomePromotionViewModel {
        public string HomePromotion1ImageTH { get; set; }
        public string HomePromotion2ImageTH { get; set; }
        public string HomePromotion3ImageTH { get; set; }
        public string HomePromotion4ImageTH { get; set; }
        [MaxLength(250)]
        public string HomePromotion1UrlTH { get; set; }
        [MaxLength(250)]
        public string HomePromotion2UrlTH { get; set; }
        [MaxLength(250)]
        public string HomePromotion3UrlTH { get; set; }
        [MaxLength(250)]
        public string HomePromotion4UrlTH { get; set; }
        public string HomePromotion1ImageEN { get; set; }
        public string HomePromotion2ImageEN { get; set; }
        public string HomePromotion3ImageEN { get; set; }
        public string HomePromotion4ImageEN { get; set; }
        [MaxLength(250)]
        public string HomePromotion1UrlEN { get; set; }
        [MaxLength(250)]
        public string HomePromotion2UrlEN { get; set; }
        [MaxLength(250)]
        public string HomePromotion3UrlEN { get; set; }
        [MaxLength(250)]
        public string HomePromotion4UrlEN { get; set; }
        public bool HomePromotion1TargetNew { get; set; }
        public bool HomePromotion2TargetNew { get; set; }
        public bool HomePromotion3TargetNew { get; set; }
        public bool HomePromotion4TargetNew { get; set; }
        public string HomePromotion1ImageMobileTH { get; set; }
        public string HomePromotion2ImageMobileTH { get; set; }
        public string HomePromotion3ImageMobileTH { get; set; }
        public string HomePromotion4ImageMobileTH { get; set; }
        public string HomePromotion1ImageMobileEN { get; set; }
        public string HomePromotion2ImageMobileEN { get; set; }
        public string HomePromotion3ImageMobileEN { get; set; }
        public string HomePromotion4ImageMobileEN { get; set; }



        public HttpPostedFileBase HomePromotion1ImageTHFile { get; set; }
        public HttpPostedFileBase HomePromotion2ImageTHFile { get; set; }
        public HttpPostedFileBase HomePromotion3ImageTHFile { get; set; }
        public HttpPostedFileBase HomePromotion4ImageTHFile { get; set; }
        public HttpPostedFileBase HomePromotion1ImageENFile { get; set; }
        public HttpPostedFileBase HomePromotion2ImageENFile { get; set; }
        public HttpPostedFileBase HomePromotion3ImageENFile { get; set; }
        public HttpPostedFileBase HomePromotion4ImageENFile { get; set; }

        public HttpPostedFileBase HomePromotion1ImageMobileTHFile { get; set; }
        public HttpPostedFileBase HomePromotion2ImageMobileTHFile { get; set; }
        public HttpPostedFileBase HomePromotion3ImageMobileTHFile { get; set; }
        public HttpPostedFileBase HomePromotion4ImageMobileTHFile { get; set; }
        public HttpPostedFileBase HomePromotion1ImageMobileENFile { get; set; }
        public HttpPostedFileBase HomePromotion2ImageMobileENFile { get; set; }
        public HttpPostedFileBase HomePromotion3ImageMobileENFile { get; set; }
        public HttpPostedFileBase HomePromotion4ImageMobileENFile { get; set; }

        //public Dictionary<ConfigName, Config> Configs { get; set; }

        public HomePromotionViewModel() { }

        public HomePromotionViewModel(Dictionary<ConfigName, Config> items) {
            //Configs = items;
            HomePromotion1ImageTH = NullUtils.cvString(items[ConfigName.HomePromotion1ImageTH].Body);
            HomePromotion2ImageTH = NullUtils.cvString(items[ConfigName.HomePromotion2ImageTH].Body);
            HomePromotion3ImageTH = NullUtils.cvString(items[ConfigName.HomePromotion3ImageTH].Body);
            HomePromotion4ImageTH = NullUtils.cvString(items[ConfigName.HomePromotion4ImageTH].Body);
            HomePromotion1UrlTH = NullUtils.cvString(items[ConfigName.HomePromotion1UrlTH].Body);
            HomePromotion2UrlTH = NullUtils.cvString(items[ConfigName.HomePromotion2UrlTH].Body);
            HomePromotion3UrlTH = NullUtils.cvString(items[ConfigName.HomePromotion3UrlTH].Body);
            HomePromotion4UrlTH = NullUtils.cvString(items[ConfigName.HomePromotion4UrlTH].Body);
            HomePromotion1ImageEN = NullUtils.cvString(items[ConfigName.HomePromotion1ImageEN].Body);
            HomePromotion2ImageEN = NullUtils.cvString(items[ConfigName.HomePromotion2ImageEN].Body);
            HomePromotion3ImageEN = NullUtils.cvString(items[ConfigName.HomePromotion3ImageEN].Body);
            HomePromotion4ImageEN = NullUtils.cvString(items[ConfigName.HomePromotion4ImageEN].Body);
            HomePromotion1UrlEN = NullUtils.cvString(items[ConfigName.HomePromotion1UrlEN].Body);
            HomePromotion2UrlEN = NullUtils.cvString(items[ConfigName.HomePromotion2UrlEN].Body);
            HomePromotion3UrlEN = NullUtils.cvString(items[ConfigName.HomePromotion3UrlEN].Body);
            HomePromotion4UrlEN = NullUtils.cvString(items[ConfigName.HomePromotion4UrlEN].Body);
            HomePromotion1TargetNew = string.Equals(NullUtils.cvString(items[ConfigName.HomePromotion1Target].Body), "_blank");
            HomePromotion2TargetNew = string.Equals(NullUtils.cvString(items[ConfigName.HomePromotion2Target].Body), "_blank");
            HomePromotion3TargetNew = string.Equals(NullUtils.cvString(items[ConfigName.HomePromotion3Target].Body), "_blank");
            HomePromotion4TargetNew = string.Equals(NullUtils.cvString(items[ConfigName.HomePromotion4Target].Body), "_blank");
            HomePromotion1ImageMobileTH = NullUtils.cvString(items[ConfigName.HomePromotion1ImageMobileTH].Body);
            HomePromotion2ImageMobileTH = NullUtils.cvString(items[ConfigName.HomePromotion2ImageMobileTH].Body);
            HomePromotion3ImageMobileTH = NullUtils.cvString(items[ConfigName.HomePromotion3ImageMobileTH].Body);
            HomePromotion4ImageMobileTH = NullUtils.cvString(items[ConfigName.HomePromotion4ImageMobileTH].Body);
            HomePromotion1ImageMobileEN = NullUtils.cvString(items[ConfigName.HomePromotion1ImageMobileEN].Body);
            HomePromotion2ImageMobileEN = NullUtils.cvString(items[ConfigName.HomePromotion2ImageMobileEN].Body);
            HomePromotion3ImageMobileEN = NullUtils.cvString(items[ConfigName.HomePromotion3ImageMobileEN].Body);
            HomePromotion4ImageMobileEN = NullUtils.cvString(items[ConfigName.HomePromotion4ImageMobileEN].Body);
        }

        public List<Config> ToList() {
            List<Config> items = new List<Config>();
            items.Add(new Config() { Id = ConfigName.HomePromotion1ImageTH, Body = HomePromotion1ImageTH.ToString() });
            items.Add(new Config() { Id = ConfigName.HomePromotion2ImageTH, Body = HomePromotion2ImageTH.ToString() });
            items.Add(new Config() { Id = ConfigName.HomePromotion3ImageTH, Body = HomePromotion3ImageTH.ToString() });
            items.Add(new Config() { Id = ConfigName.HomePromotion4ImageTH, Body = HomePromotion4ImageTH.ToString() });
            items.Add(new Config() { Id = ConfigName.HomePromotion1UrlTH, Body = HomePromotion1UrlTH.ToString() });
            items.Add(new Config() { Id = ConfigName.HomePromotion2UrlTH, Body = HomePromotion2UrlTH.ToString() });
            items.Add(new Config() { Id = ConfigName.HomePromotion3UrlTH, Body = HomePromotion3UrlTH.ToString() });
            items.Add(new Config() { Id = ConfigName.HomePromotion4UrlTH, Body = HomePromotion4UrlTH.ToString() });
            items.Add(new Config() { Id = ConfigName.HomePromotion1ImageEN, Body = HomePromotion1ImageEN.ToString() });
            items.Add(new Config() { Id = ConfigName.HomePromotion2ImageEN, Body = HomePromotion2ImageEN.ToString() });
            items.Add(new Config() { Id = ConfigName.HomePromotion3ImageEN, Body = HomePromotion3ImageEN.ToString() });
            items.Add(new Config() { Id = ConfigName.HomePromotion4ImageEN, Body = HomePromotion4ImageEN.ToString() });
            items.Add(new Config() { Id = ConfigName.HomePromotion1UrlEN, Body = HomePromotion1UrlEN.ToString() });
            items.Add(new Config() { Id = ConfigName.HomePromotion2UrlEN, Body = HomePromotion2UrlEN.ToString() });
            items.Add(new Config() { Id = ConfigName.HomePromotion3UrlEN, Body = HomePromotion3UrlEN.ToString() });
            items.Add(new Config() { Id = ConfigName.HomePromotion4UrlEN, Body = HomePromotion4UrlEN.ToString() });
            items.Add(new Config() { Id = ConfigName.HomePromotion1Target, Body = (HomePromotion1TargetNew ? "_blank" : "_self") });
            items.Add(new Config() { Id = ConfigName.HomePromotion2Target, Body = (HomePromotion2TargetNew ? "_blank" : "_self") });
            items.Add(new Config() { Id = ConfigName.HomePromotion3Target, Body = (HomePromotion3TargetNew ? "_blank" : "_self") });
            items.Add(new Config() { Id = ConfigName.HomePromotion4Target, Body = (HomePromotion4TargetNew ? "_blank" : "_self") });

            items.Add(new Config() { Id = ConfigName.HomePromotion1ImageMobileTH, Body = HomePromotion1ImageMobileTH.ToString() });
            items.Add(new Config() { Id = ConfigName.HomePromotion2ImageMobileTH, Body = HomePromotion2ImageMobileTH.ToString() });
            items.Add(new Config() { Id = ConfigName.HomePromotion3ImageMobileTH, Body = HomePromotion3ImageMobileTH.ToString() });
            items.Add(new Config() { Id = ConfigName.HomePromotion4ImageMobileTH, Body = HomePromotion4ImageMobileTH.ToString() });
            items.Add(new Config() { Id = ConfigName.HomePromotion1ImageMobileEN, Body = HomePromotion1ImageMobileEN.ToString() });
            items.Add(new Config() { Id = ConfigName.HomePromotion2ImageMobileEN, Body = HomePromotion2ImageMobileEN.ToString() });
            items.Add(new Config() { Id = ConfigName.HomePromotion3ImageMobileEN, Body = HomePromotion3ImageMobileEN.ToString() });
            items.Add(new Config() { Id = ConfigName.HomePromotion4ImageMobileEN, Body = HomePromotion4ImageMobileEN.ToString() });

            return items;
        }
        
    }
}