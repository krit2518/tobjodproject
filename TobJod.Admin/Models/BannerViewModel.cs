﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TobJod.Models;

namespace TobJod.Admin.Models {
    public class BannerViewModel : Banner {
        [Display(Name = "Image (TH) 1600x600 [jpg,png,gif. Size<=2MB]")] //1600x600
        [JsonIgnore]
        public HttpPostedFileBase ImageTHFile { get; set; }
        [Display(Name = "Image (EN) 1600x600 [jpg,png,gif. Size<=2MB]")]
        [JsonIgnore]
        public HttpPostedFileBase ImageENFile { get; set; }
        [Display(Name = "Image Mobile (TH) 1600x600 [jpg,png,gif. Size<=2MB]")]
        [JsonIgnore]
        public HttpPostedFileBase ImageMobileTHFile { get; set; }
        [Display(Name = "Image Mobile (EN) 1600x600 [jpg,png,gif. Size<=2MB]")]
        [JsonIgnore]
        public HttpPostedFileBase ImageMobileENFile { get; set; }

        public static BannerViewModel Map(Banner baseObject) {
            var serializedParent = JsonConvert.SerializeObject(baseObject);
            return JsonConvert.DeserializeObject<BannerViewModel>(serializedParent);
        }
        
    }
}