﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TobJod.Models;

namespace TobJod.Admin.Models {

    public class ProductImportViewModel : Product {
        [Display(Name = "Import File")]
        public HttpPostedFileBase ImportFile { get; set; }
    }

}