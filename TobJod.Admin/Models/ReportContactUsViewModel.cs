﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TobJod.Models;

namespace TobJod.Admin.Models {
    public class ReportContactUsViewModel : ReportViewModel {
        [Display(Name = "Category")]
        public int Category { get; set; }
        public List<SelectListItem> Categories { get; set; }
    }
}