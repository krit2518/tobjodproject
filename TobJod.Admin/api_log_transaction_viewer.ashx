﻿<%@ WebHandler Language="C#" Class="api_log_transaction_viewer" %>

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Web;
using Dapper;
using TobJod.Models;
using TobJod.Repositories;

public class api_log_transaction_viewer : IHttpHandler, System.Web.SessionState.IRequiresSessionState {

    public void ProcessRequest(HttpContext context) {
        if (!string.IsNullOrEmpty(context.Request.QueryString["f"])) {
            GetLogFile(context);
        } else {
            GetLogFileList(context);
        }
    }

    public void GetLogFileList(HttpContext context) {
        string adminId = context.Request.QueryString["adminId"];
        AdminRepository adminRepo = new AdminRepository();
        var admin = adminRepo.GetAdminByEmployeeId(adminId);
        if (admin == null) {
            context.Response.Write("Unauthrized");
            return;
        }
        context.Session["adminId"] = adminId;


        string prefix = context.Request.QueryString["prefix"];
        DirectoryInfo d = new DirectoryInfo(System.Configuration.ConfigurationManager.AppSettings["WebService_Log_Path"]);//Assuming Test is your Folder
        FileInfo[] Files = d.GetFiles("*.log"); //Getting Text files
        int count = 0;
        StringBuilder sb = new StringBuilder("<ul>");
        foreach (FileInfo file in Files) {
            if (file.Name.StartsWith(prefix)) {
                sb.AppendLine("<li><a href=\"?f=" + file.Name + "\">" + file.Name + "</a></li>");
                count++;
            }
        }
        sb.AppendLine("</ul>");
        if (count > 0) {
            context.Response.Write(sb.ToString());
        } else {
            context.Response.Write("No data found");
        }
    }

    public void GetLogFile(HttpContext context) {
        if (context.Session["adminId"] == null) {
            context.Response.Write("Unauthrized");
            return;
        }

        context.Response.ContentType = "text/plain";
        context.Response.WriteFile(System.Configuration.ConfigurationManager.AppSettings["WebService_Log_Path"] + context.Request.QueryString["f"].Trim(new char[] { '/','.' }));
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}