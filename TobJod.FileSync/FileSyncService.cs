﻿using FluentFTP;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TobJod.FileSync {
    public partial class FileSyncService : ServiceBase {

        #region Private variables

        /// <summary>
        /// Keeps a list of all the file watcher listeners, predefined on an XML file
        /// </summary>
        private List<CustomFolderSettings> listFolders;

        /// <summary>
        /// Keeps a list of all the file system watchers in execution.
        /// </summary>
        private List<FileSystemWatcher> listFileSystemWatcher;

        /// <summary>
        /// Name of the XML file where resides the specification of folders and extensions to be monitored
        /// </summary>
        private string fileNameXML;

        private string FTPHost, FTPUsername, FTPPassword, FTPPath, FTPPathSeparator, FTPHost2, FTPUsername2, FTPPassword2, FTPPath2, FTPPathSeparator2;
        private int FTPPort, FTPPort2;
        private bool FTP2;

        #endregion

        #region Protected Methods

        /// <summary>
        /// Event automatically fired when the service is started by Windows
        /// </summary>
        /// <param name="args">array of arguments</param>
        protected override void OnStart(string[] args) {
            FTPHost = ConfigurationManager.AppSettings["FTPHost"];
            FTPPort = int.Parse(ConfigurationManager.AppSettings["FTPPort"]);
            FTPUsername = ConfigurationManager.AppSettings["FTPUsername"];
            FTPPassword = ConfigurationManager.AppSettings["FTPPassword"];
            FTPPath = ConfigurationManager.AppSettings["FTPPath"];
            FTPPathSeparator = ConfigurationManager.AppSettings["FTPPathSeparator"];

            FTPHost2 = ConfigurationManager.AppSettings["FTPHost2"];
            FTPPort2 = int.Parse(ConfigurationManager.AppSettings["FTPPort2"]);
            FTPUsername2 = ConfigurationManager.AppSettings["FTPUsername2"];
            FTPPassword2 = ConfigurationManager.AppSettings["FTPPassword2"];
            FTPPath2 = ConfigurationManager.AppSettings["FTPPath2"];
            FTPPathSeparator2 = ConfigurationManager.AppSettings["FTPPathSeparator2"];

            FTP2 = !string.IsNullOrEmpty(FTPHost2);

            // Initialize the list of filesystem Watchers based on the XML configuration file
            PopulateListFileSystemWatchers();

            // Start the file system watcher for each of the file specification and folders found on the List<>
            StartFileSystemWatcher();
        }

        /// <summary>
        /// Event automatically fired when the service is stopped by Windows
        /// </summary>
        protected override void OnStop() {
            if (listFileSystemWatcher != null) {
                foreach (FileSystemWatcher fsw in listFileSystemWatcher) {
                    // Stop listening
                    fsw.EnableRaisingEvents = false;

                    // Record a log entry into Windows Event Log
                    CustomLogEvent(string.Format("Stop monitoring files with extension ({0}) in the folder ({1})", fsw.Filter, fsw.Path));

                    // Dispose the Object
                    fsw.Dispose();
                }

                // Cleans the list
                listFileSystemWatcher.Clear();
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Stops the main execution of the Windows Service
        /// </summary>
        private void StopMainExecution() {
            if (listFileSystemWatcher != null) {
                foreach (FileSystemWatcher fsw in listFileSystemWatcher) {
                    // Stop listening
                    fsw.EnableRaisingEvents = false;

                    // Record a log entry into Windows Event Log
                    CustomLogEvent(string.Format("Stop monitoring files with extension ({0}) in the folder ({1})", fsw.Filter, fsw.Path));

                    // Dispose the Object
                    fsw.Dispose();
                }

                // Cleans the list
                listFileSystemWatcher.Clear();
            }
        }

        /// <summary>
        /// Start the file system watcher for each of the file specification and folders found on the List<>
        /// </summary>
        private void StartFileSystemWatcher() {
            // Creates a new instance of the list
            this.listFileSystemWatcher = new List<FileSystemWatcher>();

            // Loop the list to process each of the folder specifications found
            foreach (CustomFolderSettings customFolder in listFolders) {
                DirectoryInfo dir = new DirectoryInfo(customFolder.FolderPath);

                // Checks whether the SystemWatcher is enabled and also the directory is a valid location
                if (customFolder.FolderEnabled && dir.Exists) {
                    // Creates a new instance of FileSystemWatcher
                    FileSystemWatcher fileSWatch = new FileSystemWatcher();

                    // Sets the filter
                    fileSWatch.Filter = customFolder.FolderFilter;

                    // Sets the folder location
                    fileSWatch.Path = customFolder.FolderPath;
                    
                    // Sets the folder inculsion
                    fileSWatch.IncludeSubdirectories = customFolder.FolderIncludeSub;

                    //// Sets the action to be executed
                    //StringBuilder actionToExecute = new StringBuilder(customFolder.ExecutableFile);

                    //// List of arguments
                    //StringBuilder actionArguments = new StringBuilder(customFolder.ExecutableArguments);

                    // Subscribe to notify filters
                    fileSWatch.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.DirectoryName;

                    // Associate the event that will be triggered when a new file is added to the monitored folder, using a lambda Expression                    
                    fileSWatch.Created += (senderObj, fileSysArgs) => fileSWatch_Created(senderObj, fileSysArgs); //, actionToExecute.ToString(), actionArguments.ToString());
                    
                    fileSWatch.Changed += (senderObj, fileSysArgs) => fileSWatch_Changed(senderObj, fileSysArgs); //, actionToExecute.ToString(), actionArguments.ToString());
                    
                    fileSWatch.Deleted += (senderObj, fileSysArgs) => fileSWatch_Deleted(senderObj, fileSysArgs); //, actionToExecute.ToString(), actionArguments.ToString());

                    // Begin watching
                    fileSWatch.EnableRaisingEvents = true;

                    // Add the systemWatcher to the list
                    listFileSystemWatcher.Add(fileSWatch);

                    // Record a log entry into Windows Event Log
                    CustomLogEvent(string.Format("Starting to monitor files with extension ({0}) in the folder ({1})", fileSWatch.Filter, fileSWatch.Path));
                } else {
                    // Record a log entry into Windows Event Log
                    CustomLogEvent(string.Format("File system monitor cannot start because the folder ({0}) does not exist", customFolder.FolderPath));
                }
            }
        }

        /// <summary>
        /// This event is triggered when a file with the specified extension is created on the monitored folder
        /// </summary>
        /// <param name="sender">Object raising the event</param>
        /// <param name="e">List of arguments - FileSystemEventArgs</param>
        void fileSWatch_Created(object sender, FileSystemEventArgs e) {
            string filePath = e.FullPath;
            string path = Path.GetDirectoryName(e.Name);
            FTPSUploadFile(filePath, path, FTPHost, FTPPort, FTPUsername, FTPPassword, FTPPath, FTPPathSeparator);
            if (FTP2) FTPSUploadFile(filePath, path, FTPHost2, FTPPort2, FTPUsername2, FTPPassword2, FTPPath2, FTPPathSeparator2);
        }

         /// <summary>
        /// This event is triggered when a file with the specified extension is changed on the monitored folder
        /// </summary>
        /// <param name="sender">Object raising the event</param>
        /// <param name="e">List of arguments - FileSystemEventArgs</param>
        void fileSWatch_Changed(object sender, FileSystemEventArgs e) {
            string filePath = e.FullPath;
            string path = Path.GetDirectoryName(e.Name);
            FTPSUploadFile(filePath, path, FTPHost, FTPPort, FTPUsername, FTPPassword, FTPPath, FTPPathSeparator);
            if (FTP2) FTPSUploadFile(filePath, path, FTPHost2, FTPPort2, FTPUsername2, FTPPassword2, FTPPath2, FTPPathSeparator2);
        }
        
        /// <summary>
        /// This event is triggered when a file with the specified extension is created on the monitored folder
        /// </summary>
        /// <param name="sender">Object raising the event</param>
        /// <param name="e">List of arguments - FileSystemEventArgs</param>
        void fileSWatch_Deleted(object sender, FileSystemEventArgs e) {
            string path = Path.GetDirectoryName(e.Name);
            string fileName = Path.GetFileName(e.Name);
            FTPSDeleteFile(fileName, path + (!string.IsNullOrEmpty(path) ? FTPPathSeparator : ""), FTPHost, FTPPort, FTPUsername, FTPPassword, FTPPath, FTPPathSeparator);
            if (FTP2) FTPSDeleteFile(fileName, path + (!string.IsNullOrEmpty(path) ? FTPPathSeparator2 : ""), FTPHost2, FTPPort2, FTPUsername2, FTPPassword2, FTPPath2, FTPPathSeparator2);
        }

        /// <summary>
        /// Record messages and logs into the Windows Event log
        /// </summary>
        /// <param name="message">Message to be recorded into the Windows Event log</param>
        private void CustomLogEvent(string message) {
            string eventSource = "TobJod File Sync Service"; // this.ServiceName;
            DateTime dt = new DateTime();
            dt = System.DateTime.UtcNow;
            message = dt.ToLocalTime() + ": " + message;

            EventLog.WriteEntry(eventSource, message);
        }

        /// <summary>
        /// Reads an XML file and populates a list of <CustomFolderSettings>
        /// </summary>
        private void PopulateListFileSystemWatchers() {
            /// Get the XML file name from the APP.Config file
            fileNameXML = AppDomain.CurrentDomain.BaseDirectory + "FolderSettings.xml";// ConfigurationManager.AppSettings["XMLFileFolderSettings"];

            // Creates an instance of XMLSerializer
            XmlSerializer deserializer = new XmlSerializer(typeof(List<CustomFolderSettings>));

            TextReader reader = new StreamReader(fileNameXML);
            object obj = deserializer.Deserialize(reader);

            // Close the TextReader object
            reader.Close();

            // Obtains a list of strongly typed CustomFolderSettings from XML Input data
            listFolders = obj as List<CustomFolderSettings>;
        }

        /// <summary>
        /// Executes a set of instructions through the command window
        /// </summary>
        /// <param name="executableFile">Name of the executable file or program</param>
        /// <param name="argumentList">List of arguments</param>
        private void ExecuteCommandLineProcess(string executableFile, string argumentList) {
            // Use ProcessStartInfo class.
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = true;
            startInfo.UseShellExecute = false;
            startInfo.FileName = executableFile;
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.Arguments = argumentList;

            try {
                // Start the process with the info we specified.
                // Call WaitForExit and then the using-statement will close.
                using (Process exeProcess = Process.Start(startInfo)) {
                    exeProcess.WaitForExit();

                    // Register a log of the successful operation
                    CustomLogEvent(string.Format("Succesful operation --> Executable: {0} --> Arguments: {1}", executableFile, argumentList));
                }
            } catch (Exception exc) {
                // Register a Log of the Exception
                CustomLogEvent(exc.Message);
            }
        }

        private bool FTPSUploadFile(string sourceFile, string destinationPath, string FTPHost, int FTPPort, string FTPUsername, string FTPPassword, string FTPPath, string FTPPathSeparator) {
            try {
                FtpClient client = new FtpClient(FTPHost, FTPPort, FTPUsername, FTPPassword);
                client.SslProtocols = System.Security.Authentication.SslProtocols.Default;
                client.EncryptionMode = FtpEncryptionMode.Implicit;
                client.SslProtocols = System.Security.Authentication.SslProtocols.Tls;
                client.ValidateCertificate += OnValidateCertificate;

                //client.SocketPollInterval = 1000;
                //client.ConnectTimeout = 2000;
                //client.ReadTimeout = 2000;
                //client.DataConnectionConnectTimeout = 2000;
                //client.DataConnectionReadTimeout = 2000;

                //client.Credentials = new NetworkCredential(FTPUsername, FTPPassword);
                client.Connect();
                
                if (!client.DirectoryExists(FTPPath + destinationPath)) client.CreateDirectory(FTPPath + destinationPath);
                client.RetryAttempts = 3;
                client.SetWorkingDirectory(FTPPath + FTPPathSeparator + destinationPath + FTPPathSeparator);
                client.UploadFile(sourceFile, Path.GetFileName(sourceFile), FtpExists.Overwrite, true, FtpVerify.Retry);
                //Console.WriteLine(FTPPath + " xxx  " + destinationPath + " yyy " + Path.GetFileName(sourceFile));
                return true;
            } catch (Exception ex) {
                CustomLogEvent("FTP Error: " + ex.ToString());
            }
            return false;
        }

        private bool FTPSDeleteFile(string fileName, string destinationPath, string FTPHost, int FTPPort, string FTPUsername, string FTPPassword, string FTPPath, string FTPPathSeparator) {
            try {
                FtpClient client = new FtpClient(FTPHost, FTPPort, FTPUsername, FTPPassword);
                client.SslProtocols = System.Security.Authentication.SslProtocols.Default;
                client.EncryptionMode = FtpEncryptionMode.Implicit;
                client.SslProtocols = System.Security.Authentication.SslProtocols.Tls;
                client.ValidateCertificate += OnValidateCertificate;


                //client.SocketPollInterval = 1000;
                //client.ConnectTimeout = 60000;
                //client.ReadTimeout = 2000;
                //client.DataConnectionConnectTimeout = 2000;
                //client.DataConnectionReadTimeout = 2000;

                //client.Credentials = new NetworkCredential(FTPUsername, FTPPassword);
                client.Connect();
                //client.ChangeDirectory(destinationPath);
                client.SetWorkingDirectory(FTPPath + FTPPathSeparator + destinationPath + FTPPathSeparator);
                client.DeleteFile(fileName);
                return true;
            } catch (Exception ex) {
                CustomLogEvent("FTP Error: " + ex.ToString());
            }
            return false;
        }

        static void OnValidateCertificate(FtpClient control, FtpSslValidationEventArgs e) {
            e.Accept = true;
        }

        //private bool SFTPUploadFile(string sourceFile, string destinationPath) {
        //    try {
        //        using (SftpClient client = new SftpClient(FTPHost, FTPPort, FTPUsername, FTPPassword)) {
        //            client.Connect();
        //            try {
        //                client.ChangeDirectory(FTPPath + destinationPath);
        //            } catch {
        //                client.CreateDirectory(FTPPath + destinationPath);
        //                client.ChangeDirectory(FTPPath + destinationPath);
        //            }
        //            using (FileStream fs = new FileStream(sourceFile, FileMode.Open)) {
        //                client.BufferSize = 4 * 1024;
        //                client.UploadFile(fs, Path.GetFileName(sourceFile));
        //            }
        //        }
        //        return true;
        //    } catch (Exception ex) {
        //        CustomLogEvent("FTP Error: " + ex.ToString());
        //    }
        //    return false;
        //}

        //private bool SFTPDeleteFile(string fileName, string destinationPath) {
        //    try {
        //        using (SftpClient client = new SftpClient(FTPHost, FTPPort, FTPUsername, FTPPassword)) {
        //            client.Connect();
        //            //client.ChangeDirectory(destinationPath);
        //            client.DeleteFile(FTPPath + destinationPath + fileName);
        //        }
        //        return true;
        //    } catch (Exception ex) {
        //        CustomLogEvent("FTP Error: " + ex.ToString());
        //    }
        //    return false;
        //}

        #endregion


        //Debugging
        public void TestStartupAndStop() {
            this.OnStart(null);
            Console.ReadLine();
            this.OnStop();
        }
    }
}