﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace TobJod.FileSync
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            if (Environment.UserInteractive) {
                FileSyncService fileSyncService = new FileSyncService();
                fileSyncService.TestStartupAndStop();
            } else {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                new FileSyncService()
                };
                ServiceBase.Run(ServicesToRun);
            }
        }

    }
}
