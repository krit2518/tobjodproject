﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace TobJod.FileSync {
    
    /// <summary>
    /// This class is intended to install the Service into the Windows Services pool by using the command installutil
    /// </summary>
    [RunInstaller(true)]
    public class ProjectInstaller : Installer
    {
        #region Private members
        /// <summary>
        /// Defines the name of the Windows Service
        /// </summary>
        private const string WindowsServiceName = "TobJod File Sync Service";

        /// <summary>
        /// Defines the description of the Windows Service
        /// </summary>
        private const string WindowsServiceDescription = "Service to monitor changes in the filesystem and trigger events as response";
        
        private ServiceProcessInstaller serviceProcessInstaller;
        private ServiceInstaller serviceInstaller;

        #endregion

        #region Constructors

        public ProjectInstaller()
        {
            serviceProcessInstaller = new ServiceProcessInstaller();
            serviceInstaller = new ServiceInstaller();
            // Here you can set properties on serviceProcessInstaller
            //or register event handlers            
            serviceProcessInstaller.Account = ServiceAccount.LocalService;
            serviceInstaller.ServiceName = WindowsServiceName;
            serviceInstaller.Description = WindowsServiceDescription;            
            this.Installers.AddRange(new Installer[] { serviceProcessInstaller, serviceInstaller }); 
        }
        //c:\Windows\Microsoft.NET\Framework64\v4.0.30319\InstallUtil.exe c:\Work\Projects\TobJod\www\TobJod.FileSync\bin\Release\TobJod.FileSync.exe
        //installutil -u <yourproject>.exe 
        #endregion
    }
}
